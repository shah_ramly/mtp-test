scriptTags = document.getElementsByTagName('script');
jsPath = scriptTags[scriptTags.length - 1].src.split('?')[0].split('/').slice(0, -1).join('/') + '/';
rootPath = jsPath.split('assets'); rootPath.pop();
rootPath = rootPath.join('assets');
sitePath = rootPath.split('backpanel'); sitePath.pop();
sitePath = sitePath.join('backpanel');

url = document.URL.split('/');
thelast = url[url.length - 1] ? url[url.length - 1] : url[url.length - 2];
backUrl = url[url.length - 1] ? document.URL.replace(/\/[^\/]*$/, '') : document.URL.replace(/\/[^\/]*$/+'/', '');

$(function(){
	
	$('html').bind('ajaxStart', function(){  
		$(this).addClass('busy');  
	}).bind('ajaxStop', function(){  
		$(this).removeClass('busy');  
	});

	setTimeout(function(){
		$('[data-title]').tooltip();
	}, 100);
	
	$(window).resize(function() {
		winWidth = $(window).width();
		winHeight = $(window).height();
		
		if(winWidth <= 975) sortableDisable();
		else sortable();
		
	}).resize();
	
	if( $('.datatable').length ) $('.datatable').dataTable({"aaSorting": [], "pageLength": 25});
	if( $('.chosen-select').length ) $('.chosen-select').chosen();
	if( $('.colorpicker').length ) $('.colorpicker').colorpicker();
	
	if( $('.date').length ){
		$('.date').datetimepicker({
			format: 'DD/MM/YYYY',
        });
	}

	$(document).on('click', 'a[href="#"]', function(e){
		e.preventDefault();
		return false;
	});

	$(document).on('click', '.remove-docs', function(e){
		$('.remove-docs').tooltip('dispose');
		parent = $(this).parent();
		parent.slideUp(function(){
			parent.remove();
			$('[data-title]').tooltip();
		});
	});
	
	$("#statusForm").submit(function(){
		var data = $(this).serializeArray();
		if( $(this).find('#html').length == 1 ) data.push({name: 'content', value: CKEDITOR.instances.html.getData() });
		if( $(this).find('#description').length == 1 ) data.push({name: 'description', value: CKEDITOR.instances.description.getData() });
		if( $(this).find('#description2').length == 1 ) data.push({name: 'description2', value: CKEDITOR.instances.description2.getData() });
		// ajax_form(data);
		ajax_form_delivery(data);
		return false;
	});
	
	$(document).on('submit', '#selfForm', function(){
		var data = $(this).serializeArray();
		if( $(this).find('#html').length == 1 ) data.push({name: 'content', value: CKEDITOR.instances.html.getData() });
		if( $(this).find('#description').length == 1 ) data.push({name: 'description', value: CKEDITOR.instances.description.getData() });
		if( $(this).find('#description2').length == 1 ) data.push({name: 'description2', value: CKEDITOR.instances.description2.getData() });
		// ajax_form(data);
		ajax_form(data, '', '');
		return false;
	});

	$("#backForm").submit(function(){
		var data = $(this).serializeArray();
		if( $(this).find('#html').length == 1 ) data.push({name: 'content', value: CKEDITOR.instances.html.getData() });
		if( $(this).find('#description').length == 1 ) data.push({name: 'description', value: CKEDITOR.instances.description.getData() });
		if( $(this).find('#description2').length == 1 ) data.push({name: 'description2', value: CKEDITOR.instances.description2.getData() });
		ajax_form_action(data, '', 'back');
		return false;
	});


	$("#redirectForm").submit(function(){
		var data = $(this).serializeArray();
		if( $(this).find('#html').length == 1 ) data.push({name: 'content', value: CKEDITOR.instances.html.getData() });
		if( $(this).find('#description').length == 1 ) data.push({name: 'description', value: CKEDITOR.instances.description.getData() });
		if( $(this).find('#description2').length == 1 ) data.push({name: 'description2', value: CKEDITOR.instances.description2.getData() });

		ajax_form_action_redirect(data, "", 'direct');
		return false;
	});

	$(document).on('click', '.delete', function(e){
		e.preventDefault();

		row = $(this).closest('tr');
		id = $(this).data('id');
		type = $(this).data('type');
		var data = { act: 'delete', type: type, id: id }
		
		bootbox.confirm('<i class="fa fa-warning danger"></i> <strong>Are you sure to delete this ' + type + '?</strong>', function(result) {
			if(result === true){
				ajax_form(data, rootPath + 'delete', '.datatable');
				
			  	row.fadeOut(function(){
					row.remove();
				});
			}
		}); 
	});
	
	$(document).on('click', '.back', function(e){
		e.preventDefault();
		window.location = backUrl;
	});
		
	if(window.location.hash) {
		var hash = window.location.hash.substring(1);
		$('a[href="#' + hash + '"]').click();
	}

	$(document).on('change', '.category', function(){
		var el = $(this);
		var id = el.val();
		var target = el.data('target');
		var type = el.data('type');
		if( type === undefined ) type = '';
		if(target){
			$.ajax({
				type: 'GET',
				url: rootPath + 'ajax/category/' + id + '/' + type,
				dataType : 'json',
				data: { },
				success: function(results){
					if(results.error == true){
						t('e', results.msg, results.title);
					}else{
						$('[name="' + target + '"]').html(results.html);
						selected = $('[name="' + target + '"]').data('selected');
						if(selected){
							$('[name="' + target + '"]').val(selected).change();
						}
						var categoryUpdatedEvent = $.Event("categoryUpdated");
						$('[name="' + target + '"]').trigger(categoryUpdatedEvent);
					}
				},
				error: function(error){
					ajax_error(error);
				},
				complete: function(){
					
				}
			});
		}
	});	
	
	$(document).on('change', '.country', function(){
		var el = $(this);
		var id = el.val();
		var target = el.data('target');
		
		if(target){
			$('select[name="' + target + '"]').prop('disabled', true);
			$('.state-loading').removeClass('hide');
			$.ajax({
				type: 'GET',
				url: rootPath + 'ajax/country/' + id,
				dataType : 'json',
				data: { },
				success: function(results){
					if(results.error == true){
						t('e', results.msg, results.title);
					}else{
						$('select[name="' + target + '"]').html(results.states);
						selected = $('select[name="' + target + '"]').data('selected');
						if(selected){
							if( $('select[name="' + target + '"] option[value="' + selected + '"]').length ){
								$('select[name="' + target + '"]').val(selected).change();
							}else{
								$('select[name="' + target + '"]').val('').change();
							}
						}
						var countryUpdatedEvent = $.Event("countryUpdated");
						$('select[name="' + target + '"]').trigger(countryUpdatedEvent);
						$('select[name="' + target + '"]').prop('disabled', false);
						$('.state-loading').addClass('hide');
					}
				},
				error: function(error){
					ajax_error(error);
				},
				complete: function(){
					$('select[name="' + target + '"]').change();
				}
			});
		}
	});	

	//$('.country').change();

	$(document).on('change', '.state', function(){
		var el = $(this);
		var id = el.val();
		var target = el.data('target');
		if(target && id){
			$('select[name="' + target + '"]').prop('disabled', true);
			$('.city-loading').removeClass('hide');
			$.ajax({
				type: 'GET',
				url: rootPath + 'ajax/cities/' + id,
				dataType : 'json',
				data: { },
				success: function(results){
					if(results.error == true){
						t('e', results.msg, results.title);
					}else{
						$('select[name="' + target + '"]').html(results.states);
						selected = $('select[name="' + target + '"]').data('selected');
						if(selected){
							if( $('select[name="' + target + '"] option[value="' + selected + '"]').length ){
								$('select[name="' + target + '"]').val(selected);
							}else{
								$('select[name="' + target + '"]').val('');
							}
						}
						var stateUpdatedEvent = $.Event("stateUpdated");
						$('select[name="' + target + '"]').trigger(stateUpdatedEvent);
						$('select[name="' + target + '"]').prop('disabled', false);
						$('.city-loading').addClass('hide');
					}
				},
				error: function(error){
					ajax_error(error);
				},
				complete: function(){

				}
			});
		}
	});	
	
	$(document).on('click', '.addMultiImg a', function(){
		newTotal = $('.multiImg').length + 1;
		while( $('#photo'+newTotal).length != 0 ){
			newTotal++;	
		}
		newImg = '<div class="multiImg">';
		newImg += '<input id="photo'+newTotal+'" name="images[' + newTotal + '][src]" value="" type="hidden">';
		newImg += '<input name="images[' + newTotal + '][alt]" class="form-control" placeholder="Alt" value="" type="text">';
		newImg += '<a href="javascript:void(0)" onclick="kcFinderB(\'photo'+newTotal+'\')">Browse</a> | ';
		newImg += '<a href="javascript:void(0)" onclick="kcFinderR(\'photo'+newTotal+'\')">Cancel</a>';
		newImg += '</div>';
		$(this).parent().prev().append(newImg);
	});
	
	if( !$('.multiImg').length ) $('.addMultiImg a').click();
	
	$(document).on('click', '.addMultiPrices a', function(){
		newTotal = $('.multiImg').length;
		while( $('select[name="prices[' + newTotal + '][country]"]').length != 0 ){
			newTotal++;	
		}
		newImg = '<div class="multiPrices">';
		newImg += '<select class="form-control" name="prices[' + newTotal + '][country]">' + countryList + '</select> ';
		newImg += '<input type="text" class="form-control" name="prices[' + newTotal + '][price]" placeholder="Price" value=""> ';
		newImg += '<a href="#" title="Remove"><i class="fa fa-trash-o"></i></a>';
		newImg += '</div>';
		$(this).parent().prev().append(newImg);
	});
		
	$(document).on('click', '.multiPrices a, .multiOptions > a, ul.options > li > a, ul.option > li > a', function(e){
		e.preventDefault();
		el = $(this);
		el.parent().remove();
	});
	
	$(document).on('click', '.addMultiOptions a', function(){
		newTotal = $('.multiOptions').length;
		while( $('input[name="options[' + newTotal + '][title]"]').length != 0 ){
			newTotal++;	
		}
		option = '<div class="multiOptions" data-id="' + newTotal + '">';
		option += '<input type="text" class="form-control" name="options[' + newTotal + '][title]" placeholder="Title (Eg: Size)" value="" required> ';
		option += '<a href="#" title="Remove"><i class="fa fa-trash-o"></i></a> ';
		option += '<ul class="options">';
		option += '<a href="javascript:;" class="btn btn-default"><i class="fa fa-plus"></i> Add Value</a>';
		option += '</ul>';
		option += '</div>';
		$(this).parent().prev().append(option);
	});
	
	$(document).on('click', 'ul.options > a', function(e){
		e.preventDefault();
		id = $(this).closest('.multiOptions').data('id');
		
		newTotal = $(this).parent().find('li').length;
		while( $('input[name="options[' + id + '][values][' + newTotal + '][title]"]').length != 0 ){
			newTotal++;	
		}
				
		li = '<li>';
		li += '<input type="text" class="form-control" name="options[' + id + '][values][' + newTotal + '][title]" value="" placeholder="Title" required> ';
		li += '<input type="text" class="form-control" name="options[' + id + '][values][' + newTotal + '][price]" value="" placeholder="Price (Eg: +10 or -10)"> ';
		li += '<input type="text" class="form-control" name="options[' + id + '][values][' + newTotal + '][stock]" value="0" placeholder="Stock"> ';
		li += '<a href="#" title="Remove"><i class="fa fa-trash-o"></i></a>';
		li += '</li>';
				
		$(this).before(li);
	});
	
	$(document).on('click', 'ul.option > a', function(e){
		e.preventDefault();
		id = $(this).closest('.singleOption').data('id');
		
		newTotal = $(this).parent().find('li').length;
		while( $('input[name="colors[' + newTotal + '][title]"]').length != 0 ){
			newTotal++;	
		}
				
		li = '<li>';
		li += '<input type="text" class="form-control" name="colors[' + newTotal + '][title]" value="" placeholder="Title" required> ';
		li += '<input type="text" class="form-control colorpicker" name="colors[' + newTotal + '][code]" value="" placeholder="#000000" required> ';
		li += '<input type="text" class="form-control" name="colors[' + newTotal + '][price]" value="" placeholder="Price (Eg: +10 or -10)"> ';
		li += '<input type="text" class="form-control" name="colors[' + newTotal + '][stock]" value="0" placeholder="Stock"> ';
		li += '<a href="#" title="Remove"><i class="fa fa-trash-o"></i></a>';
		li += '</li>';
				
		$(this).before(li);
		$('.colorpicker').colorpicker();
	});
	
	sortable();
	
	$(document).on('click', '.remove-slider', function(e){
		e.preventDefault();
		el = $(this).parent();
		el.slideUp(function(){
			el.remove();
		});
	});
	
	$(document).on('click', '.add-more-slider', function(e){
		e.preventDefault();
		
		newTotal = $('.multi-slider li').length;
		while( $('input[name="slide[' + newTotal + '][src]"]').length != 0 ){
			newTotal++;	
		}
		el = '<li>';
		el += '<input id="photo' + newTotal + '" type="text" class="form-control" onclick="kcFinderB(\'photo' + newTotal + '\')" name="slide[' + newTotal + '][src]" value="" placeholder="Browse" required>';
		el += '<input type="text" class="form-control" name="slide[' + newTotal + '][href]" value="" placeholder="http://www.google.com/">';
		el += '<input type="button" class="btn btn-danger btn-sm remove-slider" value="Remove">';
		el += '</li>';
		$('ul.multi-slider').append(el);
	});

	$(document).on('keyup', '[type="tel"]', function(){
		if (/\D/g.test(this.value)){
			this.value = this.value.replace(/\D/g, '');
		}
	});
});


sortable = function(){
	if( $('.sortable').length ) $('.sortable').sortable({ revert: true });
};

sortableDisable = function(){
	if( $('.sortable').length ) $('.sortable').sortable('disable');
}
	
$(document).ready(function(){
	if ("geolocation" in navigator) {
    $('.js-geolocation').show(); 
  } else {
    $('.js-geolocation').hide();
  }

  /* Where in the world are you? */
  $(document).on('click', '.js-geolocation', function() {
    navigator.geolocation.getCurrentPosition(function(position) {
      loadWeather(position.coords.latitude+','+position.coords.longitude); //load weather using your lat/lng coordinates
    });
  });

  loadWeather('Kuala Lumpur','');
  monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  dayNames = ["S", "M", "T", "W", "T", "F", "S"];

  var cTime = new Date(), day = cTime.getDay(), month = cTime.getMonth()+1, year = cTime.getFullYear();

  var events = [
  
      {
        "date": day+"/"+month+"/"+year, 
        "title": 'Today', 
        "link": 'javascript:;', 
        "color": 'rgba(255,255,255,0.2)', 
      },
	  /*
      {
        "date": "7/"+month+"/"+year, 
        "title": 'Kick off meeting!', 
        "link": 'javascript:;', 
        "color": 'rgba(255,255,255,0.2)', 
        "content": 'Have a kick off meeting with .inc company'
      },
      {
        "date": "19/"+month+"/"+year, 
        "title": 'Link to Google', 
        "link": 'http://www.google.com', 
        "color": 'rgba(255,255,255,0.2)', 
      }
	*/
    ];

	/*
    $('#calendar-box2').bic_calendar({
        events: events,
        dayNames: dayNames,
        monthNames: monthNames,
        showDays: true,
        displayMonthController: true,
        displayYearController: false,
        popoverOptions:{
            placement: 'top',
            trigger: 'hover',
            html: true
        },
        tooltipOptions:{
            placement: 'top',
            html: true
        }
	});
	*/
});

t = function(type, text, title) {
	if(type == "e"){
		icon = "fa fa-exclamation";
		title = title || 'Error!';
		type = 'danger';
	}else if(type == "w"){
		icon = "fa fa-warning";
		title = title || 'Warning!';
		type = 'warning';
	}else if(type == "s"){
		icon = "fa fa-check";
		title = title || 'Success!';
		type = 'success';
	}else if(type == "i"){
		icon = "fa fa-question";
		title = title || 'Information';
		type = 'info';
	}else{
		icon = "fa fa-circle-o";
		title = title || 'Information';
		type = '';
	}
	
    $.notify({
        title: title,
		message: text,
		icon: icon
    }, {
		element: 'body',
		type: type,
		allow_dismiss: true,
		template: '<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert">' +
			'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
			'<span data-notify="icon"></span> ' +
			'<span data-notify="title">{1}</span> ' +
			'<span data-notify="message">{2}</span>' +
			'<div class="progress" data-notify="progressbar">' +
				'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
			'</div>' +
			'<a href="{3}" target="{4}" data-notify="url"></a>' +
		'</div>'
    });
}

ajax_form_delivery = function(data, url, dyntable){
	$.ajax({
		type:'POST',
		url: url,
		dataType : 'json',
		data:data,
		success: function(results){
			if(results.error == true){
				t('e', results.msg, results.title);
			}else{
				//t('s', results.msg, results.title);
				window.location.reload();
			}
		},
		error: function(error){
			ajax_error(error);
		},
		complete: function(){
			if(dyntable){
				$(dyntable).dataTable().fnDraw();
				window.location = backUrl;
			}
		}
	});           
}

ajax_form = function(data, url, dyntable){
	$.ajax({
		type:'POST',
		url: url,
		dataType : 'json',
		data:data,
		success: function(results){
			if(results.error == true){
				t('e', results.msg, results.title);
			}else{
				//t('s', results.msg, results.title);
			}
			return false;
		},
		error: function(error){
			ajax_error(error);
		},
		complete: function(){
			if(dyntable){
				$(dyntable).dataTable().fnDraw();
				window.location = backUrl;
			}
		}
	});           
}


ajax_form_action_redirect = function(data, url, action){
		$.ajax({
		type:'POST',
		url: url,
		dataType : 'html',
		data:data,
		success: function(results){
			
			var	results = JSON.parse(results);
			if(results.error == true){
				t('e', results.msg, results.title);
			}else{		
				//t('s', results.msg, results.title);
				//setTimeout(function(){
					if(action == 'reload') window.location.reload();
					if(action == 'direct')   window.location = results.url  ;
					if(action == 'back'){
						window.location = backUrl;
					}
				//}, 1000);
			}
		},
		error: function(error){

			alert("Error In ajax_form_action_redirect in script.js");
			ajax_error(error);
		},
		complete: function(){}
	});  
}
	
ajax_form_action_by_email = function(data, url, action,realURL){
	$.ajax({
		type:'POST',
		url: url,
		dataType : 'json',
		data:data,
		success: function(results){
			
			if(results.error == true){
				t('e', results.msg, results.title);
			}else{		
				//t('s', results.msg, results.title);
				//setTimeout(function(){
					if(action == 'reload') window.location.reload();
					if(action == 'direct')   window.location = realURL + "?email="+data[2]['value'];
					if(action == 'back'){
						window.location = backUrl;
					}
				//}, 1000);
			}
		},
		error: function(error){
			alert("error")
			ajax_error(error);
		},
		complete: function(){}
	});           
}

ajax_error = function(error){
	if(error.status == 0){
		t('e', 'You network currently are offline!!');
	}else if(error.status == 404){
		t('e', 'Requested URL not found.');
	}else if(error.status == 500){
		t('e', 'Internel Server Error.');
	}else if(error == 'parsererror'){
		t('e', 'Parsing JSON Request failed.');
	}else if(error == 'timeout'){
		t('e', 'Request Time out.');
	}else {
		t('e', 'Unknow Error : '+ error.responseText);
	}
}

kcFinderB = function(image){
    window.KCFinder = {
        callBack: function(url) {
				var filename = url.substring(url.lastIndexOf('/')+1);
				val = url.split('upload').pop();
				val = 'upload' + val;
				$('#'+image).val(val);
				$('#'+image).prev('img').remove();
				$('#'+image).before('<img src="'+url+'" style="display:table;margin:5px;max-width:200px;max-height:100px;">');
			}
		}
		window.open(jsPath + 'kcfinder/browse.php?type=images', 'kcfinder_single', 'status=0, toolbar=0, location=0, menubar=0, directories=0, resizable=1, scrollbars=0, width=800, height=600');
}

kcFinderC = function(image){
	$('#'+image).val('');
	$('#'+image).prev('img').attr('src', rootPath + 'assets/img/no_image.jpg');
}

kcFinderR = function(image){
	el = $('#'+image).parent().attr('class');
	$('#'+image).parent().remove();
	//if( $('.' + el).length > 1 ) $('#'+image).parent().remove();
	//else t('e', 'At least 1 image is required.');
}

loadWeather = function(location, woeid) {
  /*
  $.simpleWeather({
    location: location,
    woeid: woeid,
    unit: 'c',
    success: function(weather) {
      html = '<h2><i class="wicon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+' <span class="w-temp2">/ '+weather.tempAlt+'&deg;F</span></h2>';
      html += '<span class="w-region">'+weather.city+', '+weather.region+'</li>';
      html += '<span class="w-currently">'+weather.currently+'</span>';
      html += '';  
      
      $("#weather").html(html);
    },
    error: function(error) {
      $("#weather").html('<p>'+error+'</p>');
    }
  });
  */
}
