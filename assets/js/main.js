$(function () {
    
    
    var transparent = !0,
        transparentDemo = !0,
        fixedTop = !1,
        navbar_initialized = !1,
        backgroundOrange = !1,
        sidebar_mini_active = !1,
        toggle_initialized = !1,
        $html = $("html"),
        $body = $("body");
    
        /*const mode = localStorage.getItem('mode') || 'od-mode';
        if(mode === 'ft-mode') {
            $body.removeClass('od-mode').addClass(mode);
            $('input[name*="odft_mode"]').each(function(idx, checkbox){ $(checkbox).prop('checked', true) });
        }*/
        
        $(document).on("click", ".navbar-togglerz", function () {
            var a = $(this);
            if (1 == menuDashboard.misc.navbar_menu_visible) $html.removeClass("nav-open"), menuDashboard.misc.navbar_menu_visible = 0, setTimeout(function () {
                a.parent().removeClass("toggled"), $(".bodyClick").remove()
            },10);
            else {
                if ($body.hasClass("page-pub")) {
                    setTimeout(function () {
                        a.parent().addClass("toggled")
                    }, 10);
                }
                else {setTimeout(function () {
                        a.parent().addClass("toggled")
                    }, 500);
                }
                
                $('<div class="bodyClick"></div>').appendTo("body").click(function () {
                    $html.removeClass("nav-open"), menuDashboard.misc.navbar_menu_visible = 0, setTimeout(function () {
                        a.parent().removeClass("toggled"), $(".bodyClick").remove()
                    }, 550)
                }), $html.addClass("nav-open"), menuDashboard.misc.navbar_menu_visible = 1
            }
        }),menuDashboard = {
        misc: {
            navbar_menu_visible: 0
        },
        showSidebarMessage: function (a) {
            try {
                $.notify({
                    icon: "tim-icons ui-1_bell-53",
                    message: a
                }, {
                    type: "info",
                    timer: 4e3,
                    placement: {
                        from: "top",
                        align: "right"
                    }
                })
            } catch (a) {
                console.log("Notify library is missing, please make sure you have the notifications library added.")
            }
        }
    };

    $('#navbarMainList a').on('click', function(){
        $('#navbarMainList').collapse('hide');
    });
        
        // Custom Template Tooltip -> Complete Profile + Post A Task
        $('.btn-complete-profile-header .btn-label').tooltip({
            template: '<div class="tooltip tooltip-fancy tooltip-complete-profile" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
            title: $('#completeProfileBtn').data('text'),
            placement: 'bottom'
        });
            
        $('#post_task_btn .plus-icon').tooltip({
            template: '<div class="tooltip tooltip-fancy tooltip-post-a-task" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
            title: $('#post_task_btn').data('text'),
            placement: 'bottom'
        });
    
        $('#post_job_btn .plus-icon').tooltip({
            template: '<div class="tooltip tooltip-fancy tooltip-post-a-task" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
            title: $('#post_job_btn').data('text'),
            placement: 'bottom'
        });
        
        // OD - FT Toggle Mode
        /*$('#toggle_mode').on('change', function () {
            if (this.checked) {
                $('body').removeClass('od-mode');
                $('body').addClass('ft-mode');
            } else {
                $('body').removeClass('ft-mode');
                $('body').addClass('od-mode');
            }
        });*/
    
        $(document).ready(function () {
            /*$("input[name*='odft_mode']").on("change", function () {
                odftModePreview(this);
                var mode = $(this).prop('checked') ? 'ft-mode' : 'od-mode';
                localStorage.setItem('mode', mode);
            })*/
    
            /*$("#odft_mode").trigger('change');
            if(mode === 'ft-mode'){
                $(".as-post-a-job").delay(10, function(){
                    $(this).show();
                });
            }*/
    
        });
        
       // Post A Task Modal - Subcategory Others option
        $("#od_subcategory").change(function() {
            if ($(this).val() === 'others') {
                $(this).parent().next().show();
            }
            else {
                $(this).parent().next().hide();
            }
        });
        
        // Saved Search Dropdown Width
    
        $(document).ready(function() {
            var targetWidth = 576;
            if ($(window).width() <= targetWidth) {
                var saveSearchWidth = $("#searchForm").innerWidth();
                $('.dropdown-menu-saved-search').css('width', saveSearchWidth);
            } else {
                $('.dropdown-menu-saved-search').css('width', '400');
            }
        });
        $(window).resize(function() {
            var targetWidth = 576;
            if ($(window).width() <= targetWidth) {
                var saveSearchWidth = $("#searchForm").width();
                $('.dropdown-menu-saved-search').css('width', saveSearchWidth);
            } else {
                $('.dropdown-menu-saved-search').css('width', '400');
            }
        });
        
        
        
        $(document).ready(function() {
            var targetWidthTask = 576;
            if ($(window).width() <= targetWidthTask) {
                var saveSearchWidthTask = $("#searchTaskForm").innerWidth();
                $('.dropdown-menu-saved-search').css('width', saveSearchWidthTask);
            } else {
                $('.dropdown-menu-saved-search').css('width', '400');
            }
        });
        $(window).resize(function() {
            var targetWidthTask = 576;
            if ($(window).width() <= targetWidthTask) {
                var saveSearchWidthTask = $("#searchTaskForm").width();
                $('.dropdown-menu-saved-search').css('width', saveSearchWidthTask);
            } else {
                $('.dropdown-menu-saved-search').css('width', '400');
            }
        });
        
        // Post A Task Modal - Rejection Rate By Hour Select List
        $("#byhour_rejectionRateNumerator").change(function() {
          if ($(this).data('options') === undefined) {
            /*Taking an array of all options-2 and kind of embedding it on the select1*/
            $(this).data('options', $('#byhour_rejectionRateDenominator option').clone());
          }
          var id = 100 - $(this).val();
          var options = $(this).data('options').filter('[value=' + id + ']');
          $('#byhour_rejectionRateDenominator').html(options);
        });
        
        // Post A Task Modal - Rejection Rate Lumpsum Select List
        $("#lumpsum_rejectionRateNumerator").change(function() {
          if ($(this).data('options') === undefined) {
            /*Taking an array of all options-2 and kind of embedding it on the select1*/
            $(this).data('options', $('#lumpsum_rejectionRateDenominator option').clone());
          }
          var id = 100 - $(this).val();
          var options = $(this).data('options').filter('[value=' + id + ']');
          $('#lumpsum_rejectionRateDenominator').html(options);
        });
        // Search Filter Side Panel - Custom Range Option Select Function
        $("#search_filter_dateposted").change(function() {
            var val = $(this).val();
            if(val === "customrange") {
                $(this).parent().next().show();
            }
            else {
                $(this).parent().next().hide();
            }
        });
        
        $('#tnc_snp_check').on('change', function () {
            if (this.checked) {
                console.log("HELLO");
                $('.payment-row-actions .btn-pay').prop('disabled', false);
            } else {
                $('.payment-row-actions .btn-pay').prop('disabled', true);
            }
        });
    
        
        $("#search_filter_startdate, #search_filter_dateposted").change(function() {
            var val = $(this).val();
            if(val === "customrange") {
                $(this).parent().next().show();
            }
            else {
                $(this).parent().next().hide();
            }
        });
        
        // Settings (Preference - Remote) - Anywhere,Same Country,Near Me select list show hide
        $("select#select_loc_remote").change(function () {
            var val = $(this).val();
            if (val === "remote_anywhere") {
                $(this).closest('.row-collapse-preference').find('.row-indlocremotestate').hide();
                $(this).closest('.row-collapse-preference').find('.row-indlocremotenearest').hide();
            } 
            else if (val === "remote_samecountry") {
                $(this).closest('.row-collapse-preference').find('.row-indlocremotestate').show();
                $(this).closest('.row-collapse-preference').find('.row-indlocremotenearest').hide();
            } 
            else if (val === "remote_nearest") {
                $(this).closest('.row-collapse-preference').find('.row-indlocremotenearest').show();
                $(this).closest('.row-collapse-preference').find('.row-indlocremotestate').hide();
            }
        });
        
        
        // Settings (Preference - Physical) - Anywhere,Same Country,Near Me select list show hide
        $("select#select_loc_physical").change(function () {
            var val = $(this).val();
            if (val === "physical_anywhere") {
                $(this).closest('.row-collapse-preference').find('.row-indlocphysicalstate').hide();
                $(this).closest('.row-collapse-preference').find('.row-indlocphysicalnearest').hide();
            } 
            else if (val === "physical_samecountry") {
                $(this).closest('.row-collapse-preference').find('.row-indlocphysicalstate').show();
                $(this).closest('.row-collapse-preference').find('.row-indlocphysicalnearest').hide();
            } 
            else if (val === "physical_nearest") {
                $(this).closest('.row-collapse-preference').find('.row-indlocphysicalnearest').show();
                $(this).closest('.row-collapse-preference').find('.row-indlocphysicalstate').hide();
            }
        });
    
        
        // Settings Work Days
        $('#workdaysAllWeek').change(function(){
            if(this.checked){
                $(this).closest('.form-block').find('#workdaysWeekdays').prop('checked', false);
                $(this).closest('.form-block').find('#workdaysWeekdays').prop('disabled', true);
                
                $(this).closest('.form-block').find('#fullDay').prop('checked', false);
                $(this).closest('.form-block').find('#fullDay').prop('disabled', true);
                
                $(this).closest('.form-block').find('#afterWorkHour').prop('checked', false);
                $(this).closest('.form-block').find('#afterWorkHour').prop('disabled', true);
                
                $(this).closest('.form-block').find('#workdaysWeekend').prop('checked', false);
                $(this).closest('.form-block').find('#workdaysWeekend').prop('disabled', true);
                
                $(this).closest('.form-block').find('#amwork').prop('checked', false);
                $(this).closest('.form-block').find('#amwork').prop('disabled', true);
                
                $(this).closest('.form-block').find('#pmwork').prop('checked', false);
                $(this).closest('.form-block').find('#pmwork').prop('disabled', true);
            }
        });
        $('#workdayCustom').change(function(){
            if(this.checked){
                $(this).closest('.row-indweekday').find('#workdaysWeekdays').prop('disabled', false);
                $(this).closest('.row-indweekday').find('#fullDay').prop('disabled', false);
                $(this).closest('.row-indweekday').find('#afterWorkHour').prop('disabled', false);
                
                $(this).closest('.row-indweekday').find('#workdaysWeekend').prop('disabled', false);
                $(this).closest('.row-indweekday').find('#amwork').prop('disabled', false);
                $(this).closest('.row-indweekday').find('#pmwork').prop('disabled', false);
            } 
        });
        
        $(function() {
          $(".child-weekdays").on("click",function() {
              $parent = $(this).closest('.row-indweekday').find(".parent-weekdays");
              if ($(this).is(":checked")) $parent.prop("checked",true);
              else {
                 var len = $(this).closest('.row-indweekday').find(".child-weekdays:checked").length;
                 $parent.prop("checked",len>0);
              }    
          });
          $(".parent-weekdays").on("click",function() {
              $(this).closest('.row-indweekday').find(".child-weekdays").prop("checked",this.checked);
          });
            
          $(".child-weekends").on("click",function() {
              $parent = $(this).closest('.row-indweekday').find(".parent-weekends");
              if ($(this).is(":checked")) $parent.prop("checked",true);
              else {
                 var len = $(this).closest('.row-indweekday').find(".child-weekends:checked").length;
                 $parent.prop("checked",len>0);
              }    
          });
          $(".parent-weekends").on("click",function() {
              $(this).closest('.row-indweekday').find(".child-weekends").prop("checked",this.checked);
          });
        });
    
    
    
    
        // Dashboard Search Dropdown Filter Keep Open On Click Inside
        $('.dropdown-search-filter').on('click', function(event) {
            event.stopPropagation();
        });
        $(document).on('click', '.col-notification .dropdown-menu', function (e) {
            e.stopPropagation();
          });
        // Dashboard Search Dropdown Filter Keep Open On Click Inside
        $('.dropdown-menu-saved-search .tag-remove').on('click', function(event){
            // event.stopPropagation();
        });
        $('.dropdown-menu-saved-search > .modal-advance-filter-container-left > ul > li > a').on('click', function(event){
            event.stopPropagation();
            $(this).tab('show')
        });
        
        
        
        // Search Filter Side Panel - Collapse Function
        $(".btn-link-filter").click(function(e) {
            $(this).toggleClass("is-clicked");
            $(this).next().collapse("toggle");
        });
        // FAQ Collapse Accordion
        $('.btn-collapser').click(function() {
            $(this).parent().toggleClass('is-expanded');
            $(this).parent().next().collapse('toggle');
        });
    
        // Search Filter Side Panel - Posted Date & Start Date
        $("select#selectDatePostedStart").change(function () {
            var val = $(this).val();
            if (val === "postedDate") {
                $(this).closest('.form-block-task-filter').find('.form-filter-dateposted').show();
                $(this).closest('.form-block-task-filter').find('.form-filter-startdate').hide();
            }
            else if (val === "startDate" || val === "closeDate") {
                $(this).closest('.form-block-task-filter').find('.form-filter-startdate').show();
                $(this).closest('.form-block-task-filter').find('.form-filter-dateposted').hide();
            }else{
                $(this).closest('.form-block-task-filter').find('.form-filter-startdate').hide().find('select').val('');
                $(this).closest('.form-block-task-filter').find('.form-filter-dateposted').hide().find('select').val('');
            }
        });
         /*$("select#selectDatePostedStart").change(function () {
            var val = $(this).val();
            if (val === "postedDate") {
                $(this).closest('.form-block-task-filter').find('.form-filter-dateposted input, .form-filter-dateposted select').val('');
                $(this).closest('.form-block-task-filter').find('.form-filter-dateposted').show();
                $(this).closest('.form-block-task-filter').find('.form-filter-startdate').hide();
            } 
            else if (val === "startDate" || val === "closeDate") {
                $(this).closest('.form-block-task-filter').find('.form-filter-startdate input, .form-filter-startdate select').val('');
                $(this).closest('.form-block-task-filter').find('.form-filter-startdate').show();
                $(this).closest('.form-block-task-filter').find('.form-filter-dateposted').hide();
            } 
        });*/
        
        // Search Filter Side Panel - Location
        $(".form-filter-select-by-location select").change(function () {
            var val = $(this).val();
            if (val === "locbystate") {
                $(this).parent().parent().find('.form-filter-state').show();
                $(this).parent().parent().find('.form-filter-city').show();
                $(this).parent().parent().find('.form-filter-within').hide();
                $(this).parent().parent().find('.form-filter-within').find('select').data('inputStatus', false);
            }else if (val === "locbynearest") {
                $(this).parent().parent().find('.form-filter-within').show();
                $(this).parent().parent().find('.form-filter-state').hide();
                $(this).parent().parent().find('.form-filter-city').hide();
                $(this).parent().parent().find('.form-filter-within').find('select').data('inputStatus', true);
                $(this).parent().parent().find('.form-filter-state').find('select').data('inputStatus', false);
                $(this).parent().parent().find('.form-filter-city').find('select').data('inputStatus', false);
            }else{
                $(this).parent().parent().find('.form-filter-within').hide();
                $(this).parent().parent().find('.form-filter-state').hide();
                $(this).parent().parent().find('.form-filter-city').hide();
                $(this).parent().parent().find('.form-filter-within').find('select').data('inputStatus', false);
                $(this).parent().parent().find('.form-filter-state').find('select').data('inputStatus', false);
                $(this).parent().parent().find('.form-filter-city').find('select').data('inputStatus', false);
            }
        });
        
        
        
        // Feedback Popup Others
        $("#select_feedback_area").change(function () {
            var val = $(this).val();
            if (val === "feedback_others") {
                $(this).next().prop('disabled', false).show();
            } 
            else {
                $(this).next().prop('disabled', true).hide();
            }
        });
        
        
        // Complete Profile Modal - Collapse Function
        $(".btn-complete-profile").click(function(e) {
            $(this).toggleClass("is-clicked");
            $(this).next().collapse("toggle");
        });
        
        // Load More Button Animation
        $(".btn-load-more").click(function(e) {
           $(".load-plus-icon").hide();
           $(".load-load-icon").show();
           $(".load-load-icon").addClass("load-more");
           $("#loadMoreLbl").html($(e.currentTarget).data('text'));
    
           setTimeout(function() {
              $("#loadMoreLbl").html( $("#loadMoreLbl").data('text') );
              $(".load-plus-icon").show();
              $(".load-load-icon").hide();
              $(".load-load-icon").removeClass("load-more");
           }, 1000);
    
        });
        
        
        $(document).ready(function () {
            
            $('.nav-link-next').click(function(e){
              if($('.nav-pills > .nav-item > .active').parent().next('li').hasClass('nav-link-next')){
                $('.nav-pills > li').first('li').find('a').trigger('click');
              }else{
                $('.nav-pills > .nav-item > .active').parent().next('li').find('a').trigger('click');
              }
              e.preventDefault();
            });
            
            $('.nav-link-prev').click(function(e){
              if($('.nav-pills > .nav-item > .active').parent().prev('li').hasClass('nav-link-prev')){
                $('.nav-pills > li').first('li').find('a').trigger('click');
              }else{
                $('.nav-pills > .nav-item > .active').parent().prev('li').find('a').trigger('click');
              }
              e.preventDefault();
            });
            
        });
        
        $(document).ready(function () {
            var taskFilterContainer = $('.col-task-filter-rows'),
                btnTaskFilterContainer = $('.col-task-filter-more-filter'),
                talentFilterContainer = $('.col-talent-filter-rows'),
                btnTalentFilterContainer = $('.col-talent-filter-more-filter');
            if (window.matchMedia('(max-width: 1199.98px)').matches) {
                taskFilterContainer.addClass('collapse');
                talentFilterContainer.addClass('collapse');
                
                btnTaskFilterContainer.click(function(e) {
                    $(this).toggleClass("is-clicked");
                    $(this).next().collapse("toggle");
                });
                btnTalentFilterContainer.click(function(e) {
                    $(this).toggleClass("is-clicked");
                    $(this).next().collapse("toggle");
                });
            }
            else {
                btnTaskFilterContainer.removeClass('is-clicked');
                taskFilterContainer.removeClass('collapse');
                taskFilterContainer.removeClass('show');
                
                btnTalentFilterContainer.removeClass('is-clicked');
                talentFilterContainer.removeClass('collapse');
                talentFilterContainer.removeClass('show');
            }
        });
        
        $(window).on('resize', function(){
            var taskFilterContainer = $('.col-task-filter-rows'),
                btnTaskFilterContainer = $('.col-task-filter-more-filter'),
                talentFilterContainer = $('.col-talent-filter-rows'),
                btnTalentFilterContainer = $('.col-talent-filter-more-filter');
            if (window.matchMedia('(max-width: 1199.98px)').matches) {
                taskFilterContainer.addClass('collapse');
                talentFilterContainer.addClass('collapse');
                
                btnTaskFilterContainer.click(function(e) {
                    $(this).toggleClass("is-clicked");
                    $(this).next().collapse("toggle");
                });
                btnTalentFilterContainer.click(function(e) {
                    $(this).toggleClass("is-clicked");
                    $(this).next().collapse("toggle");
                });
            }
            else {
                btnTaskFilterContainer.removeClass('is-clicked');
                taskFilterContainer.removeClass('collapse');
                taskFilterContainer.removeClass('show');
    
                btnTalentFilterContainer.removeClass('is-clicked');
                talentFilterContainer.removeClass('collapse');
                talentFilterContainer.removeClass('show');
            }
        });
        
        // Tooltip Initialize
        $('[data-toggle="tooltip"]').tooltip();
    
        function odftModePreview(ele) {
            if ($(ele).prop("checked") == true) {
                $('body').addClass('ft-mode').removeClass('od-mode');
                //$('.task-centre, .analytics').hide();
                $(".as-post-a-job").show();
    
                $("#searchForm input[name='type']").val('job');
                $("#searchTaskTab-tab").text('Job');
                $('.individual #searchTaskTab #filtertj_1').prop('checked', false).prop('disabled', true);
                $('.individual #searchTaskTab #filtertj_2').prop('checked', true).trigger('change');
                //$('.job-centre').show();
                $('input[name*="'+ele.name+'"]').each(function(idx, elem){ $(elem).prop('checked', true) })
                $('.tasks-filter .task-filter-row-nature, .tasks-filter .task-filter-row-budget').addClass('hide').find('input, select').prop('disabled', true);
                $('.tasks-filter [name="date_type"]').children().last().text('Closing Date').val('closeDate');
                $('.tasks-filter .categories-input').html( $('.tasks-filter #job-categories').html() );
                if( /\/workspace\/[0-9]+\/[0-9]+/i.test(document.location.pathname) ){
                    var extra = document.location.pathname.slice('1').split('/').indexOf('staging') > -1 ? '/staging':'';
                    document.location.pathname = extra + '/workspace/job-centre';
                }
            } else if ($(ele).prop("checked") == false) {
                $('body').addClass('od-mode').removeClass('ft-mode');
                //$('.task-centre, .analytics').show();
    
                $("#searchForm input[name='type']").val('task');
                $("#searchTaskTab-tab").text('Task');
    
                $('.individual #searchTaskTab #filtertj_2').prop('checked', false).prop('disabled', true);
                $('.individual #searchTaskTab #filtertj_1').prop('checked', true).trigger('change');
                //$('.job-centre').hide();
                $('input[name*="'+ele.name+'"]').each(function(idx, elem){ $(elem).prop('checked', false) })
                $('.tasks-filter .task-filter-row-nature, .tasks-filter .task-filter-row-budget').removeClass('hide').find('input, select').prop('disabled', false);
                $('.tasks-filter [name="date_type"]').children().last().text('Start Date').val('startDate');
                $('.tasks-filter .categories-input').html( $('.tasks-filter #task-categories').html() );
                if(document.location.pathname.indexOf('job-centre') > -1){
                    var date = new Date();
                    var extra = document.location.pathname.slice('1').split('/').indexOf('staging') > -1 ? '/staging':'';
                    document.location.pathname = extra + '/workspace/'+date.getFullYear()+'/'+(date.getMonth()+1);
                }
            }
    
            if(document.location.pathname.indexOf('search') > -1){
                $('.tasks-filter .categories-input').find('input').on('change', function(e){
                    $('#searchForm input[name="page"]').val("1");
                    $('#searchForm input[name="perform-search"]').val('yes');
                    update_task_filters($(e.currentTarget));
                });
                get_result();
            }
        }
        
        // Header Modal suggestion engine
        var movies = new Bloodhound({
            datumTokenizer: function (datum) {
                return Bloodhound.tokenizers.whitespace(datum.value);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: 'http://api.themoviedb.org/3/search/movie?query=%QUERY&api_key=f22e6ce68f5e5002e71c20bcba477e7d',
                filter: function (movies) {
                    console.log(movies);
                    // Map the remote source JSON array to a JavaScript object array
                    return $.map(movies.results, function (movie) {
                        return {
                            value: movie.original_title,
                            release_date: movie.release_date
                        };
                    });
                }
            },
            limit: 10
        });
    
        // Initialize the Bloodhound suggestion engine
        movies.initialize();
    
        // Instantiate the Typeahead UI
        $('input.search-input').typeahead(null, {
            displayKey: 'value',
            source: movies.ttAdapter(),
            templates: {
                suggestion: Handlebars.compile("<p><span class='tt-suggestion-lbl'>Release date {{release_date}} </span><span class='tt-suggestion-val'>{{value}}</span>  </p>"),
                footer: Handlebars.compile("<span class='tt-suggestion-footer'>Searched for '{{query}}'</span>")
            }
        }).on('typeahead:select', function (obj, datum) {
            $('.typeahead-close').show();
        });
    
    
    
    
        // Post A Task Modal -  Tags Input Suggestion
        $(document).ready(function () {
            var data =
              '[{ "value": 1, "text": "Task 1", "continent": "Task" }, { "value": 2, "text": "Task 2", "continent": "Task" }, { "value": 3, "text": "Task 3", "continent": "Task" }, { "value": 4, "text": "Task 4", "continent": "Task" }, { "value": 5, "text": "Task 5", "continent": "Task" }, { "value": 6, "text": "Task 6", "continent": "Task" } ]';
            var extra = document.location.pathname.slice('1').split('/').indexOf('staging') > -1 ? '/staging':'';
            if(extra === '') extra = document.location.pathname.slice('1').split('/').indexOf('translation') > -1 ? '/translation':'';
            //get data pass to json
            var skills = new Bloodhound({
              datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"),
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              prefetch: {
                  url: window.location.origin + extra + '/ajax/skills.json?all=1',
                  filter: function(list){
                      return $.map(list, function(skill){
                          return skill
                      })
                  }
              }
              //local: jQuery.parseJSON(data) //your can use json type
            });
            skills.clearPrefetchCache();
            skills.initialize();
    
            var elt = $("#tag1");
            var elt2 = $("#tag2");
            elt.tagsinput({
                itemValue: function(item){ return item.id !== undefined ? item.id : 'new:'+item },
                itemText: function(item){ return item.name || item },
                confirmKeys: [9, 13, 188],
                typeaheadjs: {
                    name: "skills",
                    displayKey: "name",
                    hint: true,
                    source: skills.ttAdapter(),
                    templates: {
                        //footer: Handlebars.compile("<span class='tt-suggestion-footer tt-selectable'>Add new '{{query}}'</span>"),
                        empty: Handlebars.compile("<div class='tt-empty'>No skills found</div><span class='tt-suggestion-footer tt-selectable add-new-tag'>Add new '{{query}}'</span>")
                    }
                },
                freeInput: true
            });
    
            elt2.tagsinput({
                itemValue: function(item){ return item.id !== undefined ? item.id : 'new:'+item },
                itemText: function(item){ return item.name || item },
                confirmKeys: [9, 13, 188],
                typeaheadjs: {
                    name: "skills",
                    displayKey: "name",
                    hint: true,
                    source: skills.ttAdapter(),
                    templates: {
                        //footer: Handlebars.compile("<span class='tt-suggestion-footer tt-selectable'>Add new '{{query}}'</span>"),
                        empty: Handlebars.compile("<div class='tt-empty'>No skills found</div><span class='tt-suggestion-footer tt-selectable add-new-tag'>Add new '{{query}}'</span>")
                    }
                },
                freeInput: true
            });
    
            $('.bs-example').bind('typeahead:render', function(e) {
                var input = e.currentTarget;
                var target = $(input).find('input[name="skills"]');
                if( target.length === 0 )
                    target = $('input.skills');
    
                var tag_btn = $('.add-new-tag');
                if( tag_btn.length ){
                    tag_btn.off('click');
                    tag_btn.on('click', function(e){
                        const tag_text = $(e.currentTarget).text();
                        const regex = /\'([\w\d\s\&\.\|]+)\'$/mg;
                        let m;
    
                        m = regex.exec(tag_text);
                        if( m !== null && m[1] !== undefined && m[1].length >= 2 ) {
                            var tag = m[1].trim();
                            target.tagsinput('add', {id:'new:'+tag, name: tag});
                        }
                    })
                }
            });
    
            elt.on('beforeItemAdd', function(event) {
                if( typeof(event.item) === 'string' ){
                    event.cancel = true;
                    var target = $(this);
                    target.tagsinput('add', {id:'new:'+event.item, name: event.item});
                    //target.tagsinput('refresh');
                }
            });
    
            elt2.on('beforeItemAdd', function(event) {
                if( typeof(event.item) === 'string' ){
                    event.cancel = true;
                    var target = $(this);
                    target.tagsinput('add', {id:'new:'+event.item, name: event.item});
                    //target.tagsinput('refresh');
                }
            });
        });
    
    
    
    
    
        // Post Task Modal - Range Budget Hour
        var jobModalHour = document.getElementById("range_budget_hour");
            if(jobModalHour) {
            var range_budget_hour = new Slider(jobModalHour, {
                min: 50,
                max: 100,
                value: 60,
                range: false,
                formatter: function (value) {
                    return 'RM' + value;
                }
            });
        }
    
    
    
        // Post Task Modal - Range Budget Lumpsum
        var jobModalLumpsum = document.getElementById("range_budget_lumpsum");
        if(jobModalLumpsum) {
            var range_budget_lumpsum = new Slider(jobModalLumpsum, {
                min: 200,
                max: 300,
                value: 250,
                range: false,
                formatter: function (value) {
                    return 'RM' + value;
                }
            });
        }
    
        // Post Job Modal - Range Budget Salary
        var jobModalSalary = document.getElementById("range_budget_salary");
        if(jobModalSalary) {
            window.range_budget_salary = new Slider("#range_budget_salary", {
                min: 1200,
                max: 6000,
                step: 50,
                value: [3500, 4200],
                range: true,
                formatter: function (value) {
                    return 'RM' + value[0] + ' - RM' + value[1];
                }
            });
        }
    
    
    
    
    
    
    
        // Post Task & Job Modal - Add More Field Question
        $(document).ready(function () {
            // Post Task Modal - Add More Field Question
            var od_maxField = 5; //Input fields increment limitation
            var od_addButton = $('.od-add-button'); //Add button selector
            var od_wrapper = $('.od-qna-wrapper'); //Input field wrapper
            var od_count = 1; //Initial field counter is 1
    
            //Once add button is clicked
            $(od_addButton).click(function () {
                //Check maximum number of input fields
                od_count = $('input[name*="od_qna[]"]').length;
                if (od_count < od_maxField) {
                    if(od_count > 1){
                        $('.od-qna-form-dynamic:last-child').find('.od-remove-field').hide();
                    }
                    od_count++; //Increment field counter
                    var removeText = $('.qna-wrapper').data('removeText');
                    var questionText = $('.qna-wrapper').data('questionText');
                    var question = $('<div class="qna-form-dynamic od-qna-form-dynamic"><div class="form-flex"><span class="qna-lbl od-qna-lbl">' + od_count + '.</span><input type="text" name="od_qna[]" class="form-control form-control-input"  placeholder="'+questionText+'"/></div><a href="javascript:void(0);" class="remove-field od-remove-field">'+removeText+'</a></div>');
                    $(od_wrapper).append(question); //Add field html
                    question.keydown(function(e){
                        if(e.keyCode === 13){
                            e.preventDefault();
                            $('.od-add-button').trigger('click');
                        }
                    });
                    $('.od-qna-form-dynamic:last-child').find('input[name*="od_qna"]').focus();
                }
    
                if (od_count == 5) {
                    $(od_addButton).hide();
                }
            });
    
            $('input[name*="od_qna"]').keydown(function(e){
                if(e.keyCode === 13){
                    e.preventDefault();
                    $('.od-add-button').trigger('click');
                }
            });
    
            //Once remove button is clicked
            $(od_wrapper).on('click', '.od-remove-field', function (e) {
                e.preventDefault();
                $(this).parent('.od-qna-form-dynamic').remove(); //Remove field html
                if(od_count > 1){
                    $('.od-qna-form-dynamic:last-child').find('.od-remove-field').show();
                }
                od_count--; //Decrement field counter
    
                if (od_count == 4) {
                    $(od_addButton).show();
                }
            });
    
            // Post Job Modal - Add More Field Question
            var ft_maxField = 5; //Input fields increment limitation
            var ft_addButton = $('.ft-add-button'); //Add button selector
            var ft_wrapper = $('.ft-qna-wrapper'); //Input field wrapper
            var ft_count = 1; //Initial field counter is 1
    
            //Once add button is clicked
            $(ft_addButton).click(function () {
                //Check maximum number of input fields
                ft_count = $('input[name*="ft_qna[]"]').length;
                if (ft_count < ft_maxField) {
                    if(ft_count > 1){
                        $('.ft-qna-form-dynamic:last-child').find('.ft-remove-field').hide();
                    }
                    ft_count++; //Increment field counter
                    var removeText = $('.qna-wrapper').data('removeText');
                    var questionText = $('.qna-wrapper').data('questionText');
                    var question = $('<div class="qna-form-dynamic ft-qna-form-dynamic"><div class="form-flex"><span class="qna-lbl ft-qna-lbl">' + ft_count + '.</span><input type="text" name="ft_qna[]" class="form-control form-control-input"  placeholder="'+questionText+'"/></div><a href="javascript:void(0);" class="remove-field ft-remove-field">'+removeText+'</a></div>');
                    $(ft_wrapper).append(question); //Add field html
                    question.keydown(function(e){
                        if(e.keyCode === 13){
                            e.preventDefault();
                            $('.ft-add-button').trigger('click');
                        }
                    });
                    $('.ft-qna-form-dynamic:last-child').find('input[name*="ft_qna"]').focus();
                }
    
                if (ft_count == 5) {
                    $(ft_addButton).hide();
                }
            });
    
            $('input[name*="ft_qna"]').keydown(function(e){
                if(e.keyCode === 13){
                    e.preventDefault();
                    $('.ft-add-button').trigger('click');
                }
            });
    
            //Once remove button is clicked
            $(ft_wrapper).on('click', '.ft-remove-field', function (e) {
                e.preventDefault();
                $(this).parent('.ft-qna-form-dynamic').remove(); //Remove field html
                if(ft_count > 1){
                    $('.ft-qna-form-dynamic:last-child').find('.ft-remove-field').show();
                }
                ft_count--; //Decrement field counter
    
                if (ft_count == 4) {
                    $(ft_addButton).show();
                }
            });
    
            // Post Task Modal - Task Location
            $('#tl_travel').change(function () {
                if (this.checked) {
                    $('#mapTaskLoc').fadeIn('fast');
                    $('#form_travelrequired').show();
                    $('.mapSearchBox').show();
                    $('#sf3 .modal-pat-keyart').hide();
                }
            });
            $('#tl_remote').change(function () {
                if (this.checked) {
                    $('#mapTaskLoc').hide();
                    $('#sf3 .modal-pat-keyart').show();
                    $('#form_travelrequired').hide();
                    $('.mapSearchBox').hide();
                }
            });
    
            // Post Task Modal - By Hour Complete By
            $('.radio-complete-day input').change(function () {
                if (this.checked) {
                    //console.log("DAYYYYYYYYYYY");
                    var parentDay = $('.radio-complete-day');
                    var parentDate = $('.radio-complete-date');
                    parentDay.find('input[type="number"]').attr("readonly", false);
                    parentDate.find('.datetimepicker-input').attr("readonly", true);
                    /*$('.radio-complete-day input[type="number]').attr("readonly", false);
                    $('.radio-complete-date .input-group-datetimepicker .datetimepicker-input').attr("readonly", true);*/
                }
            });
            $('.radio-complete-date input').change(function () {
                if (this.checked) {
                    //console.log("DATEEEEEEEEE");
                    var parentDay = $('.radio-complete-day');
                    var parentDate = $('.radio-complete-date');
                    parentDate.find('.datetimepicker-input').attr("readonly", false);
                    parentDay.find('input[type="number"]').attr("readonly", true);
                    /*$('.radio-complete-date input[type="number]').attr("readonly", false);
                    $('.radio-complete-day .input-group-datetimepicker input').attr("readonly", true);*/
                }
            });
    
    
            // Post Job Modal - Checkbox by hour & lumpsum
            $('#not_bythehour').change(function () {
                if (this.checked) {
                    $('#form_bylumpsum').hide();
                    $('#form_byhour').fadeIn('slow');
                    $('#form_datetime_hour').show();
                }
    
            });
            $('#not_lumpsum').change(function () {
                if (this.checked) {
                    $('#form_byhour').hide();
                    $('#form_bylumpsum').fadeIn('slow');
                    $('#form_datetime_lumpsum').show();
                }
    
            });
    
            // Post Task Modal - Bootstrap Tags Input
            $('input#tag1').on('change', function (event) {
    
                var $element = $(event.target);
                var $container = $element.closest('.example');
    
                if (!$element.data('tagsinput'))
                    return;
    
                var val = $element.val();
                if (val === null)
                    val = "null";
                var items = $element.tagsinput('items');
                /*console.log(items[items.length - 1]);*/
    
                $('code', $('pre.val', $container)).html(($.isArray(val) ? JSON.stringify(val) : "\"" + val.replace('"', '\\"') + "\""));
                $('code', $('pre.items', $container)).html(JSON.stringify($element.tagsinput('items')));
    
                /*console.log(val);
                console.log(items);
                console.log(JSON.stringify(val));
                console.log(JSON.stringify(items));
    
                console.log(items[items.length - 1]);*/
    
            }).trigger('change');
    
    
    
    
    
    
        });
    
        $('#form_datetime_hour, #form_datetime_hour_completeby, #form_datetime_lumpsum, #form_datetime_lumpsum_completeby').datetimepicker('destroy');
        // Post Task Modal - Calendar Initialize
        $('#form_datetime_hour').datetimepicker({
            useCurrent: false,
            format: 'DD-MMM-YYYY',
            widgetPositioning: {
                vertical: 'top',
            },
            minDate: moment().add(1, 'days').startOf('day')
        });
        $('#form_datetime_hour_completeby').datetimepicker({
            useCurrent: false,
            format: 'DD-MMM-YYYY',
            widgetPositioning: {
                vertical: 'top',
            },
            minDate: moment().add(1, 'days').startOf('day')
        });
    
        $('#form_datetime_hour').on("change.datetimepicker", function (e) {
            if( $(this).parent().find('input.od_start_by.hide').length ){
                $(this).parent().find('input.od_start_by.hide').remove();
            }
    
            var tempInput = document.createElement("input");
            tempInput.setAttribute('type', 'text');
            tempInput.setAttribute('name', 'od_start_by[by-hour]');
            tempInput.setAttribute('value', moment(e.date).format('DD-MM-YYYY'));
            tempInput.setAttribute('class', 'od_start_by hide')
            $(this).parent().append(tempInput);
    
            if(e.date !== undefined) {
                if(moment($(this).find('input').val()).isAfter(moment($('#form_datetime_hour_completeby').find('input').val()))) {
                    $('#form_datetime_hour_completeby').datetimepicker('clear');
                    $('#form_datetime_hour_completeby').datetimepicker("defaultDate", e.date);
                }
                $('#form_datetime_hour_completeby').datetimepicker("minDate", e.date);
                if (moment(e.date).isBefore(moment().add(5, 'days'))) {
                    $(this).parent().parent().parent().next().removeClass('hide');
                } else {
                    $(this).parent().parent().parent().next().addClass('hide');
                }
            } else {
                $(this).parent().parent().parent().next().addClass('hide');
            }
        });
    
        $('#form_datetime_hour_completeby').on("change.datetimepicker", function (e) {
            if( $(this).parent().find('input.od_complete_by.hide').length ){
                $(this).parent().find('input.od_complete_by.hide').remove();
            }
    
            var tempInput = document.createElement("input");
            tempInput.setAttribute('type', 'text');
            tempInput.setAttribute('name', 'od_complete_by[by-hour]');
            tempInput.setAttribute('value', moment(e.date).format('DD-MM-YYYY'));
            tempInput.setAttribute('class', 'od_complete_by hide')
            $(this).parent().append(tempInput);
    
        });
    
        $('#form_datetime_lumpsum').datetimepicker({
            useCurrent: false,
            format: 'DD-MMM-YYYY',
            minDate: moment().add(1, 'days').startOf('day')
        });
        $('#form_datetime_lumpsum_completeby').datetimepicker({
            useCurrent: false,
            format: 'DD-MMM-YYYY',
            minDate: moment().add(1, 'days').startOf('day')
        });
    
        $('#form_datetime_lumpsum').on("change.datetimepicker", function (e) {
            if( $(this).parent().find('input.od_start_by.hide').length ){
                $(this).parent().find('input.od_start_by.hide').remove();
            }
    
            var tempInput = document.createElement("input");
            tempInput.setAttribute('type', 'text');
            tempInput.setAttribute('name', 'od_start_by[lump-sum]');
            tempInput.setAttribute('value', moment(e.date).format('DD-MM-YYYY'));
            tempInput.setAttribute('class', 'od_start_by hide')
            $(this).parent().append(tempInput);
            if(e.date !== undefined) {
                if(moment($(this).find('input').val()).isAfter(moment($('#form_datetime_lumpsum_completeby').find('input').val()))) {
                    $('#form_datetime_lumpsum_completeby').datetimepicker('clear');
                    $('#form_datetime_lumpsum_completeby').datetimepicker("defaultDate", e.date);
                }
                $('#form_datetime_lumpsum_completeby').datetimepicker("minDate", e.date);
                if (moment(e.date).isBefore(moment().add(5, 'days'))) {
                    $(this).parent().parent().parent().next().removeClass('hide');
                } else {
                    $(this).parent().parent().parent().next().addClass('hide');
                }
            } else {
                $(this).parent().parent().parent().next().addClass('hide');
            }
        });
    
    
    
        $('#form_datetime_lumpsum_completeby').on("change.datetimepicker", function (e) {
            if( $(this).parent().find('input.od_complete_by.hide').length ){
                $(this).parent().find('input.od_complete_by.hide').remove();
            }
    
            var tempInput = document.createElement("input");
            tempInput.setAttribute('type', 'text');
            tempInput.setAttribute('name', 'od_complete_by[lump-sum]');
            tempInput.setAttribute('value', moment(e.date).format('DD-MM-YYYY'));
            tempInput.setAttribute('class', 'od_complete_by hide')
            $(this).parent().append(tempInput);
    
        });
    
        $('#form_datetime_closing').datetimepicker({
            format: 'DD-MMM-YYYY',
            minDate: moment()
        });
    
    
    
    
        // Task Planning Modal - Calendar Initialize
        $('#form_taskplan_date').datetimepicker({
            format: 'DD/MM/YYYY',
        });
        $('#form_taskplan_time_from').datetimepicker({
            format: 'LT',
        });
        $('#form_taskplan_time_to').datetimepicker({
            format: 'LT'
        });
    
        $('#form_taskplan_date2').datetimepicker({
            format: 'DD/MM/YYYY',
        });
        $('#form_taskplan_time_from2').datetimepicker({
            format: 'LT',
        });
        $('#form_taskplan_time_to2').datetimepicker({
            format: 'LT'
        });
    
    
        // Search Suggestion Modal - Calendar Initialize
        $('#form_datefilterfrom').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $('#form_datefilterto').datetimepicker({
            format: 'DD/MM/YYYY'
        });
    
    
    
    
        $(document).ready(function () {
    
    
            $("#btn_adv_search").click(function (e) {
                $(this).parent().closest('.modal-private-search-form').next().collapse('toggle');
                e.stopPropagation();
            });
            $(".modal-filter-action .btn-cancel").click(function (e) {
                $(this).parent().closest('.modal-advanced-filter-wrapper').collapse('toggle');
                e.stopPropagation();
            });
    
    
    
    
        });
    
    
    
        $("#selectionType").on("change", function () {
    
            if (this.value == "tasksjobs") {
                $("#inputBox").attr("placeholder", "What are your talents?");
                $(".public-search-taskjob-filter-container").addClass("active");
                $(".public-search-talent-filter-container").removeClass("active");
            } else if (this.value == "talents") {
                $("#inputBox").attr("placeholder", "Skills you need...");
                $(".public-search-talent-filter-container").addClass("active");
                $(".public-search-taskjob-filter-container").removeClass("active");
            }
    
        });
    
    
    
    
    });
    
    
    // Task Listing - Questionnaire Steps
    $(document).ready(function () {
        
        $(document).on('show.bs.modal', '.modal-apply-question', function(e){
            var form = $(e.currentTarget).find('#form_task_question');
            var formModalTaskQuestion = form.validate({
                rules: {
                    taskQuestion1: {
                        required: true
                    },
                    taskQuestion2: {
                        required: true
                    },
                    taskQuestion3: {
                        required: true
                    },
                    taskQuestion4: {
                        required: true
                    },
                    taskQuestion5: {
                        required: true
                    }
    
                },
                messages: {
                    taskQuestion1: {
                        required: "Please enter your answer"
                    },
                    taskQuestion2: {
                        required: "Please enter your answer"
                    },
                    taskQuestion3: {
                        required: "Please enter your answer"
                    },
                    taskQuestion4: {
                        required: "Please enter your answer"
                    },
                    taskQuestion5: {
                        required: "Please enter your answer"
                    }
                },
                errorElement: "span",
                errorClass: "help-inline-error",
            });
    
            $(".qa-open1").click(function () {
                if (formModalTaskQuestion.form()) {
                    $(this).parent().parent().hide();
                    $(this).parent().parent().next().show();
                }
            });
            $(".qa-back2").click(function () {
                if (formModalTaskQuestion.form()) {
                    $(this).parent().parent().hide();
                    $(this).parent().parent().prev().show();
                }
            });
            $(".qa-open2").click(function () {
                if (formModalTaskQuestion.form()) {
                    $(this).parent().parent().hide();
                    $(this).parent().parent().next().show();
                }
            });
            $(".qa-back3").click(function () {
                if (formModalTaskQuestion.form()) {
                    $(this).parent().parent().hide();
                    $(this).parent().parent().prev().show();
                }
            });
            $(".qa-open3").click(function () {
                if (formModalTaskQuestion.form()) {
                    $(this).parent().parent().hide();
                    $(this).parent().parent().next().show();
                }
            });
            $(".qa-back4").click(function () {
                if (formModalTaskQuestion.form()) {
                    $(this).parent().parent().hide();
                    $(this).parent().parent().prev().show();
                }
            });
            $(".qa-open4").click(function () {
                if (formModalTaskQuestion.form()) {
                    $(this).parent().parent().hide();
                    $(this).parent().parent().next().show();
                }
            });
            $(".qa-back5").click(function () {
                if (formModalTaskQuestion.form()) {
                    $(this).parent().parent().hide();
                    $(this).parent().parent().prev().show();
                }
            });
        });
    
        
        
        $("#btnPublicTask").click(function () {
            $("#public_selection_wrapper").hide();
            $("#public_od_wrapper").show();
        });
        $("#btnPublicJob").click(function () {
            $("#public_selection_wrapper").hide();
            $("#public_ft_wrapper").show();
        });
        $(".od-back1").click(function () {
            $("#public_od_wrapper").hide();
            $("#public_selection_wrapper").show();
        });
        $(".ft-back1").click(function () {
            $("#public_ft_wrapper").hide();
            $("#public_selection_wrapper").show();
        });
        
        
    });
    
    
    // Post Job Modal validation
    $(document).ready(function () {
        
        $("#btnPublicTask").click(function () {
            $("#public_selection_wrapper").hide();
            $("#public_od_wrapper").show();
        });
        $("#btnPublicJob").click(function () {
            $("#public_selection_wrapper").hide();
            $("#public_ft_wrapper").show();
        });
        $(".od-back1").click(function () {
            $("#public_od_wrapper").hide();
            $("#public_selection_wrapper").show();
        });
        $(".ft-back1").click(function () {
            $("#public_ft_wrapper").hide();
            $("#public_selection_wrapper").show();
        });
        
        
    });
    
    // Post Task Modal validation
    Dropzone.autoDiscover = false;
    $(document).ready(function () {
    
        // validate form on keyup and submit
        var v = jQuery("#form_post_task_od").validate({
            errorPlacement: function(error, element){
                if(element.attr("type") == "radio") {
                    error.appendTo( element.parent("div").parent("div") );
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                /*od_category: {
                    required: true
                },
                od_subcategory: {
                    required: true
                },
                od_tasktitle: {
                    required: true,
                    maxlength: 100,
                },
                od_taskdesc: {
                    required: true
                },
                od_location: {
                    required: true
                },
                od_type: {
                    required: true
                },
                "od_budget[by-hour]": {
                    required: true
                },
                "od_start_by[by-hour]": {
                    required: true
                },
                "od_complete_by[by-hour]": {
                    required: true
                },
                "od_budget[lump-sum]": {
                    required: true
                },
                "od_start_by[lump-sum]": {
                    required: true
                },
                "od_complete_by[lump-sum]": {
                    required: true
                }*/
    
            },
            /*messages: {
                od_category: {
                    required: "Please select category"
                },
                od_subcategory: {
                    required: "Please select sub category"
                },
                od_tasktitle: {
                    required: "Please enter task title",
                    maxlength: "Task title should be less than 100 characters"
                },
                od_taskdesc: {
                    required: "Please enter task description"
                },
                od_location: {
                    required: "Please select task location"
                },
                od_type: {
                    required: "Please select task nature"
                },
                "od_budget[by-hour]": {
                    required: "Please enter task budget"
                },
                "od_start_by[by-hour]": {
                    required: "Please enter start date"
                },
                "od_complete_by[by-hour]": {
                    required: "Please enter complete date"
                },
                "od_budget[lump-sum]": {
                    required: "Please enter task budget"
                },
                "od_start_by[lump-sum]": {
                    required: "Please enter start date"
                },
                "od_complete_by[lump-sum]": {
                    required: "Please enter complete date"
                }
    
            },*/
            errorElement: "span",
            errorClass: "help-inline-error small",
        });
    
        $("#form_post_task_od").on('show.bs.modal', function () {
            $("#sf1").show();
        });
    
        $(".od-open1").click(function () {
            if (v.form()) {
                $(".frm").hide();
                $("#sf2").show();
            }
        });
    
        $(".od-open2").click(function () {
            var desc = jQuery('#od_taskdesc').val();
            //var descErrorMsg = jQuery('#od_taskdesc').data('msg');
            descErrorMsg = $.validator.messages.required;
            if (v.form() /*&& desc.length*/) {
                $(".frm").hide();
                $("#sf3").show();
            }else if(!desc.length){
                jQuery('#od_taskdesc').next().after("<span for='od_taskdesc' generated='true' class='help-inline-error small'>"+descErrorMsg+"</span>")
            }
        });
    
        $(".od-open3").click(function () {
            if (v.form()) {
                $(".frm").hide();
                $("#sf4").show();
            }
        });
        $(".od-open4").click(function () {
            var descriptionDiv = document.createElement('div');
            var category = $('#od_category');
            var subcategory = $('#od_subcategory');
            var title = $('#od_tasktitle');
            var description = $('#od_taskdesc');
            var location = $('[name="od_location"]');
            var type = $('[name="od_type"]');
            var hourly_hours = $('[name="od_hours[by-hour]"]');
            var hourly_budget = $('[name="od_budget[by-hour]"]');
            var lumpsum_budget = $('[name="od_budget[lump-sum]"]');
            var hourly_start = $('[name="od_start_by[by-hour]"]');
            var hourly_end = $('[name="od_complete_by[by-hour]"]');
            var lumpsum_start = $('[name="od_start_by[lump-sum]"]');
            var lumpsum_end = $('[name="od_complete_by[lump-sum]"]');
            var skills;
    
            $('[name="skills"]').on('change', function(e){
                skills = $(this);
    
                descriptionDiv.innerHTML = description.val();
                title = $('#od_tasktitle');
                location = $('[name="od_location"]');
                type = $('[name="od_type"]');
                hourly_hours = $('[name="od_hours[by-hour]"]');
                hourly_budget = $('[name="od_budget[by-hour]"]');
                lumpsum_budget = $('[name="od_budget[lump-sum]"]');
                hourly_start = $('[name="od_start_by[by-hour]"]');
                hourly_end = $('[name="od_complete_by[by-hour]"]');
                lumpsum_start = $('[name="od_start_by[lump-sum]"]');
                lumpsum_end = $('[name="od_complete_by[lump-sum]"]');
    
                if(
                    (category.val() && subcategory.val() && title.val() && descriptionDiv.innerText && location.val() && !isNaN(parseInt(hourly_hours.val(), 10)) && parseInt(hourly_hours.val(), 10) > 0 && !isNaN(parseInt(hourly_budget.val(), 10)) && parseInt(hourly_budget.val(), 10) > 0 && hourly_start.val() && hourly_end.val() && skills.val()) ||
                    (category.val() && subcategory.val() && title.val() && descriptionDiv.innerText && location.val() && !isNaN(parseInt(lumpsum_budget.val(), 10)) && parseInt(lumpsum_budget.val(), 10) > 0 && lumpsum_start.val() && lumpsum_end.val() && skills.val())
                ){
                    $(v.currentForm).find('button.btn-step-submit').off('click');
                    $(v.currentForm).find('button.btn-step-submit.disabled').off('click');
                    $(v.currentForm).find('button.btn-step-submit').prop('disabled', false).removeClass('disabled').removeAttr('style');
                }
            });
    
            if( $('#not_bythehour').prop('checked') ){
                descriptionDiv.innerHTML = description.val();
                title = $('#od_tasktitle');
                location = $('[name="od_location"]');
                type = $('[name="od_type"]');
                hourly_hours = $('[name="od_hours[by-hour]"]');
                hourly_budget = $('[name="od_budget[by-hour]"]');
                hourly_start = $('[name="od_start_by[by-hour]"]');
                hourly_end = $('[name="od_complete_by[by-hour]"]');
    
                if( !category.val() || !subcategory.val() || !title.val() || !descriptionDiv.innerText || !location.val() || isNaN(parseInt(hourly_hours.val(), 10)) || parseInt(hourly_hours.val(), 10) <= 0 || isNaN(parseInt(hourly_budget.val(), 10)) || parseInt(hourly_budget.val(), 10) <= 0 || !hourly_start.val() || !hourly_end.val() || !$('#tag1').val() ){
                    $(v.currentForm).find('button.btn-step-submit').prop('disabled', true).addClass('disabled').css({
                        'background-color': '#eaeaea',
                        'color': '#ddd'
                    });
    
                }else{
                    $(v.currentForm).find('button.btn-step-submit').off('click');
                    $(v.currentForm).find('button.btn-step-submit.disabled').off('click');
                    $(v.currentForm).find('button.btn-step-submit').prop('disabled', false).removeClass('disabled').removeAttr('style');
                }
            }else if( $('#not_lumpsum').prop('checked') ){
                descriptionDiv.innerHTML = description.val();
                title = $('#od_tasktitle');
                location = $('[name="od_location"]');
                type = $('[name="od_type"]');
                lumpsum_budget = $('[name="od_budget[lump-sum]"]');
                lumpsum_start = $('[name="od_start_by[lump-sum]"]');
                lumpsum_end = $('[name="od_complete_by[lump-sum]"]');
    
                if( !category.val() || !subcategory.val() || !title.val() || !descriptionDiv.innerText || !location.val() || isNaN(parseInt(lumpsum_budget.val(), 10)) || parseInt(lumpsum_budget.val(), 10) <= 0 || !lumpsum_start.val() || !lumpsum_end.val() || !$('#tag1').val() ) {
                    $(v.currentForm).find('button.btn-step-submit').prop('disabled', true).addClass('disabled').css({
                        'background-color': '#eaeaea',
                        'color': '#ddd'
                    });
                }else{
                    $(v.currentForm).find('button.btn-step-submit').off('click');
                    $(v.currentForm).find('button.btn-step-submit.disabled').off('click');
                    $(v.currentForm).find('button.btn-step-submit').prop('disabled', false).removeClass('disabled').removeAttr('style');
                }
    
            }else{
                $(v.currentForm).find('button.btn-step-submit').prop('disabled', true).addClass('disabled').css({
                    'background-color': '#eaeaea',
                    'color': '#ddd'
                });
            }
    
            if( title.val() &&
                (
                    (!isNaN(parseInt($('[name="od_hours[by-hour]"]').val(), 10)) && parseInt($('[name="od_hours[by-hour]"]').val(), 10) > 0 && !isNaN(parseInt($('[name="od_budget[by-hour]"]').val(), 10)) && parseInt($('[name="od_budget[by-hour]"]').val(), 10) > 0 && $('[name="od_start_by[by-hour]"]').val() && $('[name="od_complete_by[by-hour]"]').val()) ||
                    (!isNaN(parseInt($('[name="od_budget[lump-sum]"]').val(), 10)) && parseInt($('[name="od_budget[lump-sum]"]').val(), 10) > 0 && $('[name="od_start_by[lump-sum]"]').val() && $('[name="od_complete_by[lump-sum]"]').val())
                )
            ){
                $(v.currentForm).find('button.btn-step-save-draft').prop('disabled', false).removeClass('disabled').removeAttr('style');
            }else{
    
                $(v.currentForm).find('button.btn-step-save-draft').prop('disabled', true).addClass('disabled').css({
                    'background-color': '#eaeaea',
                    'color': '#ddd'
                });
            }
    
            $(".frm").hide();
            $("#sf5").show();
    
        });
    
        $(".od-open5").click(function () {
            if (v.form()) {
                $("#loader").show();
                setTimeout(function () {
                    //$("#basicform").html('<h2>Thanks for your time.</h2>');
                }, 1000);
                return false;
            }
        });
    
        $(".od-back2").click(function () {
            $(".frm").hide();
            $("#sf1").show();
        });
    
        $(".od-back3").click(function () {
            $(".frm").hide();
            $("#sf2").show();
        });
    
        $(".od-back4").click(function () {
            $(".frm").hide();
            $("#sf3").show();
        });
        $(".od-back5").click(function () {
            $(".frm").hide();
            $("#sf4").show();
        });
    
        var dropzoneMessage;
        var dropzone = $("#my-awesome-dropzone").dropzone({
            url: window.task_upload,
            maxFilesize: 5,
            maxFiles: 5,
            acceptedFiles: 'image/*, application/pdf, .doc, .docx, .ppt, .pptx, .csv, .xls, .xlsx',
            addRemoveLinks: true,
            autoProcessQueue: true,
            dictDefaultMessage: $("#my-awesome-dropzone").data('text'),
            params: {
                form_token: $('#form_token').val()
            },
            init: function () {
                /*var uploadFiles = $("#uploadFiles"), dropZone = this;
                uploadFiles.on('click', function(){
                   dropZone.processQueue();
                });*/
            },
            success: function (file, response) {
                if( $("#my-awesome-dropzone .dz-preview").length > $("#my-awesome-dropzone").data('max-file') ){
                    t('e', $("#my-awesome-dropzone").data('error-max-file'));
                    this.removeFile(file);
                    return;
                }
                response = JSON.parse(response);
                if (response.status !== 'error') {
                    var attachements = $('.attachments');
                    var input = '<input type="hidden" name="attachments[' + file.upload.uuid + ']" value="' + response.message + '" />';
                    attachements.append(input);
                    input = '';
                } else {
                    t('e', response.message);
                    this.removeFile(file);
                }
            },
            addedfiles: function (files) {
                if (files.length) {
                    dropzoneMessage = $('#form_post_task_od .dz-message').clone();
                    $('#form_post_task_od .dz-message').remove();
                }
            },
    
            reset: function () {
                $('#form_post_task_od .dropzone-previews').removeClass('dz-started').prepend(dropzoneMessage);
                $('#form_post_task_od .attachments').html('');
                dropzoneMessage = '';
            }
        });
    
        window.feedbackdropzone = $("#feedback-dropzone").dropzone({
            url: window.feedback_upload,
            maxFilesize: 5,
            maxFiles: 3,
            acceptedFiles: 'image/*, application/pdf, .doc, .docx, .ppt, .pptx, .csv, .xls, .xlsx',
            addRemoveLinks: true,
            autoProcessQueue: true,
            dictDefaultMessage: $("#feedback-dropzone").data('text'),
            params: {
                form_token: $('#form_token').val()
            },
            init: function () {},
            success: function (file, response) {
                response = JSON.parse(response);
                if (response.status !== 'error') {
                    var attachements = $('.feedback-attachments');
                    var parts = response.message.split('/');
                    var fileId = file.upload.uuid.replaceAll('-', '_');
                    file.new_name = parts[ parts.length - 1 ];
    
                    var input = '<input type="hidden" id="' + fileId + '" name="attachments[' + file.upload.uuid + ']" value="' + response.message + '" />';
                    attachements.append(input);
                    input = '';
                } else {
                    this.removeFile(file);
                }
            },
            addedfiles: function (files) {
                if (files.length) {
                    dropzoneMessage = $('#form-feedback .dz-message').clone();
                    $('#form-feedback .dz-message').remove();
                }
            },
            removedfile: function(file) {
                var dzone = this;
                var fileName = file.new_name;
                var fileId = 'input#' + file.upload.uuid.replaceAll('-', '_');
    
                if( window.feedbackdropzone.submitted === undefined ) {
                    $.ajax({
                        type: 'POST',
                        url: window.feedback_upload.replace('upload', 'delete'),
                        data: {file_name: fileName},
                    }).done(function (response) {
                        if (response.status === 's') {
                            $('#form-feedback ' + fileId).remove();
                        }
                    });
                }else{
                    $('#form-feedback ' + fileId).remove();
                }
    
                if (file.previewElement != null && file.previewElement.parentNode != null) {
                    file.previewElement.parentNode.removeChild(file.previewElement);
                }
    
                return dzone._updateMaxFilesReachedClass();
            },
            reset: function () {
                $('#form-feedback .dropzone-previews').removeClass('dz-started').prepend(dropzoneMessage);
                $('#form-feedback .feedback-attachments').html('');
                dropzoneMessage = '';
            }
        });
    
    
        $(".move-date").click(function () {
            $("#date-moving-in").attr("disabled", true);
            if ($("input[name=moveDate]:checked").val() == "Moving In") {
                $("#date-moving-in").attr("disabled", false);
                $(".date").show();
                $("#date-moving-in").attr("required", true);
            } else {
                $(".date").hide();
                $("#date-moving-in").attr("required", false);
            }
        });
    
    });
    $(function () {
        $('.product-chooser').not('.disabled').find('.product-chooser-item').on('click', function () {
            $(this).parent().parent().find('.product-chooser-item').removeClass('selected');
            $(this).addClass('selected');
            $(this).find('input[type="radio"]').prop("checked", true);
    
        });
    });
    
    
    
    
    // Post Job Modal validation
    $(document).ready(function () {
    
        // validate form on keyup and submit
        var formFT = $("#form_post_job_ft").validate({
            rules: {
                od_category: {
                    //required: true
                },
                od_subcategory: {
                    //required: true
                },
                od_tasktitle: {
                    //required: true,
                    maxlength: 80,
                },
                od_taskdesc: {
                    //required: true
                },
                color: {
                    required: true
                },
                house_type: {
                    required: true
                },
                house_size: {
                    required: true
                },
                house_rental: {
                    required: true
                },
                house_location: {
                    required: true
                }
    
            },
            messages: {
                od_category: {
                    required: "Please select category"
                },
                od_subcategory: {
                    required: "Please select sub category"
                },
                od_tasktitle: {
                    required: "Please enter task title",
                    maxlength: "Task title should be less than 80 characters"
                },
                od_taskdesc: {
                    required: "Please enter task description"
                },
                moveDate: {
                    required: "Please select one of these option"
                },
                house_type: {
                    required: "Please select your house type"
                },
                house_rental: {
                    required: "Please select one of these option"
                },
                house_location: {
                    required: "Please enter your house location"
                }
    
            },
            errorElement: "span",
            errorClass: "help-inline-error",
        });
    
    
    
        $(".ft-open1").click(function () {
            if (formFT.form()) {
                $(".frm-ft").hide();
                $("#ft_sf2").show();
            }
        });
    
        $(".ft-open2").click(function () {
            if (formFT.form()) {
                $(".frm-ft").hide();
                $("#ft_sf3").show();
            }
        });
    
        $(".ft-open3").click(function () {
            if (formFT.form()) {
                $(".frm-ft").hide();
                $("#ft_sf4").show();
            }
        });
    
        $(".ft-open4").click(function () {
            if (formFT.form()) {
                $(".frm-ft").hide();
                $("#ft_sf5").show();
            }
        });
    
        $(".ft-open5").click(function () {
            if (formFT.form()) {
                $("#loader").show();
                setTimeout(function () {
                    $("#basicform").html('<h2>Thanks for your time.</h2>');
                }, 1000);
                return false;
            }
        });
    
        $(".ft-back2").click(function () {
            $(".frm-ft").hide();
            $("#ft_sf1").show();
        });
    
        $(".ft-back3").click(function () {
            $(".frm-ft").hide();
            $("#ft_sf2").show();
        });
    
        $(".ft-back4").click(function () {
            $(".frm-ft").hide();
            $("#ft_sf3").show();
        });
    
        $(".ft-back5").click(function () {
            $(".frm-ft").hide();
            $("#ft_sf4").show();
        });
    
        $('#modal_public_question').on('show.bs.modal', function (e) {
            $(this).find('input[name="parent_id"]').val($(e.relatedTarget).data('parentId'));
            $(this).find('#posted-by').text($(e.relatedTarget).data('postedBy'));
            $(this).find('.task-question').text(e.relatedTarget.parentElement.previousElementSibling.innerText);
        })
    
        $('.btn-step-save-draft').on('click', function(e){
            $(this).closest('form').prepend('<input type="hidden" name="draft" value="1" />').submit();
        });
    
        $('#post_job_ft, #post_task_od').on('show.bs.modal', function (event) {
            var modal = $(this);
            modal.find('.summernote').each(function () {
                $(this).summernote({
                    tooltip: false,
                    placeholder: $(this).attr('placeholder'),
                    toolbar: [
                        ['style', ['bold', 'italic', 'underline']],
                        ['para', ['ul', 'ol']],
                    ],
                    height: 100,
                    width: '100%',
                });
            });
        });
    
    });
    