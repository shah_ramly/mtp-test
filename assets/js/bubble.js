$(function () {
    $(document).on('keypress', '.tt-input', function(e){
        if(e.which == 13) {
            e.preventDefault();
            e.stopPropagation();
        }
    });

    /*
    $('.skills').tagsinput({
        freeInput: false,
        typeaheadjs: {
            source: function(q, sync) {
                languages = ['Art & Design', 'Business', 'Finance & Administration', 'Account Management Skills', 'Acquisition Skills', 'Administration Skills', 'Audit Skills', 'Banking Skills', 'Billing Skills', 'Blockchain Skills', 'Budget Management Skills', 'Budgeting Skills', 'Business Analysis Skills', 'Education', 'Bilingual Skills', 'English Skills', 'Mathematics Skills', 'Engineering', 'Automation Skills', 'Automotive Skills', 'Electrical Skills', 'Hardware Skills', 'Maintenance Skills', 'Manufacturing Skills', 'Robotics Skills', 'Welding Skills', 'Human Resource & Recruitment', 'Hris Skills', 'Human Resources Skills', 'Peoplesoft Skills', 'Performance Management Skills', 'Recruitment Skills', 'Relationship Management Skills', 'Training Skills', 'IT & Data Management', 'Active Directory Skills', 'Analytics Skills', 'Angular Skills', 'Artificial Intelligence Skills', 'AS400 Skills', 'AWS Skills', 'Big Data Skills', 'BPM Skills', 'C++ Skills', 'CCNA Skills', 'Office', 'Oil 1', 'Project Management', 'Sales & Marketing', 'Communication & Interpersonal', 'Management', 'Personal', 'Teamwork', 'Thought Process'];
                results = Array();
                $(languages).each(function(i, val){
                    if(val.toLowerCase().indexOf(q.toLowerCase()) != '-1'){
                        results.push(val);
                    }
                });
                sync(results);
            }
        }
    });
    */

    var products = [
        {
            "name": "Art & Design",
            "id": "artDesignBubble",
            "regimes": [
                {
                    "name": "3D Printing Skills",
                    "id": "threeDPrintingSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Adobe Skills",
                    "id": "adobeSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Architecture Skills",
                    "id": "architectureSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Autocad Skills",
                    "id": "autocadSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Cad Skills",
                    "id": "cadSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Chef Skills",
                    "id": "chefSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Cooking Skills",
                    "id": "cookingSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Design Skills",
                    "id": "designSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Drawing Skills",
                    "id": "drawingSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Illustrator Skills",
                    "id": "illustratorSkillsBubble",
                    "categories": [
                        
                    ]
                },
            ]
        },
        {
            "name": "Business, Finance & Administration",
            "id": "businessFinanceAdminBubble",
            "regimes": [
                {
                    "name": "Account Management Skills",
                    "id": "accountManagementSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Acquisition Skills",
                    "id": "acquisitionSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Administration Skills",
                    "id": "administrationSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Audit Skills",
                    "id": "auditSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Banking Skills",
                    "id": "bankingSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Billing Skills",
                    "id": "billingSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Blockchain Skills",
                    "id": "blockchainSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Budget Management Skills",
                    "id": "budgetManagementSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Budgeting Skills",
                    "id": "budgetingSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Business Analysis Skills",
                    "id": "businessAnalysisSkillsBubble",
                    "categories": [
                        
                    ]
                },
            ]
        },
        {
            "name": "Education",
            "id": "educationBubble",
            "regimes": [
                {
                    "name": "Bilingual Skills",
                    "id": "bilingualSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "English Skills",
                    "id": "englishSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Mathematics Skills",
                    "id": "mathematicsSkillsBubble",
                    "categories": [
                        
                    ]
                },
            ]
        },
        {
            "name": "Engineering",
            "id": "engineeringBubble",
            "regimes": [
                {
                    "name": "Automation Skills",
                    "id": "automationSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Automotive Skills",
                    "id": "automotiveSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Electrical Skills",
                    "id": "electricalSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Hardware Skills",
                    "id": "hardwareSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Maintenance Skills",
                    "id": "maintenanceSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Manufacturing Skills",
                    "id": "manufacturingSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Robotics Skills",
                    "id": "roboticsSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Welding Skills",
                    "id": "weldingSkillsBubble",
                    "categories": [
                        
                    ]
                },
            ]
        },
        {
            "name": "Human Resource & Recruitment",
            "id": "hrRecruitBubble",
            "regimes": [
                {
                    "name": "Hris Skills",
                    "id": "hrisSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Human Resources Skills",
                    "id": "hrSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Peoplesoft Skills",
                    "id": "peoplesoftSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Performance Management Skills",
                    "id": "performanceManagementSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Recruitment Skills",
                    "id": "recruitmentSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Relationship Management Skills",
                    "id": "relationshipManagementSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Training Skills",
                    "id": "trainingSkillsBubble",
                    "categories": [
                        
                    ]
                },
            ]
        },
        {
            "name": "IT & Data Management",
            "id": "itDataManageBubble",
            "regimes": [
                {
                    "name": "Active Directory Skills",
                    "id": "activeDirectorySkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Analytics Skills",
                    "id": "analyticsSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Angular Skills",
                    "id": "angularSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Artificial Intelligence Skills",
                    "id": "artificialIntelligenceSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "AS400 Skills",
                    "id": "aS400SkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "AWS Skills",
                    "id": "awsSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "Big Data Skills",
                    "id": "bigDataSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "BPM Skills",
                    "id": "bpmSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "C++ Skills",
                    "id": "cplusplusSkillsBubble",
                    "categories": [
                        
                    ]
                },
                {
                    "name": "CCNA Skills",
                    "id": "ccnasSkillsBubble",
                    "categories": [
                        
                    ]
                },
            ]
        },
        {
            "name": "Office",
            "id": "officeBubble",
            "regimes": [
                {
                    "name": "Oil 1",
                    "id": "eoroil",
                    "categories": [
                        
                    ]
                },
            ]
        },
        {
            "name": "Project Management",
            "id": "projectManageBubble",
            "regimes": [
                {
                    "name": "Oil 1",
                    "id": "eoroil",
                    "categories": [
                        
                    ]
                },
            ]
        },
        {
            "name": "Sales & Marketing",
            "id": "salesMarketingBubble",
            "regimes": [
                {
                    "name": "Oil 1",
                    "id": "eoroil",
                    "categories": [
                        
                    ]
                },
            ]
        },
        {
            "name": "Communication & Interpersonal",
            "id": "commInterpersonalBubble",
            "regimes": [
                {
                    "name": "Oil 1",
                    "id": "eoroil",
                    "categories": [
                        
                    ]
                },
            ]
        },
        {
            "name": "Management",
            "id": "managementBubble",
            "regimes": [
                {
                    "name": "Oil 1",
                    "id": "eoroil",
                    "categories": [
                        
                    ]
                },
            ]
        },
        {
            "name": "Personal",
            "id": "personalBubble",
            "regimes": [
                {
                    "name": "Oil 1",
                    "id": "eoroil",
                    "categories": [
                        
                    ]
                },
            ]
        },
        {
            "name": "Teamwork",
            "id": "teamworkBubble",
            "regimes": [
                {
                    "name": "Oil 1",
                    "id": "eoroil",
                    "categories": [
                        
                    ]
                },
            ]
        },
        {
            "name": "Thought Process",
            "id": "thoughtProcessBubble",
            "regimes": [
                {
                    "name": "Oil 1",
                    "id": "eoroil",
                    "categories": [
                        
                    ]
                },
            ]
        }
    ];

    $(document).ready(function() {
        /*
        $.each(products, function (index, value) {
            $(".button-subgroup-pills").append('<label class="btn btn-bubble"><input type="checkbox" class="firsttier" name="firsttier[]" value="' + value.id + '"><div>' + value.name + '</div></label>');
        });
        */
        
        /*
        $(".firsttier").change(function(e) {  
            if (this.checked) {
                id = $(this).val();
                name = $(this).next().text();
                $('.skills').tagsinput('add', { 'id': id, 'name': name });
                $(this).parent().fadeOut(300);
                var firstTierIndex = $('.firsttier').index(this);
                $.each(products[firstTierIndex].regimes, function (index, value) {
                    if (firstTierIndex == 0) {
                        $(".button-subgroup-pills").append('<label class="btn btn-bubble"><input type="checkbox" class="secondtier1" name="secondtier1[]" value="' + value.id + '"><div>' + value.name + '</div></label>');
                    }
                    else if (firstTierIndex == 1) {
                        $(".button-subgroup-pills").append('<label class="btn btn-bubble"><input type="checkbox" class="secondtier2" name="secondtier2[]" value="' + value.id + '"><div>' + value.name + '</div></label>');
                    }
                });
                
                
                $(".secondtier1").change(function(e) {
                    if (this.checked) {
                        id = $(this).val();
                        name = $(this).next().text();
                        $('.skills').tagsinput('add', { 'id': id, 'name': name });
                        $(this).parent().fadeOut(300);
                        var secondTierIndex = $('.secondtier1').index(this);
                        console.log("Second Tier Index: " + secondTierIndex);
                        $.each(products[firstTierIndex].regimes[secondTierIndex].categories, function (index, value) {
                            $(".button-subgroup-pills").append('<label class="btn btn-bubble"><input type="checkbox" class="thirdtier" name="thirdtier[]" value="' + value.id + '"><div>' + value.name + '</div></label>');
                        });
                    }
                });
                
                $(".secondtier2").change(function(e) {
                    if (this.checked) {
                        id = $(this).val();
                        name = $(this).next().text();
                        $('.skills').tagsinput('add', { 'id': id, 'name': name });
                        $(this).parent().fadeOut(300);
                        var secondTierIndex = $('.secondtier2').index(this);
                        console.log("Second Tier Index: " + secondTierIndex);
                        $.each(products[firstTierIndex].regimes[secondTierIndex].categories, function (index, value) {
                            $(".button-subgroup-pills").append('<label class="btn btn-bubble"><input type="checkbox" class="thirdtier" name="thirdtier[]" value="' + value.id + '"><div>' + value.name + '</div></label>');
                        });
                    }
                });
            }
        });
        */
        
        $(".secondtier").change(function(e) {
            if (this.checked) {
                var secondTierIndex = $('.secondtier').index(this);
                console.log("Second Tier Index: " + secondTierIndex);
                /*$.each(products.regimes[secondTierIndex].categories, function (index, value) {
                    $(".button-subsubsubgroup-pills").append('<label class="btn btn-bubble"><input type="checkbox" class="secondtier" name="secondtier[]" value="' + value.id + '"><div>' + value.name + '</div></label>');
                });*/
            }
        });
        
        
        var loadRegimeOptions = function ($productElement) {
            var $productElementIndex = $('.product').index(this);
            var $regimeElement = $($productElement.data('related-regime'));
            var $categoryElement = $($regimeElement.data('related-category'));
            console.log("Product Element Index: " + $productElementIndex);

            /*var selectedProductIndex = $productElement[0].selectedIndex - 1;
            

            $regimeElement.html('<option>-- Select --</option>');
            $.each(products[selectedProductIndex].regimes, function (index, value) {
                $regimeElement.append('<option value="' + value.id + '">' + value.name + '</option>');

            });
            $categoryElement.html('<option>-- Select --</option>');*/
        };
        
        var loadCategories = function ($regimeElement) {
            var $productElement = $($regimeElement.data('related-product'));
            var $categoryElement = $($regimeElement.data('related-category'));

            var selectedProductIndex = $productElement[0].selectedIndex - 1;
            var selectedRegimeIndex = $regimeElement[0].selectedIndex - 1;


            console.log("Product Element Load Cat: " + $productElement);
            console.log("Category Element Load Cat: " + $categoryElement);
            console.log("Selected Product Index Load Cat: " + selectedProductIndex);
            console.log("Selected Regime Index Load Cat: " + selectedRegimeIndex);

            $categoryElement.html('<option>-- Select --</option>');
            $.each(products[selectedProductIndex].regimes[selectedRegimeIndex].categories, function (index, value) {
                $categoryElement.append('<option value="' + value.id + '">' + value.name + '</option>');
            });
        };
    });
    
    /*$(function () {
        $.each(products, function (index, value) {
            $(".product").append('<option value="' + value.id + '">' + value.name + '</option>');
            $(".product").append('<label><input type="checkbox" value="' + value.id + '">' + value.name + '</label>');
        });

        $('.product').on('change', function (e) {
            loadRegimeOptions($(this));
        });
        $('.product_v2').on('change', function (e) {
            loadRegimeOptions($(this));
        });

        $('.regime').on('change', function () {
            loadCategories($(this));
        });
    });

    var loadRegimeOptions = function ($productElement) {
        var $regimeElement = $($productElement.data('related-regime'));
        var $categoryElement = $($regimeElement.data('related-category'));

        var selectedProductIndex = $productElement[0].selectedIndex - 1;
        
        console.log("Regime Element Load Regime: " + JSON.stringify($regimeElement));
        console.log("Category Element Load Regime: " + JSON.stringify($categoryElement));
        console.log("Selected Product Index Load Regime: " + selectedProductIndex);
        console.log("Selected Product Index: " + JSON.stringify(products[selectedProductIndex].regimes));
        

        $regimeElement.html('<option>-- Select --</option>');
        $.each(products[selectedProductIndex].regimes, function (index, value) {
            console.log("Index:" + index);
            console.log("Value:" + value);
            $regimeElement.append('<option value="' + value.id + '">' + value.name + '</option>');
            
        });
        $categoryElement.html('<option>-- Select --</option>');
    };

    var loadCategories = function ($regimeElement) {
        var $productElement = $($regimeElement.data('related-product'));
        var $categoryElement = $($regimeElement.data('related-category'));

        var selectedProductIndex = $productElement[0].selectedIndex - 1;
        var selectedRegimeIndex = $regimeElement[0].selectedIndex - 1;
        
        
        console.log("Product Element Load Cat: " + $productElement);
        console.log("Category Element Load Cat: " + $categoryElement);
        console.log("Selected Product Index Load Cat: " + selectedProductIndex);
        console.log("Selected Regime Index Load Cat: " + selectedRegimeIndex);

        $categoryElement.html('<option>-- Select --</option>');
        $.each(products[selectedProductIndex].regimes[selectedRegimeIndex].categories, function (index, value) {
            $categoryElement.append('<option value="' + value.id + '">' + value.name + '</option>');
        });
    };*/
});
