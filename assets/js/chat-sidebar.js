(function($) {

    "use strict";


    $(document).ready(function() {

        //
        // Detect mobile devices
        //

        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPod|iPad/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };



        //
        // Toggle chat
        //

        [].forEach.call(document.querySelectorAll('[data-chat="open"]'), function (a) {
            a.addEventListener('click', function () {
                document.querySelector('.main').classList.toggle('main-visible');
            }, false );
        });

        //
        // Toggle chat`s sidebar
        //

        [].forEach.call(document.querySelectorAll('[data-chat-sidebar-toggle]'), function (e) {
            e.addEventListener('click', function (event) {
                event.preventDefault();
                var chat_sidebar_id = e.getAttribute('data-chat-sidebar-toggle');
                var chat_sidebar = document.querySelector(chat_sidebar_id);

                if (typeof(chat_sidebar) != 'undefined' && chat_sidebar != null) {
                    if ( chat_sidebar.classList.contains('chat-sidebar-visible') ) {
                        chat_sidebar.classList.remove('chat-sidebar-visible')
                        document.body.classList.remove('sidebar-is-open');
                    } else {
                        [].forEach.call(document.querySelectorAll('.chat-sidebar'), function (e) {
                            e.classList.remove('chat-sidebar-visible');
                            document.body.classList.remove('sidebar-is-open');
                        });
                        chat_sidebar.classList.add('chat-sidebar-visible');
                        document.body.classList.add('sidebar-is-open');
                    }
                }

            });
        });

        //
        // Close all chat`s sidebars
        //

        [].forEach.call(document.querySelectorAll('[data-chat-sidebar-close]'), function (a) {
            a.addEventListener('click', function (event) {
                event.preventDefault();
                document.body.classList.remove('sidebar-is-open');
                [].forEach.call(document.querySelectorAll('.chat-sidebar-details'), function (a) {
                    a.classList.remove('chat-sidebar-visible');
                });
            }, false );
        });


        //
        // Mobile screen height minus toolbar height
        //

        function mobileScreenHeight() {
            if ( document.querySelectorAll('.navigation').length && document.querySelectorAll('.sidebar').length ) {
                document.querySelector('.sidebar').style.height = windowHeight - document.querySelector('.navigation').offsetHeight + 'px';
            }
        }

        if ( isMobile.any() && (document.documentElement.clientWidth < 1024) ) {
            var windowHeight = document.documentElement.clientHeight;
            mobileScreenHeight();

            window.addEventListener('resize', function(event){
                if (document.documentElement.clientHeight != windowHeight) {
                    windowHeight = document.documentElement.clientHeight;
                    mobileScreenHeight();
                }
            });
        }

        //
        // Scroll to end of chat
        //

        if (document.querySelector('.end-of-chat')) {
            document.querySelector('.end-of-chat').scrollIntoView();
        }


    });

})(jQuery);