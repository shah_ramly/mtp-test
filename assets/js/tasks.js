$(function(){
   // Time Planning Modal Show Event
   $('#modal_task_planning').on('show.bs.modal', function(event){
      var button = $(event.relatedTarget),
          modal = $(this),
          taskId = button.data('taskId'),
          totalHours = button.data('taskHours'),
          earning = button.data('taskEarning'),
          start_date = button.data('taskStart'),
          end_date = button.data('taskEnd'),
          time_planning = task_planning,
          task_start_date = moment(start_date),
          task_end_date = moment(end_date);

      modal.find('input#task-id').val(taskId);
      modal.find('input#total-hours').val(totalHours);
      modal.find('input#task-earning').val(earning);
      modal.find('strong#start-date').text(task_start_date.format('DD MMM YYYY'));
      modal.find('strong#end-date').text(task_end_date.format('DD MMM YYYY'));

      modal.find('.date, .time').datetimepicker('destroy');

      if(Object.keys(time_planning).length && Object.keys(time_planning).indexOf(taskId) > -1){
         var keys = Object.keys(time_planning);
         keys.forEach(function(plan, id){
            if(plan === taskId){
               var tp = time_planning[plan];
               tp.forEach(function(p, idx){
                  var date = moment(p.date.date, moment.DATETIME_LOCAL_MS);
                  var start_date = moment(p.start.date, moment.DATETIME_LOCAL_MS);
                  var end_date = moment(p.end.date, moment.DATETIME_LOCAL_MS);

                  if(idx > 0){
                     var timeRow = modal.find('.form-date-taskplan-row').first().clone(),
                         rowsContainer = modal.find('.form-date-taskplan-rows-container');

                     timeRow.find('.date > input, .time > input').val('').removeAttr('readOnly');
                     timeRow.find('.taskplan-row-remove i').removeClass('fa-refresh').addClass('fa-trash');
                     timeRow.html(function(x, html){ return html.replace(/form_taskplan_date/g, 'form_taskplan_date_'+idx) });
                     timeRow.html(function(x, html){ return html.replace(/form_taskplan_time_from/g, 'form_taskplan_time_from_'+idx) });
                     timeRow.html(function(x, html){ return html.replace(/form_taskplan_time_to/g, 'form_taskplan_time_to_'+idx) });

                     timeRow.addClass('added-row');
                     timeRow.find('.taskplan-row-remove').on('click', function(e){
                        removeRow(e);
                     })
                     timeRow.removeClass('hide');
                     rowsContainer.append(timeRow);
                  }

                  $(modal.find('.date')[idx]).datetimepicker('destroy');
                  $(modal.find('.time.start')[idx]).datetimepicker('destroy');
                  $(modal.find('.time.end')[idx]).datetimepicker('destroy');

                  $(modal.find('.date')[idx]).datetimepicker({
                     defaultDate: date,
                     format: 'DD-MMM-YYYY',
                     useCurrent:false,
                     minDate: task_start_date,
                     maxDate: task_end_date,
                     viewDate: date
                  });

                  $(modal.find('.time.start')[idx]).datetimepicker({
                     useCurrent: false,
                     format: 'LT',
                     defaultDate: moment(start_date),
                     minDate: moment(start_date).startOf('d'),
                     maxDate: moment(start_date).endOf('d').subtract(59, 's')
                  });
                  $(modal.find('.time.start')[idx]).data("time", start_date.format("YYYY-MM-DD H:mm:ss"));
                  $(modal.find('.time.end')[idx]).datetimepicker({
                     useCurrent: false,
                     format: 'LT',
                     defaultDate: moment(end_date),
                     minDate: moment(end_date).startOf('d').subtract(59, 's'),
                     maxDate: moment(end_date).endOf('d')
                  });
                  $(modal.find('.time.end')[idx]).data("time", end_date.format("YYYY-MM-DD H:mm:ss"));

                  $(modal.find('input[name*="date"]')[idx]).on('change.datetimepicker', function(e){

                     var startInput = $(this).parent().parent().find('.time.start input');
                     var endInput = $(this).parent().parent().find('.time.end input');
                     var startTime = startInput.val() !== '' ? moment( e.date.format('YYYY-MM-DD')+' '+startInput.val() ) : moment(e.date).startOf('d').add(7, 'h');
                     var endTime = endInput.val() !== '' ? moment( e.date.format('YYYY-MM-DD')+' '+endInput.val() ) : moment(e.date).endOf('d');

                     startTime.set({
                        'year' : e.date.get('year'),
                        'month'  : e.date.get('month'),
                        'day' : e.date.get('day')
                     });
                     endTime.set({
                        'year' : e.date.get('year'),
                        'month'  : e.date.get('month'),
                        'day' : e.date.get('day')
                     });

                     $(modal.find('.time.start')[idx]).datetimepicker({
                        format: 'LT',
                        useCurrent: false,
                        defaultDate: startTime,
                        minDate: moment(startTime).startOf('d'),
                        maxDate: moment(startTime).endOf('d').subtract(59, 's'),
                     });
                     $(modal.find('.time.start')[idx]).data("time", startTime.format("YYYY-MM-DD H:mm:ss"));

                     $(modal.find('.time.end')[idx]).datetimepicker({
                        format: 'LT',
                        useCurrent: false,
                        defaultDate: endTime,
                        minDate: moment(endTime).startOf('d').add(59, 's'),
                        maxDate: moment(endTime).endOf('d'),
                     });
                     $(modal.find('.time.end')[idx]).data("time", endTime.format("YYYY-MM-DD H:mm:ss"));
                     check_inputs();

                     //startInput = endInput = startTime = endTime = null;
                  })

                  $(modal.find('.time.start')[idx]).on("hide.datetimepicker", function(e){
                     var rowDate = $(this).parent().parent().prev().find('.date input').val();
                     var startTime = rowDate !== '' ? moment(rowDate) : e.date;
                     startTime.set({
                        'hour' : e.date.get('hour'),
                        'minute' : e.date.get('minute'),
                        'second' : e.date.get('second')
                     })
                     $.data(this, "time", startTime.format("YYYY-MM-DD H:mm:ss"));
                     $(modal.find('.time.end')[idx]).datetimepicker({
                        minDate: moment(startTime).add(59, 's'),
                        maxDate: moment(startTime).endOf('d')
                     });

                     check_inputs();
                     //rowDate = startTime = null;
                  })

                  $(modal.find('.time.end')[idx]).on("hide.datetimepicker", function(e){
                     var rowDate = $(this).parent().parent().prev().find('.date input').val();
                     var endTime = rowDate !== '' ? moment(rowDate) : e.date;
                     endTime.set({
                        'hour' : e.date.get('hour'),
                        'minute' : e.date.get('minute'),
                        'second' : e.date.get('second')
                     })
                     $.data(this, "time", endTime.format("YYYY-MM-DD h:mm:ss a"));
                     $(modal.find('.time.start')[idx]).datetimepicker({
                        minDate: moment(endTime).startOf('d'),
                        maxDate: moment(endTime).subtract(59, 's')
                     });

                     check_inputs();
                     //rowDate = endTime = null;
                  })
                  //timeRow = rowsContainer = null;
               })
            }
         });
      }else{
         modal.find('.date').datetimepicker({
            useCurrent: false,
            format: 'DD-MMM-YYYY',
            minDate: moment(task_start_date),
            maxDate: moment(task_end_date),
            viewDate: moment(task_start_date),
         });
         modal.find('.time').datetimepicker({
            useCurrent: false,
            format: 'LT',
            minDate: moment().startOf('d'),
            maxDate: moment().endOf('d')
         })

         modal.find('.date').on('change.datetimepicker', function (e) {
            var startInput = $(this).parent().parent().find('.time.start input');
            var endInput = $(this).parent().parent().find('.time.end input');
            var startTime = startInput.val() !== '' ? moment(e.date.format('YYYY-MM-DD') + ' ' + startInput.val()) : moment(e.date).startOf('d').add(7, 'h');
            var endTime = endInput.val() !== '' ? moment(e.date.format('YYYY-MM-DD') + ' ' + endInput.val()) : moment(e.date).endOf('d');

            $(this).parent().parent().find('.time.start').datetimepicker({
               format: 'LT',
               useCurrent: false,
               defaultDate: startTime,
               minDate: moment(startTime).startOf('d'),
               maxDate: moment(startTime).endOf('d').subtract(59, 's'),
            })
            $(this).parent().parent().find('.time.start').data("time", startTime.format("YYYY-MM-DD H:mm:ss"));
            $(this).parent().parent().find('.time.end').datetimepicker({
               format: 'LT',
               useCurrent: false,
               defaultDate: endTime,
               minDate: moment(endTime).startOf('d').add(59, 's'),
               maxDate: moment(endTime).endOf('d'),
            })
            $(this).parent().parent().find('.time.end').data("time", endTime.format("YYYY-MM-DD H:mm:ss"));

            check_inputs()

         })

         modal.find('.time.start:first-child').on("hide.datetimepicker", function (e) {
            var rowDate = $(this).parent().parent().prev().find('.date input').val();
            var startTime = rowDate !== '' ? moment(rowDate) : e.date;
            startTime.set({
               'hour': e.date.get('hour'),
               'minute': e.date.get('minute'),
               'second': e.date.get('second')
            })
            $.data(this, "time", startTime.format("YYYY-MM-DD H:mm:ss"));
            $(e.target).parent().next().find('.time').datetimepicker('destroy');
            $(e.target).parent().next().find('.time').datetimepicker({
               format: 'LT',
               minDate: moment(startTime).add(59, 's'),
               maxDate: moment(startTime).endOf('d')
            });
            check_inputs()

         })

         modal.find('.time.end:first-child').on("hide.datetimepicker", function (e) {
            var rowDate = $(this).parent().parent().prev().find('.date input').val();
            var endTime = rowDate !== '' ? moment(rowDate) : e.date;
            var timeField = $(e.target);
            endTime.set({
               'hour': e.date.get('hour'),
               'minute': e.date.get('minute'),
               'second': e.date.get('second')
            })
            $.data(this, "time", endTime.format("YYYY-MM-DD H:mm:ss"));
            timeField.parent().prev().children(':first-child').datetimepicker({
               format: 'LT',
               minDate: moment(endTime).startOf('d'),
               maxDate: moment(endTime).subtract(59, 's')
            });
            check_inputs();
         })
      }

      const rowsCount = 5;
      var counter = modal.find('.form-date-taskplan-row:not(.hide)').length;
      if( counter >= rowsCount ){
         modal.find('button.btn-addmore').attr('disabled', 'disabled');
      }else{
         modal.find('button.btn-addmore').removeAttr('disabled');
      }

      modal.find('button.btn-addmore').off('click');
      modal.find('button.btn-addmore').on('click', function(e){
         counter = modal.find('.form-date-taskplan-row:not(.hide)').length;
         if( counter >= rowsCount ){
            $(e.currentTarget).attr('disabled', 'disabled').addClass('disabled');
         }else{
            var timeRow = modal.find('.form-date-taskplan-row').first().clone(),
                rowsContainer = modal.find('.form-date-taskplan-rows-container'),
                id = Math.random().toString().substr(2, 8);

            timeRow.find('.taskplan-row-remove i').removeClass('fa-refresh').addClass('fa-trash');
            timeRow.find('.time input').val('').prop('readonly', false);
            timeRow.html(function(idx, html){ return html.replace(/form_taskplan_date/g, 'form_taskplan_date_'+id) });
            timeRow.html(function(idx, html){ return html.replace(/form_taskplan_time_from/g, 'form_taskplan_time_from_'+id) });
            timeRow.html(function(idx, html){ return html.replace(/form_taskplan_time_to/g, 'form_taskplan_time_to_'+id) });
            timeRow.find('.date').datetimepicker({
               viewDate: moment(start_date, 'YYYY-MM-DD'),
               format: 'DD-MMM-YYYY',
               useCurrent: false,
               minDate: moment(start_date, 'YYYY-MM-DD'),
               maxDate: moment(end_date, 'YYYY-MM-DD'),
            });

            timeRow.find('.time.start, .time.end').datetimepicker({
               format: 'LT'
            })

            timeRow.find('.date').on('change.datetimepicker', function(e){

               var startInput = $(this).parent().parent().find('.time.start input');
               var endInput = $(this).parent().parent().find('.time.end input');
               var startTime = startInput.val() !== '' ? moment( e.date.format('YYYY-MM-DD')+' '+startInput.val() ) : e.date.startOf('d').add(7, 'h');
               var endTime = endInput.val() !== '' ? moment( e.date.format('YYYY-MM-DD')+' '+endInput.val() ) : e.date.endOf('d');

               startTime.set({
                  'year' : e.date.get('year'),
                  'month'  : e.date.get('month'),
                  'day' : e.date.get('day')
               });
               endTime.set({
                  'year' : e.date.get('year'),
                  'month'  : e.date.get('month'),
                  'day' : e.date.get('day')
               });

               timeRow.find('.time.start').datetimepicker({
                  format: 'LT',
                  useCurrent: false,
                  defaultDate: startTime,
                  minDate: startTime.startOf('d'),
                  maxDate: startTime.endOf('d').subtract(59, 's'),
               });

               timeRow.find('.time.start').data("time", startTime.format("YYYY-MM-DD H:mm:ss"));
               timeRow.find('.time.end').datetimepicker({
                  format: 'LT',
                  useCurrent: false,
                  defaultDate: endTime,
                  minDate: endTime.startOf('d').subtract(59, 's'),
                  maxDate: endTime.endOf('d'),
               });

               timeRow.find('.time.end').data("time", endTime.format("YYYY-MM-DD H:mm:ss"));
               check_inputs();
               startInput = endInput = startTime = endTime = null;
            });

            timeRow.find('.time.start').on("change.datetimepicker", function(e){
               var rowDate = $(this).parent().parent().prev().find('.date input').val();
               var startTime = rowDate !== '' ? moment(rowDate) : e.date;
               startTime.set({
                  'hour' : e.date.get('hour'),
                  'minute' : e.date.get('minute'),
                  'second' : e.date.get('second')
               })

               $.data(this, "time", startTime.format("YYYY-MM-DD H:mm:ss"));
               $(e.target).parent().next().find('.time').datetimepicker({
                  defaultDate: startTime,
                  minDate: startTime.startOf('d').subtract(59, 's'),
                  maxDate: startTime.endOf('d')
               });

               check_inputs();
               rowDate = startTime = null;
            });

            timeRow.find('.time.end').on("change.datetimepicker", function(e){
               var rowDate = $(this).parent().parent().prev().find('.date input').val();
               var endTime = rowDate !== '' ? moment(rowDate) : e.date;
               endTime.set({
                  'hour' : e.date.get('hour'),
                  'minute' : e.date.get('minute'),
                  'second' : e.date.get('second')
               })
               $.data(this, "time", endTime.format("YYYY-MM-DD H:mm:ss"));
               $(e.target).parent().prev().find('.time').datetimepicker({
                  defaultDate: endTime,
                  minDate: endTime.startOf('d'),
                  maxDate: endTime.subtract(59, 's')
               });

               check_inputs();
               rowDate = endTime = null;
            });

            timeRow.addClass('added-row');
            timeRow.find('.taskplan-row-remove').on('click', function(e){
               removeRow(e);
            });
            timeRow.removeClass('hide');
            rowsContainer.append(timeRow);
            counter++;
            if( counter >= rowsCount ) {
               $(e.currentTarget).attr('disabled', 'disabled').addClass('disabled');
            }
         }
      });
      check_inputs();
   });

   $('#modal_task_planning').on('hidden.bs.modal', function(e){
      var modal = $(this);
      modal.find('.added-row').remove();
      modal.find('.date, .time').datetimepicker('clear');
      modal.find('.date, .time').datetimepicker('destroy');
      modal.find('.form-date-taskplan-row').removeClass('hide');
      modal = null;
   })

   $('.taskplan-row-remove').on('click', function(e){
      removeRow(e);
   })

   function removeRow(e){
      var modal = $('#modal_task_planning'),
          count = modal.find('.form-date-taskplan-row:not(.hide)').length;
      if( count <= 5 ) {
         modal.find('button.btn-addmore').removeAttr('disabled').removeClass('disabled');
      }

      if( $(e.currentTarget).parent().parent().hasClass('added-row') ) {
         $(e.currentTarget).parent().parent().remove();
      }else{
         $($(e.currentTarget).parent().parent().find('input')).removeAttr('readOnly');
         $(e.currentTarget).parent().parent().find('.date, .time').datetimepicker('clear');
         $(e.currentTarget).parent().parent().addClass('hide');
         /*if( $('.added-row').length === 0 ) {
            if(confirm('This will reset/clear all your Task Planning?')) {
               $.ajax({
                  url: window.location.origin + '/workspace/task/clear_time_planning',
                  method: 'POST',
                  data: {'task_id': $('input#task-id').val()}
               }).done(function (e) {
                  t('s', 'All time planning are reset');
                  setTimeout(function(){
                     window.location = window.location.origin+'/workspace/'+moment().get('year')+'/'+(moment().get('month')+1)+'#tcInProgressMonth'
                  }, 1000);
               })
            }
         }*/
      }

      check_inputs();
      modal = null;
   }

   function check_inputs(){
      var modal = document.querySelector('#modal_task_planning'),
          rows = modal.querySelectorAll('.form-date-taskplan-row:not(.hide)'),
          status = [];

      rows.forEach(function(row){
         status.push(check_input(row));
      });

      if(rows.length > 1 && status.indexOf('true') > -1) status = ['true'];

      status = [].concat(status, check_overlapping(rows));

      if( status.length && status.indexOf('false') === -1 ){
         $(modal).find('.btn-confirm').removeAttr('disabled');
      }else{
         $(modal).find('.btn-confirm').attr('disabled', 'disabled');
      }
      modal = rows = status = null;
   }

   function check_input(timeRow){
      var modal = $('#modal_task_planning');
      timeRow = $(timeRow);

      if( timeRow.find('.date input').val().length && timeRow.find('.time.start input').val().length && timeRow.find('.time.end input').val().length ){
         return 'true';
      }else{
         return 'false';
      }
   }

   function check_overlapping(rows){
      var rowsLength = rows.length;
      var status = [];
      var overlapped = [];
      rows.forEach(function(row, idx){
         if( idx+1 < rowsLength ){
            for (let i = idx + 1; i < rowsLength; i++) {
               var row_start_time = $(row).find('.time.start').data('time');
               var row_end_time = $(row).find('.time.end').data('time');
               var second_row_start_time = $(rows[i]).find('.time.start').data('time');
               var second_row_end_time = $(rows[i]).find('.time.end').data('time');
               var is_same_date = moment(moment(row_start_time).format('YYYY-MM-DD')).isSame(moment(second_row_start_time).format('YYYY-MM-DD'));
               var is_start_between = moment(row_start_time).isBetween(moment(second_row_start_time), moment(second_row_end_time));
               var is_end_between = moment(row_end_time).isBetween(moment(second_row_start_time), moment(second_row_end_time));
               var is_second_start_between = moment(second_row_start_time).isBetween(moment(row_start_time), moment(row_end_time));
               var is_second_end_between = moment(second_row_end_time).isBetween(moment(row_start_time), moment(row_end_time));

               if (row_start_time && row_end_time && is_same_date && ((is_start_between || is_end_between) || (is_second_start_between || is_second_end_between))) {
                  if( is_start_between ) {
                     overlapped.push($(row).find('.time.start input'));
                     overlapped.push($(rows[i]).find('.time.start input, .time.end input'));
                  }else if( is_end_between ){
                     overlapped.push($(row).find('.time.end input'));
                     overlapped.push($(rows[i]).find('.time.start input, .time.end input'));
                  }else if( is_second_start_between ){
                     overlapped.push($(row).find('.time.start input, .time.end input'));
                     overlapped.push($(rows[i]).find('.time.start input'));
                  }else if( is_second_end_between ){
                     overlapped.push($(row).find('.time.start input, .time.end input'));
                     overlapped.push($(rows[i]).find('.time.end input'));
                  }
                  status.push('false');

               } else if (row_start_time && row_end_time && moment(row_end_time).isBefore(moment(row_start_time).add(1, 'm'))) {
                  overlapped.push($(row).find('.time.end input'));
                  status.push('false');

               } else if (second_row_start_time && second_row_end_time && moment(second_row_end_time).isBefore(moment(second_row_start_time).add(1, 'm'))) {
                  overlapped.push($(rows[i]).find('.time.end input'));
                  status.push('false');

               } else {
                  $(row).find('.time.end input').removeClass('text-danger border-danger');
                  $(rows[i]).find('.time.start input').removeClass('text-danger border-danger');
                  status.push('true');
               }
            }
         }else{
            for (let i = idx; i < rowsLength; i++) {
               var row_start_time = $(row).find('.time.start').data('time');
               var row_end_time = $(row).find('.time.end').data('time');

               if (row_start_time && row_end_time && moment(row_end_time).isBefore(moment(row_start_time).add(1, 'm'))) {
                  $(rows[i]).find('.time.end input').addClass('text-danger border-danger');
                  status.push('false');
               } else {
                  $(row).find('.time.end input, .time.end input').removeClass('text-danger border-danger');
                  status.push('true');
               }
            }
         }

         rows.forEach(function (item, idx) {
            $(item).find('.time input').removeClass('text-danger border-danger');
         });

         overlapped.forEach(function(item, idx){
            $(item).addClass('text-danger border-danger')
         });
      });
      rowsLength = null;
      return status;
   }

});