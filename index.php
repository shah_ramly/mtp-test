<?php
require_once 'lib/limonade.php';
require_once 'vendor/autoload.php';
require_once 'lib/config.php';

function configure(){
    $base_uri = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];
    option('base_uri', ROOTPATH);
    option('site_uri', rtrim($base_uri, '/'));
    option('title_prefix', ' | ');
    option('title', SITE_NAME);
    option('script_version', '1.0.49');
    option('style_version', '1.0.37');
}

function before($route){
    global $cms;
    global $user;
    global $task_centre;
    global $job_centre;
    global $task;
    global $job;
    global $notification;
    global $feedback;

    if ($user->logged && $cms->settings()['session_timeout'] > 0) {
        $minutes = round((time() - strtotime($_SESSION[WEBSITE_PREFIX . 'LAST_LOGIN'])) / 60, 2);
        if ($minutes > $cms->settings()['session_timeout']) {
            unset($_SESSION[WEBSITE_PREFIX . 'USER_ID']);
            unset($_SESSION[WEBSITE_PREFIX . 'LAST_LOGIN']);

            header('Location: ' . url_for('/'));
            exit();
        }
    }

    layout('layout/app.html.php');
    set('path', $route['path']);
    set('func', $route['callback'][1]);
    set('settings', $cms->settings());
    set('cms', $cms);
    set('user', $user);
    set('root_dir', option('root_dir'));

    if (('xmlhttprequest' !== strtolower($_SERVER['HTTP_X_REQUESTED_WITH'] ?? ''))) {
        set('categories', $cms->categories());
        set('countries', $task_centre->set_countries());
        set('completion', $user->profileCompleteness());
    }

    // setting side menu notifications for (task centre, job centre)
    if ($user->logged) {

        if (('xmlhttprequest' !== strtolower($_SERVER['HTTP_X_REQUESTED_WITH'] ?? '')) && $route['path'] !== "**" && $route['path'] !== "/workspace/:year/:month/:day" && $route['path'] !== "/workspace/task-centre/:section/:view" && $route['path'] !== "/js-mtp/:file" && $route['path'] !== "/ajax/skills.json") {
            $task_centre->overall_notifications();
        }

        if (('xmlhttprequest' !== strtolower($_SERVER['HTTP_X_REQUESTED_WITH'] ?? '')) && $route['path'] !== "**" && $route['path'] !== "/workspace/job-centre" && $route['path'] !== "/js-mtp/:file" && $route['path'] !== "/ajax/skills.json") {
            $job_centre->overall_notifications();
        }

        if (('xmlhttprequest' !== strtolower($_SERVER['HTTP_X_REQUESTED_WITH'] ?? '')) && $route['path'] !== "**" && $route['path'] !== "/js-mtp/:file" && $route['path'] !== "/ajax/skills.json") {
            set('notifications', $notification->notifications($user->info['id']));
            set('feedbacks', $feedback->feedbacks());
        }

    }
}

if ($user->logged) {
    if ($user->info['type'] == '1') {
        dispatch('/my-company-profile-signup', array($cms, 'myCompanyProfileSignup'));
        dispatch('/my-company-profile', array($cms, 'myCompanyProfile'));
        dispatch('/my-company-page', array($cms, 'myCompanyPage'));

        dispatch_post('/my-company-profile-signup', array($post, 'myCompanyProfileSignup'));
        dispatch_post('/my-company-profile', array($post, 'myCompanyProfileSignup'));
    }

    if ($user->info['type'] == '0') {
        dispatch('/personalize', array($cms, 'personalize'));
        dispatch('/my_profile', array($cms, 'myProfile'));
        dispatch('/my_cv', array($cms, 'myCV'));
        dispatch_post('/personalize', array($post, 'personalize'));
        dispatch_post('/my_profile', array($post, 'myProfile'));
        dispatch_post('/my_cv', array($post, 'myCV'));
        dispatch_post('/my_profile/employment/add', array($post, 'my_profile_employment_add'));
        dispatch_post('/my_profile/employment/edit', array($post, 'my_profile_employment_edit'));
        dispatch_post('/my_profile/employment/delete', array($post, 'my_profile_employment_delete'));
        dispatch_post('/my_profile/education/add', array($post, 'my_profile_education_add'));
        dispatch_post('/my_profile/education/edit', array($post, 'my_profile_education_edit'));
        dispatch_post('/my_profile/education/delete', array($post, 'my_profile_education_delete'));
    }

    if ($user->info['preference']['dashboard_mode'] === 'search') {
        dispatch('/dashboard', array($task_centre, 'tasks_listing'));
    } else {
        dispatch('/dashboard', array($cms, 'dashboard'));
    }

    dispatch('/dashboard/widget', array($cms, 'dashboardWidget'));
    dispatch_post('/dashboard', array($post, 'dashboard'));

    dispatch('/ajax/chat/content/:id/:task_id/:type', array($cms, 'ajaxChatContent'));
    dispatch('/ajax/chat/count/:id/:task_id/:type', array($cms, 'ajaxChatCount'));
    dispatch('/ajax/chat/send/:id/:task_id/:type', array($cms, 'ajaxChatSend'));
    dispatch('/ajax/chat/pin', array($cms, 'ajaxPinMessage'));
    dispatch('/ajax/chat/unpin/:id', array($cms, 'ajaxUnpinMessage'));
    dispatch('/ajax/chat/sidebar/:member_id/:task_id/:type', array($cms, 'ajaxChatSidebar'));

    dispatch_post('/ajax/chat/uploadAttachment/:id/:task_id/:type', array($post, 'ajaxUploadAttachment'));
    dispatch_post('/ajax/chat/sendMessageWithFiles/:id/:task_id/:type', array($post, 'sendMessageWithFiles'));
    dispatch_post('/ajax/chat/removeChatMesssage/:id', array($post, 'ajaxRemoveChatMesssage'));

    dispatch_post('/settings', array($post, 'settingsPage'));

    dispatch('/analytics', array($cms, 'analytics'));

    //Job Centre Routes
    dispatch('/workspace/job-centre', array($job_centre, 'job_centre'));

    dispatch('/workspace/billings/:page/:filter', [$task_centre, 'billings']);
    dispatch('/workspace/task-centre/:section/:view', array($task_centre, 'task_centre'));
    dispatch('/workspace/day', array($task_centre, 'task_centre'));
    dispatch('/workspace/week', array($task_centre, 'task_centre'));
    dispatch('/workspace/search/:page', array($task_centre, 'tasks_listing'));
    dispatch('/workspace/rating-centre', array($task_centre, 'rating_centre'));
    dispatch('/workspace/my-favourite', array($task_centre, 'my_favourite'));
    dispatch('/workspace/task/applicants/:task/:page', array($task_centre, 'applicants'));
    dispatch('/workspace/task/:task/answers/:user', array($task_centre, 'get_answers'));
    dispatch('/workspace/job/:job/answers/:user', array($job_centre, 'get_answers'));
    dispatch('/workspace/task/assign', array($task_centre, 'assign'));
    dispatch('/workspace/task/payment', array($task, 'payment'));
    dispatch('/workspace/task/viewed/:task/:user', array($task_centre, 'profile_viewed'));
    dispatch('/workspace/task/:task/details', array($task_centre, 'get_task'));
    dispatch('/workspace/task/:task/billing', array($task_centre, 'billing'));
    dispatch('/workspace/job/:job/details', array($task_centre, 'get_job'));
    dispatch('/workspace/talent/:id/details', array($task_centre, 'get_talent'));
    dispatch('/workspace/:task/attachment/:file', array($task, 'download_attachment'));
    dispatch('/workspace/:task/attachment/:file/delete', array($task, 'delete_attachment'));
    dispatch('/workspace/day/:year/:month/:day', array($task_centre, 'task_centre'));
    dispatch('/workspace/day/:year/:month', array($task_centre, 'task_centre'));
    dispatch('/workspace/week/:year/:month/:day', array($task_centre, 'task_centre'));
    dispatch('/workspace/month/:year/:month/:day', array($task_centre, 'task_centre'));
    dispatch('/workspace/:year/:month/:day', array($task_centre, 'task_centre'));

    dispatch('/talent/:id', [$task_centre, 'talent_details']);
    dispatch('/company/:id', [$cms, 'myCompanyPage']);
    dispatch('/notification/:id', [$notification, 'get_notification']);

    dispatch_post('/workspace/task/create', array($task, 'create'));
    dispatch_post('/workspace/task/apply', array($task, 'apply'));
    dispatch_post('/workspace/task/assign', array($task_centre, 'assign'));
    dispatch_post('/workspace/task/hire', array($task_centre, 'hire'));
    dispatch_post('/workspace/task/favourite', array($task, 'favourite'));
    dispatch_post('workspace/task/remove-expired-favourites', array($task, 'remove_expired_favourite'));
    dispatch_post('/workspace/task/upload', array($task, 'upload'));
    dispatch_post('/workspace/task/delete', array($task, 'delete'));
    dispatch_post('/workspace/task/cancel', array($task, 'cancel'));
    dispatch_post('/workspace/task/complete', array($task, 'complete'));
    dispatch_post('/workspace/task/accept', array($task, 'accept'));
    dispatch_post('/workspace/task/reject', array($task, 'reject'));
    dispatch_post('/workspace/task/offer', array($task_centre, 'offer'));
    dispatch_post('/workspace/task/dispute', array($task, 'dispute'));
    dispatch_post('/workspace/task/close', array($task, 'close'));
    dispatch_post('/workspace/task/request_payment', array($task, 'request_payment'));
    dispatch_post('/workspace/task/time_planning', array($task, 'time_planning'));
    dispatch_post('/workspace/task/clear_time_planning', [$task, 'clear_time_planning']);
    dispatch_post('/workspace/task/retract', [$task_centre, 'retract_offer']);
    dispatch_post('/workspace/task/retract_application', [$task_centre, 'retract_application']);
    dispatch_post('/workspace/job/create', array($job, 'create'));
    dispatch_post('/workspace/job/apply', array($job, 'apply'));
    dispatch_post('/workspace/job/assign', array($task_centre, 'assign'));
    dispatch_post('/workspace/job/favourite', array($job, 'favourite'));
    dispatch_post('workspace/job/remove-expired-favourites', array($job, 'remove_expired_favourite'));
    dispatch_post('/workspace/job/cancel', array($job, 'cancel'));
    dispatch_post('/workspace/job/delete', array($job, 'delete'));
    dispatch_post('/workspace/job/shortlist', array($job_centre, 'shortlist'));
    dispatch_post('/workspace/comments/create', array($task_centre, 'create_comment'));
    dispatch_post('/workspace/comments/hide', array($task_centre, 'hide_comment'));
    dispatch_post('/workspace/rating-centre/submit-review', array($task_centre, 'create_review'));
    dispatch_post('/workspace/notifications', [$notification, 'looked_at']);
    dispatch_post('/search/save', array($search, 'save'));
    dispatch_post('/search/delete', array($search, 'delete'));
    dispatch_post('/checkAvailability', [$cms, 'checkPublicCompanyNameAvailability']);
    dispatch_post('/workspace/milestone-progress', [$task_centre, 'milestone_progress']);
    dispatch_post('/workspace/task/applicant/pin', [$task_centre, 'pin_applicant']);
    dispatch_post('/workspace/task-centre/tasks/list', [$task_centre, 'ajaxGetTasksList']);

    // Feedback Routes
    dispatch('/feedback/:page', [$feedback, 'paginate']);
    dispatch('/feedback/:feedback/attachment/:file', [$feedback, 'download_attachment']);

    dispatch_post('/feedback', [$feedback, 'save']);
    dispatch_post('/feedback/upload', [$feedback, 'upload_attachment']);
    dispatch_post('/feedback/delete', [$feedback, 'delete_attachment']);

    dispatch('/logout', array($cms, 'logout'));
    dispatch('/account', array($cms, 'accountPage'));
    dispatch('/account/history', array($cms, 'accountHistoryPage'));
    dispatch('/cart', array($cms, 'cartPage'));
    dispatch_post('/account', array($post, 'accountUpdate'));

} else {
    dispatch('/dashboard', array($cms, 'login'));
    dispatch('/my-company-profile-signup', array($cms, 'login'));
    dispatch('/my-company-profile', array($cms, 'login'));
    dispatch('/my-company-page', array($cms, 'login'));
    dispatch('/settings', array($cms, 'login'));
    dispatch('/my_cv', array($cms, 'login'));
    dispatch('/personalize', array($cms, 'login'));
    dispatch('/analytics', array($cms, 'login'));
    dispatch('/my-company-profile-signup', array($cms, 'login'));
    dispatch('/my-company-profile', array($cms, 'login'));
    dispatch('/my-company-page', array($cms, 'login'));
    dispatch('/logout', array($cms, 'logout'));
    dispatch('/my_profile/**', array($cms, 'login'));
    dispatch('/workspace/**', array($cms, 'login'));
    dispatch('/analytics', array($cms, 'login'));
    dispatch('/talent/**', array($cms, 'login'));
    dispatch('/company/**', array($cms, 'login'));
    dispatch('/notification/**', array($cms, 'login'));
}

dispatch('/payment/return', array($task, 'paymentReturn'));
dispatch_post('/payment/return', array($task, 'paymentReturn'));
dispatch('/payment/callback', array($task, 'paymentCallback'));
dispatch_post('/payment/callback', array($task, 'paymentCallback'));

dispatch('/chats', array($cms, 'chats'));
dispatch('/sign_up', array($cms, 'sign_up'));
dispatch('/sign_up/success', array($cms, 'sign_up_success'));
dispatch('/search/:page', array($search, 'public_search'));
dispatch('/task-details-public', array($search, 'task_details_public'));
dispatch('/talents/search/:page', array($search, 'talent_public_search'));
dispatch('/login', array($cms, 'login'));
dispatch('/lang/:code', array($cms, 'selectLanguage'));
dispatch('/about', array($cms, 'about'));
dispatch('/faq', array($cms, 'faq'));

//newly added UI
dispatch('/talent_listing_public', array($cms, 'talent_listing_public'));
dispatch('/talent_details_public', array($cms, 'talent_details_public'));
dispatch('/task_details_public', array($cms, 'task_details_public'));
dispatch('/task_listing_public', array($cms, 'task_listing_public'));
dispatch('/home', array($cms, 'home'));
dispatch('/task_details', array($cms, 'task_details'));
dispatch('/billing_earning', array($cms, 'billing_earning'));
dispatch('/billing_spent', array($cms, 'billing_spent'));
dispatch('/bubble_category', array($cms, 'bubble_category'));
dispatch('/settings', array($cms, 'settingsPage'));
dispatch('/components', array($cms, 'components'));

dispatch('/forgot_password', array($cms, 'forgot_password'));
dispatch('/my_company_profile', array($cms, 'my_company_profile'));
dispatch('/my_rating_centre', array($cms, 'my_rating_centre'));
dispatch('/report', array($cms, 'report'));
dispatch('/password_reset/:id', array($cms, 'password_reset'));
dispatch('/set_password/:id', array($cms, 'set_password'));
dispatch('/sendgrid', array($cms, 'sendgrid'));
dispatch('/preference', array($cms, 'preference'));

dispatch_post('/my_company_profile', array($post, 'my_company_profile'));
dispatch_post('/login', array($post, 'login'));
dispatch_post('/password_reset/:id', array($post, 'password_reset'));
dispatch_post('/set_password/:id', array($post, 'password_set'));
dispatch_post('/forgot_password', array($post, 'forgot_password'));
dispatch_post('/sign_up_individual', array($post, 'sign_up_individual'));
dispatch_post('/sign_up_company', array($post, 'sign_up_company'));
dispatch_post('/sign_up/success', array($cms, 'sign_up_success'));
dispatch_post('/search/savelocation', array($search, 'save_location'));
dispatch_post('/nearest/more', [$cms, 'getMoreNearestTaskPublic']);
dispatch_post('/recent/more', [$cms, 'getMoreRecentTaskPublic']);
dispatch_post('/on-site/more', [$cms, 'getMoreOnSiteTaskPublic']);
dispatch_post('/from-home/more', [$cms, 'getMoreHomeTaskPublic']);
dispatch_post('/featured/more', [$cms, 'getMoreTaskPublic']);
dispatch_post('/feedback', [$feedback, 'save']);


dispatch('/js-mtp/:file', array($cms, 'javascript'));

dispatch('/', array($cms, 'home'));
dispatch('/cat/:id/:page', array($cms, 'productsByCat'));
dispatch('/product/:id', array($cms, 'productById'));
dispatch('/online-store', array($cms, 'onlineStore'));
dispatch('/online-store/cat/:id/:page', array($cms, 'productsOnlineByCat'));
dispatch('/online-store/product/:id', array($cms, 'productOnlineById'));
dispatch('/register', array($cms, 'register'));

dispatch('/forgot/:token', array($cms, 'forgot'));
dispatch('/activate/:token', array($cms, 'activate'));
dispatch('/ajax/category/:id/:type', array($cms, 'getCategoriesAjax'));
dispatch('/ajax/country/:id', array($cms, 'getStates'));
dispatch('/ajax/cities/:state_id', array($cms, 'getCities'));
dispatch('/ajax/skills.json', array($cms, 'ajaxSkillList'));
dispatch('/ajax/interests.json', array($cms, 'ajaxInterestsList'));
dispatch('/ajax/interests/:id', array($cms, 'getSubInterests'));
dispatch('/ajax/skills/:id', array($cms, 'getSubSkills'));
dispatch('/ajax/total/opportunities', array($cms, 'ajaxTotalOpportunities'));

dispatch('/states/:country_id', array($task_centre, 'getStates'));
dispatch('/contactForm', array($cms, 'contactForm'));
dispatch('/login/fb', array($cms, 'fbLogin'));
dispatch('/login/fb/callback', array($cms, 'fbLoginCallback'));
dispatch('/login/fbCheck', array($cms, 'fbCheck'));
dispatch('/login/google', array($cms, 'googleLogin'));
dispatch('/login/googleCheck', array($cms, 'googleCheck'));
dispatch('/:page', array($cms, 'page'));

dispatch_post('/register', array($post, 'register'));
dispatch_post('/forgot', array($post, 'forgot'));
dispatch_post('/forgot/:token', array($post, 'resetPassword'));
dispatch_post('/product/:id/enquiry', array($post, 'productEnquiry'));
dispatch_post('/up/:type', array($post, '_upload'));

// Routes for cronjobs
dispatch('jobs/cron', [$job, 'cron']);
dispatch('chats/cron', [$notification, 'chat_cron']);
dispatch('profile/reminder1', [$cron, 'completeProfileFirstReminder']);
dispatch('profile/reminder2', [$cron, 'completeProfileSecondReminder']);
dispatch('profile/expandyourcapabilities', [$cron, 'expandYourCapabilities']);
dispatch('tasks/skillbased', [$cron, 'newTasksSkillBased']);
dispatch('tasks/newurgent', [$cron, 'newUrgentTasks']);
dispatch('scheduled/emails', [$cron, 'scheduledEmails']);

function route_missing($request_method, $request_uri){
    global $cms;
    set('settings', $cms->settings());
    set('not_found', true);
    set('title', 'Page Not Found');
    set('style', ' style="background-color:#ffffff" class="page-404" ');
    echo render('404.php', 'layout/default.php');
}

//dispatch('**', array($cms, 'not_found'));

function not_found($errno, $errstr, $errfile = null, $errline = null){
    set('errno', $errno);
    set('errstr', $errstr);
    set('errfile', $errfile);
    set('errline', $errline);
    return render('layout/login.php');
}

run();
?>