<!DOCTYPE html>
<html lang="<?php echo !isset($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) || ($_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] == 'EN') ? 'en':'ms' ?>">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <title>My Rating Centre | Make Time Pay</title>
    
    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet" />
    <link href="assets/css/glyphicons.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    
  
    
    <!-- CSS Files -->
    <link href="assets/css/animate.min.css" rel="stylesheet">
    <link href="assets/css/bootstrap-dropdownhover.min.css" rel="stylesheet">
    
    <link href="assets/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="assets/css/dropzone.min.css" rel="stylesheet">
    <link href="assets/css/swiper.min.css" rel="stylesheet">
    <link href="assets/css/typeahead.css?v=1.0.1" rel="stylesheet" />
    <link href="assets/css/bootstrap-slider.min.css?v=1.0.1" rel="stylesheet" />


</head>

<body class="white-content od-mode">
    <div class="wrapper">
        <div class="sidebar">
            <div class="sidebar-wrapper">
                <div class="logo">
                    <img src="assets/img/logo.png" alt="" /> </div>
                <ul class="nav top">
                    <li>
                        <a href="dashboard.html">
                            <i class="tim-icons icon-components"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li>
                        <a href="task-centre.html">
                            <i class="tim-icons icon-calendar-60"></i>
                            <p>Task Centre</p>
                        </a>
                    </li>
                    <!--<li class="nav-item dropdown dropright">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="tim-icons icon-badge"></i>
                            <p>My Folio</p>
                            <span class="dropright-chevron">
                                <svg class="bi bi-chevron-right" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                  <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
                                </svg>
                            </span>
                        </a>
                        <div class="dropdown-menu dropright-menu" aria-labelledby="navbarDropdown1">
                            <a class="dropdown-item sub-dropright" href="#">My Profile</a>
                            <a class="dropdown-item sub-dropright" href="#">My CV</a>
                            <a class="dropdown-item sub-dropright" href="#">Task History</a>
                            <a class="dropdown-item sub-dropright" href="#">Favourite Tasks</a>
                            <a class="dropdown-item sub-dropright" href="#">My Rating Centre</a>
                        </div>
                    </li>-->
                    <li>
                        <a href="chats.html">
                            <i class="tim-icons icon-chat-33"></i>
                            <p>Chat</p>
                        </a>
                    </li>
                    <!--<li class="nav-item dropdown dropright">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="tim-icons icon-paper"></i>
                            <p>Billings</p>
                            <span class="dropright-chevron">
                                <svg class="bi bi-chevron-right" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                  <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
                                </svg>
                            </span>
                        </a>
                        <div class="dropdown-menu dropright-menu" aria-labelledby="navbarDropdown2">
                            <a class="dropdown-item sub-dropright" href="billings-earning.html">Earning</a>
                            <a class="dropdown-item sub-dropright" href="billings-spent.html">Spent</a>
                        </div>
                    </li>-->
                    <li class="nav-item dropdown dropright">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="tim-icons icon-single-02"></i>
                            <p>Reports</p>
                            <span class="dropright-chevron">
                                <svg class="bi bi-chevron-right" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                  <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
                                </svg>
                            </span>
                        </a>
                        <div class="dropdown-menu dropright-menu" aria-labelledby="navbarDropdown3">
                            <a class="dropdown-item sub-dropright" href="report-dashboard.html">My Visibility</a>
                            <a class="dropdown-item sub-dropright" href="#">My Time Utilization</a>
                            <a class="dropdown-item sub-dropright" href="#">My Earnings</a>
                        </div>
                    </li>
                </ul>
                <!--<ul class="nav bottom">
                    <li>
                        <a href="#">
                            <i class="tim-icons icon-bell-55"></i>
                            <span class="notifaction-badge">3</span>
                            <p>Notifications</p>
                        </a>
                    </li>
                    <button class="profile">
                        <div class="tbl-photo">
                            <img src="assets/img/james.jpg" alt="Profile Photo">
                        </div>
                        <div class="tbl-name" title="Mohamad Syafiq Syazre">Mohamad Syafiq Syazre</div>
                    </button>
                </ul>-->

            </div>
        </div>
        <div class="main-panel">
            <!-- End Navbar -->
            <div class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-chart">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-12 col-header-filter">
                                        <h2 class="card-title">Test Bubble</h2>
                                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
                                            <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                            </svg>
                                        </button>
	                                    <div class="logo-sm">
		                                    <a href="<?php echo url_for('dashboard') ?>" class="logo-icon-link">
			                                    <span class="logo-icon-sm"><img src="<?php echo url_for('/assets/img/logoicon.png') ?>" alt=""></span>
		                                    </a>
	                                    </div>
                                        <ul class="col-account-header">
                                            <li class="col-right-flex col-post-task">
                                                <button type="button" class="btn-header-post" id="post_task_btn" data-toggle="modal" data-target="#post_task_od">
                                                    <span class="plus-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round">
                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                                        </svg>
                                                    </span>
                                                    <span class="plus-icon-lbl"><?php echo lang('header_post_a_task'); ?></span>
                                                </button>
                                                <button type="button" class="btn-header-post" id="post_job_btn" data-toggle="modal" data-target="#post_job_ft">
                                                    <span class="plus-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round">
                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                                        </svg>
                                                    </span>
                                                    <span class="plus-icon-lbl">Post a Job</span>
                                                </button>
                                            </li>
                                            <li class="col-right-flex col-search">
                                                <button class="btn btn-link btn-search" id="search-button" data-toggle="modal" data-target="#searchModal">
                                                    <span class="user-account-icon search-icon">
                                                        <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z" />
                                                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z" />
                                                        </svg>
                                                    </span>
                                                </button>
                                            </li>

                                            <li class="dropdown col-right-flex col-notification">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                    <span class="user-account-icon notification-icon">
                                                        <svg class="bi bi-bell-fill" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zm.995-14.901a1 1 0 1 0-1.99 0A5.002 5.002 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901z" />
                                                        </svg>
                                                    </span>
                                                    <span class="notifaction-badge">3</span>
                                                </a>
                                                <ul class="dropdown-menu dropdown-navbar">
                                                    <span class="noti-title">Notifications</span>
                                                    <ul class="nav nav-tabs nav-tabs-sm nav-header-tabs" id="noti_header_tabs" role="tablist">
                                                        <li class="nav-item nav-od-item">
                                                            <div class="nav-link active gtm-od-tab" id="noti_od_tab" data-toggle="tab" data-target="#noti_od" role="tab" aria-controls="noti_od" aria-selected="true">
                                                                <span class="nav-tabs-lbl" >On Demand</span>
                                                                <span class="notifaction-tabs-badge">9+</span>
                                                            </div>
                                                        </li>
                                                        <li class="nav-item nav-ft-item">
                                                            <div class="nav-link gtm-ft-tab" id="noti_ft_tab" data-toggle="tab" data-target="#noti_ft" role="tab" aria-controls="noti_Ft" aria-selected="true">
                                                                <span class="nav-tabs-lbl">Full Time</span>
                                                                <span class="notifaction-tabs-badge">3</span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content" id="noti_tab_contents">
                                                        <div class="tab-pane fade show active" id="noti_od" role="tabpanel" aria-labelledby="noti_od_tab">
                                                            <ul class="noti-list-container">
                                                                <li>
                                                                    <a href="#" class="noti-list-link" role="button" tabindex="0">
                                                                        <div class="header-profile-photo">
                                                                            <img src="assets/img/james.jpg" alt="Profile Photo">
                                                                        </div>
                                                                        <div class="noti-contents">
                                                                            <span class="noti-body"><span class="noti-name">James Stephens</span> marked the task as done</span>
                                                                            <span class="noti-time">A day ago</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="noti-list-link" role="button" tabindex="0">
                                                                        <div class="header-profile-photo">
                                                                            <img src="assets/img/mike.jpg" alt="Profile Photo">
                                                                        </div>
                                                                        <div class="noti-contents">
                                                                            <span class="noti-body"><span class="noti-name">Khalid Romero</span> commented on your task</span>
                                                                            <span class="noti-time">3 days ago</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="noti-list-link" role="button" tabindex="0">
                                                                        <div class="header-profile-photo">
                                                                            <img src="assets/img/emilyz.jpg" alt="Profile Photo">
                                                                        </div>
                                                                        <div class="noti-contents">
                                                                            <span class="noti-body"><span class="noti-name">Aubree Walsh</span> created a new task</span>
                                                                            <span class="noti-time">4 days ago</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="noti-list-link" role="button" tabindex="0">
                                                                        <div class="header-profile-photo">
                                                                            <img src="assets/img/james.jpg" alt="Profile Photo">
                                                                        </div>
                                                                        <div class="noti-contents">
                                                                            <span class="noti-body"><span class="noti-name">Nathan O'Neill</span> started a new chat with you</span>
                                                                            <span class="noti-time">A week ago</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <span class="noti-view-all">
                                                                <a href="#" class="noti-list-link" role="button" tabindex="0"><span class="noti-name">View all</span></a>
                                                            </span>
                                                        </div>
                                                        <div class="tab-pane fade" id="noti_ft" role="tabpanel" aria-labelledby="noti_ft">
                                                            <ul class="noti-list-container">
                                                                <li>
                                                                    <a href="#" class="noti-list-link" role="button" tabindex="0">
                                                                        <div class="header-profile-photo">
                                                                            <img src="assets/img/james.jpg" alt="Profile Photo">
                                                                        </div>
                                                                        <div class="noti-contents">
                                                                            <span class="noti-body"><span class="noti-name">James Stephens</span> marked the task as done</span>
                                                                            <span class="noti-time">A day ago</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="noti-list-link" role="button" tabindex="0">
                                                                        <div class="header-profile-photo">
                                                                            <img src="assets/img/mike.jpg" alt="Profile Photo">
                                                                        </div>
                                                                        <div class="noti-contents">
                                                                            <span class="noti-body"><span class="noti-name">Khalid Romero</span> commented on your task</span>
                                                                            <span class="noti-time">3 days ago</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="noti-list-link" role="button" tabindex="0">
                                                                        <div class="header-profile-photo">
                                                                            <img src="assets/img/emilyz.jpg" alt="Profile Photo">
                                                                        </div>
                                                                        <div class="noti-contents">
                                                                            <span class="noti-body"><span class="noti-name">Aubree Walsh</span> created a new task</span>
                                                                            <span class="noti-time">4 days ago</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="noti-list-link" role="button" tabindex="0">
                                                                        <div class="header-profile-photo">
                                                                            <img src="assets/img/james.jpg" alt="Profile Photo">
                                                                        </div>
                                                                        <div class="noti-contents">
                                                                            <span class="noti-body"><span class="noti-name">Nathan O'Neill</span> started a new chat with you</span>
                                                                            <span class="noti-time">A week ago</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <span class="noti-view-all">
                                                                <a href="#" class="noti-list-link" role="button" tabindex="0"><span class="noti-name">View all</span></a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </ul>
                                            </li>
                                            <!--<li class="dropdown col-right-flex col-settings">
                                                <a href="#">
                                                    <span class="user-account-icon settings-icon">
                                                        <svg class="bi bi-gear-fill" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 0 0-5.86 2.929 2.929 0 0 0 0 5.858z" />
                                                        </svg>
                                                    </span>
                                                </a>
                                            </li>-->
                                            <li class="dropdown col-right-flex col-account">
                                                <a href="#" class="dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                                                    <div class="header-profile-photo">
                                                        <img src="assets/img/james.jpg" alt="Profile Photo">
                                                    </div>
                                                </a>
                                                <ul class="dropdown-menu dropdown-navbar">
                                                    <li class="header-dropdown-link-list lang-switcher-list">
                                                        <a href="#" class="lang-switcher-link lang-switcher-link-en active"><span class="lang-switcher-en">EN</span></a>|<a href="#" class="lang-switcher-link lang-switcher-link-bm"><span class="lang-switcher-bm">BM</span></a>
                                                    </li>
                                                    <li class="header-dropdown-link-list dropdown-toggle-list">
                                                        <div class="btn-switch-container">
                                                            <label class="btn-switch btn-color-mode-switch">
                                                                <input type="checkbox" name="odft_mode" id="odft_mode" value="1">
                                                                <label for="odft_mode" data-on="Full Time" data-off="On Demand" class="btn-color-mode-switch-inner"></label>
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <li class="dropdown-divider"></li>
                                                    <li class="header-dropdown-link-list"><a href="my-profile.html" class="nav-item dropdown-item">My Profile</a></li>
                                                    <li class="header-dropdown-link-list"><a href="my-rating-centre.html" class="nav-item dropdown-item">My Rating Centre</a></li>
                                                    <li class="header-dropdown-link-list"><a href="billings-earning.html" class="nav-item dropdown-item">My Earning</a></li>
                                                    <li class="header-dropdown-link-list"><a href="billings-spent.html" class="nav-item dropdown-item">My Spent</a></li>
                                                    <li class="dropdown-divider"></li>
                                                    <li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Settings</a></li>
                                                    <li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Feedback</a></li>
                                                    <li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Help</a></li>
                                                    <li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Log out</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="modal modal-search fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        </div>
                                        <div class="modal-body">
                                            <div class="modal-input-filter-flex">
                                                <div class="form-group form-group-search has-icon">
                                                    <span class="search-modal-icon">
                                                        <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                                        </svg>
                                                    </span>
                                                    <input type="text" class="form-control search-input" placeholder="Search">
                                                    <span class="typeahead-close" title='clear the search'><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></span>
                                                </div>
                                                <button type="button" class="btn-icon-full btn-adv-search" id="btn_adv_search">
                                                    <span class="btn-label">Advanced Search</span>
                                                    <span class="btn-icon">
                                                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                        </svg>
                                                    </span>
                                                </button>
                                            </div>
                                            <div class="modal-advanced-filter-wrapper multi-collapse collapse">
                                                <div class="modal-advanced-filter-container">
                                                <div class="modal-search-filter-row row-filter-taskjob">
                                                    <label class="modal-search-filter-lbl">Filter by task or job</label>
                                                    <div class="form-block-filter form-block-filter-flex">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="filtertype_1">
                                                            <label class="custom-control-label" for="filtertype_1">Task</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="filtertype_2" >
                                                            <label class="custom-control-label" for="filtertype_2">Job</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-search-filter-row row-filter-location">
                                                    <label class="modal-search-filter-lbl">Filter by location</label>
                                                    <div class="form-block-filter form-block-filter-flex space-between">
                                                        <div class="form-filter-state">
                                                            <select class="form-control form-control-input" placeholder="State">
                                                                <option disabled="" selected="">State</option>
                                                                <option value="">Kuala Lumpur</option>
                                                                <option value="">Selangor</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-filter-city">
                                                            <select class="form-control form-control-input" placeholder="City">
                                                                <option disabled="" selected="">Town/City</option>
                                                                <option value="">Petaling Jaya</option>
                                                                <option value="">Shah Alam</option>
                                                                <option value="">Subang Jaya</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-search-filter-row row-filter-dateposted">
                                                    <label class="modal-search-filter-lbl">Filter by date posted</label>
                                                    <div class="form-block-filter form-block-filter-flex space-between">
                                                        <div class="form-filter-dateposted">
                                                            <select class="form-control form-control-input" placeholder="Posted Date">
                                                                <option disabled="" selected="">Date Posted</option>
                                                                <option value=""><?php echo lang('dashboard_filter_any_time'); ?></option>
                                                                <option value=""><?php echo lang('dashboard_filter_any_time'); ?></option>
                                                                <option value=""><?php echo sprintf(lang('public_search_last_x_hours'), 24); ?></option>
                                                                <option value=""><?php echo sprintf(lang('public_search_last_x_days'), 3); ?></option>
                                                                <option value=""><?php echo sprintf(lang('public_search_last_x_days'), 7); ?></option>
                                                                <option value=""><?php echo sprintf(lang('public_search_last_x_days'), 14); ?></option>
                                                                <option value=""><?php echo sprintf(lang('public_search_last_x_days'), 30); ?></option>
                                                                <option value=""><?php echo lang('public_search_custom_range'); ?></option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group form-block-filter-flex space-between form-group-customrange">
                                                            <div class="form-filter-customrange-from">
                                                                <div class="input-group input-group-datetimepicker date" id="form_datefilterfrom" data-target-input="nearest">
                                                                    <input type="text" class="form-control datetimepicker-input" data-target="#form_datefilterfrom" placeholder="From Date"/>
                                                                    <span class="input-group-addon" data-target="#form_datefilterfrom" data-toggle="datetimepicker">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-filter-customrange-to">
                                                                <div class="input-group input-group-datetimepicker date" id="form_datefilterto" data-target-input="nearest">
                                                                    <input type="text" class="form-control datetimepicker-input" data-target="#form_datefilterto" placeholder="To Date"/>
                                                                    <span class="input-group-addon" data-target="#form_datefilterto" data-toggle="datetimepicker">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-search-filter-row row-filter-category">
                                                    <label class="modal-search-filter-lbl">Filter by category/subcategory</label>
                                                    <div class="form-block-filter form-block-filter-flex space-between">
                                                        <div class="form-filter-category">
                                                            <select class="form-control form-control-input" placeholder="Category">
                                                                <option disabled="" selected="">Category</option>
                                                                <option value="">Option 1</option>
                                                                <option value="">Option 2</option>
                                                                <option value="">Option 3</option>
                                                                <option value="">Option 4</option>
                                                                <option value="">Option 5</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-filter-subcategory">
                                                            <select class="form-control form-control-input" placeholder="Sub Category">
                                                                <option disabled="" selected="">Sub Category</option>
                                                                <option value="">Option 1</option>
                                                                <option value="">Option 2</option>
                                                                <option value="">Option 3</option>
                                                                <option value="">Option 4</option>
                                                                <option value="">Option 5</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
                                                <div class="modal-filter-clear">
                                                    <a href="#" class="deflink clear-filter-link">Clear all filters</a>
                                                </div>
                                                <div class="modal-filter-action form-block-filter-flex">
                                                    <button type="button" class="btn-icon-full btn-cancel">
                                                        <span class="btn-label">Cancel</span>
                                                        <span class="btn-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                        </span>
                                                    </button>
                                                    <button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                                                        <span class="btn-label">Apply</span>
                                                        <span class="btn-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs"><polyline points="20 6 9 17 4 12"></polyline></svg>
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body-ratingcentre">
                                <ul class="nav nav-pills nav-pills-ratingcentre" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-rc-myreviews-tab" data-toggle="pill" href="#pills-rc-myreviews" role="tab" aria-controls="pills-rc-myreviews" aria-selected="true">My Reviews</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-rc-writereview-tab" data-toggle="pill" href="#pills-rc-writereview" role="tab" aria-controls="pills-rc-writereview" aria-selected="false">Write a Review</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-rc-myreviews" role="tabpanel">
                                        <div class="col-xl-12">
                                            <div class="row">
                                                <div class="col-xl-12 col-ratingcentre-top">
                                                    <div class="col-ratingcentre-top-wrapper">
                                                        <div class="row">
                                                            <div class="col-xl-12 col-ratingcentre">
                                                                <label>Product:</label>
                                                                <select id="product" class="form-control form-control-input product" name="product" data-related-regime="#regime">
                                                                    <option>-- Product --</option>
                                                                </select>
                                                                <div class="product_v2 product" name="product" data-related-regime="#regime"></div>
                                                                <br />
                                                                <label>Regime:</label>
                                                                <select id="regime" class="form-control form-control-input regime" name="regime" data-related-category="#category"
                                                                    data-related-product="#product">
                                                                    <option>-- Select --</option>
                                                                </select>
                                                                <div class="regime_v2" name="regime" data-related-category="#category" data-related-product="#product"></div>
                                                                <br />
                                                                <label>Category:</label>
                                                                <select id="category" class="form-control form-control-input category" name="category">
                                                                    <option>-- Select --</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade show" id="pills-rc-writereview" role="tabpanel">
                                        <div class="col-xl-12">
                                            <div class="row">
                                                <div class="col-xl-12 col-ratingcentre-top">
                                                    <div class="col-ratingcentre-top-wrapper">
                                                        <div class="row">
                                                            <div class="col-xl-12 col-ratingcentre">
                                                                Write a Review content
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <!-- Modal - OD Post Task Steps -->
    <div id="post_task_od" class="modal modal-steps fade" aria-labelledby="post_task_od" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target="#modal_confirm_od" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                </button>
                <div class="modal-body">
                    <div class="modal-form-wrapper">
                        <form name="form_post_task_od" id="form_post_task_od" method="post" action="">

                            <div id="sf1" class="frm modal-step-flex">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 1 of 4: Select Category</div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Category" id="od_category" name="od_category">
                                                <option disabled selected>Category</option>
                                                <option value="1" >Option 1</option>
                                                <option value="2" >Option 2</option>
                                                <option value="3" >Option 3</option>
                                                <option value="4" >Option 4</option>
                                                <option value="5" >Option 5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Sub Category" id="od_subcategory" name="od_subcategory">
                                                <option disabled selected>Sub Category</option>
                                                <option value="1" >Option 1</option>
                                                <option value="2" >Option 2</option>
                                                <option value="3" >Option 3</option>
                                                <option value="4" >Option 4</option>
                                                <option value="5" >Option 5</option>
                                            </select>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-next od-open1">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
                                            </span>
                                        </button>
                                    </div>            
                                </div>
                                
                            </div>

                            <div id="sf2" class="frm modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 2 of 4: Task Details</div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-input" placeholder="Task Title" id="od_tasktitle" name="od_tasktitle">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control form-control-input" placeholder="Task Description" id="od_taskdesc" name="od_taskdesc" rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <div class="dropzone dropzone-previews" id="my-awesome-dropzone"></div>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev od-back2">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next od-open2">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div id="sf3" class="frm modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 3 of 4: Task Estimates</div>
                                        <div class="form-group">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Nature of Task</span>
                                            </label>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="not_bythehour" name="defaultExampleRadios">
                                                <label class="custom-control-label" for="not_bythehour">By the hour</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="not_lumpsum" name="defaultExampleRadios">
                                                <label class="custom-control-label" for="not_lumpsum">Lump sum, complete by deadline</label>
                                            </div>
                                        </div>

                                        <div class="form-check-selection" id="form_byhour" style="display: none;">
                                            <div class="form-group form-estimated-hours">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Estimated hours</span>
                                                </label>
                                                <div class="form-flex">
                                                    <input type="number" name="" class="form-control form-control-input" min="0" id="" />
                                                    <span class="postlbl-hours">hours</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-estimated-hours">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Budget Per Hour</span>
                                                </label>
                                                <div class="form-flex">
                                                    <span class="estimated-lbl estimated-from">RM50</span>
                                                    <input id="range_budget_hour" type="text" /> 
                                                    <span class="estimated-lbl estimated-to">RM100</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-total-project-budget">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Total Project Budget</span>
                                                </label>
                                                <div class="form-flex">
                                                    <span class="postlbl-cur">RM150 - RM300</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-complete-by">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Complete By</span>
                                                </label>
                                                <div class="form-group">
                                                    <div class="input-group input-group-datetimepicker date" id="form_datetime_hour" data-target-input="nearest">
                                                        <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_hour" />
                                                        <span class="input-group-addon" data-target="#form_datetime_hour" data-toggle="datetimepicker">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="form-check-selection" id="form_bylumpsum" style="display: none;">
                                            <div class="form-group form-estimated-hours">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Budget For Task</span>
                                                </label>
                                                <div class="form-flex">
                                                    <span class="estimated-lbl estimated-from">RM200</span>
                                                    <input id="range_budget_lumpsum" type="text" /> 
                                                    <span class="estimated-lbl estimated-to">RM300</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-complete-by">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Complete By</span>
                                                </label>
                                                <div class="form-group">
                                                    <div class="input-group input-group-datetimepicker date" id="form_datetime_lumpsum" data-target-input="nearest">
                                                        <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_lumpsum" />
                                                        <span class="input-group-addon" data-target="#form_datetime_lumpsum" data-toggle="datetimepicker">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <!--<legend>Step 3 of 4</legend>
                                        <div class="form-group house-type-container">
                                            <div class="">
                                                <select class="form-control" name="house_type" id="sel1 house_type">
                                                    <option value="" class="select-disabled-type" selected disabled>What is your house type</option>
                                                    <option value="Apartment/Flat">Apartment/Flat</option>
                                                    <option value="Condo/Serivce Residence">Condo/Service Residence</option>
                                                    <option value="Terrace/Townhouse">Terrace/Townhouse</option>
                                                    <option value="Semi-D/Bungalow">Semi-D/Bungalow</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group house-size-container">
                                            <span class="form-label-house-size">What is your house size (sq.ft.):</span>

                                            <div class="">
                                                <input id="house_size" name="house_size" data-slider-id='ex2Slider' type="text" data-slider-min="0" data-slider-max="10000" data-slider-step="50" data-slider-value="800" />
                                            </div>
                                        </div>
    -->
                                        <div class="clearfix" style="height: 10px;clear: both;"></div>



                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev od-back3">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next od-open3">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div id="sf4" class="frm modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>

                                        <div class="modal-steps-title">Step 4 of 4: Seeker Shortlisting</div>

                                        <div class="form-group form-important-skills">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Important skills for task</span>
                                            </label>
                                            <div class="bs-example">
                                                <input type="text" name="tags" value="Tag1,Tag2,Tag3" data-role="tagsinput" />
                                            </div>
                                        </div>
                                        <div class="form-group form-qna">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Questions</span>
                                            </label>
                                            <div class="od-qna-field-wrapper">
                                                <div class="qna-wrapper od-qna-wrapper ">
                                                   <div class="form-flex">
                                                        <span class="qna-lbl od-qna-lbl">1.</span>
                                                        <input type="text" name="od_qna_field[]" class="form-control form-control-input"  placeholder="Question"/>
                                                    </div>
                                                    
                                                </div>
                                                <a href="javascript:void(0);" class="od-add-button" title="Add field">Add More</a>
                                            </div>
                                            
                                        </div>




                                        <!--<legend>Step 4 of 4</legend>

                                        <div class="form-group house-rental-container">
                                            <span class="form-label-house-rental">Choose one:</span>
                                            <div class="">
                                                <div data-toggle="buttons">
                                                    <div class="btn-group">
                                                        <label class="btn btn-signum-label">
                                                            <input type="radio" name="house_rental" id="house_rental" value="Own Stay" class="sr-only" required>Own Stay</label>
                                                        <label class="btn btn-signum-label">
                                                            <input type="radio" name="house_rental" id="house_rental" value="Rental" class="sr-only" required>Rental</label>
                                                        <label class="btn btn-signum-label">
                                                            <input type="radio" name="house_rental" id="house_rental" value="Airbnb" class="sr-only" required>Airbnb</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group house-location-container">
                                            <span class="form-label-house-location">Where is location of your house:</span>
                                            <div class="">
                                                <input type="text" placeholder="Eg: Damansara" id="house_location" name="house_location" class="form-control" autocomplete="off">
                                            </div>
                                        </div>

                                        <div class="form-group budget-container">
                                            <span class="form-label-budget">What is your budget (RM):</span>
                                            <div class="budget-range-slider">
                                                <input id="budget" name="budget" type="text" data-slider-ticks="[50000, 100000, 150000]" data-slider-ticks-snap-bounds="30" data-slider-ticks-labels='["< RM50k", "RM100k", "> RM100k"]' />
                                            </div>
                                        </div>-->


                                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                                        <!--<div class="g-recaptcha" data-sitekey="6LefLk4UAAAAAJJqnKhJX904t7Wg6FTTWOJMGgsI"></div>-->
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev od-back4">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/></svg>
                                            </span>
                                        </button>
                                        <button type="submit" class="btn-icon-full btn-step-submit">
                                            <span class="btn-label">Submit</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    
    
    <!-- Modal - FT Post Job Steps -->
    <div id="post_job_ft" class="modal modal-steps fade" aria-labelledby="post_job_ft" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target="#modal_confirm_ft" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                </button>
                <div class="modal-body">
                    <div class="modal-form-wrapper">
                        <form name="form_post_job_ft" id="form_post_job_ft" method="post" action="">

                            <div id="ft_sf1" class="frm-ft modal-step-flex">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 1 of 5: Select Category</div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Category" id="ft_category" name="ft_category">
                                                <option disabled selected>Category</option>
                                                <option value="1" >Option 1</option>
                                                <option value="2" >Option 2</option>
                                                <option value="3" >Option 3</option>
                                                <option value="4" >Option 4</option>
                                                <option value="5" >Option 5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Sub Category" id="ft_subcategory" name="ft_subcategory">
                                                <option disabled selected>Sub Category</option>
                                                <option value="1" >Option 1</option>
                                                <option value="2" >Option 2</option>
                                                <option value="3" >Option 3</option>
                                                <option value="4" >Option 4</option>
                                                <option value="5" >Option 5</option>
                                            </select>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-next ft-open1">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
                                            </span>
                                        </button>
                                    </div>            
                                </div>
                                
                            </div>

                            <div id="ft_sf2" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 2 of 5: The Job</div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-input" placeholder="Job Title" id="ft_jobtitle" name="ft_jobtitle">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control form-control-input" placeholder="Job Description" id="ft_jobdesc" name="ft_jobdesc" rows="3"></textarea>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back2">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next ft-open2">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div id="ft_sf3" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 3 of 5: The Applicant</div>
                                        
                                        <div class="form-group">
                                            <textarea class="form-control form-control-input" placeholder="The Requirements" id="ft_jobrequirement" name="ft_jobrequirement" rows="3"></textarea>
                                        </div>
                                        
                                        <div class="form-group form-qna">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Questions</span>
                                            </label>
                                            <div class="ft-qna-field-wrapper">
                                                <div class="qna-wrapper ft-qna-wrapper ">
                                                   <div class="form-flex">
                                                        <span class="qna-lbl ft-qna-lbl">1.</span>
                                                        <input type="text" name="ft_qna_field[]" class="form-control form-control-input"  placeholder="Question"/>
                                                    </div>
                                                    
                                                </div>
                                                <a href="javascript:void(0);" class="ft-add-button" title="Add field">Add More</a>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Experience Level" id="ft_jobxp" name="ft_jobxp">
                                                <option disabled selected>Experience Level</option>
                                                <option value="1" >Option 1</option>
                                                <option value="2" >Option 2</option>
                                                <option value="3" >Option 3</option>
                                                <option value="4" >Option 4</option>
                                                <option value="5" >Option 5</option>
                                            </select>
                                        </div>
                                        


                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back3">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next ft-open3">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="ft_sf4" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 4 of 5: The Location</div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Country" id="ft_jobcountry" name="ft_jobcountry">
                                                <option disabled selected>Country</option>
                                                <option value="1" >Malaysia</option>
                                                <option value="2" >Singapore</option>
                                                <option value="3" >Thailand</option>
                                                <option value="4" >Option 4</option>
                                                <option value="5" >Option 5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="State" id="ft_jobstate" name="ft_jobstate">
                                                <option disabled selected>State</option>
                                                <option value="Johor">Johor</option>
                                                <option value="Kedah">Kedah</option>
                                                <option value="Kelantan">Kelantan</option>
                                                <option value="Kuala Lumpur">Kuala Lumpur</option>
                                                <option value="Labuan">Labuan</option>
                                                <option value="Malacca">Melaka</option>
                                                <option value="Negeri Sembilan">Negeri Sembilan</option>
                                                <option value="Pahang">Pahang</option>
                                                <option value="Perak">Perak</option>
                                                <option value="Perlis">Perlis</option>
                                                <option value="Penang">Penang</option>
                                                <option value="Sabah">Sabah</option>
                                                <option value="Sarawak">Sarawak</option>
                                                <option value="Selangor">Selangor</option>
                                                <option value="Terengganu">Terengganu</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-input" placeholder="Job Location" id="ft_joblocation" name="ft_joblocation">
                                        </div>
                                        


                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back4">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next ft-open4">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            
                            

                            <div id="ft_sf5" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>

                                        <div class="modal-steps-title">Step 5 of 5: Remuneration</div>
                                        <div class="form-group form-salary-range">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Salary Range</span>
                                            </label>
                                            <div class="form-flex">
                                                <span class="estimated-lbl estimated-from">RM1200</span>
                                                <input id="range_budget_salary" type="text" /> 
                                                <span class="estimated-lbl estimated-to">RM12000</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-benefits">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Benefits</span>
                                            </label>
                                            <div class="form-block">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_1">
                                                    <label class="custom-control-label" for="benefits_1">Medical</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_2">
                                                    <label class="custom-control-label" for="benefits_2">Parking</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_3">
                                                    <label class="custom-control-label" for="benefits_3">Claimable expenses</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_4">
                                                    <label class="custom-control-label" for="benefits_4">Dental</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_5">
                                                    <label class="custom-control-label" for="benefits_5">Meals</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_6">
                                                    <label class="custom-control-label" for="benefits_6">Pantry</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_7">
                                                    <label class="custom-control-label" for="benefits_7">Commission</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back5">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/></svg>
                                            </span>
                                        </button>
                                        <button type="submit" class="btn-icon-full btn-step-submit">
                                            <span class="btn-label">Submit</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    
    
    <!-- Modal - OD Confirm Discard -->
    <div id="modal_confirm_od" class="modal modal-confirm fade" aria-labelledby="modal_confirm_od" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p class="modal-para">This will discard all the detail you have entered.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_task_od" >
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                        </span>
                    </button>
                    <button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                        <span class="btn-label">Confirm</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs"><polyline points="20 6 9 17 4 12"></polyline></svg>
                        </span>
                    </button>
                </div>
            </div>
         </div>
    </div>
    <!-- /.modal -->
    
    <!-- Modal - FT Confirm Discard -->
    <div id="modal_confirm_ft" class="modal modal-confirm fade" aria-labelledby="modal_confirm_ft" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p class="modal-para">This will discard all the detail you have entered.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_job_ft" >
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                        </span>
                    </button>
                    <button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                        <span class="btn-label">Confirm</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs"><polyline points="20 6 9 17 4 12"></polyline></svg>
                        </span>
                    </button>
                </div>
            </div>
         </div>
    </div>
    <!-- /.modal -->
    
    
    
    
    
    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.min.js"></script>
    <script src="assets/js/core/jquery-ui.min.js"></script>
    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>
    
    <!--   Plugins JS Files   -->
    <script src="assets/js/plugins/moment-with-locales.min.js"></script>
    <script src="assets/js/plugins/tempusdominus-bootstrap-4.min.js"></script>
    <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/js/plugins/dragscroll.js"></script>
    <script src="assets/js/plugins/bootstrap-tagsinput.min.js"></script>
    <script src="assets/js/plugins/swiper.min.js"></script>
    <script src="assets/js/plugins/bootstrap-slider.min.js"></script>
    <script src="/assets/js/plugins/jquery.validate.js"></script>
    
    <!--  Search Suggestion Plugin    -->
    <script src="assets/js/plugins/typeahead.bundle.min.js"></script>
    <script src="assets/js/plugins/bloodhound.js"></script>
    <script src="assets/js/plugins/handlebars.js"></script>
    
    <!-- Chart JS -->
    <script src='assets/js/plugins/snap.svg-min.js'></script>
    <script src="assets/js/plugins/chart.min.js"></script>
    <script src="assets/js/plugins/progressbar.js"></script>
    <script src="assets/js/plugins/d3.v3.min.js"></script>
    
    <!--  Notifications Plugin    -->
    <script src="assets/js/plugins/bootstrap-notify.js"></script>
    
    <!--  Custom JS    -->
    <script src="assets/js/plugins/dropzone.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/bubble.js"></script>

    
    <!-- Bar Chart Function -->
    <script>

        
        // Bar Chart Rounded Corner custom
        Chart.elements.Rectangle.prototype.draw = function() {
    
            var ctx = this._chart.ctx;
            var vm = this._view;
            var left, right, top, bottom, signX, signY, borderSkipped, radius;
            var borderWidth = vm.borderWidth;
            // Set Radius Here
            // If radius is large enough to cause drawing errors a max radius is imposed
            var cornerRadius = 12;

            if (!vm.horizontal) {
                // bar
                left = vm.x - vm.width / 2;
                right = vm.x + vm.width / 2;
                top = vm.y;
                bottom = vm.base;
                signX = 1;
                signY = bottom > top? 1: -1;
                borderSkipped = vm.borderSkipped || 'bottom';
            } else {
                // horizontal bar
                left = vm.base;
                right = vm.x;
                top = vm.y - vm.height / 2;
                bottom = vm.y + vm.height / 2;
                signX = right > left? 1: -1;
                signY = 1;
                borderSkipped = vm.borderSkipped || 'left';
            }

            // Canvas doesn't allow us to stroke inside the width so we can
            // adjust the sizes to fit if we're setting a stroke on the line
            if (borderWidth) {
                // borderWidth shold be less than bar width and bar height.
                var barSize = Math.min(Math.abs(left - right), Math.abs(top - bottom));
                borderWidth = borderWidth > barSize? barSize: borderWidth;
                var halfStroke = borderWidth / 2;
                // Adjust borderWidth when bar top position is near vm.base(zero).
                var borderLeft = left + (borderSkipped !== 'left'? halfStroke * signX: 0);
                var borderRight = right + (borderSkipped !== 'right'? -halfStroke * signX: 0);
                var borderTop = top + (borderSkipped !== 'top'? halfStroke * signY: 0);
                var borderBottom = bottom + (borderSkipped !== 'bottom'? -halfStroke * signY: 0);
                // not become a vertical line?
                if (borderLeft !== borderRight) {
                    top = borderTop;
                    bottom = borderBottom;
                }
                // not become a horizontal line?
                if (borderTop !== borderBottom) {
                    left = borderLeft;
                    right = borderRight;
                }
            }

            ctx.beginPath();
            ctx.fillStyle = vm.backgroundColor;
            ctx.strokeStyle = vm.borderColor;
            ctx.lineWidth = borderWidth;

            // Corner points, from bottom-left to bottom-right clockwise
            // | 1 2 |
            // | 0 3 |
            var corners = [
                [left, bottom],
                [left, top],
                [right, top],
                [right, bottom]
            ];

            // Find first (starting) corner with fallback to 'bottom'
            var borders = ['bottom', 'left', 'top', 'right'];
            var startCorner = borders.indexOf(borderSkipped, 0);
            if (startCorner === -1) {
                startCorner = 0;
            }

            function cornerAt(index) {
                return corners[(startCorner + index) % 4];
            }

            // Draw rectangle from 'startCorner'
            var corner = cornerAt(0);
            ctx.moveTo(corner[0], corner[1]);

            for (var i = 1; i < 4; i++) {
                corner = cornerAt(i);
                nextCornerId = i+1;
                if(nextCornerId == 4){
                    nextCornerId = 0
                }

                nextCorner = cornerAt(nextCornerId);

                width = corners[2][0] - corners[1][0];
                height = corners[0][1] - corners[1][1];
                x = corners[1][0];
                y = corners[1][1];

                var radius = cornerRadius;

                // Fix radius being too large
                if(radius > height/2){
                    radius = height/2;
                }if(radius > width/2){
                    radius = width/2;
                }

                ctx.moveTo(x + radius, y);
                ctx.lineTo(x + width - radius, y);
                ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
                ctx.lineTo(x + width, y + height - radius);
                ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
                ctx.lineTo(x + radius, y + height);
                ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
                ctx.lineTo(x, y + radius);
                ctx.quadraticCurveTo(x, y, x + radius, y);

            }

            ctx.fill();
            if (borderWidth) {
                ctx.stroke();
            }
        };
        // Bar Chart Rounded Corner custom
        
        window.onload = function() {
            var ctx = document.getElementById('barchart_canvas').getContext('2d');
            var gradient = ctx.createLinearGradient(0, 0, 0, 400);
                gradient.addColorStop(0, 'rgba(255,133,99,1)');   
                gradient.addColorStop(1, 'rgba(237,89,43,1)');
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
                    datasets: [{
                        data: [7, 11, 20, 19, 14, 10, 8],
                        backgroundColor: [
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                        ],
                        barPercentage: 0.5,
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false,
                    },
                    title: {
                        display: false,
                    },
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display:false,
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                borderDash: [8, 4],
                                color: "#eeeeee",
                                drawBorder: false,
                                lineWidth: 1,
                            },
                            ticks: {
                                display:false,
                                drawBorder: false
                            },
                        }]
                    }
                }
            });

        };
    </script>
    <!-- Bar Chart Function -->
    <script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({
                    locale: 'ru'
                });
            });
        </script>
    <!-- Donut Chart Function -->
    <script>
        
        var doughnut = document.getElementById("doChart");
        var myDoughnutChart = new Chart(doughnut, {
            type: 'doughnut',
            data: {
            labels:["Credit Card","Bank Transfer"],
            datasets: [{
                data: [4100, 2500],
                backgroundColor: ['#3644ad','#f0724c'],
                borderColor: ['#3644ad','#f0724c'],
                hoverBorderWidth: 10,
                hoverRadius: 1,
                borderWidth: 0,
             }]
           },
          options: {
              defaultFontFamily: Chart.defaults.global.defaultFontFamily = "'Poppins'",
              legend: {display: false,position: 'bottom'},
              cutoutPercentage: 70,
              maintainAspectRatio: false,
              responsive: true,
              tooltips: {
                  callbacks: {
                    label: function(tooltipItem, data) {
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                      var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                        return previousValue + currentValue;
                      });
                      var currentValue = dataset.data[tooltipItem.index];
                      var precentage = Math.floor(((currentValue/total) * 100)+0.5);         
                      return precentage + "%";
                    }
                  }
                }
          }
        });
        
    </script>
    <!-- Donut Chart Function -->

    <!-- Progress Bar Function -->
    <script>
        // progressbar.js@1.0.0 version is used
        // Docs: https://progressbarjs.readthedocs.org/en/1.0.0/

        var bar1 = new ProgressBar.Line(progress_1, {
          strokeWidth: 1,
          easing: 'easeInOut',
          duration: 1400,
          color: '#655f93',
          trailColor: '#eee',
          trailWidth: 1,
          svgStyle: {width: '100%', height: '100%'},
          text: {
            style: {
              // Text color.
              // Default: same as stroke color (options.color)
            },
            autoStyleContainer: false
          },
          step: (state, bar) => {
            bar.setText(Math.round(bar.value() * 100) + ' %');
          }
        });

        bar1.animate(0.25);  // Number from 0.0 to 1.0

        var bar2 = new ProgressBar.Line(progress_2, {
          strokeWidth: 1,
          easing: 'easeInOut',
          duration: 1400,
          color: '#007cf0',
          trailColor: '#eee',
          trailWidth: 1,
          svgStyle: {width: '100%', height: '100%'},
          text: {
            style: {
              // Text color.
              // Default: same as stroke color (options.color)
            },
            autoStyleContainer: false
          },
          step: (state, bar) => {
            bar.setText(Math.round(bar.value() * 100) + ' %');
          }
        });

        bar2.animate(0.62);  // Number from 0.0 to 1.0

        var bar3 = new ProgressBar.Line(progress_3, {
          strokeWidth: 1,
          easing: 'easeInOut',
          duration: 1400,
          color: '#00c3bc',
          trailColor: '#eee',
          trailWidth: 1,
          svgStyle: {width: '100%', height: '100%'},
          text: {
            style: {
              // Text color.
              // Default: same as stroke color (options.color)
            },
            autoStyleContainer: false
          },
          step: (state, bar) => {
            bar.setText(Math.round(bar.value() * 100) + ' %');
          }
        });

        bar3.animate(0.34);  // Number from 0.0 to 1.0
    </script>
    <!-- Progress Bar Function -->
    
    <!-- Sidebar Hover Function -->
    <script>
        // Sidebar Hover Function
        const $dropdown = $(".sidebar .dropdown");
        const $dropdownToggle = $(".sidebar .dropdown-toggle");
        const $dropdownMenu = $(".sidebar .dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function () {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                    function () {
                        const $this = $(this);
                        $this.addClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "true");
                        $this.find($dropdownMenu).addClass(showClass);
                    },
                    function () {
                        const $this = $(this);
                        $this.removeClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "false");
                        $this.find($dropdownMenu).removeClass(showClass);
                    }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });


        $('.card-header .dropdown-menu .nav-tabs .nav-link').on("click.bs.dropdown", function (e) {
            $(this).tab('show');
            e.stopPropagation();
        });
        $('.col-account-header .dropdown.col-account .switch-toggle .slider-toggler').on("click", function (e) {
            e.stopPropagation();
        });
    </script>
    <!-- Sidebar Hover Function -->


</body>

</html>
