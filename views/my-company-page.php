<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-chart">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-12 col-header-filter">
                            <h2 class="card-title-new card-title-back"><a href="javascript: window.history.back()" class="breadcrumbs-link breadcrumbs-link-title">&lt;</a> <?php echo lang('company_profile'); ?></h2>
                            <?php echo partial('workspace/partial/header.html.php'); ?>
                        </div>
                    </div>
                </div>
                <?php if( isset($company) && !empty($company) ) { $current_user = $user; $user = $company; } ?>
                <div class="card-body-companyprofile">
					<ul class="nav nav-pills nav-pills-mycompanyprofile nav-pills-mircosite">
						<li class="nav-item">
                            <a href="javascript: window.history.back()" class="breadcrumbs-link breadcrumbs-link-title">&lt;</a>
							<a class="nav-link active"><?php echo lang('company_my_company_profile'); ?></a>
						</li>
					</ul>
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-xl-12 col-companyprofile-top">
                                <div class="col-companyprofile-top-wrapper">
                                    <div class="row">
                                        <div class="col-xl-12 col-companyprofile">
                                            <div class="company-microsite-container">
                                                <div class="left">
                                                    <div class="col-company-logo">
                                                        <img src="<?php echo imgCrop($user->info['photo'], 135, 135, 'assets/img/default-avatar.png'); ?>" alt="Profile Photo">
                                                    </div>
                                                </div>
                                                <div class="right">
                                                    <div class="col-company-name-microsite"><?php echo $user->info['company']['name']; ?></div>
                                                    <div class="col-company-name-microsite-details-group">
                                                        <div class="col-company-name-microsite-details-com"><span class="microsite-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-building" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fill-rule="evenodd" d="M14.763.075A.5.5 0 0 1 15 .5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5V14h-1v1.5a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5V10a.5.5 0 0 1 .342-.474L6 7.64V4.5a.5.5 0 0 1 .276-.447l8-4a.5.5 0 0 1 .487.022zM6 8.694L1 10.36V15h5V8.694zM7 15h2v-1.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 .5.5V15h2V1.309l-7 3.5V15z"></path>
                                                                    <path d="M2 11h1v1H2v-1zm2 0h1v1H4v-1zm-2 2h1v1H2v-1zm2 0h1v1H4v-1zm4-4h1v1H8V9zm2 0h1v1h-1V9zm-2 2h1v1H8v-1zm2 0h1v1h-1v-1zm2-2h1v1h-1V9zm0 2h1v1h-1v-1zM8 7h1v1H8V7zm2 0h1v1h-1V7zm2 0h1v1h-1V7zM8 5h1v1H8V5zm2 0h1v1h-1V5zm2 0h1v1h-1V5zm0-2h1v1h-1V3z"></path>
                                                                </svg></span><?php echo $user->info['company']['reg_num']; ?></div>
                                                        <div class="col-company-name-microsite-details-com"><span class="microsite-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-map" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fill-rule="evenodd" d="M15.817.613A.5.5 0 0 1 16 1v13a.5.5 0 0 1-.402.49l-5 1a.502.502 0 0 1-.196 0L5.5 14.51l-4.902.98A.5.5 0 0 1 0 15V2a.5.5 0 0 1 .402-.49l5-1a.5.5 0 0 1 .196 0l4.902.98 4.902-.98a.5.5 0 0 1 .415.103zM10 2.41l-4-.8v11.98l4 .8V2.41zm1 11.98l4-.8V1.61l-4 .8v11.98zm-6-.8V1.61l-4 .8v11.98l4-.8z"></path>
                                                                </svg></span><?php echo $cms->getStateName($user->info['state']); ?>, <?php echo $cms->getCountryName($user->info['country']); ?></div>
                                                        <div class="col-company-name-microsite-details-com"><span class="microsite-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-envelope" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fill-rule="evenodd" d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"></path>
                                                                </svg></span><?php echo $user->info['company']['email']; ?></div>
                                                        <div class="col-company-name-microsite-details-com"><span class="microsite-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-telephone" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fill-rule="evenodd" d="M3.925 1.745a.636.636 0 0 0-.951-.059l-.97.97c-.453.453-.62 1.095-.421 1.658A16.47 16.47 0 0 0 5.49 10.51a16.471 16.471 0 0 0 6.196 3.907c.563.198 1.205.032 1.658-.421l.97-.97a.636.636 0 0 0-.06-.951l-2.162-1.682a.636.636 0 0 0-.544-.115l-2.052.513a1.636 1.636 0 0 1-1.554-.43L5.64 8.058a1.636 1.636 0 0 1-.43-1.554l.513-2.052a.636.636 0 0 0-.115-.544L3.925 1.745zM2.267.98a1.636 1.636 0 0 1 2.448.153l1.681 2.162c.309.396.418.913.296 1.4l-.513 2.053a.636.636 0 0 0 .167.604L8.65 9.654a.636.636 0 0 0 .604.167l2.052-.513a1.636 1.636 0 0 1 1.401.296l2.162 1.681c.777.604.849 1.753.153 2.448l-.97.97c-.693.693-1.73.998-2.697.658a17.47 17.47 0 0 1-6.571-4.144A17.47 17.47 0 0 1 .639 4.646c-.34-.967-.035-2.004.658-2.698l.97-.969z"></path>
                                                                </svg></span><?php echo $user->info['company']['phone']; ?></div>
                                                    </div>
                                                    <div class="rating-centre-container">
                                                        <div class="left">
                                                            <div class="stars-group">
                                                                <div class="total-stars">
                                                                    <div class="left-stars">
	                                                                    <span class="rating-centre-numbers"><?php echo number_format($ratings['overall'], 1) ?></span>
	                                                                    <span class="rating-centre-star-icon"><svg viewBox="0 0 16 16" class="bi bi-star-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"></path>
                                                                            </svg></span></div>
                                                                    <div class="left-rating"><?php echo sprintf(lang('company_based_on_x_ratings'), $ratings['count']); ?></div>
                                                                </div>
	                                                            <?php if(!is_null($ratings['since'])): ?>
                                                                <div class="left-rating-since"><?php echo lang('company_rating_since'); ?> <?php echo strftime("%d %B %Y", strtotime($ratings['since'])) ?></div>
	                                                            <?php endif ?>
                                                            </div>

                                                            <div class="left-top-rating"><?php echo lang('company_top_rating'); ?> <span class="top-rating-highlight"><?php echo number_format($ratings['as_poster_top_rating'], 1) ?> <?php echo lang('company_out_of_10'); ?></span></div>
                                                        </div>
                                                        <div class="right">
                                                            <div class="feedback-rating">
                                                                <div class="rating-stars-value"><?php echo lang('company_easy_to_work_with'); ?></div>
                                                                <div class="rating-stars-desc">
                                                                    <div id="progress_1" class="progressbar-chart_ratings"></div>
                                                                </div>
                                                            </div>
                                                            <div class="feedback-rating">
                                                                <div class="rating-stars-value"><?php echo lang('company_clear_instructions'); ?></div>
                                                                <div class="rating-stars-desc">
                                                                    <div id="progress_2" class="progressbar-chart_ratings"></div>
                                                                </div>
                                                            </div>
                                                            <div class="feedback-rating">
                                                                <div class="rating-stars-value"><?php echo lang('company_flexibility'); ?></div>
                                                                <div class="rating-stars-desc">
                                                                    <div id="progress_3" class="progressbar-chart_ratings"></div>
                                                                </div>
                                                            </div>
                                                            <div class="feedback-rating">
                                                                <div class="rating-stars-value"><?php echo lang('company_trustworthiness'); ?></div>
                                                                <div class="rating-stars-desc">
                                                                    <div id="progress_4" class="progressbar-chart_ratings"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-companyprofile-bottom-wrapper">
                                    <div class="row">
                                        <div class="col-xl-12 col-companyprofile">
                                            <div class="company-microsite-container-bottom">
                                                <div class="left">
                                                    <div class="company-about-us-container-img">
                                                        <?php for($i = 0; $i <= 5; $i++){ ?>
                                                        <?php if(!empty($user->info['company']['photos'][$i])){ ?>
                                                        <div class="company-about-us-container-img-stack"><a href="#"><img src="<?php echo imgCrop($user->info['company']['photos'][$i], 500, 200); ?>"></a></div>
                                                        <?php }else{ ?>
                                                        <div class="company-about-us-container-img-stack img-stack-empty">
                                                            <div>
                                                               <span class="empty-placholder-img">
                                                                    <img src="<?php echo url_for('assets/img/empty-placeholder-outline.svg') ?>" alt="">
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <?php }} ?>
                                                    </div>
                                                </div>
                                                <div class="right">
                                                    <div class="company-about-us-container">
                                                        <span class="about-us-title"><?php echo lang('company_statement_section_title'); ?></span>
                                                        <span class="about-us-desc">
                                                            <?php echo $user->info['company']['about_us']; ?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12">
                            <div class="row">
                                <div class="col-xl-12 col-companyprofile-bottom">
	                                <?php if( !empty($posts) ): ?>
									<div class="opportunities-company-title"><?php echo lang('company_opportunities_currently_available'); ?></div>
                                    <div class="col-microsite-row-container-main">
	                                    <?php foreach($posts as $post): ?>
                                        <?php echo partial('/workspace/partial/job-centre/company-post.html.php', ['post' => $post, 'user' => $user, 'current_user' => $current_user]) ?>
	                                    <?php endforeach; ?>
                                    </div>
	                                <?php else: ?>
	                                <div class="col-microsite-row-container-main empty-state">
		                                <div class="col-microsite-padding">
			                                <div class="col-microsite-row-container">
				                                <div class="empty-state-container">
					                                <div class="empty-img"><img src="<?php echo url_for('/assets/img/mtp-no-data-2.png') ?>" alt=""></div>
					                                <span class="empty-lbl"><?php echo $user->info['company']['name'] .' '. lang('does_not_have_any_active_jobs_right_now') ?>.</span>
					                                <span class="empty-desc"><?php echo lang('please_use_our_job_search_to_look_for_open_vacancies') ?></span>
				                                </div>
			                                </div>
		                                </div>
	                                </div>
	                                <?php endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php content_for('extra_scripts') ?>
<script>
    if( $('#progress_1').length ) {
        var bar1 = new ProgressBar.Line(progress_1, {
            strokeWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            color: '#655f93',
            trailColor: '#eee',
            trailWidth: 1,
            svgStyle: {
                width: '100%',
                height: '100%'
            },
            text: {
                style: {
                    // Text color.
                    // Default: same as stroke color (options.color)
                },
                autoStyleContainer: false
            },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) / 10);
            }
        });

        bar1.animate(<?php echo $ratings['p_easy'] / 10 ?>); // Number from 0.0 to 1.0
    }
    if( $('#progress_2').length ) {
        var bar2 = new ProgressBar.Line(progress_2, {
            strokeWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            color: '#007cf0',
            trailColor: '#eee',
            trailWidth: 1,
            svgStyle: {
                width: '100%',
                height: '100%'
            },
            text: {
                style: {
                    // Text color.
                    // Default: same as stroke color (options.color)
                },
                autoStyleContainer: false
            },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) / 10);
            }
        });

        bar2.animate(<?php echo $ratings['clear'] / 10 ?>); // Number from 0.0 to 1.0
    }
    if( $('#progress_3').length ) {
        var bar3 = new ProgressBar.Line(progress_3, {
            strokeWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            color: '#00c3bc',
            trailColor: '#eee',
            trailWidth: 1,
            svgStyle: {
                width: '100%',
                height: '100%'
            },
            text: {
                style: {
                    // Text color.
                    // Default: same as stroke color (options.color)
                },
                autoStyleContainer: false
            },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) / 10);
            }
        });

        bar3.animate(<?php echo $ratings['flexibility'] / 10 ?>); // Number from 0.0 to 1.0
    }
    if( $('#progress_4').length ) {
        var bar4 = new ProgressBar.Line(progress_4, {
            strokeWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            color: '#ff9b9b',
            trailColor: '#eee',
            trailWidth: 1,
            svgStyle: {
                width: '100%',
                height: '100%'
            },
            text: {
                style: {
                    // Text color.
                    // Default: same as stroke color (options.color)
                },
                autoStyleContainer: false
            },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) / 10);
            }
        });

        bar4.animate(<?php echo $ratings['trustworthiness'] / 10 ?>); // Number from 0.0 to 1.0
    }
</script>
<?php end_content_for() ?>
