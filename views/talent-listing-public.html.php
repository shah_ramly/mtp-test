<div class="public-container">
    <div class="container">
        <div class="col-public-search-wrapper">
            <div class="col-xl-12 col-public-search">
	            <form action="<?php echo url_for('/talents/search') ?>" id="talentSearchForm" class=" search-public-form" method="get">
		            <input type="hidden" name="scope" value="talent" />
		            <input type="hidden" name="perform-search" value="no" disabled />
		            <input type="hidden" name="page" disabled value="1" />
		            <input type="hidden" name="state"
                        <?php if(isset($filters['state']) ): ?>
				            value="<?php echo $filters['state'] ?>"
                        <?php else: ?>
				            disabled
                        <?php endif ?>
		            />
		            <input type="hidden" name="city"
                        <?php if(isset($filters['city']) ): ?>
				            value="<?php echo $filters['city'] ?>"
                        <?php else: ?>
				            disabled
                        <?php endif ?>
		            />
		            <input type="hidden" name="skills"
                        <?php if(isset($filters['skills']) ): ?>
				            value="<?php echo $filters['skills'] ?>"
                        <?php else: ?>
				            disabled
                        <?php endif ?>
		            />
		            <input type="hidden" name="completed"
                        <?php if(isset($filters['completed']) ): ?>
				            value="<?php echo $filters['completed'] ?>"
                        <?php else: ?>
				            disabled
                        <?php endif ?>
		            />
		            <div class="search-dropdown-filter-top-wrapper form-flex">
			            <div class="search-dropdown-filter-container">
				            <input type="text" name="q" class="form-control form-control-input" placeholder="<?php echo ('Search Talent by Keyword') ?>" id="inputBox" value="<?php echo !is_null($search_term) && request('scope') === 'talent' ? $search_term : '' ?>">
			            </div>
			            <span class="search-dropdown-filter-btn">
			                <button class="btn-search-public" type="submit">
			                    <span class="btn-label">Search</span>
			                    <span class="public-search-icon search-icon">
			                        <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
			                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
			                        </svg>
			                    </span>
			                </button>
			            </span>
			            <span class="bookmark-dropdown-filter-btn dropdown">
			                <button class="btn-bookmark-public dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
			                    <span class="btn-label">Save Search</span>
			                    <span class="public-search-icon search-icon">
			                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bookmarks" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			                            <path fill-rule="evenodd" d="M7 13l5 3V4a2 2 0 0 0-2-2H4a2 2 0 0 0-2 2v12l5-3zm-4 1.234l4-2.4 4 2.4V4a1 1 0 0 0-1-1H4a1 1 0 0 0-1 1v10.234z" />
			                            <path d="M14 14l-1-.6V2a1 1 0 0 0-1-1H4.268A2 2 0 0 1 6 0h6a2 2 0 0 1 2 2v12z" />
			                        </svg>
			                    </span>
			                    <i class="fa fa-caret-down"></i>
			                </button>
			                <div class="dropdown-menu dropdown-menu-saved-search">
			                    <div class="modal-advance-filter-container-left">
			                        <div class="modal-advanced-filter-container">
			                            <div class="keyword-filter-container">
			                                <label>Keyword:</label>
			                                <span><?php echo request('scope') === 'talent' ? $keywords : '' ?></span>
			                            </div>
			                            <label class="modal-search-filter-lbl">Save Search</label>
			                            <div class="input-group-flex row-indfirstname">
			                                <input type="text" name="search_name" id="Name" class="form-control form-control-input" placeholder="Name" autofocus="">
			                            </div>
			                        </div>
			                        <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
                                        <div class="modal-filter-action form-block-filter-flex">
                                            <button type="button" class="btn-public-sign-up btn-confirm" onclick="location.href='<?php echo url_for('/sign_up') ?>';">
                                                <span class="btn-label">Sign Up</span>
                                            </button>
                                        </div>
                                    </div>
			                    </div>
			                </div>
			            </span>
		            </div>
	            </form>
            </div>
            <div class="col-search-terms-result" style="<?php echo (($keywords === '' && request('scope') === 'talent') || ($keywords === '')) ? "display:none" : "" ?>">
                <div class="col-search-terms-result-title">Search Keyword :</div>
                <div class="col-search-terms-result-value"><?php echo request('scope') === 'talent' ? $keywords : '' ?></div>
            </div>
        </div>

        <div class="col-task-listing-wrapper">
            <div class="row">
                <div class="col-xl-3 col-task-filter-wrapper">
                    <div class="col-task-filter-container talents-filter">
                        <div class="col-task-filter-more-filter">
                            <span class="filter-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                    <line x1="4" y1="21" x2="4" y2="14"></line>
                                    <line x1="4" y1="10" x2="4" y2="3"></line>
                                    <line x1="12" y1="21" x2="12" y2="12"></line>
                                    <line x1="12" y1="8" x2="12" y2="3"></line>
                                    <line x1="20" y1="21" x2="20" y2="16"></line>
                                    <line x1="20" y1="12" x2="20" y2="3"></line>
                                    <line x1="1" y1="14" x2="7" y2="14"></line>
                                    <line x1="9" y1="8" x2="15" y2="8"></line>
                                    <line x1="17" y1="16" x2="23" y2="16"></line>
                                </svg>
                            </span>
                            <span class="filter-lbl">More filters</span>
                        </div>
	                    <div class="col-talent-filter-rows">
		                    <div class="task-filter-row task-filter-row-location">
			                    <button class="btn-link btn-link-filter btn-flex location" type="button" aria-expanded="false">
				                    <label class="task-filter-lbl">Filter by location</label>
				                    <span class="chevron-icon">
					                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
					                        <path d="M6 9l6 6 6-6" /></svg>
					                </span>
			                    </button>
			                    <div class="form-filter-collapse collapse">
				                    <div class="form-block-task-filter">
					                    <div class="form-filter-state">
						                    <select class="form-control form-control-input" name="state" placeholder="State">
							                    <option value="" selected="">State</option>
                                                <?php foreach($states as $state): ?>
								                    <option <?php echo request('scope') === 'talent' && isset($filters['state']) && $state['id'] === $filters['state'] ? 'selected' : '' ?>
										                    value="<?php echo $state['id'] ?>"><?php echo $state['name'] ?></option>
                                                <?php endforeach; ?>
						                    </select>
					                    </div>
					                    <div class="form-filter-city">
						                    <select class="form-control form-control-input" name="city" placeholder="City">
							                    <option selected="" value="0">Town/City</option>
                                                <?php foreach($cities as $city): ?>
								                    <option <?php echo request('scope') === 'talent' && isset($filters['city']) && $city['id'] === $filters['city'] ? 'selected' : '' ?>
										                    value="<?php echo $city['id'] ?>">
                                                        <?php echo $city['name'] ?>
								                    </option>
                                                <?php endforeach; ?>
						                    </select>
					                    </div>
				                    </div>
			                    </div>
		                    </div>
		                    <div class="task-filter-row task-filter-row-skills">
			                    <button class="btn-link btn-link-filter btn-flex skillss" type="button" aria-expanded="false">
				                    <label class="task-filter-lbl">Filter by skills</label>
				                    <span class="chevron-icon">
					                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
					                        <path d="M6 9l6 6 6-6" /></svg>
					                </span>
			                    </button>
			                    <div class="form-filter-collapse collapse">
				                    <div class="form-block-task-filter">
					                    <div class="bstags-skills">
						                    <input type="text" name="skills" class="bootstrap-tagsinput"
                                                <?php if(request('scope') === 'talent' && isset($filters['skills'])):?>
								                    data-input-status="true"
								                    value="<?php echo implode(',', $filters['skills']) ?>"
                                                <?php else: ?>
								                    data-input-status="false"
                                                <?php endif; ?>
							                       placeholder="<?php echo lang('public_search_filter_skills_placeholder'); ?>" />
					                    </div>
				                    </div>
			                    </div>
		                    </div>
		                    <div class="task-filter-row task-filter-row-task-completed">
			                    <button class="btn-link btn-link-filter btn-flex completed" type="button" aria-expanded="false">
				                    <label class="task-filter-lbl">Filter by completed task</label>
				                    <span class="chevron-icon">
					                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
					                        <path d="M6 9l6 6 6-6" /></svg>
					                </span>
			                    </button>
			                    <div class="form-filter-collapse collapse">
				                    <div class="form-block-task-filter">
					                    <div class="custom-control custom-checkbox">
						                    <input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_1"
                                                <?php if(request('scope') === 'talent' && isset($filters['completed']) && $filters['completed'] === 'all'):?>
								                    data-input-status="true"
								                    checked
                                                <?php else: ?>
								                    data-input-status="false"
                                                <?php endif; ?>
							                       value="all" />
						                    <label class="custom-control-label" for="filtertaskcompleted_1">All</label>
					                    </div>
					                    <div class="custom-control custom-checkbox">
						                    <input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_2"
                                                <?php if(request('scope') === 'talent' && isset($filters['completed']) && $filters['completed'] === '1-10'):?>
								                    data-input-status="true"
								                    checked
                                                <?php else: ?>
								                    data-input-status="false"
                                                <?php endif; ?>
							                       value="1-10" />
						                    <label class="custom-control-label" for="filtertaskcompleted_2">1-10</label>
					                    </div>
					                    <div class="custom-control custom-checkbox">
						                    <input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_3"
                                                <?php if(request('scope') === 'talent' && isset($filters['completed']) && $filters['completed'] === '10+'):?>
								                    data-input-status="true"
								                    checked
                                                <?php else: ?>
								                    data-input-status="false"
                                                <?php endif; ?>
							                       value="10+" />
						                    <label class="custom-control-label" for="filtertaskcompleted_3">10+</label>
					                    </div>
				                    </div>
			                    </div>
		                    </div>
		                    <div class="task-filter-row task-filter-row-reset">
			                    <a href="#" class="reset-filter-link reset-talent-filters">Reset all filters</a>
		                    </div>
	                    </div>
                    </div>
                </div>
                <div class="col-xl-9 col-talent-row-wrapper">
	                <div class="talent-result-container">
		                <?php if(!empty($listing)): ?>
		                <?php foreach($listing as $talent): ?>
                        <div class="col-talent-row-container">
	                        <div class="talent-row-col-top-flex">
	                            <div class="talent-row-col-left">
	                                <div class="talent-personal-info-container">
	                                    <div class="talent-avatar-container">
	                                        <div class="profile-avatar">
	                                            <a href="<?php echo url_for('/sign_up') . "?redirect=" . url_for("/talent/{$talent['number']}") ?>" class="talent-avatar-link">
		                                            <img class="avatar-img" src="<?php echo $talent['photo'] ?>" alt="">
	                                            </a>
	                                        </div>
	                                    </div>
	                                    <div class="talent-personal-info">
	                                        <a href="<?php echo url_for('/sign_up') . "?redirect=" . url_for("/talent/{$talent['number']}") ?>" class="talent-name-link">
	                                            <h5 class="profile-name"><?php echo $talent['number'] ?></h5>
	                                        </a>
	                                        <div class="talent-details-profile-location">
	                                            <span class="talent-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                                    <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
	                                                </svg></span>
	                                            <span class="talent-details-profile-location-val">
		                                            <?php echo ($talent['state'] && $talent['country']) ? "{$talent['state']}, {$talent['country']}" : 'N/A' ?>
	                                            </span>
	                                        </div>
	                                        <div class="task-row-rating-flex">
	                                            <div class="task-row-rating-star">
	                                                <?php $starred = (int)floor($talent['rating']['overall']/2); $rest = 5 - $starred; ?>
	                                                <?php if($starred > 0): ?>
	                                                    <?php foreach (range(1, $starred) as $star): ?>
				                                            <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
	                                                    <?php endforeach; ?>
	                                                <?php endif; ?>
	                                                <?php if($rest > 0): ?>
	                                                    <?php foreach (range(1, $rest) as $star1): ?>
				                                            <span class="rating-star-icon"><i class="fa fa-star"></i></span>
	                                                    <?php endforeach; ?>
	                                                <?php endif; ?>
	                                            </div>
	                                            <div class="task-row-rating-val">
	                                                <span class="task-row-average-rating-val">
		                                                <?php echo number_format((float)$talent['rating']['overall'], 1); ?>
	                                                </span>
	                                                <span class="task-row-divider">/</span>
	                                                <span class="task-row-total-rating-val">
		                                                <?php echo $talent['rating']['count'] . " review" . plural($talent['rating']['count']); ?>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="talent-about-container">
	                                    <?php echo strlen($talent['about']) > 235 ? (substr($talent['about'], 0, 235) . ' ...') : $talent['about']; ?>
	                                </div>
	                                <div class="talent-row-skills">
	                                <?php if( !empty($talent['skills']) ): ?>
		                                <div class="task-details-deadline-block">
	                                        <?php foreach ($talent['skills_list'] as $skill): ?>
				                                <a href="<?php echo url_for('talents/search') . "?scope=talent&skills=" . $skill['id'] ?>" class="skills-label-link">
					                                <span class="tag label label-info bubble-lbl"><?php echo $skill['name'] ?></span>
				                                </a>
	                                        <?php endforeach ?>
		                                </div>
	                                <?php endif ?>
	                                </div>
	                            </div>
	                            <div class="talent-row-col-right">
	                                <div class="talent-row-actions">
	                                    <button type="button" class="btn-icon-full btn-view-profile"
	                                            onclick="location.href='<?php echo url_for('/sign_up') . "?redirect=" . url_for("/talent/{$talent['number']}") ?>';">
	                                        <span class="btn-label"><?php echo lang('public_search_talent_view_profile'); ?></span>
	                                        <span class="btn-icon">
	                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
	                                                <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
	                                                <polyline points="22 4 12 14.01 9 11.01"></polyline>
	                                            </svg>
	                                        </span>
	                                    </button>
	                                </div>
	                                <div class="talent-activity-tracking-details">
	                                    <div class="talent-total-completed-task-hour">
	                                        <div class="talent-extra-info talent-total-completed-task">
	                                            <span class="talent-profile-label"><?php echo lang('public_search_talent_completed_task'); ?>:</span>
	                                            <span class="talent-profile-val"><?php echo $talent['info']['completed'] ?></span>
	                                        </div>
	                                        <div class="talent-extra-info  talent-total-completed-hour">
	                                            <span class="talent-profile-label">Total Hours Completed:</span>
	                                            <span class="talent-profile-val"><?php echo (int)$talent['info']['hours_completed'] ?></span>
	                                        </div>
	                                    </div>
	                                    <div class="talent-extra-info talent-history">
	                                        <div class="talent-last-login">
	                                            <span class="talent-profile-label">Last login:</span>
	                                            <span class="talent-profile-val"><?php echo $talent['info']['last_login'] ?></span>
	                                        </div>
	                                        <div class="talent-extra-info talent-member-since">
	                                            <span class="talent-profile-label">Member since:</span>
	                                            <span class="talent-profile-val"><?php echo $talent['member_since'] ?></span>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
		                <?php endforeach ?>
                        <?php else: ?>
		                <div class="col-task-row-container">
			                <div class="task-row-col-top-flex job">
				                <div class="empty-state-container">
					                <div class="empty-img"><img src="<?php echo url_for('assets/img/mtp-no-data-2.png') ?>" alt=""></div>
					                <span class="empty-lbl">No result to display</span>
				                </div>
			                </div>
		                </div>
                        <?php endif ?>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php content_for('scripts'); ?>
<script>
$(function(){
    $('input[name="completed"]').on('click', function(e){
        $(e.currentTarget).parent().siblings().find('input[name="completed"]').prop('checked', false);
        $(e.currentTarget).prop('checked', true);
    });


    $('.talents-filter').find('input, select').on('change', function(e){
        $('#talentSearchForm input[name="page"]').val("1");
        $('#talentSearchForm input[name="perform-search"]').val('yes');
        update_talents_filters($(e.currentTarget));
    });

    $('.reset-talent-filters').on('click', function(e){
        e.preventDefault();
        var q = $('#talentSearchForm input[name="q"]').val();
        $(".talents-filter button.btn-link-filter.is-clicked").trigger('click');
        $('.talents-filter input[name="skills"]').tagsinput('removeAll');
        document.getElementById('talentSearchForm').reset();
        $('.talents-filter input').val('');
        $('.talents-filter checkbox').prop('checked', false);
        if(q.length) q = '&q=' + q;
        window.location = "<?php echo option('site_uri') . url_for('/talents/search') . '?scope=talent' ?>"+q;
    })

    <?php if((isset($filters['state']) && !empty($filters['state'])) || (isset($filters['city']) && !empty($filters['city']))): ?>
    $(".talents-filter button.btn-link-filter.location").trigger('click');
    <?php endif ?>

    <?php if(isset($filters['completed'])): ?>
    $(".talents-filter button.btn-link-filter.completed").trigger('click');
    <?php endif ?>

    $(".btn-load-more.more-talents").on('click', function(e){
        var page = $('#talentSearchForm input[name="page"]').val();
        $('#talentSearchForm input[name="page"]').val(parseInt(page)+1);
        get_talents_result('load_more');
    });

    $('#talentSearchForm input[name="search_name"]').keydown(function(e){
        if(e.keyCode === 13){
            return false;
        }
    })

    $('#talentSearchForm').on('submit', function(){
        $('input[name="search_name"]').prop('disabled', true);
    });


    function update_talents_filters(target){
        if(target === undefined){
            var inputs = $('.talents-filter').find('input, select');
        }else{
            var inputName = target.attr('name');
            var inputs = $('.talents-filter').find('[name="'+inputName+'"]');
        }

        var value = '';
        var input = $('#talentSearchForm input[name="'+inputName+'"]');

        inputs.each(function(idx, item){
            if( (item.type === 'checkbox' && item.checked) || (item.type === 'text' && item.value !== '') || (item.type === 'select-one' && item.value !== '') ){
                value = item.value;
                if(value.length) {
                    input.prop('disabled', false);
                    input.val(value);
                    if(target !== undefined) target.data('inputStatus', true);
                    else $(item).data('inputStatus', true);
                }else{
                    input.val('');
                    input.prop('disabled', true);
                    if(target !== undefined) target.data('inputStatus', false);
                    else $(item).data('inputStatus', false);
                }
            }else{
                if(!value.length){
                    input.val('');
                    input.attr('disabled', 'disabled');
                    if(target !== undefined) target.data('inputStatus', false);
                    else $(item).data('inputStatus', false);
                }
            }
        });

        if($('#talentSearchForm input[name="perform-search"]').val() === 'yes')
            get_talents_result();
    }

    function get_talents_result(load_more = null){
        var search_form = $('#talentSearchForm').clone();
        search_form.find('input[name="search_name"]').remove();

        var page = $('#talentSearchForm input[name="page"]').val(),
            url = "<?php echo option('site_uri') . url_for('/talents/search') ?>/"+page,
            data = search_form.serialize();

        $.ajax({
            url: url,
            data: data,
        }).done(function(response){
            if(response.more){
                $(".btn-load-more.more-talents").show();
            }else{
                $(".btn-load-more.more-talents").hide();
            }
            if(load_more === null) {
                $('.talent-result-container').html(response.html);
            } else {
                var lastChildHeight = $('.talent-result-container .col-talent-row-container:last-child').height();
                var currentHeight = $('.talent-result-container').height();
                $('.talent-result-container').append(response.html);
                $('html')[0].scrollTo({ top: currentHeight+lastChildHeight - 24, left: 0, behavior: 'smooth'});
            }
            if(response.keywords !== ''){
                $('.col-search-terms-result-value').text(response.keywords);
                $('.keyword-filter-container > span').text(response.keywords);
                $('.col-search-terms-result').show();
            }else{
                $('.col-search-terms-result').hide();
            }
            window.history.pushState({'talentHtml': response.html}, '', url + '?' + data);
        });
    }

    window.onpopstate = function(e){
        if(e.state){
            $('.col-task-listing-wrapper .col-talent-row-wrapper').html(e.state.talentHtml);
        }
    };

    skills = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '<?php echo option('site_uri') . url_for('ajax/skills.json') ?>?all=1'
    });
    skills.clearPrefetchCache();
    skills.initialize();

    $('.talents-filter input[name="skills"]').tagsinput({
        allowDuplicates: false,
        confirmKeys: [9, 13, 44, 188],
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'skills',
            displayKey: 'name',
            source: skills.ttAdapter()
        }
    });

    <?php if(isset($filters['skills_list']) && !empty($filters['skills_list'])): ?>
    $('.talents-filter input[name="skills"]').off('change');
    <?php foreach($filters['skills_list'] as $skill): ?>
    $('.talents-filter input[name="skills"]').tagsinput('add', {'id': '<?php echo $skill['id'] ?>', 'name': '<?php echo $skill['name'] ?>' });
    <?php endforeach; ?>

    $('.talents-filter input[name="skills"]').tagsinput('refresh');

    $(".talents-filter button.btn-link-filter.skillss").trigger('click');

    $('.talents-filter input[name="skills"]').on('change', function(e){
        $('input[name="perform-search"]').val('yes');
        $('input[name="page"]').val("1");
        update_talents_filters($(e.currentTarget));
    });
    <?php endif ?>
});
</script>
<?php end_content_for() ?>
