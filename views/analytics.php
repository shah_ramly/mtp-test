<div class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-chart">
				<div class="card-header">
					<div class="row">
						<div class="col-sm-12 col-header-filter">
							<h2 class="card-title"><?php lang('task_awarded_thsmonth'); ?></h2>
                            <?php echo partial('workspace/partial/header.html.php'); ?>
						</div>
					</div>
				</div>
                
                

				            <div class="card-body-analytics">
								<ul class="nav nav-pills nav-pills-analytics-day" id="pills-tabAnalytics" role="tablist">
									<?php if($user->info['type'] == '0'){ ?>
									<li class="nav-item">
										<?php $newparam = $param; unset($newparam['type']); ?>
										<a class="nav-link<?php echo $type == 'seeker' ? ' active' : ''; ?>" id="pills-analytics-seeker-tab" href="<?php echo url_for('/analytics') . '?' . http_build_query($newparam); ?>"> <?php echo lang('analytic_seeker') ?></a>
									</li>
									<li class="nav-item">
										<?php $newparam = $param; $newparam['type'] = 'poster'; ?>
										<a class="nav-link<?php echo $type == 'poster' ? ' active' : ''; ?>" id="pills-analytics-poster-tab" href="<?php echo url_for('/analytics') . '?' . http_build_query($newparam); ?>"><?php echo lang('analytic_poster') ?></a>
									</li>
									<?php }else{ ?>
									<li class="nav-item">
										<?php $newparam = $param; unset($newparam['type']); ?>
										<a class="nav-link active" id="pills-analytics-poster-tab" href="<?php echo url_for('/analytics') . '?' . http_build_query($newparam); ?>"><?php echo lang('analytic_poster') ?></a>
									</li>
									<?php } ?>
								</ul>
								
                                <div class="row mt-4">
                                    <div class="col-xl-12 mt-3">
                                        <div class="row">
                                            <div class="col-xl-12 col-report-dashboard-top">
                                            <div class="col-report-dashboard-top-wrapper">
                                                    <div class="row">
                                                        <div class="col-xl-12 col-report-dashboard-top-center pl-3">
                                                            <div class="col-report-dashboard-header">
                                                                <div class="col-report-dashboard-header-left">
                                                                    <?php if($type == 'seeker'){ ?>
                                                                    <div class="col-report-dashboard-header-left">
                                                                        <div class="report-dashboard-title"><?php echo lang('analytic_how_are_you_making'); ?>	</div>
                                                                    </div>
                                                                    <?php } ?>
                                                                    <ul class="nav nav-pills nav-pills-analytics" id="pills-tab" role="tablist">
                                                                        <?php if($type == 'seeker'){ ?>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link active" id="byHoursMonthTab-tab" data-toggle="pill" href="#byHoursMonthTab" role="tab" aria-controls="byHoursMonthTab" aria-selected="true"><?php echo lang('analytic_view_by_hours'); ?></a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" id="byEarningsMonthTab-tab" data-toggle="pill" href="#byEarningsMonthTab" role="tab" aria-controls="byEarningsMonthTab" aria-selected="false"><?php echo lang('analytic_view_by_earnings'); ?></a>
                                                                        </li>
                                                                        <?php } ?>
                                                                        <?php if($type == 'poster'){ ?>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link active" id="bySpendingMonthTab-tab" data-toggle="pill" href="#bySpendingMonthTab" role="tab" aria-controls="bySpendingsMonthTab" aria-selected="false"><?php echo lang('analytic_view_by_total_expenditure'); ?></a>
                                                                        </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-report-dashboard-header-right header-filter-wrapper">
                                                                    <div class="dropdown dropdown-filter-container">
                                                                        <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            <label class="filter-btn-lbl"><?php echo lang('analytic_filter_title') ?></label>
	                                                                        <span class="drop-val"><?php echo lang('analytic_filter_by_' . $interval); ?></span>
                                                                        </button>
                                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                            <?php $newparam = $param; $newparam['interval'] = 'day';?>
                                                                            <a class="dropdown-item filter-param<?php echo $interval == 'day' ? ' active' : ''; ?>" href="<?php echo url_for('/analytics') . '?' . http_build_query($newparam); ?>">
	                                                                            <?php echo lang('analytic_filter_by_day') ?>
                                                                            </a>
                                                                            <?php $newparam = $param; unset($newparam['interval']);?>
                                                                            <a class="dropdown-item filter-param<?php echo $interval == 'month' ? ' active' : ''; ?>" href="<?php echo url_for('/analytics') . '?' . http_build_query($newparam); ?>">
	                                                                            <?php echo lang('analytic_filter_by_month') ?>

                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-content tab-analytics-month" id="pills-tabContent">
                                                            <?php if($type == 'seeker'){ ?>
                                                            <div class="tab-pane fade show active" id="byHoursMonthTab" role="tabpanel">
                                                                <div class="col-xl-12 col-report-dashboard-time">
                                                                    <div class="col-xl-7 col-report-dashboard-top-left">
                                                                        <div class="col-report-dashboard-top-left-wrapper">
                                                                            <div id="barchart_container"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                                                                <canvas id="barchart_canvas_time" style="display: block; width: 883px; height: 350px;" width="883" height="350" class="chartjs-render-monitor"></canvas>
                                                                            </div>
                                                                            <div class="btn-calendar-fiscal">
                                                                                <?php
                                                                                    $day = !empty($day) ? $day : date('d');
                                                                                    $curr_date = $year . '-' . $month . '-' . $day;

                                                                                    if($interval == 'day'){
                                                                                        $text = date('F Y', strtotime($curr_date));
                                                                                        $newparam = $param;

                                                                                        $prev_date = date('Y-m-d', strtotime($curr_date . ' -1 month')); 
                                                                                        $newparam['month'] = date('m', strtotime($prev_date));
                                                                                        $newparam['year'] = date('Y', strtotime($prev_date));
                                                                                        $prev_link =  url_for('/analytics/') . '?' . http_build_query($newparam);

                                                                                        $next_date = date('Y-m-d', strtotime($curr_date . ' +1 month')); 
                                                                                        $newparam['month'] = date('m', strtotime($next_date));
                                                                                        $newparam['year'] = date('Y', strtotime($next_date));
                                                                                        $next_link = strtotime($next_date) < time() ? url_for('/analytics/') . '?' . http_build_query($newparam) : '';
                                                                                    }

                                                                                    if($interval == 'month'){
                                                                                        $text = date('Y', strtotime($curr_date));
                                                                                        $newparam = $param;

                                                                                        $prev_date = date('Y-m-d', strtotime($curr_date . ' -1 year')); 
                                                                                        $newparam['year'] = date('Y', strtotime($prev_date));
                                                                                        $prev_link =  url_for('/analytics/') . '?' . http_build_query($newparam);

                                                                                        $next_date = date('Y-m-d', strtotime($curr_date . ' +1 year')); 
                                                                                        $newparam['year'] = date('Y', strtotime($next_date));
                                                                                        $next_link = strtotime($next_date) < time() ? url_for('/analytics/') . '?' . http_build_query($newparam) : '';
                                                                                    }
                                                                                ?>
                                                                                <button class="btn btn-link btn-border filter-param" id="download-button" onClick="location.href='<?php echo $prev_link; ?>'"><i class="tim-icons icon-minimal-left"></i></button>
                                                                                <span class="fiscal-title"><?php echo $text; ?></span>
                                                                                <button class="btn btn-link btn-border filter-param" id="download-button"<?php echo $next_link ? 'onClick="location.href=\'' . $next_link . '\'"' : ' disabled'; ?>><i class="tim-icons icon-minimal-right"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-5 col-report-dashboard-top-right">
                                                                        <div class="col-report-dashboard-top-right-wrapper">
                                                                            <!--<div class="report-dashboard-monthly-title">Most Productive Month</div>-->
                                                                            <div class="sub-col-content-report-dashboard-monthly">
                                                                                <div class="sub-row-report-dashboard report-dashboard-monthly-hour-row">
                                                                                    <div class="sub-row-report-dashboard-desc"><?php echo lang('analytic_highest_hours_recorded'); ?></div>
                                                                                    <div class="sub-row-report-dashboard-val"><?php echo $hours_highest['value'] != 1 ? $hours_highest['value'] . ' ' . lang('task_hours') : $hours_highest['value'] . ' ' . lang('task_hour'); ?></div>
                                                                                </div>
                                                                                <div class="sub-row-report-dashboard report-dashboard-monthly-day-row">
                                                                                    <div class="sub-row-report-dashboard-desc"><?php echo $interval == 'day' ? lang('analytic_most_productive_day') : lang('analytic_most_productive_month'); ?></div>
                                                                                    <div class="sub-row-report-dashboard-val"><?php echo $hours_highest['date'] ? ($interval == 'day' ? date($cms->settings()['date_format'], strtotime($hours_highest['date'])) : date('M Y', strtotime($hours_highest['date']))) : 'N/A'; ?></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade show" id="byEarningsMonthTab" role="tabpanel">
                                                                <div class="col-xl-12 col-report-dashboard-money">
                                                                    <div class="col-xl-7 col-report-dashboard-top-left">
                                                                        <div class="col-report-dashboard-top-left-wrapper">
                                                                            <div id="barchart_container">
                                                                                <canvas id="barchart_canvas_money" style="display: block; width: 0px; height: 0px;" height="0" class="chartjs-render-monitor" width="0"></canvas>
                                                                            </div>
                                                                            <div class="btn-calendar-fiscal">
                                                                                <?php
                                                                                    $day = !empty($day) ? $day : date('d');
                                                                                    $curr_date = $year . '-' . $month . '-' . $day;

                                                                                    if($interval == 'day'){
                                                                                        $text = date('F Y', strtotime($curr_date));
                                                                                        $newparam = $param;

                                                                                        $prev_date = date('Y-m-d', strtotime($curr_date . ' -1 month')); 
                                                                                        $newparam['month'] = date('m', strtotime($prev_date));
                                                                                        $newparam['year'] = date('Y', strtotime($prev_date));
                                                                                        $prev_link =  url_for('/analytics/') . '?' . http_build_query($newparam);

                                                                                        $next_date = date('Y-m-d', strtotime($curr_date . ' +1 month')); 
                                                                                        $newparam['month'] = date('m', strtotime($next_date));
                                                                                        $newparam['year'] = date('Y', strtotime($next_date));
                                                                                        $next_link = strtotime($next_date) < time() ? url_for('/analytics/') . '?' . http_build_query($newparam) : '';
                                                                                    }

                                                                                    if($interval == 'month'){
                                                                                        $text = date('Y', strtotime($curr_date));
                                                                                        $newparam = $param;

                                                                                        $prev_date = date('Y-m-d', strtotime($curr_date . ' -1 year')); 
                                                                                        $newparam['year'] = date('Y', strtotime($prev_date));
                                                                                        $prev_link =  url_for('/analytics/') . '?' . http_build_query($newparam);

                                                                                        $next_date = date('Y-m-d', strtotime($curr_date . ' +1 year')); 
                                                                                        $newparam['year'] = date('Y', strtotime($next_date));
                                                                                        $next_link = strtotime($next_date) < time() ? url_for('/analytics/') . '?' . http_build_query($newparam) : '';
                                                                                    }
                                                                                ?>
                                                                                <button class="btn btn-link btn-border filter-param" id="download-button" onClick="location.href='<?php echo $prev_link; ?>'"><i class="tim-icons icon-minimal-left"></i></button>
                                                                                <span class="fiscal-title"><?php echo $text; ?></span>
                                                                                <button class="btn btn-link btn-border filter-param" id="download-button"<?php echo $next_link ? 'onClick="location.href=\'' . $next_link . '\'"' : ' disabled'; ?>><i class="tim-icons icon-minimal-right"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-5 col-report-dashboard-top-right">
                                                                        <div class="col-report-dashboard-top-right-wrapper">
                                                                            <!--<div class="report-dashboard-monthly-title">Most Productive Month</div>-->
                                                                            <div class="sub-col-content-report-dashboard-monthly">
                                                                                <div class="sub-row-report-dashboard report-dashboard-monthly-month-row">
                                                                                    <div class="sub-row-report-dashboard-desc"><?php echo lang('analytic_highest_earnings_recorded') ?></div>
                                                                                    <div class="sub-row-report-dashboard-val">RM <?php echo number_format($earnings_highest['value'], 2, '.', ','); ?></div>
                                                                                    
                                                                                </div>
                                                                                <div class="sub-row-report-dashboard report-dashboard-monthly-month-row">
                                                                                    <div class="sub-row-report-dashboard-desc"><?php echo $interval == 'day' ? lang('analytic_highest_earning_day') : lang('analytic_highest_earning_month') ?></div>
                                                                                    <div class="sub-row-report-dashboard-val"><?php echo $earnings_highest['date'] ? ($interval == 'day' ? date($cms->settings()['date_format'], strtotime($earnings_highest['date'])) : date('M Y', strtotime($earnings_highest['date']))) : 'N/A'; ?></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php } ?>

                                                            <?php if($type == 'poster'){ ?>
                                                            <div class="tab-pane fade show active" id="bySpendingsMonthTab" role="tabpanel">
                                                                <div class="col-xl-12 col-report-dashboard-money">
                                                                    <div class="col-xl-7 col-report-dashboard-top-left">
                                                                        <div class="col-report-dashboard-top-left-wrapper">
                                                                            <div id="barchart_container">
                                                                                <canvas id="barchart_canvas_money" style="display: block; width: 0px; height: 0px;" height="0" class="chartjs-render-monitor" width="0"></canvas>
                                                                            </div>
                                                                            <div class="btn-calendar-fiscal">
                                                                                <?php
                                                                                    $day = !empty($day) ? $day : date('d');
                                                                                    $curr_date = $year . '-' . $month . '-' . $day;

                                                                                    if($interval == 'day'){
                                                                                        $text = date('F Y', strtotime($curr_date));
                                                                                        $newparam = $param;

                                                                                        $prev_date = date('Y-m-d', strtotime($curr_date . ' -1 month')); 
                                                                                        $newparam['month'] = date('m', strtotime($prev_date));
                                                                                        $newparam['year'] = date('Y', strtotime($prev_date));
                                                                                        $prev_link =  url_for('/analytics/') . '?' . http_build_query($newparam);

                                                                                        $next_date = date('Y-m-d', strtotime($curr_date . ' +1 month')); 
                                                                                        $newparam['month'] = date('m', strtotime($next_date));
                                                                                        $newparam['year'] = date('Y', strtotime($next_date));
                                                                                        $next_link = strtotime($next_date) < time() ? url_for('/analytics/') . '?' . http_build_query($newparam) : '';
                                                                                    }

                                                                                    if($interval == 'month'){
                                                                                        $text = date('Y', strtotime($curr_date));
                                                                                        $newparam = $param;

                                                                                        $prev_date = date('Y-m-d', strtotime($curr_date . ' -1 year')); 
                                                                                        $newparam['year'] = date('Y', strtotime($prev_date));
                                                                                        $prev_link =  url_for('/analytics/') . '?' . http_build_query($newparam);

                                                                                        $next_date = date('Y-m-d', strtotime($curr_date . ' +1 year')); 
                                                                                        $newparam['year'] = date('Y', strtotime($next_date));
                                                                                        $next_link = strtotime($next_date) < time() ? url_for('/analytics/') . '?' . http_build_query($newparam) : '';
                                                                                    }
                                                                                ?>
                                                                                <button class="btn btn-link btn-border filter-param" id="download-button" onClick="location.href='<?php echo $prev_link; ?>'"><i class="tim-icons icon-minimal-left"></i></button>
                                                                                <span class="fiscal-title"><?php echo $text; ?></span>
                                                                                <button class="btn btn-link btn-border filter-param" id="download-button"<?php echo $next_link ? 'onClick="location.href=\'' . $next_link . '\'"' : ' disabled'; ?>><i class="tim-icons icon-minimal-right"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-5 col-report-dashboard-top-right">
                                                                        <div class="col-report-dashboard-top-right-wrapper">
                                                                            <!--<div class="report-dashboard-monthly-title">Most Productive Month</div>-->
                                                                            <div class="sub-col-content-report-dashboard-monthly">
                                                                                <div class="sub-row-report-dashboard report-dashboard-monthly-month-row">
                                                                                    <div class="sub-row-report-dashboard-desc"><?php echo lang('analytic_highest_expenditure_recorded') ?></div>
                                                                                    <div class="sub-row-report-dashboard-val">RM <?php echo number_format($earnings_highest['value'], 2, '.', ','); ?></div>
                                                                                </div>
                                                                                <div class="sub-row-report-dashboard report-dashboard-monthly-month-row">
                                                                                    <div class="sub-row-report-dashboard-desc"><?php echo lang('analytic_highest_number_of_expenditures') ?></div>
                                                                                    <div class="sub-row-report-dashboard-val"><?php echo $earnings_highest['date'] ? ($interval == 'month' ? date('M Y', strtotime($earnings_highest['date'])) : date($cms->settings()['date_format'], strtotime($earnings_highest['date']))) : 'N/A'; ?></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12">
                                        <div class="row">
                                            <div class="col-xl-12 col-report-dashboard">
                                                <div class="analytics-calendar">
                                                    <div class="analytics-calendar-dates-container">
                                                        <div class="analytics-calendar-dates-list">
                                                            <div class="analytics-calendar-item analytics-calendar-nav analytics-calendar-prev">
                                                                <?php 
                                                                if($interval == 'month'){
                                                                    $href = url_for('/analytics') . '?month=' . date('n', strtotime($monthList[2])) . '&year=' . date('Y', strtotime($monthList[2])); 

                                                                }else if($interval == 'day'){
                                                                    $href = url_for('/analytics') . '?interval=day&day=' . date('d', strtotime($dayList[2])) . '&month=' . date('n', strtotime($dayList[2])) . '&year=' . date('Y', strtotime($dayList[2])); 
                                                                }
                                                                ?>
                                                                <button class="filter-param" onClick="location.href='<?php echo $href; ?>'">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                        <path d="M15 18l-6-6 6-6"></path></svg>
                                                                </button>
                                                            </div>
                                                            <div class="analytics-calendar-item analytics-data-container">
                                                                <div class="analytics-calendar-item-container">
                                                                    <div class="analytics-calendar-item-holder">
                                                                        <?php if($interval == 'month'){ ?>
                                                                        <?php foreach($monthList as $i => $month){ ?>
                                                                        <div class="analytics-calendar-date-item<?php echo $i == 3 ? ' active' : ''; ?>">
                                                                            <?php $newparam = $param; $newparam['year'] = date('Y', strtotime($month)); $newparam['month'] = date('n', strtotime($month)); ?>
                                                                            <a href="<?php echo url_for('/analytics') . '?' . http_build_query($newparam); ?>" class="analytics-calendar-date-item-link filter-param">
                                                                                <!--<span class="analytics-calendar-day">Fri</span>-->
                                                                                <span class="analytics-calendar-date"><?php echo date('M Y', strtotime($month)); ?></span>
                                                                            </a>
                                                                        </div>
                                                                        <?php } ?>
                                                                        <?php }else if($interval == 'day'){ ?>
                                                                        <?php foreach($dayList as $i => $day){ ?>
                                                                        <div class="analytics-calendar-date-item<?php echo $i == 3 ? ' active' : ''; ?>">
                                                                            <?php $newparam = $param; $newparam['year'] = date('Y', strtotime($day)); $newparam['month'] = date('n', strtotime($day)); $newparam['day'] = date('d', strtotime($day)); ?>
                                                                            <a href="<?php echo url_for('/analytics') . '?' . http_build_query($newparam); ?>" class="analytics-calendar-date-item-link filter-param">
                                                                                <span class="analytics-calendar-day"><?php echo date('D', strtotime($day)); ?></span>
                                                                                <span class="analytics-calendar-date"><?php echo date($cms->settings()['date_format'], strtotime($day)); ?></span>
                                                                            </a>
                                                                        </div>
                                                                        <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="analytics-calendar-item analytics-calendar-nav analytics-calendar-next">
                                                                <?php
                                                                if($interval == 'month'){
                                                                    $href = url_for('/analytics') . '?month=' . date('n', strtotime($monthList[4])) . '&year=' . date('Y', strtotime($monthList[4]));

                                                                }else if($interval == 'day'){
                                                                    $href = url_for('/analytics') . '?interval=day&day=' . date('d', strtotime($dayList[4])) . '&month=' . date('n', strtotime($dayList[4])) . '&year=' . date('Y', strtotime($dayList[4]));
                                                                }
                                                                ?>
                                                                <button class="filter-param" onClick="location.href='<?php echo $href; ?>'">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                        <path d="M9 18l6-6-6-6"></path></svg>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-analytics-calendar">
                                                    <div class="col-analytics-calendar-top">
                                                        <?php if($type == 'seeker'){ ?>
                                                        <div class="sub-col-report-dashboard report-dashboard-agg-earn-row">
                                                            <div class="sub-row-report-dashboard-val">RM<?php echo number_format($aggregated['earnings'], 2, '.', ','); ?></div>
                                                            <div class="sub-row-report-dashboard-desc"><?php echo lang('analytic_aggregated_earning'); ?></div>
                                                        </div>
                                                        <div class="sub-col-report-dashboard report-dashboard-wellness-row">
                                                            <div class="sub-row-report-dashboard-val"><?php echo $aggregated['hours']; ?></div>
                                                            <div class="sub-row-report-dashboard-desc"><?php echo lang('analytic_aggregated_hours'); ?></div>
                                                        </div>
                                                        <?php } ?>
                                                        <?php if($type == 'poster'){ ?>
                                                        <div class="sub-col-report-dashboard report-dashboard-agg-earn-row job">
                                                            <div class="sub-row-report-dashboard-val">RM<?php echo number_format($aggregated['earnings'], 2, '.', ','); ?></div>
                                                            <div class="sub-row-report-dashboard-desc"><?php echo lang('analytic_aggregated_expenditure'); ?></div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                    <?php if($type == 'seeker'){ ?>                                                   
                                                    <div class="col-analytics-calendar-btm">
                                                        <div class="sub-col-title"><?php echo lang('analytic_task_awarded_this_'.$interval) ?> <?php //echo ucwords($interval); ?></div>
                                                        <div class="analytics-calendar-task-today">
                                                            <?php if($tasks){ foreach($tasks as $task){ ?>
                                                            <div class="tbl-group-calendar-bar-flex">
                                                                <div class="tbl-group-calendar-bar-row row-1">
                                                                    <span class="tbl-group-calendar-bar-title"><?php echo $task['title']; ?></span>
                                                                </div>
                                                                <div class="tbl-group-calendar-bar-row row-2">RM<?php echo number_format($task['budget'], 2, '.', ','); ?></div>
                                                                <div class="tbl-group-calendar-bar-row row-3">
                                                                    <span class="tbl-taskcentre-completeby"><?php echo date($cms->settings()['date_format'], strtotime($task['assigned'])); ?></span></div>
                                                                <div class="tbl-group-calendar-bar-row row-4">
                                                                    <a href="<?php echo url_for('/workspace/task/' . $task['slug'] . '/details'); ?>" class="btn-txt-only btn-edit">
	                                                                    <?php echo lang('task_view_task_details') ?>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <?php }}else{ ?>
                                                            <div class="tbl-group-calendar-bar-flex px-3">
                                                                <div class=""><i><?php echo lang('analytic_no_data'); ?></i></div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-analytics-calendar-btm">
                                                        <div class="sub-col-title"><?php echo lang('analytic_task_completed_this_'.$interval) ?> <?php //echo ucwords($interval); ?></div>
                                                        <div class="analytics-calendar-task-today">
                                                            <?php if($tasks_completed){ foreach($tasks_completed as $task){ ?>
                                                            <div class="tbl-group-calendar-bar-flex">
                                                                <div class="tbl-group-calendar-bar-row row-1">
                                                                    <span class="tbl-group-calendar-bar-title"><?php echo $task['title']; ?></span>
                                                                </div>
                                                                <?php
                                                                    if($task['status'] == 'disputed'){
                                                                        $percent = 100 - $task['rejection_rate'];
                                                                        $task['budget'] = $task['budget'] * ($percent / 100);
                                                                    }
                                                                ?>
                                                                <div class="tbl-group-calendar-bar-row row-2">RM<?php echo number_format($task['budget'], 2, '.', ','); ?></div>
                                                                <div class="tbl-group-calendar-bar-row row-3">
                                                                    <span class="tbl-taskcentre-completeby"><?php echo date($cms->settings()['date_format'], strtotime($task['updated_at'])); ?></span></div>
                                                                <div class="tbl-group-calendar-bar-row row-4">
                                                                    <a href="<?php echo url_for('/workspace/task/' . $task['slug'] . '/details'); ?>" class="btn-txt-only btn-edit">
                                                                        <?php echo lang('task_view_task_details') ?>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <?php }}else{ ?>
                                                            <div class="tbl-group-calendar-bar-flex px-3">
                                                                <div class=""><i><?php echo lang('analytic_no_data'); ?></i></div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <?php } ?>

                                                    <?php if($type == 'poster'){ ?>                                                    
                                                    <div class="col-analytics-calendar-btm">
                                                        <div class="sub-col-title"><?php echo lang('analytic_task_posted_this_'.$interval) ?> <?php //echo ucwords($interval); ?></div>
                                                        <div class="analytics-calendar-task-today">
                                                            <?php if($tasks){ foreach($tasks as $task){ ?>
                                                            <div class="tbl-group-calendar-bar-flex">
                                                                <div class="tbl-group-calendar-bar-row row-1">
                                                                    <span class="tbl-group-calendar-bar-title"><?php echo $task['title']; ?></span>
                                                                </div>
                                                                <div class="tbl-group-calendar-bar-row row-2">RM<?php echo number_format($task['budget'], 2, '.', ','); ?></div>
                                                                <div class="tbl-group-calendar-bar-row row-3">
                                                                    <span class="tbl-taskcentre-completeby"><?php echo date($cms->settings()['date_format'], strtotime($task['created_at'])); ?></span></div>
                                                                <div class="tbl-group-calendar-bar-row row-4">
                                                                    <a href="<?php echo url_for('/workspace/task/' . $task['slug'] . '/details'); ?>" class="btn-txt-only btn-edit">
	                                                                    <?php echo lang('task_view_task_details') ?>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <?php }}else{ ?>
                                                            <div class="tbl-group-calendar-bar-flex px-3">
                                                                <div class=""><i><?php echo lang('analytic_no_data'); ?></i></div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-analytics-calendar-btm">
                                                        <div class="sub-col-title"><?php echo lang('analytic_task_completed_this_'.$interval) ?> <?php //echo ucwords($interval); ?></div>
                                                        <div class="analytics-calendar-task-today">
                                                            <?php if($tasks_completed){ foreach($tasks_completed as $task){ ?>
                                                            <div class="tbl-group-calendar-bar-flex">
                                                                <div class="tbl-group-calendar-bar-row row-1">
                                                                    <span class="tbl-group-calendar-bar-title"><?php echo $task['title']; ?></span>
                                                                </div>
                                                                <?php
                                                                    if($task['status'] == 'disputed'){
                                                                        $percent = 100 - $task['rejection_rate'];
                                                                        $task['budget'] = $task['budget'] * ($percent / 100);
                                                                    }
                                                                ?>
                                                                <div class="tbl-group-calendar-bar-row row-2">RM<?php echo number_format($task['budget'], 2, '.', ','); ?></div>
                                                                <div class="tbl-group-calendar-bar-row row-3">
                                                                    <span class="tbl-taskcentre-completeby"><?php echo date($cms->settings()['date_format'], strtotime($task['updated_at'])); ?></span></div>
                                                                <div class="tbl-group-calendar-bar-row row-4">
                                                                    <a href="<?php echo url_for('/workspace/task/' . $task['slug'] . '/details'); ?>" class="btn-txt-only btn-edit">
                                                                        <?php echo lang('task_view_task_details') ?>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <?php }}else{ ?>
                                                            <div class="tbl-group-calendar-bar-flex px-3">
                                                                <div class=""><i><?php echo lang('analytic_no_data'); ?></i></div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
			</div>
		</div>
	</div>
</div>

    <!-- Chart JS -->
    <script src='<?php echo url_for('/assets/js/plugins/snap.svg-min.js') ?>'></script>
	<script src="<?php echo url_for('/assets/js/plugins/chart.min.js') ?>"></script>
	<script src="<?php echo url_for('/assets/js/plugins/progressbar.js') ?>"></script>
	<script src="<?php echo url_for('/assets/js/plugins/d3.v3.min.js') ?>"></script>
    
    <!-- Bar Chart Time Function -->
    <script>
        $(document).on('change', '[name="analyticsRadioSelect"]', function(){
            location.href = $('[name="analyticsRadioSelect"]:checked').data('url');
        });

        $(document).on('click', '#pills-tab .nav-item a', function(){
            id = $(this).attr('href');
            // byHoursMonthTab
            // byEarningsMonthTab
            
            $('.filter-param').each(function(){
                el = $(this);
                if( el.prop('tagName') == 'A' ){
                    href = el.attr('href');
                    if (/#/i.test(href)){
                        href = href.replace('#byHoursMonthTab', '');
                        href = href.replace('#byEarningsMonthTab', '');
                        if(id != 'byHoursMonthTab'){
                            href = href + id;
                        }
                    }else{
                        href = href + id;
                    }
                    el.attr('href', href);

                }else if(el.prop('tagName') == 'BUTTON'){
                    href = el.attr('onclick');
                    if (/#/i.test(href)){
                        href = href.replace('#byHoursMonthTab', '');
                        href = href.replace('#byEarningsMonthTab', '');
                        if(id != 'byHoursMonthTab'){
                            //href = href + id;
                        }
                    }else{
                        href = href + id;
                    }
                    el.attr('onclick', href);
                }
            });
        });

        window.onload = function() {
            <?php if($type == 'seeker'){ ?>
            var ctx = document.getElementById('barchart_canvas_time').getContext('2d');
            var ctx2 = document.getElementById('barchart_canvas_money').getContext('2d');
            var gradient = ctx.createLinearGradient(0, 0, 0, 400);
            var gradient2 = ctx2.createLinearGradient(0, 0, 0, 400);
            gradient.addColorStop(0, 'rgba(255,133,99,1)');
            gradient.addColorStop(1, 'rgba(237,89,43,1)');
            gradient2.addColorStop(0, 'rgba(100,175,255,1)');
            gradient2.addColorStop(1, 'rgba(0,123,255,1)');

            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: <?php echo json_encode($labels); ?>,
                    datasets: [{
                        data: <?php echo json_encode(array_values($hours)); ?>,
                        backgroundColor: [<?php foreach(range(1, sizeof($labels)) as $i){ echo 'gradient,'; } ?>],
                        barPercentage: 0.5,
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false,
                    },
                    title: {
                        display: false,
                    },
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false,
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                borderDash: [8, 4],
                                color: "#eeeeee",
                                drawBorder: false,
                                lineWidth: 1,
                            },
                            ticks: {
                                display: false,
                                drawBorder: false
                            },
                        }]
                    }
                }
            });
            
            window.myBar2 = new Chart(ctx2, {
                type: 'bar',
                data: {
                    labels: <?php echo json_encode($labels); ?>,
                    datasets: [{
                        data: <?php echo json_encode(array_values($earnings)); ?>,
                        backgroundColor: [<?php foreach(range(1, sizeof($labels)) as $i){ echo 'gradient2,'; } ?>],
                        barPercentage: 0.5,
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false,
                    },
                    title: {
                        display: false,
                    },
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false,
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                borderDash: [8, 4],
                                color: "#eeeeee",
                                drawBorder: false,
                                lineWidth: 1,
                            },
                            ticks: {
                                display: false,
                                drawBorder: false
                            },
                        }]
                    }
                }
            });
            <?php } ?>

            <?php if($type == 'poster'){ ?>
            var ctx2 = document.getElementById('barchart_canvas_money').getContext('2d');
            var gradient2 = ctx2.createLinearGradient(0, 0, 0, 400);
            gradient2.addColorStop(0, 'rgba(100,175,255,1)');
            gradient2.addColorStop(1, 'rgba(0,123,255,1)');

            window.myBar2 = new Chart(ctx2, {
                type: 'bar',
                data: {
                    labels: <?php echo json_encode($labels); ?>,
                    datasets: [{
                        data: <?php echo json_encode(array_values($earnings)); ?>,
                        backgroundColor: [<?php foreach(range(1, sizeof($labels)) as $i){ echo 'gradient2,'; } ?>],
                        barPercentage: 0.5,
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false,
                    },
                    title: {
                        display: false,
                    },
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false,
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                borderDash: [8, 4],
                                color: "#eeeeee",
                                drawBorder: false,
                                lineWidth: 1,
                            },
                            ticks: {
                                display: false,
                                drawBorder: false
                            },
                        }]
                    }
                }
            });
            <?php } ?>

            // Bar Chart Rounded Corner custom
            Chart.elements.Rectangle.prototype.draw = function() {

            var ctx = this._chart.ctx;
            var vm = this._view;
            var left, right, top, bottom, signX, signY, borderSkipped, radius;
            var borderWidth = vm.borderWidth;
            // Set Radius Here
            // If radius is large enough to cause drawing errors a max radius is imposed
            var cornerRadius = 12;

            if (!vm.horizontal) {
                // bar
                left = vm.x - vm.width / 2;
                right = vm.x + vm.width / 2;
                top = vm.y;
                bottom = vm.base;
                signX = 1;
                signY = bottom > top ? 1 : -1;
                borderSkipped = vm.borderSkipped || 'bottom';
            } else {
                // horizontal bar
                left = vm.base;
                right = vm.x;
                top = vm.y - vm.height / 2;
                bottom = vm.y + vm.height / 2;
                signX = right > left ? 1 : -1;
                signY = 1;
                borderSkipped = vm.borderSkipped || 'left';
            }

            // Canvas doesn't allow us to stroke inside the width so we can
            // adjust the sizes to fit if we're setting a stroke on the line
            if (borderWidth) {
                // borderWidth shold be less than bar width and bar height.
                var barSize = Math.min(Math.abs(left - right), Math.abs(top - bottom));
                borderWidth = borderWidth > barSize ? barSize : borderWidth;
                var halfStroke = borderWidth / 2;
                // Adjust borderWidth when bar top position is near vm.base(zero).
                var borderLeft = left + (borderSkipped !== 'left' ? halfStroke * signX : 0);
                var borderRight = right + (borderSkipped !== 'right' ? -halfStroke * signX : 0);
                var borderTop = top + (borderSkipped !== 'top' ? halfStroke * signY : 0);
                var borderBottom = bottom + (borderSkipped !== 'bottom' ? -halfStroke * signY : 0);
                // not become a vertical line?
                if (borderLeft !== borderRight) {
                    top = borderTop;
                    bottom = borderBottom;
                }
                // not become a horizontal line?
                if (borderTop !== borderBottom) {
                    left = borderLeft;
                    right = borderRight;
                }
            }

            ctx.beginPath();
            ctx.fillStyle = vm.backgroundColor;
            ctx.strokeStyle = vm.borderColor;
            ctx.lineWidth = borderWidth;

            // Corner points, from bottom-left to bottom-right clockwise
            // | 1 2 |
            // | 0 3 |
            var corners = [
                [left, bottom],
                [left, top],
                [right, top],
                [right, bottom]
            ];

            // Find first (starting) corner with fallback to 'bottom'
            var borders = ['bottom', 'left', 'top', 'right'];
            var startCorner = borders.indexOf(borderSkipped, 0);
            if (startCorner === -1) {
                startCorner = 0;
            }

            function cornerAt(index) {
                return corners[(startCorner + index) % 4];
            }

            // Draw rectangle from 'startCorner'
            var corner = cornerAt(0);
            ctx.moveTo(corner[0], corner[1]);

            for (var i = 1; i < 4; i++) {
                corner = cornerAt(i);
                nextCornerId = i + 1;
                if (nextCornerId == 4) {
                    nextCornerId = 0
                }

                nextCorner = cornerAt(nextCornerId);

                width = corners[2][0] - corners[1][0];
                height = corners[0][1] - corners[1][1];
                x = corners[1][0];
                y = corners[1][1];

                var radius = cornerRadius;

                // Fix radius being too large
                if (radius > height / 2) {
                    radius = height / 2;
                }
                if (radius > width / 2) {
                    radius = width / 2;
                }

                ctx.moveTo(x + radius, y);
                ctx.lineTo(x + width - radius, y);
                ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
                ctx.lineTo(x + width, y + height - radius);
                ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
                ctx.lineTo(x + radius, y + height);
                ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
                ctx.lineTo(x, y + radius);
                ctx.quadraticCurveTo(x, y, x + radius, y);

            }

            ctx.fill();
            if (borderWidth) {
                ctx.stroke();
            }
            };
            // Bar Chart Rounded Corner custom
        };

    </script>
    <!-- Bar Chart Time Function -->