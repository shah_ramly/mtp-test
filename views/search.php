<!DOCTYPE html>
<html lang="<?php echo !isset($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) || ($_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] == 'EN') ? 'en':'ms' ?>">


<body class="white-content od-mode">
   
                           
                            <div class="card-body-ratingcentre">
                                <ul class="nav nav-pills nav-pills-ratingcentre" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="searchTaskTab-tab" data-toggle="pill" href="#searchTaskTab" role="tab" aria-controls="searchTaskTab" aria-selected="true">Task</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="searchTalentTab-tab" data-toggle="pill" href="#searchTalentTab" role="tab" aria-controls="searchTalentTab" aria-selected="false">Talent</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="searchTaskTab" role="tabpanel">
                                        <div class="col-xl-12">
                                            <div class="row">
                                                <div class="col-xl-12 col-tasklisting-top">
                                                    <div class="col-public-search-wrapper">
                                                        <div class="col-xl-12 col-public-search">
                                                            <form action="#" id="searchForm" class=" search-public-form">
                                                                <div class="search-dropdown-filter-top-wrapper form-flex">
                                                                    <div class="search-dropdown-filter-container">
                                                                        <input type="text" class="form-control form-control-input" placeholder="What are your talents?" id="inputBox">
                                                                    </div>
                                                                    <span class="search-dropdown-filter-btn">
                                                                        <button class="btn-search-public" type="submit">
                                                                            <span class="public-search-icon search-icon">
                                                                                <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                    <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                                                                    <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                                                                </svg>
                                                                            </span>
                                                                        </button>
                                                                    </span>
                                                                    <span class="bookmark-dropdown-filter-btn dropdown">
                                                                        <button class="btn-bookmark-public dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                            <span class="public-search-icon search-icon">
                                                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bookmarks" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                    <path fill-rule="evenodd" d="M7 13l5 3V4a2 2 0 0 0-2-2H4a2 2 0 0 0-2 2v12l5-3zm-4 1.234l4-2.4 4 2.4V4a1 1 0 0 0-1-1H4a1 1 0 0 0-1 1v10.234z" />
                                                                                    <path d="M14 14l-1-.6V2a1 1 0 0 0-1-1H4.268A2 2 0 0 1 6 0h6a2 2 0 0 1 2 2v12z" />
                                                                                </svg>
                                                                            </span>
                                                                            <i class="fa fa-caret-down"></i>
                                                                        </button>
                                                                        <div class="dropdown-menu dropdown-menu-saved-search">
                                                                            <div class="modal-advance-filter-container-left">
                                                                                <div class="modal-advanced-filter-container">
                                                                                    <div class="keyword-filter-container">
                                                                                        <label>Keyword</label>
                                                                                        <span>cooking, malaysia, personal, by the hour, project management, sales marketing, teamwork, thought process</span>
                                                                                    </div>
                                                                                    <label class="modal-search-filter-lbl">Save Search</label>
                                                                                    <div class="input-group-flex row-indfirstname">
                                                                                        <input type="text" id="Name" class="form-control form-control-input" placeholder="Name" required="" autofocus="">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
                                                                                    <div class="modal-filter-action form-block-filter-flex">
                                                                                        <button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                                                                                            <span class="btn-label">Save</span>
                                                                                            <span class="btn-icon">
                                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                                                    <polyline points="20 6 9 17 4 12"></polyline>
                                                                                                </svg>
                                                                                            </span>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </span>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>

                                                    <div class="col-task-listing-wrapper">
                                                        <div class="row">
                                                            <div class="col-xl-3 col-task-filter-wrapper">
                                                                <div class="col-task-filter-container">
                                                                    <div class="task-filter-row task-filter-row-location">
                                                                        <label class="task-filter-lbl">Filter by task or job</label>
                                                                        <div class="form-block-task-filter">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtertj_1">
                                                                                <label class="custom-control-label" for="filtertj_1">Task</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtertj_2">
                                                                                <label class="custom-control-label" for="filtertj_2">Job</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-filter-row task-filter-row-location">
                                                                        <button class="btn-link btn-link-filter btn-flex" type="button" aria-expanded="false">
                                                                            <label class="task-filter-lbl">Filter by category</label>
                                                                            <span class="chevron-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M6 9l6 6 6-6"/></svg>
                                                                            </span>
                                                                        </button>
                                                                        <div class="form-filter-collapse collapse">
                                                                            <div class="form-block-task-filter">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_0">
                                                                                <label class="custom-control-label" for="filtercat_0">All category</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_1">
                                                                                <label class="custom-control-label" for="filtercat_1">Administration</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_2">
                                                                                <label class="custom-control-label" for="filtercat_2">Art & Design</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_3">
                                                                                <label class="custom-control-label" for="filtercat_3">Business</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_4">
                                                                                <label class="custom-control-label" for="filtercat_4">Communication & Interpersonal</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_5">
                                                                                <label class="custom-control-label" for="filtercat_5">Education</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_6">
                                                                                <label class="custom-control-label" for="filtercat_6">Engineering</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_7">
                                                                                <label class="custom-control-label" for="filtercat_7">Finance</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_8">
                                                                                <label class="custom-control-label" for="filtercat_8">HR & Recruitment</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_9">
                                                                                <label class="custom-control-label" for="filtercat_9">IT & Data Management</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_10">
                                                                                <label class="custom-control-label" for="filtercat_10">Management</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_11">
                                                                                <label class="custom-control-label" for="filtercat_11">Office</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_12">
                                                                                <label class="custom-control-label" for="filtercat_12">Personal</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_13">
                                                                                <label class="custom-control-label" for="filtercat_13">Project Management</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_14">
                                                                                <label class="custom-control-label" for="filtercat_14">Sales Marketing</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_15">
                                                                                <label class="custom-control-label" for="filtercat_15">Teamwork</label>
                                                                            </div>
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="filtercat_16">
                                                                                <label class="custom-control-label" for="filtercat_16">Thought Process</label>
                                                                            </div>
                                                                        </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-filter-row task-filter-row-location">
                                                                        <button class="btn-link btn-link-filter btn-flex" type="button" aria-expanded="false">
                                                                            <label class="task-filter-lbl">Filter by location</label>
                                                                            <span class="chevron-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M6 9l6 6 6-6"/></svg>
                                                                            </span>
                                                                        </button>
                                                                        <div class="form-filter-collapse collapse">
                                                                            <div class="form-block-task-filter">
                                                                                <div class="form-filter-state">
                                                                                    <select class="form-control form-control-input" placeholder="State">
                                                                                        <option disabled="" selected="">State</option>
                                                                                        <option value="">Kuala Lumpur</option>
                                                                                        <option value="">Selangor</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="form-filter-city">
                                                                                    <select class="form-control form-control-input" placeholder="City">
                                                                                        <option disabled="" selected="">Town/City</option>
                                                                                        <option value="">Petaling Jaya</option>
                                                                                        <option value="">Shah Alam</option>
                                                                                        <option value="">Subang Jaya</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-filter-row task-filter-row-location">
                                                                        <button class="btn-link btn-link-filter btn-flex" type="button" aria-expanded="false">
                                                                            <label class="task-filter-lbl">Filter by nature of task</label>
                                                                            <span class="chevron-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M6 9l6 6 6-6"/></svg>
                                                                            </span>
                                                                        </button>
                                                                        <div class="form-filter-collapse collapse">
                                                                            <div class="form-block-task-filter">
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filternat_1">
                                                                                    <label class="custom-control-label" for="filternat_1">By the hour</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filternat_2">
                                                                                    <label class="custom-control-label" for="filternat_2">Lump sum</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-filter-row task-filter-row-budget">
                                                                        <button class="btn-link btn-link-filter btn-flex" type="button" aria-expanded="false">
                                                                            <label class="task-filter-lbl">Filter by budget per hour</label>
                                                                            <span class="chevron-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M6 9l6 6 6-6"/></svg>
                                                                            </span>
                                                                        </button>
                                                                        <div class="form-filter-collapse collapse">
                                                                            <div class="form-block-task-filter">
                                                                                <input id="range_budget_hour_filter" type="text" />
                                                                                <span class="estimated-flex">
                                                                                    <span class="estimated-lbl estimated-from">RM10</span>
                                                                                    <span class="estimated-lbl estimated-to">RM100</span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-filter-row task-filter-row-budget">
                                                                        <button class="btn-link btn-link-filter btn-flex" type="button" aria-expanded="false">
                                                                            <label class="task-filter-lbl">Filter by budget per task</label>
                                                                            <span class="chevron-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M6 9l6 6 6-6"/></svg>
                                                                            </span>
                                                                        </button>
                                                                        <div class="form-filter-collapse collapse">
                                                                            <div class="form-block-task-filter">
                                                                                <input id="range_budget_lumpsum_filter" type="text" />
                                                                                <span class="estimated-flex">
                                                                                    <span class="estimated-lbl estimated-from">RM10</span>
                                                                                    <span class="estimated-lbl estimated-to">RM1000</span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-filter-row task-filter-row-dateposted">
                                                                        <button class="btn-link btn-link-filter btn-flex" type="button" aria-expanded="false">
                                                                            <label class="task-filter-lbl">Filter by date</label>
                                                                            <span class="chevron-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M6 9l6 6 6-6"/></svg>
                                                                            </span>
                                                                        </button>
                                                                        <div class="form-filter-collapse collapse">
                                                                            <div class="form-block-task-filter">
                                                                                <div class="form-filter-datepostedstart">
                                                                                    <div class="form-filter-datepostedstart">
                                                                                        <select class="form-control form-control-input" placeholder="<?php echo lang('public_search_posted_date'); ?>">
                                                                                            <option selected=""><?php echo lang('public_search_select'); ?></option>
                                                                                            <option value=""><?php echo lang('public_search_posted_date'); ?></option>
                                                                                            <option value=""><?php echo lang('public_search_start_date'); ?></option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-filter-dateposted">
                                                                                    <div class="form-filter-dateposted">
                                                                                        <select class="form-control form-control-input" id="search_filter_dateposted" placeholder="<?php echo lang('public_search_posted_date'); ?>">
                                                                                            <option disabled="" selected=""><?php echo lang('public_search_select'); ?></option>
                                                                                            <option value=""><?php echo lang('public_search_any_time'); ?></option>
                                                                                            <option value=""><?php echo sprintf(lang('public_search_last_x_hours'), 24); ?></option>
                                                                                            <option value=""><?php echo sprintf(lang('public_search_last_x_days'), 3); ?></option>
                                                                                            <option value=""><?php echo sprintf(lang('public_search_last_x_days'), 7); ?></option>
                                                                                            <option value=""><?php echo sprintf(lang('public_search_last_x_days'), 14); ?></option>
                                                                                            <option value=""><?php echo sprintf(lang('public_search_last_x_days'), 30); ?></option>
                                                                                            <option value="customrange"><?php echo lang('public_search_custom_range'); ?></option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="task-filter-row task-filter-row-datepicker-fromto" style="display:none;">
                                                                                        <div class="form-group form-datetime-search-filter-from">
                                                                                            <div class="form-group">
                                                                                                <div class="input-group input-group-datetimepicker date" id="form_datetime_search_filter_from" data-target-input="nearest">
                                                                                                    <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_search_filter_from" placeholder="<?php echo lang('public_search_date_from'); ?>" />
                                                                                                    <span class="input-group-addon" data-target="#form_datetime_search_filter_from" data-toggle="datetimepicker">
                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                    </span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                        <div class="form-group form-datetime-search-filter-to">
                                                                                            <div class="form-group">
                                                                                                <div class="input-group input-group-datetimepicker date" id="form_datetime_search_filter_to" data-target-input="nearest">
                                                                                                    <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_search_filter_to" placeholder="<?php echo lang('public_search_date_to'); ?>" />
                                                                                                    <span class="input-group-addon" data-target="#form_datetime_search_filter_to" data-toggle="datetimepicker">
                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                    </span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-filter-row task-filter-row-skills">
                                                                        <button class="btn-link btn-link-filter btn-flex" type="button" aria-expanded="false">
                                                                            <label class="task-filter-lbl">Filter by skills</label>
                                                                            <span class="chevron-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M6 9l6 6 6-6"/></svg>
                                                                            </span>
                                                                        </button>
                                                                        <div class="form-filter-collapse collapse">
                                                                            <div class="form-block-task-filter">
                                                                                <div class="bstags-skills">
                                                                                    <input type="text" name="tagss" data-role="tagsinput" class="bootstrap-tagsinput" placeholder="Enter skills..." />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-filter-row task-filter-row-reset">
                                                                        <a href="#" class="reset-filter-link">Reset all filters</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xl-9 col-task-row-wrapper">
                                                                <div class="col-task-row-container">
                                                                    <div class="task-row-col-top-flex">
                                                                        <div class="task-row-col-left">
                                                                            <a href="task-details.html" class="task-row-taskname">Conduct an online pottery class</a>
                                                                            <div class="task-row-taskdesc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                                        </div>
                                                                        <div class="task-row-col-right">
                                                                            <div class="task-row-taskamount">
                                                                                <span class="taskamount-prefix">RM</span>
                                                                                <span class="taskamount-val">440</span>
                                                                                <span class="taskamount-suffix">.00</span>
                                                                            </div>
                                                                            <div class="task-row-taskduration">
                                                                                <span class="taskduration-val">6 hours task</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-row-col-btm-flex">
                                                                        <div class="task-row-col-left">
                                                                            <label class="task-listing-lbl">About the Poster:</label>
                                                                            <div class="task-row-posterdetails">
                                                                                <div class="task-row-posterdetails-val">
                                                                                    <div class="task-details-profile-location">
                                                                                        <span class="task-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                                                                            </svg></span>
                                                                                        <span class="task-details-profile-location-val">Kuala Lumpur, Malaysia</span>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="task-row-rating-flex">
                                                                                    <div class="task-row-rating-star">
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                                                    </div>
                                                                                    <div class="task-row-rating-val">
                                                                                        <span class="task-row-average-rating-val">4.8</span>
                                                                                        <span class="task-row-divider">/</span>
                                                                                        <span class="task-row-total-rating-val">5 reviews</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="task-row-xtrainfo">
                                                                                <span class="task-details-type">On Demand</span>
                                                                                <div class="task-row-taskid">
                                                                                    <span class="task-row-taskid-lbl">Task ID:</span>
                                                                                    <span class="task-row-taskid-val">#2032211</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="task-row-col-right">
                                                                            <div class="task-row-actions">
                                                                                <button type="button" class="btn-icon-full btn-fav">
                                                                                    <span class="btn-label">Favourite</span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                                <button type="button" class="btn-icon-full btn-apply" data-toggle="modal" data-target="#modal_apply_question">
                                                                                    <span class="btn-label">Apply</span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                                                                            <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-task-row-container">
                                                                    <div class="task-row-col-top-flex">
                                                                        <div class="task-row-col-left">
                                                                            <a href="task-details.html" class="task-row-taskname">Conduct an online pottery class</a>
                                                                            <div class="task-row-taskdesc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                                        </div>
                                                                        <div class="task-row-col-right">
                                                                            <div class="task-row-taskamount">
                                                                                <span class="taskamount-prefix">RM</span>
                                                                                <span class="taskamount-val">440</span>
                                                                                <span class="taskamount-suffix">.00</span>
                                                                            </div>
                                                                            <div class="task-row-taskduration">
                                                                                <span class="taskduration-val">6 hours task</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-row-col-btm-flex">
                                                                        <div class="task-row-col-left">
                                                                            <label class="task-listing-lbl">About the Poster:</label>
                                                                            <div class="task-row-posterdetails">
                                                                                <div class="task-row-posterdetails-val">
                                                                                    <div class="task-details-profile-location">
                                                                                        <span class="task-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                                                                            </svg></span>
                                                                                        <span class="task-details-profile-location-val">Kuala Lumpur, Malaysia</span>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="task-row-rating-flex">
                                                                                    <div class="task-row-rating-star">
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                                                    </div>
                                                                                    <div class="task-row-rating-val">
                                                                                        <span class="task-row-average-rating-val">4.8</span>
                                                                                        <span class="task-row-divider">/</span>
                                                                                        <span class="task-row-total-rating-val">5 reviews</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="task-row-xtrainfo">
                                                                                <span class="task-details-type">On Demand</span>
                                                                                <div class="task-row-taskid">
                                                                                    <span class="task-row-taskid-lbl">Task ID:</span>
                                                                                    <span class="task-row-taskid-val">#2032211</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="task-row-col-right">
                                                                            <div class="task-row-actions">
                                                                                <button type="button" class="btn-icon-full btn-fav">
                                                                                    <span class="btn-label">Favourite</span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                                <button type="button" class="btn-icon-full btn-apply" data-toggle="modal" data-target="#modal_apply_question">
                                                                                    <span class="btn-label">Apply</span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                                                                            <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-task-row-container">
                                                                    <div class="task-row-col-top-flex">
                                                                        <div class="task-row-col-left">
                                                                            <a href="task-details.html" class="task-row-taskname">Conduct an online pottery class</a>
                                                                            <div class="task-row-taskdesc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                                        </div>
                                                                        <div class="task-row-col-right">
                                                                            <div class="task-row-taskamount">
                                                                                <span class="taskamount-prefix">RM</span>
                                                                                <span class="taskamount-val">440</span>
                                                                                <span class="taskamount-suffix">.00</span>
                                                                            </div>
                                                                            <div class="task-row-taskduration">
                                                                                <span class="taskduration-val">6 hours task</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-row-col-btm-flex">
                                                                        <div class="task-row-col-left">
                                                                            <label class="task-listing-lbl">About the Poster:</label>
                                                                            <div class="task-row-posterdetails">
                                                                                <div class="task-row-posterdetails-val">
                                                                                    <div class="task-details-profile-location">
                                                                                        <span class="task-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                                                                            </svg></span>
                                                                                        <span class="task-details-profile-location-val">Kuala Lumpur, Malaysia</span>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="task-row-rating-flex">
                                                                                    <div class="task-row-rating-star">
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                                                    </div>
                                                                                    <div class="task-row-rating-val">
                                                                                        <span class="task-row-average-rating-val">4.8</span>
                                                                                        <span class="task-row-divider">/</span>
                                                                                        <span class="task-row-total-rating-val">5 reviews</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="task-row-xtrainfo">
                                                                                <span class="task-details-type">On Demand</span>
                                                                                <div class="task-row-taskid">
                                                                                    <span class="task-row-taskid-lbl">Task ID:</span>
                                                                                    <span class="task-row-taskid-val">#2032211</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="task-row-col-right">
                                                                            <div class="task-row-actions">
                                                                                <button type="button" class="btn-icon-full btn-fav">
                                                                                    <span class="btn-label">Favourite</span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                                <button type="button" class="btn-icon-full btn-apply" data-toggle="modal" data-target="#modal_apply_question">
                                                                                    <span class="btn-label">Apply</span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                                                                            <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-task-row-container">
                                                                    <div class="task-row-col-top-flex">
                                                                        <div class="task-row-col-left">
                                                                            <a href="task-details.html" class="task-row-taskname">Conduct an online pottery class</a>
                                                                            <div class="task-row-taskdesc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                                        </div>
                                                                        <div class="task-row-col-right">
                                                                            <div class="task-row-taskamount">
                                                                                <span class="taskamount-prefix">RM</span>
                                                                                <span class="taskamount-val">440</span>
                                                                                <span class="taskamount-suffix">.00</span>
                                                                            </div>
                                                                            <div class="task-row-taskduration">
                                                                                <span class="taskduration-val">6 hours task</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-row-col-btm-flex">
                                                                        <div class="task-row-col-left">
                                                                            <label class="task-listing-lbl">About the Poster:</label>
                                                                            <div class="task-row-posterdetails">
                                                                                <div class="task-row-posterdetails-val">
                                                                                    <div class="task-details-profile-location">
                                                                                        <span class="task-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                                                                            </svg></span>
                                                                                        <span class="task-details-profile-location-val">Kuala Lumpur, Malaysia</span>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="task-row-rating-flex">
                                                                                    <div class="task-row-rating-star">
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                                                    </div>
                                                                                    <div class="task-row-rating-val">
                                                                                        <span class="task-row-average-rating-val">4.8</span>
                                                                                        <span class="task-row-divider">/</span>
                                                                                        <span class="task-row-total-rating-val">5 reviews</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="task-row-xtrainfo">
                                                                                <span class="task-details-type">On Demand</span>
                                                                                <div class="task-row-taskid">
                                                                                    <span class="task-row-taskid-lbl">Task ID:</span>
                                                                                    <span class="task-row-taskid-val">#2032211</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="task-row-col-right">
                                                                            <div class="task-row-actions">
                                                                                <button type="button" class="btn-icon-full btn-fav">
                                                                                    <span class="btn-label">Favourite</span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                                <button type="button" class="btn-icon-full btn-apply" data-toggle="modal" data-target="#modal_apply_question">
                                                                                    <span class="btn-label">Apply</span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                                                                            <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-task-row-container">
                                                                    <div class="task-row-col-top-flex">
                                                                        <div class="task-row-col-left">
                                                                            <a href="task-details.html" class="task-row-taskname">Conduct an online pottery class</a>
                                                                            <div class="task-row-taskdesc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                                        </div>
                                                                        <div class="task-row-col-right">
                                                                            <div class="task-row-taskamount">
                                                                                <span class="taskamount-prefix">RM</span>
                                                                                <span class="taskamount-val">440</span>
                                                                                <span class="taskamount-suffix">.00</span>
                                                                            </div>
                                                                            <div class="task-row-taskduration">
                                                                                <span class="taskduration-val">6 hours task</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-row-col-btm-flex">
                                                                        <div class="task-row-col-left">
                                                                            <label class="task-listing-lbl">About the Poster:</label>
                                                                            <div class="task-row-posterdetails">
                                                                                <div class="task-row-posterdetails-val">
                                                                                    <div class="task-details-profile-location">
                                                                                        <span class="task-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                                                                            </svg></span>
                                                                                        <span class="task-details-profile-location-val">Kuala Lumpur, Malaysia</span>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="task-row-rating-flex">
                                                                                    <div class="task-row-rating-star">
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                                                    </div>
                                                                                    <div class="task-row-rating-val">
                                                                                        <span class="task-row-average-rating-val">4.8</span>
                                                                                        <span class="task-row-divider">/</span>
                                                                                        <span class="task-row-total-rating-val">5 reviews</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="task-row-xtrainfo">
                                                                                <span class="task-details-type">On Demand</span>
                                                                                <div class="task-row-taskid">
                                                                                    <span class="task-row-taskid-lbl">Task ID:</span>
                                                                                    <span class="task-row-taskid-val">#2032211</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="task-row-col-right">
                                                                            <div class="task-row-actions">
                                                                                <button type="button" class="btn-icon-full btn-fav">
                                                                                    <span class="btn-label">Favourite</span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                                <button type="button" class="btn-icon-full btn-apply" data-toggle="modal" data-target="#modal_apply_question">
                                                                                    <span class="btn-label">Apply</span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                                                                            <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--<div class="col-task-row-container">
                                                                    <div class="task-row-col-top-flex">
                                                                        <div class="task-row-col-left">
                                                                            <a href="task-details.html" class="task-row-taskname">Conduct an online pottery class</a>
                                                                            <div class="task-row-taskdesc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                                        </div>
                                                                        <div class="task-row-col-right">
                                                                            <div class="task-row-taskamount">
                                                                                <span class="taskamount-prefix">RM</span>
                                                                                <span class="taskamount-val">440</span>
                                                                                <span class="taskamount-suffix">.00</span>
                                                                            </div>
                                                                            <div class="task-row-taskduration">
                                                                                <span class="taskduration-val">6 hours task</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-row-col-btm-flex">
                                                                        <div class="task-row-col-left">
                                                                            <div class="task-row-posterdetails">
                                                                                <div class="task-row-posterdetails-val">
                                                                                    <a href="#" class="posterdetail-link dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                                                                        <div class="tbl-group-calendar-photo">
                                                                                            <img src="../assets/img/james.jpg" alt="Profile Photo">
                                                                                        </div>
                                                                                        <span class="posterdetail-name">Tim Hover</span>
                                                                                    </a>
                                                                                    <div class="dropdown-menu dropdown-menu-profile-details">
                                                                                        <div class="task-details-row task-details-profile">
                                                                                            <div class="profile-avatar">
                                                                                                <a href="#" class="task-details-poster-link"><img class="avatar-img" src="../assets/img/mike.jpg" alt=""></a>
                                                                                            </div>
                                                                                            <a href="#" class="task-details-poster-link">
                                                                                                <h5 class="profile-name">#93908121</h5>
                                                                                            </a>
                                                                                            <div class="task-details-profile-location">
                                                                                                <span class="task-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                        <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" /></svg></span>
                                                                                                <span class="task-details-profile-location-val">Kuala Lumpur, Malaysia</span>
                                                                                            </div>
                                                                                            <div class="task-details-profile-rating">
                                                                                                <div class="task-details-profile-rating-star">
                                                                                                    <span class="rating-star-icon active"><svg viewBox="0 0 16 16" class="bi bi-star-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                            <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"></path>
                                                                                                        </svg></span>
                                                                                                    <span class="rating-star-icon active"><svg viewBox="0 0 16 16" class="bi bi-star-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                            <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"></path>
                                                                                                        </svg></span>
                                                                                                    <span class="rating-star-icon active"><svg viewBox="0 0 16 16" class="bi bi-star-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                            <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"></path>
                                                                                                        </svg></span>
                                                                                                    <span class="rating-star-icon active"><svg viewBox="0 0 16 16" class="bi bi-star-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                            <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"></path>
                                                                                                        </svg></span>
                                                                                                    <span class="rating-star-icon"><svg viewBox="0 0 16 16" class="bi bi-star-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                            <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"></path>
                                                                                                        </svg></span>
                                                                                                </div>
                                                                                                <div class="task-details-profile-rating-val">
                                                                                                    <span class="task-row-average-rating-val">4.2</span>
                                                                                                    <span class="task-row-divider">/</span>
                                                                                                    <span class="task-row-total-rating-val">5 reviews</span>
                                                                                                </div>
                                                                                                <div class="task-details-profile-published">
                                                                                                    <span class="task-details-profile-label">Task Published:</span>
                                                                                                    <span class="task-details-profile-val">2</span>
                                                                                                </div>
                                                                                                <div class="task-details-profile-dispute">
                                                                                                    <span class="task-details-profile-val">Dispute Resolution </span>
                                                                                                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="This user has adopted dispute resolution for 6 out of 38 projects undertaken">
                                                                                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                            <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                                                                        </svg>
                                                                                                    </span>
                                                                                                </div>
                                                                                                <div class="task-details-profile-membersince">
                                                                                                    <span class="task-details-profile-label">Member since:</span>
                                                                                                    <span class="task-details-profile-val">August 2020</span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="task-row-rating-flex">
                                                                                    <div class="task-row-rating-star">
                                                                                        <span class="rating-star-icon active"><svg viewBox="0 0 16 16" class="bi bi-star-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"></path>
                                                                                            </svg></span>
                                                                                        <span class="rating-star-icon active"><svg viewBox="0 0 16 16" class="bi bi-star-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"></path>
                                                                                            </svg></span>
                                                                                        <span class="rating-star-icon active"><svg viewBox="0 0 16 16" class="bi bi-star-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"></path>
                                                                                            </svg></span>
                                                                                        <span class="rating-star-icon active"><svg viewBox="0 0 16 16" class="bi bi-star-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"></path>
                                                                                            </svg></span>
                                                                                        <span class="rating-star-icon"><svg viewBox="0 0 16 16" class="bi bi-star-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"></path>
                                                                                            </svg></span>
                                                                                    </div>
                                                                                    <div class="task-row-rating-val">
                                                                                        <span class="task-row-average-rating-val">4.8</span>
                                                                                        <span class="task-row-divider">/</span>
                                                                                        <span class="task-row-total-rating-val">5 reviews</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="task-row-col-right">
                                                                            <div class="task-row-actions">
                                                                                <button type="button" class="btn-icon-full btn-fav">
                                                                                    <span class="btn-label">Favourite</span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                                <button type="button" class="btn-icon-full btn-apply" data-toggle="modal" data-target="#modal_apply_question">
                                                                                    <span class="btn-label">Apply</span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                                                                            <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade show" id="searchTalentTab" role="tabpanel">
                                        <div class="col-xl-12">
                                            <div class="row">
                                                <div class="col-xl-12 col-tasklisting-top">
                                                    <div class="col-public-search-wrapper">
                                                        <div class="col-xl-12 col-public-search">
                                                            <form action="#" id="searchForm" class=" search-public-form">
                                                                <div class="search-dropdown-filter-top-wrapper form-flex">
                                                                    <div class="search-dropdown-filter-container">
                                                                        <input type="text" class="form-control form-control-input" placeholder="What are your talents?" id="inputBox">
                                                                    </div>
                                                                    <span class="search-dropdown-filter-btn">
                                                                        <button class="btn-search-public" type="submit">
                                                                            <span class="public-search-icon search-icon">
                                                                                <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                    <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                                                                    <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                                                                </svg>
                                                                            </span>
                                                                        </button>
                                                                    </span>
                                                                    <span class="bookmark-dropdown-filter-btn dropdown">
                                                                        <button class="btn-bookmark-public dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                            <span class="public-search-icon search-icon">
                                                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bookmarks" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                    <path fill-rule="evenodd" d="M7 13l5 3V4a2 2 0 0 0-2-2H4a2 2 0 0 0-2 2v12l5-3zm-4 1.234l4-2.4 4 2.4V4a1 1 0 0 0-1-1H4a1 1 0 0 0-1 1v10.234z" />
                                                                                    <path d="M14 14l-1-.6V2a1 1 0 0 0-1-1H4.268A2 2 0 0 1 6 0h6a2 2 0 0 1 2 2v12z" />
                                                                                </svg>
                                                                            </span>
                                                                            <i class="fa fa-caret-down"></i>
                                                                        </button>
                                                                        <div class="dropdown-menu dropdown-menu-saved-search">
                                                                            <div class="modal-advance-filter-container-left">
                                                                                <div class="modal-advanced-filter-container">
                                                                                    <div class="keyword-filter-container">
                                                                                        <label>Keyword</label>
                                                                                        <span>cooking, malaysia, personal, by the hour, project management, sales marketing, teamwork, thought process</span>
                                                                                    </div>
                                                                                    <label class="modal-search-filter-lbl">Save Search</label>
                                                                                    <div class="input-group-flex row-indfirstname">
                                                                                        <input type="text" id="Name" class="form-control form-control-input" placeholder="Name" required="" autofocus="">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
                                                                                    <div class="modal-filter-action form-block-filter-flex">
                                                                                        <button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                                                                                            <span class="btn-label">Save</span>
                                                                                            <span class="btn-icon">
                                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                                                    <polyline points="20 6 9 17 4 12"></polyline>
                                                                                                </svg>
                                                                                            </span>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </span>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>

                                                    <div class="col-task-listing-wrapper">
                                                        <div class="row">
                                                            <div class="col-xl-3 col-task-filter-wrapper">
                                                                <div class="col-task-filter-container">
                                                                    <div class="task-filter-row task-filter-row-location">
                                                                        <button class="btn-link btn-link-filter btn-flex" type="button" aria-expanded="false">
                                                                            <label class="task-filter-lbl">Filter by location</label>
                                                                            <span class="chevron-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M6 9l6 6 6-6"/></svg>
                                                                            </span>
                                                                        </button>
                                                                        <div class="form-filter-collapse collapse">
                                                                            <div class="form-block-task-filter">
                                                                                <div class="form-filter-state">
                                                                                    <select class="form-control form-control-input" placeholder="State">
                                                                                        <option disabled="" selected="">State</option>
                                                                                        <option value="">Kuala Lumpur</option>
                                                                                        <option value="">Selangor</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="form-filter-city">
                                                                                    <select class="form-control form-control-input" placeholder="City">
                                                                                        <option disabled="" selected="">Town/City</option>
                                                                                        <option value="">Petaling Jaya</option>
                                                                                        <option value="">Shah Alam</option>
                                                                                        <option value="">Subang Jaya</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-filter-row task-filter-row-location">
                                                                        <button class="btn-link btn-link-filter btn-flex" type="button" aria-expanded="false">
                                                                            <label class="task-filter-lbl">Filter by category</label>
                                                                            <span class="chevron-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M6 9l6 6 6-6"/></svg>
                                                                            </span>
                                                                        </button>
                                                                        <div class="form-filter-collapse collapse">
                                                                            <div class="form-block-task-filter">
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_0">
                                                                                    <label class="custom-control-label" for="filtercat_0">All category</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_1">
                                                                                    <label class="custom-control-label" for="filtercat_1">Administration</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_2">
                                                                                    <label class="custom-control-label" for="filtercat_2">Art & Design</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_3">
                                                                                    <label class="custom-control-label" for="filtercat_3">Business</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_4">
                                                                                    <label class="custom-control-label" for="filtercat_4">Communication & Interpersonal</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_5">
                                                                                    <label class="custom-control-label" for="filtercat_5">Education</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_6">
                                                                                    <label class="custom-control-label" for="filtercat_6">Engineering</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_7">
                                                                                    <label class="custom-control-label" for="filtercat_7">Finance</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_8">
                                                                                    <label class="custom-control-label" for="filtercat_8">HR & Recruitment</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_9">
                                                                                    <label class="custom-control-label" for="filtercat_9">IT & Data Management</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_10">
                                                                                    <label class="custom-control-label" for="filtercat_10">Management</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_11">
                                                                                    <label class="custom-control-label" for="filtercat_11">Office</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_12">
                                                                                    <label class="custom-control-label" for="filtercat_12">Personal</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_13">
                                                                                    <label class="custom-control-label" for="filtercat_13">Project Management</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_14">
                                                                                    <label class="custom-control-label" for="filtercat_14">Sales Marketing</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_15">
                                                                                    <label class="custom-control-label" for="filtercat_15">Teamwork</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtercat_16">
                                                                                    <label class="custom-control-label" for="filtercat_16">Thought Process</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-filter-row task-filter-row-skills">
                                                                        <button class="btn-link btn-link-filter btn-flex" type="button" aria-expanded="false">
                                                                            <label class="task-filter-lbl">Filter by skills</label>
                                                                            <span class="chevron-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M6 9l6 6 6-6"/></svg>
                                                                            </span>
                                                                        </button>
                                                                        <div class="form-filter-collapse collapse">
                                                                            <div class="form-block-task-filter">
                                                                                <div class="bstags-skills">
                                                                                    <input type="text" name="tagss" data-role="tagsinput" class="bootstrap-tagsinput" placeholder="Enter skills..." />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-filter-row task-filter-row-task-completed">
                                                                        <button class="btn-link btn-link-filter btn-flex" type="button" aria-expanded="false">
                                                                            <label class="task-filter-lbl">Filter by completed task</label>
                                                                            <span class="chevron-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M6 9l6 6 6-6"/></svg>
                                                                            </span>
                                                                        </button>
                                                                        <div class="form-filter-collapse collapse">
                                                                            <div class="form-block-task-filter">
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtertaskcompleted_1">
                                                                                    <label class="custom-control-label" for="filtertaskcompleted_1">All</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtertaskcompleted_2">
                                                                                    <label class="custom-control-label" for="filtertaskcompleted_2">1-10</label>
                                                                                </div>
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="filtertaskcompleted_3">
                                                                                    <label class="custom-control-label" for="filtertaskcompleted_3">10+</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="task-filter-row task-filter-row-reset">
                                                                        <a href="#" class="reset-filter-link">Reset all filters</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xl-9 col-talent-row-wrapper">
                                                                <div class="col-talent-row-container">
                                                                    <div class="talent-row-col-top-flex">
                                                                        <div class="talent-row-col-left">
                                                                            <div class="talent-personal-info-container">
                                                                                <div class="talent-avatar-container">
                                                                                    <div class="profile-avatar">
                                                                                        <a href="#" class="talent-avatar-link"><img class="avatar-img" src="../assets/img/mike.jpg" alt=""></a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="talent-personal-info">
                                                                                    <a href="#" class="talent-name-link">
                                                                                        <h5 class="profile-name">#93908121</h5>
                                                                                    </a>
                                                                                    <div class="talent-details-profile-location">
                                                                                        <span class="talent-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                                                                            </svg></span>
                                                                                        <span class="talent-details-profile-location-val">Kuala Lumpur, Malaysia</span>
                                                                                    </div>
                                                                                    <div class="task-row-rating-flex">
                                                                                        <div class="task-row-rating-star">
                                                                                            <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                            <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                            <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                            <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                            <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                                                        </div>
                                                                                        <div class="task-row-rating-val">
                                                                                            <span class="task-row-average-rating-val">4.8</span>
                                                                                            <span class="task-row-divider">/</span>
                                                                                            <span class="task-row-total-rating-val">5 reviews</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="talent-about-container">
                                                                                Looking for an experienced developer to take your SaaS project off the ground? I might be the right person for you. I have the necessary skill set to develop a complex web App. My experience includes working on a number of Applications for business, such as CRM, Lead Management, ERP and more.</div>
                                                                        </div>
                                                                        <div class="talent-row-col-right">
                                                                            <div class="talent-row-actions">
                                                                                <button type="button" class="btn-icon-full btn-view-profile" onclick="location.href='#';">
                                                                                    <span class="btn-label"><?php echo lang('public_search_talent_view_profile'); ?></span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                                                                            <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="talent-total-completed-task-hour">
                                                                                <div class="talent-extra-info talent-total-completed-task">
                                                                                    <span class="talent-profile-label"><?php echo lang('public_search_talent_completed_task'); ?>:</span>
                                                                                    <span class="talent-profile-val">38</span>
                                                                                </div>
                                                                                <div class="talent-extra-info  talent-total-completed-hour">
                                                                                    <span class="talent-profile-label"><?php echo lang('public_search_talent_total_hours_completed'); ?>:</span>
                                                                                    <span class="talent-profile-val">342</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="talent-extra-info talent-history">
                                                                                <div class="talent-last-login">
                                                                                    <span class="talent-profile-label">Last login:</span>
                                                                                    <span class="talent-profile-val">14 hours ago</span>
                                                                                </div>
                                                                                <div class="talent-extra-info talent-member-since">
                                                                                    <span class="talent-profile-label">Member since:</span>
                                                                                    <span class="talent-profile-val">August 2020</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="talent-row-col-btm-flex">
                                                                        <div class="talent-row-col-left">
                                                                            <div class="talent-row-skills">
                                                                                <div class="task-details-deadline-block">
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Graphic Design</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Illustration</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe Illustrator</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe Photoshop</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe After Effects</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Animation</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Motion Graphics</span></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="talent-row-col-right">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-talent-row-container">
                                                                    <div class="talent-row-col-top-flex">
                                                                        <div class="talent-row-col-left">
                                                                            <div class="talent-personal-info-container">
                                                                                <div class="talent-avatar-container">
                                                                                    <div class="profile-avatar">
                                                                                        <a href="#" class="talent-avatar-link"><img class="avatar-img" src="../assets/img/mike.jpg" alt=""></a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="talent-personal-info">
                                                                                    <a href="#" class="talent-name-link">
                                                                                        <h5 class="profile-name">#93908121</h5>
                                                                                    </a>
                                                                                    <div class="talent-details-profile-location">
                                                                                        <span class="talent-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                                                                            </svg></span>
                                                                                        <span class="talent-details-profile-location-val">Kuala Lumpur, Malaysia</span>
                                                                                    </div>
                                                                                    <div class="task-row-rating-flex">
                                                                                        <div class="task-row-rating-star">
                                                                                            <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                            <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                            <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                            <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                            <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                                                        </div>
                                                                                        <div class="task-row-rating-val">
                                                                                            <span class="task-row-average-rating-val">4.8</span>
                                                                                            <span class="task-row-divider">/</span>
                                                                                            <span class="task-row-total-rating-val">5 reviews</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="talent-about-container">
                                                                                Looking for an experienced developer to take your SaaS project off the ground? I might be the right person for you. I have the necessary skill set to develop a complex web App. My experience includes working on a number of Applications for business, such as CRM, Lead Management, ERP and more.</div>
                                                                        </div>
                                                                        <div class="talent-row-col-right">
                                                                            <div class="talent-row-actions">
                                                                                <button type="button" class="btn-icon-full btn-view-profile" onclick="location.href='#';">
                                                                                    <span class="btn-label"><?php echo lang('public_search_talent_view_profile'); ?></span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                                                                            <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="talent-total-completed-task-hour">
                                                                                <div class="talent-extra-info talent-total-completed-task">
                                                                                    <span class="talent-profile-label"><?php echo lang('public_search_talent_completed_task'); ?>:</span>
                                                                                    <span class="talent-profile-val">38</span>
                                                                                </div>
                                                                                <div class="talent-extra-info  talent-total-completed-hour">
                                                                                    <span class="talent-profile-label"><?php echo lang('public_search_talent_total_hours_completed'); ?>:</span>
                                                                                    <span class="talent-profile-val">342</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="talent-extra-info talent-history">
                                                                                <div class="talent-last-login">
                                                                                    <span class="talent-profile-label">Last login:</span>
                                                                                    <span class="talent-profile-val">14 hours ago</span>
                                                                                </div>
                                                                                <div class="talent-extra-info talent-member-since">
                                                                                    <span class="talent-profile-label">Member since:</span>
                                                                                    <span class="talent-profile-val">August 2020</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="talent-row-col-btm-flex">
                                                                        <div class="talent-row-col-left">
                                                                            <div class="talent-row-skills">
                                                                                <div class="task-details-deadline-block">
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Graphic Design</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Illustration</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe Illustrator</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe Photoshop</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe After Effects</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Animation</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Motion Graphics</span></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="talent-row-col-right">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-talent-row-container">
                                                                    <div class="talent-row-col-top-flex">
                                                                        <div class="talent-row-col-left">
                                                                            <div class="talent-personal-info-container">
                                                                                <div class="talent-avatar-container">
                                                                                    <div class="profile-avatar">
                                                                                        <a href="#" class="talent-avatar-link"><img class="avatar-img" src="../assets/img/mike.jpg" alt=""></a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="talent-personal-info">
                                                                                    <a href="#" class="talent-name-link">
                                                                                        <h5 class="profile-name">#93908121</h5>
                                                                                    </a>
                                                                                    <div class="talent-details-profile-location">
                                                                                        <span class="talent-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                                                                            </svg></span>
                                                                                        <span class="talent-details-profile-location-val">Kuala Lumpur, Malaysia</span>
                                                                                    </div>
                                                                                    <div class="task-row-rating-flex">
                                                                                        <div class="task-row-rating-star">
                                                                                            <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                            <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                            <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                            <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                            <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                                                        </div>
                                                                                        <div class="task-row-rating-val">
                                                                                            <span class="task-row-average-rating-val">4.8</span>
                                                                                            <span class="task-row-divider">/</span>
                                                                                            <span class="task-row-total-rating-val">5 reviews</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="talent-about-container">
                                                                                Looking for an experienced developer to take your SaaS project off the ground? I might be the right person for you. I have the necessary skill set to develop a complex web App. My experience includes working on a number of Applications for business, such as CRM, Lead Management, ERP and more.</div>
                                                                        </div>
                                                                        <div class="talent-row-col-right">
                                                                            <div class="talent-row-actions">
                                                                                <button type="button" class="btn-icon-full btn-view-profile" onclick="location.href='#';">
                                                                                    <span class="btn-label"><?php echo lang('public_search_talent_view_profile'); ?></span>
                                                                                    <span class="btn-icon">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                            <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                                                                            <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="talent-total-completed-task-hour">
                                                                                <div class="talent-extra-info talent-total-completed-task">
                                                                                    <span class="talent-profile-label"><?php echo lang('public_search_talent_completed_task'); ?>:</span>
                                                                                    <span class="talent-profile-val">38</span>
                                                                                </div>
                                                                                <div class="talent-extra-info  talent-total-completed-hour">
                                                                                    <span class="talent-profile-label"><?php echo lang('public_search_talent_total_hours_completed'); ?>:</span>
                                                                                    <span class="talent-profile-val">342</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="talent-extra-info talent-history">
                                                                                <div class="talent-last-login">
                                                                                    <span class="talent-profile-label">Last login:</span>
                                                                                    <span class="talent-profile-val">14 hours ago</span>
                                                                                </div>
                                                                                <div class="talent-extra-info talent-member-since">
                                                                                    <span class="talent-profile-label">Member since:</span>
                                                                                    <span class="talent-profile-val">August 2020</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="talent-row-col-btm-flex">
                                                                        <div class="talent-row-col-left">
                                                                            <div class="talent-row-skills">
                                                                                <div class="task-details-deadline-block">
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Graphic Design</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Illustration</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe Illustrator</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe Photoshop</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe After Effects</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Animation</span></a>
                                                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Motion Graphics</span></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="talent-row-col-right">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal - OD Post Task Steps -->
    <div id="post_task_od" class="modal modal-steps fade" aria-labelledby="post_task_od" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target="#modal_confirm_od" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="modal-form-wrapper">
                        <form name="form_post_task_od" id="form_post_task_od" method="post" action="">

                            <div id="sf1" class="frm modal-step-flex">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 1 of 5: Select Category</div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Category" id="od_category" name="od_category">
                                                <option disabled selected>Category</option>
                                                <option value="1">Option 1</option>
                                                <option value="2">Option 2</option>
                                                <option value="3">Option 3</option>
                                                <option value="4">Option 4</option>
                                                <option value="5">Option 5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Sub Category" id="od_subcategory" name="od_subcategory">
                                                <option disabled selected>Sub Category</option>
                                                <option value="1">Option 1</option>
                                                <option value="2">Option 2</option>
                                                <option value="3">Option 3</option>
                                                <option value="4">Option 4</option>
                                                <option value="5">Option 5</option>
                                            </select>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-next od-open1">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div id="sf2" class="frm modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 2 of 5: Task Details</div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-input" placeholder="Task Title" id="od_tasktitle" name="od_tasktitle">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control form-control-input" placeholder="Task Description" id="od_taskdesc" name="od_taskdesc" rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <div class="dropzone dropzone-previews" id="my-awesome-dropzone"></div>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev od-back2">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next od-open2">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div id="sf3" class="frm modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left">
                                    <div id="mapTaskLoc" style="display:none;"></div>
                                </div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 3 of 5: Task Location</div>
                                        <div class="form-group">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Task Location</span>
                                            </label>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="tl_remote" name="tlRadioSelect">
                                                <label class="custom-control-label" for="tl_remote">Can be done remotely</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="tl_travel" name="tlRadioSelect">
                                                <label class="custom-control-label" for="tl_travel">Requires travelling</label>
                                            </div>
                                        </div>
                                        <div class="form-check-selection" id="form_travelrequired" style="display:none;">
                                            <input id="loc" type="text" class="form-control form-control-input" placeholder="Enter your address or drag the pin on map" />
                                        </div>
                                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev od-back3">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next od-open3">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div id="sf4" class="frm modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 4 of 5: Task Estimates</div>
                                        <div class="form-group">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Nature of Task</span>
                                            </label>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="not_bythehour" name="defaultExampleRadios">
                                                <label class="custom-control-label" for="not_bythehour">By the hour</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="not_lumpsum" name="defaultExampleRadios">
                                                <label class="custom-control-label" for="not_lumpsum">Lump sum, complete by deadline</label>
                                            </div>
                                        </div>
                                        <div class="form-check-selection" id="form_byhour" style="display: none;">
                                            <div class="form-group form-estimated-hours">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Estimated hours</span>
                                                </label>
                                                <div class="form-flex">
                                                    <input type="number" name="" class="form-control form-control-input" min="0" id="" />
                                                    <span class="postlbl-hours">hours</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-estimated-hours">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Budget Per Hour</span>
                                                </label>
                                                <div class="form-flex">
                                                    <span class="estimated-lbl estimated-from">RM50</span>
                                                    <input id="range_budget_hour" type="text" />
                                                    <span class="estimated-lbl estimated-to">RM100</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-total-project-budget">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Total Project Budget</span>
                                                </label>
                                                <div class="form-flex">
                                                    <span class="postlbl-cur">RM150 - RM300</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-complete-by">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt"><?php echo lang('public_search_start_date'); ?></span>
                                                </label>
                                                <div class="form-group">
                                                    <div class="input-group input-group-datetimepicker date" id="form_datetime_hour" data-target-input="nearest">
                                                        <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_hour" />
                                                        <span class="input-group-addon" data-target="#form_datetime_hour" data-toggle="datetimepicker">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-complete-by-hour">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Completed by</span>
                                                </label>
                                                <div class="form-group">
                                                    <div class="custom-control custom-radio radio-complete-day">
                                                        <input type="radio" class="custom-control-input" id="not_bythehour_day" name="byTheHourDayRadio" checked="checked">
                                                        <label class="custom-control-label" for="not_bythehour_day"></label>
                                                        <div class="form-flex">
                                                            <input type="number" name="" class="form-control form-control-input" min="0" id="" />
                                                            <span class="postlbl-hours">days</span>
                                                        </div>
                                                    </div>
                                                    <div class="custom-control custom-radio radio-complete-date">
                                                        <input type="radio" class="custom-control-input" id="not_bythehour_date" name="byTheHourDayRadio">
                                                        <label class="custom-control-label" for="not_bythehour_date"></label>
                                                        <div class="input-group input-group-datetimepicker date" id="form_datetime_hour_completeby" data-target-input="nearest">
                                                            <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_hour_completeby" readonly="readonly" />
                                                            <span class="input-group-addon" data-target="#form_datetime_hour_completeby" data-toggle="datetimepicker">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="task-filter-row task-filter-row-location">
                                                    <div class="form-block-task-filter">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="urgent_hour">
                                                            <label class="custom-control-label" for="urgent_hour">Mark this task as urgent <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="Within 5-day between posted date and start date ">
                                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                        <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                                    </svg>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-check-selection" id="form_bylumpsum" style="display: none;">
                                            <div class="form-group form-estimated-hours">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Budget For Task</span>
                                                </label>
                                                <div class="form-flex">
                                                    <span class="estimated-lbl estimated-from">RM200</span>
                                                    <input id="range_budget_lumpsum" type="text" />
                                                    <span class="estimated-lbl estimated-to">RM300</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-complete-by">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt"><?php echo lang('public_search_start_date'); ?></span>
                                                </label>
                                                <div class="form-group">
                                                    <div class="input-group input-group-datetimepicker date" id="form_datetime_lumpsum" data-target-input="nearest">
                                                        <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_lumpsum" />
                                                        <span class="input-group-addon" data-target="#form_datetime_lumpsum" data-toggle="datetimepicker">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-complete-by-lumpsum">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Completed by</span>
                                                </label>
                                                <div class="form-group">
                                                    <div class="custom-control custom-radio radio-complete-day">
                                                        <input type="radio" class="custom-control-input" id="not_bylumpsum_day" name="byLumpsumDayRadio" checked="checked">
                                                        <label class="custom-control-label" for="not_bylumpsum_day"></label>
                                                        <div class="form-flex">
                                                            <input type="number" name="" class="form-control form-control-input" min="0" id="" />
                                                            <span class="postlbl-hours">days</span>
                                                        </div>
                                                    </div>
                                                    <div class="custom-control custom-radio radio-complete-date">
                                                        <input type="radio" class="custom-control-input" id="not_bylumpsum_date" name="byLumpsumDayRadio">
                                                        <label class="custom-control-label" for="not_bylumpsum_date"></label>
                                                        <div class="input-group input-group-datetimepicker date" id="form_datetime_lumpsum_completeby" data-target-input="nearest">
                                                            <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_lumpsum_completeby" readonly="readonly" />
                                                            <span class="input-group-addon" data-target="#form_datetime_lumpsum_completeby" data-toggle="datetimepicker">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="task-filter-row task-filter-row-location">
                                                    <div class="form-block-task-filter">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="urgent_lumpsum">
                                                            <label class="custom-control-label" for="urgent_lumpsum">Mark this task as urgent <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="Within 5-day between posted date and start date ">
                                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                        <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                                    </svg>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev od-back4">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next od-open4">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div id="sf5" class="frm modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 5 of 5: Seeker Shortlisting</div>
                                        <div class="form-group form-important-skills">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Important skills for task</span>
                                            </label>
                                            <div class="bs-example">
                                                <input type="text" name="tags" value="Tag1,Tag2,Tag3" data-role="tagsinput" />
                                            </div>
                                        </div>
                                        <div class="form-group form-qna">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Questions</span>
                                            </label>
                                            <div class="od-qna-field-wrapper">
                                                <div class="qna-wrapper od-qna-wrapper ">
                                                    <div class="form-flex">
                                                        <span class="qna-lbl od-qna-lbl">1.</span>
                                                        <input type="text" name="od_qna_field[]" class="form-control form-control-input" placeholder="Question" />
                                                    </div>
                                                </div>
                                                <a href="javascript:void(0);" class="od-add-button" title="Add field">Add More</a>
                                            </div>
                                        </div>
                                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev od-back5">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="submit" class="btn-icon-full btn-step-submit">
                                            <span class="btn-label">Submit</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <!-- Modal - FT Post Job Steps -->
    <div id="post_job_ft" class="modal modal-steps fade" aria-labelledby="post_job_ft" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target="#modal_confirm_ft" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="modal-form-wrapper">
                        <form name="form_post_job_ft" id="form_post_job_ft" method="post" action="">

                            <div id="ft_sf1" class="frm-ft modal-step-flex">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 1 of 5: Select Category</div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Category" id="ft_category" name="ft_category">
                                                <option disabled selected>Category</option>
                                                <option value="1">Option 1</option>
                                                <option value="2">Option 2</option>
                                                <option value="3">Option 3</option>
                                                <option value="4">Option 4</option>
                                                <option value="5">Option 5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Sub Category" id="ft_subcategory" name="ft_subcategory">
                                                <option disabled selected>Sub Category</option>
                                                <option value="1">Option 1</option>
                                                <option value="2">Option 2</option>
                                                <option value="3">Option 3</option>
                                                <option value="4">Option 4</option>
                                                <option value="5">Option 5</option>
                                            </select>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-next ft-open1">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>

                            </div>

                            <div id="ft_sf2" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 2 of 5: The Job</div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-input" placeholder="Job Title" id="ft_jobtitle" name="ft_jobtitle">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control form-control-input" placeholder="Job Description" id="ft_jobdesc" name="ft_jobdesc" rows="3"></textarea>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back2">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next ft-open2">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div id="ft_sf3" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 3 of 5: The Applicant</div>

                                        <div class="form-group">
                                            <textarea class="form-control form-control-input" placeholder="The Requirements" id="ft_jobrequirement" name="ft_jobrequirement" rows="3"></textarea>
                                        </div>

                                        <div class="form-group form-qna">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Questions</span>
                                            </label>
                                            <div class="ft-qna-field-wrapper">
                                                <div class="qna-wrapper ft-qna-wrapper ">
                                                    <div class="form-flex">
                                                        <span class="qna-lbl ft-qna-lbl">1.</span>
                                                        <input type="text" name="ft_qna_field[]" class="form-control form-control-input" placeholder="Question" />
                                                    </div>

                                                </div>
                                                <a href="javascript:void(0);" class="ft-add-button" title="Add field">Add More</a>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Experience Level" id="ft_jobxp" name="ft_jobxp">
                                                <option disabled selected>Experience Level</option>
                                                <option value="1">Option 1</option>
                                                <option value="2">Option 2</option>
                                                <option value="3">Option 3</option>
                                                <option value="4">Option 4</option>
                                                <option value="5">Option 5</option>
                                            </select>
                                        </div>



                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back3">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next ft-open3">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div id="ft_sf4" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 4 of 5: The Location</div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Country" id="ft_jobcountry" name="ft_jobcountry">
                                                <option disabled selected>Country</option>
                                                <option value="1">Malaysia</option>
                                                <option value="2">Singapore</option>
                                                <option value="3">Thailand</option>
                                                <option value="4">Option 4</option>
                                                <option value="5">Option 5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="State" id="ft_jobstate" name="ft_jobstate">
                                                <option disabled selected>State</option>
                                                <option value="Johor">Johor</option>
                                                <option value="Kedah">Kedah</option>
                                                <option value="Kelantan">Kelantan</option>
                                                <option value="Kuala Lumpur">Kuala Lumpur</option>
                                                <option value="Labuan">Labuan</option>
                                                <option value="Malacca">Melaka</option>
                                                <option value="Negeri Sembilan">Negeri Sembilan</option>
                                                <option value="Pahang">Pahang</option>
                                                <option value="Perak">Perak</option>
                                                <option value="Perlis">Perlis</option>
                                                <option value="Penang">Penang</option>
                                                <option value="Sabah">Sabah</option>
                                                <option value="Sarawak">Sarawak</option>
                                                <option value="Selangor">Selangor</option>
                                                <option value="Terengganu">Terengganu</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-input" placeholder="Job Location" id="ft_joblocation" name="ft_joblocation">
                                        </div>



                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back4">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next ft-open4">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>



                            <div id="ft_sf5" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>

                                        <div class="modal-steps-title">Step 5 of 5: Remuneration</div>
                                        <div class="form-group form-salary-range">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Salary Range</span>
                                            </label>
                                            <div class="form-flex">
                                                <span class="estimated-lbl estimated-from">RM1200</span>
                                                <input id="range_budget_salary" type="text" />
                                                <span class="estimated-lbl estimated-to">RM12000</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-benefits">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Benefits</span>
                                            </label>
                                            <div class="form-block">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_1">
                                                    <label class="custom-control-label" for="benefits_1">Medical</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_2">
                                                    <label class="custom-control-label" for="benefits_2">Parking</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_3">
                                                    <label class="custom-control-label" for="benefits_3">Claimable expenses</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_4">
                                                    <label class="custom-control-label" for="benefits_4">Dental</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_5">
                                                    <label class="custom-control-label" for="benefits_5">Meals</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_6">
                                                    <label class="custom-control-label" for="benefits_6">Pantry</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_7">
                                                    <label class="custom-control-label" for="benefits_7">Commission</label>
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back5">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="submit" class="btn-icon-full btn-step-submit">
                                            <span class="btn-label">Submit</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <!-- Modal - OD Confirm Discard -->
    <div id="modal_confirm_od" class="modal modal-confirm fade" aria-labelledby="modal_confirm_od" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p class="modal-para">This will discard all the detail you have entered.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_task_od">
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                        <span class="btn-label">Confirm</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <!-- Modal - FT Confirm Discard -->
    <div id="modal_confirm_ft" class="modal modal-confirm fade" aria-labelledby="modal_confirm_ft" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p class="modal-para">This will discard all the detail you have entered.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_job_ft">
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                        <span class="btn-label">Confirm</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->


    <!-- Modal - Seeker's Questionnaire on Task Application -->
    <div id="modal_apply_question" class="modal modal-apply-question fade" aria-labelledby="modal_custom_widget" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">The Poster has requested for your participation in a pre-assessment questionaire.</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <form name="form_task_question" id="form_task_question" action="task-centre.html">
                        <div id="apply_question_1" class="frm form-apply-question">
                            <div class="modal-steps-title">Question 1 of 3</div>
                            <div class="task-question">Lorem ipsum dolor sit amet?</div>
                            <textarea class="form-control form-control-input" placeholder="Your Answer" rows="3" id="taskQuestion1"></textarea>
                            <div class="form-group button-container">
                                <button type="button" class="btn-icon-full btn-step-next qa-open1">
                                    <span class="btn-label">Next</span>
                                    <span class="btn-icon">
                                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                        </svg>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <div id="apply_question_2" class="frm form-apply-question" style="display:none;">
                            <div class="modal-steps-title">Question 2 of 3</div>
                            <div class="task-question">Consectetur adipiscing elit, sed do eiusmod tempor incididunt?</div>
                            <textarea class="form-control form-control-input" placeholder="Your Answer" rows="3" id="taskQuestion2"></textarea>
                            <div class="form-group button-container">
                                <button type="button" class="btn-icon-full btn-step-prev qa-back2">
                                    <span class="btn-label">Back</span>
                                    <span class="btn-icon">
                                        <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                    </span>
                                </button>
                                <button type="button" class="btn-icon-full btn-step-next qa-open2">
                                    <span class="btn-label">Next</span>
                                    <span class="btn-icon">
                                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                        </svg>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <div id="apply_question_3" class="frm form-apply-question" style="display:none;">
                            <div class="modal-steps-title">Question 3 of 3</div>
                            <div class="task-question">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium?</div>
                            <textarea class="form-control form-control-input" placeholder="Your Answer" rows="3" id="taskQuestion3"></textarea>
                            <div class="form-group button-container">
                                <button type="button" class="btn-icon-full btn-step-prev qa-back3">
                                    <span class="btn-label">Back</span>
                                    <span class="btn-icon">
                                        <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                    </span>
                                </button>
                                <button type="submit" class="btn-icon-full btn-step-next qa-open3">
                                    <span class="btn-label">Complete</span>
                                    <span class="btn-icon">
                                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                        </svg>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->


    <script>
        //the maps api is setup above
        
        $(window).on("load", function() {
            var latlng = new google.maps.LatLng(3.110651, 101.666760); //Set the default location of map
            var map = new google.maps.Map(document.getElementById('mapTaskLoc'), {
                center: latlng,
                zoom: 15, //The zoom value for map
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: 'Place the marker for your location!', //The title on hover to display
                draggable: true //this makes it drag and drop
            });
            google.maps.event.addListener(marker, 'dragend', function(a) {
                console.log(a);
                document.getElementById('loc').value = a.latLng.lat().toFixed(4) + ', ' + a.latLng.lng().toFixed(4); //Place the value in input box
            });
        });
        
        
    </script>
    <!-- Post Task Modal - Google Map Drop Pin -->



    <script type="text/javascript">
        $(function() {
            $('#datetimepicker2').datetimepicker({
                locale: 'ru'
            });
        });

    </script>



    <!-- Sidebar Hover Function -->
    <script>
        // Sidebar Hover Function
        const $dropdown = $(".sidebar .dropdown");
        const $dropdownToggle = $(".sidebar .dropdown-toggle");
        const $dropdownMenu = $(".sidebar .dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function() {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                    function() {
                        const $this = $(this);
                        $this.addClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "true");
                        $this.find($dropdownMenu).addClass(showClass);
                    },
                    function() {
                        const $this = $(this);
                        $this.removeClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "false");
                        $this.find($dropdownMenu).removeClass(showClass);
                    }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });


        $('.card-header .dropdown-menu .nav-tabs .nav-link').on("click.bs.dropdown", function(e) {
            $(this).tab('show');
            e.stopPropagation();
        });
        $('.col-account-header .dropdown.col-account .switch-toggle .slider-toggler').on("click", function(e) {
            e.stopPropagation();
        });

    </script>
    <!-- Sidebar Hover Function -->

    <script>
        // Task Listing Filter - Range Budget Hour
        var range_budget_hour_filter = new Slider("#range_budget_hour_filter", {
            min: 10,
            max: 100,
            step: 1,
            value: [30, 65],
            range: true,
            tooltip_position: 'bottom',
            formatter: function(value) {
                return 'RM' + value[0] + ' - RM' + value[1];
            }
        });

        // Task Listing Filter - Range Budget Lumpsum
        var range_budget_lumpsum_filter = new Slider("#range_budget_lumpsum_filter", {
            min: 10,
            max: 1000,
            step: 50,
            value: [300, 650],
            range: true,
            tooltip_position: 'bottom',
            formatter: function(value) {
                return 'RM' + value[0] + ' - RM' + value[1];
            }
        });

    </script>

    
    

</body>

</html>
