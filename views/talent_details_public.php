<body class="white-content">

    <div class="public-wrapper">
     

        <div class="public-container">
            <div class="container">

                <div class="col-talent-details-wrapper">
                    <div class="row">
                        <div class="col-talent-details-top-wrapper">
                            <div class="col-left-card">
                                <div class="col-personal-info-card-container">
                                    <div class="talent-details-personal-info-flex">
                                        <div class="talent-details-avatar-container">
                                            <div class="profile-avatar">
                                                <img class="avatar-img" src="/mtp-dev/assets/img/mike.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="talent-details-personal-info">
                                            <h2 class="talent-details-name">#93908121</h2>
                                            <div class="talent-details-profile-location">
                                                <span class="talent-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                                    </svg></span>
                                                <span class="talent-details-profile-location-val">Kuala Lumpur, Malaysia</span>
                                            </div>
                                            <div class="task-row-rating-flex">
                                                <div class="task-row-rating-star">
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                </div>
                                                <div class="task-row-rating-val">
                                                    <span class="task-row-average-rating-val">4.8</span>
                                                    <span class="task-row-divider">/</span>
                                                    <span class="task-row-total-rating-val">5 reviews</span>
                                                </div>
                                            </div>
                                            <div class="talent-row-skills">
                                                <label class="talent-details-lbl">Skills</label>
                                                <div class="task-details-deadline-block">
                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Graphic Design</span></a>
                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Illustration</span></a>
                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe Illustrator</span></a>
                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe Photoshop</span></a>
                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe After Effects</span></a>
                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Animation</span></a>
                                                    <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Motion Graphics</span></a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-talent-details-aboutme-container">
                                    <label class="talent-details-lbl">About Me</label>
                                    <div class="talent-details-about">Looking for an experienced developer to take your SaaS project off the ground? I might be the right person for you. I have the necessary skill set to develop a complex web App. My experience includes working on a number of Applications for business, such as CRM, Lead Management, ERP and more.</div>
                                </div>
                                <div class="col-talent-details-workxp-container">
                                    <label class="talent-details-lbl">Work Experience</label>
                                    <div class="talent-details-workxp-row">
                                        <div class="talent-details-workxp-compname">Setel Ventures Sdn Bhd</div>
                                        <div class="talent-details-workxp-pos-duration">
                                            <div class="talent-details-workxp-pos">Product Designer</div>
                                            <div class="talent-details-workxp-duration">3 years 5 months</div>
                                        </div>
                                        <div class="talent-details-workxp-resp">
                                            <span class="talent-details-workxp-list-lbl">Responsibilities</span>
                                            <ul class="talent-details-workxp-list">
                                                <li>Contribute to high-level strategic decisions with the rest of the product team</li>
                                                <li>Main lead that manages relationships with internal stakeholders of a project or solution with regards to UX/UI requirements and design-based thinking for the company</li>
                                                <li>A contributing member of an internal full-stack design team that provides guidance on user requirements prioritization, scheduling, stakeholder presentations</li>
                                                <li>Advocates for the end user by influencing decisions to ensure that products, solutions and design decisions are aligned with well-defined user needs and expectations</li>
                                                <li>Serves as an input to qualitative and quantitative user experience data gathering efforts including working with the data and IT stakeholders to ensure that key performance indicators of user experience are readily captured</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="col-talent-details-education-container">
                                    <label class="talent-details-lbl">Education</label>

                                </div>-->
                                <div class="col-talent-details-language-container">
                                    <label class="talent-details-lbl">Languages</label>
                                    <div class="task-details-deadline-block">
                                        <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">English</span></a>
                                        <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Malay</span></a>
                                    </div>

                                </div>
                                <div class="col-rating-list-container">
                                    <label class="talent-details-lbl">Rating & Feedback</label>
                                    <ul class="col-rating-list">
                                        <li>
                                            <div class="col-xl-12 col-ratings-bottom">
                                                <div class="col-rating-first-bottom-group">
                                                    <div class="col-rating-first-bottom">
                                                        <div class="rating-avatar">
                                                            <div class="col-rating-photo">
                                                                <img src="/mtp-dev/assets/img/james.jpg" alt="Profile Photo">
                                                            </div>
                                                        </div>
                                                        <div class="rating-name">Mike tyson</div>
                                                    </div>
                                                    <div class="bottom-rating-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                </div>
                                                <div class="task-row-rating-star">
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-xl-12 col-ratings-bottom">
                                                <div class="col-rating-first-bottom-group">
                                                    <div class="col-rating-first-bottom">
                                                        <div class="rating-avatar">
                                                            <div class="col-rating-photo">
                                                                <img src="/mtp-dev/assets/img/james.jpg" alt="Profile Photo">
                                                            </div>
                                                        </div>
                                                        <div class="rating-name">Mike tyson</div>
                                                    </div>
                                                    <div class="bottom-rating-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                </div>
                                                <div class="task-row-rating-star">
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-xl-12 col-ratings-bottom">
                                                <div class="col-rating-first-bottom-group">
                                                    <div class="col-rating-first-bottom">
                                                        <div class="rating-avatar">
                                                            <div class="col-rating-photo">
                                                                <img src="/mtp-dev/assets/img/james.jpg" alt="Profile Photo">
                                                            </div>
                                                        </div>
                                                        <div class="rating-name">Mike tyson</div>
                                                    </div>
                                                    <div class="bottom-rating-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                </div>
                                                <div class="task-row-rating-star">
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-xl-12 col-ratings-bottom">
                                                <div class="col-rating-first-bottom-group">
                                                    <div class="col-rating-first-bottom">
                                                        <div class="rating-avatar">
                                                            <div class="col-rating-photo">
                                                                <img src="/mtp-dev/assets/img/james.jpg" alt="Profile Photo">
                                                            </div>
                                                        </div>
                                                        <div class="rating-name">Mike tyson</div>
                                                    </div>
                                                    <div class="bottom-rating-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                </div>
                                                <div class="task-row-rating-star">
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-xl-12 col-ratings-bottom">
                                                <div class="col-rating-first-bottom-group">
                                                    <div class="col-rating-first-bottom">
                                                        <div class="rating-avatar">
                                                            <div class="col-rating-photo">
                                                                <img src="/mtp-dev/assets/img/james.jpg" alt="Profile Photo">
                                                            </div>
                                                        </div>
                                                        <div class="rating-name">Mike tyson</div>
                                                    </div>
                                                    <div class="bottom-rating-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                </div>
                                                <div class="task-row-rating-star">
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                    <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-right-card">
                                <div class="col-activity-info-card-container">
                                    <div class="talent-total-completed-task-hour">
                                        <label class="talent-details-lbl">Activity</label>
                                        <div class="talent-extra-info talent-total-completed-task">
                                            <span class="talent-profile-label">Completed Task:</span>
                                            <span class="talent-profile-val">38</span>
                                        </div>
                                        <div class="talent-extra-info  talent-total-ongoing-task">
                                            <span class="talent-profile-label">Ongoing Task:</span>
                                            <span class="talent-profile-val">0</span>
                                        </div>
                                        <div class="talent-extra-info  talent-total-completed-hour">
                                            <span class="talent-profile-label">Total Hours Completed:</span>
                                            <span class="talent-profile-val">342</span>
                                        </div>
                                    </div>
                                    <div class="talent-history">
                                        <label class="talent-details-lbl">Information</label>
                                        <div class="talent-extra-info talent-last-login">
                                            <span class="talent-profile-label">Last login:</span>
                                            <span class="talent-profile-val">14 hours ago</span>
                                        </div>
                                        <div class="talent-extra-info talent-extra-info talent-member-since">
                                            <span class="talent-profile-label">Member since:</span>
                                            <span class="talent-profile-val">August 2020</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-skill-endorse-card-container">
                                    <div class="talent-skill-endorse">
                                        <label class="talent-details-lbl">Skill Endorsements</label>
                                        <div class="talent-extra-info talent-skill-endorse">
                                            <a href="#" class="skill-link"><span class="talent-profile-label">Graphic Designer</span></a>
                                            <span class="talent-profile-val">23</span>
                                        </div>
                                        <div class="talent-extra-info talent-skill-endorse">
                                            <a href="#" class="skill-link"><span class="talent-profile-label">Illustration</span></a>
                                            <span class="talent-profile-val">18</span>
                                        </div>
                                        <div class="talent-extra-info talent-skill-endorse">
                                            <a href="#" class="skill-link"><span class="talent-profile-label">Adobe Illustrator</span></a>
                                            <span class="talent-profile-val">16</span>
                                        </div>
                                        <div class="talent-extra-info talent-skill-endorse">
                                            <a href="#" class="skill-link"><span class="talent-profile-label">Adobe Photoshop</span></a>
                                            <span class="talent-profile-val">12</span>
                                        </div>
                                        <div class="talent-extra-info talent-skill-endorse">
                                            <a href="#" class="skill-link"><span class="talent-profile-label">Adobe After Effects</span></a>
                                            <span class="talent-profile-val">8</span>
                                        </div>
                                        <div class="talent-extra-info talent-skill-endorse">
                                            <a href="#" class="skill-link"><span class="talent-profile-label">Animation</span></a>
                                            <span class="talent-profile-val">4</span>
                                        </div>
                                        <div class="talent-extra-info talent-skill-endorse">
                                            <a href="#" class="skill-link"><span class="talent-profile-label">Motion Graphics</span></a>
                                            <span class="talent-profile-val">3</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-talent-details-btm-wrapper">

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

       
    </div>