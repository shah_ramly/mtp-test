<li class="dropdown col-right-flex col-account">
	<a href="#" class="dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
		<div class="header-profile-photo">
			<img src="<?php echo imgCrop($user->info['photo'], 35, 35, 'assets/img/default-avatar.png') ?>" alt="Profile Photo">
		</div>
	</a>
	<ul class="dropdown-menu dropdown-navbar">
		<button type="button" class="account-close">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor"
			     stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
				<line x1="18" y1="6" x2="6" y2="18"></line>
				<line x1="6" y1="6" x2="18" y2="18"></line>
			</svg>
		</button>
		<?php if( $user->info['type'] === '0' ): ?>
		<li class="header-dropdown-link-list header-usergreet">
			<div class="user-greet-row">
				<div class="user-greet-profile-photo">
					<img src="<?php echo imgCrop($user->info['photo'], 42, 42, 'assets/img/default-avatar.png') ?>" alt="Profile Photo">
				</div>
				<div class="user-info-container-menu">
				<span class="user-greet-lbl"><?php echo lang('user_menu_title'); ?> <span class="user-greet-name"><?php echo ucfirst($user->info['firstname']) ?></span>!</span>
				<div class="user-info-menu">
				<div class="user-info-display"><?php echo lang('title_subscription_plan'); ?> <span><?php echo lang($user->info['plan']['title']); ?></span></div>
				<div class="user-info-display"><?php echo lang('member_since'); ?> <span><?php echo strftime('%b %Y', strtotime($user->info['date_created'])); ?></span></div>
			</div>
			</div>
			</div>
			<li class="dropdown-divider"></li>
		</li>
		<?php endif ?>
		<!--<li class="header-dropdown-link-list header-lang lang-switcher-list">
		<?php
/*			if(strtolower($_SESSION['WEB_LANGUAGE']) == 'en'){
				$en_active = "active";
				$bm_active = "";
			}else{
				$en_active = "";
				$bm_active = "active";
			}
		*/?>
			<a href="<?php /*echo url_for('/lang/EN'); */?>" class="lang-switcher-link lang-switcher-link-en <?php /*echo $en_active; */?>">
				<span class="lang-switcher-en">EN</span>
			</a>|
			<a href="<?php /*echo url_for('/lang/BM'); */?>" class="lang-switcher-link lang-switcher-link-en <?php /*echo $bm_active; */?>">
				<span class="lang-switcher-bm">BM</span>
			</a>
		</li>-->
		<?php if( $user->info['type'] === '0' ): ?>
		<li class="header-dropdown-link-list header-myprofile">
			<a href="<?php echo url_for('my_profile'); ?>" class="nav-item dropdown-item"><?php echo lang('user_menu_my_profile'); ?></a>
		</li>
		<li class="header-dropdown-link-list header-mycv">
			<a href="<?php echo url_for('my_cv'); ?>" class="nav-item dropdown-item"><?php echo lang('user_menu_my_cv'); ?></a>
		</li>
		<li class="header-dropdown-link-list header-myfavourite">
			<a href="<?php echo url_for('workspace/my-favourite'); ?>" class="nav-item dropdown-item"><?php echo lang('user_menu_my_fav'); ?></a>
		</li>
		<li class="dropdown-divider"></li>
		<li class="header-dropdown-link-list header-settings">
			<a href="<?php echo url_for('settings'); ?>" class="nav-item dropdown-item"><?php echo lang('user_menu_my_pref'); ?></a>
		</li>
		<li class="header-dropdown-link-list header-widget">
			<a href="#" class="nav-item dropdown-item"
	           data-toggle="modal"
	           data-target="#modal_custom_widget"><?php echo lang('user_menu_dashboard_widget'); ?></a>
		</li>
		<li class="header-dropdown-link-list header-dropdown-flex">
			<a href="#" class="nav-item dropdown-item header-feedback gtm-header-feedback" title="<?php echo lang('user_menu_feedback'); ?>" data-backdrop="static" data-toggle="modal"
			   data-target="#modal_feedback">
	            <span class="header-nav-icons header-feedback-icon">
	                <svg xmlns="http://www.w3.org/2000/svg" width="16"
	                     height="16" fill="currentColor"
	                     class="bi bi-envelope" viewBox="0 0 16 16">
	                    <path fill-rule="evenodd"
	                          d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
	                </svg>
	            </span>
			</a>
			<a href="#" class="nav-item dropdown-item header-help gtm-header-help" title="<?php echo lang('user_menu_help'); ?>" data-toggle="modal"
			   data-target="#modal_help">
	            <span class="header-nav-icons header-help-icon">
	                <svg xmlns="http://www.w3.org/2000/svg" width="16"
	                     height="16" fill="currentColor"
	                     class="bi bi-question-circle" viewBox="0 0 16 16">
	                    <path fill-rule="evenodd"
	                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
	                    <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z"/>
	                </svg>
	            </span>
			</a>
			<a href="<?php echo url_for('logout'); ?>" class="nav-item dropdown-item header-logout gtm-header-logout" title="<?php echo lang('user_menu_log_out'); ?>">
	            <span class="header-nav-icons header-logout-icon">
	                <svg xmlns="http://www.w3.org/2000/svg" width="16"
	                     height="16" fill="currentColor"
	                     class="bi bi-box-arrow-right" viewBox="0 0 16 16">
	                    <path fill-rule="evenodd"
	                          d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
	                    <path fill-rule="evenodd"
	                          d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
	                </svg>
	            </span>
			</a>
		</li>
        <?php endif ?>

        <?php if($user->info['type'] === '1'): ?>
			<li class="header-dropdown-link-list header-usergreet">
			<div class="user-greet-row">
				<div class="user-greet-profile-photo">
					<img src="<?php echo imgCrop($user->info['photo'], 42, 42, 'assets/img/default-avatar.png') ?>" alt="Profile Photo">
				</div>
				<div class="user-info-container-menu">
				<span class="user-greet-lbl"><?php echo lang('user_menu_title'); ?> <span class="user-greet-name"><?php echo ucfirst($user->info['company']['name']) ?></span>!</span>
				<div class="user-info-menu">
				<div class="user-info-display"><?php echo lang('title_subscription_plan'); ?> <span><?php echo lang($user->info['plan']['title']); ?></span></div>
				<div class="user-info-display"><?php echo lang('member_since'); ?> <span><?php echo strftime('%b %Y', strtotime($user->info['date_created'])); ?></span></div>
			</div>
			</div>
			</div>
			<li class="dropdown-divider"></li>
		</li>
	        <!--<li class="dropdown-divider"></li>-->
			<li class="header-dropdown-link-list">
				<a href="<?php echo url_for('my-company-profile'); ?>" class="nav-item dropdown-item"><?php echo lang('user_menu_company_profile') ?></a>
			</li>
			<li class="header-dropdown-link-list">
				<a href="<?php echo url_for('my-company-page'); ?>" class="nav-item dropdown-item"><?php echo lang('user_menu_company_page') ?></a>
			</li>
			<li class="header-dropdown-link-list header-dropdown-flex">
			<a href="#" class="nav-item dropdown-item header-feedback gtm-header-feedback" title="<?php echo lang('user_menu_feedback'); ?>" data-backdrop="static" data-toggle="modal"
			   data-target="#modal_feedback">
	            <span class="header-nav-icons header-feedback-icon">
	                <svg xmlns="http://www.w3.org/2000/svg" width="16"
	                     height="16" fill="currentColor"
	                     class="bi bi-envelope" viewBox="0 0 16 16">
	                    <path fill-rule="evenodd"
	                          d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
	                </svg>
	            </span>
			</a>
			<a href="#" class="nav-item dropdown-item header-help gtm-header-help" title="<?php echo lang('user_menu_help'); ?>" data-toggle="modal"
			   data-target="#modal_help">
	            <span class="header-nav-icons header-help-icon">
	                <svg xmlns="http://www.w3.org/2000/svg" width="16"
	                     height="16" fill="currentColor"
	                     class="bi bi-question-circle" viewBox="0 0 16 16">
	                    <path fill-rule="evenodd"
	                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
	                    <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z"/>
	                </svg>
	            </span>
			</a>
			<a href="<?php echo url_for('logout'); ?>" class="nav-item dropdown-item header-logout gtm-header-logout" title="<?php echo lang('user_menu_log_out'); ?>">
	            <span class="header-nav-icons header-logout-icon">
	                <svg xmlns="http://www.w3.org/2000/svg" width="16"
	                     height="16" fill="currentColor"
	                     class="bi bi-box-arrow-right" viewBox="0 0 16 16">
	                    <path fill-rule="evenodd"
	                          d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
	                    <path fill-rule="evenodd"
	                          d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
	                </svg>
	            </span>
			</a>
		</li>
        <?php endif ?>
	</ul>
</li>

<?php $dashboard_widget = explode(',', $user->info['dashboard_widget']); ?>
<!-- Modal - Widget Customization -->
<div id="modal_custom_widget" class="modal modal-custom-widget fade" aria-labelledby="modal_custom_widget" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <form id="dashboardWidgetForm">
        <div class="modal-content">
			<div class="modal-header"><?php echo lang('customize_widget_modal_main_title') ?></div>
            <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                    <line x1="18" y1="6" x2="6" y2="18"></line>
                    <line x1="6" y1="6" x2="18" y2="18"></line>
                </svg>
            </button>
            <div class="modal-body">
				<div class="btn-switch-container btn-switch-dashboard-mode">
					<label class="btn-switch btn-color-mode-switch">
						<input type="checkbox" name="dashboard_mode" id="dashboard_mode" <?php echo $user->info['preference']['dashboard_mode'] === 'work' ? 'checked' : '' ?>>
						<label for="acc_susp" data-on="<?php echo lang('dashboard_mode_work') ?>" data-off="<?php echo lang('dashboard_mode_search') ?>" class="btn-color-mode-switch-inner"></label>
					</label>
				</div>
				<div class="dashboard-search-mode-container" id="dashboardSearchMode">
					<p class="modal-para"><?php echo lang('dashboard_mode_search_modal_description') ?></p>
					<div class="search-mode-dropdown-flex">
						<span class="search-mode-lbl pre-lbl"><?php echo lang('dashboard_mode_search_show') ?></span>
						<select name="search_mode" class="form-control form-control-input">
                            <option value="latest" <?php echo $user->info['preference']['search_mode'] === 'latest' ? 'selected' : '' ?>><?php echo lang('dashboard_mode_search_latest') ?></option>
                            <option value="match" <?php echo $user->info['preference']['search_mode'] === 'match' ? 'selected' : '' ?>><?php echo lang('dashboard_mode_search_best_matches') ?></option>
                        </select>
						<span class="search-mode-lbl post-lbl"><?php echo lang('dashboard_mode_search_task_first') ?>.</span>
					</div>
				</div>
				<div class="dashboard-work-mode-container" id="dashboardWorkMode" style="display:none;">
					<p class="modal-para"><?php echo lang('customize_widget_modal_description') ?></p>
					<div class="work-mode-select-container">
						<div class="work-mode-dropdown-flex">
							<span class="work-mode-lbl pre-lbl">Widget 1</span>
							<select name="dashboard_widget[]" data-id="widget-1" class="form-control form-control-input">
								<option value="" selected disabled><?php echo lang('dashboard_mode_work_select_widget') ?></option>
								<option value="1" <?php echo (isset($dashboard_widget[0]) && '1' === $dashboard_widget[0]) ? 'selected' : ''; ?>><?php echo lang('widget_today_task_title') ?></option> <!--Today's Task-->
								<option value="2" <?php echo (isset($dashboard_widget[0]) && '2' === $dashboard_widget[0]) ? 'selected' : ''; ?>><?php echo lang('widget_task_curated_title') ?></option> <!--Tasks Curated For You-->
								<option value="3" <?php echo (isset($dashboard_widget[0]) && '3' === $dashboard_widget[0]) ? 'selected' : ''; ?>><?php echo lang('widget_task_applied_for_title') ?></option> <!--Task Applied For-->
								<option value="4" <?php echo (isset($dashboard_widget[0]) && '4' === $dashboard_widget[0]) ? 'selected' : ''; ?>><?php echo lang('widget_total_earnings') ?></option> <!--Total Earnings-->
								<option value="5" <?php echo (isset($dashboard_widget[0]) && '5' === $dashboard_widget[0]) ? 'selected' : ''; ?>><?php echo lang('widget_total_expenditure') ?></option> <!--Total Expenditure-->
								<option value="6" <?php echo (isset($dashboard_widget[0]) && '6' === $dashboard_widget[0]) ? 'selected' : ''; ?>><?php echo lang('widget_urgent_task') ?></option> <!--Urgent Task-->
								<option value="7" <?php echo (isset($dashboard_widget[0]) && '7' === $dashboard_widget[0]) ? 'selected' : ''; ?>><?php echo lang('widget_ongoing_task_title') ?></option> <!--On-Going Task-->
								<option value="8" <?php echo (isset($dashboard_widget[0]) && '8' === $dashboard_widget[0]) ? 'selected' : ''; ?>><?php echo lang('widget_task_posted') ?></option> <!--Task Posted-->
								<option value="9" <?php echo (isset($dashboard_widget[0]) && '9' === $dashboard_widget[0]) ? 'selected' : ''; ?>><?php echo lang('widget_task_closed_title') ?></option> <!--Task Closed-->
								<option value="10" <?php echo (isset($dashboard_widget[0]) && '10' === $dashboard_widget[0]) ? 'selected' : ''; ?>><?php echo lang('widget_full_time_work_recommendation_title') ?></option> <!--Full-Time Work Recommendations-->
								<option value="11" <?php echo (isset($dashboard_widget[0]) && '11' === $dashboard_widget[0]) ? 'selected' : ''; ?>><?php echo lang('widget_full_time_position_posted_title') ?></option> <!--Full-Time Positions Posted-->
							</select>
						</div>
						<div class="work-mode-dropdown-flex">
							<span class="work-mode-lbl pre-lbl">Widget 2</span>
							<select name="dashboard_widget[]" data-id="widget-2" class="form-control form-control-input">
								<option value="" selected disabled><?php echo lang('dashboard_mode_work_select_widget') ?></option>
								<option value="1" <?php echo (isset($dashboard_widget[1]) && '1' === $dashboard_widget[1]) ? 'selected' : ''; ?>><?php echo lang('widget_today_task_title') ?></option> <!--Today's Task-->
								<option value="2" <?php echo (isset($dashboard_widget[1]) && '2' === $dashboard_widget[1]) ? 'selected' : ''; ?>><?php echo lang('widget_task_curated_title') ?></option> <!--Tasks Curated For You-->
								<option value="3" <?php echo (isset($dashboard_widget[1]) && '3' === $dashboard_widget[1]) ? 'selected' : ''; ?>><?php echo lang('widget_task_applied_for_title') ?></option> <!--Task Applied For-->
								<option value="4" <?php echo (isset($dashboard_widget[1]) && '4' === $dashboard_widget[1]) ? 'selected' : ''; ?>><?php echo lang('widget_total_earnings') ?></option> <!--Total Earnings-->
								<option value="5" <?php echo (isset($dashboard_widget[1]) && '5' === $dashboard_widget[1]) ? 'selected' : ''; ?>><?php echo lang('widget_total_expenditure') ?></option> <!--Total Expenditure-->
								<option value="6" <?php echo (isset($dashboard_widget[1]) && '6' === $dashboard_widget[1]) ? 'selected' : ''; ?>><?php echo lang('widget_urgent_task') ?></option> <!--Urgent Task-->
								<option value="7" <?php echo (isset($dashboard_widget[1]) && '7' === $dashboard_widget[1]) ? 'selected' : ''; ?>><?php echo lang('widget_ongoing_task_title') ?></option> <!--On-Going Task-->
								<option value="8" <?php echo (isset($dashboard_widget[1]) && '8' === $dashboard_widget[1]) ? 'selected' : ''; ?>><?php echo lang('widget_task_posted') ?></option> <!--Task Posted-->
								<option value="9" <?php echo (isset($dashboard_widget[1]) && '9' === $dashboard_widget[1]) ? 'selected' : ''; ?>><?php echo lang('widget_task_closed_title') ?></option> <!--Task Closed-->
								<option value="10" <?php echo (isset($dashboard_widget[1]) && '10' === $dashboard_widget[1]) ? 'selected' : ''; ?>><?php echo lang('widget_full_time_work_recommendation_title') ?></option> <!--Full-Time Work Recommendations-->
								<option value="11" <?php echo (isset($dashboard_widget[1]) && '11' === $dashboard_widget[1]) ? 'selected' : ''; ?>><?php echo lang('widget_full_time_position_posted_title') ?></option> <!--Full-Time Positions Posted-->
							</select>
						</div>
						<div class="work-mode-dropdown-flex">
							<span class="work-mode-lbl pre-lbl">Widget 3</span>
							<select name="dashboard_widget[]" data-id="widget-3" class="form-control form-control-input">
								<option value="" selected disabled><?php echo lang('dashboard_mode_work_select_widget') ?></option>
								<option value="1" <?php echo (isset($dashboard_widget[2]) && '1' === $dashboard_widget[2]) ? 'selected' : ''; ?>><?php echo lang('widget_today_task_title') ?></option> <!--Today's Task-->
								<option value="2" <?php echo (isset($dashboard_widget[2]) && '2' === $dashboard_widget[2]) ? 'selected' : ''; ?>><?php echo lang('widget_task_curated_title') ?></option> <!--Tasks Curated For You-->
								<option value="3" <?php echo (isset($dashboard_widget[2]) && '3' === $dashboard_widget[2]) ? 'selected' : ''; ?>><?php echo lang('widget_task_applied_for_title') ?></option> <!--Task Applied For-->
								<option value="4" <?php echo (isset($dashboard_widget[2]) && '4' === $dashboard_widget[2]) ? 'selected' : ''; ?>><?php echo lang('widget_total_earnings') ?></option> <!--Total Earnings-->
								<option value="5" <?php echo (isset($dashboard_widget[2]) && '5' === $dashboard_widget[2]) ? 'selected' : ''; ?>><?php echo lang('widget_total_expenditure') ?></option> <!--Total Expenditure-->
								<option value="6" <?php echo (isset($dashboard_widget[2]) && '6' === $dashboard_widget[2]) ? 'selected' : ''; ?>><?php echo lang('widget_urgent_task') ?></option> <!--Urgent Task-->
								<option value="7" <?php echo (isset($dashboard_widget[2]) && '7' === $dashboard_widget[2]) ? 'selected' : ''; ?>><?php echo lang('widget_ongoing_task_title') ?></option> <!--On-Going Task-->
								<option value="8" <?php echo (isset($dashboard_widget[2]) && '8' === $dashboard_widget[2]) ? 'selected' : ''; ?>><?php echo lang('widget_task_posted') ?></option> <!--Task Posted-->
								<option value="9" <?php echo (isset($dashboard_widget[2]) && '9' === $dashboard_widget[2]) ? 'selected' : ''; ?>><?php echo lang('widget_task_closed_title') ?></option> <!--Task Closed-->
								<option value="10" <?php echo (isset($dashboard_widget[2]) && '10' === $dashboard_widget[2]) ? 'selected' : ''; ?>><?php echo lang('widget_full_time_work_recommendation_title') ?></option> <!--Full-Time Work Recommendations-->
								<option value="11" <?php echo (isset($dashboard_widget[2]) && '11' === $dashboard_widget[2]) ? 'selected' : ''; ?>><?php echo lang('widget_full_time_position_posted_title') ?></option> <!--Full-Time Positions Posted-->
							</select>
						</div>
						<div class="work-mode-dropdown-flex">
							<span class="work-mode-lbl pre-lbl">Widget 4</span>
							<select name="dashboard_widget[]" data-id="widget-4" class="form-control form-control-input">
								<option value="" selected disabled><?php echo lang('dashboard_mode_work_select_widget') ?></option>
								<option value="1" <?php echo (isset($dashboard_widget[3]) && '1' === $dashboard_widget[3]) ? 'selected' : ''; ?>><?php echo lang('widget_today_task_title') ?></option> <!--Today's Task-->
								<option value="2" <?php echo (isset($dashboard_widget[3]) && '2' === $dashboard_widget[3]) ? 'selected' : ''; ?>><?php echo lang('widget_task_curated_title') ?></option> <!--Tasks Curated For You-->
								<option value="3" <?php echo (isset($dashboard_widget[3]) && '3' === $dashboard_widget[3]) ? 'selected' : ''; ?>><?php echo lang('widget_task_applied_for_title') ?></option> <!--Task Applied For-->
								<option value="4" <?php echo (isset($dashboard_widget[3]) && '4' === $dashboard_widget[3]) ? 'selected' : ''; ?>><?php echo lang('widget_total_earnings') ?></option> <!--Total Earnings-->
								<option value="5" <?php echo (isset($dashboard_widget[3]) && '5' === $dashboard_widget[3]) ? 'selected' : ''; ?>><?php echo lang('widget_total_expenditure') ?></option> <!--Total Expenditure-->
								<option value="6" <?php echo (isset($dashboard_widget[3]) && '6' === $dashboard_widget[3]) ? 'selected' : ''; ?>><?php echo lang('widget_urgent_task') ?></option> <!--Urgent Task-->
								<option value="7" <?php echo (isset($dashboard_widget[3]) && '7' === $dashboard_widget[3]) ? 'selected' : ''; ?>><?php echo lang('widget_ongoing_task_title') ?></option> <!--On-Going Task-->
								<option value="8" <?php echo (isset($dashboard_widget[3]) && '8' === $dashboard_widget[3]) ? 'selected' : ''; ?>><?php echo lang('widget_task_posted') ?></option> <!--Task Posted-->
								<option value="9" <?php echo (isset($dashboard_widget[3]) && '9' === $dashboard_widget[3]) ? 'selected' : ''; ?>><?php echo lang('widget_task_closed_title') ?></option> <!--Task Closed-->
								<option value="10" <?php echo (isset($dashboard_widget[3]) && '10' === $dashboard_widget[3]) ? 'selected' : ''; ?>><?php echo lang('widget_full_time_work_recommendation_title') ?></option> <!--Full-Time Work Recommendations-->
								<option value="11" <?php echo (isset($dashboard_widget[3]) && '11' === $dashboard_widget[3]) ? 'selected' : ''; ?>><?php echo lang('widget_full_time_position_posted_title') ?></option> <!--Full-Time Positions Posted-->
							</select>
						</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
                    <span class="btn-label"><?php echo lang('cancel') ?></span>
                    <span class="btn-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </span>
                </button>
                <button type="submit" class="btn-icon-full btn-confirm">
                    <span class="btn-label"><?php echo lang('apply') ?></span>
                    <span class="btn-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                            <polyline points="20 6 9 17 4 12"></polyline>
                        </svg>
                    </span>
                </button>
            </div>
        </div>
        </form>
    </div>
</div>
<!-- /.modal -->

<script>
    window.addEventListener('load', function(){
		$('body').append($('#modal_custom_widget').detach());
		$(document).on('click', '[name="dashboard_widget[]"]', function(){
			selected = $('[name="dashboard_widget[]"]:checked').length;
			if(selected >= 5){
				t('e', '<?php echo lang('dashboard_widget_maximum_select_error_message') ?>'); //You can only select maximum number of 4 widgets only.
				$(this).prop('checked', false);
				return false;
			}
		});

		$(document).on('submit', '#dashboardWidgetForm', function(e){
			e.preventDefault();
			el = $(this);
			data = el.serializeArray();
			/*if( el.find('#dashboard_mode').prop('checked') === window.dashboard_mode.prop('checked') && el.find("[name=search_mode]").val() === window.search_mode.value ){
                $('#modal_custom_widget').modal('hide');
			    return;
			}*/
			$.ajax({
				type:'POST',
				url: rootPath + 'dashboard',
				dataType : 'json',
				data: data,
				success: function(results){
					if(results.error == true){
						t('e', results.msg);
					}else{
						$('#modal_custom_widget').modal('hide');
                        <?php if( strpos(request_uri(), '/dashboard') !== false ): ?>
                        $('.widget-container').load(rootPath + 'dashboard/widget');
                        <?php else: ?>
						document.location.href = '<?php echo option('site_uri') . url_for('/dashboard') ?>';
						<?php endif ?>
						if( el.find('#dashboard_mode').prop('checked') !== window.dashboard_mode.prop('checked') || el.find("[name=search_mode]").val() !== window.search_mode.value ){
                            document.location.href = '<?php echo option('site_uri') . url_for('/dashboard') ?>';
						}
					}
				},
				error: function(error){
					ajax_error(error);
				},
				complete: function(){}
			});

			return false;
		});

		$('[name*="dashboard_widget[]"]').on('change', function(e){
		    var $this = $(e.currentTarget);
		    var $widgets = $('[name*="dashboard_widget[]"]');
		    var selected_values = [];
		    var disabled = false;
		    $widgets.each(function(idx, select){
		        if(select.value){
		            selected_values[idx] = select.value;
		        }
		    });

            $widgets.each(function(idx, widget){
                if( $(widget).data('id') !== $this.data('id') ) {
                    $(widget).children().each(function (idx2, option) {
                        if (selected_values.indexOf(option.value) !== -1 && selected_values.indexOf(option.value) != idx) {
                            option.disabled = true;
	                        option.classList.add('hide');
                        }else if(option.value){
                            option.disabled = false;
                           option.classList.remove('hide');
                        }
                    });
                }
            });

            $widgets.each(function(idx, select){
                if(!select.value.length){
                    disabled = true;
                }
            });

            $this.closest('form').find('button[type=submit]').prop('disabled', disabled);

		});
		<?php if( !empty($dashboard_widget) ): ?>
        $('[name*="dashboard_widget[]"]').first().trigger('change');
	    <?php endif ?>
	});
</script>


<script>
	$(document).ready(function () {
	    window.dashboard_mode = $("#dashboard_mode").clone();
	    window.search_mode = $("[name=search_mode]").clone()[0];
        $("#dashboard_mode").on("change", function () {
            dashboardMode(this);
        });
        <?php if( $user->info['preference']['dashboard_mode'] === 'work' ): ?>
        $("#dashboard_mode").trigger('change');
        <?php endif ?>
    });
    function dashboardMode(ele) {
        if ($(ele).prop("checked") == true) {
			$('#dashboardSearchMode').hide();
            $('#dashboardWorkMode').show();
            var $widgets = $('[name*="dashboard_widget[]"]');
            var disabled = false;
            $widgets.each(function(idx, select){
                if(!select.value.length){
                    disabled = true;
                }
            });
            $(ele).closest('form').find('button[type=submit]').prop('disabled', disabled);
        } else if ($(ele).prop("checked") == false) {
            $('#dashboardWorkMode').hide();
			$('#dashboardSearchMode').show();
            $(ele).closest('form').find('button[type=submit]').prop('disabled', false);
        }
    }
</script>