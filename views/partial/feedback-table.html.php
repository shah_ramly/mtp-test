<?php if($feedbacks): ?>
<table class="table feedback-table">
	<thead>
	<tr>
		<th class="info col-log-date"><?php echo lang('feedback_table_date_time_header') ?></th>
		<th class="info col-log-area"><?php echo lang('feedback_table_area_header') ?></th>
		<th class="info col-log-attachment"><?php echo lang('feedback_table_attachment_header') ?></th>
	</tr>
	</thead>
	<tbody>
    <?php if( $feedbacks->totalRecord ): ?>
    <?php foreach ($feedbacks->result() as $feedback): ?>
	    <tr>
		    <td class="info col-log-date">
			    <span class="logdate"><?php echo $feedback['date'] ?></span><br>
			    <span class="logtime"><?php echo $feedback['time'] ?></span>
		    </td>
		    <td class="info col-log-area">
			    <!--<span class="feedback-area"><?php /*echo lang('feedback_' . str_replace(['&', ' '], ['and', '_'], strtolower($feedback['area']))) */?></span>-->
			    <span class="feedback-area"><?php echo lang($feedback['area']) ?></span>
			    <span class="feedback-desc">
				    <?php if( $feedback['data'] ): ?>
				    <?php $data = json_decode($feedback['data'], true) ?>
				    <?php $report = isset($data['post_id']) ? 'Task ID: ' . substr($data['post_id'], -6) : ( isset($data['member_id']) ? 'Talent ID: ' . (string)('TL' . (20100 + $data['member_id'])) : '') ?>
				    <span class="task-row-taskid-val reported-id" style="color:#bababa"><?php echo $report ?></span><br/>
				    <?php endif ?>
                    <?php echo stripslashes($feedback['feedback']) ?>
                </span>
		    </td>
		    <td class="info col-log-attachment">
			    <div class="feedback-attachment-rows">
                    <?php if( !empty($feedback['attachments']) ): ?>
                        <?php $length = count($feedback['attachments']) ?>
                        <?php $count = 1; ?>
                        <?php foreach( $feedback['attachments'] as $key => $attach ): ?>
						    <a href="<?php echo url_for("/feedback/{$feedback['id']}/attachment/{$attach['file']}") ?>" class="feedback-attachment" title="<?php echo $attach['name'] ?>"><?php echo $count; ?></a>
                            <?php if($key+1 < $length) echo '<br>' ?>
                            <?php $count += 1; ?>
                        <?php endforeach; ?>
                    <?php endif ?>
			    </div>
		    </td>
	    </tr>
    <?php endforeach ?>
    <?php endif ?>
	</tbody>
</table>
<div class="table-listing-footer-pagination-feedback">
	<div class="footer-total-page">
		<?php echo lang('pagination_page') .' '. $feedbacks->page .' '. lang('pagination_of') .' '. $feedbacks->totalPage ?>
	</div>
	<nav class="nav-tbl-pagination">
		<ul class="pagination">
            <?php echo $feedbacks->display() ?>
		</ul>
	</nav>
</div>
<?php endif ?>