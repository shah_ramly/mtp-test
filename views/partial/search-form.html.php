<div class="col-xl-12 col-public-search">
	<ul class="nav nav-pills nav-pills-dashboard-search" id="pills-tab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="searchTaskTab-tab" data-toggle="pill" href="#searchTaskTab" role="tab" aria-controls="searchTaskTab" aria-selected="true"><?php echo lang('dashboard_search_for_on_demand_tasks'); ?></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="searchJobTab-tab" data-toggle="pill" href="#searchJobTab" role="tab" aria-controls="searchJobTab" aria-selected="false"><?php echo lang('dashboard_search_for_full_time_work'); ?></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="searchTalentTab-tab" data-toggle="pill" href="#searchTalentTab" role="tab" aria-controls="searchTalentTab" aria-selected="false"><?php echo lang('dashboard_search_for_talent'); ?></a>
		</li>
	</ul>
	<div class="tab-content" id="pills-tabSearch">
		<div class="tab-pane fade show active" id="searchTaskTab" role="tabpanel">
			<form action="<?php echo option('site_uri') . url_for('workspace/search') ?>" id="searchTaskForm" class="search-public-form" novalidate>
				<input type="hidden" name="scope" value="task" />
				<button type="submit" class="hide"></button>
				<div class="search-dropdown-filter-top-wrapper form-flex">
		            <div class="search-dropdown-filter-container search-dropdown-more-filter-container">
                        <div class="search-field-group">
                            <input type="text" class="form-control form-control-input" name="q"
                                placeholder="<?php echo lang('dashboard_search_tasks_by_keyword_placeholder'); ?>" id="inputBox" />
                            <span class="glass-icon">
                                <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                    <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                </svg>
                            </span>
                        </div>
		            </div>
		            <span class="search-dropdown-filter-btn">
                        <button class="btn-filter-search collapsed" data-toggle="collapse" type="button" onclick="return false"
                                data-target="#advancedFilterTaskCollapse"
                                aria-expanded="false"
                                aria-controls="advancedFilterTaskCollapse">
                            <span class="slider-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sliders" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M11.5 2a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM9.05 3a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0V3h9.05zM4.5 7a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM2.05 8a2.5 2.5 0 0 1 4.9 0H16v1H6.95a2.5 2.5 0 0 1-4.9 0H0V8h2.05zm9.45 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm-2.45 1a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0v-1h9.05z"></path>
                                </svg>
                            </span>
                            <i class="fa fa-caret-down"></i>
                        </button>
                    </span>
	            </div>

				<!-- <div class="search-dropdown-filter-top-wrapper form-flex">
					<div class="search-dropdown-filter-container">
						<input type="text" class="form-control form-control-input" name="q" placeholder="<?php echo lang('dashboard_search_tasks_by_keyword_placeholder'); ?>" id="inputBox">
					</div>
					<span class="search-dropdown-filter-btn">
		                <button class="btn-search-public" type="submit">
		                    <span class="btn-label">Search</span>
		                    <span class="public-search-icon search-icon">
		                        <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
		                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
		                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
		                        </svg>
		                    </span>
		                </button>
		            </span>
					<span class="bookmark-dropdown-filter-btn dropdown">
		                <button class="btn-bookmark dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
		                    <span class="btn-label"><?php echo lang('dashboard_save_search_button'); ?></span>
		                </button>
		                <div class="dropdown-menu dropdown-menu-saved-search">
		                    <div class="modal-advance-filter-container-left">
		                        <ul class="nav nav-pills nav-pills-save-search" id="pills-saveSearch" role="tablist">
		                            <li class="nav-item">
		                                <a class="nav-link active" id="saveTask-tab" data-toggle="tab" data-target="#taskSaveTaskTab" role="tab" aria-controls="saveTaskTab" aria-selected="true"><?php echo lang('dashboard_save_on_demand'); ?></a>
		                            </li>
		                            <li class="nav-item">
		                                <a class="nav-link" id="saveJob-tab" data-toggle="tab" data-target="#taskSaveJobTab" role="tab" aria-controls="saveJobTab" aria-selected="false"><?php echo lang('dashboard_save_full_time'); ?> </a>
		                            </li>
		                            <li class="nav-item">
		                                <a class="nav-link" id="saveTalent-tab" data-toggle="tab" data-target="#taskSaveTalentTab" role="tab" aria-controls="saveTalentTab" aria-selected="false"><?php echo lang('dashboard_save_talent'); ?></a>
		                            </li>
		                        </ul>
		                        <div class="tab-content" id="pills-tabSaveSearch">
		                            <div class="tab-pane fade show active" id="taskSaveTaskTab" role="tabpanel">
		                                <div class="saved-search-content">
                                            <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'task'; }))): ?>
                                            <?php foreach($task_search as $name => $data): ?>
	                                            <div class="saved-search-row">
	                                                <a href="<?php echo $data['url'] ?>" class="skills-label-link">
														<span class="tag label label-saved-task bubble-lbl">
															<?php echo $name ?>
														</span>
													</a>
		                                            <span class="tag-remove" data-toggle="modal"
		                                                  data-search-title="<?php echo $name ?>"
		                                                  data-search-id="<?php echo $data['id'] ?>"
		                                                  data-target="#modal_remove_saved"></span>
	                                            </div>
                                            <?php endforeach ?>
                                            <?php else: ?>
	                                            <div class="empty-state-container">
	                                                <div class="empty-img"><img src="<?php echo url_for('/assets/img/mtp-no-data-2.png') ?>" alt=""></div>
	                                                <span class="empty-lbl"><?php echo lang('no_saved_search') ?></span>
	                                                <span class="empty-desc"><?php echo lang('you_havent_saved_any_search_yet') ?>.</span>
	                                            </div>
                                            <?php endif ?>
                                        </div>
		                            </div>
		                            <div class="tab-pane fade show" id="taskSaveJobTab" role="tabpanel">
		                                <div class="saved-search-content">
                                            <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'job'; }))): ?>
                                            <?php foreach($task_search as $name => $data): ?>
	                                            <div class="saved-search-row">
	                                                <a href="<?php echo $data['url'] ?>" class="skills-label-link">
														<span class="tag label label-saved-job bubble-lbl">
															<?php echo $name ?>
														</span>
													</a>
		                                            <span class="tag-remove" data-toggle="modal"
		                                                  data-search-title="<?php echo $name ?>"
		                                                  data-search-id="<?php echo $data['id'] ?>"
		                                                  data-target="#modal_remove_saved"></span>
	                                            </div>
                                            <?php endforeach ?>
                                            <?php else: ?>
	                                            <div class="empty-state-container">
	                                                <div class="empty-img"><img src="<?php echo url_for('/assets/img/mtp-no-data-2.png') ?>" alt=""></div>
	                                                <span class="empty-lbl"><?php echo lang('no_saved_search') ?></span>
	                                                <span class="empty-desc"><?php echo lang('you_havent_saved_any_search_yet') ?>.</span>
	                                            </div>
                                            <?php endif ?>
                                        </div>
		                            </div>
		                            <div class="tab-pane fade show" id="taskSaveTalentTab" role="tabpanel">
		                                <div class="saved-search-content">
                                            <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'talent'; }))): ?>
                                            <?php foreach($task_search as $name => $data): ?>
	                                            <div class="saved-search-row">
	                                                <a href="<?php echo $data['url'] ?>" class="skills-label-link">
														<span class="tag label label-saved-talent bubble-lbl">
															<?php echo $name ?>
														</span>
													</a>
		                                            <span class="tag-remove" data-toggle="modal"
		                                                  data-search-title="<?php echo $name ?>"
		                                                  data-search-id="<?php echo $data['id'] ?>"
		                                                  data-target="#modal_remove_saved"></span>
	                                            </div>
                                            <?php endforeach ?>
                                            <?php else: ?>
	                                            <div class="empty-state-container">
	                                                <div class="empty-img"><img src="<?php echo url_for('/assets/img/mtp-no-data-2.png') ?>" alt=""></div>
	                                                <span class="empty-lbl"><?php echo lang('no_saved_search') ?></span>
	                                                <span class="empty-desc"><?php echo lang('you_havent_saved_any_search_yet') ?>.</span>
	                                            </div>
                                            <?php endif ?>
                                        </div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </span>
					<span class="bookmark-dropdown-addfilter-btn dropdown">
		                <button class="btn-addbookmark dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
		                    <span class="public-search-icon addsearch-icon">
		                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
		                            <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
		                        </svg>
		                    </span>
		                </button>
		                <div class="dropdown-menu dropdown-menu-saved-search dropdown-menu-add-saved-search">
		                    <div class="modal-advance-filter-container-left">
		                        <div class="modal-advanced-filter-container">
		                            <div class="keyword-filter-container">
		                                <label><?php echo lang('dashboard_save_search_keyword'); ?></label>
		                                <span class="task-keywords"><?php echo lang('dashboard_on_demand_save_search_keyword_default_text') ?></span>
		                            </div>
		                            <label class="modal-search-filter-lbl"><?php echo lang('dashboard_save_search_button'); ?></label>
		                            <div class="input-group-flex row-indfirstname">
		                                <input type="text" id="search_name" name="search_name" class="form-control form-control-input" placeholder="<?php echo lang('dashboard_on_demand_save_search_name_placeholder'); ?>">
		                            </div>
		                        </div>
		                        <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
		                            <div class="modal-filter-action form-block-filter-flex">
		                                <button type="button" class="btn-icon-full btn-confirm save-search" data-dismiss="modal" data-orientation="next">
		                                    <span class="btn-label"><?php echo lang('dashboard_save_button'); ?></span>
		                                    <span class="btn-icon">
		                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
		                                            <polyline points="20 6 9 17 4 12"></polyline>
		                                        </svg>
		                                    </span>
		                                </button>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </span>
				</div> -->
				<div class="search-advanced-filter-wrapper">
					<button class="btn-icon-invert-full btn-advanced-filter" type="button" data-toggle="collapse" data-target="#advancedFilterTaskCollapse" aria-expanded="false" aria-controls="collapseExample">
						<span class="btn-label"><?php echo lang('advanced_filter') ?></span>
						<span class="btn-icon-invert">
								<svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
							</svg>
						</span>
					</button>
					<div class="collapse dropdown-menu-advanced-filter" id="advancedFilterTaskCollapse">
						<div class="search-dropdown-filter-btm-wrapper form-flex">
							<div class="dropdown-filter-container filter-dashboard-savedsearch">
		                        <span class="bookmark-dropdown-filter-btn dropdown">
		                            <button class="btn-savesearch dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
		                            <span class="btn-label"><?php echo lang('dashboard_save_search'); ?></span>
		                            </button>
		                            <div class="dropdown-menu dropdown-menu-saved-search">
		                                <div class="modal-advance-filter-container-left">
		                                    <ul class="nav nav-pills nav-pills-save-search" id="pills-saveSearch" role="tablist">
		                                        <li class="nav-item">
		                                            <a class="nav-link active" id="saveTask-tab" data-toggle="tab" data-target="#taskSaveTaskTab" role="tab" aria-controls="saveTaskTab" aria-selected="true"><?php echo lang('dashboard_save_on_demand'); ?></a>
		                                        </li>
		                                        <li class="nav-item">
		                                            <a class="nav-link" id="saveJob-tab" data-toggle="tab" data-target="#taskSaveJobTab" role="tab" aria-controls="saveJobTab" aria-selected="false"><?php echo lang('dashboard_save_full_time'); ?></a>
		                                        </li>
		                                        <li class="nav-item">
		                                            <a class="nav-link" id="saveTalent-tab" data-toggle="tab" data-target="#taskSaveTalentTab" role="tab" aria-controls="saveTalentTab" aria-selected="false"><?php echo lang('dashboard_save_talent'); ?></a>
		                                        </li>
		                                    </ul>
		                                    <div class="tab-content" id="pills-tabSaveSearch">
		                                        <div class="tab-pane fade show active" id="taskSaveTaskTab" role="tabpanel">
		                                            <div class="saved-search-content">
		                                                <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'task'; }))): ?>
		                                                <?php foreach($task_search as $name => $data): ?>
		                                                    <div class="saved-search-row">
		                                                        <a href="<?php echo $data['url'] ?>" class="skills-label-link">
		                                                            <span class="tag label label-saved-task bubble-lbl"><?php echo $name ?></span>
		                                                        </a>
		                                                        <span class="tag-remove" data-toggle="modal"
		                                                            data-search-title="<?php echo $name ?>"
		                                                            data-search-id="<?php echo $data['id'] ?>"
		                                                            data-target="#modal_remove_saved"></span>
		                                                    </div>
		                                                <?php endforeach ?>
		                                                <?php else: ?>
		                                                    <div class="empty-state-container">
		                                                        <div class="empty-img"><img src="<?php echo url_for('/assets/img/mtp-no-data-2.png') ?>" alt=""></div>
		                                                        <span class="empty-lbl"><?php echo lang('no_saved_search') ?></span>
		                                                        <span class="empty-desc"><?php echo lang('you_havent_saved_any_search_yet') ?>.</span>
		                                                    </div>
		                                                <?php endif ?>
		                                            </div>
		                                        </div>
		                                        <div class="tab-pane fade show" id="taskSaveJobTab" role="tabpanel">
		                                            <div class="saved-search-content">
		                                                <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'job'; }))): ?>
		                                                <?php foreach($task_search as $name => $data): ?>
		                                                    <div class="saved-search-row">
		                                                        <a href="<?php echo $data['url'] ?>" class="skills-label-link">
		                                                            <span class="tag label label-saved-job bubble-lbl">
		                                                                <?php echo $name ?>
		                                                            </span>
		                                                        </a>
		                                                        <span class="tag-remove" data-toggle="modal"
		                                                            data-search-title="<?php echo $name ?>"
		                                                            data-search-id="<?php echo $data['id'] ?>"
		                                                            data-target="#modal_remove_saved"></span>
		                                                    </div>
		                                                <?php endforeach ?>
		                                                <?php else: ?>
		                                                    <div class="empty-state-container">
		                                                        <div class="empty-img"><img src="<?php echo url_for('/assets/img/mtp-no-data-2.png') ?>" alt=""></div>
		                                                        <span class="empty-lbl"><?php echo lang('no_saved_search') ?></span>
		                                                        <span class="empty-desc"><?php echo lang('you_havent_saved_any_search_yet') ?>.</span>
		                                                    </div>
		                                                <?php endif ?>
		                                            </div>
		                                        </div>
		                                        <div class="tab-pane fade show" id="taskSaveTalentTab" role="tabpanel">
		                                            <div class="saved-search-content">
		                                                <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'talent'; }))): ?>
		                                                <?php foreach($task_search as $name => $data): ?>
		                                                    <div class="saved-search-row">
		                                                        <a href="<?php echo $data['url'] ?>" class="skills-label-link">
		                                                            <span class="tag label label-saved-talent bubble-lbl">
		                                                                <?php echo $name ?>
		                                                            </span>
		                                                        </a>
		                                                        <span class="tag-remove" data-toggle="modal"
		                                                            data-search-title="<?php echo $name ?>"
		                                                            data-search-id="<?php echo $data['id'] ?>"
		                                                            data-target="#modal_remove_saved"></span>
		                                                    </div>
		                                                <?php endforeach ?>
		                                                <?php else: ?>
		                                                    <div class="empty-state-container">
		                                                        <div class="empty-img"><img src="<?php echo url_for('/assets/img/mtp-no-data-2.png') ?>" alt=""></div>
		                                                        <span class="empty-lbl"><?php echo lang('no_saved_search') ?></span>
		                                                        <span class="empty-desc"><?php echo lang('you_havent_saved_any_search_yet') ?>.</span>
		                                                    </div>
		                                                <?php endif ?>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                        </span>
		                        <span class="bookmark-dropdown-addfilter-btn dropdown">
		                            <button class="btn-addsavesearch dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
		                                <span class="public-search-icon addsearch-icon">
		                                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
		                                        <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
		                                    </svg>
		                                </span>
		                            </button>
		                            <div class="dropdown-menu dropdown-menu-saved-search dropdown-menu-add-saved-search">
		                                <div class="modal-advance-filter-container-left">
		                                    <div class="modal-advanced-filter-container">
		                                        <div class="keyword-filter-container">
		                                            <label><?php echo lang('dashboard_save_search_keyword'); ?></label>
		                                            <span><?php echo request('scope') === 'task' && !empty($keywords) ? $keywords : lang('dashboard_on_demand_save_search_keyword_default_text') ?></span>
		                                        </div>
		                                        <label class="modal-search-filter-lbl"><?php echo lang('dashboard_save_search_button'); ?></label>
		                                        <div class="input-group-flex row-indfirstname">
		                                            <input type="text" id="search_name" name="search_name" class="form-control form-control-input" placeholder="<?php echo lang('dashboard_on_demand_save_search_name_placeholder'); ?>" />
		                                        </div>
		                                    </div>
		                                    <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
		                                        <div class="modal-filter-action form-block-filter-flex">
		                                            <button type="button" class="btn-icon-full btn-confirm save-search" data-dismiss="modal" data-orientation="next">
		                                                <span class="btn-label"><?php echo lang('dashboard_save_button'); ?></span>
		                                                <span class="btn-icon">
		                                                    <svg
		                                                            xmlns="http://www.w3.org/2000/svg"
		                                                            width="24"
		                                                            height="24"
		                                                            viewBox="0 0 24 24"
		                                                            fill="none"
		                                                            stroke="currentColor"
		                                                            stroke-width="2.5"
		                                                            stroke-linecap="round"
		                                                            stroke-linejoin="arcs"
		                                                    >
		                                                        <polyline points="20 6 9 17 4 12"></polyline>
		                                                    </svg>
		                                                </span>
		                                            </button>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                        </span>
		                    </div>
							<div class="dropdown dropdown-filter-container filter-dashboard-taskcategory">
								<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownTaskCategory" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<label class="filter-btn-lbl"><?php echo lang('dashboard_category'); ?></label>
								</button>
								<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTaskCategory">
									<div class="form-block-task-filter">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input select-all-category"
											       name="category" value="0" id="taskfiltercat_0">
											<label class="custom-control-label" for="taskfiltercat_0"><?php echo lang('dashboard_category_all'); ?></label>
										</div>
		                                <?php if(isset($task_categories)): ?>
		                                <?php foreach($task_categories as $category): ?>
										<div class="custom-control custom-checkbox categories-input">
											<input type="checkbox" class="custom-control-input" name="category"
											       data-input-status="false"
											       value="<?php echo $category['id'] ?>" id="taskfiltercat_<?php echo $category['id'] ?>">
											<label class="custom-control-label" for="taskfiltercat_<?php echo $category['id'] ?>"><?php echo $category['title'] ?></label>
										</div>
		                                <?php endforeach ?>
		                                <?php endif ?>
									</div>
								</div>
							</div>
							<div class="dropdown dropdown-filter-container filter-dashboard-tasklocation">
								<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownTaskLocation" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<label class="filter-btn-lbl"><?php echo lang('dashboard_location'); ?></label>
								</button>
								<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTaskLocation">
									<div class="form-block-task-filter">
										<div class="form-filter-select-by-location">
											<select class="form-control form-control-input" placeholder="State">
												<option selected=""><?php echo lang('dashboard_location_select_by'); ?></option>
												<option value="locbystate"><?php echo lang('dashboard_location_select_by_state'); ?></option>
												<option value="locbynearest"><?php echo lang('dashboard_location_select_by_nearest'); ?></option>
											</select>
										</div>
										<div class="form-filter-state" style="<?php echo !isset($filters['state']) ? 'display: none' : '' ?>">
											<select class="form-control form-control-input state" name="state" id="listing_state"
											        data-target="city"
											        data-input-status="false"
											        placeholder="<?php echo lang('dashboard_location_select_by_state'); ?>">
												<option value="" selected><?php echo lang('dashboard_location_select_by_select_state'); ?></option>
		                                        <?php foreach($states as $state): ?>
													<option value="<?php echo $state['id'] ?>"><?php echo $state['name'] ?></option>
		                                        <?php endforeach; ?>
											</select>
										</div>
										<div class="form-filter-city" style="<?php echo !isset($filters['city']) ? 'display: none' : '' ?>">
											<select class="form-control form-control-input" name="city" id="listing_city"
											        data-input-status="false"
											        placeholder="City">
												<option selected value=""><?php echo lang('dashboard_location_select_by_town'); ?></option>
		                                        <?php foreach($cities as $city): ?>
													<option value="<?php echo $city['id'] ?>">
		                                                <?php echo $city['name'] ?>
													</option>
		                                        <?php endforeach; ?>
											</select>
										</div>
										<div class="form-filter-within" style="display:none;">
											<select class="form-control form-control-input" name="within" placeholder="<?php echo lang('dashboard_location_select_within_word'); ?>">
												<option value="" selected><?php echo lang('dashboard_location_select_within'); ?></option>
												<option value="5km"><?php echo lang('dashboard_location_select_within_word'); ?> 5km</option>
												<option value="10km"><?php echo lang('dashboard_location_select_within_word'); ?> 10km</option>
												<option value="20km"><?php echo lang('dashboard_location_select_within_word'); ?> 20km</option>
												<option value="30km"><?php echo lang('dashboard_location_select_within_word'); ?> 30km</option>
												<option value="50km"><?php echo lang('dashboard_location_select_within_word'); ?> 50km</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="dropdown dropdown-filter-container filter-dashboard-tasktype">
								<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownTaskType" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<label class="filter-btn-lbl"><?php echo lang('dashboard_task_type'); ?></label>
								</button>
								<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTaskType">
									<div class="form-block-task-filter">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="task_type" value="by-hour" id="filternat_1">
											<label class="custom-control-label" for="filternat_1"><?php echo lang('dashboard_task_type_by_hour'); ?></label>
										</div>
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="task_type" value="lump-sum" id="filternat_2">
											<label class="custom-control-label" for="filternat_2"><?php echo lang('dashboard_task_type_lump_sum'); ?></label>
										</div>
									</div>
								</div>
							</div>
							<div class="dropdown dropdown-filter-container filter-dashboard-payrange">
								<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownPayRange" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<label class="filter-btn-lbl"><?php echo lang('dashboard_pay_range'); ?></label>
								</button>
								<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownPayRange">
									<div class="form-block-task-filter">
										<select class="form-control form-control-input" name="task_budget" data-input-status="false">
											<option value="" selected><?php echo lang('dashboard_pay_range_select_amount'); ?></option>
											<option value="1|50">RM1 - RM50</option>
											<option value="51|100">RM51 - RM100</option>
											<option value="101|500">RM101 - RM500</option>
											<option value="501|999">RM501 - RM999</option>
											<option value="1000|100000">RM1000 <?php echo lang('dashboard_pay_range_select_amount_and_above'); ?></option>
										</select>
									</div>
								</div>
							</div>
							<div class="dropdown dropdown-filter-container filter-dashboard-taskdate">
								<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownTaskDate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<label class="filter-btn-lbl"><?php echo lang('dashboard_filter_date'); ?></label>
								</button>
								<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTaskDate">
									<div class="form-block-task-filter">
										<div class="form-filter-datepostedstart">
											<select class="form-control form-control-input" id="selectDatePostedStart" name="date_type"
											        data-input-status="false"
											        placeholder="<?php echo lang('public_search_posted_date'); ?>">
												<option value="" selected><?php echo lang('dashboard_filter_date_select'); ?></option>
												<option value="postedDate"><?php echo lang('dashboard_filter_posted_date'); ?></option>
												<option value="startDate"><?php echo lang('dashboard_filter_start_date'); ?></option>
											</select>
										</div>
										<div class="form-filter-dateposted" style="display:none;">
											<div class="form-filter-dateposted">
												<select class="form-control form-control-input" name="date_period"
												        data-input-status="false"
												        id="search_filter_dateposted" placeholder="<?php echo lang('public_search_posted_date'); ?>">
													<option value="" selected><?php echo lang('dashboard_filter_date_select'); ?></option>
													<option value="any"><?php echo lang('dashboard_filter_any_time'); ?></option>
													<option value="24h"><?php echo lang('dashboard_filter_last_24'); ?></option>
													<option value="3d"><?php echo lang('dashboard_filter_last_3'); ?></option>
													<option value="7d"><?php echo lang('dashboard_filter_last_7'); ?></option>
													<option value="14d"><?php echo lang('dashboard_filter_last_14'); ?></option>
													<option value="30d"><?php echo lang('dashboard_filter_last_30'); ?></option>
													<option value="customrange"><?php echo lang('dashboard_filter_custom'); ?></option>
												</select>
											</div>
											<div class="task-filter-row task-filter-row-datepicker-fromto" style="display:none;">
												<div class="form-group form-datetime-search-filter-from">
													<div class="form-group">
														<div class="input-group input-group-datetimepicker date" id="form_datetime_search_filter_from" data-target-input="nearest">
															<input type="text" class="form-control datetimepicker-input" name="from" data-target="#form_datetime_search_filter_from" placeholder="<?php echo lang('public_search_date_from'); ?>" autocomplete="off" />
															<span class="input-group-addon" data-target="#form_datetime_search_filter_from" data-toggle="datetimepicker">
				                                                <span class="fa fa-calendar"></span>
				                                            </span>
														</div>
													</div>

												</div>
												<div class="form-group form-datetime-search-filter-to">
													<div class="form-group">
														<div class="input-group input-group-datetimepicker date" id="form_datetime_search_filter_to" data-target-input="nearest">
															<input type="text" class="form-control datetimepicker-input" name="to" data-target="#form_datetime_search_filter_to" placeholder="<?php echo lang('public_search_date_to'); ?>" autocomplete="off" />
															<span class="input-group-addon" data-target="#form_datetime_search_filter_to" data-toggle="datetimepicker">
				                                                <span class="fa fa-calendar"></span>
				                                            </span>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-filter-startdate" style="display:none;">
											<div class="form-filter-startdate">
												<select class="form-control form-control-input" id="search_filter_startdate" name="date_period"
												        placeholder="<?php echo lang('public_search_start_date'); ?>">
													<option value="" selected><?php echo lang('public_search_select'); ?></option>
													<option value="any"><?php echo lang('public_search_any_time'); ?></option>
													<option value="24h"><?php echo sprintf(lang('public_search_next_x_hours'), 24); ?></option>
													<option value="3d"><?php echo sprintf(lang('public_search_next_x_days'), 3); ?></option>
													<option value="7d"><?php echo sprintf(lang('public_search_next_x_days'), 7); ?></option>
													<option value="14d"><?php echo sprintf(lang('public_search_next_x_days'), 14); ?></option>
													<option value="30d"><?php echo sprintf(lang('public_search_next_x_days'), 30); ?></option>
													<option value="customrange"><?php echo lang('public_search_custom_range'); ?></option>
												</select>
											</div>
											<div class="task-filter-row task-filter-row-datepicker-fromto" style="display:none;">
												<div class="form-group form-datetime-search-filter-from">
													<div class="form-group">
														<div class="input-group input-group-datetimepicker date" id="form_datetime_search_filter_from_start" data-target-input="nearest">
															<input type="text" class="form-control datetimepicker-input" name="from" data-target="#form_datetime_search_filter_from_start" placeholder="<?php echo lang('public_search_date_from'); ?>" autocomplete="off" />
															<span class="input-group-addon" data-target="#form_datetime_search_filter_from_start" data-toggle="datetimepicker">
				                                                <span class="fa fa-calendar"></span>
				                                            </span>
														</div>
													</div>

												</div>
												<div class="form-group form-datetime-search-filter-to">
													<div class="form-group">
														<div class="input-group input-group-datetimepicker date" id="form_datetime_search_filter_to_start" data-target-input="nearest">
															<input type="text" class="form-control datetimepicker-input" name="to" data-target="#form_datetime_search_filter_to_start" placeholder="<?php echo lang('public_search_date_to'); ?>" autocomplete="off" />
															<span class="input-group-addon" data-target="#form_datetime_search_filter_to_start" data-toggle="datetimepicker">
				                                                <span class="fa fa-calendar"></span>
				                                            </span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="dropdown dropdown-filter-container filter-dashboard-taskskills">
								<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownTaskSkills" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<label class="filter-btn-lbl"><?php echo lang('dashboard_skills'); ?></label>
								</button>
								<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTaskSkills">
									<div class="form-block-task-filter">
										<div class="bstags-skills">
											<input type="text" name="skills" id="task-skills" data-role="tagsinput" class="bootstrap-tagsinput" placeholder="<?php echo lang('dashboard_skills_placeholder'); ?>" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
			<form action="<?php echo option('site_uri') . url_for('workspace/search') ?>" id="hiddenSearchTaskForm" method="get">
				<input type="hidden" name="q" value="" disabled />
				<input type="hidden" name="scope" value="task" />
				<input type="hidden" name="type" value="task" />
				<input type="hidden" name="page" disabled value="1" />
				<input type="hidden" name="category" disabled />
				<input type="hidden" name="location_by" disabled />
				<input type="hidden" name="within" disabled />
				<input type="hidden" name="state" disabled />
				<input type="hidden" name="city" disabled />
				<input type="hidden" name="task_type" disabled />
				<input type="hidden" name="task_budget" disabled />
				<input type="hidden" name="date_type" disabled />
				<input type="hidden" name="date_period" disabled />
				<input type="hidden" name="from" disabled />
				<input type="hidden" name="to" disabled />
				<input type="hidden" name="skills" disabled />
			</form>
		</div>
		<div class="tab-pane fade show" id="searchJobTab" role="tabpanel">
			<form action="<?php echo option('site_uri') . url_for('workspace/search') ?>" id="searchJobForm" class="search-public-form" novalidate>
				<input type="hidden" name="scope" value="job" />
				<button type="submit" class="hide"></button>
				<div class="search-dropdown-filter-top-wrapper form-flex">
					<div class="search-dropdown-filter-container search-dropdown-more-filter-container">
						<div class="search-field-group">
							<input type="text" class="form-control form-control-input" name="q"
							       placeholder="<?php echo lang('dashboard_search_jobs_by_keyword_placeholder'); ?>" id="inputBox">
							<span class="glass-icon">
                                <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                    <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                </svg>
                            </span>
						</div>
					</div>
					<span class="search-dropdown-filter-btn">
                        <button class="btn-filter-search collapsed" data-toggle="collapse" type="button"
                              data-target="#advancedFilterTaskCollapse"
                              aria-expanded="false"
                              aria-controls="advancedFilterTaskCollapse">
                            <span class="slider-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sliders" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M11.5 2a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM9.05 3a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0V3h9.05zM4.5 7a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM2.05 8a2.5 2.5 0 0 1 4.9 0H16v1H6.95a2.5 2.5 0 0 1-4.9 0H0V8h2.05zm9.45 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm-2.45 1a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0v-1h9.05z"></path>
                                </svg>
                            </span>
                            <i class="fa fa-caret-down"></i>
                        </button>
		            </span>
				</div>
				<div class="search-advanced-filter-wrapper">
					<button class="btn-icon-invert-full btn-advanced-filter" type="button" data-toggle="collapse" data-target="#advancedFilterTaskCollapse" aria-expanded="false" aria-controls="collapseExample">
						<span class="btn-label"><?php echo lang('advanced_filter') ?></span>
						<span class="btn-icon-invert">
								<svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
							</svg>
						</span>
					</button>
					<div class="collapse dropdown-menu-advanced-filter" id="advancedFilterTaskCollapse">
						<div class="search-dropdown-filter-btm-wrapper form-flex">
							<div class="dropdown-filter-container filter-dashboard-savedsearch">
								<span class="bookmark-dropdown-filter-btn dropdown">
		                            <button class="btn-savesearch dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
		                                <span class="btn-label"><?php echo lang('dashboard_save_search'); ?></span>
		                            </button>
					                <div class="dropdown-menu dropdown-menu-saved-search">
					                    <div class="modal-advance-filter-container-left">
					                        <ul class="nav nav-pills nav-pills-save-search" id="pills-saveSearch" role="tablist">
					                            <li class="nav-item">
					                                <a class="nav-link active" id="saveTask-tab" data-toggle="tab" data-target="#jobSaveTaskTab" role="tab" aria-controls="saveTaskTab" aria-selected="true"><?php echo lang('dashboard_save_on_demand'); ?></a>
					                            </li>
					                            <li class="nav-item">
					                                <a class="nav-link" id="saveJob-tab" data-toggle="tab" data-target="#jobSaveJobTab" role="tab" aria-controls="saveJobTab" aria-selected="false"><?php echo lang('dashboard_save_full_time'); ?> </a>
					                            </li>
					                            <li class="nav-item">
					                                <a class="nav-link" id="saveTalent-tab" data-toggle="tab" data-target="#jobSaveTalentTab" role="tab" aria-controls="saveTalentTab" aria-selected="false"><?php echo lang('dashboard_save_talent'); ?></a>
					                            </li>
					                        </ul>
					                        <div class="tab-content" id="pills-tabSaveSearch">
					                            <div class="tab-pane fade show active" id="jobSaveTaskTab" role="tabpanel">
					                                <div class="saved-search-content">
			                                            <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'task'; }))): ?>
			                                                <?php foreach($task_search as $name => $data): ?>
					                                            <div class="saved-search-row">
				                                                <a href="<?php echo $data['url'] ?>" class="skills-label-link">
																	<span class="tag label label-saved-task bubble-lbl">
																		<?php echo $name ?>
																	</span>
																</a>
					                                            <span class="tag-remove" data-toggle="modal"
					                                                  data-search-title="<?php echo $name ?>"
					                                                  data-search-id="<?php echo $data['id'] ?>"
					                                                  data-target="#modal_remove_saved"></span>
				                                            </div>
			                                                <?php endforeach ?>
			                                            <?php endif ?>
			                                        </div>
					                            </div>
					                            <div class="tab-pane fade show" id="jobSaveJobTab" role="tabpanel">
					                                <div class="saved-search-content">
			                                            <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'job'; }))): ?>
			                                                <?php foreach($task_search as $name => $data): ?>
					                                            <div class="saved-search-row">
				                                                <a href="<?php echo $data['url'] ?>" class="skills-label-link">
																	<span class="tag label label-saved-job bubble-lbl">
																		<?php echo $name ?>
																	</span>
																</a>
					                                            <span class="tag-remove" data-toggle="modal"
					                                                  data-search-title="<?php echo $name ?>"
					                                                  data-search-id="<?php echo $data['id'] ?>"
					                                                  data-target="#modal_remove_saved"></span>
				                                            </div>
			                                                <?php endforeach ?>
			                                            <?php endif ?>
			                                        </div>
					                            </div>
					                            <div class="tab-pane fade show" id="jobSaveTalentTab" role="tabpanel">
					                                <div class="saved-search-content">
			                                            <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'talent'; }))): ?>
			                                                <?php foreach($task_search as $name => $data): ?>
					                                            <div class="saved-search-row">
				                                                <a href="<?php echo $data['url'] ?>" class="skills-label-link">
																	<span class="tag label label-saved-talent bubble-lbl">
																		<?php echo $name ?>
																	</span>
																</a>
					                                            <span class="tag-remove" data-toggle="modal"
					                                                  data-search-title="<?php echo $name ?>"
					                                                  data-search-id="<?php echo $data['id'] ?>"
					                                                  data-target="#modal_remove_saved"></span>
				                                            </div>
			                                                <?php endforeach ?>
			                                            <?php endif ?>
			                                        </div>
					                            </div>
					                        </div>
					                    </div>
					                </div>
					            </span>
								<span class="bookmark-dropdown-addfilter-btn dropdown">
					                <button class="btn-addsavesearch dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
					                    <span class="public-search-icon addsearch-icon">
					                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
					                            <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
					                        </svg>
					                    </span>
					                </button>
					                <div class="dropdown-menu dropdown-menu-saved-search dropdown-menu-add-saved-search">
					                    <div class="modal-advance-filter-container-left">
					                        <div class="modal-advanced-filter-container">
					                            <div class="keyword-filter-container">
					                                <label><?php echo lang('dashboard_save_search_keyword'); ?></label>
					                                <span class="job-keywords"><?php echo lang('dashboard_full_time_save_search_keyword_default_text') ?></span>
					                            </div>
					                            <label class="modal-search-filter-lbl"><?php echo lang('dashboard_save_search_button'); ?></label>
					                            <div class="input-group-flex row-indfirstname">
					                                <input type="text" id="search_name" name="search_name" class="form-control form-control-input" placeholder="<?php echo lang('dashboard_full_time_save_search_name_placeholder'); ?>">
					                            </div>
					                        </div>
					                        <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
					                            <div class="modal-filter-action form-block-filter-flex">
					                                <button type="button" class="btn-icon-full btn-confirm save-search" data-dismiss="modal" data-orientation="next">
					                                    <span class="btn-label"><?php echo lang('dashboard_save_button'); ?></span>
					                                    <span class="btn-icon">
					                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					                                            <polyline points="20 6 9 17 4 12"></polyline>
					                                        </svg>
					                                    </span>
					                                </button>
					                            </div>
					                        </div>
					                    </div>
					                </div>
					            </span>
							</div>
							<div class="dropdown dropdown-filter-container filter-dashboard-jobcategory">
								<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownJobCategory" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<label class="filter-btn-lbl"><?php echo lang('dashboard_category'); ?></label>
								</button>
								<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownJobCategory">
									<div class="form-block-task-filter">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" name="category" value="0"
											       class="custom-control-input select-all-category" id="jobfilterjobcat_0">
											<label class="custom-control-label" for="jobfilterjobcat_0"><?php echo lang('dashboard_category_all'); ?></label>
										</div>
		                                <?php if(isset($job_categories)): ?>
		                                <?php foreach($job_categories as $category): ?>
										<div class="custom-control custom-checkbox categories-input">
											<input type="checkbox" class="custom-control-input" name="category"
											       data-input-status="false"
											       value="<?php echo $category['id'] ?>" id="jobfiltercat_<?php echo $category['id'] ?>">
											<label class="custom-control-label" for="jobfiltercat_<?php echo $category['id'] ?>"><?php echo $category['title'] ?></label>
										</div>
		                                <?php endforeach ?>
		                                <?php endif ?>
									</div>
								</div>
							</div>
							<div class="dropdown dropdown-filter-container filter-dashboard-joblocation">
								<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownJobLocation" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<label class="filter-btn-lbl"><?php echo lang('dashboard_location'); ?></label>
								</button>
								<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownJobLocation">
									<div class="form-block-task-filter">
										<div class="form-filter-select-by-location">
											<select class="form-control form-control-input" placeholder="State">
												<option disabled="" selected=""><?php echo lang('dashboard_location_select_by'); ?></option>
												<option value="locbystate"><?php echo lang('dashboard_location_select_by_state'); ?></option>
												<option value="locbynearest"><?php echo lang('dashboard_location_select_by_nearest'); ?></option>
											</select>
										</div>
										<div class="form-filter-state" style="<?php echo !isset($filters['state']) ? 'display: none' : '' ?>">
											<select class="form-control form-control-input state" name="state" id="listing_state"
											        data-target="city"
											        data-input-status="false"
											        placeholder="State">
												<option value="" selected><?php echo lang('dashboard_location_select_by_state'); ?></option>
		                                        <?php foreach($states as $state): ?>
													<option value="<?php echo $state['id'] ?>"><?php echo $state['name'] ?></option>
		                                        <?php endforeach; ?>
											</select>
										</div>
										<div class="form-filter-city" style="<?php echo !isset($filters['city']) ? 'display: none' : '' ?>">
											<select class="form-control form-control-input" name="city" id="listing_city"
											        data-input-status="false"
											        placeholder="City">
												<option selected value=""><?php echo lang('dashboard_location_select_by_town'); ?></option>
		                                        <?php foreach($cities as $city): ?>
													<option value="<?php echo $city['id'] ?>">
		                                                <?php echo $city['name'] ?>
													</option>
		                                        <?php endforeach; ?>
											</select>
										</div>
										<div class="form-filter-within" style="display:none;">
											<select class="form-control form-control-input" name="within" placeholder="<?php echo lang('dashboard_location_select_within_word'); ?>">
												<option value="" selected><?php echo lang('dashboard_location_select_within'); ?></option>
												<option value="5km"><?php echo lang('dashboard_location_select_within_word'); ?> 5km</option>
												<option value="10km"><?php echo lang('dashboard_location_select_within_word'); ?> 10km</option>
												<option value="20km"><?php echo lang('dashboard_location_select_within_word'); ?> 20km</option>
												<option value="30km"><?php echo lang('dashboard_location_select_within_word'); ?> 30km</option>
												<option value="50km"><?php echo lang('dashboard_location_select_within_word'); ?> 50km</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="dropdown dropdown-filter-container filter-dashboard-jobdate">
								<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownJobDate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<label class="filter-btn-lbl"><?php echo lang('dashboard_filter_date'); ?></label>
								</button>
								<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownJobDate">
									<div class="form-block-task-filter">
										<div class="form-filter-datepostedstart">
											<div class="form-filter-datepostedstart">
												<select class="form-control form-control-input" id="selectDatePostedStart" name="date_type"
												        data-input-status="false"
												        placeholder="<?php echo lang('public_search_posted_date'); ?>">
													<option value="" selected><?php echo lang('dashboard_filter_date_select'); ?></option>
													<option value="postedDate"><?php echo lang('dashboard_filter_posted_date'); ?></option>
													<option value="closeDate"><?php echo lang('dashboard_filter_closing_date'); ?></option>
												</select>
											</div>
										</div>
										<div class="form-filter-dateposted" style="display:none;">
											<div class="form-filter-dateposted">
												<select class="form-control form-control-input" name="date_period"
												        data-input-status="false"
												        id="search_filter_dateposted" placeholder="<?php echo lang('public_search_posted_date'); ?>">
													<option value="" selected><?php echo lang('dashboard_filter_date_select'); ?></option>
													<option value="any"><?php echo lang('dashboard_filter_any_time'); ?></option>
													<option value="24h"><?php echo lang('dashboard_filter_last_24'); ?></option>
													<option value="3d"><?php echo lang('dashboard_filter_last_3'); ?></option>
													<option value="7d"><?php echo lang('dashboard_filter_last_7'); ?></option>
													<option value="14d"><?php echo lang('dashboard_filter_last_14'); ?></option>
													<option value="30d"><?php echo lang('dashboard_filter_last_30'); ?></option>
													<option value="customrange"><?php echo lang('dashboard_filter_custom'); ?></option>
												</select>
											</div>
											<div class="task-filter-row task-filter-row-datepicker-fromto" style="display:none;">
												<div class="form-group form-datetime-search-filter-from">
													<div class="form-group">
														<div class="input-group input-group-datetimepicker date" id="job_form_datetime_search_filter_from" data-target-input="nearest">
															<input type="text" class="form-control datetimepicker-input" name="from" data-target="#job_form_datetime_search_filter_from" placeholder="<?php echo lang('public_search_date_from'); ?>" autocomplete="off" />
															<span class="input-group-addon" data-target="#job_form_datetime_search_filter_from" data-toggle="datetimepicker">
				                                                <span class="fa fa-calendar"></span>
				                                            </span>
														</div>
													</div>

												</div>
												<div class="form-group form-datetime-search-filter-to">
													<div class="form-group">
														<div class="input-group input-group-datetimepicker date" id="job_form_datetime_search_filter_to" data-target-input="nearest">
															<input type="text" class="form-control datetimepicker-input" name="to" data-target="#job_form_datetime_search_filter_to" placeholder="<?php echo lang('public_search_date_to'); ?>" autocomplete="off" />
															<span class="input-group-addon" data-target="#job_form_datetime_search_filter_to" data-toggle="datetimepicker">
				                                                <span class="fa fa-calendar"></span>
				                                            </span>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-filter-startdate" style="display:none;">
											<div class="form-filter-startdate">
												<select class="form-control form-control-input" id="search_filter_startdate" name="date_period"
												        placeholder="<?php echo lang('dashboard_filter_start_date'); ?>">
													<option value="" selected><?php echo lang('dashboard_filter_date_select'); ?></option>
													<option value="any"><?php echo lang('dashboard_filter_any_time'); ?></option>
													<option value="24h"><?php echo lang('dashboard_filter_last_24'); ?></option>
													<option value="3d"><?php echo lang('dashboard_filter_last_3'); ?></option>
													<option value="7d"><?php echo lang('dashboard_filter_last_7'); ?></option>
													<option value="14d"><?php echo lang('dashboard_filter_last_14'); ?></option>
													<option value="30d"><?php echo lang('dashboard_filter_last_30'); ?></option>
													<option value="customrange"><?php echo lang('dashboard_filter_custom'); ?></option>
												</select>
											</div>
											<div class="task-filter-row task-filter-row-datepicker-fromto" style="display:none;">
												<div class="form-group form-datetime-search-filter-from">
													<div class="form-group">
														<div class="input-group input-group-datetimepicker date" id="job_form_datetime_search_filter_from_start" data-target-input="nearest">
															<input type="text" class="form-control datetimepicker-input" name="form" data-target="#job_form_datetime_search_filter_from_start" placeholder="<?php echo lang('public_search_date_from'); ?>" autocomplete="off" />
															<span class="input-group-addon" data-target="#job_form_datetime_search_filter_from_start" data-toggle="datetimepicker">
				                                                <span class="fa fa-calendar"></span>
				                                            </span>
														</div>
													</div>

												</div>
												<div class="form-group form-datetime-search-filter-to">
													<div class="form-group">
														<div class="input-group input-group-datetimepicker date" id="job_form_datetime_search_filter_to_start" data-target-input="nearest">
															<input type="text" class="form-control datetimepicker-input" name="to" data-target="#job_form_datetime_search_filter_to_start" placeholder="<?php echo lang('public_search_date_to'); ?>" autocomplete="off" />
															<span class="input-group-addon" data-target="#job_form_datetime_search_filter_to_start" data-toggle="datetimepicker">
				                                                <span class="fa fa-calendar"></span>
				                                            </span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="dropdown dropdown-filter-container filter-dashboard-jobskills">
								<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownJobSkills" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<label class="filter-btn-lbl"><?php echo lang('dashboard_skills'); ?></label>
								</button>
								<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownJobSkills">
									<div class="form-block-task-filter">
										<div class="bstags-skills">
											<input type="text" name="skills" id="job-skills" data-role="tagsinput" class="bootstrap-tagsinput" placeholder="<?php echo lang('dashboard_skills_placeholder'); ?>" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
			<form action="<?php echo option('site_uri') . url_for('workspace/search') ?>" id="hiddenSearchJobForm" method="get">
				<input type="hidden" name="q" disabled />
				<input type="hidden" name="scope" value="job" />
				<input type="hidden" name="type" value="job" />
				<input type="hidden" name="page" disabled value="1" />
				<input type="hidden" name="type" value="job" />
				<input type="hidden" name="category" disabled />
				<input type="hidden" name="location_by" disabled />
				<input type="hidden" name="within" disabled />
				<input type="hidden" name="state" disabled />
				<input type="hidden" name="city" disabled />
				<input type="hidden" name="date_type" disabled />
				<input type="hidden" name="date_period" disabled />
				<input type="hidden" name="from" disabled />
				<input type="hidden" name="to" disabled />
				<input type="hidden" name="skills" disabled />
			</form>
		</div>
		<div class="tab-pane fade show" id="searchTalentTab" role="tabpanel">
			<form action="<?php echo option('site_uri') . url_for('workspace/search') ?>" id="searchTalentForm" class=" search-public-form" novalidate>
				<input type="hidden" name="scope" value="talent" />
				<button type="submit" class="hide"></button>
				<div class="search-dropdown-filter-top-wrapper form-flex">
					<div class="search-dropdown-filter-container search-dropdown-more-filter-container">
						<div class="search-field-group">
							<input type="text" class="form-control form-control-input" name="q"
							       placeholder="<?php echo lang('public_search_talent_by_keywords_placeholder'); ?>" id="inputBox">
							<span class="glass-icon">
                                <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                    <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                </svg>
                            </span>
						</div>
					</div>
					<span class="search-dropdown-filter-btn">
		                <button class="btn-filter-search collapsed" data-toggle="collapse" type="button"
		                        data-target="#advancedFilterTaskCollapse"
		                        aria-expanded="false"
		                        aria-controls="advancedFilterTaskCollapse">
                            <span class="slider-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sliders" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M11.5 2a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM9.05 3a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0V3h9.05zM4.5 7a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM2.05 8a2.5 2.5 0 0 1 4.9 0H16v1H6.95a2.5 2.5 0 0 1-4.9 0H0V8h2.05zm9.45 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm-2.45 1a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0v-1h9.05z"></path>
                                </svg>
                            </span>
                            <i class="fa fa-caret-down"></i>
                        </button>
		            </span>
				</div>
				<div class="search-advanced-filter-wrapper">
					<button class="btn-icon-invert-full btn-advanced-filter" type="button" data-toggle="collapse" data-target="#advancedFilterTaskCollapse" aria-expanded="false" aria-controls="collapseExample">
						<span class="btn-label"><?php echo lang('advanced_filter') ?></span>
						<span class="btn-icon-invert">
								<svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
							</svg>
						</span>
					</button>
					<div class="collapse dropdown-menu-advanced-filter" id="advancedFilterTaskCollapse">
						<div class="search-dropdown-filter-btm-wrapper form-flex">
							<div class="dropdown-filter-container filter-dashboard-savedsearch">
								<span class="bookmark-dropdown-filter-btn dropdown">
					                <button class="btn-savesearch dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
		                                <span class="btn-label"><?php echo lang('dashboard_save_search'); ?></span>
		                            </button>
					                <div class="dropdown-menu dropdown-menu-saved-search">
					                    <div class="modal-advance-filter-container-left">
					                        <ul class="nav nav-pills nav-pills-save-search" id="pills-saveSearch" role="tablist">
					                            <li class="nav-item">
					                                <a class="nav-link active" id="saveTask-tab" data-toggle="tab" data-target="#talentSaveTaskTab" role="tab" aria-controls="saveTaskTab" aria-selected="true"><?php echo lang('dashboard_save_on_demand'); ?></a>
					                            </li>
					                            <li class="nav-item">
					                                <a class="nav-link" id="saveJob-tab" data-toggle="tab" data-target="#talentSaveJobTab" role="tab" aria-controls="saveJobTab" aria-selected="false"><?php echo lang('dashboard_save_full_time'); ?> </a>
					                            </li>
					                            <li class="nav-item">
					                                <a class="nav-link" id="saveTalent-tab" data-toggle="tab" data-target="#talentSaveTalentTab" role="tab" aria-controls="saveTalentTab" aria-selected="false"><?php echo lang('dashboard_save_talent'); ?></a>
					                            </li>
					                        </ul>
					                        <div class="tab-content" id="pills-tabSaveSearch">
					                            <div class="tab-pane fade show active" id="talentSaveTaskTab" role="tabpanel">
					                                <div class="saved-search-content">
			                                            <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'task'; }))): ?>
			                                                <?php foreach($task_search as $name => $data): ?>
					                                            <div class="saved-search-row">
				                                                <a href="<?php echo $data['url'] ?>" class="skills-label-link">
																	<span class="tag label label-saved-task bubble-lbl">
																		<?php echo $name ?>
																	</span>
																</a>
					                                            <span class="tag-remove" data-toggle="modal"
					                                                  data-search-title="<?php echo $name ?>"
					                                                  data-search-id="<?php echo $data['id'] ?>"
					                                                  data-target="#modal_remove_saved"></span>
				                                            </div>
			                                                <?php endforeach ?>
			                                            <?php endif ?>
			                                        </div>
					                            </div>
					                            <div class="tab-pane fade show" id="talentSaveJobTab" role="tabpanel">
					                                <div class="saved-search-content">
			                                            <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'job'; }))): ?>
			                                                <?php foreach($task_search as $name => $data): ?>
					                                            <div class="saved-search-row">
				                                                <a href="<?php echo $data['url'] ?>" class="skills-label-link">
																	<span class="tag label label-saved-job bubble-lbl">
																		<?php echo $name ?>
																	</span>
																</a>
					                                            <span class="tag-remove" data-toggle="modal"
					                                                  data-search-title="<?php echo $name ?>"
					                                                  data-search-id="<?php echo $data['id'] ?>"
					                                                  data-target="#modal_remove_saved"></span>
				                                            </div>
			                                                <?php endforeach ?>
			                                            <?php endif ?>
			                                        </div>
					                            </div>
					                            <div class="tab-pane fade show" id="talentSaveTalentTab" role="tabpanel">
					                                <div class="saved-search-content">
			                                            <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'talent'; }))): ?>
			                                                <?php foreach($task_search as $name => $data): ?>
					                                            <div class="saved-search-row">
				                                                <a href="<?php echo $data['url'] ?>" class="skills-label-link">
																	<span class="tag label label-saved-talent bubble-lbl">
																		<?php echo $name ?>
																	</span>
																</a>
					                                            <span class="tag-remove" data-toggle="modal"
					                                                  data-search-title="<?php echo $name ?>"
					                                                  data-search-id="<?php echo $data['id'] ?>"
					                                                  data-target="#modal_remove_saved"></span>
				                                            </div>
			                                                <?php endforeach ?>
			                                            <?php endif ?>
			                                        </div>
					                            </div>
					                        </div>
					                    </div>
					                </div>
					            </span>
								<span class="bookmark-dropdown-addfilter-btn dropdown">
					                <button class="btn-addsavesearch dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
					                    <span class="public-search-icon addsearch-icon">
					                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
					                            <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
					                        </svg>
					                    </span>
					                </button>
					                <div class="dropdown-menu dropdown-menu-saved-search dropdown-menu-add-saved-search">
					                    <div class="modal-advance-filter-container-left">
					                        <div class="modal-advanced-filter-container">
					                            <div class="keyword-filter-container">
					                                <label><?php echo lang('dashboard_save_search_keyword'); ?></label>
					                                <span class="talent-keywords"><?php echo lang('dashboard_talent_save_search_keyword_default_text') ?></span>
					                            </div>
					                            <label class="modal-search-filter-lbl"><?php echo lang('dashboard_save_search_button'); ?></label>
					                            <div class="input-group-flex row-indfirstname">
					                                <input type="text" id="search_name" name="search_name" class="form-control form-control-input" placeholder="<?php echo lang('dashboard_talent_save_search_name_placeholder'); ?>">
					                            </div>
					                        </div>
					                        <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
					                            <div class="modal-filter-action form-block-filter-flex">
					                                <button type="button" class="btn-icon-full btn-confirm save-search" data-dismiss="modal" data-orientation="next">
					                                    <span class="btn-label"><?php echo lang('dashboard_save_button'); ?></span>
					                                    <span class="btn-icon">
					                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					                                            <polyline points="20 6 9 17 4 12"></polyline>
					                                        </svg>
					                                    </span>
					                                </button>
					                            </div>
					                        </div>
					                    </div>
					                </div>
					            </span>
							</div>
							<div class="dropdown dropdown-filter-container filter-dashboard-talentcategory">
								<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownTalentCategory" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<label class="filter-btn-lbl"><?php echo lang('dashboard_category'); ?></label>
								</button>
								<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTalentCategory">
									<div class="form-block-task-filter">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" name="category" value="0"
											       class="custom-control-input select-all-category" id="talentfiltercat_0">
											<label class="custom-control-label" for="talentfiltercat_0"><?php echo lang('dashboard_category_all'); ?></label>
										</div>
		                                <?php if(isset($task_categories)): ?>
		                                <?php foreach($task_categories as $category): ?>
										<div class="custom-control custom-checkbox categories-input">
											<input type="checkbox" class="custom-control-input" name="category"
											       data-input-status="false"
											       value="<?php echo $category['id'] ?>" id="talentfiltercat_<?php echo $category['id'] ?>">
											<label class="custom-control-label" for="talentfiltercat_<?php echo $category['id'] ?>"><?php echo $category['title'] ?></label>
										</div>
		                                <?php endforeach ?>
		                                <?php endif ?>
									</div>
								</div>
							</div>
							<div class="dropdown dropdown-filter-container filter-dashboard-talentlocation">
								<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownTalentLocation" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<label class="filter-btn-lbl"><?php echo lang('dashboard_location'); ?></label>
								</button>
								<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTalentLocation">
									<div class="form-block-task-filter">
										<div class="form-filter-select-by-location">
											<select class="form-control form-control-input" placeholder="State">
												<option disabled="" selected=""><?php echo lang('dashboard_location_select_by'); ?></option>
												<option value="locbystate"><?php echo lang('dashboard_location_select_by_state'); ?></option>
												<option value="locbynearest"><?php echo lang('dashboard_location_select_by_nearest'); ?></option>
											</select>
										</div>
										<div class="form-filter-state" style="<?php echo !isset($filters['state']) ? 'display: none' : '' ?>">
											<select class="form-control form-control-input state" name="state" id="listing_state"
											        data-target="city"
											        data-input-status="false"
											        placeholder="State">
												<option value="" selected><?php echo lang('dashboard_location_select_by_state'); ?></option>
		                                        <?php foreach($states as $state): ?>
													<option value="<?php echo $state['id'] ?>"><?php echo $state['name'] ?></option>
		                                        <?php endforeach; ?>
											</select>
										</div>
										<div class="form-filter-city" style="<?php echo !isset($filters['city']) ? 'display: none' : '' ?>">
											<select class="form-control form-control-input" name="city" id="listing_city"
											        data-input-status="false"
											        placeholder="City">
												<option selected value=""><?php echo lang('dashboard_location_select_by_town'); ?></option>
		                                        <?php foreach($cities as $city): ?>
													<option value="<?php echo $city['id'] ?>">
		                                                <?php echo $city['name'] ?>
													</option>
		                                        <?php endforeach; ?>
											</select>
										</div>
										<div class="form-filter-within" style="display:none;">
											<select class="form-control form-control-input" name="within" placeholder="within">
												<option value="" selected><?php echo lang('dashboard_location_select_within_word'); ?></option>
												<option value="5km"><?php echo lang('dashboard_location_select_within_word'); ?> 5km</option>
												<option value="10km"><?php echo lang('dashboard_location_select_within_word'); ?> 10km</option>
												<option value="20km"><?php echo lang('dashboard_location_select_within_word'); ?> 20km</option>
												<option value="30km"><?php echo lang('dashboard_location_select_within_word'); ?> 30km</option>
												<option value="50km"><?php echo lang('dashboard_location_select_within_word'); ?> 50km</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="dropdown dropdown-filter-container filter-dashboard-talentskills">
								<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownTalentSkills" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<label class="filter-btn-lbl"><?php echo lang('dashboard_skills'); ?></label>
								</button>
								<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTalentSkills">
									<div class="form-block-task-filter">
										<div class="bstags-skills">
											<input type="text" name="skills" id="talent-skills" data-role="tagsinput" class="bootstrap-tagsinput" placeholder="<?php echo lang('dashboard_skills_placeholder'); ?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="dropdown dropdown-filter-container filter-dashboard-completedtask">
								<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownCompletedTask" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<label class="filter-btn-lbl"><?php echo lang('public_search_talent_completed_task'); ?></label>
								</button>
								<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownCompletedTask">
									<div class="form-block-task-filter">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_1"
											       data-input-status="false"
											       value="all" />
											<label class="custom-control-label" for="filtertaskcompleted_1"><?php echo lang('dashboard_all'); ?></label>
										</div>
										<div class="custom-control custom-checkbox">
											<input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_2"
											       data-input-status="false"
											       value="1-10" />
											<label class="custom-control-label" for="filtertaskcompleted_2">1-10</label>
										</div>
										<div class="custom-control custom-checkbox">
											<input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_3"
											       data-input-status="false"
											       value="10+" />
											<label class="custom-control-label" for="filtertaskcompleted_3">10+</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
			<form action="<?php echo url_for('/workspace/search') ?>" id="hiddenSearchTalentForm" class=" search-public-form" method="get">
				<input type="hidden" name="scope" value="talent" />
				<input type="hidden" name="q" disabled />
				<input type="hidden" name="page" disabled value="1" />
				<input type="hidden" name="category" disabled />
				<input type="hidden" name="location_by" disabled />
				<input type="hidden" name="within" disabled />
				<input type="hidden" name="state" disabled />
				<input type="hidden" name="city" disabled />
				<input type="hidden" name="skills" disabled />
				<input type="hidden" name="completed" disabled />
			</form>
		</div>
	</div>
</div>

<?php content_for('scripts') ?>
<script>
    var keywords = [];
    $('.nav-pills-dashboard-search .nav-item > a').on('click', function(e){
        keywords = [];
    });
    $('input[name="category"]').on('click', function (e) {
        var target = $(e.currentTarget);
        if (target.val() === '0') {
            target.parent().siblings().find('[type="checkbox"]').prop('checked', false);
        } else {
            target.parent().siblings().find('.select-all-category').prop('checked', false);
        }
    });

    $('input[name="completed"]').on('click', function(e){
        $(e.currentTarget).parent().siblings().find('input[name="completed"]').prop('checked', false);
        $(e.currentTarget).prop('checked', true);
    });

    $('#searchTaskForm, #searchJobForm, #searchTalentForm').find('input, select').on('change', function(e){
        var current_form = $(e.currentTarget).closest('form');
        var target_form = $(e.currentTarget).closest('form').next('form');
        target_form.find('input[name="q"]').val(current_form.find('#inputBox').val());
        update_filters($(e.currentTarget), current_form, target_form);
    });

    function update_filters(target, current_form, target_form){
        if(target === undefined){
            var inputs = $(current_form).find('input, select');
        }else{
            var inputName = target.attr('name');
            var inputs = target;
            if(target.attr('type') === 'checkbox') inputs = $(current_form).find('[name="'+inputName+'"]');
        }

        var taskType = '';
        var scope = '';
        var categories = '';
        var value = '';
        var input = $(target_form).find('input[name="'+inputName+'"]');

        if(inputName === 'location_by' && !$(inputs).val().length){
            input = $(target_form).find('input[name="location_by"], input[name="state"], input[name="city"], input[name="within"]');
        }else if(inputName === 'location_by' && $(inputs).val() === 'locbystate'){
            $(target_form).find('input[name="within"]')
                .val('').prop('disabled', true);
        }

        if( inputName === 'date_type' && ! $(inputs).val().length ){
            input = $(target_form).find('input[name="date_type"], input[name="date_period"], input[name="from"], input[name="to"]');
        }

        if(inputName === 'within'){
            $(target_form).find('input[name="state"], input[name="city"]')
                .val('').prop('disabled', true);
        }

        inputs.each(function(idx, item){
            if(
                ((item.type === 'checkbox' && item.checked) ||
                    (item.type === 'text' && item.value !== '') ||
                    (item.type === 'select-one' && item.value !== ''))
            ) {
                if(item.name === 'type' || item.name === 'category' || item.name === 'task_type'){
                    if(item.name === 'category') {
                        if(categories === '')
                            categories += item.value;
                        else
                            categories +=','+item.value;
                    }else if(item.name === 'type') {
                        if(scope === '')
                            scope += item.value;
                        else
                            scope +=','+item.value;
                    }else{
                        if(taskType === '')
                            taskType += item.value;
                        else
                            taskType +=','+item.value;
                    }
                    keywords.push( $(item).next().text() );
                }else {
                    if(item.name === 'category') categories = item.value;
                    else if(item.name === 'type') scope = item.value;
                    else if(item.name === 'task_type') taskType = item.value;
                    else value = item.value;
                    if(item.type === 'checkbox') {
                        keywords.push($(item).next().text());
                    }else {
                        keywords.push($(item).find(':selected').text());
                    }
                }

                if(scope !== '') value = scope;
                else if(categories !== '') value = categories;
                else if(taskType !== '') value = taskType;

                if(value.length) {
                    input.removeAttr('disabled');
                    input.val(value);
                    if(target !== undefined) target.data('inputStatus', true);
                    else $(item).data('inputStatus', true);
                }else{
                    input.val('');
                    input.attr('disabled', 'disabled');
                    input.prop('disabled', true);
                    if(target !== undefined) target.data('inputStatus', false);
                    else $(item).data('inputStatus', false);
                }
            }else{
                if(!value.length){
                    input.val('');
                    input.attr('disabled', 'disabled');
                    input.prop('disabled', true);
                    if(target !== undefined) target.data('inputStatus', false);
                    else $(item).data('inputStatus', false);
                }
            }
        });

        keywords = [...new Set(keywords)];
        current_form.find('.keyword-filter-container span').text( keywords.join(', ') );
    }

    $('#form_datetime_search_filter_from, #form_datetime_search_filter_from_start, #job_form_datetime_search_filter_from, #job_form_datetime_search_filter_from_start, #form_datetime_search_filter_to, #form_datetime_search_filter_to_start, #job_form_datetime_search_filter_to, #job_form_datetime_search_filter_to_start').datetimepicker('destroy');
    $('#form_datetime_search_filter_from, #form_datetime_search_filter_to, #job_form_datetime_search_filter_from, #job_form_datetime_search_filter_to').datetimepicker({
        format: 'DD-MMM-YYYY',
        useCurrent: false,
        allowInputToggle: true,
        maxDate: moment()
    });

    $('#form_datetime_search_filter_from_start, #form_datetime_search_filter_to_start, #job_form_datetime_search_filter_from_start, #job_form_datetime_search_filter_to_start').datetimepicker({
        format: 'DD-MMM-YYYY',
        useCurrent: false,
        allowInputToggle: true
    });

    $('#form_datetime_search_filter_from, #job_form_datetime_search_filter_from').on("change.datetimepicker", function (e) {
        $('#form_datetime_search_filter_from, #job_form_datetime_search_filter_from').datetimepicker("maxDate", moment());
        $('#form_datetime_search_filter_to, #job_form_datetime_search_filter_to').datetimepicker("maxDate", moment());
    });

    $('#form_datetime_search_filter_from, #job_form_datetime_search_filter_from').on("hide.datetimepicker", function (e) {
        var nextDay = moment(e.date).add(1, 'days');
        $('#form_datetime_search_filter_to, #job_form_datetime_search_filter_to').datetimepicker("minDate", nextDay);
    });

    $('#form_datetime_search_filter_to, #job_form_datetime_search_filter_to').on("hide.datetimepicker", function (e) {
        var prevDay = moment(e.date).subtract(1, 'days');
        $('#form_datetime_search_filter_from, #job_form_datetime_search_filter_from').datetimepicker("maxDate", prevDay);
    });

    $('#form_datetime_search_filter_from, #form_datetime_search_filter_from_start, #job_form_datetime_search_filter_from, #job_form_datetime_search_filter_from_start').on('hide.datetimepicker', function(e){
        var target = $(e.currentTarget).find('input');
        var current_form = $(e.currentTarget).closest('form');
        var target_form = current_form.next('form');
        target.data('inputStatus', true);
	    update_filters(target, current_form, target_form);
	    target = null;
    });

    $('#form_datetime_search_filter_to, #form_datetime_search_filter_to_start, #job_form_datetime_search_filter_to, #job_form_datetime_search_filter_to_start').on('hide.datetimepicker', function(e){
        var target = $(e.currentTarget).find('input');
        var current_form = $(e.currentTarget).closest('form');
        var target_form = current_form.next('form');
        target.data('inputStatus', true);
        update_filters(target, current_form, target_form);
        target = null;
    });

    $('#searchTaskForm, #searchJobForm, #searchTalentForm').on('submit', function(e){
        var target_form = $(e.currentTarget).next('form');
        target_form[0].submit();
        e.preventDefault();
        return false;
    });

    $('input[name="search_name"]').keydown(function(e){
        if(e.keyCode === 13){
            $(e.currentTarget).parent().parent().parent().find('.save-search').trigger('click');
            return false;
        }
    });

    $('.save-search').on('click', function(e){
        e.preventDefault();
        var current_form = $(e.currentTarget).closest('form');
        var target_form = current_form.next('form');
        var search_name = current_form.find('input[name="search_name"]');
        var search_form = target_form.clone();
        search_form.append('<input type="text" name="search_name" value="'+search_name.val()+'" />');
        var icon = '', title = '', className = '', message = '';
        if(search_name.val().length){
            $.ajax({
                url: "<?php echo option('site_uri') . url_for('/search/save') ?>",
                method: 'POST',
                data: search_form.serialize(),
            }).done(function(response){
                search_name.val('');
                var search = $.parseJSON(response.search);
                if(typeof (search) === 'object') {
                    var index = search.scope === 'task' ? 0 : (search.scope === 'job' ? 1 : 2);
                    var container = $($('#pills-tab a.nav-link.active').attr('href')).find('.saved-search-content')[index];
                    $(container).append('<div class="saved-search-row"><a href="'+search.url+'" class="skills-label-link">'+
                        '<span class="tag label label-saved-'+search.scope+' bubble-lbl">'+search.name+'</span>'+
                        '</a><span class="tag-remove" data-toggle="modal" data-search-title="'+search.name+'" data-search-id="'+search.id+'" data-target="#modal_remove_saved"></span></div>'
                    );
                }
                $.notify({
                    title: '<b>Success!</b>',
                    message: response.message,
                    icon: "fa fa-check"
                }, {
                    style: 'metro',
                    type: 'success',
                    globalPosition: 'top right',
                    showAnimation: 'slideDown',
                    hideAnimation: 'slideUp',
                    showDuration: 200,
                    hideDuration: 200,
                    autoHide: true,
                    clickToHide: true
                });
            });
        }else{
            $.notify({
                title: '<b>Error!</b>',
                message: "Please type a search name.",
                icon: "fa fa-exclamation"
            }, {
                style: 'metro',
                type: 'danger',
                globalPosition: 'top right',
                showAnimation: 'slideDown',
                hideAnimation: 'slideUp',
                showDuration: 200,
                hideDuration: 200,
                autoHide: true,
                clickToHide: true
            });
        }
        search_form = null;
    });

    $('#modal_remove_saved').on('show.bs.modal', function (event) {
        var link = $(event.relatedTarget);
        var search_id = link.data('searchId');
        var search_title = link.data('searchTitle');
        var modal = $(this);

        modal.find('span.highlighted').text("'"+search_title+"'");
        modal.find('.delete-search.btn-confirm').on('click', function(e){
            $.ajax({
                url: '<?php echo option('site_uri') . url_for('search/delete') ?>',
                method: 'POST',
                data: {'search_name': search_title, 'search_id': search_id}
            }).done(function(response){
                if(response.status === 'success'){
                    link.parent().remove();
                    t('s', response.message);
                }
            });
        });
    });

    var skills = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '<?php echo option('site_uri') . url_for('ajax/skills.json') ?>?all=1'
    });
    skills.clearPrefetchCache();
    skills.initialize();

    $('#task-skills, #job-skills, #talent-skills').tagsinput({
        allowDuplicates: false,
        confirmKeys: [9, 13, 44, 188],
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'skills',
            displayKey: 'name',
            source: skills.ttAdapter()
        }
    });

</script>
<?php end_content_for() ?>
