<li class="dropdown col-right-flex col-notification">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        <span class="user-account-icon notification-icon">
            <svg class="bi bi-bell-fill" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zm.995-14.901a1 1 0 1 0-1.99 0A5.002 5.002 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901z" />
            </svg>
        </span>
		<?php $count = isset($notifications) ? (int) $notifications['on_demand']['new'] + $notifications['full_time']['new'] : 0 ?>
		<?php if( isset($notifications) && $count > 0 ): ?>
		<span class="notifaction-badge"><?php echo $count > 9 ? '9+' : $count ?></span>
		<?php endif ?>
	</a>
	<ul class="dropdown-menu dropdown-navbar">
		<div class="noti-header-title-flex">
			<span class="noti-title"><?php echo lang('notification_main_title') ?></span>
			<button type="button" class="noti-close">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
		</div>
		<ul class="nav nav-tabs nav-tabs-sm nav-header-tabs" id="noti_header_tabs" role="tablist">
			<li class="nav-item nav-od-item">
				<div class="nav-link active gtm-od-tab" id="noti_od_tab" data-toggle="tab" data-target="#noti_od" role="tab" aria-controls="noti_od" aria-selected="true">
					<span class="nav-tabs-lbl"><?php echo lang('notification_on_demand_tab_title') ?></span>
                    <?php if( isset($notifications['on_demand']['new']) && (int)$notifications['on_demand']['new'] > 0 ): ?>
					<span class="notifaction-tabs-badge"><?php echo (int)$notifications['on_demand']['new'] > 9 ? '9+' : (int)$notifications['on_demand']['new'] ?></span>
					<?php endif ?>
				</div>
			</li>
			<li class="nav-item nav-ft-item">
				<div class="nav-link gtm-ft-tab" id="noti_ft_tab" data-toggle="tab" data-target="#noti_ft" role="tab" aria-controls="noti_Ft" aria-selected="true">
					<span class="nav-tabs-lbl"><?php echo lang('notification_full_time_tab_title') ?></span>
                    <?php if( isset($notifications['full_time']['new']) && (int)$notifications['full_time']['new'] > 0 ): ?>
						<span class="notifaction-tabs-badge"><?php echo (int)$notifications['full_time']['new'] > 9 ? '9+' : (int)$notifications['full_time']['new'] ?></span>
                    <?php endif ?>
				</div>
			</li>
		</ul>
		<div class="tab-content" id="noti_tab_contents">
			<div class="tab-pane fade show active" id="noti_od" role="tabpanel" aria-labelledby="noti_od_tab">
				<?php if( empty($notifications['on_demand']['notifications']) ): ?>
				<div class="empty-state-container">
					<div class="empty-img"><img src="<?php echo url_for('/assets/img/mtp-no-data-2.png') ?>" alt=""></div>
					<span class="empty-lbl"><?php echo lang('no_notifications_yet') ?></span>
					<span class="empty-desc"><?php echo lang('stay_tuned_notification_about_your_activity_will_show_up_here') ?>.</span>
				</div>
				<?php else: ?>
				<ul class="noti-list-container">
					<?php foreach ($notifications['on_demand']['notifications'] as $notification): ?>
                    <?php preg_match_all('/%([a-zA-Z0-9,&\-\.\/\s]+)%/i', $notification['message'], $words, PREG_SET_ORDER, 0) ?>
					<li>
						<a href="<?php echo option("site_uri") . url_for('/notification/' . base64_encode($notification['id'] . '|' . urlencode(str_replace("/staging", '', $notification['url'])))) ?>"
						   class="noti-list-link <?php if( is_null($notification['looked_at']) ) echo 'active' ?>" role="button" tabindex="0">
							<div class="header-profile-photo">
								<img src="<?php echo imgCrop($notification['photo'], 35, 35, 'assets/img/default-avatar.png') ?>" alt="Profile Photo">
							</div>
							<div class="noti-contents">
								<span class="noti-body">
	                                <?php if(!empty($words)): ?>
	                                    <?php foreach ($words as $word): ?>
	                                    <?php $notification['message'] = str_replace($word[0], "<span class='noti-name'>{$word[1]}</span>", $notification['message']) ?>
										<?php endforeach ?>
									<?php endif ?>
									<?php if($notification['event'] === 'applied' && $notification['count'] > 1):
										$notification['message'] = str_replace('a new application', $notification['count'] . ' applications', $notification['message']);
									?>
									<?php elseif($notification['event'] === 'question' && $notification['count'] > 1):
										$notification['message'] = str_replace('a new question', $notification['count'] . ' questions', $notification['message']);
									?>
									<?php elseif($notification['event'] === 'answer' && $notification['count'] > 1):
										$notification['message'] = str_replace('an answer', $notification['count'] . ' answers', $notification['message']);
									?>
	                                <?php endif ?>
									<?php echo $notification['message'] ?>
								</span>
								<span class="noti-time"><?php echo \Carbon\Carbon::parse($notification['created_at'])->diffForHumans() ?></span>
							</div>
						</a>
					</li>
					<?php endforeach ?>
				</ul>
				<?php endif ?>
			</div>
			<div class="tab-pane fade" id="noti_ft" role="tabpanel" aria-labelledby="noti_ft">
				<?php if( empty($notifications['full_time']['notifications']) ): ?>
				<div class="empty-state-container">
					<div class="empty-img"><img src="<?php echo url_for('/assets/img/mtp-no-data-2.png') ?>" alt=""></div>
					<span class="empty-lbl"><?php echo lang('no_notifications_yet') ?></span>
					<span class="empty-desc"><?php echo lang('stay_tuned_notification_about_your_activity_will_show_up_here') ?>.</span>
				</div>
				<?php else: ?>
				<ul class="noti-list-container">
                    <?php foreach ($notifications['full_time']['notifications'] as $notification): ?>
                        <?php preg_match_all('/%([a-zA-Z0-9,\.\s]+)%/i', $notification['message'], $words, PREG_SET_ORDER, 0) ?>
						<li>
							<a href="<?php echo option("site_uri") . url_for('/notification/' . base64_encode($notification['id'] . '|' . str_replace("/staging", '', $notification['url']))) ?>"
							   class="noti-list-link <?php if( is_null($notification['looked_at']) ) echo 'active' ?>" role="button" tabindex="0">
								<div class="header-profile-photo">
									<img src="<?php echo imgCrop($notification['photo'], 35, 35) ?>" alt="Profile Photo">
								</div>
								<div class="noti-contents">
									<span class="noti-body">
										<?php if(!empty($words)): ?>
                                            <?php foreach ($words as $word): ?>
                                                <?php $notification['message'] = str_replace($word[0], "<span class='noti-name'>{$word[1]}</span>", $notification['message']) ?>
                                            <?php endforeach ?>
                                        <?php endif ?>

                                        <?php if($notification['event'] === 'applied' && $notification['count'] > 1):
                                            $notification['message'] = str_replace('a new application', $notification['count'] . ' applications', $notification['message']);
                                        endif ?>

										<?php echo $notification['message'] ?>.
									</span>
									<span class="noti-time"><?php echo \Carbon\Carbon::parse($notification['created_at'])->diffForHumans() ?></span>
								</div>
							</a>
						</li>
                    <?php endforeach ?>
				</ul>
				<?php endif ?>
			</div>
		</div>
	</ul>
</li>

<script>
	window.addEventListener('load', function(){
        $('.col-notification').on('hidden.bs.dropdown', function(e){
	        /*$.ajax({
		        url: '<?php echo option("site_uri") . url_for("workspace/notifications") ?>',
		        method: 'POST'
	        }).done(function(response){
	           if( response.updated === 'success' ){
	               $('.notifaction-badge, .notifaction-tabs-badge').text('').fadeOut();
	           }
	        });*/
	    })
	})
</script>