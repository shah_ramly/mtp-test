<?php
$counter = 1;

if($list){
    foreach($list as $task){
        $todayminusthree = date('Y-m-d', strtotime('-3 days'));
        // Check if task start posted is after 3 days
        $new = ($task['created_at'] <= $todayminusthree ? '':'<span class="new-highlight">New</span>');

        switch ($counter) {
            case 1:
                $color = "gold";
                break;
            case 2:
                $color = "green";
                break;
            case 3:
                $color = "red";
                break;
            case 4:
                $color = "orange";
                break;
            case 5:
                $color = "maroon";
                break;
            case 6:
                $color = "purple";
                break;
            case 7:
                $color = "cyan";
                break;
            default:
                $color = "blue";
                $counter = 1;
        }

        $counter += 1;
        $amount = explode(".", $task['budget']);
        $state = explode(",", $task['state_country']);

        ?>
        <li class="<?php echo $color; ?>-card">
            <a href="<?php echo url_for('/sign_up') . '?redirect=' . $cms->site_host . url_for("/dashboard") . "?task={$task['slug']}" ?>" class="gtm-wos-tiles">
                <div class="recentpost-card">
                    <div class="recentpost-info">
                        <div class="recentpost-cat"><?php echo $task['category_name']; ?></div>
                        <div class="recentpost-title"><?php echo $task['title']; ?></div>
                        <div class="recentpost-loc"><?php echo $state[0]; ?></div>
                        <div class="recentpost-id"><?php echo $task['number']; ?></div>
                        <?php echo $new; ?>
                    </div>
                    <div class="amount-val recentpost-amount">
                        <span class="amount-prefix">RM</span>
                        <span class="amount-val"><?php echo number_format($amount[0], 0); ?></span>
                        <span class="amount-suffix"></span>
                    </div>
                </div>
            </a>
        </li>

        <?php
    }
}
?>