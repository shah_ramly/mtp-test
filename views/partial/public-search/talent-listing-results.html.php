<?php if(!empty($listing)): ?>
    <?php foreach($listing as $talent): ?>
		<div class="col-talent-row-container">
			<div class="talent-row-col-top-flex">
				<div class="talent-row-col-left">
					<div class="talent-personal-info-container">
						<div class="talent-avatar-container">
							<div class="profile-avatar">
								<a href="<?php echo url_for('/sign_up') . "?redirect={$_SESSION[WEBSITE_PREFIX.'REDIRECT']}" ?>" class="talent-avatar-link">
									<img class="avatar-img" src="<?php echo $talent['photo'] ?>" alt="">
								</a>
							</div>
						</div>
						<div class="talent-personal-info">
							<a href="<?php echo url_for("sign_up") ?>" class="talent-name-link">
								<h5 class="profile-name"><?php echo hash_name(strtolower($talent['firstname'] . ' ' . $talent['lastname'])); ?></h5>
							</a>
							<div class="talent-details-profile-location">
                                <span class="talent-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                    </svg></span>
								<span class="talent-details-profile-location-val">
                                    <?php echo ($talent['state'] && $talent['country']) ? "{$talent['state']}, {$talent['country']}" : 'N/A' ?>
                                </span>
							</div>
							<div class="task-row-rating-flex">
								<div class="task-row-rating-star">
                                    <?php $starred = (int)floor($talent['rating']['overall']/2); $rest = 5 - $starred; ?>
                                    <?php if($starred > 0): ?>
                                        <?php foreach (range(1, $starred) as $star): ?>
											<span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <?php if($rest > 0): ?>
                                        <?php foreach (range(1, $rest) as $star1): ?>
											<span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
								</div>
								<div class="task-row-rating-val">
                                    <span class="task-row-average-rating-val">
                                        <?php echo number_format((float)$talent['rating']['overall'], 1); ?>
                                    </span>
									<span class="task-row-divider">/</span>
									<span class="task-row-total-rating-val">
                                        <?php echo $talent['rating']['count'] . " review" . plural($talent['rating']['count']); ?>
                                    </span>
								</div>
							</div>
						</div>
					</div>
					<div class="talent-about-container">
                        <?php echo strlen($talent['about']) > 235 ? (substr($talent['about'], 0, 235) . ' ...') : $talent['about']; ?>
					</div>
					<div class="talent-row-skills">
                        <?php if( !empty($talent['skills']) ): ?>
							<div class="task-details-deadline-block">
                                <?php foreach ($talent['skills_list'] as $skill): ?>
									<a href="<?php echo url_for('search') . "?scope=talent&skills=" . $skill['id'] ?>" class="skills-label-link">
										<span class="tag label label-info bubble-lbl"><?php echo $skill['name'] ?></span>
									</a>
                                <?php endforeach ?>
							</div>
                        <?php endif ?>
					</div>
				</div>
				<div class="talent-row-col-right">
					<div class="talent-row-actions">
						<button type="button" class="btn-icon-full btn-view-profile" onclick="location.href='<?php echo url_for('/sign_up') . "?redirect={$_SESSION[WEBSITE_PREFIX.'REDIRECT']}" ?>';">
							<span class="btn-label"><?php echo lang('public_search_talent_view_profile'); ?></span>
							<span class="btn-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                    <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                </svg>
                            </span>
						</button>
					</div>
					<div class="talent-activity-tracking-details">
						<div class="talent-total-completed-task-hour">
							<div class="talent-extra-info talent-total-completed-task">
								<span class="talent-profile-label"><?php echo lang('public_search_talent_completed_task'); ?>:</span>
								<span class="talent-profile-val"><?php echo $talent['info']['completed'] ?></span>
							</div>
							<div class="talent-extra-info  talent-total-completed-hour">
								<span class="talent-profile-label"><?php echo lang('public_search_talent_total_hours_completed'); ?>:</span>
								<span class="talent-profile-val"><?php echo (int)$talent['info']['hours_completed'] ?></span>
							</div>
						</div>
						<div class="talent-extra-info talent-history">
							<div class="talent-last-login">
								<span class="talent-profile-label"><?php echo lang('public_search_talent_last_login'); ?>:</span>
								<span class="talent-profile-val"><?php echo $talent['info']['last_login'] ?></span>
							</div>
							<div class="talent-extra-info talent-member-since">
								<span class="talent-profile-label"><?php echo lang('public_search_talent_member_since'); ?>:</span>
								<span class="talent-profile-val"><?php echo $talent['member_since'] ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    <?php endforeach ?>
<?php else: ?>
	<div class="col-task-row-container">
		<div class="task-row-col-top-flex job">
			<div class="empty-state-container">
				<div class="empty-img"><img src="<?php echo url_for('assets/img/mtp-no-data-2.png') ?>" alt=""></div>
				<span class="empty-lbl">No result to display</span>
			</div>
		</div>
	</div>
<?php endif ?>
<?php
$_SESSION[WEBSITE_PREFIX.'REDIRECT'] = option('site_uri') . url_for('workspace') . $_SERVER['REQUEST_URI'];
?>
