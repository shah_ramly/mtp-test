<?php if(!empty($listing)): ?>
	<?php $counter = 1; ?>
    <?php foreach($listing as $list): ?>
		<?php 
			switch ($counter) {
				case 1:
					$color = "gold";
					break;
				case 2:
					$color = "green";
					break;
				case 3:
					$color = "red";
					break;
				case 4:
					$color = "orange";
					break;
				case 5:
					$color = "maroon";
					break;
				case 6:
					$color = "purple";
					break;
				case 7:
					$color = "cyan";
					break;
				default:
					$color = "blue";
					$counter = 1;
			}
	
			$counter += 1;
        ?>
        <?php $type = in_array($list['type'], ['internal_job', 'external_job']) ? 'job' : 'task'; ?>
        <?php $external = in_array($list['type'], ['external_job', 'external_task']) ? '?s=external' : '' ?>
		<?php 
			$todayminusthree = date('Y-m-d', strtotime('-3 days'));
			$new = (!empty($list['created_at']) && $list['created_at'] <= $todayminusthree ? '':'<span class="new-highlight">New</span>'); 
		?>
		<div class="<?php echo $color; ?>-card col-task-card-container <?php echo $type !== 'task' ? 'col-job-card-container' : '' ?>">
			<?php if(isset($_SESSION['WEB_USER_ID'])): ?>
			<a href="<?php echo option('site_uri') . url_for("/dashboard") . "?{$type}={$list['slug']}" ?>" class="task-row-taskname">
			<?php else: ?>
			<a href="<?php echo option('site_uri') . url_for('/sign_up') . "?redirect=" . url_for("/dashboard") . "?{$type}={$list['slug']}" ?>" class="task-row-taskname">
			<?php endif ?>
				<div class="recentpost-card">
					<div class="recentpost-info">
						<div class="recentpost-cat"><?php echo $list['category'] === 'others'  ? lang('others') : $list['category_name']; ?></div>
						<div class="recentpost-title"><?php echo $list['title'] ?></div>
						<div class="task-row-col-left">
						<?php if( $type === 'task' ): ?>
							<?php $state = explode(",", $list['state_country']); ?>
							<div class="recentpost-loc"><?php echo $list['location'] === 'remotely' || $list['location'] === '' ? lang('public_search_working_remotely') : "{$state[0]}"; ?></div>
							<div class="recentpost-id"><?php echo strtoupper($list['number']) ?></div>
							<?php echo $new; ?>
						
						<?php else: ?>
							<div class="recentpost-loc"><?php echo "{$list['job_state']}"; ?></div>
							<div class="recentpost-id"><?php echo strtoupper($list['number']); ?></div>
							<?php echo $new; ?>
						<?php endif ?>
					</div>
					</div>

					<div class="amount-val recentpost-amount">
					<?php if($type === 'task'): ?>
						<?php $budget = explode('.', $list['budget']) ?>
						<div class="task-row-taskamount">
							<span class="amount-prefix">RM</span>
							<span class="amount-val"><?php echo number_format($budget[0]) ?></span>
						</div>
					<?php elseif( $list['type'] === 'internal_job' ): ?>
						<?php $salary = explode('.', $list['salary_range_max']) ?>
						<div class="task-row-taskamount">
							<span class="amount-prefix">RM</span>
							<span class="amount-val"><?php echo $salary[0] ?></span>
						</div>
					<?php else: ?>
						<?php $salary = $list['salary_range_max'] != 0 ? explode('.', $list['salary_range_max']) :
							($list['salary_range_min'] != 0 ? explode('.', $list['salary_range_min']) : 0) ?>

						<?php if(is_array($salary)): ?>
							<div class="task-row-taskamount">
								<span class="amount-prefix">RM</span>
								<span class="amount-val"><?php echo $salary[0] ?></span>
							</div>
						<?php endif ?>
					<?php endif ?>
					</div>
				</div>
			</a>
		</div>
    <?php endforeach ?>
<?php else: ?>
	<div class="col-task-row-container">
		<div class="task-row-col-top-flex job">
			<div class="empty-state-container">
				<div class="empty-img"><img src="<?php echo url_for('assets/img/mtp-no-data-2.png') ?>" alt=""></div>
				<span class="empty-lbl"><?php echo lang('public_search_no_result'); ?></span>
			</div>
		</div>
	</div>
<?php endif ?>