<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <title>
        Workspace | Make Time Pay
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
   
    <link href="assets/css/glyphicons.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- CSS Files -->
    <!-- CSS only -->
      
    
    <link href="assets/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />

</head>

<body class="white-content">
    <div class="wrapper">


        <div class="search-components-container">
            <div class="form-group form-group-search has-icon">
                <span class="search-form-icon">
                    <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                        <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                    </svg>
                </span>
                <input type="text" class="form-control search-input" placeholder="Search">
            </div>
        </div>


        <form id="radioBtnForm">
            <div class="radiobtn">
                <input type="radio" id="huey" name="drone" value="creditdebit" checked />
                <label for="huey">
                    <span class="radio-label-header">Credit or Debit Card</span>
                    <span class="radio-label-desc">Use a credit or debit card to pay with automatic payments enabled.</span>
                </label>
            </div>

            <div class="radiobtn">
                <input type="radio" id="dewey" name="drone" value="paypal" />
                <label for="dewey">
                    <span class="radio-label-header">Paypal</span>
                    <span class="radio-label-desc">Prepay your bill by making incremental Paypal payments.</span>
                </label>
            </div>

            <div class="radiobtn">
                <input type="radio" id="louie" name="drone" value="debit" />
                <label for="louie">
                    <span class="radio-label-header">Debit</span>
                    <span class="radio-label-desc">Direct debits are particularly suitable for periodically recurring payment obligations.</span>
                </label>
            </div>
            <div class="radiobtn">
                <input type="radio" id="invoice" name="drone" value="invoice" />
                <label for="invoice">
                    <span class="radio-label-header">Invoice</span>
                    <span class="radio-label-desc">Prepay your bill by making incremental Paypal payments.</span>
                </label>
            </div>

        </form>


        
        <form id="payment-form" class="ui payment form attached segment input-form-container">
            <div class="form-row-group">
                <label for="creditcard" class="input-lbl">
                    <span class="input-label-txt">Card Number</span>
                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="Your credit card number">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                        </svg>
                    </span>
                </label>

                <div class="form-group has-icon field">
                    <div class="ui icon input">
                        <i class="cc-default cc-icon"></i>
                        <input type="tel" name="" class="form-control form-control-input" id="cc-number" placeholder="•••• •••• •••• ••••" data-payment='cc-number'/>
                    </div>
                    <span class="ccard-form-icon">
                        <svg viewBox="0 0 16 16" class="bi bi-credit-card" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v1h14V4a1 1 0 0 0-1-1H2zm13 4H1v5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V7z"/>
                          <path d="M2 10a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-1z"/>
                        </svg>
                    </span>
                </div>
            </div>
            
            
            <div class="form-row-group">
                <label for="creditcard" class="input-lbl">
                    <span class="input-label-txt">Name</span>
                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="Your friendly name">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                        </svg>
                    </span>
                </label>

                <div class="form-group field">
                    <input type="text" name="" class="form-control form-control-input" id="name" placeholder="Please enter your name" value="John Doe"/>
                    <span class="input-validation success">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"><polyline points="20 6 9 17 4 12"></polyline></svg>
                    </span>
                </div>
            </div>
            
            <div class="form-row-group">
                <label for="creditcard" class="input-lbl">
                    <span class="input-label-txt">Email Address</span>
                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="Your email address">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                        </svg>
                    </span>
                </label>

                <div class="form-group field error">
                    <input type="text" name="" class="form-control form-control-input" id="name" placeholder="Please enter your name" value="johndoe.com"/>
                    <span class="input-validation error">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </span>
                </div>
            </div>
        </form>
        
        
        
        <div id="components-container" class="components-container">
           
            <div id="components-checkbox">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="defaultIndeterminate2" checked>
                    <label class="custom-control-label" for="defaultIndeterminate2">Default indeterminate</label>
                </div>
                <!-- Default checked -->
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="defaultChecked2" checked>
                    <label class="custom-control-label" for="defaultChecked2">Default checked</label>
                </div>
            </div>
            
            <div id="components-radio">
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" id="defaultUnchecked" name="defaultExampleRadios">
                    <label class="custom-control-label" for="defaultUnchecked">Default unchecked</label>
                </div>

                <!-- Default checked -->
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" id="defaultChecked" name="defaultExampleRadios" checked>
                    <label class="custom-control-label" for="defaultChecked">Default checked</label>
                </div>
            </div>
           
            <div id="components-range">
                <!--<form>
                    <div class="form-group all">
                        <input type="range" class="form-control-range" id="formControlRange" value="75">
                    </div>
                </form>-->
                
                <div class="custom-slider-widget">
                    <div class="slider-wrapper">
                        <input type="range" min="0" max="100" value="50" id="widget1" step="1">
                        <div class="custom-track" aria-hidden="true">
                            <span class="custom-fill"></span>
                            <span class="custom-thumb"></span>
                        </div>
                        <output for="widget1" aria-hidden="true">50</output>%
                    </div>
                </div>
            </div>
            
            
            <!-- Toggler -->
            <div id="components-toggler">
                <label class="switch">
                    <input type="checkbox" checked>
                    <span class="slider round"></span>
                </label>
            </div>
            
            
            <!-- Progress Bar -->
            <div id="components-progress">
                <div class="progress">
                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 60%"></div>
                </div>
                <div class="progress-number">60%</div>
            </div>
            
            
            <!--<div class="form-group">
                <div class="input-group date" id="form_datetime_hour" data-target-input="nearest">
                    <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_hour"/>
                    <div class="input-group-append" data-target="#form_datetime_hour" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>-->
            
            <div class="form-group">
                <div class="input-group input-group-datetimepicker date" id="form_datetime_hour" data-target-input="nearest">
                    <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_hour" />
                    <span class="input-group-addon" data-target="#form_datetime_hour" data-toggle="datetimepicker">
                        <span class="fa fa-calendar"></span>
                    </span>
                </div>
            </div>
            
        </div>
        
        
        <div id="table-listing-container" class="table-listing-container">
            <div id="table-filter-container" class="table-filter-container">
                <div class="dropdown dropdown-filter-container">
                    <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="drop-val">Mutual Funds</span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
                <div class="dropdown dropdown-filter-container">
                    <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <label>Risk Category</label> <span class="drop-val">All</span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
                <div class="dropdown dropdown-filter-container">
                    <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <label>AUM</label> <span class="drop-val">All</span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
                <div class="dropdown dropdown-filter-container">
                    <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <label>Global Asset Class</label> <span class="drop-val">All</span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
                <div class="dropdown dropdown-filter-container">
                    <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <label>Distribution Channel</label> <span class="drop-val">All</span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
                <div class="dropdown dropdown-filter-container">
                    <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="drop-val">Mawer Investment Management Ltd.</span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
                
            </div>
           
            <table class="table">
               <thead>
                <tr>
                    <th class="active">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input select-all" id="defaultCheckedtbl">
                            <label class="custom-control-label" for="defaultCheckedtbl"></label>
                        </div>
                    </th>
                    <th class="success">Fund Name</th>
                    <th class="warning">Type of Fund</th>
                    <th class="danger">YTD</th>
                    <th class="info">5 YR</th>
                    <th class="info">Beta</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="active">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="select-item custom-control-input checkbox" name="select-item" id="defaultCheckedtblRow1" />
                            <label class="custom-control-label" for="defaultCheckedtblRow1"></label>
                        </div>
                    </td>
                    <td class="info">Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
                    <td class="info"><span class="tbl-lbl">Mutual Fund</span></td>
                    <td class="success">1.23%</td>
                    <td class="warning">6.95%</td>
                    <td class="danger">0.9001</td>
                </tr>
                <tr>
                    <td class="active">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="select-item custom-control-input checkbox" name="select-item" id="defaultCheckedtblRow2" />
                            <label class="custom-control-label" for="defaultCheckedtblRow2"></label>
                        </div>
                    </td>
                    <td class="info">Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
                    <td class="info"><span class="tbl-lbl">Mutual Fund</span></td>
                    <td class="success">1.23%</td>
                    <td class="warning">6.95%</td>
                    <td class="danger">0.9001</td>
                </tr>
                <tr>
                    <td class="active">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="select-item custom-control-input checkbox" name="select-item" id="defaultCheckedtblRow3" />
                            <label class="custom-control-label" for="defaultCheckedtblRow3"></label>
                        </div>
                    </td>
                    <td class="info">Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
                    <td class="info"><span class="tbl-lbl">Mutual Fund</span></td>
                    <td class="success">1.23%</td>
                    <td class="warning">6.95%</td>
                    <td class="danger">0.9001</td>
                <tr>
                    <td class="active">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="select-item custom-control-input checkbox" name="select-item" id="defaultCheckedtblRow4" />
                            <label class="custom-control-label" for="defaultCheckedtblRow4"></label>
                        </div>
                    </td>
                    <td class="info">Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
                    <td class="info"><span class="tbl-lbl">Mutual Fund</span></td>
                    <td class="success">1.23%</td>
                    <td class="warning">6.95%</td>
                    <td class="danger">0.9001</td>
                </tr>
                </tbody>
            </table>
            
            
        </div>
        

<div class="bs-example">
    <div class="btn-group btn-group-toggle" data-toggle="buttons">
        <label class="btn btn-primary active">
            <input type="checkbox" name="options" autocomplete="off" checked> Option A
        </label>
        <label class="btn btn-primary">
            <input type="checkbox" name="options" autocomplete="off"> Option B
        </label>
        <label class="btn btn-primary active">
            <input type="checkbox" name="options" autocomplete="off" checked> Option C
        </label>
    </div>
</div>



    </div>
    <!--  extra Core JS Files   -->
   <script src="assets/js/plugins/jquery.payment.js"></script>
    <script src="assets/js/creditcard-validation.js"></script>
    
    
    
    
     <!-- Bootstrap Datepicker Function -->
    <script>
        
        $(function () {
            $('#form_datetime_hour').datetimepicker({
                
            });
        });
        
    </script>
    
    
    
    
    <!----- MTP025 checkbox indeterminate ----->
    <script>
        
        $('#defaultIndeterminate2').prop('indeterminate', true);
        
        
        // range slider output
        const slider = document.querySelector('.slider-wrapper'),
              sliderInput = slider.querySelector('input'),
              sliderOutput = slider.querySelector('output'),
              sliderThumb = slider.querySelector('.custom-thumb'),
              sliderFill = slider.querySelector('.custom-fill');

        function initSlider(min, max, startValue) {
          sliderInput.setAttribute('min', min);
          sliderInput.setAttribute('max', max);
          sliderInput.value = startValue;

          const onSliderChange = function(event) {
            let value = event.target.value;
            sliderOutput.innerHTML = value;
            sliderThumb.style.left = (value/max * 100) + '%';
            sliderFill.style.width = (value/max * 100) + '%';
          }

          sliderInput.addEventListener('input', onSliderChange);
          sliderInput.addEventListener('change', onSliderChange);

          // set slider to initial value
          const initialInput = new Event('input', {
            'target': { 'value': startValue }
          });
          sliderInput.dispatchEvent(initialInput);
        }

        initSlider(0, 100, 50);
        
    </script>
    
    
    
    
    <!----- MTP026 checkbox table highlights ----->
    <script>
    
        $(document).ready(function () {
            $('.record_table tr').click(function (event) {
                if (event.target.type !== 'checkbox') {
                    $(':checkbox', this).trigger('click');
                }
            });

            $("tbody input[type='checkbox']").change(function (e) {
                if ($(this).is(":checked")) {
                    $(this).closest('tbody tr').addClass("highlight_row");
                } else {
                    $(this).closest('tbody tr').removeClass("highlight_row");
                }
            });
            
            
            $("input.select-all").click(function () {
                var checked = this.checked;
                $("input.select-item").each(function (index,item) {
                    item.checked = checked;
                });
            });

            //button select all or cancel
            $("#select-all").click(function () {
                var all = $("input.select-all")[0];
                all.checked = !all.checked
                var checked = all.checked;
                $("input.select-item").each(function (index,item) {
                    item.checked = checked;
                });
            });

            //button select invert
            $("#select-invert").click(function () {
                $("input.select-item").each(function (index,item) {
                    item.checked = !item.checked;
                });
                checkSelected();
            });

            //button get selected info
            $("#selected").click(function () {
                var items=[];
                $("input.select-item:checked:checked").each(function (index,item) {
                    items[index] = item.value;
                });
                if (items.length < 1) {
                    alert("no selected items!!!");
                }else {
                    var values = items.join(',');
                    console.log(values);
                    var html = $("<div></div>");
                    html.html("selected:"+values);
                    html.appendTo("body");
                }
            });

            //column checkbox select all or cancel
            $("input.select-all").click(function () {
                var checked = this.checked;
                $("input.select-item").each(function (index,item) {
                    item.checked = checked;
                });
                if ($('input.select-item').is(":checked")) {
                    $('tbody tr').addClass("highlight_row");
                } else {
                    $('tbody tr').removeClass("highlight_row");
                }
            });

            //check selected items
            $("input.select-item").click(function () {
                var checked = this.checked;
                console.log(checked);
                checkSelected();
            });

            //check is all selected
            function checkSelected() {
                var all = $("input.select-all")[0];
                var total = $("input.select-item").length;
                var len = $("input.select-item:checked:checked").length;
                console.log("total:"+total);
                console.log("len:"+len);
                all.checked = len===total;
            }
            
            
            
            
            
            
            
            
            
            
        });
    
    </script>
    
    
    
    
    
    
    

    <script>
        const $dropdown = $(".sidebar .dropdown");
        const $dropdownToggle = $(".sidebar .dropdown-toggle");
        const $dropdownMenu = $(".sidebar .dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function() {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                    function() {
                        const $this = $(this);
                        $this.addClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "true");
                        $this.find($dropdownMenu).addClass(showClass);
                    },
                    function() {
                        const $this = $(this);
                        $this.removeClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "false");
                        $this.find($dropdownMenu).removeClass(showClass);
                    }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });

    </script>

    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })

        var widthOverflow = $('.tbl-header-calendar-gantt-body-overflow')[0].scrollWidth;
        var widthColParent = $('.col-right-card-flex').width();
        var maxDrag = 0;
        maxDrag = widthOverflow - widthColParent;
        console.log(maxDrag);
        console.log(widthColParent);
        $('.tbl-body-calendar-gantt').css('width', widthOverflow);





        var splitobj = Split(["#colLeftCard", "#colRightCard"], {
            elementStyle: function(dimension, size, gutterSize) {
                $(window).trigger('resize'); // Optional
                if (size <= 70) {
                    return {
                        'flex-basis': 'calc(' + size + '% - ' + gutterSize + 'px)'
                    }
                }

            },
            gutterStyle: function(dimension, gutterSize) {
                return {
                    'flex-basis': gutterSize + 'px'
                }
            },
            sizes: [50, 50],
            minSize: [40, 40],
            gutterSize: 5,
            cursor: 'col-resize',
            expandToMin: true,
        });

    </script>

    <script>
        $(document).ready(function() {
            $().ready(function() {
                $sidebar = $('.sidebar');
                $navbar = $('.navbar');
                $main_panel = $('.main-panel');

                $full_page = $('.full-page');

                $sidebar_responsive = $('body > .navbar-collapse');
                sidebar_mini_active = true;
                white_color = false;

                window_width = $(window).width();

                fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();



                $('.fixed-plugin a').click(function(event) {
                    if ($(this).hasClass('switch-trigger')) {
                        if (event.stopPropagation) {
                            event.stopPropagation();
                        } else if (window.event) {
                            window.event.cancelBubble = true;
                        }
                    }
                });

                $('.fixed-plugin .background-color span').click(function() {
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');

                    var new_color = $(this).data('color');

                    if ($sidebar.length != 0) {
                        $sidebar.attr('data', new_color);
                    }

                    if ($main_panel.length != 0) {
                        $main_panel.attr('data', new_color);
                    }

                    if ($full_page.length != 0) {
                        $full_page.attr('filter-color', new_color);
                    }

                    if ($sidebar_responsive.length != 0) {
                        $sidebar_responsive.attr('data', new_color);
                    }
                });

                $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function() {
                    var $btn = $(this);

                    if (sidebar_mini_active == true) {
                        $('body').removeClass('sidebar-mini');
                        sidebar_mini_active = false;
                        blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                    } else {
                        $('body').addClass('sidebar-mini');
                        sidebar_mini_active = true;
                        blackDashboard.showSidebarMessage('Sidebar mini activated...');
                    }

                    // we simulate the window Resize so the charts will get updated in realtime.
                    var simulateWindowResize = setInterval(function() {
                        window.dispatchEvent(new Event('resize'));
                    }, 180);

                    // we stop the simulation of Window Resize after the animations are completed
                    setTimeout(function() {
                        clearInterval(simulateWindowResize);
                    }, 1000);
                });

                $('.switch-change-color input').on("switchChange.bootstrapSwitch", function() {
                    var $btn = $(this);

                    if (white_color == true) {

                        $('body').addClass('change-background');
                        setTimeout(function() {
                            $('body').removeClass('change-background');
                            $('body').removeClass('white-content');
                        }, 900);
                        white_color = false;
                    } else {

                        $('body').addClass('change-background');
                        setTimeout(function() {
                            $('body').removeClass('change-background');
                            $('body').addClass('white-content');
                        }, 900);

                        white_color = true;
                    }


                });

                $('.light-badge').click(function() {
                    $('body').addClass('white-content');
                });

                $('.dark-badge').click(function() {
                    $('body').removeClass('white-content');
                });
            });
        });

    </script>
    <script>
        $(document).ready(function() {
            // Javascript method's body can be found in assets/js/demos.js
            demo.initDashboardPageCharts();

        });

    </script>
    <script src="assets/js/bootstrap3-typeahead.js"></script>
    <script>
    var $input = $(".search-input");
    $input.typeahead({
      source: [
        {id: "someId1", name: "jQueryScript.Net"},
        {id: "someId2", name: "Angular Components"},
        {id: "someId3", name: "React Components"},
        {id: "someId4", name: "Vue.js Components"},
        {id: "someId5", name: "Native JavaScript"},
        {id: "someId6", name: "jQuery Plugins"},
        {id: "someId7", name: "Vanilla JavaScript"}
      ],
      autoSelect: false
    });
    $input.change(function() {
      var current = $input.typeahead("getActive");
      if (current) {
        // Some item from your model is active!
        if (current.name == $input.val()) {
          // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
        } else {
          // This means it is only a partial match, you can either add a new item
          // or take the active if you don't want new items
        }
      } else {
        // Nothing is active so it is a new value (or maybe empty value)
      }
    });
    </script>










</body>

</html>
