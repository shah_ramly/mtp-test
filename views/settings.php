<!DOCTYPE html>
<html lang="<?php echo !isset($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) || ($_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] == 'EN') ? 'en':'ms' ?>">

<body class="white-content od-mode">
	<div class="card-body-companyprofile card-body-settings">
		<ul class="nav nav-pills nav-pills-settings">
			<li class="nav-item">
				<a class="nav-link active"><?php echo lang('pref_title'); ?></a>
			</li>
		</ul>
		<div class="row">
			<div class="col-xl-12">
				<div class="row">
					<div class="col-xl-12 col-report-dashboard-top">
						<div class="col-report-dashboard-top-wrapper">
							<div class="row">
								<div class="col-xl-12">
									<div class="form-individualprofile">
										<form class="form-settings" id="redirectForm">
                                            <div class="form-collapse-row row-collapse-bank-info">
                                                <div class="collapse-title-lbl">
                                                    <span class="title-collapse"><?php echo lang('pref_bank'); ?></span>
                                                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo lang('pref_bank_tooltips'); ?>">
                                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div class="form-flex form-flex-banknotes">
                                                    <div class="input-group-flex row-bankacc">
                                                        <select class="form-control form-control-input" name="preference[bank]">
                                                            <option disabled selected><?php echo lang('pref_select_bank'); ?></option>
                                                            <option value="PHBMMYKL" <?php echo $user->info['preference']['bank'] == "PHBMMYKL" ? ' selected' : ''; ?>>Affin Bank</option>
                                                            <option value="MFBBMYKL" <?php echo $user->info['preference']['bank'] == "MFBBMYKL" ? " selected" : ''; ?>>Alliance Bank</option>
                                                            <option value="ARBKMYKL" <?php echo $user->info['preference']['bank'] == "ARBKMYKL" ? " selected" : ''; ?>>AmBank</option>
                                                            <option value="BIMBMYKL" <?php echo $user->info['preference']['bank'] == "BIMBMYKL" ? " selected" : ''; ?>>Bank Islam</option>
                                                            <option value="BKRMMYKL" <?php echo $user->info['preference']['bank'] == "BKRMMYKL" ? " selected" : ''; ?>>Bank Kerjasama Rakyat Malaysia</option>
                                                            <option value="BMMBMYKL" <?php echo $user->info['preference']['bank'] == "BMMBMYKL" ? " selected" : ''; ?>>Bank Muamalat</option>
                                                            <option value="BSNAMYK1" <?php echo $user->info['preference']['bank'] == "BSNAMYK1" ? " selected" : ''; ?>>Bank Simpanan Nasional</option>
                                                            <option value="CIBBMYKL" <?php echo $user->info['preference']['bank'] == "CIBBMYKL" ? " selected" : ''; ?>>CIMB Bank</option>
                                                            <option value="HLBBMYKL" <?php echo $user->info['preference']['bank'] == "HLBBMYKL" ? " selected" : ''; ?>>Hong Leong Bank</option>
                                                            <option value="HBMBMYKL" <?php echo $user->info['preference']['bank'] == "HBMBMYKL" ? " selected" : ''; ?>>HSBC</option>
                                                            <option value="KFHOMYKL" <?php echo $user->info['preference']['bank'] == "KFHOMYKL" ? " selected" : ''; ?>>Kuwait Finance House Bank</option>
                                                            <option value="MBBEMYKL" <?php echo $user->info['preference']['bank'] == "MBBEMYKL" ? " selected" : ''; ?>>Maybank</option>
                                                            <option value="OCBCMYKL" <?php echo $user->info['preference']['bank'] == "OCBCMYKL" ? " selected" : ''; ?>>OCBC Bank</option>
                                                            <option value="PBBEMYKL" <?php echo $user->info['preference']['bank'] == "PBBEMYKL" ? " selected" : ''; ?>>Public Bank</option>
                                                            <option value="RHBBMYKL" <?php echo $user->info['preference']['bank'] == "RHBBMYKL" ? " selected" : ''; ?>>RHB Bank</option>
                                                            <option value="SCBLMYKX" <?php echo $user->info['preference']['bank'] == "SCBLMYKX" ? " selected" : ''; ?>>Standard Chartered</option>
                                                            <option value="UOVBMYK" <?php echo $user->info['preference']['bank'] == "UOVBMYK" ? " selected" : ''; ?>>UOB</option>
                                                        </select>
                                                    </div>
                                                    <div class="input-group-flex row-banknotes">
                                                        <div class="pref-banknotes"><?php echo lang('pref_bank_notes'); ?></div>
                                                    </div>
                                                </div>
                                                <div class="form-flex" id="work_location">
                                                    <div class="input-group-flex row-accholdername">
                                                        <input type="text" class="form-control form-control-input" placeholder="<?php echo lang('pref_account_name'); ?>" name="preference[acc_name]" value="<?php echo  $user->info['preference']['acc_name']; ?>">
                                                    </div>
                                                    <div class="input-group-flex row-accno">
                                                        <input type="text" class="form-control form-control-input" placeholder="<?php echo lang('pref_account_num'); ?>" name="preference[acc_num]" value="<?php echo  $user->info['preference']['acc_num']; ?>" maxlength="16">
                                                    </div>
                                                </div>
                                            </div>
											<?php if($user->info['type'] == '0'){ ?>
											<div class="form-collapse-row row-collapse-preference">
												<div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('pref_task_pref'); ?></span></div>
												<div class="form-flex">
													<div class="input-group-block row-indlocremote">
														<label class="input-lbl input-lbl-block">
															<span class="input-label-txt"><?php echo lang('pref_remote_match'); ?>:</span>
														</label>
														<div class="form-flex">
															<div class="input-group-flex row-indlocremoteselect">
		                                                        <select class="form-control form-control-input" name="preference[rm_work_location]" placeholder="Work Location">
		                                                            <!--option value="" disabled selected>Select Work Location</option-->
		                                                            <!--option value="near" <?php echo $user->info['preference']['rm_work_location'] == "near" ? " selected" : ""; ?>>Near me</option-->
		                                                            <option value="same_country" <?php echo $user->info['preference']['rm_work_location'] == "same_country" ? " selected" : ""; ?>><?php echo lang('pref_remote_match_same_country'); ?></option>
		                                                            <option value="anywhere" <?php echo $user->info['preference']['rm_work_location'] == "anywhere" ? " selected" : ""; ?>><?php echo lang('pref_remote_match_anywhere'); ?></option>
		                                                        </select>
															</div>
															<div class="input-group-flex row-indlocremotestate rm-work rm-state d-none">
																<select class="form-control form-control-input" placeholder="State" name="preference[rm_state]" data-selected="<?php echo $user->info['preference']['rm_state']; ?>">
																	<option value="0" selected><?php echo lang('pref_all_states'); ?></option>
																	<?php foreach($cms->states($user->info['country']) as $state){ ?>
																	<option value="<?php echo $state['id']; ?>" <?php echo $state['id'] == $user->info['preference']['rm_state'] ? " selected" : ""; ?>><?php echo $state['name']; ?></option>
																	<?php } ?>
																</select>
															</div>
														</div>
													</div>
                                                </div>
                                                <div class="form-flex">
                                                    <div class="input-group-block row-indlocphysical" id="workdays">
                                                        <label class="input-lbl input-lbl-block">
                                                            <span class="input-label-txt"><?php echo lang('pref_physical'); ?>:</span>
                                                        </label>
                                                        <div class="form-flex">
                                                            <div class="input-group-flex row-indlocphysicalselect">
                                                                <select class="form-control form-control-input" placeholder="Work Loc" name="preference[p_work_location]">
                                                                <!--<option value="near" <?php echo ($user->info['preference']['p_work_location'] == "near") ? " selected" : ""; ?>><?php echo lang('pref_near_me'); ?></option>-->
                                                                    <option value="same_country" <?php echo ($user->info['preference']['p_work_location'] == "same_country") ? " selected" : ""; ?>><?php echo lang('pref_same_country'); ?></option>
                                                                    <option value="anywhere" <?php echo ($user->info['preference']['p_work_location'] == "anywhere") ? " selected" : ""; ?>><?php echo lang('pref_anywhere'); ?></option>
                                                                </select>
                                                            </div>
                                                            <div class="input-group-flex row-indlocphysicalstate p-work p-state d-none">
                                                                <select class="form-control form-control-input" placeholder="State" name="preference[p_state]" data-selected="<?php echo $user->info['preference']['p_state']; ?>">        
                                                                    <option value="0" selected><?php echo lang('pref_all_states'); ?></option>
                                                                    <?php foreach($cms->states($user->info['country']) as $state){ ?>
                                                                    <option value="<?php echo $state['id']; ?>" <?php echo $state['id'] == $user->info['preference']['p_state'] ? " selected" : ""; ?>><?php echo $state['name']; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="input-group-flex row-indlocphysicalnearest p-work p-radius d-none">
                                                                <select class="form-control form-control-input" name="preference[p_radius]" placeholder="Work Radius">
                                                                    <option value="" disabled selected><?php echo lang('pref_select_radius'); ?></option>
                                                                    <option value="Within 5km" <?php echo $user->info['preference']['p_radius'] == "Within 5km" ? " selected" : ""; ?>><?php echo lang('pref_select_radius_within'); ?> 5km</option>
                                                                    <option value="Within 10km" <?php echo $user->info['preference']['p_radius'] == "Within 10km" ? " selected" : ""; ?>><?php echo lang('pref_select_radius_within'); ?> 10km</option>
                                                                    <option value="Within 20km" <?php echo $user->info['preference']['p_radius'] == "Within 20km" ? " selected" : ""; ?>><?php echo lang('pref_select_radius_within'); ?> 20km</option>
                                                                    <option value="Within 30km" <?php echo $user->info['preference']['p_radius'] == "Within 30km" ? " selected" : ""; ?>><?php echo lang('pref_select_radius_within'); ?> 30km</option>
                                                                    <option value="Within 50km" <?php echo $user->info['preference']['p_radius'] == "Within 50km" ? " selected" : ""; ?>><?php echo lang('pref_select_radius_within'); ?> 50km</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-flex" id="myProfileWorkdays">
                                                    <div class="input-group-block">
                                                        <label class="input-lbl input-lbl-block">
                                                            <span class="input-label-txt"><?php echo lang('pref_workday_pref'); ?></span>
                                                        </label>
                                                        <div class="form-block">
                                                            <div class="form-radio-row row-indallweek">
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" class="custom-control-input" id="workdaysAllWeek" name="preference[work_day]" value="all_week"<?php echo $user->info['preference']['work_day'] == 'all_week' ? ' checked' : ''; ?>>
                                                                    <label class="custom-control-label" for="workdaysAllWeek"><?php echo lang('pref_workday_pref_all_week'); ?></label>
                                                                </div>
                                                            </div>
                                                            <div class="form-radio-row row-indweekday">
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" class="custom-control-input" id="workdayCustom" name="preference[work_day]" value="custom"<?php echo $user->info['preference']['work_day'] == 'custom' ? ' checked' : ''; ?>>
                                                                    <label class="custom-control-label" for="workdayCustom"><?php echo lang('pref_workday_custom'); ?></label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox" id="checkWeekdays">
                                                                    <input type="checkbox" class="custom-control-input parent-weekdays" id="workdaysWeekdays" name="preference[work_day_weekday]" value="1"<?php echo $user->info['preference']['work_day_weekday'] ? ' checked' : ''; ?>>
                                                                    <label class="custom-control-label" for="workdaysWeekdays"><?php echo lang('pref_workday_weekdays'); ?></label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox" id="checkFullday">
                                                                    <input type="checkbox" class="custom-control-input child-weekdays" id="fullDay" name="preference[work_day_weekday_full]" value="1"<?php echo $user->info['preference']['work_day_weekday_full'] ? ' checked' : ''; ?>>
                                                                    <label class="custom-control-label" for="fullDay"><?php echo lang('pref_workday_fullday'); ?></label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox" id="checkAfterHour">
                                                                    <input type="checkbox" class="custom-control-input child-weekdays" id="afterWorkHour" name="preference[work_day_weekday_after]" value="1"<?php echo $user->info['preference']['work_day_weekday_after'] ? ' checked' : ''; ?>>
                                                                    <label class="custom-control-label" for="afterWorkHour"><?php echo lang('pref_workday_after_hour'); ?></label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox" id="checkWeekends">
                                                                    <input type="checkbox" class="custom-control-input parent-weekends" id="workdaysWeekend" name="preference[work_day_weekend]" value="1"<?php echo $user->info['preference']['work_day_weekend'] ? ' checked' : ''; ?>>
                                                                    <label class="custom-control-label" for="workdaysWeekend"><?php echo lang('pref_workday_weekends'); ?></label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox" id="checkAm">
                                                                    <input type="checkbox" class="custom-control-input child-weekends" id="amwork" name="preference[work_day_weekend_am]" value="1"<?php echo $user->info['preference']['work_day_weekend_am'] ? ' checked' : ''; ?>>
                                                                    <label class="custom-control-label" for="amwork">a.m</label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox" id="checkPm">
                                                                    <input type="checkbox" class="custom-control-input child-weekends" id="pmwork" name="preference[work_day_weekend_pm]" value="1"<?php echo $user->info['preference']['work_day_weekend_pm'] ? ' checked' : ''; ?>>
                                                                    <label class="custom-control-label" for="pmwork">p.m</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--div class="form-flex">
                                                    <div class="input-group-flex row-indworkhours">
                                                        <select class="form-control form-control-input" placeholder="Select Work Hours" name="preference[working_hours]">
                                                            <option disabled selected>Select Work Hours</option>
                                                            <option value="1" <?php echo ($user->info['preference']['working_hours'] == "1") ? " selected" : ""; ?> >1</option>
                                                            <option value="2" <?php echo ($user->info['preference']['working_hours'] == "2") ? " selected" : ""; ?>>2</option>
                                                            <option value="3" <?php echo ($user->info['preference']['working_hours'] == "3") ? " selected" : ""; ?>>3</option>
                                                            <option value="4" <?php echo ($user->info['preference']['working_hours'] == "4") ? " selected" : ""; ?>>4</option>
                                                            <option value="5" <?php echo ($user->info['preference']['working_hours'] == "5") ? " selected" : ""; ?>>5</option>
                                                            <option value="6" <?php echo ($user->info['preference']['working_hours'] == "6") ? " selected" : ""; ?>>6</option>
                                                            <option value="7" <?php echo ($user->info['preference']['working_hours'] == "7") ? " selected" : ""; ?>>7</option>
                                                            <option value="8" <?php echo ($user->info['preference']['working_hours'] == "8") ? " selected" : ""; ?>>8</option>
                                                            <option value="9" <?php echo ($user->info['preference']['working_hours'] == "9") ? " selected" : ""; ?>>9</option>
                                                            <option value="10" <?php echo ($user->info['preference']['working_hours'] == "10") ? " selected" : ""; ?>>10</option>
                                                            <option value="11" <?php echo ($user->info['preference']['working_hours'] == "11") ? " selected" : ""; ?>>11</option>
                                                            <option value="12" <?php echo ($user->info['preference']['working_hours'] == "12") ? " selected" : ""; ?>>12</option>
                                                        </select>
                                                    </div>
                                                </div-->
                                                <div class="form-flex" id="myProfileWorkload">
                                                    <div class="input-group-block row-indworkload" id="workload">
                                                        <label class="input-lbl">
                                                            <span class="input-label-txt"><?php echo lang('pref_workload_pref'); ?></span>
                                                        </label>
                                                        <div class="form-flex">
                                                        <label class="input-lbl input-lbl-block">
                                                            <span class="input-label-txt"><?php echo lang('pref_workload_pref_like_to_monetize'); ?></span>
                                                        </label>
                                                            <input type="number" name="preference[work_load_hours]"  class="form-control form-control-input" min="1" max="70" id="" value="<?php echo  $user->info['preference']['work_load_hours'] ?>" />
                                                            <span class="postlbl-hours"><?php echo lang('pref_workload_pref_hours_available'); ?></span>   
                                                        </div>
	                                                    <p class="help-notes"><?php echo lang('pref_workload_pref_note'); ?></p>
	                                                    <p class="help-notes"><?php echo lang('pref_workload_pref_note2'); ?></p>
                                                    </div>
                                                </div>
                                                <div class="form-flex" id="myPreferredLang">
                                                <label class="input-lbl">
                                                            <span class="input-label-txt"><?php echo lang('pref_lang_for_site') ?></span>
                                                        </label>
                                                        <div class="form-flex">
                                                        <div class="custom-control custom-radio">
                                                                    <input type="radio" class="custom-control-input" id="langEng" name="preference[language]" value="1" <?php echo $user->info['preference']['language'] === '1' ? 'checked' : '' ?>>
                                                                    <label class="custom-control-label" for="langEng">English</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" class="custom-control-input" id="langBM" name="preference[language]" value="2" <?php echo $user->info['preference']['language'] === '2' ? 'checked' : '' ?>>
                                                                    <label class="custom-control-label" for="langBM">BM</label>
                                                                </div>
                                                                </div>
                                                                </div>
                                                <div class="form-flex" id="mySurvey">
                                                <label class="input-lbl">
                                                            <span class="input-label-txt"><?php echo lang('pref_survey_title') ?></span>
                                                        </label>
                                                        <div class="form-flex">
                                                <div class="custom-control custom-checkbox" id="checkSurvey">
                                                                    <input type="checkbox" class="custom-control-input child-weekends" id="survey" name="preference[survey]" value="1"  <?php echo $user->info['preference']['survey'] === '1' ? 'checked' : '' ?>>
                                                                    <label class="custom-control-label" for="survey"><?php echo lang('pref_survey_i_would_like_to_be_part') ?></label>
                                                                </div>
                                                                </div>
                                                                </div>
                                                <!--div class="form-flex">
                                                    <div class="input-group-flex row-indexpectedearn">
                                                        <select class="form-control form-control-input" name="preference[expected_earning]" placeholder="Expected Earnings">
                                                            <option disabled selected>Expected Earnings</option>
                                                            <?php foreach(expectedEarningList() as $value => $title){ ?>
                                                            <option value="<?php echo $value; ?>" <?php echo ($user->info['preference']['expected_earning'] == $value) ? " selected" : ""; ?>><?php echo $title; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div-->
                                            </div>
											<!--<div class="form-collapse-row row-collapse-dispute-resolution">
												<div class="collapse-title-lbl"><span class="title-collapse">Dispute Resolution</span><i class="fa fa-caret-down"></i></div>
												<div class="form-flex">
													<div class="input-group row-dispute-resolution">
														<span class="dispute-resolution-lbl first">Seeker</span>
														<input id="disputePercentage" type="text" />
														<span class="dispute-resolution-lbl last">Poster</span>
													</div>
													<div class="dispute-info">Parties are advised to resolve any issues amicably through discussion. A Dispute Resolution will lead to a <u>permanent indicator</u> on your profile that you have adopted this option for one or more of your projects</div>
												</div>
											</div>-->
											<?php } ?>
											<div class="footer-form-action">
												<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
													<span class="btn-label"><?php echo lang('pref_save'); ?></span>
													<span class="btn-icon">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
															<polyline points="20 6 9 17 4 12"></polyline>
														</svg>
													</span>
												</button>
												<button type="button" class="btn-icon-full btn-cancel" onClick="location.href='<?php echo url_for('/dashboard'); ?>'">
													<span class="btn-label"><?php echo lang('pref_cancel'); ?></span>
													<span class="btn-icon">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
															<line x1="18" y1="6" x2="6" y2="18"></line>
															<line x1="6" y1="6" x2="18" y2="18"></line>
														</svg>
													</span>
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
    
<script type="text/javascript">
    $(function(){
        $('.card-title').text('Settings');
        <?php if($user->info['preference']['work_day'] != 'custom'){ ?>
            $('[name="preference[work_day_weekday]"], [name="preference[work_day_weekday_full]"], [name="preference[work_day_weekday_after]"], [name="preference[work_day_weekend]"], [name="preference[work_day_weekend_am]"], [name="preference[work_day_weekend_pm]"]').prop({ 'checked': false, 'disabled': true });
        <?php } ?>
        

        $(document).on('change', '[name="preference[rm_work_location]"]', function(){
            $('.rm-work').hide();
            val = $(this).val();
            
            if(val == 'near'){
                $('.rm-radius').removeClass('d-none').show();
            }else if(val == 'same_country'){
                $('.rm-state').removeClass('d-none').show();
            }
        });
        $('[name="preference[rm_work_location]"]').change();

        $(document).on('change', '[name="preference[p_work_location]"]', function(){
            $('.p-work').hide();
            val = $(this).val();
            
            if(val == 'near'){
                $('.p-radius').removeClass('d-none').show();
            }else if(val == 'same_country'){
                $('.p-state').removeClass('d-none').show();
            }
        });
        $('[name="preference[p_work_location]"]').change();
    });
</script>

    


