<div class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-chart">
				<div class="card-header">
					<div class="row">
						<div class="col-sm-12 col-header-filter">
							<h2 class="card-title"><?php echo lang('dashboard_title'); ?></h2>
                            <?php echo partial('workspace/partial/header.html.php'); ?>
						</div>
					</div>
				</div>
				<?php if($preference['dashboard_mode'] === 'search'): ?>
				<?php echo partial('workspace/partial/dashboard-tasks-listing.html.php') ?>
				<?php else: ?>
				<div class="card-body-dashboard">
                    <?php if((int)$user->info['type'] === 0): ?>
					<div class="row">
						<div class="col-xl-12">
							<div class="row">
								<?php echo partial("partial/search-form.html.php") ?>
							</div>

							<?php // Calculate Wellness Indicator Productivity
							$work_load_hours = $user->info['preference']['work_load_hours'];
							$wellness_percent = round(($workload_this_week / ($work_load_hours < 1 ? 0 : $work_load_hours)) * 100);
							$wellness_margin = 7;
							if($wellness_percent >= 30 && $wellness_percent < 66)  $wellness_margin = 45;
							if($wellness_percent >= 66 && $wellness_percent <= 100)  $wellness_margin = 85;
							?>
							<div class="row">
								<div class="col-xl-7 col-dashboard-top-left">
									<div class="col-dashboard">
										<div class="col-dashboard-flex">
											<div class="col-flex-left">
												<?php if($wellness_margin <= 7){ ?>
												<div class="indicator-img"><img src="<?php echo imgCrop('assets/img/MTP-Web-Illus-Dashboard-1.png'); ?>" alt="" /></div>
												<?php } ?>
												<?php if($wellness_margin == 45){ ?>
												<div class="indicator-img"><img src="<?php echo imgCrop('assets/img/MTP-Web-Illus-Dashboard-1.png'); ?>" alt="" /></div>
												<?php } ?>
												<?php if($wellness_margin >= 85){ ?>
												<div class="indicator-img"><img src="<?php echo imgCrop('assets/img/MTP-Web-Illus-Indicator-take-a-break.png'); ?>" alt="" /></div>
												<?php } ?>
											</div>
											<div class="col-flex-right">
												<div class="col-title-container-flex">
													<div class="col-title-main"><?php echo lang('dashboard_wellness_indicator'); ?></div>
													<div class="col-title-sub"><?php echo lang('dashboard_this_week_productivity'); ?></div>
												</div>
												<div class="col-bar-indicator">
													<div class="bar-indicator"><span class="bar-indicator-point" style="left:<?php echo $wellness_margin; ?>%;"></span></div>
													<div class="bar-status-flex">
														<span class="bar-status"><?php echo lang('dashboard_this_week_productivity_relaxed') ?></span>
														<span class="bar-status"><?php echo lang('dashboard_this_week_productivity_doing_great') ?></span>
														<span class="bar-status"><?php echo lang('dashboard_this_week_productivity_take_a_break') ?></span>
													</div>
												</div>
												<!--div class="col-main-desc">
													<p class="body-desc">Time to get up and make time pay. Explore even more curated tasks for you <a href="#" class="body-link">here ></a></p>
												</div-->
												<div class="col-workload-flex">
													<div class="col-desc"><?php echo lang('dashboard_wellness_description'); ?></div>
													<div class="col-total-workload-btn-flex">
														<div class="col-total-workload">
															<div class="sm-title"><?php echo lang('dashboard_wellness_workload_this_week'); ?></div>
															<div class="total-workload">
                                                                <span class="total-workload-lg"><?php echo $workload_this_week; ?>/<?php echo $work_load_hours < 1 ? 0 : $work_load_hours; ?>
                                                                </span>
																<span class="total-workload-sm"><?php echo lang('dashboard_wellness_workload_this_week_hours'); ?></span>
															</div>
														</div>
														<a href="<?php echo url_for('/settings#workload'); ?>" class="btn-icon-link gtm-dashboard-wellness">
															<div class="btn-icon-custom btn-chevron-right"><span class="btn-label"><?php echo lang('dashboard_wellness_workload_this_week_change'); ?></span><span class="btn-icon"><svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" /></svg></span></div>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xl-5 col-dashboard-top-right">
									<div class="col-dashboard">
										<div class="sub-col-container">
											<div class="sub-col-left-row-1-group-new-task">
												<div class="sub-col-center-row">
													<div class="sub-col-new-task-listing-title"><?php echo lang('dashboard_latest_task_title'); ?></div>
													<div class="sub-col-new-task-listing-button">
														<ul class="nav nav-tabs nav-tabs-sm nav-header-tabs"
														    id="latest_task_header_tabs" role="tablist">
															<li class="nav-item nav-od-item">
																<div class="nav-link active gtm-dashboard-justposted" id="justposted_tab"
																     data-toggle="tab" data-target="#justposted"
																     role="tab" aria-controls="justposted"
																     aria-selected="true">
																	<span class="nav-tabs-lbl"><?php echo lang('dashboard_latest_task_just_posted'); ?></span>
																</div>
															</li>
															<li class="nav-item nav-ft-item">
																<div class="nav-link gtm-dashboard-matchskills" id="matchskills_tab"
																     data-toggle="tab" data-target="#matchskills"
																     role="tab" aria-controls="matchskills"
																     aria-selected="false">
																	<span class="nav-tabs-lbl"><?php echo lang('dashboard_latest_task_matches_my_skills'); ?></span>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="tab-content" id="latest_task_tab_contents">
												<div class="tab-pane fade show active" id="justposted" role="tabpanel"
												     aria-labelledby="justposted">
													<div class="sub-col-row-2-group-new-task">
														<?php if(empty($just_posted)): ?>
														<div class="sub-col-content empty-state">
															<div class="empty-state-container">
																<div class="empty-img"><img src="<?php echo url_for("assets/img/mtp-no-data-2.png") ?>" alt=""></div>
																<span class="empty-lbl"><?php echo lang('dashboard_latest_no_task_to_display'); ?></span>
															</div>
														</div>
														<?php endif ?>
														<?php if( !empty($just_posted) ): ?>
														<?php $groups = array_chunk($just_posted, 4) ?>
														<div id="swiper_just_posted" class="swiper-container">
															<div class="swiper-wrapper">
																<?php foreach ($groups as $group): ?>
																<div class="swiper-slide sub-col-row">
																	<?php foreach ($group as $posted): ?>
																	<div class="sub-col-row-2-group-new-task-sub-container">
																		<div class="sub-col-row-2-group-new-task-listing">
																			<div class="sub-col-row-2-group-new-task-listing-title">
																				<?php echo $posted['title'] ?>
																			</div>
																			<div class="sub-col-row-2-group-new-task-listing-description">
																				<?php echo strip_tags(str_replace('<', ' <', $posted['description'])) ?>
																			</div>
																		</div>
																		<div class="sub-col-row-2-group-new-task-view-more">
																			<a href="<?php echo url_for("workspace/task/{$posted['slug']}/details") ?>"
																			   class="viewmore-taskdetails-link gtm-justposted-view">
                                                                                            <span class="link-icon">
                                                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                     width="24"
                                                                                                     height="24"
                                                                                                     viewBox="0 0 24 24"
                                                                                                     fill="none"
                                                                                                     stroke="currentColor"
                                                                                                     stroke-width="2"
                                                                                                     stroke-linecap="round"
                                                                                                     stroke-linejoin="arcs">
                                                                                                    <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                                                                    <circle cx="12"
                                                                                                            cy="12"
                                                                                                            r="3"></circle>
                                                                                                </svg>
                                                                                            </span>
																				<span class="link-lbl"><?php echo lang('dashboard_latest_view_more'); ?></span>
																			</a>
																		</div>
																	</div>
																	<?php endforeach ?>
																</div>
																<?php endforeach ?>
															</div>
															<?php if( count($just_posted) > 4 ): ?>
															<div id="swiper_pagination_just_posted"
															     class="swiper-pagination"></div>
															<?php endif ?>
														</div>
														<?php endif ?>
													</div>
												</div>
												<div class="tab-pane fade show" id="matchskills" role="tabpanel"
												     aria-labelledby="matchskills">
													<div class="sub-col-row-2-group-new-task">
														<?php if(empty($matched_skills)): ?>
														<div class="sub-col-content empty-state">
															<div class="empty-state-container">
																<div class="empty-img"><img src="<?php echo url_for("assets/img/mtp-no-data-2.png") ?>" alt=""></div>
																<span class="empty-lbl"><?php echo lang('dashboard_latest_no_task_to_display'); ?></span>
															</div>
														</div>
														<?php endif ?>
                                                        <?php if( !empty($matched_skills) ): ?>
                                                        <?php $groups = array_chunk($matched_skills, 4) ?>
														<div id="swiper_match_skills" class="swiper-container">
															<div class="swiper-wrapper">
																<?php foreach($groups as $group): ?>
																<div class="swiper-slide sub-col-row">
																	<?php foreach ($group as $matched): ?>
																	<div class="sub-col-row-2-group-new-task-sub-container">
																		<div class="sub-col-row-2-group-new-task-listing">
																			<div class="sub-col-row-2-group-new-task-listing-title">
																				<?php echo $matched['title'] ?>
																			</div>
																			<div class="sub-col-row-2-group-new-task-listing-description">
																				<?php echo strip_tags(str_replace('<', ' <', $matched['description'])) ?>
																			</div>
																		</div>
																		<div class="sub-col-row-2-group-new-task-view-more">
																			<a href="<?php echo url_for("workspace/task/{$matched['slug']}/details") ?>"
																			   class="viewmore-taskdetails-link gtm-matchskills-view">
                                                                                            <span class="link-icon">
                                                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                     width="24"
                                                                                                     height="24"
                                                                                                     viewBox="0 0 24 24"
                                                                                                     fill="none"
                                                                                                     stroke="currentColor"
                                                                                                     stroke-width="2"
                                                                                                     stroke-linecap="round"
                                                                                                     stroke-linejoin="arcs">
                                                                                                    <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                                                                    <circle cx="12"
                                                                                                            cy="12"
                                                                                                            r="3"></circle>
                                                                                                </svg>
                                                                                            </span>
																				<span class="link-lbl"><?php echo lang('dashboard_latest_view_more'); ?></span>
																			</a>
																		</div>
																	</div>
																	<?php endforeach ?>
																</div>
																<?php endforeach ?>
															</div>
															<?php if( count($matched_skills) > 4 ): ?>
															<div id="swiper_pagination_match_skills"
															     class="swiper-pagination"></div>
															<?php endif ?>
														</div>
														<?php endif ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-12">
							<div class="row widget-container">
                                <?php echo $cms->dashboardWidget(); ?>
							</div>
						</div>
					</div>
                        <?php else: ?>
					<div class="row">
                        <div class="col-xl-12">
	                        <div class="row">
                                <?php echo partial("partial/search-form.html.php") ?>
	                        </div>
                        </div>
						<div class="col-xl-12">
							<div class="row">
                                <?php echo $cms->dashboardWidget(); ?>
							</div>
						</div>
					</div>
						<?php endif ?>
				</div>
				<?php endif ?>
			</div>
		</div>
	</div>
</div>

    
<script>
window.onload = function(){
    if( $('#swiper_just_posted').length ) {
        var swiperJustPosted = new Swiper('#swiper_just_posted', {
            pagination: {
                el: '#swiper_pagination_just_posted',
                type: 'bullets',
            },
            loop: true,
            speed: 1000,
            spaceBetween: 50,
            observer: true,
            observeParents: true
        });
    }
    if( $('#swiper_match_skills').length ) {
        var swiperMatchSkills = new Swiper('#swiper_match_skills', {
            pagination: {
                el: '#swiper_pagination_match_skills',
                type: 'bullets',
            },
            loop: true,
            speed: 1000,
            spaceBetween: 50,
            observer: true,
            observeParents: true
        });
    }
    <?php if( isset($was_viewing) ): ?>
    $(document).on('submit', 'form.favourite-form', function(e){
        e.preventDefault();
        var form = $(this);

        form.find('[type=submit]').prop('disabled', true);

        if( form.find('.btn-fav').length ){
            form.find('.btn-fav').removeClass('btn-fav').addClass('btn-unfav');
        }else{
            form.find('.btn-unfav').removeClass('btn-unfav').addClass('btn-fav');
        }

        $.ajax({
            url: '<?php echo option("site_uri") ?>' + form.attr('action'),
            method: 'POST',
            data: form.serialize()
        }).done(function(response){
            if(response.status === 'success'){
                if( response.action === 'added' ){
                    form.find('[name*="favourited"]').val('true');
                }else{
                    form.find('[name*="favourited"]').val('false');
                }
                //t('s', response.message, response.title);
            }else{
                //t('e', response.message, response.title);
                if( form.find('.btn-unfav').length ){
                    form.find('.btn-unfav').removeClass('btn-unfav').addClass('btn-fav');
                }else{
                    form.find('.btn-fav').removeClass('btn-fav').addClass('btn-unfav');
                }
            }
            form.find('[type=submit]').prop('disabled', false);
        });
    });
	<?php endif ?>
}
</script>

<?php if( (int)$user->info['type'] === 0 && isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'personalize') !== false && (int)$completion <= 50 ): ?>
<?php content_for('scripts'); ?>
<script>
    $(function() {
        var elem = '<div class="popover-complete-profile"><div class="popover-title">Complete your profile to start posting and applying for task!</div><div class="popover-desc">You’re just a few quick and easy steps towards making your time pay.</div><div class="popover-actions"><button id="close-popover" data-toggle="clickover" class="btn-txt-only btn-gotit gtm-popover-got-it" onclick="$(&quot;#completeProfileBtn&quot;).popover(&quot;dispose&quot;);">Got it</button></div></div>';

        $('#completeProfileBtn').popover({animation:true, content:elem, html:true,placement:'bottom',sanitize: false});
        window.setTimeout("$('#completeProfileBtn').popover('show')",300);

    });
</script>
<?php end_content_for(); ?>
<?php endif ?>
