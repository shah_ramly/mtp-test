<?php
$dashboard_widget = explode(',', $user->info['dashboard_widget']);
?>

<?php if(in_array('1', $dashboard_widget)){ ?>
<div class="col-xl-3 col-dashboard-btm col-dashboard-btm-left col-dashboard-today-task">
    <div class="col-dashboard col-vert-flex">
        <div class="col-dashboard-top-content">
            <div class="sub-col-title"><?php echo lang('widget_today_task_title'); ?></div>
			
			<?php if($today_task) { ?>
            <div class="sub-col-content">
                <div class="sub-col-content-flex">
                    <div class="sub-col-card-row sub-col-card-row-val">
						<?php 
							$count=0; 
							foreach($today_task as $today){ 
								$count++;
							}
							
							if($count < 10){
								$count = '0'.$count;
							}
						?>
                        <div class="sub-col-content-left-val"><?php echo $count; ?></div>
                    </div>
					<?php foreach($today_task as $today){ ?>
                    <div class="sub-col-card-row">
                        <div class="sub-col-card-row-flex">
                            <div class="sub-col-card-row-left">
	                            <a href="<?php echo url_for("workspace/task/{$today['slug']}/details") ?>" class="gtm-widget-today-task-link">
                                <div class="sub-col-card-row-left-val"><?php echo $today['title']; ?></div>
	                            </a>
                            </div>
                        </div>
                    </div>
					<?php } 
					?>
                </div>
            </div>
			<?php }else{ ?>
			<div class="sub-col-content empty-state">
				<div class="empty-state-container">
					<div class="empty-img"><img src="<?php echo url_for("assets/img/mtp-no-data-2.png") ?>" alt=""></div>
					<span class="empty-lbl"><?php echo lang('widget_no_task_title'); ?></span>
				</div>
			</div>
			<?php } ?>
        </div>
    </div>
</div>
<?php } ?>

<?php if(in_array('2', $dashboard_widget)){ ?>
<div class="col-xl-3 col-dashboard-btm col-dashboard-btm-left-mid col-dashboard-curated-task">
    <div class="col-dashboard col-vert-flex">
        <div class="col-dashboard-top-content">
            <div class="sub-col-title"><?php echo lang('widget_task_curated_title'); ?></div>
			<div class="sub-col-content empty-state">
				<div class="empty-state-container">
					<div class="empty-img"><img src="assets/img/mtp-no-data-2.png" alt=""></div>
					<span class="empty-lbl"><?php echo lang('widget_no_task_title'); ?></span>
				</div>
			</div>
        </div>
    </div>
</div>
<!-- Initialize Swiper -->
<script>
    window.addEventListener('load', function(){
        var swiperCurated = new Swiper('#swiper_curated_tasks', {
            pagination: {
                el: '#swiper_pagination_curated_tasks',
                type: 'bullets',
            },
            loop: true,
            speed: 1000
        });
    });
</script>
<!-- Initialize Swiper -->
<?php } ?>

<?php if(in_array('3', $dashboard_widget)){ ?>
<div class="col-xl-3 col-dashboard-btm col-dashboard-btm-right-mid col-dashboard-task-pending-decision">
    <div class="col-dashboard col-vert-flex">
        <div class="col-dashboard-top-content">
            <div class="sub-col-title"><?php echo lang('widget_task_applied_for_title'); ?></div>
			<?php if($pending_task){ ?>
            <div class="sub-col-content">
                <div id="swiper_pending_tasks" class="swiper-container sub-col-rows">
                    <div class="swiper-wrapper">
					<?php foreach($pending_task as $pending){ ?>
                        <div class="swiper-slide sub-col-row">
	                        <a href="<?php echo url_for("workspace/task/{$pending['slug']}/details") ?>" class="gtm-widget-pending-task-link">
                            <div class="sub-col-swiper-title"><?php echo $pending['title']; ?></div>
	                        </a>
                            <div class="sub-col-swiper-subtitle"><span class="highlighted"><?php echo ($pending['hours'] == 0 ) ? lang('dashboard_widget_daily') : $pending['hours'].'-'.($pending['hours'] == 1 ? lang('task_hour'):lang('task_hours'));?></span> <?php echo lang('widget_task_applied_for_task_pays'); ?> <span class="highlighted"><?php echo 'RM'.number_format($pending['budget']); ?></span></div>
                            <div class="sub-col-swiper-desc"><?php echo lang('widget_task_applied_for_you_applied_for_you_applied_for'); ?> <?php echo date($cms->settings()['date_format'], strtotime($pending['applied_date'])); ?></div>
                            <div class="sub-col-swiper-desc"><?php echo lang('widget_task_applied_sets_to_close_on'); ?> <?php echo date($cms->settings()['date_format'], strtotime($pending['complete_by'])); ?></div>
                            <!--div class="sub-col-swiper-desc-link"><a href="#">Retract Application</a></div-->
                        </div>
					<?php } ?>
                    </div>
                    <div id="swiper_pagination_pending_tasks" class="swiper-pagination"></div>
                </div>
            </div>
			<?php }else{ ?>
			<div class="sub-col-content empty-state">
				<div class="empty-state-container">
					<div class="empty-img"><img src="assets/img/mtp-no-data-2.png" alt=""></div>
					<span class="empty-lbl"><?php echo lang('widget_no_task_title'); ?></span>
				</div>
			</div>
			<?php } ?>
        </div>
    </div>
</div>
<!-- Initialize Swiper -->
<script>
    window.addEventListener('load', function(){
        var swiperPending = new Swiper('#swiper_pending_tasks', {
            pagination: {
                el: '#swiper_pagination_pending_tasks',
                type: 'bullets',
            },
            loop: true,
            speed: 1000
        });
    });
</script>
<!-- Initialize Swiper -->
<?php } ?>

<?php if(in_array('4', $dashboard_widget)){ ?>
<div class="col-xl-3 col-dashboard-btm col-dashboard-btm-right col-dashboard-total-earnings">
    <div class="col-dashboard col-vert-flex">
        <div class="col-dashboard-top-content">
            <div class="sub-col-title"><?php echo lang('widget_total_earnings'); ?></div>
            <div class="sub-col-content">
                <div class="sub-two-rows-flex">
                    <div class="sub-two-rows-top">
                        <div class="sub-two-rows-val">
						<?php
							$total_earning = 0.00;
								foreach($earning_this_month as $income){
									// Get total amount of closed task
									if($income['status'] == 'closed'){
										$total_earning += $income['budget'];
									}
									// Get total amount of disputed amount
									if($income['status'] == 'disputed'){
										$income_percentage = (100 - $income['rejection_rate']) / 100;
										$total_earning += $income['budget'] * $income_percentage;
									}
									
								}
						
						?>
						
						
						
						<?php echo ($total_earning == 0) ? 'RM0.00':'RM'.number_format($total_earning,2); ?></div>
                        <div class="sub-two-rows-desc"><?php echo lang('widget_total_earnings_this_month_so_far'); ?></div>
                    </div>
                    <div class="sub-two-rows-btm">
					
						<?php
							$total_last_earning = 0.00;
								foreach($earning_last_month as $income){
									// Get total amount of closed task
									if($income['status'] == 'closed'){
										$total_last_earning += $income['budget'];
									}
									// Get total amount of disputed amount
									if($income['status'] == 'disputed'){
										$income_percentage = (100 - $income['rejection_rate']) / 100;
										$total_last_earning += $income['budget'] * $income_percentage;
									}
									
								}
						
						?>
                        <div class="sub-two-rows-val"><?php echo ($total_last_earning == 0) ? 'RM0.00':'RM'.number_format($total_last_earning,2); ?></div>
                        <div class="sub-two-rows-desc"><?php echo lang('widget_total_earnings_previous_month'); ?></div>
                    </div>
                </div>
            </div>
        </div>
        <a href="<?php echo url_for("workspace/billings/earning/"); ?>" class="btn-icon-link gtm-widget-earning">
            <div class="btn-icon-custom btn-chevron-right"><span class="btn-label"><?php echo lang('widget_more'); ?></span><span class="btn-icon"><svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" /></svg></span></div>
        </a>
    </div>
</div>
<?php } ?>

<?php if(in_array('5', $dashboard_widget)){ ?>
<div class="col-xl-3 col-dashboard-btm col-dashboard-btm-right col-dashboard-total-spending">
    <div class="col-dashboard col-vert-flex">
        <div class="col-dashboard-top-content">
            <div class="sub-col-title"><?php echo lang('widget_total_expenditure'); ?></div>
            <div class="sub-col-content">
                <div class="sub-two-rows-flex">
                    <div class="sub-two-rows-top">
                        <div class="sub-two-rows-val">
						<?php
							$total_spending = 0.00;
								foreach($spending_this_month as $spend){
									// Get total amount of closed task
									if($spend['status'] == 'closed'){
										$total_spending += $spend['budget'];
									}
									// Get total amount of disputed amount
									if($spend['status'] == 'disputed'){
										$income_percentage = (100 - $spend['rejection_rate']) / 100;
										$total_spending += $spend['budget'] * $income_percentage;
									}
									
								}
						
						?>
						
						<?php echo ($total_spending == 0) ? 'RM0.00':'RM'.number_format($total_spending,2); ?>
						
						</div>
                        <div class="sub-two-rows-desc"><?php echo lang('widget_total_earnings_this_month_so_far'); ?></div>
                    </div>
                    <div class="sub-two-rows-btm">
                        <div class="sub-two-rows-val">
						<?php
							$total_last_spending = 0;
								foreach($spending_last_month as $spend){
									// Get total amount of closed task
									if($spend['status'] == 'closed'){
										$total_last_spending += round($spend['budget'],2);
									}
									// Get total amount of disputed amount
									if($spend['status'] == 'disputed'){
										$income_percentage = (100 - $spend['rejection_rate']) / 100;
										$total_last_spending += round($spend['budget'],2) * $income_percentage;
									}
									
								}
						
						?>
						
						<?php echo ($total_last_spending == 0) ? 'RM0.00':'RM'.number_format($total_last_spending,2); ?>
						</div>
                        <div class="sub-two-rows-desc"><?php echo lang('widget_total_earnings_previous_month'); ?></div>
                    </div>
                </div>
            </div>
        </div>
        <a href="<?php echo url_for("workspace/billings/spent") ?>" class="btn-icon-link gtm-widget-spending">
            <div class="btn-icon-custom btn-chevron-right"><span class="btn-label"><?php echo lang('widget_more'); ?></span><span class="btn-icon"><svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" /></svg></span></div>
        </a>
    </div>
</div>
<?php } ?>

<?php if(in_array('6', $dashboard_widget)){ ?>
<div class="col-xl-3 col-dashboard-btm col-dashboard-btm-left-mid col-urgent-task">
    <div class="col-dashboard-soft-red col-vert-flex">
        <div class="col-dashboard-top-content">
            <div class="sub-col-title"><?php echo lang('widget_urgent_task'); ?></div>
			<?php if($urgent_task){ ?>
            <div class="sub-col-content">
                <div id="swiper_curated_tasks" class="swiper-container sub-col-rows">
                    <div class="swiper-wrapper">
						<?php foreach($urgent_task as $urgent){ ?>
                        <div class="swiper-slide sub-col-row">
	                        <a href="<?php echo url_for("workspace/task/{$urgent['slug']}/details") ?>" class="gtm-widget-urgent-task-link">
                            <div class="sub-col-swiper-title"><?php echo $urgent['title']; ?></div>
	                        </a>
                            <div class="sub-col-swiper-subtitle"><span class="highlighted"><?php echo ($urgent['hours'] == 0 ) ? lang('dashboard_widget_daily') : $urgent['hours'].'-'.($urgent['hours'] == 1 ? lang('task_hour'):lang('task_hours'));?></span> <?php echo lang('widget_task_applied_for_task_pays'); ?> <span class="highlighted"><?php echo 'RM'.number_format($urgent['budget']); ?></span></div>
                            <div class="sub-col-swiper-desc"><?php echo $urgent['description']; ?></div>
                        </div>
						<?php } ?>
                    </div>
                    <div id="swiper_pagination_curated_tasks" class="swiper-pagination"></div>
                </div>
            </div>
			<?php }else{ ?>
			<div class="sub-col-content empty-state">
				<div class="empty-state-container">
					<div class="empty-img"><img src="assets/img/mtp-no-data-2.png" alt=""></div>
					<span class="empty-lbl"><?php echo lang('widget_no_task_title'); ?></span>
				</div>
			</div>
			<?php } ?>
        </div>
    </div>
</div>
<?php } ?>

<?php if(in_array('7', $dashboard_widget)){ ?>
<div class="col-xl-3 col-dashboard-btm col-dashboard-btm-left-mid col-dashboard-ongoingtask">
    <div class="col-dashboard col-vert-flex">
        <div class="col-dashboard-top-content">
            <div class="sub-col-title"><?php echo lang('widget_ongoing_task_title'); ?></div>
			<?php if($ongoing_task){ ?>
            <div class="sub-col-content">
                <div id="swiper_ongoing_tasks" class="swiper-container sub-col-rows">
                    <div class="swiper-wrapper">
						<?php foreach($ongoing_task as $index => $ongoing){ ?>
                        <div class="swiper-slide sub-col-row">
	                        <a href="<?php echo url_for("workspace/task/{$ongoing['slug']}/details") ?>" class="gtm-widget-ongoing-task-link">
                            <div class="sub-col-swiper-title"><?php echo $ongoing['title']; ?></div>
	                        </a>
                            <div class="sub-col-swiper-subtitle"><span class="highlighted"><?php echo ($ongoing['hours'] == 0 ) ? lang('dashboard_widget_daily') : $ongoing['hours'].'-'.($ongoing['hours'] == 1 ? lang('task_hour'):lang('task_hours'));?></span> <?php echo lang('widget_task_applied_for_task_pays'); ?> <span class="highlighted"><?php echo 'RM'.number_format($ongoing['budget']); ?></span></div>
                            <div class="sub-col-swiper-desc">
	                            <?php
                                $days  = \Carbon\Carbon::parse($ongoing['complete_by'])->diffInDays(\Carbon\Carbon::parse($ongoing['start_by']));
                                $today = \Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::parse($ongoing['start_by']));
	                            ?>
                                <div id="progress_ongoingtask_<?php echo ($index+1) ?>" class="progressbar-chart progress_"
                                     data-percentage="<?php echo ($today > $days || in_array($ongoing['status'], ['completed', 'accepted', 'rejected']) ) ? 100 : ($today/$days) * 100 ?>">

                                </div>
                            </div>
                        </div>
						<?php } ?>
                    </div>
                    <div id="swiper_pagination_ongoing_tasks" class="swiper-pagination"></div>
                </div>
            </div>
			<?php }else{ ?>
			<div class="sub-col-content empty-state">
				<div class="empty-state-container">
					<div class="empty-img"><img src="assets/img/mtp-no-data-2.png" alt=""></div>
					<span class="empty-lbl"><?php echo lang('widget_no_task_title'); ?></span>
				</div>
			</div>			
			<?php } ?>
        </div>
    </div>
</div>

	<script>
        window.addEventListener('load', function(){
            var swiperTaskCreated = new Swiper('#swiper_ongoing_tasks', {
                pagination: {
                    el: '#swiper_pagination_ongoing_tasks',
                    type: 'bullets',
                },
                loop: true,
                speed: 1000
            });

            // progressbar.js@1.0.0 version is used
            // Docs: https://progressbarjs.readthedocs.org/en/1.0.0/
            var progressBars = $('.progress_');
            progressBars.each(function(idx, progressbar){
                var percentage = $(progressbar).data('percentage');
                var color = (percentage >= 100) ? '#00c3bc' : ( percentage <= 25 ? '#655f93' : '#007cf0' );
                new ProgressBar.Line(progressbar, {
                    strokeWidth: 1,
                    easing: 'easeInOut',
                    duration: 1400,
                    color: color,
                    trailColor: '#eee',
                    trailWidth: 1,
                    svgStyle: {
                        width: '100%',
                        height: '100%'
                    },
                    text: {
                        style: {
                            // Text color.
                            // Default: same as stroke color (options.color)
                        },
                        autoStyleContainer: false
                    },
                    step: (state, bar) => {
                        bar.setText(Math.round(bar.value() * 100) + ' %');
                    }
                }).animate(percentage/100); // Number from 0.0 to 1.0
            });
        });
	</script>
	<!-- Progress Bar Function -->
<?php } ?>

<?php if(in_array('8', $dashboard_widget)){ ?>
<div class="col-xl-3 col-dashboard-btm col-dashboard-btm-left-mid col-dashboard-task-created">
    <div class="col-dashboard col-vert-flex">
        <div class="col-dashboard-top-content">
            <div class="sub-col-title"><?php echo lang('widget_task_posted'); ?></div>
			<?php if($created_task){ ?>
            <div class="sub-col-content">
                <div id="swiper_tasks_created" class="swiper-container sub-col-rows">
                    <div class="swiper-wrapper">
						<?php foreach($created_task as $created){ ?>
                        <div class="swiper-slide sub-col-row">
	                        <a href="<?php echo url_for("workspace/task/{$created['slug']}/details") ?>" class="gtm-widget-created-task-link">
                            <div class="sub-col-swiper-title"><?php echo $created['title']; ?></div>
	                        </a>
                            <div class="sub-col-swiper-subtitle"><span class="highlighted"><?php echo ($created['hours'] == 0 ) ? lang('dashboard_widget_daily') : $created['hours'].'-'.($created['hours'] == 1 ? lang('task_hour'):lang('task_hours'));?></span> <?php echo lang('widget_task_applied_for_task_pays'); ?> <span class="highlighted"><?php echo 'RM'.number_format($created['budget']); ?></span></div>
                            <div class="sub-col-swiper-desc"><?php echo lang('widget_task_you_posted_this_on'); ?> <?php echo date($cms->settings()['date_format'], strtotime($created['created_at'])); ?></div>
                            <div class="sub-col-swiper-desc"><?php echo lang('widget_task_is_currently_in_progress'); ?> <?php echo date($cms->settings()['date_format'], strtotime($created['complete_by'])); ?></div>
                            <div class="sub-col-swiper-desc-link"><?php echo lang('widget_in_progress'); ?></div>
                        </div>
						<?php } ?>
                    </div>
                    <div id="swiper_pagination_tasks_created" class="swiper-pagination"></div>
                </div>
            </div>
			<?php }else{ ?>
			<div class="sub-col-content empty-state">
				<div class="empty-state-container">
					<div class="empty-img"><img src="assets/img/mtp-no-data-2.png" alt=""></div>
					<span class="empty-lbl"><?php echo lang('widget_no_task_title'); ?></span>
				</div>
			</div>
			<?php } ?>
        </div>
    </div>
</div>
<!-- Initialize Swiper -->
<script>
    window.addEventListener('load', function(){
        var swiperTaskCreated = new Swiper('#swiper_tasks_created', {
            pagination: {
                el: '#swiper_pagination_tasks_created',
                type: 'bullets',
            },
            loop: true,
            speed: 1000
        });
    });
</script>
<!-- Initialize Swiper -->
<?php } ?>

<?php if(in_array('9', $dashboard_widget)){ ?>
<div class="col-xl-3 col-dashboard-btm col-dashboard-btm-left-mid col-dashboard-previous-posted-task">
    <div class="col-dashboard col-vert-flex">
        <div class="col-dashboard-top-content">
            <div class="sub-col-title"><?php echo lang('widget_task_closed_title'); ?></div>
			<?php if($prev_post_task){ ?>
            <div class="sub-col-content">
                <div id="swiper_previously_posted_task" class="swiper-container sub-col-rows">
                    <div class="swiper-wrapper">
						<?php foreach($prev_post_task as $prev_post){ ?>
                        <div class="swiper-slide sub-col-row">
	                        <a href="<?php echo url_for("workspace/task/{$prev_post['slug']}/details") ?>" class="gtm-widget-previousposted-task-link">
                            <div class="sub-col-swiper-title"><?php echo $prev_post['title']; ?></div>
	                        </a>
                            <div class="sub-col-swiper-subtitle"><span class="highlighted"><?php echo ($prev_post['hours'] == 0 ) ? lang('dashboard_widget_daily') : $prev_post['hours'].'-'.($prev_post['hours'] == 1 ? lang('task_hour'):lang('task_hours'));?></span> <?php echo lang('widget_task_applied_for_task_pays'); ?> <span class="highlighted"><?php echo 'RM'.number_format($prev_post['budget']); ?></span></div>
                            <div class="sub-col-swiper-desc"><?php echo lang('widget_task_you_posted_this_on'); ?> <?php echo date($cms->settings()['date_format'], strtotime($prev_post['created_at'])); ?></div>
                            <div class="sub-col-swiper-desc"><?php echo lang('widget_task_was_closed_on'); ?> <?php echo date($cms->settings()['date_format'], strtotime($prev_post['complete_by'])); ?></div>
                            <div class="sub-col-swiper-desc-link"><?php echo lang('widget_closed'); ?></div>
                            <?php /*echo ($prev_post['status'] == 'in-progress' ? "In progress" : ucfirst($prev_post['status'])); */?>
                        </div>
						<?php } ?>
                    </div>
                    <div id="swiper_pagination_previously_posted_task" class="swiper-pagination"></div>
                </div>
            </div>
			<?php }else{ ?>
			<div class="sub-col-content empty-state">
				<div class="empty-state-container">
					<div class="empty-img"><img src="assets/img/mtp-no-data-2.png" alt=""></div>
					<span class="empty-lbl"><?php echo lang('widget_no_task_title'); ?></span>
				</div>
			</div>
			<?php } ?>
        </div>
    </div>
</div>
<!-- Initialize Swiper -->
<script>
    window.addEventListener('load', function(){
        var swiperPrevious = new Swiper('#swiper_previously_posted_task', {
            pagination: {
                el: '#swiper_pagination_previously_posted_task',
                type: 'bullets',
            },
            loop: true,
            speed: 1000
        });
    });
</script>
<!-- Initialize Swiper -->
<?php } ?>

<?php if(in_array('10', $dashboard_widget)){ ?>
<div class="col-xl-3 col-dashboard-btm col-dashboard-btm-left col-dashboard-new-job">
    <div class="col-dashboard col-vert-flex">
        <div class="col-dashboard-top-content">
            <div class="sub-col-title"><?php echo lang('widget_full_time_work_recommendation_title'); ?></div>
			<?php if($new_job_recommendations){ ?>
            <div class="sub-col-content">
                <div class="sub-col-content-flex">
                    <?php 
                        shuffle($new_job_recommendations);
                        $i = 1;
                        foreach($new_job_recommendations as $job){
                            if($i <= 3){
                    ?>
                    <div class="sub-col-card-row">
                        <div class="sub-col-card-row-flex">
                            <div class="sub-col-card-row-left">
                                <a href="<?php echo url_for('/workspace/job/' . $job['slug'] . '/details'); ?>" class="gtm-widget-new-job-link">
                                    <div class="sub-col-card-row-left-val">
                                        <?php echo $job['title']; ?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php $i++; }} ?>
                </div>
            </div>
			<?php }else{ ?>
			<div class="sub-col-content empty-state">
				<div class="empty-state-container">
					<div class="empty-img"><img src="assets/img/mtp-no-data-2.png" alt=""></div>
					<span class="empty-lbl"><?php echo lang('widget_no_job_title'); ?></span>
				</div>
			</div>
			<?php } ?>
        </div>
    </div>
</div>
<?php } ?>

<?php if(in_array('11', $dashboard_widget)){ ?>
<div class="col-xl-3 col-dashboard-btm col-dashboard-btm-left-mid col-dashboard-job-created">
    <div class="col-dashboard col-vert-flex">
        <div class="col-dashboard-top-content">
            <div class="sub-col-title"><?php echo lang('widget_full_time_position_posted_title'); ?></div>
			
			<?php if($job_created){ ?>
            <div class="sub-col-content">
                <div id="swiper_job_created" class="swiper-container sub-col-rows">
                    <div class="swiper-wrapper">
						<?php foreach($job_created as $job){ 
						//Break date format
						$created = explode(" ",$job['created_at']);
						?>
                        <div class="swiper-slide sub-col-row">
                            <div class="sub-col-swiper-title"><?php echo $job['title']; ?></div>
                            <div class="sub-col-swiper-subtitle"><span class="highlighted"><?php echo lang('widget_full_time_position_posted_salary_range'); ?> : </span> RM<?php echo number_format($job['salary_range_min']) ?> - RM<?php echo number_format($job['salary_range_max']) ?></div>
                            <div class="sub-col-swiper-subtitle"><span class="highlighted"><?php echo lang('widget_full_time_position_posted_job_location'); ?> : </span> <?php echo $job['state_name'] ?></div>
                            <div class="sub-col-swiper-desc"><?php echo lang('widget_full_time_position_posted_you_created_this_job_on'); ?> <?php echo date($cms->settings()['date_format'], strtotime($job['created_at'])) ?></div>
                            <div class="sub-col-swiper-desc"><?php echo lang('widget_full_time_position_posted_job_in_progress'); ?> <?php echo date($cms->settings()['date_format'], strtotime($job['closed_at'])); ?></div>
                            <div class="sub-col-swiper-desc-link"><?php echo lang('widget_in_progress'); ?></div>
                            <?php /*echo ucfirst($job['status']); */?>
                        </div>
						<?php } ?>
                    </div>
                    <div id="swiper_pagination_job_created" class="swiper-pagination"></div>
                </div>
            </div>
			<?php }else{ ?>
			<div class="sub-col-content empty-state">
				<div class="empty-state-container">
					<div class="empty-img"><img src="assets/img/mtp-no-data-2.png" alt=""></div>
					<span class="empty-lbl"><?php echo lang('widget_no_job_title'); ?></span>
				</div>
			</div>
			<?php } ?>
        </div>
    </div>
</div>
<!-- Initialize Swiper -->
<script>
    window.addEventListener('load', function(){
        var swiperJobCreated = new Swiper('#swiper_job_created', {
            pagination: {
                el: '#swiper_pagination_job_created',
                type: 'bullets',
            },
            loop: true,
            speed: 1000
        });
    });
</script>
<!-- Initialize Swiper -->
<?php } ?>
