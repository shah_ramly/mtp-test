<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-chart">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-12 col-header-filter">
	                        <h2 class="card-title-new card-title-back"><a href="javascript: window.history.back()" class="breadcrumbs-link breadcrumbs-link-title">&lt;</a><?php echo lang('talent_profile_details'); ?></h2>
	                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
		                        <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			                        <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
		                        </svg>
	                        </button>
                            <?php echo partial('workspace/partial/header.html.php'); ?>
                        </div>
                    </div>
                </div>
                <div class="card-body-companyprofile card-body-profiledetails">
					<ul class="nav nav-pills nav-pills-profiledetails">
						<li class="nav-item">
                            <a href="javascript: window.history.back()" class="breadcrumbs-link breadcrumbs-link-title">&lt;</a>
							<a class="nav-link active"><?php echo lang('talent_profile_details'); ?></a>
						</li>
					</ul>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="row">
                                <div class="col-xl-12 col-report-dashboard-top">
                                    <div class="col-talent-details-wrapper">
                                        <div class="row">
                                            <div class="col-talent-details-top-wrapper">
                                                <div class="col-left-card">
                                                    <div class="col-personal-info-card-container">
                                                        <div class="talent-details-personal-info-flex">
                                                            <div class="talent-details-avatar-container">
                                                                <div class="profile-avatar">
                                                                    <img class="avatar-img" src="<?php echo $talent['photo'] ?>" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="talent-details-personal-info">
                                                                <h2 class="talent-details-name"><?php echo "{$talent['firstname']} {$talent['lastname']}" ?></h2>
                                                                <div class="talent-details-profile-location">
                                                                    <span class="talent-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                            <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                                                        </svg></span>
                                                                    <span class="talent-details-profile-location-val">
	                                                                    <?php echo ($talent['state'] && $talent['country']) ? "{$talent['state']}, {$talent['country']}" : 'N/A' ?>
                                                                    </span>
                                                                </div>
                                                                <div class="task-row-rating-flex">
                                                                    <div class="task-row-rating-star">
                                                                        <?php $starred = (int)floor($talent['rating']['overall']/2); $rest = 5 - $starred; ?>
                                                                        <?php if($starred > 0): ?>
                                                                            <?php foreach (range(1, $starred) as $star): ?>
			                                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                        <?php if($rest > 0): ?>
                                                                            <?php foreach (range(1, $rest) as $star1): ?>
			                                                                    <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                    <div class="task-row-rating-val">
                                                                        <span class="task-row-average-rating-val">
	                                                                        <?php echo number_format((float)$talent['rating']['overall'], 1); ?>
                                                                        </span>
                                                                        <span class="task-row-divider">/</span>
                                                                        <span class="task-row-total-rating-val">
	                                                                        <?php echo $talent['rating']['count'] . " " . lang('talent_reviews'); ?>
                                                                        </span>
                                                                    </div>
                                                                </div>
																<div class="task-row-profile-dispute">
                                                                    <span class="task-details-profile-val"><?php  echo lang('task_details_dispute_resolution');?> </span>
                                                                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="top"
                                                                        title="<?php echo lang('this_user_has_adopted_dispute_resolution_for') .' 0 '. lang('dispute_resolution_out_of') .' 0 '. lang('dispute_resolution_projects_undertaken') ?>">
                                                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                            <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                                        </svg>
                                                                    </span>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-right-card">
                                                    <div class="col-more-details-container">
                                                        <div class="col-activity-info-card-container">
                                                            <div class="talent-details-right-panel-row talent-more-details">
                                                                <label class="task-details-lbl" data-toggle="collapse" data-target="#talentMoreDetails" aria-expanded="false" aria-controls="taskDetailsAll">
                                                                    <i class="fa fa-caret-down"></i>
                                                                    <span><?php echo lang('talent_activity'); ?></span>
                                                                </label>
                                                                <div class="collapse show" id="talentMoreDetails">
                                                                    <div class="talent-details-expanded">
                                                                        <div class="talent-extra-info talent-total-completed-task">
                                                                            <span class="talent-profile-label"><?php echo lang('talent_completed_task'); ?>:</span>
                                                                            <span class="talent-profile-val">
                                                                                <?php echo $talent['info']['completed'] ?>
                                                                            </span>
                                                                        </div>
                                                                        <div class="talent-extra-info  talent-total-ongoing-task">
                                                                            <span class="talent-profile-label"><?php echo lang('talent_ongoing_task'); ?>:</span>
                                                                            <span class="talent-profile-val">
                                                                                <?php echo $talent['info']['on_going'] ?>
                                                                            </span>
                                                                        </div>
                                                                        <div class="talent-extra-info  talent-total-completed-hour">
                                                                            <span class="talent-profile-label"><?php echo lang('talent_total_hours_completed'); ?>:</span>
                                                                            <span class="talent-profile-val">
                                                                                <?php echo (int)$talent['info']['hours_completed'] ?>
                                                                            </span>
                                                                        </div>
                                                                        <div class="talent-extra-info talent-last-login">
                                                                            <span class="talent-profile-label"><?php echo lang('talent_last_login'); ?>:</span>
                                                                            <span class="talent-profile-val">
                                                                                <?php echo $talent['info']['last_login'] ?>
                                                                            </span>
                                                                        </div>
                                                                        <div class="talent-extra-info talent-member-since">
                                                                            <span class="talent-profile-label"><?php echo lang('talent_member_since'); ?>:</span>
                                                                            <span class="talent-profile-val"><?php echo $talent['member_since'] ?></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php if( !empty($talent['skills']) ): ?>
                                                            <div class="talent-details-right-panel-row talent-row-skills">
                                                                <label class="task-details-lbl" data-toggle="collapse" data-target="#talentProSkills" aria-expanded="false" aria-controls="taskDetailsAll">
                                                                    <i class="fa fa-caret-down"></i>
                                                                    <span><?php echo lang('talent_professional_skills'); ?></span>
                                                                </label>
                                                                <div class="collapse" id="talentProSkills">
                                                                    <div class="talent-details-expanded">
                                                                        <div class="task-details-deadline-block">
                                                                            <?php foreach ($talent['skills'] as $skill): ?>
                                                                            <span class="skills-label-link"><span class="tag label label-info bubble-lbl"><?php echo $skill ?></span></span>
                                                                            <?php endforeach ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php endif ?>
                                                            <?php if( !empty($talent['languages']) ): ?>
                                                            <div class="talent-details-right-panel-row col-talent-details-language-container">
                                                                <label class="task-details-lbl" data-toggle="collapse" data-target="#talentPrefLang" aria-expanded="false" aria-controls="taskDetailsAll">
                                                                    <i class="fa fa-caret-down"></i>
                                                                    <span><?php echo lang('talent_preferred_language'); ?></span>
                                                                </label>
                                                                <div class="collapse" id="talentPrefLang">
                                                                    <div class="task-details-deadline-block">
                                                                        <?php foreach ($talent['languages'] as $talent_language): ?>
                                                                        <span class="skills-label-link">
                                                                            <span class="tag label label-info bubble-lbl">
                                                                                <?php echo $talent_language; ?>
                                                                            </span>
                                                                        </span>
                                                                        <?php endforeach; ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php endif ?>
                                                            <!--div class="talent-history">
                                                                <label class="talent-details-lbl">Information</label>
                                                                <div class="talent-extra-info talent-last-login">
                                                                    <span class="talent-profile-label">Last login:</span>
                                                                    <span class="talent-profile-val">
                                                                        <?php echo $talent['info']['last_login'] ?>
                                                                    </span>
                                                                </div>
                                                                <div class="talent-extra-info talent-extra-info talent-member-since">
                                                                    <span class="talent-profile-label">Member since:</span>
                                                                    <span class="talent-profile-val">
                                                                        <?php echo $talent['member_since'] ?>
                                                                    </span>
                                                                </div>
                                                            </div-->
                                                        </div>
                                                        <div class="task-row-report-user">
                                                            <div class="task-report-user-flex">
                                                                <button class="report-user-link gtm-report-task" data-report="<?php echo $talent['m_id'] ?>" data-select="Report User" data-toggle="modal" data-target="#modal_feedback">Report User</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-talent-details-btm-wrapper">
                                                <div class="col-left-card">
                                                    <div class="col-talent-details-aboutme-container">
                                                        <label class="talent-details-lbl"><?php echo lang('talent_personal_statement'); ?></label>
                                                        <div class="talent-details-about">
                                                            <?php 
                                                                if($talent['about']){
                                                                    echo $talent['about'];
                                                                }else{ 
                                                            ?>
                                                            <div class="empty-state-container">
                                                                <div class="empty-img"><img src="<?php echo imgCrop('assets/img/mtp-no-data-2.png'); ?>" alt=""></div>
                                                                <span class="empty-lbl"><?php echo lang('talent_no_data_to_display'); ?></span>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-talent-details-workxp-container">
                                                        <label class="talent-details-lbl"><?php echo lang('talent_work_experience'); ?></label>
                                                        <div class="talent-details-workxp-rows">
                                                            <?php if(!empty($talent['employments'])){ ?>
	                                                        <?php foreach ($talent['employments'] as $employment): ?>
                                                            <div class="talent-details-workxp-row">
                                                                <div class="talent-details-workxp-compname">
	                                                                <?php echo $employment['company_name'] ?>
                                                                </div>
                                                                <div class="talent-details-workxp-pos-duration">
                                                                    <div class="talent-details-workxp-pos">
	                                                                    <?php echo $employment['position'] ?>
                                                                    </div>
                                                                </div>
                                                                <div class="talent-details-workxp-pos-duration">
                                                                    <div class="talent-details-workxp-location">
                                                                        <?php echo $employment['location'] ?>
                                                                    </div>
                                                                    <div class="talent-details-workxp-duration">
	                                                                    <?php echo $employment['duration'] ?>
                                                                    </div>
                                                                </div>
	                                                            <div class="talent-details-workxp-resp job-desc">
                                                                <span class="talent-details-workxp-list-lbl"><?php echo lang('talent_job_descriptions'); ?></span>
		                                                            <?php echo $employment['job_desc'] ?>
	                                                            </div>
                                                                <div class="talent-details-workxp-resp">
	                                                                <span class="talent-details-workxp-list-lbl"><?php echo lang('talent_responsibilities'); ?></span>
                                                                    <?php echo $employment['job_responsibilities'] ?>
                                                                </div>
                                                            </div>
	                                                        <?php endforeach; ?>
                                                            <?php }else{ ?>
                                                            <div class="empty-state-container">
                                                                <div class="empty-img"><img src="<?php echo imgCrop('assets/img/mtp-no-data-2.png'); ?>" alt=""></div>
                                                                <span class="empty-lbl"><?php echo lang('talent_no_data_to_display'); ?></span>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-talent-details-edu-container">
                                                        <label class="talent-details-lbl"><?php echo lang('talent_education'); ?></label>
                                                        <div class="talent-details-edu-rows">
                                                            <?php if(!empty($talent['educations'])){ ?>
	                                                        <?php foreach ($talent['educations'] as $education): ?>
                                                            <div class="talent-details-edu-row">
                                                                <div class="talent-details-edu-insname">
	                                                                <?php echo $education['edu_institution'] ?>
                                                                </div>
                                                                <div class="talent-details-edu-level-fieldstudy-grade">
                                                                    <div class="talent-details-edu-level">
                                                                        <?php echo $education['highest_edu_level'] ?>
                                                                    </div>
                                                                    <div class="talent-details-edu-fieldstudy">
	                                                                    <?php echo "{$education['field']}, {$education['major']}" ?>
                                                                    </div>
                                                                    <div class="talent-details-edu-grade">
	                                                                    <?php echo $education['grade'] ?>
                                                                    </div>
                                                                </div>
                                                                <div class="talent-details-edu-location-year">
                                                                    <div class="talent-details-edu-location">
	                                                                    <?php echo "{$education['state']}, {$education['country']}" ?>
                                                                    </div>
                                                                    <div class="talent-details-edu-year">
	                                                                    <?php echo $education['grad_year'] ?>
                                                                    </div>
                                                                </div>
                                                            </div>
	                                                        <?php endforeach ?>
                                                            <?php }else{ ?>
                                                            <div class="empty-state-container">
                                                                <div class="empty-img"><img src="<?php echo imgCrop('assets/img/mtp-no-data-2.png'); ?>" alt=""></div>
                                                                <span class="empty-lbl"><?php echo lang('talent_no_data_to_display'); ?></span>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-rating-list-container">
                                                        <label class="talent-details-lbl"><?php echo lang('talent_individual_ratings_and_feedback'); ?></label>
	                                                    <?php if ( !empty($talent['reviews']) ){ ?>
                                                        <ul class="col-rating-list">
	                                                        <?php foreach($talent['reviews'] as $reviewer): ?>
	                                                        <li>
		                                                        <div class="col-xl-12 col-ratings-bottom">
			                                                        <div class="col-rating-first-bottom-group">
				                                                        <div class="col-rating-user-info">
					                                                        <div class="rating-avatar">
						                                                        <a href="#" class="rating-photo-link">
							                                                        <div class="col-rating-photo">
								                                                        <img src="<?php echo $reviewer['photo'] ?>" alt="Profile Photo">
							                                                        </div>
						                                                        </a>
					                                                        </div>
					                                                        <a href="#" class="rating-name-link">
                                                                                <?php echo is_null($reviewer['company_name']) ? trim($reviewer['firstname']) . ' ' . trim($reviewer['lastname']) : $reviewer['company_name'] ?>
					                                                        </a>
					                                                        <span class="task-details-comment-date">
						                                                        <?php echo \Carbon\Carbon::parse($reviewer['created_at'])->diffForHumans() ?>
					                                                        </span>
				                                                        </div>
				                                                        <div class="bottom-rating-description"><?php echo $reviewer['review'] ?></div>
			                                                        </div>
			                                                        <div class="col-bottom-rating-stars-task">
				                                                        <div class="rating-star-average-group">
					                                                        <div class="task-row-rating-star">
                                                                                <?php
                                                                                $starred = (int)floor($reviewer['total_rating'] /2);
                                                                                $rest = 5 - $starred;
                                                                                ?>
                                                                                <?php if($starred > 0): ?>
                                                                                    <?php foreach (range(1, $starred) as $star): ?>
								                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                    <?php endforeach; ?>
                                                                                <?php endif; ?>
                                                                                <?php if($rest > 0): ?>
                                                                                    <?php foreach (range(1, $rest) as $star1): ?>
								                                                        <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                                                    <?php endforeach; ?>
                                                                                <?php endif; ?>
					                                                        </div>
					                                                        <div class="task-row-rating-val">
						                                                        <span class="task-row-average-rating-val"><?php echo $reviewer['total_rating'] ?></span>
					                                                        </div>
				                                                        </div>
			                                                        </div>
		                                                        </div>
	                                                        </li>
	                                                        <?php endforeach ?>
                                                        </ul>
                                                        <?php }else{ ?>
                                                        <div class="empty-state-container">
                                                            <div class="empty-img"><img src="<?php echo imgCrop('assets/img/mtp-no-data-2.png'); ?>" alt=""></div>
                                                            <span class="empty-lbl"><?php echo lang('talent_no_data_to_display'); ?></span>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="task-row-report-user task-row-report-user-mobile">
                                                        <div class="task-report-user-flex">
                                                            <button class="report-user-link gtm-report-task" data-report="<?php echo $talent['m_id'] ?>" data-select="Report User" data-toggle="modal" data-target="#modal_feedback"><?php echo lang('report_user'); ?></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-right-card">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>