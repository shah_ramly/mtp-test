<div class="col-talent-row-container">
    <div class="talent-row-col-top-flex">
        <div class="talent-row-col-left">
            <div class="talent-personal-info-container">
                <div class="talent-avatar-container">
                    <div class="profile-avatar">
                        <a href="<?php echo url_for("/talent/{$talent['number']}") ?>" class="talent-avatar-link gtm-talent-details">
	                        <img class="avatar-img" src="<?php echo $talent['photo'] ?>" alt="">
                        </a>
                    </div>
                </div>
                <div class="talent-personal-info">
                    <a href="<?php echo url_for("/talent/{$talent['number']}") ?>" class="talent-name-link gtm-talent-details">
                        <h5 class="profile-name"><?php echo ucwords(strtolower($talent['firstname'] . ' ' . $talent['lastname'])); ?></h5>
                    </a>
                    <div class="talent-details-profile-location">
                        <span class="talent-details-profile-location-icon">
	                        <svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor"
	                             xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z">
                                </path>
                            </svg>
                        </span>
                        <span class="talent-details-profile-location-val">
	                        <?php echo ($talent['state'] && $talent['country']) ? "{$talent['state']}, {$talent['country']}" : 'N/A' ?>
                        </span>
                    </div>
                    <div class="task-row-rating-flex">
                        <div class="task-row-rating-star">
                            <?php $starred = (int)floor($talent['rating']['overall']/2); $rest = 5 - $starred; ?>
                            <?php if($starred > 0): ?>
                                <?php foreach (range(1, $starred) as $star): ?>
			                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php if($rest > 0): ?>
                                <?php foreach (range(1, $rest) as $star1): ?>
			                        <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <div class="task-row-rating-val">
                            <span class="task-row-average-rating-val">
	                            <?php echo number_format((float)$talent['rating']['overall'], 1); ?>
                            </span>
                            <span class="task-row-divider">/</span>
                            <span class="task-row-total-rating-val">
	                            <?php echo $talent['rating']['count'] . ' ' . ((int)$talent['rating']['count'] === 1 ? lang('task_details_review') : lang('task_details_reviews')); ?>
                            </span>
                        </div>
                    </div>
                    <div class="talent-total-completed-task-hour">
                        <div class="talent-total-completed-task-hour-flex">
                            <span class="col-taskcount">
                                <span class="col-taskcount-icon bi-tooltip" data-toggle="tooltip" data-placement="auto" data-template="<div class='tooltip tooltip-fancy tooltip-tc-date' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>" data-title="<?php echo lang('talent_completed_task'); ?>" data-original-title="" title="">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-card-checklist" viewBox="0 0 16 16">
                                        <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z" />
                                        <path d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0zM7 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z" />
                                    </svg>
                                </span>
                                <span class="col-taskcount-val"><?php echo $talent['info']['completed'] ?></span>
                            </span>
                            <span class="col-taskhours">
                                <span class="col-taskhours-icon bi-tooltip" data-toggle="tooltip" data-placement="auto" data-template="<div class='tooltip tooltip-fancy tooltip-tc-date' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>" data-title="<?php echo lang('talent_total_hours_completed'); ?>" data-original-title="" title="">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-clock" viewBox="0 0 16 16">
                                        <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z" />
                                        <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z" />
                                    </svg>
                                </span>
                                <span class="col-taskhours-val"><?php echo (int)$talent['info']['hours_completed'] ?></span>
                            </span>
                        </div>
                        <button type="button" class="btn-icon-full btn-view-profile btn-icon-view-profile gtm-talent-details" onclick="location.href='<?php echo url_for("/talent/{$talent['number']}") ?>';">
                            <span class="btn-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-arrow-right-circle-fill" viewBox="0 0 16 16">
                                    <path d="M8 0a8 8 0 1 1 0 16A8 8 0 0 1 8 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z" /></svg>
                            </span>
                        </button>
                    </div>

                </div>
            </div>
            
            <!-- <div class="talent-row-skills">
                <?php if( !empty($talent['skills']) ): ?>
		            <div class="task-details-deadline-block">
                        <?php foreach ($talent['skills_list'] as $skill): ?>
				            <a href="<?php echo url_for('workspace/search') . "?scope=talent&skills=" . $skill['id'] ?>" class="skills-label-link">
					            <span class="tag label label-info bubble-lbl"><?php echo $skill['name'] ?></span>
				            </a>
                        <?php endforeach ?>
		            </div>
                <?php endif ?>
            </div> -->
        </div>
    </div>
</div>