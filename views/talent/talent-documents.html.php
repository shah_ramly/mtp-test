<div class="form-flex py-2">
    <div class="input-group-block row">
        <label class="input-lbl input-lbl-block">
            <span class="input-label-txt"><?php echo $title ?></span>
        </label>
        <div class="uploads">
            <ul class="my-docs ">
            <?php foreach($docs as $i => $file):
                 $src = explode('/', $file);
                 $filename = end($src); ?>
                 <li><a href="<?php echo url_for($file) ?>" target="_blank" title="<?php echo $filename; ?>"><?php echo ($i+1); ?></a></li>
            <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>