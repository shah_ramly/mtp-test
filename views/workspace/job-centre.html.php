<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-chart">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-12 col-header-filter">
                            <h2 class="card-title"><?php echo 'Job Centre' ?></h2>
                            <?php echo partial('workspace/partial/header.html.php'); ?>
                        </div>
                    </div>
                </div>
                <div class="card-body-taskcentre">
                    <ul class="nav nav-pills nav-pills-taskcentre" id="tcTabList" role="tablist">
                        <li class="nav-item-arrow nav-item-arrow-prev">
                            <a class="nav-link nav-link-arrow nav-link-prev" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M15 18l-6-6 6-6" /></svg></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="tcAll-tab" data-toggle="pill" href="#tcAll" role="tab" aria-controls="tcAll" aria-selected="true"><?php echo lang('job_all_tab_title') ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tcPending-tab" data-toggle="pill" href="#tcPending" role="tab" aria-controls="tcPending" aria-selected="false"><?php echo lang('job_applied_tab_title') ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tcClosed-tab" data-toggle="pill" href="#tcClosed" role="tab" aria-controls="tcClosed" aria-selected="false"><?php echo lang('job_closed_tab_title') ?></a>
                        </li>
                        <li class="nav-item-arrow nav-item-arrow-next">
                            <a class="nav-link nav-link-arrow nav-link-next" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M9 18l6-6-6-6" /></svg></a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
	                    <?php echo partial("/workspace/partial/job-centre/{$type}/all.html.php", ['jobs' => $jobs['all']]) ?>
	                    <?php echo partial("/workspace/partial/job-centre/{$type}/pending.html.php", ['jobs' => array_merge($jobs['draft'], $jobs['published'])]) ?>
	                    <?php echo partial("/workspace/partial/job-centre/{$type}/closed.html.php", ['jobs' => $jobs['closed']]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php content_for('modals') ?>

	<!-- Modal - OD Confirm Delete Job -->
	<div id="modal_confirm_delete_ft" class="modal modal-confirm fade" aria-labelledby="modal_confirm_delete_ft" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p class="modal-para"><?php echo lang('job_are_you_sure_you_want_to') ?> <span class="text-danger font-weight-bold"><?php echo strtoupper(lang('job_delete')) ?></span> <?php echo lang('job_delete_this_job') ?>?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
						<span class="btn-label"><?php echo lang('post_discard_cancel') ?></span>
						<span class="btn-icon">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                                <line x1="18" y1="6" x2="6" y2="18"></line>
	                                <line x1="6" y1="6" x2="18" y2="18"></line>
	                            </svg>
	                        </span>
					</button>
					<form method="post" action="<?php echo url_for('/workspace/job/delete') ?>">
	                    <?php echo html_form_token_field(); ?>
						<input type="hidden" name="job_id" value="" />
						<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
							<span class="btn-label"><?php echo lang('post_discard_confirm') ?></span>
							<span class="btn-icon">
		                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
		                                <polyline points="20 6 9 17 4 12"></polyline>
		                            </svg>
		                        </span>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->

	<!-- Modal - OD Confirm Cancel Job -->
	<div id="modal_confirm_cancel_ft" class="modal modal-confirm fade" aria-labelledby="modal_confirm_cancel_ft" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p class="modal-para"><?php echo lang('job_are_you_sure_you_want_to') ?> <span class="text-warning font-weight-bold"><?php echo strtoupper(lang('job_cancel')) ?></span> <?php echo lang('job_delete_this_job') ?>?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
						<span class="btn-label"><?php echo lang('post_discard_cancel') ?></span>
						<span class="btn-icon">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                                <line x1="18" y1="6" x2="6" y2="18"></line>
	                                <line x1="6" y1="6" x2="18" y2="18"></line>
	                            </svg>
	                        </span>
					</button>
					<form method="post" action="<?php echo url_for('/workspace/job/cancel') ?>">
	                    <?php echo html_form_token_field(); ?>
						<input type="hidden" name="job_id" value="" />
						<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
							<span class="btn-label"><?php echo lang('post_discard_confirm') ?></span>
							<span class="btn-icon">
		                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
		                                <polyline points="20 6 9 17 4 12"></polyline>
		                            </svg>
		                        </span>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->
<?php end_content_for() ?>

<?php content_for('scripts') ?>
<script>
    $('#modal_confirm_delete_ft').on('show.bs.modal', function (event) {
        var link = $(event.relatedTarget);
        var job_id = link.data('jobId');
        var modal = $(this);
        modal.find('input[name="job_id"]').val(job_id);
        modal.find('.btn-confirm').on('click', function(e){
            modal.find('form')[0].submit();
        });
    });

    $('#modal_confirm_cancel_ft').on('show.bs.modal', function (event) {
        var link = $(event.relatedTarget);
        var job_id = link.data('jobId');
        var modal = $(this);
        modal.find('input[name="job_id"]').val(job_id);
        modal.find('.btn-confirm').on('click', function(e){
            modal.find('form')[0].submit();
        });
    });

    $('#post_job_ft').on('show.bs.modal', function (event) {
        var link = $(event.relatedTarget);
        var job_id = link.data('jobId');
        var modal = $(this);
        modal.find('.summernote').each(function() {
            $(this).summernote({
                tooltip: false,
                placeholder: $(this).attr('placeholder'),
                toolbar: [
                    ['style', ['bold', 'italic', 'underline']],
                    ['para', ['ul', 'ol']],
                ],
                height: 100,
                width: '100%',
            });
        });
        if(job_id){
            var url = '<?php echo option('site_uri') . url_for("/workspace/job/job_id/details") ?>';
            url = url.replace("job_id", job_id);
            $.ajax({
                url: url
            }).done(function(response){

                var job = response.job;
                var questions = job.questions[Object.keys(job.questions)[0]] || [];
                var skills = job.skills;

                modal.find('#form_post_job_ft').prepend('<input type="hidden" name="job_id" value="' + job.job_id + '" />');
                modal.find('#ft_category').val(job.category).trigger('change');
                modal.find('#ft_subcategory').on("categoryUpdated", function(e){
                    $(this).val(job.sub_category);
                });

                modal.find('#ft_jobcountry').val(job.country_id).trigger('change');
                modal.find('#ft_jobstate').on("countryUpdated", function(e){
                    $(this).val(job.state_id);
                })

                modal.find('#ft_jobtitle').val(job.title);
                modal.find('#ft_jobdesc').val(job.description);
                modal.find('#ft_jobrequirement').val(job.requirements);
                modal.find('#ft_jobxp').val(job.experience_level);
                modal.find('#ft_joblocation').val(job.location);
                modal.find('#ft_joblocation').data('editJob', true);
                modal.find('#ft_jobdesc').summernote('code', job.description);
                modal.find('#ft_jobrequirement').summernote('code', job.requirements);

                if(window.range_budget_salary !== undefined) {
                    window.range_budget_salary.setValue([parseInt(job.salary_range_min, 10), parseInt(job.salary_range_max, 10)]);
                }

                var close_date = moment(job.closed_at);
                if(close_date.isBefore(moment()) || close_date.isSame(moment()))
                    close_date = false;
                else
                    close_date = close_date.format("DD-MMM-YYYY");

				modal.find('#form_datetime_closing').datetimepicker('clear');
                modal.find('input[name="ft_close_date"]').val(close_date || '');
                modal.find('#form_datetime_closing').datetimepicker({
                    defaultDate: close_date
                });

                questions.forEach(function(i, index){
                    modal.find('input[name*="ft_qna[]"]').last().val(i.name);
                    if(index != questions.length - 1){
                        modal.find('.ft-add-button').trigger('click');
                    }
                });

                modal.find('input[name*="ft_jobbenefits[]"]').each(function(idx, benefit){
	                if( job.benefits.indexOf(benefit.value) > -1 ){
	                    benefit.checked = true;
	                }
                });

                skills.forEach(function(skill, idx){
                    $('#tag2').tagsinput('add', skill);
                });

            });
        }else{
            if(window.range_budget_salary !== undefined) {
                window.range_budget_salary.setValue([3500, 4200]);
            }
        }
    });
</script>
<?php end_content_for() ?>