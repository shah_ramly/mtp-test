<div class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-chart">
				<div class="card-header">
					<div class="row">
						<div class="col-sm-12 col-header-filter">
							<div class="logo-sm">
								<a href="<?php echo url_for('dashboard') ?>" class="logo-icon-link">
									<span class="logo-icon-sm">
										<img src="<?php echo url_for('/assets/img/logoicon.png') ?>" alt="" />
                                    </span>
								</a>
							</div>
							<h2 class="card-title"><?php echo lang('menu_task_centre'); ?></h2>
							<?php echo partial('workspace/partial/filter.html.php'); ?>
							<?php echo partial('workspace/partial/header.html.php'); ?>
						</div>
					</div>
				</div>
                <?php //echo partial('partial/search.html.php') ?>
				<?php echo partial('workspace/partial/task-centre.html.php') ?>
			</div>
		</div>
	</div>
</div>