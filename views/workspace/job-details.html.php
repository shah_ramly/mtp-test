<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-chart">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-12 col-header-filter">
                            <h2 class="card-title-new card-title-back">
                                <a href="javascript: window.history.back()" class="breadcrumbs-link breadcrumbs-link-title">&lt;</a> <?php echo lang('job_details_title'); ?></h2>
                            <!-- <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
                                <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                </svg>
                            </button> -->
                            <?php echo partial('workspace/partial/header.html.php'); ?>
                        </div>
                    </div>
                </div>
	            <?php if(!isset($job['external'])): ?>
                <?php echo partial('workspace/partial/job-details-body.html.php') ?>
	            <?php else: ?>
                <?php echo partial('workspace/partial/external-job-details-body.html.php') ?>
	            <?php endif ?>
            </div>
        </div>
    </div>
</div>

<?php content_for('extra_scripts'); ?>
<script>
	$('.apply-external').on('click', function(e){
		var job_id = $(e.currentTarget).data('jobId');
		var form_token = $(e.currentTarget).data('formToken');
		if( job_id ){
		    $.ajax({
			    url: '<?php echo option('site_uri') . url_for('/workspace/job/apply') ?>',
			    method: 'POST',
			    data: {'job_id': job_id, 'form_token': form_token, 'external': 'external'}
		    })
		}
	})
</script>
<?php end_content_for() ?>
