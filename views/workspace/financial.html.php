<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-chart">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-12 col-header-filter">
                            <h2 class="card-title"><?php echo lang('menu_financial'); ?></h2>
                            <?php echo partial('workspace/partial/header.html.php'); ?>
                        </div>
                    </div>
                </div>
	            <div class="card-body-financial">
		            <ul class="nav nav-pills nav-pills-financial" id="pills-tabFinancial" role="tablist">
						<?php if((int)$user->info['type'] === 0): ?>
			            <li class="nav-item">
				            <a class="nav-link <?php echo $current_page === 'earning' ? 'active' : '' ?>" id="pills-earning-tab" href="<?php echo url_for('/workspace/billings/earning') ?>" role="tab" aria-controls="pills-rc-myreviews" aria-selected="true">
					            <?php echo lang('my_earnings_tab_title') ?>
				            </a>
			            </li>
						<?php endif ?>
			            <li class="nav-item">
				            <a class="nav-link <?php echo $current_page === 'spent' ? 'active' : '' ?>" id="pills-spent-tab" href="<?php echo url_for('/workspace/billings/spent') ?>" role="tab" aria-controls="pills-rc-writereview" aria-selected="false">
					            <?php echo lang('my_expenditure_tab_title') ?>

				            </a>
			            </li>
		            </ul>
		            <div class="tab-content" id="pills-tabFinancialContent">
			            <div class="tab-pane fade <?php echo $current_page === 'earning' ? 'show active' : '' ?>" id="pills-earning" role="tabpanel">
				            <div class="col-xl-12 col-financial-container">
					            <?php if($current_page === 'earning'): ?>
					            <?php echo partial($current_tab) ?>
					            <?php endif ?>
				            </div>
			            </div>
			            <div class="tab-pane fade  <?php echo $current_page === 'spent' ? 'show active' : '' ?>" id="pills-spent" role="tabpanel">
				            <div class="col-xl-12 col-financial-container">
                                <?php if($current_page === 'spent'): ?>
                                <?php echo partial($current_tab) ?>
					            <?php endif ?>
				            </div>
			            </div>
		            </div>
	            </div>
            </div>
        </div>
    </div>
</div>