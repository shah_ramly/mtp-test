<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-chart">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-12 col-header-filter">
                            <h2 class="card-title"><?php echo lang('rating_title_my_rating_centre'); ?></h2>
                            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
                                <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                </svg>
                            </button>
                            <?php echo partial('workspace/partial/header.html.php'); ?>
                        </div>
                    </div>
                </div>
                <div class="card-body-ratingcentre">
	                <ul class="nav nav-pills nav-pills-ratingcentre" id="pills-tabRatingCentre" role="tablist">
		                <li class="nav-item">
			                <a class="nav-link active gtm-rc-myreviews" id="pills-rc-myreviews-tab" data-toggle="pill" href="#pills-rc-myreviews" role="tab" aria-controls="pills-rc-myreviews" aria-selected="true"><?php echo lang('rating_ratings_received'); ?></a>
		                </li>
		                <li class="nav-item">
			                <a class="nav-link gtm-rc-postedreviews" id="pills-rc-writereview-tab" data-toggle="pill" href="#pills-rc-writereview" role="tab" aria-controls="pills-rc-writereview" aria-selected="false"><?php echo lang('rating_ratings_posted'); ?></a>
		                </li>
		                <li class="nav-item">
			                <a class="nav-link gtm-rc-writereviews" id="pills-rc-writereview-tab" data-toggle="modal" data-target="#modal_write_review" href="#"><?php echo lang('rating_post_a_rating'); ?></a>
		                </li>
	                </ul>
	                <div class="tab-content" id="pills-tabRatingCentreContent">
		                <div class="tab-pane fade show active" id="pills-rc-myreviews" role="tabpanel">
                            <?php echo partial('workspace/partial/rating-centre/my-reviews.html.php') ?>
		                </div>
		                <div class="tab-pane fade show" id="pills-rc-writereview" role="tabpanel">
                            <?php echo partial('workspace/partial/rating-centre/write-review.html.php') ?>
		                </div>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php content_for('modals'); ?>
<?php echo partial('workspace/partial/rating-centre/review-modal.html.php',
	['posted' => $projects['posted'], 'performed' => $projects['performed']]); ?>
<?php end_content_for() ?>

<?php content_for('scripts') ?>
<?php echo partial('workspace/partial/rating-centre/scripts.html.php') ?>
<?php end_content_for() ?>
