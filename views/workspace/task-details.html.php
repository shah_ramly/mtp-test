<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-chart">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-12 col-header-filter">
	                        <div class="logo-sm">
		                        <a href="<?php echo url_for('dashboard') ?>" class="logo-icon-link">
			                        <span class="logo-icon-sm"><img src="<?php echo url_for('/assets/img/logoicon.png') ?>" alt="" /></span>
		                        </a>
	                        </div>
	                        <h2 class="card-title-new card-title-back">
		                        <a href="javascript: window.history.back()" class="breadcrumbs-link breadcrumbs-link-title">&lt;</a> <?php echo lang('task_details_title'); ?></h2>
                            <!--<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
                                <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                </svg>
                            </button>-->
                            <?php echo partial('workspace/partial/header.html.php'); ?>
                        </div>
                    </div>
                </div>
                <?php if(!isset($task['external'])): ?>
                    <?php echo partial('workspace/partial/task-details-body.html.php') ?>
                <?php else: ?>
                    <?php echo partial('workspace/partial/external-task-details-body.html.php') ?>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>

<?php content_for('extra_scripts'); ?>
<script>
    $('.apply-external-task').on('click', function(e){
        var target = $(e.currentTarget);
        var task_id = target.data('taskId');
        var form_token = target.data('formToken');
        if( task_id ){
            $.ajax({
                url: '<?php echo option('site_uri') . url_for('/workspace/task/apply') ?>',
                method: 'POST',
                data: {'task_id': task_id, 'form_token': form_token, 'external': 'external'}
            }).done(function(){
                target.attr('href', '#');
                target.find('button').attr('disabled', 'disabled');
            })
        }
    });

    $('.action-hide').on('click', function(e){
        e.preventDefault();
        var target = $(e.currentTarget);
        var comment = target.data('commentId');
        var post = target.data('postId');
        var type = target.data('postType');
        var state = target.data('updateState');
        target.addClass('disabled-link');
        $.ajax({
	        method: 'POST',
	        url: '<?php echo option('site_uri') . url_for('/workspace/comments/hide') ?>',
	        data: { 'comment_id': comment, 'post_id': post, 'type': type, 'state': state }
        }).done(function(response){
            target.removeClass('disabled-link');
            target.parent().find('.action-reply').toggleClass('hide');
	        if( response.status === 'success' ){
                if( target.hasClass('is-hidden') ){
                    target.addClass('hide');
                    $('a.is-not-hidden[data-comment-id="'+comment+'"]').removeClass('hide');
                }else{
                    target.addClass('hide');
                    $('a.is-hidden[data-comment-id="'+comment+'"]').removeClass('hide');
                }
                t(response.status === 'success' ? 's' : 'e', response.message, response.title);
	        }
        });
    });
</script>
<?php end_content_for(); ?>