<?php

$colors = [
    'gold-card', // 'gold'
    'green-card', // 'green'
    'red-card', // 'red'
    'orange-card', // 'orange'
    'maroon-card', // 'maroon'
    'purple-card', // 'purple'
    'cyan-card', // 'cyan'
    'blue-card' // 'blue'
];

?>
<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-chart">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-12 col-header-filter">
                            <h2 class="card-title"><?php echo lang('fav_title'); ?></h2>
                            <?php echo partial('workspace/partial/header.html.php'); ?>
                        </div>
                    </div>
                </div>
	            <div class="card-body-favourite">
					<ul class="nav nav-pills nav-pills-favourite" id="pills-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active"  id="favTaskTab-tab" data-toggle="pill" href="#favTaskTab" role="tab" aria-controls="favTaskTab" aria-selected="true"><?php echo lang('fav_task_title_my'); ?> <span class="heart-icon">❤</span> <?php echo lang('fav_task_title_task'); ?></a>
						</li>
                        <li class="nav-item">
							<a class="nav-link"  id="favTaskJob-tab" data-toggle="pill" href="#favTaskJob" role="tab" aria-controls="favTaskJob" aria-selected="false"><?php echo lang('fav_job_title_my'); ?> <span class="heart-icon">❤</span> <?php echo lang('fav_job_title_job'); ?></a>
						</li>
					</ul>
                    <div class="tab-content" id="pills-tabFavorite">
		                <div class="tab-pane fade show active" id="favTaskTab" role="tabpanel">
                            <div class="col-xl-12 col-tasklisting-top col-tasklisting-fav">
                                <div class="active-fav-header-flex">
                                    <button class="btn-filter-search btn-fav-expired" data-toggle="collapse"
                                            data-target="#activeFavTask" aria-expanded="true"
                                            aria-controls="activeFavTask">
	                                    <span class="title-collapse"><?php echo lang('current_favourites_title'); ?></span>
	                                    <span class="expired-total">(<?php echo $total['active_tasks'] ?>)</span><i class="fa fa-caret-down"></i>
                                    </button>
                                </div>
                                <div class="collapse show" id="activeFavTask">
                                    <div class="col-task-listing-wrapper col-task-tiles-wrapper">
                                        <div class="row">
                                                <div class="col-xl-12 col-task-row-wrapper-my-favourite">
                                                    <?php if(!empty($all['active_tasks'])): ?>
                                                    <?php set('url', request_uri()); ?>
	                                                <?php $counter = 0; ?>
                                                    <?php foreach ($favourites_list as $favourite): ?>
                                                    <?php if( !isset($favourite['task_id']) || !in_array($favourite['task_id'], array_column($all['active_tasks'], 'task_id')) ) continue; ?>
                                                    <div class="col-my-favourite-wrapper <?php echo $colors[$counter] ?>">
                                                        <?php
                                                        set('task', $favourite);
                                                        set('favourites', ['ids' => $favourites]);
                                                        set('tasks_applied', $applied);
                                                        if( $favourite['type'] === 'internal_task' )
                                                            echo partial('workspace/partial/task-listing/task.html.php');
                                                        else
                                                            echo partial('workspace/partial/task-listing/external_task.html.php');
                                                        ?>
                                                    </div>
	                                                    <?php $counter++;
                                                            if( $counter > (count($colors) - 1) ) $counter = 0; ?>
                                                    <?php endforeach; ?>
                                                    <?php else: ?>
                                                    <div class="col-my-favourite-wrapper empty">
                                                        <div class="col-task-row-container">
                                                            <div class="task-row-col-top-flex job">
                                                                <div class="empty-state-container">
                                                                    <div class="empty-img"><img src="<?php echo url_for("/assets/img/mtp-no-data-2.png") ?>" alt=""></div>
                                                                    <span class="empty-lbl"><?php echo lang('fav_no_task'); ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if($total['active_tasks_more']): ?>
                                    <input type="hidden" name="active-tasks-page" id="active-tasks-page" value="<?php echo $current_page['active_tasks']+1 ?>" />
                                    <button class="btn-icon-full btn-load-more more-favourites"
                                            data-section="active_tasks"
                                            data-page="<?php echo $current_page['active_tasks']+1 ?>"
                                            data-text="<?php echo lang('search_load_more_loading') ?>">
                                            <span class="btn-label" id="loadMoreLbl" data-text="<?php echo lang('public_search_load_more'); ?>"><?php echo lang('public_search_load_more'); ?></span>
                                            <span class="btn-icon load-plus-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                                            </span>
                                            <span class="btn-icon load-load-icon" style="display:none;">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg>
                                            </span>
                                    </button>
                                    <button class="btn-icon-full btn-load-more back-to-top hide" data-text="<?php echo lang('search_load_more_loading') ?>">
                                            <span class="btn-label" id="loadMoreLbl" data-text="<?php echo lang('public_search_load_more'); ?>"><span><?php echo lang('end_of_list'); ?></span> - <?php echo lang('back_to_top'); ?></span>
                                            <span class="btn-icon arrowtop-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-arrow-up-short" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5z"/>
                                                </svg>
                                            </span>
                                            <span class="btn-icon load-load-icon" style="display:none;">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg>
                                            </span>
                                        </button>
                                    <?php endif ?>
                                </div>
                            </div>
                            <div class="col-xl-12 col-expired-fav">
                                <div class="expired-fav-header-flex">
                                    <button class="btn-filter-search btn-fav-expired" data-toggle="collapse"
                                            data-target="#expiredFavTask" aria-expanded="true"
                                            aria-controls="expiredFavTask">
	                                    <span class="title-collapse"><?php echo lang('expired_favourites_title'); ?></span>
	                                    <span class="expired-total">(<?php echo $total['inactive_tasks'] ?>)</span><i class="fa fa-caret-down"></i>
                                    </button>
	                                <form method="post" action="<?php echo url_for('workspace/task/remove-expired-favourites') ?>">
                                        <?php echo html_form_token_field() ?>
		                                <button type="submit" class="btn-icon-full btn-unheart" id="unheart_btn" <?php echo (int)$total['inactive_tasks'] === 0 ? 'disabled' : '' ?>>
			                                <span class="btn-lbl"><?php echo lang('fav_unheart_all'); ?></span>
		                                </button>
	                                </form>
                                </div>
                                <div class="collapse show" id="expiredFavTask">
                                    <div class="col-tasklisting-top col-tasklisting-fav">
                                        <div class="col-task-listing-wrapper col-task-tiles-wrapper">
                                            <div class="row">
                                                <div class="col-xl-12 col-task-row-wrapper-my-favourite">
                                                    <?php if(!empty($all['inactive_tasks'])): ?>
                                                        <?php set('url', request_uri()); ?>
                                                        <?php foreach ($favourites_list as $favourite): ?>
                                                            <?php if( !isset($favourite['task_id']) || !in_array($favourite['task_id'], array_column($all['inactive_tasks'], 'task_id')) ) continue; ?>
                                                    <div class="col-my-favourite-wrapper">
                                                        <?php
                                                        set('task', $favourite);
                                                        set('favourites', ['ids' => $favourites]);
                                                        set('tasks_applied', $applied);
                                                        if( $favourite['type'] === 'internal_task' )
                                                            echo partial('workspace/partial/task-listing/task.html.php');
                                                        else
                                                            echo partial('workspace/partial/task-listing/external_task.html.php');
                                                        ?>
                                                    </div>
                                                    <?php endforeach; ?>
                                                    <?php else: ?>
                                                    <div class="col-my-favourite-wrapper empty">
                                                        <div class="col-task-row-container">
                                                            <div class="task-row-col-top-flex job">
                                                                <div class="empty-state-container">
                                                                    <div class="empty-img"><img src="<?php echo url_for("/assets/img/mtp-no-data-2.png") ?>" alt=""></div>
                                                                    <span class="empty-lbl"><?php echo lang('fav_no_task'); ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php endif ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($total['inactive_tasks_more']): ?>
	                                    <input type="hidden" name="inactive-tasks-page" id="inactive-tasks-page" value="<?php echo $current_page['inactive_tasks']+1 ?>" />
                                        <button class="btn-icon-full btn-load-more more-favourites"
                                                data-section="inactive_tasks"
                                                data-page="<?php echo $current_page['inactive_tasks']+1 ?>"
                                                data-text="<?php echo lang('search_load_more_loading') ?>">
                                            <span class="btn-label" id="loadMoreLbl" data-text="<?php echo lang('public_search_load_more'); ?>"><?php echo lang('public_search_load_more'); ?></span>
                                            <span class="btn-icon load-plus-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                                            </span>
                                            <span class="btn-icon load-load-icon" style="display:none;">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg>
                                            </span>
                                        </button>
                                        <button class="btn-icon-full btn-load-more back-to-top hide" data-text="<?php echo lang('search_load_more_loading') ?>">
                                            <span class="btn-label" id="loadMoreLbl" data-text="<?php echo lang('public_search_load_more'); ?>"><span><?php echo lang('end_of_list'); ?></span> - <?php echo lang('back_to_top'); ?></span>
                                            <span class="btn-icon arrowtop-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-arrow-up-short" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5z"/>
                                                </svg>
                                            </span>
                                            <span class="btn-icon load-load-icon" style="display:none;">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg>
                                            </span>
                                        </button>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade show" id="favTaskJob" role="tabpanel">
                            <div class="col-xl-12 col-tasklisting-top col-tasklisting-fav">
                                <div class="active-fav-header-flex">
                                    <button class="btn-filter-search btn-fav-expired" data-toggle="collapse"
                                            data-target="#activeFavJob" aria-expanded="true"
                                            aria-controls="activeFavJob">
	                                    <span class="title-collapse"><?php echo lang('current_favourites_title'); ?></span>
	                                    <span class="expired-total">(<?php echo $total['active_jobs'] ?>)</span><i class="fa fa-caret-down"></i>
                                    </button>
                                </div>
                                <div class="collapse show" id="activeFavJob">
                                    <div class="col-task-listing-wrapper col-task-tiles-wrapper">
                                            <div class="row">
                                                <div class="col-xl-12 col-task-row-wrapper-my-favourite">
                                                    <?php if(!empty($all['active_jobs'])): ?>
                                                        <?php set('url', request_uri()); ?>
                                                        <?php foreach ($favourites_list as $favourite): ?>
                                                            <?php if( !isset($favourite['job_id']) || !in_array($favourite['job_id'], array_column($all['active_jobs'], 'task_id')) ) continue; ?>
                                                    <div class="col-my-favourite-wrapper">
                                                        <?php
                                                        set('job', $favourite);
                                                        set('favourites', ['ids' => $favourites]);
                                                        set('jobs_applied', $applied);
                                                        if( $favourite['type'] === 'internal_job' )
                                                            echo partial('workspace/partial/task-listing/job.html.php');
                                                        else
                                                            echo partial('workspace/partial/task-listing/external_job.html.php');
                                                        ?>
                                                    </div>
                                                    <?php endforeach; ?>
                                                    <?php else: ?>
                                                    <div class="col-my-favourite-wrapper empty">
                                                        <div class="col-task-row-container">
                                                            <div class="task-row-col-top-flex job">
                                                                <div class="empty-state-container">
                                                                    <div class="empty-img"><img src="<?php echo url_for("/assets/img/mtp-no-data-2.png") ?>" alt=""></div>
                                                                    <span class="empty-lbl"><?php echo lang('fav_no_task'); ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php endif ?>
                                                </div>
                                            </div>
                                    </div>
                                    <?php if($total['active_jobs_more']): ?>
                                    <input type="hidden" name="active-jobs-page" id="active-jobs-page" value="<?php echo $current_page['active_jobs']+1 ?>" />
                                    <button class="btn-icon-full btn-load-more more-favourites"
                                            data-section="active_jobs"
                                            data-page="<?php echo $current_page['active_jobs']+1 ?>"
                                            data-text="<?php echo lang('search_load_more_loading') ?>">
                                            <span class="btn-label" id="loadMoreLbl" data-text="<?php echo lang('public_search_load_more'); ?>"><?php echo lang('public_search_load_more'); ?></span>
                                            <span class="btn-icon load-plus-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                                            </span>
                                            <span class="btn-icon load-load-icon" style="display:none;">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg>
                                            </span>
                                    </button>
                                    <button class="btn-icon-full btn-load-more back-to-top hide" data-text="<?php echo lang('search_load_more_loading') ?>">
                                            <span class="btn-label" id="loadMoreLbl" data-text="<?php echo lang('public_search_load_more'); ?>"><span><?php echo lang('end_of_list') ?></span> - <?php echo lang('back_to_top') ?></span>
                                            <span class="btn-icon arrowtop-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-arrow-up-short" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5z"/>
                                                </svg>
                                            </span>
                                            <span class="btn-icon load-load-icon" style="display:none;">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg>
                                            </span>
                                        </button>
                                    <?php endif ?>
                                </div>
                            </div>
                            <div class="col-xl-12 col-expired-fav">
                                <div class="expired-fav-header-flex">
                                    <button class="btn-filter-search btn-fav-expired" data-toggle="collapse"
                                            data-target="#expiredFavJob" aria-expanded="true"
                                            aria-controls="expiredFavJob">
	                                    <span class="title-collapse"><?php echo lang('expired_favourites_title'); ?></span>
	                                    <span class="expired-total">(<?php echo $total['inactive_jobs'] ?>)</span><i class="fa fa-caret-down"></i>
                                    </button>
	                                <form method="post" action="<?php echo url_for('workspace/job/remove-expired-favourites') ?>">
		                                <?php echo html_form_token_field() ?>
	                                    <button type="submit" class="btn-icon-full btn-unheart" id="unheart_btn" <?php echo (int)$total['inactive_jobs'] === 0 ? 'disabled' : '' ?>>
	                                        <span class="btn-lbl"><?php echo lang('fav_unheart_all'); ?></span>
	                                    </button>
	                                </form>
                                </div>
                                <div class="collapse show" id="expiredFavJob">
                                    <div class="col-tasklisting-top col-tasklisting-fav">
                                        <div class="col-task-listing-wrapper col-task-tiles-wrapper">
                                            <div class="row">
                                                <div class="col-xl-12 col-task-row-wrapper-my-favourite">
                                                    <?php if(!empty($all['inactive_jobs'])): ?>
                                                        <?php set('url', request_uri()); ?>
                                                        <?php foreach ($favourites_list as $favourite): ?>
                                                            <?php if( !isset($favourite['job_id']) || !in_array($favourite['job_id'], array_column($all['inactive_jobs'], 'task_id')) ) continue; ?>
                                                    <div class="col-my-favourite-wrapper">
                                                        <?php
                                                        set('job', $favourite);
                                                        set('favourites', ['ids' => $favourites]);
                                                        set('jobs_applied', $applied);
                                                        if( $favourite['type'] === 'internal_job' )
                                                            echo partial('workspace/partial/task-listing/job.html.php');
                                                        else
                                                            echo partial('workspace/partial/task-listing/external_job.html.php');
                                                        ?>
                                                    </div>
                                                    <?php endforeach; ?>
                                                    <?php else: ?>
                                                    <div class="col-my-favourite-wrapper empty">
                                                        <div class="col-task-row-container">
                                                            <div class="task-row-col-top-flex job">
                                                                <div class="empty-state-container">
                                                                    <div class="empty-img"><img src="<?php echo url_for("/assets/img/mtp-no-data-2.png") ?>" alt=""></div>
                                                                    <span class="empty-lbl"><?php echo lang('fav_no_task'); ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php endif ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($total['inactive_jobs_more']): ?>
                                        <input type="hidden" name="inactive-jobs-page" id="inactive-jobs-page" value="<?php echo $current_page['inactive_jobs']+1 ?>" />
                                        <button class="btn-icon-full btn-load-more more-favourites"
                                                data-section="inactive_jobs"
                                                data-page="<?php echo $current_page['inactive_jobs']+1 ?>"
                                                data-text="<?php echo lang('search_load_more_loading') ?>">
                                            <span class="btn-label" id="loadMoreLbl" data-text="<?php echo lang('public_search_load_more'); ?>"><?php echo lang('public_search_load_more'); ?></span>
                                            <span class="btn-icon load-plus-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                                            </span>
                                            <span class="btn-icon load-load-icon" style="display:none;">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg>
                                            </span>
                                        </button>
                                        <button class="btn-icon-full btn-load-more back-to-top hide" data-text="<?php echo lang('search_load_more_loading') ?>">
                                            <span class="btn-label" id="loadMoreLbl" data-text="<?php echo lang('public_search_load_more'); ?>"><span><?php echo lang('end_of_list'); ?></span> - <?php echo lang('back_to_top'); ?></span>
                                            <span class="btn-icon arrowtop-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-arrow-up-short" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5z"/>
                                                </svg>
                                            </span>
                                            <span class="btn-icon load-load-icon" style="display:none;">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg>
                                            </span>
                                        </button>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div id="modal_remove_fav" class="modal modal-confirm modal-remove-fav fade" aria-labelledby="modal_remove_fav" tabindex="-1" role="dialog" aria-modal="false">
	<div class="modal-dialog">
		<div class="modal-content">
            <div class="modal-header"><?php echo lang('remove_fav_modal_main_title'); ?></div>
            <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                    <line x1="18" y1="6" x2="6" y2="18"></line>
                    <line x1="6" y1="6" x2="18" y2="18"></line>
                </svg>
            </button>
			<div class="modal-body">
				<p class="modal-para"><?php echo sprintf( lang('do_you_want_to_remove_this_x_from_favourite'), '<span id="type"></span>' ); ?></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
					<span class="btn-label"><?php echo lang('modal_cancel'); ?></span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
				</button>
				<button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal">
					<span class="btn-label"><?php echo lang('modal_confirm'); ?></span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
				</button>
			</div>
		</div>
	</div>
</div>

<?php content_for('extra_scripts'); ?>
<script>
	$('.more-favourites').on('click', function(e){
	   e.preventDefault();
	   var url = '<?php echo option("site_uri") . url_for('/workspace/my-favourite') ?>';
	   var button = $(e.currentTarget);
	   var section = button.data('section');
	   var page = button.data('page');
		button.prop('disabled', true);
	   if( section && page ){
	       $.ajax({
		       method: 'GET',
		       url: url + '?page='+page+'&section='+section
	       }).done(function(response){
	           button.prop('disabled', false);
	           if(response.html.length) {
                   button.attr('data-page', (parseInt(page, 10) + 1)).data('page', (parseInt(page, 10) + 1));
                   button.parent().find('.col-task-row-wrapper-my-favourite').append(response.html);
               }

	           if(!response.more){
	               button.next('button.hide').removeClass('hide');
	               button.addClass('hide');
	           }
	       });
	   }
	});

    $(".btn-load-more.back-to-top").on('click', function(e){
        e.preventDefault();
        $('html')[0].scrollTo({ top: 0, left: 0, behavior: 'smooth'});
    });

    $('.col-task-row-wrapper-my-favourite').on('click', 'button.btn-icon-heart.btn-unfav', function(e){
        e.preventDefault();
        var type = $(e.currentTarget).parent().find('[name="task_id"]').length ? '<?php echo lang('remove_fav_modal_task'); ?>' : '<?php echo lang('remove_fav_modal_job'); ?>';
        $('#modal_remove_fav').find('#type').text(type);
        $('#modal_remove_fav').modal('show', e.currentTarget);
    });

    $('#modal_remove_fav').on('show.bs.modal', function(e){
       var button = e.relatedTarget;
       $(this).find('button.btn-confirm').on('click', function(e){
           e.preventDefault();
           $(button).parent()[0].submit();
       });
    });

    $('#modal_remove_fav').on('hidden.bs.modal', function(e){
        $(this).find('button.btn-confirm').off('click');
    });

</script>
<?php end_content_for(); ?>
