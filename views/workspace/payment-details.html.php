<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-chart">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-12 col-header-filter">
                            <h2 class="card-title-new card-title-back"><a href="javascript: window.history.back()" class="breadcrumbs-link breadcrumbs-link-title">&lt;</a><?php echo lang('task_payment_details_main_title') ?></h2>
                            <?php echo partial('workspace/partial/header.html.php'); ?>
                        </div>
                    </div>
                </div>
                <div class="card-body-payment-details">
					<ul class="nav nav-pills nav-pills-paymentdetails">
						<li class="nav-item">
                            <a href="javascript: window.history.back()" class="breadcrumbs-link breadcrumbs-link-title">&lt;</a>
							<a class="nav-link active"><?php echo lang('task_payment_details_main_title') ?></a>
						</li>
					</ul>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="row">
                                <div class="col-xl-12 col-payment-wrapper">
                                    <div class="col-payment-flex-container">
                                        <div class="col-task-seeker-wrapper">
                                            <div class="row-payment-task-details">
                                                <div class="sub-col-title"><?php echo lang('task_payment_details_payment_for') ?>:</div>
                                                <h3 class="task-details-title"><?php echo $task['title'] ?></h3>
                                                <div class="task-payment-details-row task-details-title-components">
                                                    <div class="task-id-flex">
                                                        <span class="task-row-taskid-lbl"><?php  echo lang('task_id')?>:</span>
                                                        <span class="task-row-taskid-val"><?php echo $task['number'] ?></span>
                                                    </div>
                                                </div>
                                                <div class="task-payment-details-row task-payment-details-date">
                                                    <div class="task-payment-details-row task-payment-details-start-date">
                                                        <label class="task-payment-details-lbl">
                                                            <?php echo lang('task_details_start_date') ?>
                                                            <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo lang('task_details_start_date_tooltip') ?>">
                                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                        <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                                                                    </svg>
                                                                </span>
                                                        </label>
                                                        <div class="task-payment-details-val">
                                                            <?php echo \Carbon\Carbon::parse($task['start_by'])->format($cms->settings()['date_format']); ?>
                                                        </div>
                                                    </div>
                                                    <div class="task-payment-details-row task-payment-details-completion-date">
                                                        <label class="task-payment-details-lbl">
                                                            <?php echo lang('task_details_complete_by') ?>
                                                            <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo lang('task_details_complete_by_tooltip') ?>">
                                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                        <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                                                                    </svg>
                                                                </span>
                                                        </label>
                                                        <div class="task-payment-details-val">
                                                            <?php echo \Carbon\Carbon::parse($task['complete_by'])->format($cms->settings()['date_format']); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="payment-arrow-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 512.19 512.19" style="enable-background:new 0 0 512.19 512.19;" xml:space="preserve"> <path fill="currentColor" d="M295.445,35.381c-4.218-4.112-10.972-4.026-15.084,0.192c-1.921,1.971-3.006,4.608-3.028,7.36 v117.333H10.667C4.776,160.266,0,165.042,0,170.933V341.6c0,5.891,4.776,10.667,10.667,10.667h266.667v116.885 c-0.011,5.891,4.757,10.675,10.648,10.686c2.84,0.005,5.565-1.123,7.571-3.134L509.056,263.37c4.171-4.16,4.179-10.914,0.019-15.085 c-0.006-0.006-0.013-0.013-0.019-0.019L295.445,35.381z"/> </svg>
                                                </span>
                                            </div>
                                            <div class="row-payment-seeker-details">
                                                <div class="sub-col-title"><?php echo lang('task_payment_details_made_to') ?>:</div>
                                                <div class="talent-personal-info-container">
                                                    <div class="talent-avatar-container">
                                                        <div class="profile-avatar">
	                                                        <img class="avatar-img" src="<?php echo imgCrop($talent['photo'], 100, 100, 'assets/img/default-avatar.png'); ?>" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="talent-personal-info">
                                                        <div class="col-talent-personal-info">
                                                            <span class="">
                                                                <h5 class="profile-name">
	                                                                <?php echo "{$talent['firstname']} {$talent['lastname']}" ?>
                                                                </h5>
                                                            </span>
                                                            <div class="talent-details-profile-location">
                                                                <span class="talent-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                        <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                                                    </svg></span>
                                                                <span class="talent-details-profile-location-val">
	                                                                <?php echo "{$talent['state']}, {$talent['country']}" ?>
                                                                </span>
                                                            </div>
                                                            <div class="task-row-rating-flex">
                                                                <div class="task-row-rating-star">
                                                                    <?php $starred = (int)floor($talent['rating']['overall']/2); $rest = 5 - $starred; ?>
                                                                    <?php if($starred > 0): ?>
                                                                        <?php foreach (range(1, $starred) as $star): ?>
		                                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                        <?php endforeach; ?>
                                                                    <?php endif; ?>
                                                                    <?php if($rest > 0): ?>
                                                                        <?php foreach (range(1, $rest) as $star1): ?>
		                                                                    <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                                        <?php endforeach; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="task-row-rating-val">
                                                                    <span class="task-row-average-rating-val">
	                                                                    <?php echo number_format((float)$talent['rating']['overall'], 1); ?>
                                                                    </span>
                                                                    <span class="task-row-divider">/</span>
                                                                    <span class="task-row-total-rating-val">
	                                                                    <?php echo $talent['rating']['count'] .' '. ( $talent['rating']['count'] == 1 ? lang('task_details_review') : lang('task_details_reviews')); ?>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="payment-arrow-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 512.19 512.19" style="enable-background:new 0 0 512.19 512.19;" xml:space="preserve"> <path fill="currentColor" d="M295.445,35.381c-4.218-4.112-10.972-4.026-15.084,0.192c-1.921,1.971-3.006,4.608-3.028,7.36 v117.333H10.667C4.776,160.266,0,165.042,0,170.933V341.6c0,5.891,4.776,10.667,10.667,10.667h266.667v116.885 c-0.011,5.891,4.757,10.675,10.648,10.686c2.84,0.005,5.565-1.123,7.571-3.134L509.056,263.37c4.171-4.16,4.179-10.914,0.019-15.085 c-0.006-0.006-0.013-0.013-0.019-0.019L295.445,35.381z"/> </svg>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-payment-details-wrapper">
                                            <div class="row-payment-details-billing-container">
                                                <div class="sub-col-title"><?php echo lang('task_payment_details_for_the_amount') ?>:</div>
                                                <div class="payment-details-billing">
                                                    <div class="payment-details-subtotal">
                                                        <div class="payment-details-flex-row">
                                                            <span class="payment-details-lbl"><?php echo lang('post_step_4_task_value') ?></span>
                                                            <?php $budget = explode('.', $task['budget']) ?>
                                                            <span class="payment-details-val"><?php echo $cms->price($task['budget']) ?></span>
                                                        </div>
                                                        <div class="payment-details-flex-row">
                                                            <span class="payment-details-lbl"><?php echo lang('task_payment_details_service_fee') ?></span>
                                                            <span class="payment-details-val"><?php echo $cms->price($task['service_charge']); ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="payment-details-total">
                                                        <div class="payment-details-flex-row">
                                                            <span class="payment-details-lbl"><?php echo lang('task_payment_details_total') ?></span>
                                                            <span class="payment-details-val"><?php echo $cms->price($task['budget'] + $task['service_charge']); ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="payment-row-tnc">
                                                    <div class="custom-control custom-checkbox tnc-snp">
                                                        <input type="checkbox" class="custom-control-input" id="tnc_snp_check">
                                                        <label class="custom-control-label" for="tnc_snp_check"><?php echo lang('task_payment_details_tnc_checkbox') ?></span>
                                                    </div>
                                                </div>
	                                            <?php if(!$assigned && !empty($senangpay)): ?>
                                                <div class="payment-row-actions">
	                                                <form action="<?php echo SENANGPAY['payment_url']; ?>" method="post">
                                                        <?php foreach($senangpay as $name => $val){ ?>
                                                        <input type="hidden" name="<?php echo $name; ?>" value="<?php echo $val; ?>">
                                                        <?php } ?>
	                                                    <button type="submit" class="btn-icon-full btn-pay" disabled>
	                                                        <span class="btn-label"><?php echo lang('task_payment_details_pay_now') ?></span>
	                                                        <span class="btn-icon">
	                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
	                                                                <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
	                                                                <polyline points="22 4 12 14.01 9 11.01"></polyline>
	                                                            </svg>
	                                                        </span>
	                                                    </button>
	                                                </form>
                                                </div>
	                                            <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sp-container">
                                        <div class="sp-payment-method">
                                            <div class="sp-payment-partner-available-flex">
                                                <div class="sp-payment-partner">
                                                    <label class="sp-lbl"><?php echo lang('task_payment_details_our_payment_partner') ?></label>
                                                    <div class="sp-logo">
                                                        <img src="<?php echo url_for("/assets/img/logo_senangpay_v3.png") ?>" alt="senangpay">
                                                    </div>
                                                </div>
                                                <div class="sp-payment-available-container">
                                                    <label class="sp-lbl"><?php echo lang('task_payment_details_available_payment_method') ?></label>
                                                    <ul class="sp-payment-method-list">
                                                        <li class="credit">
                                                            <div class="credit-img"><img src="<?php echo url_for("/assets/img/cc-mobile@2x.png") ?>" alt="credit"></div>
                                                        </li>
                                                        <li class="fpx">
                                                            <div class="fpx-img"><img src="<?php echo url_for("/assets/img/fpx-mobile@2x.png") ?>" alt="fpx"></div>
                                                        </li>
                                                        <li class="boost">
                                                            <div class="boost"><img src="<?php echo url_for("/assets/img/boost-mobile@2x.png") ?>" alt="boost"></div>
                                                        </li>
                                                        <li class="tng">
                                                            <div class="tng-img"><img src="<?php echo url_for("/assets/img/tng-mobile@2x.png") ?>" alt="tng"></div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="sp-desc-footer"><?php echo lang('task_payment_details_if_you_have_any_questions') ?> <a href="mailto:query@maketimepay.com">query@maketimepay.com</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
    <?php if($task_payment){ ?>
    <!-- Modal - Payment Modal -->
    <div id="modal_payment_success" class="modal modal-payment-success fade" aria-labelledby="modal_payment_success" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"><?php echo lang('task_payment_details_transaction_' . ($task_payment['status'] == '1' ? 'successful' : 'unsuccessful')) ?></div>
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div id="table-listing-container-activitylog" class="table-listing-container">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td class="info title"><?php echo lang('task_payment_details_transcation_amount') ?></td>
                                    <td class="info"><?php echo $cms->price($task_payment['total'] + $task_payment['service_charge']); ?></td>
                                </tr>
                                <tr>
                                    <td class="info title"><?php echo lang('task_payment_details_transcation_payment_method') ?></td>
                                    <td class="info">SenangPay</td>
                                </tr>
                                <tr>
                                    <td class="info title"><?php echo $task_payment['status'] == '1' ? lang('billing_details_payment_code') : lang('task_payment_details_transaction_error'); ?></th>
                                    <td class="info"><?php echo $task_payment['status'] == '1' ? $task_payment['id'] : $msg; ?></td>
                                </tr>
                                <tr>
                                    <td class="info title"><?php echo lang('task_payment_details_transcation_date') ?></td>
                                    <td class="info"><?php echo date('d/m/Y H:i:s', strtotime($task_payment['date'])); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="<?php echo $task_payment['status'] == '1' ? $redirect : '#'; ?>"<?php echo $task_payment['status'] == '1' ? '' : ' data-dismiss="modal"'; ?>>
                        <button type="button" class="btn-icon-full btn-step-next od-open1">
                            <span class="btn-label"><?php echo $task_payment['status'] == '1' ? lang('task_payment_details_transaction_go_to_inprogress') : lang('task_payment_details_transaction_retry'); ?></span>
                            <span class="btn-icon">
                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                </svg>
                            </span>
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
    

    <!-- Payment Modal on load -->
    <script>
        $(window).on('load', function() {
            $('#modal_payment_success').modal('show');
        });
    </script>
    <?php } ?>