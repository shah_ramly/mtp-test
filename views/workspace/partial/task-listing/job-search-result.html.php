<?php if (!empty($listing)): ?>
	<?php foreach ($listing as $job): ?><?php
        set('job', $job);
        if($job['type'] === 'internal_job'):
            echo partial('workspace/partial/task-listing/job.html.php');
        else:
            echo partial('workspace/partial/task-listing/external_job.html.php');
        endif; ?>
	<?php endforeach; ?>
<?php else: ?>
	<div class="col-task-row-container">
		<div class="task-row-col-top-flex job">
			<div class="empty-state-container">
				<div class="empty-img"><img src="<?php echo url_for('assets/img/mtp-no-data-2.png') ?>" alt=""></div>
				<span class="empty-lbl">No result to display</span>
			</div>
		</div>
	</div>
<?php endif ?>
