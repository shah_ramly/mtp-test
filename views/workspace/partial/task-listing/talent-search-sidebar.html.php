<div class="col-task-filter-container talents-filter">
    <div class="col-talent-filter-more-filter">
        <span class="filter-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                <line x1="4" y1="21" x2="4" y2="14"></line>
                <line x1="4" y1="10" x2="4" y2="3"></line>
                <line x1="12" y1="21" x2="12" y2="12"></line>
                <line x1="12" y1="8" x2="12" y2="3"></line>
                <line x1="20" y1="21" x2="20" y2="16"></line>
                <line x1="20" y1="12" x2="20" y2="3"></line>
                <line x1="1" y1="14" x2="7" y2="14"></line>
                <line x1="9" y1="8" x2="15" y2="8"></line>
                <line x1="17" y1="16" x2="23" y2="16"></line>
            </svg>
        </span>
        <span class="filter-lbl">More filters</span>
    </div>
    <div class="col-talent-filter-rows">
        <div class="task-filter-row task-filter-row-location">
            <button class="btn-link btn-link-filter btn-flex location" type="button" aria-expanded="false">
                <label class="task-filter-lbl">Filter by location</label>
                <span class="chevron-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                        <path d="M6 9l6 6 6-6" /></svg>
                </span>
            </button>
            <div class="form-filter-collapse collapse">
                <div class="form-block-task-filter">
                    <div class="form-filter-state">
                        <select class="form-control form-control-input" name="state" placeholder="State">
	                        <option value="" selected="">State</option>
                            <?php foreach($states as $state): ?>
		                        <option <?php echo request('scope') === 'talent' && isset($filters['state']) && $state['id'] === $filters['state'] ? 'selected' : '' ?>
				                        value="<?php echo $state['id'] ?>"><?php echo $state['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-filter-city">
                        <select class="form-control form-control-input" name="city" placeholder="City">
	                        <option selected="" value="0">Town/City</option>
                            <?php foreach($cities as $city): ?>
		                        <option <?php echo request('scope') === 'talent' && isset($filters['city']) && $city['id'] === $filters['city'] ? 'selected' : '' ?>
				                        value="<?php echo $city['id'] ?>">
                                    <?php echo $city['name'] ?>
		                        </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="task-filter-row task-filter-row-skills">
            <button class="btn-link btn-link-filter btn-flex skillss" type="button" aria-expanded="false">
                <label class="task-filter-lbl">Filter by skills</label>
                <span class="chevron-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                        <path d="M6 9l6 6 6-6" /></svg>
                </span>
            </button>
            <div class="form-filter-collapse collapse">
                <div class="form-block-task-filter">
                    <div class="bstags-skills">
                        <input type="text" name="skills" class="bootstrap-tagsinput" id="talent-skills"
                            <?php if(request('scope') === 'talent' && isset($filters['skills'])):?>
		                        data-input-status="true"
		                        value="<?php echo implode(',', $filters['skills']) ?>"
                            <?php else: ?>
		                        data-input-status="false"
                            <?php endif; ?>
                               placeholder="Enter skills..." />
                    </div>
                </div>
            </div>
        </div>
        <div class="task-filter-row task-filter-row-task-completed">
            <button class="btn-link btn-link-filter btn-flex completed" type="button" aria-expanded="false">
                <label class="task-filter-lbl">Filter by completed task</label>
                <span class="chevron-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                        <path d="M6 9l6 6 6-6" /></svg>
                </span>
            </button>
            <div class="form-filter-collapse collapse">
                <div class="form-block-task-filter">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_1"
                            <?php if(request('scope') === 'talent' && isset($filters['completed']) && $filters['completed'] === 'all'):?>
		                        data-input-status="true"
		                        checked
                            <?php else: ?>
		                        data-input-status="false"
                            <?php endif; ?>
                               value="all" />
                        <label class="custom-control-label" for="filtertaskcompleted_1">All</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_2"
                            <?php if(request('scope') === 'talent' && isset($filters['completed']) && $filters['completed'] === '1-10'):?>
		                        data-input-status="true"
		                        checked
                            <?php else: ?>
		                        data-input-status="false"
                            <?php endif; ?>
                               value="1-10" />
                        <label class="custom-control-label" for="filtertaskcompleted_2">1-10</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_3"
                            <?php if(request('scope') === 'talent' && isset($filters['completed']) && $filters['completed'] === '10+'):?>
		                        data-input-status="true"
		                        checked
                            <?php else: ?>
		                        data-input-status="false"
                            <?php endif; ?>
                               value="10+" />
                        <label class="custom-control-label" for="filtertaskcompleted_3">10+</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="task-filter-row task-filter-row-reset">
            <a href="#" class="reset-filter-link reset-talent-filters">Reset all filters</a>
        </div>
    </div>
</div>
<?php content_for('scripts'); ?>
<script>
	window.addEventListener('load', function(){
        $('input[name="completed"]').on('click', function(e){
            $(e.currentTarget).parent().siblings().find('input[name="completed"]').prop('checked', false);
            $(e.currentTarget).prop('checked', true);
        });


        $('.talents-filter').find('input, select').on('change', function(e){
            $('#talentSearchForm input[name="page"]').val("1");
            $('#talentSearchForm input[name="perform-search"]').val('yes');
            update_talents_filters($(e.currentTarget));
        });

        $('.reset-talent-filters').on('click', function(e){
            e.preventDefault();
            var q = $('#talentSearchForm input[name="q"]').val();
            $(".talents-filter button.btn-link-filter.is-clicked").trigger('click');
            $('.talents-filter #talent-skills').tagsinput('removeAll');
            document.getElementById('searchForm').reset();
            $('.talents-filter input').val('');
            if(q.length) q = '&q=' + q;
            window.location = "<?php echo option('site_uri') . url_for('/workspace/search') . '?scope=talent' ?>"+q;
        })

        <?php if((isset($filters['state']) && !empty($filters['state'])) || (isset($filters['city']) && !empty($filters['city']))): ?>
        $(".talents-filter button.btn-link-filter.location").trigger('click');
        <?php endif ?>

        <?php if(isset($filters['completed'])): ?>
        $(".talents-filter button.btn-link-filter.completed").trigger('click');
        <?php endif ?>

        $(".btn-load-more.more-talents").on('click', function(e){
            var page = $('#talentSearchForm input[name="page"]').val();
            $('#talentSearchForm input[name="page"]').val(parseInt(page)+1);
            get_talents_result('load_more');
        });

        $('#talentSearchForm input[name="search_name"]').keydown(function(e){
            if(e.keyCode === 13){
                $('.save-talent-search').trigger('click');
                return false;
            }
        })

        $('.save-talent-search').on('click', function(e){
            var search_form = $('#talentSearchForm');
            var search_name = search_form.find('input[name="search_name"]');
            var icon = '', title = '', className = '', message = '';
            if(search_name.val().length){
                $.ajax({
                    url: "<?php echo option('site_uri') . url_for('/search/save') ?>",
                    method: 'POST',
                    data: search_form.serialize()
                }).done(function(response){
                    search_name.val('');
                    $.notify({
                        title: '<b>Success!</b>',
                        message: response.message,
                        icon: "fa fa-check"
                    }, {
                        style: 'metro',
                        type: 'success',
                        globalPosition: 'top right',
                        showAnimation: 'slideDown',
                        hideAnimation: 'slideUp',
                        showDuration: 200,
                        hideDuration: 200,
                        autoHide: true,
                        clickToHide: true
                    });
                });
            }else{
                $.notify({
                    title: '<b>Error!</b>',
                    message: "Please type a search name.",
                    icon: "fa fa-exclamation"
                }, {
                    style: 'metro',
                    type: 'danger',
                    globalPosition: 'top right',
                    showAnimation: 'slideDown',
                    hideAnimation: 'slideUp',
                    showDuration: 200,
                    hideDuration: 200,
                    autoHide: true,
                    clickToHide: true
                });
            }
        });

        $('#talentSearchForm').on('submit', function(){
            $('input[name="search_name"]').prop('disabled', true);
        });


        function update_talents_filters(target){
            if(target === undefined){
                var inputs = $('.talents-filter').find('input, select');
            }else{
                var inputName = target.attr('name');
                var inputs = $('.talents-filter').find('[name="'+inputName+'"]');
            }

            var value = '';
            var input = $('#talentSearchForm input[name="'+inputName+'"]');

            inputs.each(function(idx, item){
                if( (item.type === 'checkbox' && item.checked) || (item.type === 'text' && item.value !== '') || (item.type === 'select-one' && item.value !== '') ){
                    value = item.value;
                    if(value.length) {
                        input.prop('disabled', false);
                        input.val(value);
                        if(target !== undefined) target.data('inputStatus', true);
                        else $(item).data('inputStatus', true);
                    }else{
                        input.val('');
                        input.prop('disabled', true);
                        if(target !== undefined) target.data('inputStatus', false);
                        else $(item).data('inputStatus', false);
                    }
                }else{
                    if(!value.length){
                        input.val('');
                        input.attr('disabled', 'disabled');
                        if(target !== undefined) target.data('inputStatus', false);
                        else $(item).data('inputStatus', false);
                    }
                }
            });

            if($('#talentSearchForm input[name="perform-search"]').val() === 'yes')
                get_talents_result();
        }

        function get_talents_result(load_more = null){
            var search_form = $('#searchTalentTab #talentSearchForm').clone();
            search_form.find('input[name="search_name"]').remove();

            var page = $('#talentSearchForm input[name="page"]').val(),
                url = "<?php echo option('site_uri') . url_for('/workspace/search') ?>/"+page,
                data = search_form.serialize();

            request_talents(url, data, load_more)
        }

        window.request_talents = function(url, data, load_more = null, update = null){
            $.ajax({
                url: url,
                data: data,
            }).done(function(response){
                if(response.more){
                    $(".btn-load-more.more-talents").show();
                }else{
                    $(".btn-load-more.more-talents").hide();
                }
                if(load_more === null) {
                    $('.talent-result-container').html(response.html);
                } else {
                    var lastChildHeight = $('.talent-result-container .col-talent-row-container:last-child').height();
                    var currentHeight = $('.talent-result-container').height();
                    $('.talent-result-container').append(response.html);
                    $('html')[0].scrollTo({ top: currentHeight+lastChildHeight - 24, left: 0, behavior: 'smooth'});
                }
                if(response.keywords !== ''){
                    $('#searchTalentTab .col-search-terms-result-value').text(response.keywords);
                    $('#searchTalentTab .keyword-filter-container > span').text(response.keywords);
                    $('#searchTalentTab .col-search-terms-result').show();
                }else{
                    $('#searchTalentTab .col-search-terms-result').hide();
                }
                var stateUrl = data.length > 0 ? url + '?' + data : url;
                if( update === null ) {
                    history.pushState({
                        'talentHtml': response.html,
                        'talentUrl': url,
                        'talentData': data
                    }, '', stateUrl);
                }else {
                    history.replaceState({
                        'talentHtml': response.html,
                        'talentUrl': url,
                        'talentData': data
                    }, '', stateUrl);
                }
            });
		}

        var skills = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '<?php echo option('site_uri') . url_for('ajax/skills.json') ?>?all=1'
        });
        skills.clearPrefetchCache();
        skills.initialize();

        $('.talents-filter #talent-skills').tagsinput({
            allowDuplicates: false,
            confirmKeys: [9, 13, 44, 188],
            itemValue: 'id',
            itemText: 'name',
            typeaheadjs: {
                name: 'skills',
                displayKey: 'name',
                source: skills.ttAdapter()
            }
        });

        <?php if(isset($filters['skills_list']) && !empty($filters['skills_list']) && $scope === 'talent'): ?>
        $('.talents-filter #talent-skills]').off('change');
        <?php foreach($filters['skills_list'] as $skill): ?>
        $('.talents-filter #talent-skills').tagsinput('add', {'id': '<?php echo $skill['id'] ?>', 'name': '<?php echo $skill['name'] ?>' });
        <?php endforeach; ?>

        $('.talents-filter #talent-skills').tagsinput('refresh');

        $(".talents-filter button.btn-link-filter.skillss").trigger('click');

        $('.talents-filter #talent-skills').on('change', function(e){
            $('#talentSearchForm input[name="perform-search"]').val('yes');
            $('#talentSearchForm input[name="page"]').val("1");
            update_talents_filters($(e.currentTarget));
        });
        <?php endif ?>

	})
</script>
<?php end_content_for(); ?>