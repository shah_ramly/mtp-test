<div class="tab-pane fade show <?php if($scope === 'talent'){ ?> active <?php } ?>" id="searchTalentTab" role="tabpanel">
    <div class="col-xl-12">
        <div class="row">
            <div class="col-xl-12 col-tasklisting-top">
                <div class="col-public-search-wrapper">
	                <form action="<?php echo option('site_uri') . url_for('workspace/search') ?>" id="searchForm" class="search-public-form" method="get">
		                <input type="hidden" name="scope" value="talent" />
		                <input type="hidden" name="page" value="1" disabled />
		                <div class="search-dropdown-filter-top-wrapper form-flex">
			                <div class="search-dropdown-filter-container search-dropdown-more-filter-container">
                                <div class="search-field-group">
				                    <input type="text" class="form-control form-control-input" name="q"
				                       placeholder="<?php echo lang('public_search_talent_by_keywords_placeholder'); ?>" id="inputBox"
				                       value="<?php echo !is_null($search_term) && ($scope === 'talent' || request('scope') === 'talent') ? $search_term : '' ?>"
				                    />
                                    <span class="glass-icon">
                                        <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                        </svg>
                                    </span>
                                </div>
			                </div>
			                <span class="search-dropdown-filter-btn">
                                <button class="btn-filter-search <?php echo empty($filters) ? 'collapsed' : '' ?>" type="button" data-toggle="collapse" data-target="#advancedFilterTalentCollapse" aria-expanded="false" aria-controls="advancedFilterTalentCollapse">
                                    <span class="slider-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sliders" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M11.5 2a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM9.05 3a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0V3h9.05zM4.5 7a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM2.05 8a2.5 2.5 0 0 1 4.9 0H16v1H6.95a2.5 2.5 0 0 1-4.9 0H0V8h2.05zm9.45 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm-2.45 1a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0v-1h9.05z"></path>
                                        </svg>
                                    </span>
                                    <i class="fa fa-caret-down"></i>
                                </button>
                            </span>
		                </div>
		                <div class="col-search-terms-result" style="<?php echo ((empty($keywords) && $scope === 'talent') || empty($keywords)) ? 'display:none' : '' ?>">
			                <div class="col-search-terms-result-title"><?php echo lang('public_search_keyword_word') ?> :</div>
			                <div class="col-search-terms-result-value"><?php echo $scope === 'talent' ? $keywords : '' ?></div>
                            <a href="<?php echo url_for('workspace/search') ?>?scope=talent" class="reset-filter-link"><?php echo lang('reset_filter') ?></a>
		                </div>
		                <div class="search-advanced-filter-wrapper">
			                <div class="collapse dropdown-menu-advanced-filter <?php echo !empty($filters) ? 'show' : '' ?>" id="advancedFilterTalentCollapse">
		                        <div class="search-dropdown-filter-btm-wrapper form-flex">
                                    <div class="dropdown-filter-container filter-dashboard-savedsearch">
                                        <span class="bookmark-dropdown-filter-btn dropdown">
                                            <button class="btn-savesearch dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                            <span class="btn-label"><?php echo lang('dashboard_save_search'); ?></span>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-saved-search">
                                                <div class="modal-advance-filter-container-left">
                                                    <ul class="nav nav-pills nav-pills-save-search" id="pills-saveSearch" role="tablist">
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="saveTask-tab" data-toggle="tab" data-target="#talentSaveTaskTab" role="tab" aria-controls="talentSaveTaskTab" aria-selected="false"><?php echo lang('dashboard_save_on_demand'); ?></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="saveJob-tab" data-toggle="tab" data-target="#talentSaveJobTab" role="tab" aria-controls="talentSaveJobTab" aria-selected="false"><?php echo lang('dashboard_save_full_time'); ?></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link active" id="saveTalent-tab" data-toggle="tab" data-target="#talentSaveTalentTab" role="tab" aria-controls="talentSaveTalentTab" aria-selected="true"><?php echo lang('dashboard_save_talent'); ?></a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content" id="pills-tabSaveSearch">
                                                        <div class="tab-pane fade show" id="talentSaveTaskTab" role="tabpanel">
                                                            <div class="saved-search-content">
                                                                <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'task'; }))): ?>
                                                                    <?php foreach($task_search as $name => $data): ?>
                                                                        <div class="saved-search-row">
                                                                            <a href="<?php echo $data['url'] ?>" class="skills-label-link">
                                                                                <span class="tag label label-saved-task bubble-lbl">
                                                                                    <?php echo $name ?>
                                                                                </span>
                                                                            </a>
                                                                            <span class="tag-remove" data-toggle="modal"
                                                                                data-search-title="<?php echo $name ?>"
                                                                                data-search-id="<?php echo $data['id'] ?>"
                                                                                data-target="#modal_remove_saved"></span>
                                                                        </div>
                                                                    <?php endforeach ?>
                                                                <?php else: ?>
                                                                    <div class="empty-state-container">
                                                                        <div class="empty-img"><img src="<?php echo url_for('/assets/img/mtp-no-data-2.png') ?>" alt=""></div>
                                                                        <span class="empty-lbl"><?php echo lang('no_saved_search') ?></span>
                                                                        <span class="empty-desc"><?php echo lang('you_havent_saved_any_search_yet') ?>.</span>
                                                                    </div>
                                                                <?php endif ?>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade show" id="talentSaveJobTab" role="tabpanel">
                                                            <div class="saved-search-content">
                                                                <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'job'; }))): ?>
                                                                    <?php foreach($task_search as $name => $data): ?>
                                                                        <div class="saved-search-row">
                                                                            <a href="<?php echo $data['url'] ?>" class="skills-label-link">
                                                                                <span class="tag label label-saved-job bubble-lbl">
                                                                                    <?php echo $name ?>
                                                                                </span>
                                                                            </a>
                                                                            <span class="tag-remove" data-toggle="modal"
                                                                                data-search-title="<?php echo $name ?>"
                                                                                data-search-id="<?php echo $data['id'] ?>"
                                                                                data-target="#modal_remove_saved"></span>
                                                                        </div>
                                                                    <?php endforeach ?>
                                                                <?php else: ?>
                                                                    <div class="empty-state-container">
                                                                        <div class="empty-img"><img src="<?php echo url_for('/assets/img/mtp-no-data-2.png') ?>" alt=""></div>
                                                                        <span class="empty-lbl"><?php echo lang('no_saved_search') ?></span>
                                                                        <span class="empty-desc"><?php echo lang('you_havent_saved_any_search_yet') ?>.</span>
                                                                    </div>
                                                                <?php endif ?>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade show active" id="talentSaveTalentTab" role="tabpanel">
                                                            <div class="saved-search-content">
                                                                <?php if(!empty($saved_search) && !empty($task_search = array_filter($saved_search, function($search){ return $search['scope'] === 'talent'; }))): ?>
                                                                    <?php foreach($task_search as $name => $data): ?>
                                                                    <div class="saved-search-row">
                                                                        <a href="<?php echo $data['url'] ?>" class="skills-label-link">
                                                                            <span class="tag label label-saved-talent bubble-lbl">
                                                                                <?php echo $name ?>
                                                                            </span>
                                                                        </a>
                                                                        <span class="tag-remove" data-toggle="modal"
                                                                            data-search-title="<?php echo $name ?>"
                                                                            data-search-id="<?php echo $data['id'] ?>"
                                                                            data-target="#modal_remove_saved"></span>
                                                                    </div>
                                                                    <?php endforeach ?>
                                                                <?php else: ?>
                                                                    <div class="empty-state-container">
                                                                        <div class="empty-img"><img src="<?php echo url_for('/assets/img/mtp-no-data-2.png') ?>" alt=""></div>
                                                                        <span class="empty-lbl"><?php echo lang('no_saved_search') ?></span>
                                                                        <span class="empty-desc"><?php echo lang('you_havent_saved_any_search_yet') ?>.</span>
                                                                    </div>
                                                                <?php endif ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </span>
                                        <span class="bookmark-dropdown-addfilter-btn dropdown">
                                            <button class="btn-addsavesearch dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                                <span class="public-search-icon addsearch-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                                                        <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                                                    </svg>
                                                </span>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-saved-search dropdown-menu-add-saved-search">
                                                <div class="modal-advance-filter-container-left">
                                                    <div class="modal-advanced-filter-container">
                                                        <div class="keyword-filter-container">
                                                            <label><?php echo lang('dashboard_save_search_keyword'); ?></label>
                                                            <span><?php echo request('scope') === 'talent' && !empty($keywords) ? $keywords : lang('dashboard_talent_save_search_keyword_default_text') ?></span>
                                                        </div>
                                                        <label class="modal-search-filter-lbl"><?php echo lang('dashboard_save_search_button'); ?></label>
                                                        <div class="input-group-flex row-indfirstname">
                                                            <input type="text" id="search_name" name="search_name" class="form-control form-control-input" placeholder="<?php echo lang('dashboard_talent_save_search_name_placeholder'); ?>" />
                                                        </div>
                                                    </div>
                                                    <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
                                                        <div class="modal-filter-action form-block-filter-flex">
                                                            <button type="button" class="btn-icon-full btn-confirm save-search" data-dismiss="modal" data-orientation="next">
                                                                <span class="btn-label"><?php echo lang('dashboard_save_button'); ?></span>
                                                                <span class="btn-icon">
                                                                    <svg
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            width="24"
                                                                            height="24"
                                                                            viewBox="0 0 24 24"
                                                                            fill="none"
                                                                            stroke="currentColor"
                                                                            stroke-width="2.5"
                                                                            stroke-linecap="round"
                                                                            stroke-linejoin="arcs"
                                                                    >
                                                                        <polyline points="20 6 9 17 4 12"></polyline>
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="dropdown dropdown-filter-container filter-dashboard-talentcategory" style="display:none">
                                        <button class="btn btn-dropdown-label dropdown-toggle<?php echo isset($filters['category']) ? 'border-info' : '' ?>"
                                                type="button" id="dropdownTalentCategory" data-toggle="dropdown"
                                                aria-haspopup="true" aria-expanded="false">
                                            <label class="filter-btn-lbl"><?php echo lang('public_search_category'); ?></label>
                                        </button>
                                        <div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTalentCategory">
                                            <div class="form-block-task-filter">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="category" value="0" id="filtercat_0"
                                                        <?php if(isset($filters['category']) && ( in_array('0', explode(',', $filters['category'])) )): ?>
                                                            data-input-status="true"
                                                            checked
                                                        <?php else: ?>
                                                            data-input-status="false"
                                                        <?php endif; ?>
                                                    >
                                                    <label class="custom-control-label" for="filtercat_0"><?php echo lang('public_search_all_category'); ?></label>
                                                </div>
                                                <?php if(isset($task_categories)): ?>
                                                    <?php foreach($task_categories as $category): ?>
                                                        <div class="custom-control custom-checkbox categories-input">
                                                            <input type="checkbox" class="custom-control-input" name="category"
                                                                <?php if(isset($filters['category']) && (in_array($category['id'], explode(',', $filters['category'])))): ?>
                                                                    data-input-status="true"
                                                                    checked
                                                                <?php else: ?>
                                                                    data-input-status="false"
                                                                <?php endif; ?>
                                                                value="<?php echo $category['id'] ?>" id="filtercat_<?php echo $category['id'] ?>">
                                                            <label class="custom-control-label" for="filtercat_<?php echo $category['id'] ?>"><?php echo $category['name'] ?></label>
                                                        </div>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dropdown dropdown-filter-container filter-dashboard-talentlocation">
                                        <button class="btn btn-dropdown-label dropdown-toggle <?php echo isset($filters['location_by']) ? 'border-info' : '' ?>" type="button" id="dropdownTalentLocation" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <label class="filter-btn-lbl"><?php echo lang('dashboard_location'); ?></label>
                                        </button>
                                        <div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTalentLocation">
                                            <div class="form-block-task-filter">
                                                <div class="form-filter-select-by-location">
                                                    <select class="form-control form-control-input" placeholder="State" name="location_by">
                                                        <option value="" selected><?php echo lang('public_search_select_by'); ?></option>
                                                        <option <?php echo isset($filters['location_by']) && $filters['location_by'] === 'locbystate' ? 'selected' : '' ?> value="locbystate"><?php echo lang('public_search_filter_location_select_by_state') ?></option>
                                                        <option <?php echo isset($filters['location_by']) && $filters['location_by'] === 'locbynearest' ? 'selected' : '' ?> value="locbynearest"><?php echo lang('public_search_filter_location_select_by_nearest') ?></option>
                                                    </select>
                                                </div>
                                                <div class="form-filter-state" style="<?php echo !isset($filters['state']) ? 'display: none' : '' ?>">
                                                    <select class="form-control form-control-input state" name="state" id="listing_state"
                                                            data-target="city"
                                                        <?php if(isset($filters['state'])):?>
                                                            data-input-status="true"
                                                        <?php else: ?>
                                                            data-input-status="false"
                                                        <?php endif; ?>
                                                            placeholder="State">
                                                        <option value="" selected><?php echo lang('dashboard_location_select_by_state'); ?></option>
                                                        <?php foreach($states as $state): ?>
                                                            <option <?php echo $scope === 'talent' && isset($filters['state']) && $state['id'] === $filters['state'] ? 'selected' : '' ?>
                                                                    value="<?php echo $state['id'] ?>"><?php echo $state['name'] ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="form-filter-city" style="<?php echo !isset($filters['city']) ? 'display: none' : '' ?>">
                                                    <select class="form-control form-control-input" name="city" id="listing_city"
                                                        <?php if(isset($filters['city'])):?>
                                                            data-input-status="true"
                                                        <?php else: ?>
                                                            data-input-status="false"
                                                        <?php endif; ?>
                                                            placeholder="City">
                                                        <option selected value=""><?php echo lang('public_search_filter_location_select_by_town') ?></option>
                                                        <?php foreach($cities as $city): ?>
                                                            <option <?php echo request('scope') === 'talent' && isset($filters['city']) && $city['id'] === $filters['city'] ? 'selected' : '' ?>
                                                                    value="<?php echo $city['id'] ?>">
                                                                <?php echo $city['name'] ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="form-filter-within" style="<?php echo isset($filters['location_by']) && $filters['location_by'] === 'locbynearest' ? '' : 'display: none'; ?>">
                                                    <select class="form-control form-control-input" name="within" placeholder="within">
                                                        <option value="" selected><?php echo lang('public_search_filter_location_select_by_nearest_select_within') ?></option>
                                                        <option <?php echo isset($filters['within']) && $filters['within'] === '5km' ? 'selected' : '' ?> value="5km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance') ?> 5km</option>
                                                        <option <?php echo isset($filters['within']) && $filters['within'] === '10km' ? 'selected' : '' ?> value="10km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance') ?> 10km</option>
                                                        <option <?php echo isset($filters['within']) && $filters['within'] === '20km' ? 'selected' : '' ?> value="20km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance') ?> 20km</option>
                                                        <option <?php echo isset($filters['within']) && $filters['within'] === '30km' ? 'selected' : '' ?> value="30km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance') ?> 30km</option>
                                                        <option <?php echo isset($filters['within']) && $filters['within'] === '50km' ? 'selected' : '' ?> value="50km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance') ?> 50km</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dropdown dropdown-filter-container filter-dashboard-talentskills">
                                        <button class="btn btn-dropdown-label dropdown-toggle <?php echo isset($filters['skills']) ? 'border-info' : '' ?>" type="button" id="dropdownTaskSkills" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <label class="filter-btn-lbl"><?php echo lang('dashboard_skills') ?></label>
                                        </button>
                                        <div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTaskSkills">
                                            <div class="form-block-task-filter">
                                                <div class="bstags-skills">
                                                    <input type="text" name="skills" id="talent-skills"
                                                        <?php if(request('scope') === 'talent' && isset($filters['skills'])):?>
                                                            data-input-status="true"
                                                            checked
                                                            value="<?php echo implode(',', $filters['skills']) ?>"
                                                        <?php else: ?>
                                                            data-input-status="false"
                                                        <?php endif; ?>
                                                        class="bootstrap-tagsinput" placeholder="<?php echo lang('dashboard_skills_placeholder') ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dropdown dropdown-filter-container filter-dashboard-completedtask">
                                        <button class="btn btn-dropdown-label dropdown-toggle <?php echo isset($filters['completed']) ? 'border-info' : '' ?>" type="button" id="dropdownCompletedTask" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <label class="filter-btn-lbl"><?php echo lang('public_search_talent_completed_task') ?></label>
                                        </button>
                                        <div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownCompletedTask">
                                            <div class="form-block-task-filter">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_1"
                                                        <?php if(request('scope') === 'talent' && isset($filters['completed']) && $filters['completed'] === 'all'):?>
                                                            data-input-status="true"
                                                            checked
                                                        <?php else: ?>
                                                            data-input-status="false"
                                                        <?php endif; ?>
                                                        value="all" />
                                                    <label class="custom-control-label" for="filtertaskcompleted_1"><?php echo lang('public_search_talent_completed_task_all') ?></label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_2"
                                                        <?php if(request('scope') === 'talent' && isset($filters['completed']) && $filters['completed'] === '1-10'):?>
                                                            data-input-status="true"
                                                            checked
                                                        <?php else: ?>
                                                            data-input-status="false"
                                                        <?php endif; ?>
                                                        value="1-10" />
                                                    <label class="custom-control-label" for="filtertaskcompleted_2">1-10</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_3"
                                                        <?php if(request('scope') === 'talent' && isset($filters['completed']) && $filters['completed'] === '10+'):?>
                                                            data-input-status="true"
                                                            checked
                                                        <?php else: ?>
                                                            data-input-status="false"
                                                        <?php endif; ?>
                                                        value="10+" />
                                                    <label class="custom-control-label" for="filtertaskcompleted_3">10+</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
			                </div>
		                </div>
	                </form>
	                <form action="<?php echo url_for('/workspace/search') ?>" id="hiddenSearchForm" class=" search-public-form" method="get">
		                <input type="hidden" name="scope" value="talent" />
		                <input type="hidden" name="q"
                            <?php if(!is_null($search_term) && ($scope === 'talent' || request('scope') === 'talent')): ?>
				                value="<?php echo $search_term ?>"
                            <?php else: ?>
				                disabled
                            <?php endif ?>
		                />
		                <input type="hidden" name="perform-search" value="no" disabled />
		                <input type="hidden" name="page" disabled value="<?php echo $current_page ? $current_page : 1 ?>" />
		                <input type="hidden" name="category"
                            <?php if(isset($filters['category']) ): ?>
				                value="<?php echo $filters['category'] ?>"
                            <?php else: ?>
				                disabled
                            <?php endif ?>
		                />
		                <input type="hidden" name="location_by"
                            <?php if(isset($filters['location_by']) ): ?>
				                value="<?php echo $filters['location_by'] ?>"
                            <?php else: ?>
				                disabled
                            <?php endif ?>
		                />
		                <input type="hidden" name="within"
                            <?php if(isset($filters['within']) ): ?>
				                value="<?php echo $filters['within'] ?>"
                            <?php else: ?>
				                disabled
                            <?php endif ?>
		                />
		                <input type="hidden" name="state"
                            <?php if(isset($filters['state']) ): ?>
				                value="<?php echo $filters['state'] ?>"
                            <?php else: ?>
				                disabled
                            <?php endif ?>
		                />
		                <input type="hidden" name="city"
                            <?php if(isset($filters['city']) ): ?>
				                value="<?php echo $filters['city'] ?>"
                            <?php else: ?>
				                disabled
                            <?php endif ?>
		                />
		                <input type="hidden" name="skills"
                            <?php if(isset($filters['skills']) ): ?>
				                value="<?php echo implode(',', $filters['skills']) ?>"
                            <?php else: ?>
				                disabled
                            <?php endif ?>
		                />
		                <input type="hidden" name="completed"
                            <?php if(isset($filters['completed']) ): ?>
				                value="<?php echo $filters['completed'] ?>"
                            <?php else: ?>
				                disabled
                            <?php endif ?>
		                />
	                </form>
                </div>
                <div class="col-task-listing-wrapper col-talent-tiles-wrapper">
                    <div class="row">
                        <div class="col-xl-12 col-talent-row-wrapper">
	                        <div class="talent-result-container">
	                        <?php if(!empty($talents)): ?>
	                        <?php foreach($talents as $talent): ?>
                                <?php echo partial('talent/talent-row.html.php', ['talent' => $talent]); ?>
	                        <?php endforeach; ?>
                            <?php else: ?>
		                        <div class="col-task-row-container">
			                        <div class="task-row-col-top-flex job">
				                        <div class="empty-state-container">
					                        <div class="empty-img"><img src="<?php echo url_for('assets/img/mtp-no-data-2.png') ?>" alt=""></div>
					                        <span class="empty-lbl"><?php echo lang('public_search_no_result') ?></span>
				                        </div>
			                        </div>
		                        </div>
                            <?php endif ?>
	                        </div>
                            <?php if(isset($more_talents) && $more_talents): ?>
		                        <button class="btn-icon-full btn-load-more more-talents" data-text="<?php echo lang('search_load_more_loading') ?>">
			                        <span class="btn-label" id="loadMoreLbl" data-text="<?php echo lang('public_search_load_more'); ?>"><?php echo lang('public_search_load_more'); ?></span>
			                        <span class="btn-icon load-plus-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                                </span>
			                        <span class="btn-icon load-load-icon" style="display:none;">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg>
                                </span>
		                        </button>
	                            <button class="btn-icon-full btn-load-more back-to-top" style="display:none" data-text="Loading...">
		                            <span class="btn-label" id="loadMoreLbl" data-text="Load More"><span><?php echo lang('end_of_list') ?></span> - <?php echo lang('back_to_top') ?></span>
		                            <span class="btn-icon arrowtop-icon">
	                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-arrow-up-short" viewBox="0 0 16 16">
	                                        <path fill-rule="evenodd" d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5z"></path>
	                                    </svg>
	                                </span>
		                            <span class="btn-icon load-load-icon" style="display:none;">
	                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg>
	                                </span>
	                            </button>
                            <?php else: ?>
                            <button class="btn-icon-full btn-load-more back-to-top" data-text="Loading...">
	                            <span class="btn-label" id="loadMoreLbl" data-text="Load More"><span><?php echo lang('end_of_list') ?></span> - <?php echo lang('back_to_top') ?></span>
	                            <span class="btn-icon arrowtop-icon">
	                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-arrow-up-short" viewBox="0 0 16 16">
	                                <path fill-rule="evenodd" d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5z"></path>
	                                </svg>
	                            </span>
	                            <span class="btn-icon load-load-icon" style="display:none;">
	                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg>
	                            </span>
                            </button>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php content_for('scripts') ?>
<script>
    $('input[name="completed"]').on('click', function(e){
        $(e.currentTarget).parent().siblings().find('input[name="completed"]').prop('checked', false);
        $(e.currentTarget).prop('checked', true);
    });

    $('#searchForm').find('input, select').on('change', function(e){
        $('#hiddenSearchForm input[name="page"]').val("1");
        $('#hiddenSearchForm input[name="q"]').val($('#inputBox').val());
        if( $(e.currentTarget).attr('name') !== 'date_type' ) {
            if( $(e.currentTarget).attr('name') === 'date_period' && $(e.currentTarget).val() !== '' ){
                if( $(e.currentTarget).val() !== 'customrange' ){
                    $('#hiddenSearchForm input[name="perform-search"]').val('yes');
                    update_talent_filters($(e.currentTarget));
                }else{
                    $('#hiddenSearchForm input[name="perform-search"]').val('no');
                    update_talent_filters($(e.currentTarget));
                }
            }else{
                if($(e.currentTarget).attr('name') === 'location_by' && ! $(e.currentTarget).val().length){
                    $('[name="city"], [name="state"], [name="within"]').val('').data('inputStatus', false);
                    $('#hiddenSearchForm input[name="perform-search"]').val('yes');
                }else if($(e.currentTarget).attr('name') === 'location_by'){
                    $('#hiddenSearchForm input[name="perform-search"]').val('no');
                }else{
                    $('#hiddenSearchForm input[name="perform-search"]').val('yes');
                }
                update_talent_filters($(e.currentTarget));
            }
        }else{
            if( $('#hiddenSearchForm input[name="from"]').val().length && $('#hiddenSearchForm input[name="to"]').val().length )
                $('#hiddenSearchForm input[name="perform-search"]').val('yes');
            else if( ! $(e.currentTarget).val().length)
                $('#hiddenSearchForm input[name="perform-search"]').val('yes');
            else
                $('#hiddenSearchForm input[name="perform-search"]').val('no');
            update_talent_filters($(e.currentTarget));
        }
    });

    function update_talent_filters(target){
        if(target === undefined){
            var inputs = $('#searchForm').find('input, select');
        }else{
            var inputName = target.attr('name');
            var inputs = target;
            if(target.attr('type') === 'checkbox') inputs = $('#searchForm').find('[name="'+inputName+'"]');
        }

        var taskType = '';
        var scope = '';
        var categories = '';
        var value = '';
        var input = $('#hiddenSearchForm input[name="'+inputName+'"]');

        if(inputName === 'location_by' && !$(inputs).val().length){
            input = $('#hiddenSearchForm input[name="location_by"], #hiddenSearchForm input[name="state"], #hiddenSearchForm input[name="city"], #hiddenSearchForm input[name="within"]');
        }else if(inputName === 'location_by' && $(inputs).val() === 'locbystate'){
            $('#hiddenSearchForm input[name="within"]')
                .val('').prop('disabled', true);
        }else if(inputName === 'location_by' && $(inputs).val() === 'locbynearest' ){
            <?php if( !isset($_SESSION['current_user_location']) ): ?>
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (location) {
                    latlng = {lat: location.coords.latitude, lng: location.coords.longitude};
                    if( window.user_location === undefined ) {
                        $.ajax({
                            url: "<?php echo option("site_uri") . url_for("/search/savelocation") ?>",
                            method: 'POST',
                            data: {'location': latlng}
                        }).done(function (response) {
                            window.user_location = true;
                            //document.location.href = document.location.href;
                        })
                    }
                }, function(){
                    $(inputs).val('').trigger('change');
                    t('w', '<?php echo lang('please_allow_for_location_to_get_nearby_result') ?>', '<?php echo lang('warning') ?>');//alert("Please click Allow for location to get nearby result");
                    return false;
                });
            }
            <?php endif ?>
        }

        if( inputName === 'date_type' && ! $(inputs).val().length ){
            input = $('#hiddenSearchForm input[name="date_type"], #hiddenSearchForm input[name="date_period"], #hiddenSearchForm input[name="from"], #hiddenSearchForm input[name="to"]');
        }

        if(inputName === 'within'){
            $('#hiddenSearchForm input[name="state"], #hiddenSearchForm input[name="city"]')
                .val('').prop('disabled', true);
        }

        inputs.each(function(idx, item){
            if(
                ((item.type === 'checkbox' && item.checked) ||
                    (item.type === 'text' && item.value !== '') ||
                    (item.type === 'select-one' && item.value !== ''))
            ) {
                if(item.name === 'type' || item.name === 'category' || item.name === 'task_type'){
                    if(item.name === 'category') {
                        if(categories === '')
                            categories += item.value;
                        else
                            categories +=','+item.value;
                    }else if(item.name === 'type') {
                        if(scope === '')
                            scope += item.value;
                        else
                            scope +=','+item.value;
                    }else{
                        if(taskType === '')
                            taskType += item.value;
                        else
                            taskType +=','+item.value;
                    }
                }else {
                    if(item.name === 'category') categories = item.value;
                    else if(item.name === 'type') scope = item.value;
                    else if(item.name === 'task_type') taskType = item.value;
                    else value = item.value;
                }

                if(scope !== '') value = scope;
                else if(categories !== '') value = categories;
                else if(taskType !== '') value = taskType;

                if(value.length) {
                    input.removeAttr('disabled');
                    input.val(value);
                    if(target !== undefined) target.data('inputStatus', true);
                    else $(item).data('inputStatus', true);
                }else{
                    input.val('');
                    input.attr('disabled', 'disabled');
                    input.prop('disabled', true);
                    if(target !== undefined) target.data('inputStatus', false);
                    else $(item).data('inputStatus', false);
                }
            }else{
                if(!value.length){
                    input.val('');
                    input.attr('disabled', 'disabled');
                    input.prop('disabled', true);
                    if(target !== undefined) target.data('inputStatus', false);
                    else $(item).data('inputStatus', false);
                }
            }
        });

        if($('#hiddenSearchForm input[name="perform-search"]').val() === 'yes')
            get_result();
    }

    $(".btn-load-more.more-talents").on('click', function(e){
        var page = $('#hiddenSearchForm input[name="page"]').val();
        $('#hiddenSearchForm input[name="page"]').val(parseInt(page)+1);
        get_result('load_more');
    });

    $(".btn-load-more.back-to-top").on('click', function(e){
        e.preventDefault();
        $('html')[0].scrollTo({ top: 0, left: 0, behavior: 'smooth'});
    });

    $('#searchForm input[name="search_name"]').keydown(function(e){
        if(e.keyCode === 13){
            $('.save-search').trigger('click');
            return false;
        }
    });

    $('.save-search').on('click', function(e){
        e.preventDefault();
        var search_name = $('input[name="search_name"]');
        var search_form = $('#hiddenSearchForm').clone();
        search_form.append('<input type="text" name="search_name" value="'+search_name.val()+'" />');
        var icon = '', title = '', className = '', message = '';
        if(search_name.val().length){
            $.ajax({
                url: "<?php echo option('site_uri') . url_for('/search/save') ?>",
                method: 'POST',
                data: search_form.serialize(),
            }).done(function(response){
                search_name.val('');
                var search = $.parseJSON(response.search);
                if(typeof (search) === 'object') {
                    var container = $('#talentSaveTalentTab .saved-search-content');
                    if( container.find('.empty-state-container').length ){
                        container.find('.empty-state-container').remove();
                    }
                    container.append('<div class="saved-search-row"><a href="'+search.url+'" class="skills-label-link">'+
                        '<span class="tag label label-saved-talent bubble-lbl">'+search.name+'</span>'+
                        '</a><span class="tag-remove" data-toggle="modal" data-search-title="'+search.name+'" data-search-id="'+search.id+'" data-target="#modal_remove_saved"></span></div>'
                    );

                }
                $.notify({
                    title: '<b>Success!</b>',
                    message: response.message,
                    icon: "fa fa-check"
                }, {
                    style: 'metro',
                    type: 'success',
                    globalPosition: 'top right',
                    showAnimation: 'slideDown',
                    hideAnimation: 'slideUp',
                    showDuration: 200,
                    hideDuration: 200,
                    autoHide: true,
                    clickToHide: true
                });
            });
        }else{
            $.notify({
                title: '<b>Error!</b>',
                message: "Please type a search name.",
                icon: "fa fa-exclamation"
            }, {
                style: 'metro',
                type: 'danger',
                globalPosition: 'top right',
                showAnimation: 'slideDown',
                hideAnimation: 'slideUp',
                showDuration: 200,
                hideDuration: 200,
                autoHide: true,
                clickToHide: true
            });
        }
        search_form = null;
    });

    $('#searchForm').on('submit', function(e){
        update_talent_filters();
        e.preventDefault();
        return false;
    });

    var skills = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '<?php echo option('site_uri') . url_for('ajax/skills.json') ?>?all=1'
    });
    skills.clearPrefetchCache();
    skills.initialize();

    $('#talent-skills').tagsinput({
        allowDuplicates: false,
        confirmKeys: [9, 13, 44, 188],
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'skills',
            displayKey: 'name',
            source: skills.ttAdapter()
        }
    });

    <?php if(isset($filters['skills']) && !empty($filters['skills']) && $scope === 'task'): ?>

    <?php if(!is_array($filters['skills'])) $filters['skills'] = [$filters['skills']]; ?>
    $('#talent-skills').off('change');
    <?php echo json_encode($filters['skills_list']) ?>.forEach(function(item, idx){
        $('#talent-skills').tagsinput('add', item);
    });
    $('#talent-skills').on('change', function(e){
        if( $(e.currentTarget).attr('name') !== 'date_type' ) {
            if( $(e.currentTarget).attr('name') === 'date_period' && $(e.currentTarget).val() !== '' ){
                if( $(e.currentTarget).val() !== 'customrange' ){
                    $('#hiddenSearchForm input[name="perform-search"]').val('yes');
                }
            }else {
                $('#hiddenSearchForm input[name="perform-search"]').val('yes');
            }
        }
        $('#hiddenSearchForm input[name="page"]').val("1");
        update_talent_filters($(e.currentTarget));
    });
    <?php endif ?>

    function get_result(load_more = null){
        var search_form = $('#hiddenSearchForm').clone();
        search_form.find('input[name="search_name"]').remove();

        var page = $('#hiddenSearchForm input[name="page"]').val() || 1,
            url = "<?php echo option('site_uri') . url_for('/workspace/search') ?>/"+page,
            data = search_form.serialize();

        request_talents(url, data, load_more);
    }

    function request_talents(url, data, load_more = null, update = null){

        $.ajax({
            url: url,
            data: data,
        }).done(function(response){
            if(response.more){
                $(".btn-load-more.more-talents").show();
                $(".btn-load-more.back-to-top").removeAttr('disabled').hide();
            }else{
                $(".btn-load-more.more-talents").hide();
                $(".btn-load-more.back-to-top").removeAttr('disabled').show();
            }
            if(load_more === null) {
                $('.talent-result-container').html(response.html);
            } else {
                var currentHeight = $('.talent-result-container').height();
                $('.talent-result-container').append(response.html);
                $('html')[0].scrollTo({ top: currentHeight+100, left: 0, behavior: 'smooth'});
            }
            if(response.keywords !== ''){
                $('#searchTalentTab .col-search-terms-result-value').text(response.keywords);
                $('#searchTalentTab .keyword-filter-container > span').text(response.keywords);
                $('#searchTalentTab .col-search-terms-result').show();
            }else{
                $('#searchTalentTab .col-search-terms-result').hide();
            }
            if( $('.col-task-row-wrapper').css('display') === 'none' )
                $('.col-task-row-wrapper').fadeIn();

            var stateUrl = data.length > 0 ? url + '?' + data : url;
            stateUrl = decodeURI(stateUrl);
            url = decodeURI(url);
            if (update === null) {
                history.pushState({
                    'talentHtml': response.html,
                    'talentUrl': url,
                    'talentData': data
                }, '', stateUrl);
            }else {
                history.replaceState({
                    'talentHtml': response.html,
                    'talentUrl': url,
                    'talentData': data
                }, '', stateUrl);
            }
        });
    }

    window.addEventListener('popstate', function(e){
        if(e.state){
            $('#searchTalentTab .talent-result-container').html(e.state.talentHtml);
            request_talents(e.state.talentUrl, e.state.talentData, null, 'update');
        }
    })
</script>
<?php end_content_for() ?>