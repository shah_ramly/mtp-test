<div class="color-card job-color-card">
    <a href="<?php echo url_for("/workspace/job/{$job['slug']}/details"); ?>" class="gtm-job-tiles">
        <div class="recentpost-card">
            <div class="recentpost-info">
                <div class="recentpost-cat"><?php echo $job['category'] ?></div>
                <div class="recentpost-title"><?php echo $job['title'] ?></div>
                <div class="recentpost-loc"><?php echo "{$job['job_state']}, {$job['job_country']}"; ?></div>
                <div class="recentpost-id"><?php echo $job['number']; ?></div>
                <?php if( \Carbon\Carbon::parse($job['created_at'])->gte( \Carbon\Carbon::now()->subDays(3) ) ): ?>
	            <span class="new-highlight"><?php echo lang('tiles_new')?></span>
                <?php endif ?>
            </div>
            <div class="amount-val recentpost-amount">
                <span class="amount-prefix">RM</span>
				<?php $salary = explode('.', $job['salary_range_max']) ?>
                <span class="amount-val"><?php echo $salary[0] ?></span>
                <span class="amount-suffix"></span>
            </div>
        </div>
    </a>
    <?php if( !in_array($job['job_id'], $jobs_applied['ids']) ): ?>
	<form method="post" class="favourite-form" action="<?php echo url_for('/workspace/job/favourite') ?>">
        <?php echo html_form_token_field(); ?>
		<input type="hidden" name="job_id" value="<?php echo $job['job_id']; ?>">
		<input type="hidden" name="favourited" value="<?php echo in_array($job['job_id'], $favourites['ids']) ? "true":"false"; ?>">
		<input type="hidden" name="url" value="<?php echo isset($url) ? $url : url_for('/workspace/search'); ?>">
		<button type="submit" class="btn-icon-heart <?php echo in_array($job['job_id'], $favourites['ids']) && ($job['user_id'] !== $current_user['id']) ? 'btn-unfav':'btn-fav' ?>"  <?php echo ($job['user_id'] === $current_user['id']) ? 'disabled="disabled" style="display:none"'  : '' ?>>
			<span class="fav-icon"<?php echo ($job['user_id'] === $current_user['id']) ? ' style="color:#ccc"'  : '' ?>></span>
		</button>
	</form>
	<?php else: ?>
	<form method="post" class="favourite-form" action="<?php echo url_for('/workspace/job/favourite') ?>">
        <?php echo html_form_token_field(); ?>
		<input type="hidden" name="job_id" value="<?php echo $job['job_id']; ?>">
		<input type="hidden" name="favourited" value="<?php echo in_array($job['job_id'], $favourites['ids']) ? "true":"false"; ?>">
		<input type="hidden" name="url" value="<?php echo isset($url) ? $url : url_for('/workspace/search'); ?>">
		<button type="submit" class="btn-icon-heart <?php echo in_array($job['job_id'], $favourites['ids']) && ($job['user_id'] !== $current_user['id']) ? 'btn-unfav':'btn-fav' ?>"  <?php echo ($job['user_id'] === $current_user['id']) ? 'disabled="disabled" style="display:none"'  : '' ?> title="<?php echo lang('you_have_applied_for_this_job')?>">
			<span class="check2circle-icon"<?php echo ($job['user_id'] === $current_user['id']) ? ' style="color:#ccc"'  : '' ?>></span>
		</button>
	</form>
	<?php endif ?>
</div>