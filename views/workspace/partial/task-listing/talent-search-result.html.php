<?php if (!empty($talents)): ?>
    <?php foreach($talents as $talent): ?>
        <?php echo partial('talent/talent-row.html.php', ['talent' => $talent]); ?>
    <?php endforeach; ?>
<?php else: ?>
	<div class="col-task-row-container">
		<div class="task-row-col-top-flex job">
			<div class="empty-state-container">
				<div class="empty-img"><img src="<?php echo url_for('assets/img/mtp-no-data-2.png') ?>" alt=""></div>
				<span class="empty-lbl"><?php echo lang('public_search_no_result'); ?></span>
			</div>
		</div>
	</div>
<?php endif ?>
