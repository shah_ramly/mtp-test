<div class="color-card <?php echo isset($color) ? $color : "" ?>">
    <a href="<?php echo url_for("/workspace/task/{$task['slug']}/details") . '?s=external'; ?>" class="gtm-featured-tiles">
        <div class="recentpost-card">
            <div class="recentpost-info">
                <div class="recentpost-cat"><?php echo $task['subcategory_name'] ?></div>
                <div class="recentpost-title"><?php echo $task['title']; ?></div>
                <div class="recentpost-loc"><?php echo ucwords($task['state_country']); ?></div>
                <div class="recentpost-id"><?php echo strtoupper($task['number']); ?></div>
                <?php if( \Carbon\Carbon::parse($task['created_at'])->gte( \Carbon\Carbon::now()->subDays(3) ) ): ?>
		            <span class="new-highlight"><?php echo lang('tiles_new')?></span>
                <?php endif ?>
            </div>
            <div class="amount-val recentpost-amount">
                <span class="amount-prefix">RM</span>
				<?php $budget = explode('.', $task['budget']) ?>
                <span class="amount-val"><?php echo number_format($budget[0]) ?></span>
                <span class="amount-suffix"></span>
            </div>
        </div>
    </a>
    <?php if( !in_array($task['task_id'], $tasks_applied['ids']) ): ?>
	<form method="post" class="favourite-form" action="<?php echo url_for('/workspace/task/favourite') ?>">
		<?php echo html_form_token_field(); ?>
		<input type="hidden" name="task_id" value="<?php echo $task['task_id']; ?>">
		<input type="hidden" name="external" value="true">
		<input type="hidden" name="favourited" value="<?php echo in_array($task['task_id'], $favourites['ids']) ? "true":"false"; ?>">
		<input type="hidden" name="url" value="<?php echo isset($url) ? $url : url_for('/workspace/search'); ?>">
		<button type="submit" class="btn-icon-heart <?php echo in_array($task['task_id'], $favourites['ids']) ? 'btn-unfav':'btn-fav' ?>" <?php echo ($task['user_id'] === $current_user['id']) ? 'disabled="disabled" style="display:none"'  : '' ?>>
			<!-- <span class="btn-label"><?php echo in_array($task['task_id'], $favourites['ids']) ? lang('task_remove_fav'):lang('task_save_fav') ?></span> -->
			<span class="fav-icon" <?php echo ($task['user_id'] === $current_user['id']) ? ' style="color:#ccc"'  : '' ?>></span>
		</button>
	</form>
	<?php else: ?>
	<form method="post" class="favourite-form" action="<?php echo url_for('/workspace/task/favourite') ?>">
        <?php echo html_form_token_field(); ?>
		<input type="hidden" name="task_id" value="<?php echo $task['task_id']; ?>">
		<input type="hidden" name="external" value="true">
		<input type="hidden" name="favourited" value="<?php echo in_array($task['task_id'], $favourites['ids']) ? "true":"false"; ?>">
		<input type="hidden" name="url" value="<?php echo isset($url) ? $url : url_for('/workspace/search'); ?>">
		<button type="submit" class="btn-icon-heart <?php echo in_array($task['task_id'], $favourites['ids']) ? 'btn-unfav':'btn-fav' ?>" <?php echo ($task['user_id'] === $current_user['id']) ? 'disabled="disabled" style="display:none"'  : '' ?> title="<?php echo lang('you_have_applied_for_this_task')?>">
			<!-- <span class="btn-label"><?php echo in_array($task['task_id'], $favourites['ids']) ? lang('task_remove_fav'):lang('task_save_fav') ?></span> -->
			<span class="check2circle-icon" <?php echo ($task['user_id'] === $current_user['id']) ? ' style="color:#ccc"'  : '' ?>></span>
		</button>
	</form>
    <?php endif ?>
</div>


