<?php

$colors = [
    'gold-card', // 'gold'
    'green-card', // 'green'
    'red-card', // 'red'
    'orange-card', // 'orange'
    'maroon-card', // 'maroon'
    'purple-card', // 'purple'
    'cyan-card', // 'cyan'
    'blue-card' // 'blue'
];

?>
<?php if (!empty($listing)): ?>
	<?php $counter = 0; ?>
	<?php foreach ($listing as $task): ?><?php
		set('color', $colors[$counter]);
        set('task', $task);
        if ($task['type'] === 'internal_task'): ?><?php
            echo partial('workspace/partial/task-listing/task.html.php'); ?><?php
        else: ?><?php
            echo partial('workspace/partial/task-listing/external_task.html.php');
        endif; ?><?php
        $counter++;
        if( $counter > (count($colors) - 1) ) $counter = 0;
    endforeach; ?>
<?php else: ?>
	<div class="col-task-row-container">
		<div class="task-row-col-top-flex job">
			<div class="empty-state-container">
				<div class="empty-img"><img src="<?php echo url_for('assets/img/mtp-no-data-2.png') ?>" alt=""></div>
				<span class="empty-lbl"><?php echo lang('public_search_no_result') ?></span>
			</div>
		</div>
	</div>
<?php endif ?>
