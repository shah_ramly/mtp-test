<div class="col-xl-12 col-public-search disabled">
	<form action="<?php echo url_for('/workspace/search') ?>" id="talentSearchForm" class=" search-public-form" method="get">
        <?php //echo html_form_token_field(); ?>
		<input type="hidden" name="scope" value="talent" />
		<input type="hidden" name="perform-search" value="no" disabled />
		<input type="hidden" name="page" disabled value="<?php echo $current_page ? $current_page : 1 ?>" />
		<input type="hidden" name="state"
            <?php if(isset($filters['state']) ): ?>
				value="<?php echo $filters['state'] ?>"
            <?php else: ?>
				disabled
            <?php endif ?>
		/>
		<input type="hidden" name="city"
            <?php if(isset($filters['city']) ): ?>
				value="<?php echo $filters['city'] ?>"
            <?php else: ?>
				disabled
            <?php endif ?>
		/>
		<input type="hidden" name="skills"
            <?php if(isset($filters['skills']) ): ?>
				value="<?php echo implode(',', $filters['skills']) ?>"
            <?php else: ?>
				disabled
            <?php endif ?>
		/>
		<input type="hidden" name="completed"
            <?php if(isset($filters['completed']) ): ?>
				value="<?php echo $filters['completed'] ?>"
            <?php else: ?>
				disabled
            <?php endif ?>
		/>
		<div class="search-dropdown-filter-top-wrapper form-flex">
			<div class="search-dropdown-filter-container">
				<input type="text" name="q" class="form-control form-control-input" placeholder="<?php echo ('Search Talent by Keyword') ?>" id="inputBox" value="<?php echo !is_null($search_term) && ($scope === 'talent' || request('scope') === 'talent') ? $search_term : '' ?>">
			</div>
			<span class="search-dropdown-filter-btn">
                <button class="btn-search-public" type="submit">
                    <span class="btn-label">Search</span>
                    <span class="public-search-icon search-icon">
                        <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                        </svg>
                    </span>
                </button>
            </span>
			<span class="bookmark-dropdown-filter-btn dropdown">
                <button class="btn-bookmark-public dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                    <span class="btn-label"><?php echo lang('dashboard_save_search'); ?></span>
                    <span class="public-search-icon search-icon">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bookmarks" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M7 13l5 3V4a2 2 0 0 0-2-2H4a2 2 0 0 0-2 2v12l5-3zm-4 1.234l4-2.4 4 2.4V4a1 1 0 0 0-1-1H4a1 1 0 0 0-1 1v10.234z" />
                            <path d="M14 14l-1-.6V2a1 1 0 0 0-1-1H4.268A2 2 0 0 1 6 0h6a2 2 0 0 1 2 2v12z" />
                        </svg>
                    </span>
                    <i class="fa fa-caret-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-saved-search">
                    <div class="modal-advance-filter-container-left">
                        <div class="modal-advanced-filter-container">
                            <div class="keyword-filter-container">
                                <label><?php echo lang('dashboard_save_search_keyword'); ?>:</label>
                                <span><?php echo request('scope') === 'talent' ? $keywords : '' ?></span>
                            </div>
                            <label class="modal-search-filter-lbl"><?php echo lang('dashboard_save_search'); ?></label>
                            <div class="input-group-flex row-indfirstname">
                                <input type="text" name="search_name" id="Name" class="form-control form-control-input" placeholder="Name" autofocus="">
                            </div>
                        </div>
                        <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
                            <div class="modal-filter-action form-block-filter-flex">
                                <button type="button" class="btn-icon-full btn-confirm save-talent-search" data-dismiss="modal" data-orientation="next">
                                    <span class="btn-label">Save</span>
                                    <span class="btn-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                            <polyline points="20 6 9 17 4 12"></polyline>
                                        </svg>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </span>
		</div>
	</form>
</div>
<div class="col-search-terms-result" style="<?php echo (($keywords === '' && request('scope') === 'talent') || ($keywords === '')) ? "display:none" : "" ?>">
	<div class="col-search-terms-result-title">Search Keyword: </div>
	<div class="col-search-terms-result-value"><?php echo request('scope') === 'talent' ? $keywords : '' ?></div>
</div>