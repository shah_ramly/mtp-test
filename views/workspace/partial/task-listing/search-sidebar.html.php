<div class="col-task-filter-container tasks-filter">
    <div class="task-filter-row task-filter-row-location" style="display:none">
        <div class="form-block-task-filter">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="filtertj_1" name="type" value="task"
		               <?php if(isset($filters['type']) && in_array('task', $filters['type'])):?>
		               data-input-status="true"
	                   checked
	                   <?php else: ?>
	                   data-input-status="false"
		               <?php endif; ?>
                >
                <label class="custom-control-label" for="filtertj_1">Task</label>
            </div>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="filtertj_2" name="type" value="job"
                    <?php if(isset($filters['type']) && in_array('job', $filters['type'])):?>
		               data-input-status="true"
		               checked
                    <?php else: ?>
	                   data-input-status="false"
	                <?php endif; ?>
	            >
                <label class="custom-control-label" for="filtertj_2">Job</label>
            </div>
        </div>
    </div>
    <div class="task-filter-row task-filter-row-location">
        <button class="btn-link btn-link-filter btn-flex category" type="button" aria-expanded="false">
            <label class="task-filter-lbl">By category</label>
            <span class="chevron-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <path d="M6 9l6 6 6-6" /></svg>
            </span>
        </button>
        <div class="form-filter-collapse collapse">
            <div class="form-block-task-filter">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="category" value="0" id="filtercat_0"
                    <?php if(isset($filters['category']) && ( in_array('0', explode(',', $filters['category'])) )): ?>
		                    data-input-status="true"
		                    checked
                    <?php else: ?>
		                    data-input-status="false"
                    <?php endif; ?>
                    >
                    <label class="custom-control-label" for="filtercat_0"><?php echo lang('public_search_all_category'); ?></label>
                </div>
	            <div class="categories-input">
	            <?php /*if(isset($task_categories)): */?><!--
	            <?php /*foreach($task_categories as $category): */?>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="category"
                        <?php /*if(isset($filters['category']) && (in_array($category['id'], explode(',', $filters['category'])))): */?>
		                    data-input-status="true"
		                    checked
                        <?php /*else: */?>
		                    data-input-status="false"
                        <?php /*endif; */?>
                           value="<?php /*echo $category['id'] */?>" id="filtercat_<?php /*echo $category['id'] */?>">
                    <label class="custom-control-label" for="filtercat_<?php /*echo $category['id'] */?>"><?php /*echo $category['name'] */?></label>
                </div>
	            <?php /*endforeach */?>
	            --><?php /*endif */?>
	            </div>
            </div>
        </div>
	    <template id="task-categories">
            <?php if(isset($task_categories)): ?>
                <?php foreach($task_categories as $category): ?>
				    <div class="custom-control custom-checkbox categories-input">
					    <input type="checkbox" class="custom-control-input" name="category"
                            <?php if(isset($filters['category']) && (in_array($category['id'], explode(',', $filters['category'])))): ?>
							    data-input-status="true"
							    checked
                            <?php else: ?>
							    data-input-status="false"
                            <?php endif; ?>
						       value="<?php echo $category['id'] ?>" id="filtercat_<?php echo $category['id'] ?>">
					    <label class="custom-control-label" for="filtercat_<?php echo $category['id'] ?>"><?php echo $category['name'] ?></label>
				    </div>
                <?php endforeach ?>
            <?php endif ?>
	    </template>
	    <template id="job-categories">
            <?php if(isset($job_categories)): ?>
                <?php foreach($job_categories as $category): ?>
				    <div class="custom-control custom-checkbox categories-input">
					    <input type="checkbox" class="custom-control-input" name="category"
                            <?php if(isset($filters['category']) && (in_array($category['id'], explode(',', $filters['category'])))): ?>
							    data-input-status="true"
							    checked
                            <?php else: ?>
							    data-input-status="false"
                            <?php endif; ?>
						       value="<?php echo $category['id'] ?>" id="filtercat_<?php echo $category['id'] ?>">
					    <label class="custom-control-label" for="filtercat_<?php echo $category['id'] ?>"><?php echo $category['name'] ?></label>
				    </div>
                <?php endforeach ?>
            <?php endif ?>
	    </template>
    </div>
    <div class="task-filter-row task-filter-row-location task-filter-row-nature <?php echo isset($filters['type']) && in_array('job', $filters['type']) ? 'hide':'' ?>">
        <button class="btn-link btn-link-filter btn-flex nature" type="button" aria-expanded="false">
            <label class="task-filter-lbl">By nature of task</label>
            <span class="chevron-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <path d="M6 9l6 6 6-6" /></svg>
            </span>
        </button>
        <div class="form-filter-collapse collapse">
            <div class="form-block-task-filter">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="task_type" value="by-hour"
                           <?php if(isset($filters['task_type']) && ($filters['task_type'] === 'by-hour'|| in_array('by-hour', $filters['task_type']))):?>
		                    data-input-status="true"
		                    checked
                        <?php else: ?>
		                    data-input-status="false"
                        <?php endif; ?>
                           id="filternat_1">
                    <label class="custom-control-label" for="filternat_1"><?php echo lang('dashboard_task_type_by_hour'); ?></label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="task_type" value="lump-sum"
                           <?php if(isset($filters['task_type']) && ($filters['task_type'] === 'lump-sum' || in_array('lump-sum', $filters['task_type']))):?>
		                    data-input-status="true"
		                    checked
                        <?php else: ?>
		                    data-input-status="false"
                        <?php endif; ?>
                           id="filternat_2">
                    <label class="custom-control-label" for="filternat_2"><?php echo lang('dashboard_task_type_lump_sum'); ?></label>
                </div>
            </div>
        </div>
    </div>
    <div class="task-filter-row task-filter-row-budget  <?php echo isset($filters['type']) && in_array('job', $filters['type']) ? 'hide':'' ?>">
        <button class="btn-link btn-link-filter btn-flex task-budget" type="button" aria-expanded="false">
            <label class="task-filter-lbl">By budget per task</label>
            <span class="chevron-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <path d="M6 9l6 6 6-6" /></svg>
            </span>
        </button>
	    <div class="form-filter-collapse collapse">
		    <div class="form-block-task-filter">
			    <select class="form-control form-control-input" name="task_budget"
                    <?php if(isset($filters['task_budget']) && !empty($filters['task_budget'])):?>
					    data-input-status="true"
                    <?php else: ?>
					    data-input-status="false"
                    <?php endif; ?>
			    >
				    <option value="" selected="">Select Amount</option>
				    <option  <?php echo (isset($filters['task_budget']) && implode(',', $filters['task_budget']) === '1,50') ? 'selected' : '' ?> value="1,50">RM1 - RM50</option>
				    <option  <?php echo (isset($filters['task_budget']) && implode(',', $filters['task_budget']) === '51,100') ? 'selected' : '' ?> value="51,100">RM51 - RM100</option>
				    <option  <?php echo (isset($filters['task_budget']) && implode(',', $filters['task_budget']) === '101,500') ? 'selected' : '' ?> value="101,500">RM101 - RM500</option>
				    <option  <?php echo (isset($filters['task_budget']) && implode(',', $filters['task_budget']) === '501,999') ? 'selected' : '' ?> value="501,999">RM501 - RM999</option>
				    <option  <?php echo (isset($filters['task_budget']) && implode(',', $filters['task_budget']) === '1000,100000') ? 'selected' : '' ?> value="1000,100000">RM1000 and above</option>
			    </select>
		    </div>
	    </div>
    </div>
    <div class="task-filter-row task-filter-row-dateposted">
        <button class="btn-link btn-link-filter btn-flex date" type="button" aria-expanded="false">
            <label class="task-filter-lbl">By date</label>
            <span class="chevron-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <path d="M6 9l6 6 6-6" /></svg>
            </span>
        </button>
        <div class="form-filter-collapse collapse">
            <div class="form-block-task-filter">
                <div class="form-filter-datepostedstart">
                    <div class="form-filter-datepostedstart">
                        <select class="form-control form-control-input" id="selectDatePostedStart" name="date_type"
                            <?php if(isset($filters['date_type'])):?>
		                        data-input-status="true"
                            <?php else: ?>
		                        data-input-status="false"
                            <?php endif; ?>
                                placeholder="<?php echo lang('public_search_posted_date'); ?>">
                            <option value="" selected=""><?php echo lang('public_search_select'); ?></option>
                            <option value="postedDate"
                                <?php if(isset($filters['date_type']) && $filters['date_type'] === 'postedDate'):?>
		                            selected
                                <?php endif; ?>
                            ><?php echo lang('public_search_posted_date'); ?></option>
                            <option value="<?php echo isset($filters['type']) && in_array('job', $filters['type']) ? 'closeDate':'startDate' ?>"
                                <?php if(isset($filters['date_type']) && ($filters['date_type'] === 'startDate' || $filters['date_type'] === 'closeDate')):?>
		                            selected
                                <?php endif; ?>
                            > <?php echo isset($filters['type']) && in_array('job', $filters['type']) ? lang('public_search_closing_date') : lang('public_search_start_date'); ?></option>
                        </select>
                    </div>
                </div>
                <div class="form-filter-dateposted" <?php echo $filters['date_type'] !== 'postedDate' ? 'style="display:none"': '' ?>>
                    <div class="form-filter-dateposted">
                        <select class="form-control form-control-input" name="date_period"
                            <?php if(isset($filters['date_period'])):?>
		                        data-input-status="true"
                            <?php else: ?>
		                        data-input-status="false"
                            <?php endif; ?>
                                id="search_filter_dateposted" placeholder="<?php echo lang('public_search_posted_date'); ?>">
                            <option value="" selected disabled><?php echo lang('public_search_select'); ?></option>
                            <option value="any" <?php echo (isset($filters['date_period']) && $filters['date_period'] === 'any') ? 'selected' : '' ?>><?php echo lang('dashboard_filter_any_time'); ?></option>
                            <option value="24h" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '24h') ? 'selected' : '' ?>>Last 24 hours</option>
                            <option value="3d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '3d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_last_x_days'), 3); ?></option>
                            <option value="7d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '7d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_last_x_days'), 7); ?></option>
                            <option value="14d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '14d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_last_x_days'), 14); ?></option>
                            <option value="30d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '30d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_last_x_days'), 30); ?></option>
                            <option value="customrange" <?php echo (isset($filters['date_period']) && $filters['date_period'] === 'customrange') ? 'selected' : '' ?>><?php echo lang('public_search_custom_range'); ?></option>
                        </select>
                    </div>
                    <div class="task-filter-row task-filter-row-datepicker-fromto" <?php echo $filters['date_type'] === 'postedDate' && $filters['date_period'] === 'customrange' ? '': 'style="display:none"' ?>>
                        <div class="form-group form-datetime-search-filter-from">
                            <div class="form-group">
                                <div class="input-group input-group-datetimepicker date" id="form_datetime_search_filter_from" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="from"
                                        <?php if(isset($filters['from'])):?>
                                            value="<?php echo $filters['from'] ?>"
		                                    data-input-status="true"
                                        <?php else: ?>
		                                    data-input-status="false"
                                        <?php endif; ?>
                                           data-target="#form_datetime_search_filter_from" placeholder="<?php echo lang('public_search_date_from'); ?>" />
                                    <span class="input-group-addon" data-target="#form_datetime_search_filter_from" data-toggle="datetimepicker">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-datetime-search-filter-to">
                            <div class="form-group">
                                <div class="input-group input-group-datetimepicker date" id="form_datetime_search_filter_to" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="to"
                                        <?php if(isset($filters['to'])):?>
		                                    value="<?php echo $filters['to'] ?>"
		                                    data-input-status="true"
                                        <?php else: ?>
		                                    data-input-status="false"
                                        <?php endif; ?>
                                           data-target="#form_datetime_search_filter_to" placeholder="<?php echo lang('public_search_date_to'); ?>" />
                                    <span class="input-group-addon" data-target="#form_datetime_search_filter_to" data-toggle="datetimepicker">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
	            <div class="form-filter-startdate"  <?php echo !in_array($filters['date_type'], ['startDate', 'closeDate']) ? 'style="display:none"': '' ?>>
		            <div class="form-filter-startdate">
			            <select class="form-control form-control-input" id="search_filter_startdate" name="date_period" placeholder="<?php echo isset($filters['type']) && in_array('job', $filters['type']) ? lang('public_search_closing_date') : lang('public_search_start_date'); ?>">
				            <option  value="" selected disabled><?php echo lang('public_search_select'); ?></option>
				            <option value="any" <?php echo (isset($filters['date_period']) && $filters['date_period'] === 'any') ? 'selected' : '' ?>><?php echo lang('public_search_any_time'); ?></option>
				            <option value="24h" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '24h') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_next_x_hours'), 24); ?></option>
				            <option value="3d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '3d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_next_x_days'), 3); ?></option>
				            <option value="7d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '7d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_next_x_days'), 7); ?></option>
				            <option value="14d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '14d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_next_x_days'), 14); ?></option>
				            <option value="30d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '30d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_next_x_days'), 30); ?></option>
				            <option value="customrange" <?php echo (isset($filters['date_period']) && $filters['date_period'] === 'customrange') ? 'selected' : '' ?>><?php echo lang('public_search_custom_range'); ?></option>
			            </select>
		            </div>

		            <div class="task-filter-row task-filter-row-datepicker-fromto" <?php echo in_array($filters['date_type'], ['startDate', 'closeDate']) && $filters['date_period'] === 'customrange' ? '': 'style="display:none"' ?>>
			            <div class="form-group form-datetime-search-filter-from">
				            <div class="form-group">
					            <div class="input-group input-group-datetimepicker date" id="form_datetime_search_filter_from_start" data-target-input="nearest">
						            <input type="text" class="form-control datetimepicker-input" name="from"
                                        <?php if(isset($filters['from'])):?>
								            value="<?php echo $filters['from'] ?>"
								            data-input-status="true"
                                        <?php else: ?>
								            data-input-status="false"
                                        <?php endif; ?>
                                           data-target="#form_datetime_search_filter_from_start" placeholder="<?php echo lang('public_search_date_from'); ?>" />
						            <span class="input-group-addon" data-target="#form_datetime_search_filter_from_start" data-toggle="datetimepicker">
                                        <span class="fa fa-calendar"></span>
                                    </span>
					            </div>
				            </div>
			            </div>
			            <div class="form-group form-datetime-search-filter-to">
				            <div class="form-group">
					            <div class="input-group input-group-datetimepicker date" id="form_datetime_search_filter_to_start" data-target-input="nearest">
						            <input type="text" class="form-control datetimepicker-input" name="to"
                                        <?php if(isset($filters['to'])):?>
								            value="<?php echo $filters['to'] ?>"
								            data-input-status="true"
                                        <?php else: ?>
								            data-input-status="false"
                                        <?php endif; ?>
                                           data-target="#form_datetime_search_filter_to_start" placeholder="<?php echo lang('public_search_date_to'); ?>" />
						            <span class="input-group-addon" data-target="#form_datetime_search_filter_to_start" data-toggle="datetimepicker">
                                        <span class="fa fa-calendar"></span>
                                    </span>
					            </div>
				            </div>
			            </div>
		            </div>
	            </div>
            </div>
        </div>
    </div>
    <div class="task-filter-row task-filter-row-skills">
        <button class="btn-link btn-link-filter btn-flex skillss" type="button" aria-expanded="false">
            <label class="task-filter-lbl">By skills</label>
            <span class="chevron-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <path d="M6 9l6 6 6-6" /></svg>
            </span>
        </button>
        <div class="form-filter-collapse collapse">
            <div class="form-block-task-filter">
                <div class="bstags-skills">
                    <input type="text" name="skills" id="task-skills"
                        <?php if(request('scope') === 'task' && isset($filters['skills'])):?>
		                    data-input-status="true"
		                    checked
                            value="<?php echo implode(',', $filters['skills']) ?>"
                        <?php else: ?>
		                    data-input-status="false"
                        <?php endif; ?>
                           class="bootstrap-tagsinput" placeholder="Enter skills..." />
                </div>
            </div>
        </div>
    </div>
    <div class="task-filter-row task-filter-row-reset">
        <a href="#" class="reset-filter-link reset-task-filters">Reset all filters</a>
    </div>
    <!--<div class="task-filter-row task-filter-row-reset">
	    <button class="btn-search-public" style="width:100%" id="filter-search">Search</button>
    </div>-->
</div>
<script>
	window.addEventListener('load', function(){
	    <?php if(isset($filters['type']) && in_array('job', $filters['type'])): ?>
        $('.tasks-filter .categories-input').html( $('.tasks-filter #job-categories').html() );
        <?php else: ?>
        $('.tasks-filter .categories-input').html( $('.tasks-filter #task-categories').html() );
		<?php endif ?>
	    $('.tasks-filter').find('input, select').on('change', function(e){
            $('#searchForm input[name="page"]').val("1");

	        if( $(e.currentTarget).attr('name') !== 'date_type' ) {
                if( $(e.currentTarget).attr('name') === 'date_period' && $(e.currentTarget).val() !== '' ){
                    if( $(e.currentTarget).val() !== 'customrange' ){
                        $('#searchForm input[name="perform-search"]').val('yes');
                        update_task_filters($(e.currentTarget));
                    }else{
                        $('#searchForm input[name="perform-search"]').val('no');
                        update_task_filters($(e.currentTarget));
                    }
                }else{
                    $('#searchForm input[name="perform-search"]').val('yes');
                    update_task_filters($(e.currentTarget));
                }
            }else{
	            if( $('#searchForm input[name="from"]').val().length && $('#searchForm input[name="to"]').val().length )
                    $('#searchForm input[name="perform-search"]').val('yes');
	            else
                    $('#searchForm input[name="perform-search"]').val('no');
                update_task_filters($(e.currentTarget));
	        }
	    });
        $('#form_datetime_search_filter_from, #form_datetime_search_filter_from_start, #form_datetime_search_filter_to, #form_datetime_search_filter_to_start').datetimepicker('destroy');
        $('#form_datetime_search_filter_from, #form_datetime_search_filter_to').datetimepicker({
	        format: 'DD-MMM-YYYY',
	        useCurrent: false,
	        maxDate: moment()
        });

        $('#form_datetime_search_filter_from_start, #form_datetime_search_filter_to_start').datetimepicker({
	        format: 'DD-MMM-YYYY',
	        useCurrent: false
        });

        $('#form_datetime_search_filter_from').on("change.datetimepicker", function (e) {
            $('#form_datetime_search_filter_from').datetimepicker("maxDate", moment());
            $('#form_datetime_search_filter_to').datetimepicker("maxDate", moment());
        });

        $('#form_datetime_search_filter_from').on("hide.datetimepicker", function (e) {
            var nextDay = moment(e.date).add(1, 'days');
            $('#form_datetime_search_filter_to').datetimepicker("minDate", nextDay);
        });

        $('#form_datetime_search_filter_to').on("hide.datetimepicker", function (e) {
            var prevDay = moment(e.date).subtract(1, 'days');
            $('#form_datetime_search_filter_from').datetimepicker("maxDate", prevDay);
        });

	    $('#form_datetime_search_filter_from, #form_datetime_search_filter_from_start').on('hide.datetimepicker', function(e){
            var target = $(e.currentTarget).find('input');
            target.data('inputStatus', true);
            if( $('input[name="to"]').val().length ) {
                $('#searchForm input[name="perform-search"]').val('yes');
                update_task_filters(target);
                target = null;
            }
	    });

	    $('#form_datetime_search_filter_to, #form_datetime_search_filter_to_start').on('hide.datetimepicker', function(e){
            var target = $(e.currentTarget).find('input');
	        target.data('inputStatus', true);
	        update_task_filters(target);
            target = null;
            $('#searchForm input[name="perform-search"]').val('yes');
            update_task_filters($('input[name="from"]'));
	    });

	    $('#filter-search').on('click', function(){
	        var search_form = $('#searchTaskTab #searchForm').clone();
	        search_form.find('input[name="search_name"]').remove();
            search_form[0].submit();
	    });

	    $('.reset-task-filters').on('click', function(e){
	        e.preventDefault();
	        var q = $('#searchForm input[name="q"]').val();
            $("#searchForm button.btn-link-filter.is-clicked").trigger('click');
            $('#searchForm #task-skills').tagsinput('removeAll');
            document.getElementById('searchForm').reset();
            $('.tasks-filter input').val('');
            if(q.length) q = '&q=' + q;
            window.location = "<?php echo option('site_uri') . url_for('/workspace/search') . '?scope=task' ?>"+q;
	    })

	    <?php if(isset($filters['category']) && !empty($filters['category'])): ?>
        $(".tasks-filter button.btn-link-filter.category").trigger('click');
        <?php endif ?>

	    <?php if((isset($filters['state']) && !empty($filters['state'])) || (isset($filters['city']) && !empty($filters['city']))): ?>
        $(".tasks-filter button.btn-link-filter.location").trigger('click');
        <?php endif ?>

	    <?php if(isset($filters['task_type']) && !empty($filters['task_type'])): ?>
        $(".tasks-filter button.btn-link-filter.nature").trigger('click');
        <?php endif ?>

	    <?php if(isset($filters['task_budget']) && !empty($filters['task_budget'])): ?>
        $(".tasks-filter button.btn-link-filter.task-budget").trigger('click');
        <?php endif ?>

	    <?php if(isset($filters['date_type']) && !empty($filters['date_type'])): ?>
        $(".tasks-filter button.btn-link-filter.date").trigger('click');
        $('#searchForm input[name="perform-search"]').val('no');
        <?php endif ?>

	    <?php if(isset($filters['date_period']) && !empty($filters['date_period'])): ?>
		<?php if($filters['date_period'] === 'customrange'): ?>
        $("#search_filter_dateposted").parent().next().show();
		<?php endif ?>
        $('#searchForm input[name="perform-search"]').val('no');
        <?php endif ?>

        $(".btn-load-more.more-tasks").on('click', function(e){
            var page = $('#searchForm input[name="page"]').val();
            $('#searchForm input[name="page"]').val(parseInt(page)+1);
            get_result('load_more');
        });

        $('#searchForm input[name="search_name"]').keydown(function(e){
            if(e.keyCode === 13){
                $('.save-search').trigger('click');
                return false;
            }
        })

        $('.save-search').on('click', function(e){
            var search_name = $('input[name="search_name"]');
            var search_form = $('#searchForm');
            var icon = '', title = '', className = '', message = '';
            if(search_name.val().length){
	            $.ajax({
		            url: "<?php echo option('site_uri') . url_for('/search/save') ?>",
		            method: 'POST',
		            data: search_form.serialize()
	            }).done(function(response){
	                search_name.val('');
                    $.notify({
                        title: '<b>Success!</b>',
                        message: response.message,
                        icon: "fa fa-check"
                    }, {
                        style: 'metro',
                        type: 'success',
                        globalPosition: 'top right',
                        showAnimation: 'slideDown',
                        hideAnimation: 'slideUp',
                        showDuration: 200,
                        hideDuration: 200,
                        autoHide: true,
                        clickToHide: true
                    });
	            });
            }else{
                $.notify({
                    title: '<b>Error!</b>',
                    message: "Please type a search name.",
                    icon: "fa fa-exclamation"
                }, {
                    style: 'metro',
                    type: 'danger',
                    globalPosition: 'top right',
                    showAnimation: 'slideDown',
                    hideAnimation: 'slideUp',
                    showDuration: 200,
                    hideDuration: 200,
                    autoHide: true,
                    clickToHide: true
                });
            }
        });

        $('#searchForm').on('submit', function(){
            $('input[name="search_name"]').attr('disabled', 'disabled');
        });

        var current_mode = localStorage.getItem('mode') || 'od-mode';
        if(current_mode === 'ft-mode') {
            $('.individual #searchTaskTab #filtertj_2').prop('checked', true).trigger('change');
        }

        var skills = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '<?php echo option('site_uri') . url_for('ajax/skills.json') ?>?all=1'
        });
        skills.clearPrefetchCache();
        skills.initialize();

        $('.tasks-filter #task-skills').tagsinput({
            allowDuplicates: false,
            confirmKeys: [9, 13, 44, 188],
            itemValue: 'id',
            itemText: 'name',
            typeaheadjs: {
                name: 'skills',
                displayKey: 'name',
                source: skills.ttAdapter()
            }
        });

        <?php if(isset($filters['skills']) && !empty($filters['skills']) && $scope === 'task'): ?>
        $(".tasks-filter button.btn-link-filter.skillss").trigger('click');
        <?php if(!is_array($filters['skills'])) $filters['skills'] = [$filters['skills']]; ?>
        $('.tasks-filter #task-skills').off('change');
        <?php echo json_encode($filters['skills_list']) ?>.forEach(function(item, idx){
            $('.tasks-filter #task-skills').tagsinput('add', item);
        });
        $('.tasks-filter #task-skills').on('change', function(e){
            if( $(e.currentTarget).attr('name') !== 'date_type' ) {
                if( $(e.currentTarget).attr('name') === 'date_period' && $(e.currentTarget).val() !== '' ){
                    if( $(e.currentTarget).val() !== 'customrange' ){
                        $('#searchForm input[name="perform-search"]').val('yes');
                    }
                }else {
                    $('#searchForm input[name="perform-search"]').val('yes');
                }
            }
            $('#searchForm input[name="page"]').val("1");
            update_task_filters($(e.currentTarget));
        });
        <?php endif ?>

	});

    function update_task_filters(target){
	    if(target === undefined){
            var inputs = $('.tasks-filter').find('input, select');
	    }else{
            var inputName = target.attr('name');
            var inputs = target;
            if(target.attr('type') === 'checkbox') inputs = $('.tasks-filter').find('[name="'+inputName+'"]');
	    }

        var taskType = '';
        var scope = '';
        var categories = '';
        var value = '';
        var input = $('#searchForm input[name="'+inputName+'"]');

        inputs.each(function(idx, item){
            if(
                ((item.type === 'checkbox' && item.checked) ||
                    (item.type === 'text' && item.value !== '') ||
                    (item.type === 'select-one' && item.value !== ''))
            ) {
                if(item.name === 'type' || item.name === 'category' || item.name === 'task_type'){
                    if(item.name === 'category') {
                        if(categories === '')
                            categories += item.value;
                        else
                            categories +=','+item.value;
                    }else if(item.name === 'type') {
                        if(scope === '')
                            scope += item.value;
                        else
                            scope +=','+item.value;
                    }else{
                        if(taskType === '')
                            taskType += item.value;
                        else
                            taskType +=','+item.value;
                    }
                }else {
                    if(item.name === 'category') categories = item.value;
                    else if(item.name === 'type') scope = item.value;
                    else if(item.name === 'task_type') taskType = item.value;
                    else value = item.value;
                }

                if(scope !== '') value = scope;
                else if(categories !== '') value = categories;
                else if(taskType !== '') value = taskType;

                if(value.length) {
                    input.removeAttr('disabled');
                    input.val(value);
                    if(target !== undefined) target.data('inputStatus', true);
                    else $(item).data('inputStatus', true);
                }else{
                    input.val('');
                    input.attr('disabled', 'disabled');
                    if(target !== undefined) target.data('inputStatus', false);
                    else $(item).data('inputStatus', false);
                }
            }else{
                if(!value.length){
                    input.val('');
                    input.attr('disabled', 'disabled');
                    if(target !== undefined) target.data('inputStatus', false);
                    else $(item).data('inputStatus', false);
                }
            }
        });

        if($('#searchForm input[name="perform-search"]').val() === 'yes')
            get_result();
    }

    function get_result(load_more = null){
        var search_form = $('#searchTaskTab #searchForm').clone();
        search_form.find('input[name="search_name"]').remove();

	    var page = $('#searchForm input[name="page"]').val(),
		    url = "<?php echo option('site_uri') . url_for('/workspace/search') ?>/"+page,
	        data = search_form.serialize();

	    request_tasks(url, data, load_more);
    }

    function request_tasks(url, data, load_more = null, update = null){

        $.ajax({
            url: url,
            data: data,
        }).done(function(response){
            if(response.more){
                $(".btn-load-more.more-tasks").show();
            }else{
                $(".btn-load-more.more-tasks").hide();
            }
            if(load_more === null) {
                $('.task-result-container').html(response.html);
            } else {
                var currentHeight = $('.task-result-container').height();
                $('.task-result-container').append(response.html);
                $('html')[0].scrollTo({ top: currentHeight+200, left: 0, behavior: 'smooth'});
            }
            if(response.keywords !== ''){
                $('#searchTaskTab .col-search-terms-result-value').text(response.keywords);
                $('#searchTaskTab .keyword-filter-container > span').text(response.keywords);
                $('#searchTaskTab .col-search-terms-result').show();
            }else{
                $('#searchTaskTab .col-search-terms-result').hide();
            }
            if( $('.col-task-row-wrapper').css('display') === 'none' )
                $('.col-task-row-wrapper').fadeIn();

            var activeTab = $('#pills-tabContent').find('.tab-pane.active').attr('id');
            if( activeTab === 'searchTaskTab' ) {
                var stateUrl = data.length > 0 ? url + '?' + data : url;
                if (update === null) {
                    history.pushState({
                        'taskHtml': response.html,
                        'taskUrl': url,
                        'taskData': data
                    }, '', stateUrl);
                }else {
                    history.replaceState({
                        'taskHtml': response.html,
                        'taskUrl': url,
                        'taskData': data
                    }, '', stateUrl);
                }
            }
        });
    }

	window.addEventListener('popstate', function(e){
        if(e.state){
            $('#searchTaskTab .task-result-container').html(e.state.taskHtml);
            $('#searchTalentTab .talent-result-container').html(e.state.talentHtml);

            var activeTab = $('#pills-tabContent').find('.tab-pane.active').attr('id');
            if( activeTab === 'searchTaskTab' ){
                request_tasks(e.state.taskUrl, e.state.taskData, null, 'update');
            }else{
                window.request_talents(e.state.talentUrl, e.state.talentData, null, 'update');
            }
        }
	})
</script>