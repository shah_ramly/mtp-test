<div class="card-body-tasklisting card-body-taskdetails">
	<ul class="nav nav-pills nav-pills-taskdetailstitle">
		<li class="nav-item">
			<a href="javascript: window.history.back()" class="breadcrumbs-link breadcrumbs-link-title">&lt;</a>
			<a class="nav-link active"><?php echo lang('task_details_title'); ?></a>
		</li>
	</ul>
    <div class="col-xl-12">
        <div class="row">
            <div class="col-xl-12 col-taskdetails-top">
	            <!--<div class="breadcrumbs-back"><a href="javascript: window.history.back()" class="breadcrumbs-link">&lt; <?php /*echo lang('task_details_back'); */?></a></div>-->
                <div class="col-task-details-wrapper">
                    <div class="col-task-details-header">
                        <div class="task-details-title-wrapper">
                            <h3 class="task-details-title text-translate"><?php echo $task['title']; ?></h3>
	                        <div class="task-details-row task-details-category">
		                        <span class="taskcentre-details-val val-taskcat"><?php echo $task['category_name'] ?></span>
		                        <span class="taskcentre-details-val val-taskcatsymbol">&gt;</span>
		                        <span class="taskcentre-details-val val-tasksubcat"><?php echo $task['subcategory_name'] ?></span>
	                        </div>
                        </div>
                    </div>
	                <div class="col-task-details-flex col-task-details-box-container">
		                <div class="col-task-details-left">
			                <div class="col-task-details-box">
				                <div class="task-details-row task-details-desc-cost">
					                <div class="task-details-row task-details-about">
						                <label class="task-details-lbl"><?php  echo lang('task_description')?></label>
						                <div class="task-details-about-block text-translate">
							                <?php echo nl2br($task['description']) ?>
						                </div>
										<a href="#" class="btn-translate d-none"><?php echo lang('task_details_view_translation') ?></a>
										<input type="hidden" name="current_lang">
					                </div>
					                <div class="task-details-row task-details-cost-breakdown">
						                <label class="task-details-lbl"><?php echo lang('post_step_4_task_value') ?></label>
						                <div class="col-task-details-amount">
							                <div class="task-row-taskamount">
								                <span class="taskamount-prefix">RM</span>
								                <span class="taskamount-val"><?php echo $task['hours'] > 0 ? number_format($task['budget'], 0) : number_format($task['budget'], 0) ?></span>
								                <span class="taskamount-suffix">.00</span>
							                </div>
						                </div>

						                <div class="taskcentre-details-row row-taskcostbreakdown">
							                <div class="taskcentre-details-row row-taskhourlyrate">
								                <label class="taskcentre-details-lbl lbl-taskhourlyrate"><?php echo lang('task_details_cost_breakdown') ?></label>
                                                <?php if($task['task_type'] === 'lump-sum'): ?>
								                <div class="taskcentre-details-val val-taskbythehour">RM<?php echo number_format($task['budget'], 2) ?></div>
								                <?php else: ?>
								                <div class="taskcentre-details-val val-taskbythehour">RM<?php echo number_format($task['budget']/$task['hours'], 2) ?>/hr</div>
								                <div class="taskcentre-details-val val-taskhourlyrate">
									                <span class="subval-taskcentre val-taskhourlyrate">x <?php echo $task['hours'] ?></span>
									                <span class="subicon-taskcentre clock-icon">
                                                        <span class="bi-tooltip" data-toggle="tooltip" data-placement="auto"
                                                              data-template="<div class='tooltip tooltip-fancy tooltip-tc-date' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>"
                                                              data-title="<?php echo $task['hours'] .' '. ($task['hours'] == 1 ? lang('task_hour') : lang('task_hours')) ?>" data-original-title="" title="">
                                                            <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-clock" viewBox="0 0 16 16">
                                                                <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"></path>
                                                                <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"></path>
                                                            </svg>
                                                        </span>
                                                    </span>
								                </div>
								                <?php endif ?>
							                </div>
						                </div>

						                <div class="task-details-title-components">
							                <span class="task-details-type"><?php echo lang('task_on_demand') ?></span>
							                <span class="task-details-posted">
                                                <span class="task-details-posted-icon"><svg viewBox="0 0 16 16" class="bi bi-calendar" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1zm1-3a2 2 0 0 0-2 2v11a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2z" />
                                                        <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1a.5.5 0 0 1-1 0V.5a.5.5 0 0 1 .5-.5zm9 0a.5.5 0 0 1 .5.5V1a.5.5 0 0 1-1 0V.5a.5.5 0 0 1 .5-.5z" /></svg></span>
                                                <span class="task-details-location-val">
	                                                <?php if( isset($applied_at[$task['task_id']]) ): ?>
	                                                <?php echo lang('task_applied_on') .' '. \Carbon\Carbon::parse($applied_at[$task['task_id']])->format('d F Y'); ?>
	                                                <?php else: ?>
	                                                <?php echo lang('task_posted') .' '. \Carbon\Carbon::parse($task['created_at'])->diffForHumans(); ?>
	                                                <?php endif ?>
                                                </span>
                                            </span>
						                </div>
						                <div class="task-details-row task-details-date">
							                <div class="task-details-row task-details-startdate">
								                <label class="task-details-lbl"><?php  echo lang('task_details_start_date')?>
									                <span class="bi-tooltip" data-toggle="tooltip" data-placement="top"
									                      title="<?php echo lang('task_details_start_date_tooltip') ?>">
                                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                        </svg>
                                                    </span>
								                </label>
								                <div class="task-details-deadline-block"><?php echo \Carbon\Carbon::parse($task['start_by'])->format('d F Y'); ?></div>
							                </div>
							                <div class="task-details-row task-details-deadline">
								                <label class="task-details-lbl"><?php  echo lang('task_details_complete_by')?>
									                <span class="bi-tooltip" data-toggle="tooltip" data-placement="top"
									                      title="<?php echo lang('task_details_complete_by_tooltip') ?>">
                                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                        </svg>
                                                    </span>
								                </label>
								                <div class="task-details-deadline-block"><?php echo \Carbon\Carbon::parse($task['complete_by'])->format('d F Y'); ?></div>
							                </div>
						                </div>
					                </div>
				                </div>
                                <?php echo partial('workspace/partial/task-comments.html.php',
                                    [
                                        'post_id'      => $task['task_id'],
                                        'post_user_id' => $task['user_id'],
                                        'user_id'      => $current_user['id'],
                                        'current_user' => $current_user,
                                        'comments'     => $task['comments'],
                                        'answers'      => $task['answers'],
                                        'post'         => $task
                                    ]); ?>

			                </div>
		                </div>
		                <div class="col-task-details-right">
			                <div class="col-task-details-box">
				                <div class="task-details-row task-details-profile">
					                <label class="task-details-lbl" data-toggle="collapse" data-target="#taskDetailsPoster" aria-expanded="true" aria-controls="taskDetailsPoster">
						                <i class="fa fa-caret-down"></i>
						                <span><?php  echo lang('task_details_poster_details')?></span>
					                </label>
					                <div class="collapse show" id="taskDetailsPoster">
                                        <?php $poster_type = isset($task['poster']['company']) ? 'company' : 'talent'; ?>
						                <div class="profile-wrapper">
							                <div class="profile-avatar-wrapper">
								                <div class="profile-avatar">
									                <a href="<?php echo url_for("{$poster_type}/{$task['poster']['number']}") ?>" class="task-details-poster-link">
										                <img class="avatar-img" src="<?php echo imgCrop($task['poster']['photo'], 100, 100, 'assets/img/default-avatar.png') ?>" alt="">
									                </a>
								                </div>
								                <div class="profile-avatar-details">
									                <a href="<?php echo url_for("{$poster_type}/{$task['poster']['number']}") ?>" class="task-details-poster-link">
										                <h5 class="profile-name"><?php echo ($task['poster']['type'] === '1') ? $task['poster']['company']['name'] : "{$task['poster']['firstname']} {$task['poster']['lastname']}"; ?></h5>
									                </a>
									                <div class="task-details-profile-location">
                                                        <span class="task-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" /></svg></span>
										                <span class="task-details-profile-location-val"><?php echo "{$task['poster']['state']}, {$task['poster']['country']}"; ?></span>
									                </div>
									                <div class="task-details-profile-rating">
										                <div class="task-details-profile-rating-star">
                                                            <?php $starred = (int)floor($task['rating']['overall']/2); $rest = 5 - $starred; ?>
                                                            <?php if($starred > 0): ?>
                                                                <?php foreach (range(1, $starred) as $star): ?>
													                <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                            <?php if($rest > 0): ?>
                                                                <?php foreach (range(1, $rest) as $star1): ?>
													                <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
										                </div>
										                <div class="task-details-profile-rating-val">
											                <span class="task-row-average-rating-val"><?php echo number_format((float)$task['rating']['overall'], 1); ?></span>
											                <span class="task-row-divider">/</span>
											                <span class="task-row-total-rating-val"><?php echo $task['rating']['count'] .' '. lang('task_details_reviews'); ?></span>
										                </div>
									                </div>
									                <div class="profile-info-details">
										                <div class="task-details-profile-published">
											                <span class="task-details-profile-label"><?php  echo lang('task_published')?>:</span>
											                <span class="task-details-profile-val"><?php echo $task['published_count']; ?></span>
										                </div>
										                <div class="task-details-profile-dispute">
											                <span class="task-details-profile-val"><?php  echo lang('task_details_dispute_resolution');?> </span>
											                <span class="bi-tooltip" data-toggle="tooltip" data-placement="top"
											                      title="<?php echo lang('this_user_has_adopted_dispute_resolution_for') .' '. $task['disputed_count'] .' '. lang('dispute_resolution_out_of') .' '. $task['published_count'] .' '. lang('dispute_resolution_projects_undertaken') ?>">
                                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                                </svg>
                                                            </span>
										                </div>
										                <div class="task-details-profile-membersince">
											                <span class="task-details-profile-label"><?php  echo lang('task_details_member_since');?>:</span>
											                <span class="task-details-profile-val"><?php echo Carbon\Carbon::parse($task['date_created'])->format('F Y'); ?></span>
										                </div>
									                </div>
								                </div>
							                </div>
						                </div>
					                </div>
				                </div>
				                <div class="task-details-row task-details-alldetails">
					                <label class="task-details-lbl" data-toggle="collapse" data-target="#taskDetailsAll" aria-expanded="false" aria-controls="taskDetailsAll">
						                <i class="fa fa-caret-down"></i>
						                <span><?php echo lang('task_details_location') ?></span>
					                </label>
					                <div class="collapse" id="taskDetailsAll">
						                <div class="task-details-row task-details-taskloc">
                                            <?php if( $task['location'] === 'remotely' ): ?>
							                <span class="task-details-location-val"><?php echo lang('this_task_can_be_done_remotely') ?></span>
							                <?php else: ?>
							                <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCKkk8Hj4PMcGn7bheNToZyDVJixuLsVL8&q=<?php echo urlencode($task['location']) ?>" width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
							                <?php endif ?>
						                </div>
					                </div>
				                </div>
				                <div class="task-details-row task-details-skills">
					                <label class="task-details-lbl" data-toggle="collapse" data-target="#taskDetailsSkills" aria-expanded="false" aria-controls="taskDetailsSkills">
						                <i class="fa fa-caret-down"></i>
						                <span><?php  echo lang('task_details_skills')?> <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo lang('task_details_skills_tooltip') ?>">
                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                </svg>
                                            </span>
                                        </span>
					                </label>
					                <div class="collapse" id="taskDetailsSkills">
						                <div class="task-details-deadline-block">
                                        <?php if(!empty($task['skills'])): ?>
                                        <?php foreach ($task['skills'] as $skill): ?>
							                <a href="#" class="skills-label-link">
			                                    <span class="tag label label-info bubble-lbl">
				                                    <?php echo $skill['name']; ?>
			                                    </span>
							                </a>
                                        <?php endforeach; ?>
						                <?php endif ?>
						                </div>
					                </div>
				                </div>
			                </div>
                            <?php if( !in_array($task['status'], ['in-progress', 'completed', 'closed']) && $task['user_id'] !== $current_user['id'] && $current_user['type'] === '0' ): ?>
			                <div class="task-details-row task-details-actions">
				                <div class="task-details-actions-fav">
					                <form method="post" action="<?php echo url_for('/workspace/task/favourite') ?>">
                                        <?php echo html_form_token_field(); ?>
						                <input type="hidden" name="task_id" value="<?php echo $task['task_id']; ?>">
						                <input type="hidden" name="favourited" value="<?php echo in_array($task['task_id'], $task_favourites['ids']) ? "true":"false"; ?>">
						                <input type="hidden" name="url" value="<?php echo url_for("/workspace/task/{$task['slug']}/details"); ?>">
						                <button type="submit" class="btn-icon-full <?php echo in_array($task['task_id'], $task_favourites['ids']) ? 'btn-unfav':'btn-fav' ?>"
                                            <?php echo ($task['user_id'] === $current_user['id'] || (!in_array($task['task_status'], ['published', 'in-progress']) && !in_array($task['task_id'], $task_favourites['ids']))) ? 'disabled="disabled"' : '' ?> style="min-width:100%;max-width:100%"
						                >
							                <span class="btn-label"><?php echo in_array($task['task_id'], $task_favourites['ids']) ? lang('task_remove_fav'):lang('task_save_fav') ?></span>
							                <span class="btn-icon">
	                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
	                                                <path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path>
	                                            </svg>
	                                        </span>
						                </button>
					                </form>
				                </div>
				                <div class="task-details-actions-apply">
					                <form name="form_task_question_<?php echo $task['task_id']; ?>" id="form_task_question_<?php echo $task['task_id']; ?>"  method="post" action="<?php echo url_for('/workspace/task/apply') ?>">
                                        <?php echo html_form_token_field(); ?>
						                <input type="hidden" name="task_id" value="<?php echo $task['task_id']; ?>">
						                <input type="hidden" name="url" value="<?php echo url_for("/workspace/task/{$task['slug']}/details"); ?>">
						                <button
                                            <?php if(!empty($task['questions']) || (int)$completion < 100): ?>
								                type="button"
                                            <?php else: ?>
								                type="submit"
                                            <?php endif; ?>
                                            <?php echo ($task['user_id'] === $current_user['id']|| !in_array($task['task_status'], ['published', 'in-progress']) || in_array($task['task_id'], $task_applied['ids'])) ? 'disabled="disabled"' : '' ?>
								                class="btn-icon-full <?php echo in_array($task['task_id'], $task_applied['ids']) ? 'btn-fav':'btn-apply'; ?>"
								                style="min-width:100%;max-width:100%"
                                            <?php if(!empty($task['questions'])  && (int)$completion === 100): ?>
								                data-toggle="modal" data-target="#modal_apply_question_<?php echo $task['task_id']; ?>"
                                            <?php elseif((int)$completion !== 100): ?>
								                data-toggle="modal" data-target="#modal_complete_profile"
                                            <?php endif; ?>
						                >
	                                        <span class="btn-label">
		                                        <?php echo in_array($task['task_id'], $task_applied['ids']) ? lang('task_details_applied'):lang('task_details_apply_this_task'); ?>
	                                        </span>
							                <span class="btn-icon">
	                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
	                                                <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
	                                                <polyline points="22 4 12 14.01 9 11.01"></polyline>
	                                            </svg>
	                                        </span>
						                </button>
					                </form>
				                </div>
				                <div class="task-row-taskid task-row-taskid-taskdetails">
									<div class="task-id-flex">
										<span class="task-row-taskid-lbl"><?php  echo lang('task_id')?>:</span>
										<span class="task-row-taskid-val"><?php echo $task['number'] ?></span>
									</div>
									<div class="task-report-flex">
										<button class="report-task-link gtm-report-task" data-report="<?php echo $task['task_id'] ?>" data-select="Report Inappropriate Content" data-toggle="modal" data-target="#modal_feedback"><?php echo lang('report_inappropriate_content')?></button>
									</div>
				                </div>
			                </div>
			                <?php endif ?>
		                </div>
	                </div>

                </div>

            </div>
        </div>
    </div>

</div>
<?php if(!empty($task['questions']) && $task['user_id'] !== $current_user['id']  && (int)$completion === 100): ?>
	<!-- Modal - Seeker's Questionnaire on Task Application -->
	<div id="modal_apply_question_<?php echo $task['task_id'] ?>" class="modal modal-apply-question fade" aria-labelledby="modal_custom_widget" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><?php echo lang('post_step_2_post_requested_participation_questionnaire'); ?></h5>
				</div>
				<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
						<line x1="18" y1="6" x2="6" y2="18"></line>
						<line x1="6" y1="6" x2="18" y2="18"></line>
					</svg>
				</button>
				<div class="modal-body">
					<form name="form_task_question_<?php echo $task['task_id']; ?>" id="form_task_question" method="post" action="<?php echo url_for('/workspace/task/apply') ?>">
                        <?php echo html_form_token_field(); ?>
						<input type="hidden" name="task_id" value="<?php echo $task['task_id']; ?>">
						<input type="hidden" name="url" value="<?php echo url_for('/workspace/search'); ?>">
                        <?php $count = count($task['questions'][$task['task_id']]); $counter = 1; ?>
                        <?php foreach($task['questions'][$task['task_id']] as $question): ?>
							<div id="apply_question_<?php echo $counter ?>" class="frm form-apply-question"
                                <?php echo ($counter >= 2) ? ' style="display:none;"' : '' ?>
							>
								<div class="modal-steps-title"><?php echo lang('task_question') ?> <?php echo $counter ?> <?php echo lang('task_question_of') ?> <?php echo $count ?></div>
								<div class="task-question"><?php echo $question['name'] ?></div>
								<textarea class="form-control form-control-input" name="answer[<?php echo $question['id'] ?>]" placeholder="<?php echo lang('task_question_your_answer') ?>" rows="3" id="taskQuestion<?php echo $counter ?>" required></textarea>
								<div class="form-group button-container">
                                    <?php if($counter == 1) : ?>
                                        <?php if($count > 1): ?>
											<button type="button" class="btn-icon-full btn-step-next qa-open<?php echo $counter ?>">
												<span class="btn-label"><?php echo lang('post_next') ?></span>
												<span class="btn-icon">
		                                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
		                                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
		                                        </svg>
		                                    </span>
											</button>
                                        <?php else: ?>
											<button type="submit" class="btn-icon-full btn-step-next qa-open<?php echo $counter ?>">
												<span class="btn-label"><?php echo lang('post_complete') ?></span>
												<span class="btn-icon">
		                                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
		                                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
		                                        </svg>
		                                    </span>
											</button>
                                        <?php endif ?>
                                    <?php else: ?>
										<button type="button" class="btn-icon-full btn-step-prev qa-back<?php echo $counter ?>">
											<span class="btn-label"><?php echo lang('post_back') ?></span>
											<span class="btn-icon">
	                                    <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                        <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
	                                </span>
										</button>
                                        <?php if($counter < $count): ?>
											<button type="button" class="btn-icon-full btn-step-next qa-open<?php echo $counter ?>">
												<span class="btn-label"><?php echo lang('post_next') ?></span>
												<span class="btn-icon">
	                                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                                    </svg>
	                                </span>
											</button>
                                        <?php else: ?>
											<button type="submit" class="btn-icon-full btn-step-next">
												<span class="btn-label"><?php echo lang('post_complete') ?></span>
												<span class="btn-icon">
	                                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                                        </svg>
	                                    </span>
											</button>
                                        <?php endif ?>
                                    <?php endif ?>
								</div>
							</div>
                            <?php $counter++; ?>
                        <?php endforeach; ?>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->
<?php endif ?>

<script>
	$(document).ready(function(){
		if( !$('[name="current_lang"]').val() ){
			detect( $('.task-details-title').text() );
		}
		
		$(document).on('click', '.btn-translate', function(){
			el = $(this);

			if( !el.hasClass('translated') ){
				$('.text-translate').each(function(){
					el2 = $(this);
					if(!el2.data('original')){
						el2.data('original', el2.html());
					}
					translate( el2.html(), $('[name="current_lang"]').val(), '<?php echo strtolower($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) == 'bm' ? 'ms' : strtolower($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']); ?>', el2)
				});
				el.text('<?php echo lang('task_details_view_original') ?>').addClass('translated');

			}else{

				$('.text-translate').each(function(){
					el2 = $(this);
					el2.html( el2.data('original') );
				});
				el.text('<?php echo lang('task_details_view_translation') ?>').removeClass('translated');;
			}			
		});
	});
	
	function translate(text, from, to, element){
		$.ajax({
			type: 'POST',
			url: 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyCKkk8Hj4PMcGn7bheNToZyDVJixuLsVL8',
			dataType : 'json',
			data: { source: from, target: to, q: text },
			success: function(results){
				if(results.data.translations[0].translatedText){
					element.html(results.data.translations[0].translatedText);
				}
			},
			error: function(error){
				ajax_error(error);
			},
			complete: function(){}
		});
	}

	function detect(text){
		lang = '';
		var translated = '';
		var url = "https://translation.googleapis.com/language/translate/v2/detect?key=AIzaSyCKkk8Hj4PMcGn7bheNToZyDVJixuLsVL8";
		url += "&q=" + text;

        $.get(url, function (data, status) {
			if(data.data.detections[0][0].language){
				lang = data.data.detections[0][0].language;
				$('[name="current_lang"]').val(lang);

				if(lang != '<?php echo strtolower($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) == 'bm' ? 'ms' : strtolower($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) ; ?>'){
					$('.btn-translate').removeClass('d-none');
				}
			}
		});
	}

	$('#post-comment').on('submit', function(e){
		$(this).find('button[type="submit"]').prop('disabled', true).css({'backgroundColor': '#eaeaea!important','color': '#ddd!important'});
	});

	$('input[name="comment"]').on('keyup', function(){
	    if( $(this).val().length ){
            $('#post-comment').find('button[type="submit"]').prop('disabled', false).removeAttrs('style');
	    }else{
            $('#post-comment').find('button[type="submit"]').prop('disabled', true).css({'backgroundColor': '#eaeaea!important','color': '#ddd!important'});
	    }
	});

	$('#form_task_question').on('submit', function(e){
		$(this).find('button[type="submit"]').prop('disabled', true);
	});
</script>