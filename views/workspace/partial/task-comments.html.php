<?php if( $user->info['type'] !== '1' || $post_user_id === $user->info['id'] ):


if($post['user_id'] !== $current_user['id']) {
    $comments = array_filter($comments, function ($comment) use ($current_user, $post) {
        return ! (bool) $comment['hidden'] || ((bool)$comment['hidden'] && $comment['user_id'] === $current_user['id']);
    });
}

?>
<div class="task-details-row task-details-comment">
	<label class="task-details-lbl" data-toggle="collapse" data-target="#taskDetailsQuestion" aria-expanded="true" aria-controls="taskDetailsQuestion">
		<span>Q & A (<?php echo count($comments) ?>)</span><i class="fa fa-caret-down"></i>
	</label>
	<div class="collapse show" id="taskDetailsQuestion">
		<div class="task-details-comment-header signup-prompt">
			<?php $status = isset($post['task_status']) ? $post['task_status'] : $post['job_status'] ?>
	        <?php if($post_user_id !== $user_id && in_array($status, ['published', 'in-progress', 'marked-completed'])): ?>
			<ul class="task-details-comments-list">
				<li class="task-details-comment-row">
					<a target="_blank" href="#" class="rf">
						<div class="task-details-profile-photo">
							<img src="<?php echo imgCrop($current_user['photo'], 35, 35, 'assets/img/default-avatar.png') ?>" alt="">
						</div>
					</a>
					<div class="task-details-comment-text task-details-comment-textarea-wrapper">
						<form id="post-comment" name="post-comment" method="post" action="<?php echo url_for('/workspace/comments/create') ?>">
							<?php echo html_form_token_field() ?>
							<div class="task-details-comment-text-wrap">
								<!--<textarea class="form-control form-control-input" name="comment" placeholder="<?php /*echo lang('task_details_what_would_you_like_to_know') */?>" rows="3"></textarea>-->
								<input class="form-control form-control-input" name="comment" placeholder="<?php echo lang('task_details_what_would_you_like_to_know') ?>" />
								<input type="hidden" name="post_id" value="<?php echo $post_id; ?>" />
								<input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
								<input type="hidden" name="post_type" value="<?php echo isset($post['task_id']) ? 'task' : 'job' ?>" />
							</div>
							<div class="task-details-comment-actions">
								<button type="submit" class="btn-full btn-post" disabled style="background-color:#eaeaea!important;color:#ddd!important">
									<span class="btn-label"><?php echo lang('task_details_ask_a_question'); ?></span>
								</button>
							</div>
						</form>
					</div>
				</li>
			</ul>
	        <?php endif ?>
		</div>
		<div class="task-details-comment-container">
			<?php if(!empty($comments)): ?>
			<ul class="task-details-comments-list">
				<?php foreach ($comments as $comment): ?>
				<li class="task-details-comment-row">
					<a target="_blank" href="#" class="rf">
						<div class="task-details-profile-photo">
							<img src="<?php echo imgCrop($comment['photo'], 35, 35, 'assets/img/default-avatar.png') ?>" alt="">
						</div>
					</a>
					<div class="task-details-comment-text">
						<div class="task-details-comment-user-date-wrap">
							<a class="task-details-username-link" href="#">
								<?php echo "{$comment['firstname']} {$comment['lastname']}"; ?>
							</a>
							<span class="task-details-comment-date">
								<?php echo \Carbon\Carbon::parse($comment['created_at'])->diffForHumans(); ?>
							</span>
						</div>
						<div class="task-details-comment-text-wrap">
							<div class="task-details-comment-text">
								<?php echo $comment['comment'] ?>
							</div>
						</div>
						<?php if($user_id === $post_user_id): ?>
						<div class="task-details-comment-actions">
							<a href="#" class="task-details-comment-action action-reply <?php echo $comment['hidden'] ? 'hide' : '' ?>"
							   data-post-id="<?php echo $comment['post_id'] ?>"
							   data-parent-id="<?php echo $comment['comment_id'] ?>"
							   data-toggle="modal"
							   data-posted-by="<?php echo "{$comment['firstname']} {$comment['lastname']}"; ?>"
							   data-target="#modal_public_question">
								<?php if(in_array($status, ['published', 'in-progress', 'marked-completed'])): ?>
								<span class="action-text"><?php echo lang('answer_question') ?></span>
								<?php endif ?>
							</a>

							<a href="#" class="task-details-comment-action action-hide is-not-hidden <?php echo $comment['hidden'] ? 'hide' : '' ?>"
							   data-update-state="1"
							   data-comment-id="<?php echo $comment['comment_id'] ?>"
							   data-post-id="<?php echo $comment['post_id'] ?>"
							   data-post-type="<?php echo isset($post['task_id']) ? 'task' : 'job' ?>"
							   title="<?php echo lang('hide_this_question') ?>">
                                <span class="action-hide-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
                                      <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
                                      <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
                                    </svg>
                                </span>
							</a>

							<a href="#" class="task-details-comment-action action-hide is-hidden <?php echo $comment['hidden'] ? '' : 'hide' ?>"
							   data-update-state="0"
							   data-comment-id="<?php echo $comment['comment_id'] ?>"
							   data-post-id="<?php echo $comment['post_id'] ?>"
							   data-post-type="<?php echo isset($post['task_id']) ? 'task' : 'job' ?>"
							   title="<?php echo lang('this_question_is_hidden') ?>">
                                <span class="action-hide-icon hidden-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-eye-slash" viewBox="0 0 16 16">
                                      <path d="M13.359 11.238C15.06 9.72 16 8 16 8s-3-5.5-8-5.5a7.028 7.028 0 0 0-2.79.588l.77.771A5.944 5.944 0 0 1 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.134 13.134 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755-.165.165-.337.328-.517.486l.708.709z"/>
                                      <path d="M11.297 9.176a3.5 3.5 0 0 0-4.474-4.474l.823.823a2.5 2.5 0 0 1 2.829 2.829l.822.822zm-2.943 1.299l.822.822a3.5 3.5 0 0 1-4.474-4.474l.823.823a2.5 2.5 0 0 0 2.829 2.829z"/>
                                      <path d="M3.35 5.47c-.18.16-.353.322-.518.487A13.134 13.134 0 0 0 1.172 8l.195.288c.335.48.83 1.12 1.465 1.755C4.121 11.332 5.881 12.5 8 12.5c.716 0 1.39-.133 2.02-.36l.77.772A7.029 7.029 0 0 1 8 13.5C3 13.5 0 8 0 8s.939-1.721 2.641-3.238l.708.709zm10.296 8.884l-12-12 .708-.708 12 12-.708.708z"/>
                                    </svg>
                                </span>
							</a>

						</div>
						<?php endif; ?>
					</div>
				</li>
				<?php if(isset($answers[$comment['comment_id']])): ?>
				<?php foreach($answers[$comment['comment_id']] as $answer): ?>
				<li class="task-details-comment-row task-details-comment-answer-row">
					<a target="_blank" href="#" class="rf">
						<div class="task-details-profile-photo">
							<img src="<?php echo imgCrop($answer['photo'], 35, 35, 'assets/img/default-avatar.png') ?>" alt="Profile Photo">
						</div>
					</a>
					<div class="task-details-comment-text">
						<div class="task-details-comment-user-date-wrap">
							<a class="task-details-username-link" href="#">
	                            <?php echo $answer['type'] !== '1' ? "{$answer['firstname']} {$answer['lastname']}" : $answer['company_name']; ?>
							</a>
							<span class="task-details-comment-date">
								<?php echo \Carbon\Carbon::parse($answer['created_at'])->diffForHumans(); ?>
							</span>
						</div>
						<div class="task-details-comment-text-wrap">
							<div class="task-details-comment-text">
								<?php echo $answer['comment'] ?>
							</div>
						</div>
	                    <?php if($answer['parent_id'] === $comment['comment_id'] && $answer['user_id'] !== $current_user['id']): ?>
							<div class="task-details-comment-actions">
								<a href="#" class="task-details-comment-action action-reply"
								   data-post-id="<?php echo $comment['post_id'] ?>"
								   data-parent-id="<?php echo $comment['comment_id'] ?>"
								   data-toggle="modal"
								   data-posted-by="<?php echo $answer['type'] !== '1' ? "{$answer['firstname']} {$answer['lastname']}" : $answer['company_name']; ?>"
								   data-target="#modal_public_question">
	                                <?php if(in_array($status, ['published', 'in-progress', 'marked-completed'])): ?>
									<span class="action-text"><?php echo lang('reply_question') ?></span>
									<?php endif ?>
								</a>
							</div>
	                    <?php endif; ?>
					</div>
				</li>
				<?php endforeach; // end of answers loop ?>
				<?php endif; // answers endif ?>
				<?php endforeach; // end of comments loop ?>
			</ul>
			<?php elseif( $post_user_id === $user->info['id'] ): ?>
			<p><?php echo lang('no_questions_posted') ?></p>
			<?php endif; // comments endif ?>
		</div>
	</div>
</div>

<!-- Modal - Seeker's Questionnaire on Task Application -->
<div id="modal_public_question" class="modal modal-public-question fade" aria-labelledby="modal_custom_widget" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Answer a question from <span id="posted-by">Unknown</span></h5>
			</div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<form name="form_task_question" id="form_task_question" method="post" action="<?php echo url_for('/workspace/comments/create') ?>">
                    <?php echo html_form_token_field() ?>
					<div id="apply_question_1" class="frm form-apply-question">
						<div class="task-question"></div>
						<textarea class="form-control form-control-input" name="comment" placeholder="Your Answer" rows="3" id="taskQuestion1"></textarea>
						<input type="hidden" name="post_id" value="<?php echo $post_id; ?>" />
						<input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
						<input type="hidden" name="post_type" value="<?php echo isset($post['task_id']) ? 'task' : 'job' ?>" />
						<input type="hidden" name="parent_id" value="" />
						<div class="form-group button-container">
							<button type="submit" class="btn-icon-full btn-step-next qa-open1">
								<span class="btn-label">Answer</span>
								<span class="btn-icon">
                                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                        </svg>
                                    </span>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->
<?php endif ?>