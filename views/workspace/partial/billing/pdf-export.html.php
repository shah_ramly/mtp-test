<style type="text/css">
    <!--
    table { vertical-align: top; }
    tr    { vertical-align: top; }
    td    { vertical-align: top; }
    -->
</style>
<page backcolor="#FEFEFE" backimgx="center" backimgy="bottom" backimgw="100%" backtop="0" backbottom="30mm" footer="date;time;page" style="font-size: 8pt">
	<table cellspacing="0" style="width: 100%; text-align: center; font-size: 14px">
		<tr>
			<td style="width: 75%;">
			</td>
			<td style="width: 25%; color: #444444;text-align:right;">
				<?php
				$image = str_replace('https', 'http', option('site_uri')) . url_for('/assets/img/mtp_logo_dark.png');
                $imageData = base64_encode(file_get_contents($image));
                $src = 'data:image/png;base64,'.$imageData;
				?>
				<img style="width: 70%;" src="<?php echo $src ?>" alt="Logo"><br><br>
				<?php echo ucwords(str_replace('-', ' ', $title)) ?>
			</td>
		</tr>
	</table>
	<br>
	<br>
	<br>
	<br>
	<table cellspacing="1" style="width: 100%; border: solid 0px black; background: #FFFFFF; font-size: 8pt;">
		<colgroup>
			<col style="padding:2px; width: 10%; text-align: left">
			<col style="padding:2px; width: 20%; text-align: left">
			<col style="padding:2px; width: 40%; text-align: left">
			<col style="padding:2px; width: 10%; text-align: right">
			<col style="padding:2px; width: 10%; text-align: center">
			<col style="padding:2px; width: 10%; text-align: right">
		</colgroup>
		<thead>
		<tr style="background: #E7E7E7;">
			<th style="border-bottom: solid 0px black;">Number</th>
			<th style="border-bottom: solid 0px black;">Title</th>
			<th style="border-bottom: solid 0px black;">Description</th>
			<th style="border-bottom: solid 0px black;">Budget</th>
			<th style="border-bottom: solid 0px black;">Complete By</th>
			<th style="border-bottom: solid 0px black;">Status</th>
		</tr>
		</thead>
		<tbody>
        <?php foreach($data as $row): ?>
			<tr>
				<td style="border-top: solid 1px #cacaca;"><?php echo $row['number'] ?></td>
				<td style="border-top: solid 1px #cacaca;"><?php echo $row['title'] ?></td>
				<td style="border-top: solid 1px #cacaca;"><?php echo $row['description'] ?></td>
				<td style="border-top: solid 1px #cacaca;"><?php echo $row['budget'] ?></td>
				<td style="border-top: solid 1px #cacaca;"><?php echo \Carbon\Carbon::parse($row['complete_by'])->format("Y-m-d") ?></td>
				<td style="border-top: solid 1px #cacaca;"><?php echo str_replace("-", " ", in_array($row['status'], ['marked-completed']) ? 'in-progress' : $row['status']) ?></td>
			</tr>
        <?php endforeach ?>
		</tbody>
	</table>
</page>
