<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-chart" style="box-shadow:none">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-12 col-header-filter">
	                        <h2 class="card-title"><?php echo 'My Spent' ?></h2>
	                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
		                        <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			                        <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
		                        </svg>
	                        </button>
                            <?php echo partial('workspace/partial/header.html.php'); ?>
                        </div>
                    </div>
                </div>
                <div class="card-body-billings-earning">
                    <ul class="nav nav-pills nav-pills-billings" id="pills-tab" role="tablist">
                        <li class="nav-item-arrow nav-item-arrow-prev">
                            <a class="nav-link nav-link-arrow nav-link-prev" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M15 18l-6-6 6-6" /></svg></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if($tab === 'all') echo 'active' ?>" id="pills-all-tab" data-toggle="pill" href="#pills-all" role="tab" aria-controls="pills-all" aria-selected="<?php echo ($tab === 'all') ? 'true' : 'false' ?>">All Status</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if($tab === 'inprogress') echo 'active' ?>" id="pills-in-progress-tab" data-toggle="pill" href="#pills-in-progress" role="tab" aria-controls="pills-in-progress" aria-selected="<?php echo ($tab === 'progress') ? 'true' : 'false' ?>">In Progress</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if($tab === 'completed') echo 'active' ?>" id="pills-completed-tab" data-toggle="pill" href="#pills-completed" role="tab" aria-controls="pills-completed" aria-selected="<?php echo ($tab === 'completed') ? 'true' : 'false' ?>">Completed</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if($tab === 'closed') echo 'active' ?>" id="pills-closed-tab" data-toggle="pill" href="#pills-closed" role="tab" aria-controls="pills-closed" aria-selected="<?php echo ($tab === 'closed') ? 'true' : 'false' ?>">Closed</a>
                        </li>
                        <li class="nav-item-arrow nav-item-arrow-next">
                            <a class="nav-link nav-link-arrow nav-link-next" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M9 18l6-6-6-6" /></svg></a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <?php echo partial('workspace/partial/billing/spent/all-tab.html.php'); ?>
                        <?php echo partial('workspace/partial/billing/spent/in-progress-tab.html.php'); ?>
                        <?php echo partial('workspace/partial/billing/spent/completed-tab.html.php'); ?>
                        <?php echo partial('workspace/partial/billing/spent/closed-tab.html.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php content_for('modals'); ?>
<?php if(!empty($projects['ids'])): ?>
	<?php echo partial('workspace/partial/rating-centre/review-modal.html.php',
	    ['posted' => $projects['posted'], 'performed' => $projects['performed']]); ?>
<?php endif ?>
	<!-- Modal - OD Confirm Accept Task -->
	<div id="modal_confirm_accept_od" class="modal modal-confirm fade" aria-labelledby="modal_confirm_accept_od" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">Accept Confirmation</div>
				<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
						<line x1="18" y1="6" x2="6" y2="18"></line>
						<line x1="6" y1="6" x2="18" y2="18"></line>
					</svg>
				</button>
				<div class="modal-body">
					<p>
						Parties are advised to resolve any issues amicably through discussion. A Dispute Resolution will lead to a permanent indicator on your profile that you have adopted this option for one or more of your projects
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
						<span class="btn-label">Cancel</span>
						<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
					</button>
					<form method="post" action="<?php echo url_for('/workspace/task/accept') ?>">
                        <?php echo html_form_token_field(); ?>
						<input type="hidden" name="task_id" value="" />
						<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
							<span class="btn-label">Proceed</span>
							<span class="btn-icon">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                                <polyline points="20 6 9 17 4 12"></polyline>
	                            </svg>
	                        </span>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->
	<!-- Modal - OD Confirm Reject Task -->
	<div id="modal_confirm_reject_od" class="modal modal-confirm fade" aria-labelledby="modal_confirm_reject_od" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">Reject Confirmation</div>
				<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
						<line x1="18" y1="6" x2="6" y2="18"></line>
						<line x1="6" y1="6" x2="18" y2="18"></line>
					</svg>
				</button>
				<div class="modal-body">
					<p>
						Parties are advised to resolve any issues amicably through discussion. A Dispute Resolution will lead to a permanent indicator on your profile that you have adopted this option for one or more of your projects
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
						<span class="btn-label">Cancel</span>
						<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
					</button>
					<form method="post" action="<?php echo url_for('/workspace/task/reject') ?>">
                        <?php echo html_form_token_field(); ?>
						<input type="hidden" name="task_id" value="" />
						<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
							<span class="btn-label">Proceed</span>
							<span class="btn-icon">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                                <polyline points="20 6 9 17 4 12"></polyline>
	                            </svg>
	                        </span>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->
	<!-- Modal - OD Confirm Dispute Task -->
	<div id="modal_confirm_dispute_od" class="modal modal-confirm fade" aria-labelledby="modal_confirm_dispute_od" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">Dispute Confirmation</div>
				<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
						<line x1="18" y1="6" x2="6" y2="18"></line>
						<line x1="6" y1="6" x2="18" y2="18"></line>
					</svg>
				</button>
				<div class="modal-body">
					<p>
						Parties are advised to resolve any issues amicably through discussion. A Dispute Resolution will lead to a permanent indicator on your profile that you have adopted this option for one or more of your projects
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
						<span class="btn-label">Cancel</span>
						<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
					</button>
					<form method="post" action="<?php echo url_for('/workspace/task/dispute') ?>">
                        <?php echo html_form_token_field(); ?>
						<input type="hidden" name="task_id" value="" />
						<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
							<span class="btn-label">Proceed</span>
							<span class="btn-icon">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                                <polyline points="20 6 9 17 4 12"></polyline>
	                            </svg>
	                        </span>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->
	<!-- Modal - Confirm Rlease Fund -->
	<div id="modal_confirm_releasefund" class="modal modal-confirm fade" aria-labelledby="modal_confirm_releasefund" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">Release Fund Confirmation</div>
				<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
						<line x1="18" y1="6" x2="6" y2="18"></line>
						<line x1="6" y1="6" x2="18" y2="18"></line>
					</svg>
				</button>
				<div class="modal-body">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</div>
				<div class="modal-footer">
					<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
						<span class="btn-label">Cancel</span>
						<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
					</button>
					<form method="post" action="<?php echo url_for('/workspace/task/close') ?>">
                        <?php echo html_form_token_field(); ?>
						<input type="hidden" name="task_id" value="" />
						<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
							<span class="btn-label">Proceed</span>
							<span class="btn-icon">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                                <polyline points="20 6 9 17 4 12"></polyline>
	                            </svg>
	                        </span>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->
<?php end_content_for() ?>

<?php content_for('scripts') ?>
<!-- Filter Datepicker Function -->
<script>
    $('#calendar_all_1').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($from) ? "'{$from}'" : 'false' ?>
    });

    $('#calendar_all_2').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($to) ? "'{$to}'" : 'false' ?>
    });

    $('#calendar_inprogress_1').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($from) ? "'{$from}'" : 'false' ?>
    });

    $('#calendar_inprogress_2').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($to) ? "'{$to}'" : 'false' ?>
    });

    $('#calendar_completed_1').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($from) ? "'{$from}'" : 'false' ?>
    });

    $('#calendar_completed_2').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($to) ? "'{$to}'" : 'false' ?>
    });

    $('#calendar_closed_1').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($from) ? "'{$from}'" : 'false' ?>
    });

    $('#calendar_closed_2').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($to) ? "'{$to}'" : 'false' ?>
    });

    $('.custom-filter-date').on('click', function(e){
        e.preventDefault();
        var $filter = $(e.currentTarget);

        $filter.parent().find('a').removeClass('active');
        $filter.parent().prev().find('span.drop-val').text($filter.text());
        $filter.addClass('active');
        $filter.parent().parent().siblings().show();
        $filter.parent().parent().siblings().find('input').prop('disabled', false);
    });

    $('.date').on('hide.datetimepicker', function(e){
        if( $(e.currentTarget).prop('id').indexOf('_1') > -1 ){
            if( moment(e.date).isValid() ){
                if($(this).parent().next().find('.date input').val().length){
                    var end = $(this).parent().next().find('.date input').val().toString();
                    if(moment(end).isValid()){
                        var tab = $(e.currentTarget).prop('id').split('_')[1];
                        var cUrl = btoa( moment(e.date).format("YYYY/MM/DD") +"-"+ moment(end).format("YYYY/MM/DD")+"-"+tab );
                        window.location.href = "<?php echo url_for('workspace/billings/spent/custom-') ?>"+cUrl;
                    }
                }
            }
        }else{
            if( moment(e.date).isValid() ){
                if($(this).parent().prev().find('.date input').val().length){
                    var start = $(this).parent().prev().find('.date input').val().toString();
                    if( moment(start).isValid() ) {
                        var tab = $(e.currentTarget).prop('id').split('_')[1];
                        var cUrl = btoa( moment(start).format("YYYY/MM/DD") + "-" + moment(e.date).format("YYYY/MM/DD")+"-"+tab );
                        window.location.href = "<?php echo url_for('workspace/billings/spent/custom-') ?>"+cUrl;
                    }
                }
            }
        }
    })

</script>
<!-- Filter Datepicker Function -->

<!-- Progress Bar Function -->
<script>
    // progressbar.js@1.0.0 version is used
    // Docs: https://progressbarjs.readthedocs.org/en/1.0.0/
    var progressBars = $('.progress_');
    progressBars.each(function(idx, progressbar){
        var percentage = $(progressbar).data('percentage');
        var color = (percentage >= 100) ? '#00c3bc' : ( percentage <= 25 ? '#655f93' : '#007cf0' );
        new ProgressBar.Line(progressbar, {
            strokeWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            color: color,
            trailColor: '#eee',
            trailWidth: 1,
            svgStyle: {
                width: '100%',
                height: '100%'
            },
            text: {
                style: {
                    // Text color.
                    // Default: same as stroke color (options.color)
                },
                autoStyleContainer: false
            },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) + ' %');
            }
        }).animate(percentage/100); // Number from 0.0 to 1.0
    });

</script>
<!-- Progress Bar Function -->

<!-- Reset Review Form when opened -->
<script>
    $('#modal_write_review').on('show.bs.modal', function(e){
        var modal = $(this),
            button = $(e.relatedTarget),
            taskId = button.data('taskId');

        modal.find('form')[0].reset();
        $('.form-group-rating-poster').hide();
        $('.form-group-rating-talent').hide();
        $('#modal_write_review .row-starrating label').hide();

        if(taskId){
            var hiddenInput = $('<input type="hidden" name="task" />');
            hiddenInput.val(taskId);
            modal.find('form').prepend(hiddenInput);
            modal.find('select#select_task_posted_performed').val(taskId).prop('disabled', true).trigger('change');
        }
    });
</script>
<!-- Reset Review Form when opened -->
<!-- Write Review Modal - Rating Star Function -->
<script>
    $("#timeliness_rating_group .btn-rating").on('click', (function(e) {

        var previous_value = $("#timeliness_rating").val();

        var selected_value = $(this).attr("data-attr");
        $("#timeliness_rating").val(selected_value);

        $(".timeliness-rating").empty();
        $(".timeliness-rating").html(selected_value);


        for (i = 1; i <= selected_value; ++i) {
            $("#timeliness_rating_group #rating-star-" + i).toggleClass('btn-staractive');
            $("#timeliness_rating_group #rating-star-" + i).toggleClass('btn-stardefault');
        }

        for (ix = 1; ix <= previous_value; ++ix) {
            $("#timeliness_rating_group #rating-star-" + ix).toggleClass('btn-staractive');
            $("#timeliness_rating_group #rating-star-" + ix).toggleClass('btn-stardefault');
        }

    }));
    $("#quality_rating_group .btn-rating").on('click', (function(e) {

        var previous_value = $("#quality_rating").val();

        var selected_value = $(this).attr("data-attr");
        $("#quality_rating").val(selected_value);

        $(".quality-rating").empty();
        $(".quality-rating").html(selected_value);


        for (i = 1; i <= selected_value; ++i) {
            $("#quality_rating_group #rating-star-" + i).toggleClass('btn-staractive');
            $("#quality_rating_group #rating-star-" + i).toggleClass('btn-stardefault');
        }

        for (ix = 1; ix <= previous_value; ++ix) {
            $("#quality_rating_group #rating-star-" + ix).toggleClass('btn-staractive');
            $("#quality_rating_group #rating-star-" + ix).toggleClass('btn-stardefault');
        }

    }));
    $("#communication_rating_group .btn-rating").on('click', (function(e) {

        var previous_value = $("#communication_rating").val();

        var selected_value = $(this).attr("data-attr");
        $("#communication_rating").val(selected_value);

        $(".communication-rating").empty();
        $(".communication-rating").html(selected_value);


        for (i = 1; i <= selected_value; ++i) {
            $("#communication_rating_group #rating-star-" + i).toggleClass('btn-staractive');
            $("#communication_rating_group #rating-star-" + i).toggleClass('btn-stardefault');
        }

        for (ix = 1; ix <= previous_value; ++ix) {
            $("#communication_rating_group #rating-star-" + ix).toggleClass('btn-staractive');
            $("#communication_rating_group #rating-star-" + ix).toggleClass('btn-stardefault');
        }

    }));
    $("#responsiveness_rating_group .btn-rating").on('click', (function(e) {

        var previous_value = $("#responsiveness_rating").val();

        var selected_value = $(this).attr("data-attr");
        $("#responsiveness_rating").val(selected_value);

        $(".responsiveness-rating").empty();
        $(".responsiveness-rating").html(selected_value);


        for (i = 1; i <= selected_value; ++i) {
            $("#responsiveness_rating_group #rating-star-" + i).toggleClass('btn-staractive');
            $("#responsiveness_rating_group #rating-star-" + i).toggleClass('btn-stardefault');
        }

        for (ix = 1; ix <= previous_value; ++ix) {
            $("#responsiveness_rating_group #rating-star-" + ix).toggleClass('btn-staractive');
            $("#responsiveness_rating_group #rating-star-" + ix).toggleClass('btn-stardefault');
        }

    }));
</script>
<!-- Write Review Modal - Rating Star Function -->

<!-- Write Review Modal - Change Number Rating Based On Task Posted/Performed -->
<script>
    $("#select_task_posted_performed").change(function() {
        var selected = $("option:selected", this);
        if (selected.parent()[0].id === "review_task_posted") {
            $('#modal_write_review .row-starrating label').show();
            $('.form-group-rating-talent').show();
            $('.form-group-rating-poster').hide();
            $('.form-group-rating-poster').find('input[type="radio"]').prop('disabled', true);
            $('.form-group-rating-talent').find('input[type="radio"]').prop('disabled', false);
        } else if (selected.parent()[0].id === "review_task_performed") {
            $('#modal_write_review .row-starrating label').show();
            $('.form-group-rating-poster').show();
            $('.form-group-rating-talent').hide();
            $('.form-group-rating-talent').find('input[type="radio"]').prop('disabled', true);
            $('.form-group-rating-poster').find('input[type="radio"]').prop('disabled', false);
        }
    })
</script>

<script>
    $('#modal_confirm_accept_od').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var task_id = button.data('taskId');
        var modal = $(this);
        modal.find('input[name="task_id"]').val(task_id);
        modal.find('.btn-confirm').on('click', function(e){
            modal.find('form')[0].submit();
        });
    });
    $('#modal_confirm_reject_od').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var task_id = button.data('taskId');
        var modal = $(this);
        modal.find('input[name="task_id"]').val(task_id);
        modal.find('.btn-confirm').on('click', function(e){
            modal.find('form')[0].submit();
        });
    });
    $('#modal_confirm_dispute_od').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var task_id = button.data('taskId');
        var modal = $(this);
        modal.find('input[name="task_id"]').val(task_id);
        modal.find('.btn-confirm').on('click', function(e){
            modal.find('form')[0].submit();
        });
    });
    $('#modal_confirm_releasefund').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var task_id = button.data('taskId');
        var modal = $(this);
        modal.find('input[name="task_id"]').val(task_id);
        modal.find('.btn-confirm').on('click', function(e){
            modal.find('form')[0].submit();
        });
    });
</script>

<?php if( isset($_SESSION['review_id']) ): ?>
<script>
	$('#<?php echo $_SESSION['review_id'] ?>').trigger('click');
</script>
<?php unset($_SESSION['review_id']) ?>
<?php endif ?>
<?php end_content_for(); ?>