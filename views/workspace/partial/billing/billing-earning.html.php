<?php if(!isset($tab) || $tab === 'all' ): ?>
<?php echo partial('workspace/partial/billing/earning/all-tab.html.php'); ?>
<?php elseif ($tab === 'inprogress'): ?>
    <?php echo partial('workspace/partial/billing/earning/in-progress-tab.html.php'); ?>
<?php elseif($tab === 'completed'): ?>
    <?php echo partial('workspace/partial/billing/earning/completed-tab.html.php'); ?>
<?php elseif($tab === 'closed'): ?>
    <?php echo partial('workspace/partial/billing/earning/closed-tab.html.php'); ?>
<?php endif ?>

<?php content_for('modals'); ?>
<?php if(!empty($projects['ids'])): ?>
    <?php echo partial('workspace/partial/rating-centre/review-modal.html.php',
        ['posted' => $projects['posted'], 'performed' => $projects['performed']]); ?>
<?php endif ?>

<!-- Modal - Confirm Completed Request -->
<div id="modal_confirm_complete_request" class="modal modal-confirm fade" aria-labelledby="modal_confirm_complete_request" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php echo lang('task_completion_request_confirmation_title') ?></div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<?php echo lang('task_completion_request_confirmation_body') ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
					<span class="btn-label"><?php echo lang('cancel') ?></span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
				</button>
				<form method="post" action="<?php echo url_for('/workspace/task/complete') ?>">
                    <?php echo html_form_token_field(); ?>
					<input type="hidden" name="task_id" value="" />
					<input type="hidden" name="user_id" value="" />
					<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
						<span class="btn-label"><?php echo lang('proceed') ?></span>
						<span class="btn-icon">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                                <polyline points="20 6 9 17 4 12"></polyline>
	                            </svg>
	                        </span>
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->

<!-- Modal - Confirm Payment Request -->
<div id="modal_confirm_payment_request" class="modal modal-confirm fade" aria-labelledby="modal_confirm_payment_request" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php echo lang('task_payment_request_confirmation_title') ?></div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<?php echo lang('task_payment_request_confirmation_body') ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
					<span class="btn-label"><?php echo lang('cancel') ?></span>
					<span class="btn-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </span>
				</button>
				<form method="post" action="<?php echo url_for('/workspace/task/request_payment') ?>">
                    <?php echo html_form_token_field(); ?>
					<input type="hidden" name="task_id" value="" />
					<input type="hidden" name="user_id" value="" />
					<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
						<span class="btn-label"><?php echo lang('proceed') ?></span>
						<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->
<?php end_content_for() ?>

<?php content_for('scripts') ?>
<!-- Filter Datepicker Function -->
<script>
    $('#calendar_all_1').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($from) ? "'{$from}'" : 'false' ?>
    });

    $('#calendar_all_2').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($to) ? "'{$to}'" : 'false' ?>
    });

    $('#calendar_inprogress_1').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($from) ? "'{$from}'" : 'false' ?>
    });

    $('#calendar_inprogress_2').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($to) ? "'{$to}'" : 'false' ?>
    });

    $('#calendar_completed_1').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($from) ? "'{$from}'" : 'false' ?>
    });

    $('#calendar_completed_2').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($to) ? "'{$to}'" : 'false' ?>
    });

    $('#calendar_closed_1').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($from) ? "'{$from}'" : 'false' ?>
    });

    $('#calendar_closed_2').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: <?php echo isset($to) ? "'{$to}'" : 'false' ?>
    });

    $('.custom-filter-date').on('click', function(e){
        e.preventDefault();
        var $filter = $(e.currentTarget);

        $filter.parent().find('a').removeClass('active');
        $filter.parent().prev().find('span.drop-val').text($filter.text());
        $filter.addClass('active');
        $filter.parent().parent().siblings().show();
        $filter.parent().parent().siblings().find('input').prop('disabled', false);
    });

    $('.date').on('hide.datetimepicker', function(e){
        if( $(e.currentTarget).prop('id').indexOf('_1') > -1 ){
            if( moment(e.date).isValid() ){
                if($(this).parent().next().find('.date input').val().length){
                    var end = $(this).parent().next().find('.date input').val().toString();
                    if(moment(end).isValid()){
                        var tab = $(e.currentTarget).prop('id').split('_')[1];
                        var cUrl = btoa( moment(e.date).format("YYYY/MM/DD") +"-"+ moment(end).format("YYYY/MM/DD")+"-"+tab );
                        window.location.href = "<?php echo url_for('workspace/billings/earning/custom-') ?>"+cUrl;
                    }
                }
            }
        }else{
            if( moment(e.date).isValid() ){
                if($(this).parent().prev().find('.date input').val().length){
                    var start = $(this).parent().prev().find('.date input').val().toString();
                    if( moment(start).isValid() ) {
                        var tab = $(e.currentTarget).prop('id').split('_')[1];
                        var cUrl = btoa( moment(start).format("YYYY/MM/DD") + "-" + moment(e.date).format("YYYY/MM/DD")+"-"+tab );
                        window.location.href = "<?php echo url_for('workspace/billings/earning/custom-') ?>"+cUrl;
                    }
                }
            }
        }
    })

</script>
<!-- Filter Datepicker Function -->

<!-- Progress Bar Function -->
<script>
    // progressbar.js@1.0.0 version is used
    // Docs: https://progressbarjs.readthedocs.org/en/1.0.0/
    var progressBars = $('.progress_');
    progressBars.each(function(idx, progressbar){
        var percentage = $(progressbar).data('percentage');
        var color = (percentage >= 100) ? '#00c3bc' : ( percentage <= 25 ? '#655f93' : '#007cf0' );
        new ProgressBar.Line(progressbar, {
            strokeWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            color: color,
            trailColor: '#eee',
            trailWidth: 1,
            svgStyle: {
                width: '100%',
                height: '100%'
            },
            text: {
                style: {
                    // Text color.
                    // Default: same as stroke color (options.color)
                },
                autoStyleContainer: false
            },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) + ' %');
            }
        }).animate(percentage/100); // Number from 0.0 to 1.0
    });

</script>
<!-- Progress Bar Function -->

<!-- Reset Review Form when opened -->
<script>
    $('#modal_write_review').on('show.bs.modal', function(e){
        var modal = $(this),
            button = $(e.relatedTarget),
            taskId = button.data('taskId');

        modal.find('form')[0].reset();
        $('.form-group-rating-poster').hide();
        $('.form-group-rating-talent').hide();
        $('#modal_write_review .row-starrating label').hide();

        if(taskId){
            var hiddenInput = $('<input type="hidden" name="task" />');
            hiddenInput.val(taskId);
            modal.find('form').prepend(hiddenInput);
            modal.find('select#select_task_posted_performed').val(taskId).prop('disabled', true).trigger('change');
        }
    });
</script>
<!-- Reset Review Form when opened -->
<!-- Write Review Modal - Rating Star Function -->
<script>
    $("#timeliness_rating_group .btn-rating").on('click', (function(e) {

        var previous_value = $("#timeliness_rating").val();

        var selected_value = $(this).attr("data-attr");
        $("#timeliness_rating").val(selected_value);

        $(".timeliness-rating").empty();
        $(".timeliness-rating").html(selected_value);


        for (i = 1; i <= selected_value; ++i) {
            $("#timeliness_rating_group #rating-star-" + i).toggleClass('btn-staractive');
            $("#timeliness_rating_group #rating-star-" + i).toggleClass('btn-stardefault');
        }

        for (ix = 1; ix <= previous_value; ++ix) {
            $("#timeliness_rating_group #rating-star-" + ix).toggleClass('btn-staractive');
            $("#timeliness_rating_group #rating-star-" + ix).toggleClass('btn-stardefault');
        }

    }));
    $("#quality_rating_group .btn-rating").on('click', (function(e) {

        var previous_value = $("#quality_rating").val();

        var selected_value = $(this).attr("data-attr");
        $("#quality_rating").val(selected_value);

        $(".quality-rating").empty();
        $(".quality-rating").html(selected_value);


        for (i = 1; i <= selected_value; ++i) {
            $("#quality_rating_group #rating-star-" + i).toggleClass('btn-staractive');
            $("#quality_rating_group #rating-star-" + i).toggleClass('btn-stardefault');
        }

        for (ix = 1; ix <= previous_value; ++ix) {
            $("#quality_rating_group #rating-star-" + ix).toggleClass('btn-staractive');
            $("#quality_rating_group #rating-star-" + ix).toggleClass('btn-stardefault');
        }

    }));
    $("#communication_rating_group .btn-rating").on('click', (function(e) {

        var previous_value = $("#communication_rating").val();

        var selected_value = $(this).attr("data-attr");
        $("#communication_rating").val(selected_value);

        $(".communication-rating").empty();
        $(".communication-rating").html(selected_value);


        for (i = 1; i <= selected_value; ++i) {
            $("#communication_rating_group #rating-star-" + i).toggleClass('btn-staractive');
            $("#communication_rating_group #rating-star-" + i).toggleClass('btn-stardefault');
        }

        for (ix = 1; ix <= previous_value; ++ix) {
            $("#communication_rating_group #rating-star-" + ix).toggleClass('btn-staractive');
            $("#communication_rating_group #rating-star-" + ix).toggleClass('btn-stardefault');
        }

    }));
    $("#responsiveness_rating_group .btn-rating").on('click', (function(e) {

        var previous_value = $("#responsiveness_rating").val();

        var selected_value = $(this).attr("data-attr");
        $("#responsiveness_rating").val(selected_value);

        $(".responsiveness-rating").empty();
        $(".responsiveness-rating").html(selected_value);


        for (i = 1; i <= selected_value; ++i) {
            $("#responsiveness_rating_group #rating-star-" + i).toggleClass('btn-staractive');
            $("#responsiveness_rating_group #rating-star-" + i).toggleClass('btn-stardefault');
        }

        for (ix = 1; ix <= previous_value; ++ix) {
            $("#responsiveness_rating_group #rating-star-" + ix).toggleClass('btn-staractive');
            $("#responsiveness_rating_group #rating-star-" + ix).toggleClass('btn-stardefault');
        }

    }));
</script>
<!-- Write Review Modal - Rating Star Function -->

<!-- Write Review Modal - Change Number Rating Based On Task Posted/Performed -->
<script>
    $("#select_task_posted_performed").change(function() {
        var selected = $("option:selected", this);
        if (selected.parent()[0].id === "review_task_posted") {
            $('#modal_write_review .row-starrating label').show();
            $('.form-group-rating-talent').show();
            $('.form-group-rating-poster').hide();
            $('.form-group-rating-poster').find('input[type="radio"]').prop('disabled', true);
            $('.form-group-rating-talent').find('input[type="radio"]').prop('disabled', false);
        } else if (selected.parent()[0].id === "review_task_performed") {
            $('#modal_write_review .row-starrating label').show();
            $('.form-group-rating-poster').show();
            $('.form-group-rating-talent').hide();
            $('.form-group-rating-talent').find('input[type="radio"]').prop('disabled', true);
            $('.form-group-rating-poster').find('input[type="radio"]').prop('disabled', false);
        }
    })
</script>
<script>
    $('#modal_confirm_complete_request').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var task_id = button.data('taskId');
        var user_id = button.data('userId');
        var modal = $(this);
        modal.find('input[name="task_id"]').val(task_id);
        modal.find('input[name="user_id"]').val(user_id);
        modal.find('.btn-confirm').on('click', function(e){
            modal.find('form')[0].submit();
        });
    });

    $('#modal_confirm_payment_request').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var task_id = button.data('taskId');
        var user_id = button.data('userId');
        var modal = $(this);
        modal.find('input[name="task_id"]').val(task_id);
        modal.find('input[name="user_id"]').val(user_id);
        modal.find('.btn-confirm').on('click', function(e){
            modal.find('form')[0].submit();
        });
    });
</script>

<?php if( isset($_SESSION['review_id']) ): ?>
	<script>
        $('#<?php echo $_SESSION['review_id'] ?>').trigger('click');
	</script>
    <?php unset($_SESSION['review_id']) ?>
<?php endif ?>
<?php end_content_for(); ?>
