<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-chart">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-12 col-header-filter">
                            <h2 class="card-title-new"><?php echo lang('billing_details_main_title') ?></h2>
	                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
		                        <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			                        <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
		                        </svg>
	                        </button>
                            <?php echo partial('workspace/partial/header.html.php'); ?>
                        </div>
                    </div>
                </div>
                <div class="card-body-payment-details">
					<ul class="nav nav-pills nav-pills-billingdetails">
						<li class="nav-item">
							<a class="nav-link active"><?php echo lang('task_billing_details') ?></a>
						</li>
					</ul>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="row">
                                <div class="col-xl-12 col-payment-wrapper">
	                                <div class="breadcrumbs-back">
		                                <a href="javascript: window.history.back()" class="breadcrumbs-link">&lt; <?php echo lang('task_details_back') ?></a>
	                                </div>
                                    <div class="col-billing-details-wrapper">
                                        <div class="col-payment-flex-container">
                                            <div class="col-xl-7 col-task-seeker-wrapper">
                                                <div class="row-payment-task-details">
                                                    <div class="sub-col-title"><?php echo lang('task_view_task_details') ?></div>
                                                    <h3 class="task-details-title"><?php echo $task['title'] ?></h3>
                                                    <div class="task-payment-details-row task-details-title-components">
                                                        <span class="task-details-type"><?php echo lang('task_on_demand') ?></span>
                                                        <span class="task-details-location">
                                                            <span class="task-details-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                                                </svg></span>
                                                            <span class="task-details-location-val">
	                                                            <?php /*echo !is_null($task['state_country']) ? $task['state_country'] : ($task['location'] === 'remotely') ? 'Working ' . ucwords($task['location']) : ucwords($task['location']);*/?>
                                                                <?php echo $task['location'] === 'remotely' ? lang('task_details_working_remotely') : ucwords($task['state_country']); ?>
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div class="task-payment-details-row task-payment-flex task-payment-details-nature-row">
                                                        <div class="task-payment-details-task-nature">
                                                            <label class="task-payment-details-lbl"><?php echo lang('task_nature_of_task') ?></label>
                                                            <div class="task-payment-details-val">
	                                                            <?php echo $task['task_type'] === 'by-hour' ? lang('task_by_the_hour') :
                                                                    lang('task_lump_sum'); ?>
                                                            </div>
                                                        </div>
                                                        <?php if( $task['task_type'] === 'by-hour' ): ?>
                                                        <div class="task-payment-details-estimated-hours">
                                                            <label class="task-payment-details-lbl"><?php echo lang('post_step_4_estimated') ?></label>
                                                            <div class="task-payment-details-val">
                                                                <?php echo ($task['task_type'] === 'by-hour') ?
                                                                    $task['hours'] . " " . ($task['hours'] == 1 ? lang('task_hour'):lang('task_hours')) . " " . lang('task_details_task') :
                                                                     lang('task_lump_sum') ?>
                                                            </div>
                                                        </div>
	                                                    <?php endif ?>
                                                    </div>
                                                    <div class="task-payment-details-row task-payment-details-task-category">
                                                        <label class="task-payment-details-lbl"><?php echo lang('task_details_category') ?></label>
                                                        <div class="task-payment-details-val">
	                                                        <?php echo $task['category_name'] ?>
                                                        </div>
                                                    </div>
                                                    <div class="task-payment-details-row task-payment-details-task-subcategory">
                                                        <label class="task-payment-details-lbl"><?php echo lang('task_details_subcategory') ?></label>
                                                        <div class="task-payment-details-val">
                                                            <?php echo $task['subcategory_name'] ?>
                                                        </div>
                                                    </div>
	                                                <div class="task-payment-details-row task-payment-details-start-date">
		                                                <label class="task-payment-details-lbl">
			                                                <?php echo lang('task_details_start_date') ?>
				                                            <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo lang('task_details_start_date_tooltip') ?>">
                                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                                                                </svg>
                                                            </span>
		                                                </label>
		                                                <div class="task-payment-details-val">
                                                            <?php echo \Carbon\Carbon::parse($task['start_by'])->format($cms->settings()['date_format']); ?>
		                                                </div>
	                                                </div>
                                                    <div class="task-payment-details-row task-payment-details-completion-date">
                                                        <label class="task-payment-details-lbl">
	                                                        <?php echo lang('task_details_complete_by') ?>
				                                            <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo lang('task_details_complete_by_tooltip') ?>">
                                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                                                                </svg>
                                                            </span>
                                                        </label>
                                                        <div class="task-payment-details-val">
                                                            <?php echo \Carbon\Carbon::parse($task['complete_by'])->format($cms->settings()['date_format']); ?>
                                                        </div>
                                                    </div>
                                                    <div class="task-payment-details-row task-payment-taskid">
                                                        <label class="task-payment-details-lbl"><?php echo lang('task_id') ?></label>
                                                        <div class="task-payment-details-val">
	                                                        <?php echo $task['number'] ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row-payment-seeker-details">
                                                    <div class="sub-col-title"><?php echo lang('task_details_poster_details') ?></div>
                                                    <div class="talent-personal-info-container">
                                                        <div class="talent-avatar-container">
                                                            <div class="profile-avatar">
                                                                <a href="<?php echo url_for("/talent/{$task['user']['number']}")?>" class="talent-avatar-link">
	                                                                <img class="avatar-img"
	                                                                     src="<?php echo imgCrop($task['user']['photo'], 100, 100, 'assets/img/default-avatar.png') ?>" alt="">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="talent-personal-info">
                                                            <div class="col-talent-personal-info">
	                                                            <?php $type = $task['user']['type'] === '1' ? 'company' : 'talent' ?>
                                                                <a href="<?php echo url_for("/{$type}/{$task['user']['number']}")?>" class="talent-name-link">
                                                                    <h5 class="profile-name">
                                                                        <?php echo $task['user']['type'] === '1' ? $task['user']['company']['name'] : "{$task['user']['firstname']} {$task['user']['lastname']}"; ?>
                                                                    </h5>
                                                                </a>
                                                                <div class="talent-details-profile-location">
                                                                    <span class="talent-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                            <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                                                        </svg></span>
                                                                    <span class="talent-details-profile-location-val">
	                                                                    <?php echo "{$task['user']['state']}, {$task['user']['country']}"; ?>
                                                                    </span>
                                                                </div>
                                                                <div class="task-row-rating-flex">
                                                                    <div class="task-row-rating-star">
                                                                        <?php $starred = (int)floor($task['user']['rating']['overall']/2); $rest = 5 - $starred; ?>
                                                                        <?php if($starred > 0): ?>
                                                                            <?php foreach (range(1, $starred) as $star): ?>
		                                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                        <?php if($rest > 0): ?>
                                                                            <?php foreach (range(1, $rest) as $star1): ?>
		                                                                        <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                    <div class="task-row-rating-val">
                                                                        <span class="task-row-average-rating-val">
	                                                                        <?php echo number_format((float)$task['user']['rating']['overall'], 1); ?>
                                                                        </span>
                                                                        <span class="task-row-divider">/</span>
                                                                        <span class="task-row-total-rating-val">
	                                                                        <?php echo "{$task['user']['rating']['count']} " . ( $task['user']['rating']['count'] == 1 ? lang('task_details_review') : lang('task_details_reviews')); ?>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-5 col-payment-details-wrapper">
                                                <div class="row-payment-details-billing-container">
                                                    <div class="sub-col-title"><?php echo lang('task_payment_details') ?></div>
                                                    <div class="payment-details-billing">
                                                        <div class="payment-details-subtotal">
                                                            <div class="payment-details-flex-row">
                                                                <span class="payment-details-lbl"><?php echo lang('post_step_4_task_value') ?></span>
                                                                <span class="payment-details-val">RM
                                                                <?php echo number_format($task['budget'], 2) ?>
                                                                </span>
                                                            </div>
                                                            <div class="payment-details-flex-row">
                                                                <span class="payment-details-lbl"><?php echo lang('billing_details_payment_code') ?></span>
                                                                <span class="payment-details-val">
	                                                                <?php echo $payment_code = substr(uniqid('', true), -5, 5); ?>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="payment-rejection-rate">
                                                        <label class="input-lbl">
                                                            <span class="input-label-txt"><?php echo lang('post_step_4_rejection_rate') ?>
	                                                            <span class="bi-tooltip"
	                                                                  data-toggle="tooltip" data-placement="top"
	                                                                  title="<?php echo lang('post_step_4_rejection_rate_tooltip') ?>">
                                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                        <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                                                                    </svg>
                                                                </span></span>
                                                        </label>
                                                        <div class="rejection-rate-container">
                                                            <span class="rejection-rate-poster">
                                                                <span class="rejection-rate-lbl"><?php echo lang('task_poster') ?></span>
                                                                <span class="rejection-rate-val">
	                                                                <?php echo "{$task['rejection_rate']}%" ?>
                                                                </span>
                                                                <span class="rejection-rate-amount">
	                                                                RM<?php echo number_format((float)$task['budget'] * ($task['rejection_rate']/100), 2) ?>
                                                                </span>
                                                            </span>
                                                            <span class="rejection-rate-seeker">
                                                                <span class="rejection-rate-lbl"><?php echo lang('task_seeker') ?></span>
                                                                <span class="rejection-rate-val">
	                                                                <?php echo (100 - $task['rejection_rate']) . "%" ?>
                                                                </span>
                                                                <span class="rejection-rate-amount">
	                                                                RM<?php echo number_format((float)$task['budget'] * ((100-$task['rejection_rate'])/100), 2) ?>
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="payment-row-actions">
	                                                    <?php if( $task['in-progress'] ): ?>
	                                                    <button type="button"
	                                                            data-toggle="modal"
	                                                            data-target="#modal_confirm_complete_request"
	                                                            data-task-id="<?php echo $task['task_id'] ?>"
	                                                            data-user-id="<?php echo $task['user_id'] ?>"
	                                                            class="btn-icon-full btn-markcomplete">
		                                                    <span class="btn-label"><?php echo lang('task_mark_as_complete') ?></span>
		                                                    <span class="btn-icon">
                                                            <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                            </svg>
                                                        </span>
	                                                    </button>
	                                                    <?php elseif ( $task['completed'] && !$task['has_payment'] ): ?>
                                                        <button type="button"
                                                                data-toggle="modal"
                                                                data-target="#modal_confirm_payment_request"
                                                                data-task-id="<?php echo $task['task_id'] ?>"
                                                                data-user-id="<?php echo $task['user_id'] ?>"
                                                                class="btn-icon-full btn-reqpayment">
                                                            <span class="btn-label"><?php echo lang('request_for_payment') ?></span>
                                                            <span class="btn-icon">
                                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                                </svg>
                                                            </span>
                                                        </button>
	                                                    <?php endif ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php echo partial('workspace/partial/billing/activity-log.html.php', ['activities' => $task['activities']]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php content_for('modals'); ?>
<?php if( $task['in-progress'] ): ?>
	<!-- Modal - Confirm Completed Request -->
	<div id="modal_confirm_complete_request" class="modal modal-confirm fade" aria-labelledby="modal_confirm_complete_request" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header"><?php echo lang('task_completion_request_confirmation_title') ?></div>
				<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
						<line x1="18" y1="6" x2="6" y2="18"></line>
						<line x1="6" y1="6" x2="18" y2="18"></line>
					</svg>
				</button>
				<div class="modal-body">
					<?php echo lang('task_completion_request_confirmation_body') ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
						<span class="btn-label"><?php echo lang('cancel') ?></span>
						<span class="btn-icon">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                                <line x1="18" y1="6" x2="6" y2="18"></line>
	                                <line x1="6" y1="6" x2="18" y2="18"></line>
	                            </svg>
	                        </span>
					</button>
					<form method="post" action="<?php echo url_for('/workspace/task/complete') ?>">
	                    <?php echo html_form_token_field(); ?>
						<input type="hidden" name="task_id" value="" />
						<input type="hidden" name="user_id" value="" />
						<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
							<span class="btn-label"><?php echo lang('proceed') ?></span>
							<span class="btn-icon">
		                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
		                                <polyline points="20 6 9 17 4 12"></polyline>
		                            </svg>
		                        </span>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->
<?php elseif( $task['completed'] && !$task['has_payment'] ): ?>
	<!-- Modal - Confirm Payment Request -->
	<div id="modal_confirm_payment_request" class="modal modal-confirm fade" aria-labelledby="modal_confirm_payment_request" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header"><?php echo lang('task_payment_request_confirmation_title') ?></div>
				<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
						<line x1="18" y1="6" x2="6" y2="18"></line>
						<line x1="6" y1="6" x2="18" y2="18"></line>
					</svg>
				</button>
				<div class="modal-body">
					<?php echo lang('task_payment_request_confirmation_body') ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
						<span class="btn-label"><?php echo lang('cancel') ?></span>
						<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
					</button>
					<form method="post" action="<?php echo url_for('/workspace/task/request_payment') ?>">
                        <?php echo html_form_token_field(); ?>
						<input type="hidden" name="task_id" value="" />
						<input type="hidden" name="user_id" value="" />
						<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
							<span class="btn-label"><?php echo lang('proceed') ?></span>
							<span class="btn-icon">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                                <polyline points="20 6 9 17 4 12"></polyline>
	                            </svg>
	                        </span>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal -->
<?php endif ?>
<?php end_content_for(); ?>

<?php content_for('scripts'); ?>
<?php if( $task['in-progress'] ): ?>
	<script>
        $('#modal_confirm_complete_request').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var task_id = button.data('taskId');
            var user_id = button.data('userId');
            var modal = $(this);
            modal.find('input[name="task_id"]').val(task_id);
            modal.find('input[name="user_id"]').val(user_id);
            modal.find('.btn-confirm').on('click', function(e){
                modal.find('form')[0].submit();
            });
        });
	</script>
<?php elseif( $task['completed'] && !$task['has_payment'] ): ?>
<script>
    $('#modal_confirm_payment_request').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var task_id = button.data('taskId');
        var user_id = button.data('userId');
        var modal = $(this);
        modal.find('input[name="task_id"]').val(task_id);
        modal.find('input[name="user_id"]').val(user_id);
        modal.find('.btn-confirm').on('click', function(e){
            modal.find('form')[0].submit();
        });
    });
</script>
<?php endif ?>
<?php end_content_for(); ?>
