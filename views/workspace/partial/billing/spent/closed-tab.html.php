<!--<div class="tab-pane fade <?php /*if($tab === 'closed') echo 'show active' */?>" id="pills-closed" role="tabpanel" aria-labelledby="pills-closed-tab">
    <div class="col-xl-12">-->
        <div class="tab-container">
            <div class="tab-filter-container">
                <div id="table-filter-container" class="table-filter-container">
                    <div class="table-filter-left-col">
	                    <div class="dropdown dropdown-filter-container dropdown-filter-status-spent">
		                    <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownStatusSpent" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			                    <label class="filter-btn-lbl"><?php echo lang('task_view'); ?></label> <span class="drop-val">
				                    <?php if( stripos(request_uri(), 'inprogress') || $tab === 'inprogress' ): ?>
					                   <?php echo lang('task_in_progress'); ?>
                                    <?php elseif( stripos(request_uri(), 'completed') || $tab === 'completed' ): ?>
					                    <?php echo lang('task_completed'); ?>
                                    <?php elseif( stripos(request_uri(), 'closed') || $tab === 'closed' ): ?>
					                    <?php echo lang('task_closed'); ?>
                                    <?php else: ?>
					                    <?php echo lang('task_all'); ?>
                                    <?php endif ?>
			                    </span>
		                    </button>
		                    <div class="dropdown-menu" aria-labelledby="dropdownStatusSpent">
			                    <a class="dropdown-item" href="<?php echo url_for('workspace/billings/spent') ?>">
				                    <?php echo lang('task_all') ?>
			                    </a>
			                    <a class="dropdown-item <?php echo stripos(request_uri(), 'inprogress') || $tab === 'inprogress' ? 'active' : '' ?>" href="<?php echo url_for('workspace/billings/spent/all-inprogress') ?>">
				                    <?php echo lang('task_in_progress') ?>
			                    </a>
			                    <a class="dropdown-item <?php echo stripos(request_uri(), 'completed') || $tab === 'completed' ? 'active' : '' ?>" href="<?php echo url_for('workspace/billings/spent/all-completed')?>">
				                    <?php echo lang('task_completed') ?>
			                    </a>
			                    <a class="dropdown-item <?php echo stripos(request_uri(), 'closed') || $tab === 'closed' ? 'active' : '' ?>" href="<?php echo url_for('workspace/billings/spent/all-closed')?>">
				                    <?php echo lang('task_closed') ?>
			                    </a>
		                    </div>
	                    </div>
                        <div class="dropdown dropdown-filter-container">
	                        <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                        <label class="filter-btn-lbl"><?php echo lang('task_filter_date_title') ?></label> <span class="drop-val">
		                            <?php if( stripos(request_uri(), 'month') ): ?>
			                            <?php echo lang('task_filter_date_this_month') ?>
                                    <?php elseif( stripos(request_uri(), 'year') ): ?>
			                            <?php echo lang('task_filter_date_this_year') ?>
                                    <?php elseif( stripos(request_uri(), 'quarter') ): ?>
			                            <?php echo lang('task_filter_date_this_quarter') ?>
                                    <?php elseif( stripos(request_uri(), 'custom') ): ?>
			                            <?php echo lang('task_filter_date_custom') ?>
                                    <?php else: ?>
			                            <?php echo lang('task_filter_date_all') ?>
                                    <?php endif ?>
	                            </span>
	                        </button>
	                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		                        <a class="dropdown-item" href="<?php echo url_for('workspace/billings/spent') ?>">
			                        <?php echo lang('task_all') ?>
		                        </a>
		                        <a class="dropdown-item <?php if(stripos(request_uri(), 'month')) echo 'active'; ?>" href="<?php echo url_for('workspace/billings/spent/month-closed') ?>">
			                        <?php echo lang('task_filter_date_this_month') ?>
		                        </a>
		                        <a class="dropdown-item <?php if(stripos(request_uri(), 'quarter')) echo 'active'; ?>" href="<?php echo url_for('workspace/billings/spent/quarter-closed')?>">
			                        <?php echo lang('task_filter_date_this_quarter') ?>
		                        </a>
		                        <a class="dropdown-item <?php if(stripos(request_uri(), 'year')) echo 'active'; ?>" href="<?php echo url_for('workspace/billings/spent/year-closed')?>">
			                        <?php echo lang('task_filter_date_this_year') ?>
		                        </a>
		                        <a class="dropdown-item <?php if(stripos(request_uri(), 'custom')) echo 'active'; ?> custom-filter-date" href="">
			                        <?php echo lang('task_filter_date_custom') ?>
		                        </a>
	                        </div>
                        </div>
                        <div class="form-group form-group-filter-calendar filter-calendar-from" <?php echo !stripos(request_uri(), 'custom') ? 'style="display:none;"' : '' ?>>
                            <div class="input-group input-group-datetimepicker date" id="calendar_closed_1" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-target="#calendar_closed_1" placeholder="<?php echo lang('select_date') ?>" <?php echo !isset($from) ? "disabled" : '' ?> />
                                <span class="input-group-addon" data-target="#calendar_closed_1" data-toggle="datetimepicker">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group form-group-filter-calendar filter-calendar-to" <?php echo !stripos(request_uri(), 'custom') ? 'style="display:none;"' : '' ?>>
                            <div class="input-group input-group-datetimepicker date" id="calendar_closed_2" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-target="#calendar_closed_2" placeholder="<?php echo lang('select_date') ?>" <?php echo !isset($to) ? "disabled" : '' ?> />
                                <span class="input-group-addon" data-target="#calendar_closed_2" data-toggle="datetimepicker">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="table-filter-right-col">
                        <div class="dropdown dropdown-filter-container">
                            <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <label class="filter-btn-lbl"><?php echo lang('export_this_list') ?></label> <span class="drop-val"><?php echo lang('select_format') ?></span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" target="_blank" href="<?php echo url_for(request_uri()) . '?export=pdf' ?>"><?php echo lang('export_pdf') ?></a>
                                <a class="dropdown-item" target="_blank" href="<?php echo url_for(request_uri()) . '?export=excel' ?>"><?php echo lang('export_excel') ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-table-container">
                <?php if( isset($tasks) && !empty($tasks['inactive']) ): ?>
		            <table class="table table-billings inactive">
			            <tbody>
                        <?php foreach( $tasks['inactive'] as $index => $task ): ?>
                            <?php $task['has_payment'] = $payment_request->has($task['id']) ?>
                            <?php echo partial('/workspace/partial/billing/spent/inactive-row.html.php', ['task' => $task, 'index' => $index+1]) ?>
                        <?php endforeach ?>
			            </tbody>
		            </table>
                <?php else: ?>
	                <table class="table table-billings inactive empty-state">
		                <tbody>
		                <tr>
			                <td class="info empty-col">
				                <div class="empty-state-container">
					                <div class="empty-img"><img src="<?php echo url_for("assets/img/mtp-no-data-2.png") ?>" alt=""></div>
					                <span class="empty-lbl"><?php echo lang('no_transaction_yet') ?></span>
					                <span class="empty-desc"><?php echo lang('post_a_task_and_receive_applications') ?></span>
				                </div>
			                </td>
		                </tr>
		                </tbody>
	                </table>
                <?php endif ?>
            </div>
        </div>
<!--    </div>
</div>-->