<tr>
    <td class="active check-col">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="select-item custom-control-input checkbox" name="select-item[]" id="inactive_spent_<?php echo $index ?? 1 ?>" />
            <label class="custom-control-label" for="inactive_spent_<?php echo $index ?? 1 ?>"></label>
        </div>
    </td>
    <td class="info namedesc-col">
	    <div class="tbl-main-title tbl-task-name">
            <?php echo (strlen($task['title']) > 60) ? (substr($task['title'], 0, 60) . ' ...') : $task['title'] ?>
	    </div>
	    <div class="tbl-desc tbl-task-desc" title="<?php echo strlen(strip_tags(str_replace('<', ' <', $task['description']))) ?>">
            <?php echo (strlen(strip_tags(str_replace('<', ' <', $task['description']))) > 85) ? substr(strip_tags(str_replace('<', ' <', $task['description'])), 0, 85) . ' ...' : strip_tags(str_replace('<', ' <', $task['description'])); ?>
	    </div>
    </td>
    <td class="info idate-col">
        <div class="tbl-sub-title tbl-task-id"><?php echo $task['number'] ?></div>
        <div class="tbl-desc tbl-task-date" style="white-space:nowrap">
            <?php echo \Carbon\Carbon::parse($task['created_at'])->format("d M Y") ?>
        </div>
    </td>
    <td class="info amt-col">
        <div class="tbl-sub-title tbl-task-amt"> RM<?php echo number_format($task['budget'], 2) ?></div>
        <div class="tbl-desc tbl-task-status"><?php echo lang('task_' . str_replace('-', '_', $task['status'])) ?></div>
    </td>
    <td class="info status-col">

    </td>
    <td class="info req-col">

    </td>
    <td class="info more-opt-col dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <span class="tbl-more-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round">
                    <circle cx="12" cy="12" r="1"></circle>
                    <circle cx="19" cy="12" r="1"></circle>
                    <circle cx="5" cy="12" r="1"></circle>
                </svg>
            </span>
        </a>
        <ul class="dropdown-menu dropdown-navbar">
            <?php if( !empty($projects['ids']) && in_array($task['id'], $projects['ids']) ): ?>
		        <li class="header-dropdown-link-list">
			        <a href="#" class="nav-item dropdown-item"
			           id="<?php echo $task['id'] ?>"
			           data-task-id="<?php echo $task['id'] ?>"
			           data-toggle="modal"
			           data-target="#modal_write_review"><?php echo lang('review_this_task') ?></a>
		        </li>
            <?php endif ?>
            <li class="header-dropdown-link-list">
	            <a href="<?php echo url_for("workspace/task/{$task['slug']}/billing") ?>" class="nav-item dropdown-item"><?php echo lang('task_billing_details') ?></a>
            </li>
            <li class="header-dropdown-link-list">
	            <a href="#"
	               data-toggle="modal"
	               data-user-id="<?php echo $task['assigned_to'] ?>"
	               data-target="#modal_chat"
	               data-task-id="<?php echo $task['id'] ?>"
	               data-task-slug="<?php echo $task['slug'] ?>"
	               data-task-closed="<?php echo in_array($task['status'], ['closed', 'disputed']) ? 'true' : 'false' ?>"
	               data-task-closed-date="<?php echo in_array($task['status'], ['closed', 'disputed']) ? \Carbon\Carbon::parse($task['updated_at'])->format("d F Y - h.ia") : 'false' ?>"
	               data-chat-enabled="true"
	               class="nav-item dropdown-item chatButton"><?php echo lang('task_chat_history') ?></a>
            </li>
        </ul>
    </td>
</tr>