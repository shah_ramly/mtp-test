<tr>
    <td class="active check-col">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="select-item custom-control-input checkbox" name="select-item[]" id="spent_<?php echo $index ?? 1 ?>" />
            <label class="custom-control-label" for="spent_<?php echo $index ?? 1 ?>"></label>
        </div>
    </td>
    <td class="info namedesc-col">
        <div class="tbl-main-title tbl-task-name">
	        <?php echo (strlen($task['title']) > 60) ? (substr($task['title'], 0, 60) . ' ...') : $task['title'] ?>
        </div>
        <div class="tbl-desc tbl-task-desc" title="<?php echo strip_tags(str_replace('<', ' <', $task['description'])) ?>">
            <?php echo (strlen(strip_tags(str_replace('<', ' <', $task['description']))) > 85) ? substr(strip_tags(str_replace('<', ' <', $task['description'])), 0, 85) . ' ...' : strip_tags(str_replace('<', ' <', $task['description'])); ?>
        </div>
    </td>
    <td class="info idate-col">
        <div class="tbl-sub-title tbl-task-id"><?php echo $task['number'] ?></div>
        <div class="tbl-desc tbl-task-date" style="white-space:nowrap">
	        <?php echo \Carbon\Carbon::parse($task['created_at'])->format("d M Y") ?>
        </div>
    </td>
    <td class="info amt-col">
        <div class="tbl-sub-title tbl-task-amt"> RM<?php echo number_format($task['budget'], 2) ?></div>
        <div class="tbl-desc tbl-task-status"><?php echo lang('task_' . str_replace('-', '_', (in_array($task['status'], ['marked-completed']) ? 'in-progress' : $task['status']))) ?></div>
    </td>
    <td class="info status-col">
	    <?php if( $task['status'] === 'completed' && $task['has_payment'] ): ?>
		    <button type="button" class="btn-icon-full btn-releasefund"
		            data-toggle="modal"
		            data-task-id="<?php echo $task['id'] ?>"
		            data-target="#modal_confirm_releasefund">
			    <span class="btn-label"><?php echo lang('task_release_fund') ?></span>
			    <span class="btn-icon">
                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                </svg>
            </span>
		    </button>
	    <?php else: ?>
            <?php
            $days  = \Carbon\Carbon::parse($task['complete_by'])->diffInDays(\Carbon\Carbon::parse($task['start_by']));
            $today = \Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::parse($task['start_by']));
            ?>
		    <div data-percentage="<?php echo ($today > $days) ? 100 : ($today/$days) * 100 ?>" class="progressbar-chart progress_"></div>
	    <?php endif ?>
    </td>
    <td class="info req-col"></td>
    <td class="info more-opt-col dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <span class="tbl-more-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round">
                    <circle cx="12" cy="12" r="1"></circle>
                    <circle cx="19" cy="12" r="1"></circle>
                    <circle cx="5" cy="12" r="1"></circle>
                </svg>
            </span>
        </a>
        <ul class="dropdown-menu dropdown-navbar">
            <li class="header-dropdown-link-list">
	            <a href="<?php echo url_for("workspace/task/{$task['slug']}/billing") ?>" class="nav-item dropdown-item"><?php echo lang('task_billing_details') ?></a>
            </li>
            <li class="header-dropdown-link-list">
	            <a href="#"
	               data-toggle="modal"
	               data-user-id="<?php echo $task['assigned_to'] ?>"
	               data-target="#modal_chat"
	               data-task-id="<?php echo $task['id'] ?>"
	               data-task-slug="<?php echo $task['slug'] ?>"
	               data-task-closed="<?php echo in_array($task['status'], ['closed', 'disputed']) ? 'true' : 'false' ?>"
	               data-task-closed-date="<?php echo in_array($task['status'], ['closed', 'disputed']) ? \Carbon\Carbon::parse($task['updated_at'])->format("d F Y - h.ia") : 'false' ?>"
	               data-chat-enabled="true"
	               class="nav-item dropdown-item chatButton"><?php echo lang('task_chat') ?></a>
            </li>
	        <?php if( $task['status'] === 'marked-completed' ): ?>
	        <li class="dropdown-divider"></li>
	        <li class="header-dropdown-link-list">
		        <a href="#"
		           data-toggle="modal"
		           data-target="#modal_confirm_accept_od"
		           data-task-id="<?php echo $task['id'] ?>"
		           class="nav-item dropdown-item"><?php echo lang('task_accept') ?></a>
	        </li>
	        <li class="header-dropdown-link-list">
		        <a href="#"
		           data-toggle="modal"
		           data-target="#modal_confirm_reject_od"
		           data-task-id="<?php echo $task['id'] ?>"
		           class="nav-item dropdown-item"><?php echo lang('task_reject') ?></a>
	        </li>
	        <li class="header-dropdown-link-list">
		        <a href="#"
		           data-toggle="modal"
		           data-target="#modal_confirm_dispute_od"
		           data-task-id="<?php echo $task['id'] ?>"
		           class="nav-item dropdown-item"><?php echo lang('task_dispute') ?></a>
	        </li>
	        <?php endif ?>
        </ul>
    </td>
</tr>