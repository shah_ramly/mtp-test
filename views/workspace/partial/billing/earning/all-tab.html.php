<!--<div class="tab-pane fade <?php /*if($tab === 'all') echo 'show active' */?>" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
    <div class="col-xl-12">-->
        <div class="tab-container">
            <div class="tab-filter-container">
                <div id="table-filter-container" class="table-filter-container">
                    <div class="table-filter-left-col">
	                    <div class="dropdown dropdown-filter-container dropdown-filter-status-earning">
		                    <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownStatusEarning" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			                    <label class="filter-btn-lbl"><?php echo lang('task_view') ?></label>
			                    <span class="drop-val">
				                    <?php if( stripos(request_uri(), 'inprogress') || $tab === 'inprogress' ):
					                    echo lang('task_in_progress');
                                    elseif( stripos(request_uri(), 'completed') || $tab === 'completed' ):
					                    echo lang('task_completed');
                                    elseif( stripos(request_uri(), 'closed') || $tab === 'closed' ):
					                    echo lang('task_closed');
                                    else:
					                    echo lang('task_all');
                                    endif ?>
			                    </span>
		                    </button>
		                    <div class="dropdown-menu" aria-labelledby="dropdownStatusEarning">
			                    <a class="dropdown-item" href="<?php echo url_for('workspace/billings/earning') ?>">
				                    <?php echo lang('task_all') ?>
			                    </a>
			                    <a class="dropdown-item <?php echo stripos(request_uri(), 'inprogress') || $tab === 'inprogress' ? 'active' : '' ?>" href="<?php echo url_for('workspace/billings/earning/all-inprogress') ?>">
				                    <?php echo lang('task_in_progress') ?>
			                    </a>
			                    <a class="dropdown-item <?php echo stripos(request_uri(), 'completed') || $tab === 'completed' ? 'active' : '' ?>" href="<?php echo url_for('workspace/billings/earning/all-completed')?>">
				                    <?php echo lang('task_completed') ?>
			                    </a>
			                    <a class="dropdown-item <?php echo stripos(request_uri(), 'closed') || $tab === 'closed' ? 'active' : '' ?>" href="<?php echo url_for('workspace/billings/earning/all-closed')?>">
				                    <?php echo lang('task_closed') ?>
			                    </a>
		                    </div>
	                    </div>
                        <div class="dropdown dropdown-filter-container">
	                        <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                        <label class="filter-btn-lbl"><?php echo lang('task_filter_date_title') ?></label> <span class="drop-val">
		                            <?php if( stripos(request_uri(), 'month') ):
			                            echo lang('task_filter_date_this_month');
                                    elseif( stripos(request_uri(), 'year') ):
			                            echo lang('task_filter_date_this_year');
                                    elseif( stripos(request_uri(), 'quarter') ):
			                            echo lang('task_filter_date_this_quarter');
                                    elseif( stripos(request_uri(), 'custom') ):
		                                echo lang('task_filter_date_custom');
                                    else:
			                            echo lang('task_filter_date_all');
                                    endif ?>
	                            </span>
	                        </button>
	                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		                        <a class="dropdown-item" href="<?php echo url_for('workspace/billings/earning') ?>"><?php echo lang('task_all') ?></a>
		                        <a class="dropdown-item <?php if(stripos(request_uri(), 'month')) echo 'active'; ?>" href="<?php echo url_for('workspace/billings/earning/month-all') ?>">
			                        <?php echo lang('task_filter_date_this_month') ?>
		                        </a>
		                        <a class="dropdown-item <?php if(stripos(request_uri(), 'quarter')) echo 'active'; ?>" href="<?php echo url_for('workspace/billings/earning/quarter-all')?>">
			                        <?php echo lang('task_filter_date_this_quarter') ?>
		                        </a>
		                        <a class="dropdown-item <?php if(stripos(request_uri(), 'year')) echo 'active'; ?>" href="<?php echo url_for('workspace/billings/earning/year-all')?>">
			                        <?php echo lang('task_filter_date_this_year') ?>
		                        </a>
		                        <a class="dropdown-item <?php if(stripos(request_uri(), 'custom')) echo 'active'; ?> custom-filter-date" href="">
			                        <?php echo lang('task_filter_date_custom') ?>
		                        </a>
	                        </div>
                        </div>
                        <div class="form-group form-group-filter-calendar filter-calendar-from" <?php echo !stripos(request_uri(), 'custom') ? 'style="display:none;"' : '' ?>>
                            <div class="input-group input-group-datetimepicker date" id="calendar_all_1" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-target="#calendar_all_1" placeholder="<?php echo lang('select_date') ?>" <?php echo !isset($from) ? "disabled" : '' ?> />
                                <span class="input-group-addon" data-target="#calendar_all_1" data-toggle="datetimepicker">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group form-group-filter-calendar filter-calendar-to" <?php echo !stripos(request_uri(), 'custom') ? 'style="display:none;"' : '' ?>>
                            <div class="input-group input-group-datetimepicker date" id="calendar_all_2" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-target="#calendar_all_2" placeholder="<?php echo lang('select_date') ?>" <?php echo !isset($to) ? "disabled" : '' ?> />
                                <span class="input-group-addon" data-target="#calendar_all_2" data-toggle="datetimepicker">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="table-filter-right-col">
                        <div class="dropdown dropdown-filter-container">
                            <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <label class="filter-btn-lbl"><?php echo lang('export_this_list') ?></label> <span class="drop-val"><?php echo lang('select_format') ?></span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" target="_blank" href="<?php echo url_for(request_uri()) . '?export=pdf' ?>"><?php echo lang('export_pdf') ?></a>
                                <a class="dropdown-item" target="_blank" href="<?php echo url_for(request_uri()) . '?export=excel' ?>"><?php echo lang('export_excel') ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-table-container">
                <?php if( isset($tasks) && !empty($tasks['active']) ): ?>
		            <table class="table table-billings active">
			            <tbody>
                        <?php foreach( $tasks['active'] as $index => $task ): ?>
                            <?php $task['has_payment'] = $payment_request->has($task['id']) ?>
                            <?php echo partial('/workspace/partial/billing/earning/active-row.html.php', ['task' => $task, 'index' => $index+1]) ?>
                        <?php endforeach ?>
			            </tbody>
		            </table>
                <?php endif ?>
                <?php if( isset($tasks) && !empty($tasks['inactive']) ): ?>
		            <table class="table table-billings inactive">
			            <tbody>
                        <?php foreach( $tasks['inactive'] as $Index => $task ): ?>
                            <?php $task['has_payment'] = $payment_request->has($task['id']) ?>
                            <?php echo partial('/workspace/partial/billing/earning/inactive-row.html.php', ['task' => $task, 'index' => $index+1]) ?>
                        <?php endforeach ?>
			            </tbody>
		            </table>
                <?php endif ?>
	            <?php if( empty($tasks['active']) && empty($tasks['inactive']) ): ?>
		            <table class="table table-billings inactive empty-state">
			            <tbody>
			            <tr>
				            <td class="info empty-col">
					            <div class="empty-state-container">
						            <div class="empty-img"><img src="<?php echo url_for("assets/img/mtp-no-data-2.png") ?>" alt=""></div>
						            <span class="empty-lbl"><?php echo lang('no_earning_transaction_yet') ?></span>
						            <span class="empty-desc"><?php echo lang('apply_task_now_to_start_earning') ?></span>
					            </div>
				            </td>
			            </tr>
			            </tbody>
		            </table>
	            <?php endif ?>
            </div>
        </div>
<!--    </div>
</div>-->

<?php content_for('modals'); ?>
<!-- Modal - Confirm Completed Request -->
<div id="modal_confirm_complete_request" class="modal modal-confirm fade" aria-labelledby="modal_confirm_complete_request" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Completion Request Confirmation</div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
					<span class="btn-label">Cancel</span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
				</button>
				<form method="post" action="<?php echo url_for('/workspace/task/complete') ?>">
                    <?php echo html_form_token_field(); ?>
					<input type="hidden" name="task_id" value="" />
					<input type="hidden" name="user_id" value="" />
					<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
						<span class="btn-label">Proceed</span>
						<span class="btn-icon">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                                <polyline points="20 6 9 17 4 12"></polyline>
	                            </svg>
	                        </span>
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->
<!-- Modal - Confirm Payment Request -->
<div id="modal_confirm_payment_request" class="modal modal-confirm fade" aria-labelledby="modal_confirm_payment_request" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Payment Request Confirmation</div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
					<span class="btn-label">Cancel</span>
					<span class="btn-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </span>
				</button>
				<form method="post" action="<?php echo url_for('/workspace/task/request_payment') ?>">
                    <?php echo html_form_token_field(); ?>
					<input type="hidden" name="task_id" value="" />
					<input type="hidden" name="user_id" value="" />
					<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
						<span class="btn-label">Proceed</span>
						<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->
<?php end_content_for(); ?>