<div class="card-body-taskcentre card-body-taskcentre-progress">
    <?php if($view === 'list'): ?>
	<ul class="nav nav-pills nav-pills-taskcentre" id="tcTabList" role="tablist">
		<li class="nav-item-arrow nav-item-arrow-prev">
			<a class="nav-link nav-link-arrow nav-link-prev" href="#">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
				     stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
					<path d="M15 18l-6-6 6-6" />
				</svg>
			</a>
		</li>
		<?php if( $user->info['type'] !== '1' ): ?>
		<li class="nav-item">
			<a class="nav-link <?php echo $section === 'applied' ? 'active' : '' ?>" id="tcAll-tab"
			   data-toggle="pill" href="#tcAll" role="tab" aria-controls="tcAll"
			   aria-selected="<?php echo $section === 'applied' ? 'true' : 'false' ?>" data-section="applied">Applied Tasks</a>
		</li>
		<?php endif ?>
		<li class="nav-item">
			<a class="nav-link <?php echo $section === 'posted' ? 'active' : '' ?>" id="tcInProgressMonth-tab"
			   data-toggle="pill" href="#tcInProgressMonth" role="tab" aria-controls="tcInProgressMonth"
			   aria-selected="<?php echo $section === 'posted' ? 'true' : 'false' ?>" data-section="posted">Posted Tasks</a>
		</li>
		<li class="nav-item-arrow nav-item-arrow-next">
			<a class="nav-link nav-link-arrow nav-link-next" href="#">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
				     stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
					<path d="M9 18l6-6-6-6" /></svg>
			</a>
		</li>
	</ul>
	<?php echo partial('workspace/partial/task-centre/list-view.html.php'); ?>
	<?php else: ?>
		<ul class="nav nav-pills nav-pills-taskcentre" id="tcTabList" role="tablist">
			<li class="nav-item-arrow nav-item-arrow-prev">
				<a class="nav-link nav-link-arrow nav-link-prev" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
						<path d="M15 18l-6-6 6-6" /></svg></a>
			</li>
            <?php if( $user->info['type'] !== '1' ): ?>
			<li class="nav-item">
				<a class="nav-link <?php echo $section === 'applied' ? 'active' : '' ?>" id="tcInProgressApplied-tab"
				   data-toggle="pill" href="#tcInProgressApplied" role="tab" aria-controls="tcInProgressApplied"
				   aria-selected="<?php echo $section === 'applied' ? 'true' : 'false' ?>" data-section="applied">Applied Tasks</a>
			</li>
			<?php endif ?>
			<li class="nav-item">
				<a class="nav-link <?php echo $section === 'posted' ? 'active' : '' ?>" id="tcInProgressPosted-tab"
				   data-toggle="pill" href="#tcInProgressPosted" role="tab" aria-controls="tcInProgressPosted"
				   aria-selected="<?php echo $section === 'posted' ? 'true' : 'false' ?>" data-section="posted">Posted Tasks</a>
			</li>
			<li class="nav-item-arrow nav-item-arrow-next">
				<a class="nav-link nav-link-arrow nav-link-next" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
						<path d="M9 18l6-6-6-6" /></svg></a>
			</li>
		</ul>
        <?php echo partial('workspace/partial/task-centre/calendar-view.html.php'); ?>
	<?php endif ?>
	<!--<ul class="nav nav-pills nav-pills-taskcentre" id="tcTabList" role="tablist">
		<li class="nav-item-arrow nav-item-arrow-prev">
			<a class="nav-link nav-link-arrow nav-link-prev" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"><path d="M15 18l-6-6 6-6"/></svg></a>
		</li>
		<li class="nav-item">
			<a class="nav-link <?php /*echo !isset($current_tab) || $current_tab === '#tcAll' ? 'active' : '' */?>" id="tcAll-tab" data-toggle="pill" href="#tcAll" role="tab" aria-controls="tcAll" aria-selected="true"><?php /*echo lang('task_all'); */?></a>
		</li>

		<li class="nav-item">
            <?php /*if(isset($month) && $month === 'active'): */?>
	            <a class="nav-link <?php /*echo isset($current_tab) && $current_tab === '#tcInProgressMonth' ? 'active' : '' */?>" id="tcInProgressMonth-tab" data-toggle="pill" href="#tcInProgressMonth" role="tab" aria-controls="tcInProgressMonth" aria-selected="false"><?php /*echo lang('task_in_progress'); */?></a>
            <?php /*elseif(isset($week) && $week === 'active'): */?>
	            <a class="nav-link <?php /*echo isset($current_tab) && $current_tab === '#tcInProgressWeek' ? 'active' : '' */?>" id="tcInProgressWeek-tab" data-toggle="pill" href="#tcInProgressWeek" role="tab" aria-controls="tcInProgressWeek" aria-selected="false"><?php /*echo lang('task_in_progress'); */?></a>
            <?php /*else: */?>
	            <a class="nav-link <?php /*echo isset($current_tab) && $current_tab === '#tcInProgressDay' ? 'active' : '' */?>" id="tcInProgressDay-tab" data-toggle="pill" href="#tcInProgressDay" role="tab" aria-controls="tcInProgressDay" aria-selected="false"><?php /*echo lang('task_in_progress'); */?></a>
            <?php /*endif; */?>
		</li>

		<li class="nav-item-arrow nav-item-arrow-next">
			<a class="nav-link nav-link-arrow nav-link-next" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"><path d="M9 18l6-6-6-6"/></svg></a>
		</li>
	</ul>
    <div class="tab-content" id="pills-tabContent">
	    <?php /*echo partial('workspace/partial/task-centre/all.html.php') */?>
        <?php /*if(isset($month) && $month === 'active'): */?>
            <?php /*echo partial('workspace/partial/task-centre/month.html.php') */?>
        <?php /*elseif(isset($week) && $week === 'active'): */?>
            <?php /*echo partial('workspace/partial/task-centre/week.html.php') */?>
        <?php /*else: */?>
            <?php /*echo partial('workspace/partial/task-centre/day.html.php') */?>
        <?php /*endif; */?>
    </div>-->
</div>

<?php content_for('extra_modals'); ?>
<div id="modal_tc_retract" class="modal modal-milestone-opt modal-tc-retract fade show" aria-labelledby="modal_tc_retract" tabindex="-1" role="dialog" aria-modal="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php echo lang('task_centre_retract_application') ?></div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<p class="modal-para"><?php echo lang('you_are_retracting_your_application') ?></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
					<span class="btn-label"><?php echo lang('modal_cancel') ?></span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
				</button>
				<form method="post" action="<?php echo url_for('workspace/task/retract_application') ?>">
                    <?php echo html_form_token_field() ?>
					<input type="hidden" name="task_id" value="" />
					<input type="hidden" name="user_id" value="" />
					<button type="submit" class="btn-icon-full btn-confirm" data-orientation="next">
						<span class="btn-label"><?php echo lang('modal_confirm') ?></span>
						<span class="btn-icon">
	                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                            <polyline points="20 6 9 17 4 12"></polyline>
	                        </svg>
	                    </span>
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Modal - Milestone Seeker -->
<div id="modal_milestone_seeker" class="modal modal-milestone fade" aria-labelledby="modal_milestone_seeker" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php echo lang('modal_progress') ?></div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="milestone-progress-content">
				<div class="modal-body" style="padding-top:7.2rem;padding-bottom:7.2rem;text-align:center">
					<div class="spinner-border spinner-border" style="width:5rem;height:5rem" role="status">
						<span class="sr-only"><?php echo lang('modal_loading') ?></span>
					</div>
				</div>
				<div class="modal-footer">

				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal - Milestone Seeker -->

<!-- Modal - Milestone Poster -->
<div id="modal_milestone_poster" class="modal modal-milestone fade" aria-labelledby="modal_milestone_poster" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php echo lang('modal_progress') ?></div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="milestone-progress-content">
				<div class="modal-body" style="padding-top:7.2rem;padding-bottom:7.2rem;text-align:center">
					<div class="spinner-border spinner-border" style="width:5rem;height:5rem" role="status">
						<span class="sr-only"><?php echo lang('modal_loading') ?></span>
					</div>
				</div>
				<div class="modal-footer">

				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal - Milestone Poster -->
<?php end_content_for(); ?>

<?php content_for('extra_scripts'); ?>
<script>
    $('.applied-tasks-list').on("click", ".btn-progress", function() {
        var button = $(this);
        var progressSeekerID = button.data('id');
        var task = button.data('taskId');
        var poster = button.data('userId');
        var task_status = button.data('taskState');
        var originalContent = $($('#modal_milestone_seeker .milestone-progress-content').html()).clone();

        $("#modal_milestone_seeker .modal-dialog").addClass(progressSeekerID);

        $.ajax({
            method: 'POST',
            url: '<?php echo url_for("workspace/milestone-progress") ?>',
            data: {'task': task, 'poster': poster, 'task_status': task_status, 'progress_status': progressSeekerID },
        }).done(function(response){
            $('#modal_milestone_seeker .milestone-progress-content').html(response);
            if( $(response).find('span.hide-status').length ){
                button.removeClass('active');
                if( button.siblings('.active').length === 0 && button.parent().find('.active').length === 0 ) {
                    button.closest('.applied-task-row').removeClass('active-row');
                }
            }
        });

        $('#modal_milestone_poster').on('hidden.bs.modal', function() {
            $(".modal-dialog").removeClass(progressSeekerID);
            $('#modal_milestone_seeker .milestone-progress-content').html(originalContent);
        });

        $('#modal_milestone_seeker').on('hidden.bs.modal', function() {
            $("#modal_milestone_seeker .modal-dialog").removeClass(progressSeekerID);
            $('#modal_milestone_seeker .milestone-progress-content').html(originalContent);
        });
    });

    $('.posted-tasks-list').on("click", ".applicant-progress-link", function(e) {
        var $this = $(this);
        var progressPosterID = $this.data('id');
        var task = $this.data('taskId');
        var seeker = $this.data('seekerId');
        var task_status = $this.data('taskState');
        var originalContent = $($('#modal_milestone_poster .milestone-progress-content').html()).clone();

        $("#modal_milestone_poster .modal-dialog").addClass(progressPosterID);
        $this.closest('tr').children('td').first().removeClass('new-applicant');

        $.ajax({
            method: 'POST',
            url: '<?php echo url_for("workspace/milestone-progress") ?>',
            data: {'task': task, 'seeker': seeker, 'task_status': task_status },
        }).done(function(response){
            $('#modal_milestone_poster .milestone-progress-content').html(response);
        });

        $('#modal_milestone_poster').on('hidden.bs.modal', function() {
            $("#modal_milestone_poster .modal-dialog").removeClass(progressPosterID);
            $('#modal_milestone_poster .milestone-progress-content').html(originalContent);
        });
    });

    $('#modal_milestone_poster').on('show.bs.modal', function(e){
        var modal = $(this);
        var target = $(e.relatedTarget);
        var id = target.data('id');
        var content = modal.find('.modal-dialog');
        window.poster_modal = content.clone();
        content.addClass(id);
    });

    $('#modal_milestone_poster').on('hidden.bs.modal', function(e){
        var modal = $(this);
        var content = window.poster_modal;
        window.poster_modal = undefined;
        modal.find('.modal-dialog').html(content.html());
    });

    $('#modal_milestone_seeker').on('show.bs.modal', function(e){
        var modal = $(this);
        var target = $(e.relatedTarget);
        var id = target.data('id');
        var content = modal.find('.modal-dialog');
        window.seeker_modal = content.clone();
        content.addClass(id);
    });

    $('#modal_milestone_seeker').on('hidden.bs.modal', function(e){
        var modal = $(this);
        var content = window.seeker_modal;
        window.seeker_modal = undefined;
        modal.find('.modal-dialog').html(content.html());
    });

    $('#modal_tc_retract').on('show.bs.modal', function(e){
        var modal = $(this);
        var target = $(e.relatedTarget);
        var task = target.data('taskId');
        var user = target.data('userId');

        modal.find('input[name="task_id"]').val(task);
        modal.find('input[name="user_id"]').val(user);
    });

    $('.modal-dialog').on('change', '[name="milestone_close_poster"]', function() {
        milestonePercentage(this);
    });

    $('.modal-dialog').on('change', '#disputePoster_rejectionRateNumerator', function() {
        var percentage = $(this).val();
        var content = $(this).closest('.milestone-radio-dispute-percentage');
        var price = content.find('[name="task_price"]').val();
        $('.modal-dialog').find('[name="percentage"]').val(percentage);
        if( percentage > 0 && price ){
            var pay = (percentage/100) * price;
            content.find('.to-be-paid').text('RM ' + pay.toFixed(2));
            content.find('.balance').text('RM ' + (price - pay).toFixed(2));
        }else{
            content.find('.to-be-paid').text('RM 0.00');
            content.find('.balance').text('RM ' + parseInt(price, 10).toFixed(2));
        }
    });

    function milestonePercentage(ele) {
        var text = $(ele).closest('.milestone-content').find('#milestoneDisputeOpt');
        var content = $(ele).closest('.milestone-progress-content');

        if ($(ele).prop("checked") == true && $(ele).val() == 'no') {
            content.find('[name="dispute"]').val('yes');
            text.show();
        } else {
            content.find('[name="dispute"]').val('no');
            text.hide();
            $(ele).closest('.milestone-content').find('#milestoneDisputePercent').hide();
            $(ele).closest('.milestone-content').find('#selectRejectOption').children().first().prop('selected', true);
        }
    }

    $('.modal-dialog').on('change', '#selectRejectOption', (function() {
        var val = $(this).val();
        if (val === "milestone-reject-1") {
            $('#milestoneDisputePercent').show();
        } else if (val === "milestone-reject-2") {
            $('#milestoneDisputePercent').show();
        }
        $('.modal-dialog').find('[name="reason"]').val(val);
    }));

    $('.tasks-contents').on('click', '.applicant-pin-link', function(e){
        e.preventDefault();
        var pinned = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pin-angle-fill" viewBox="0 0 16 16">'+
            '<path d="M9.828.722a.5.5 0 0 1 .354.146l4.95 4.95a.5.5 0 0 1 0 .707c-.48.48-1.072.588-1.503.588-.177 0-.335-.018-.46-.039l-3.134 3.134a5.927 5.927 0 0 1 .16 1.013c.046.702-.032 1.687-.72 2.375a.5.5 0 0 1-.707 0l-2.829-2.828-3.182 3.182c-.195.195-1.219.902-1.414.707-.195-.195.512-1.22.707-1.414l3.182-3.182-2.828-2.829a.5.5 0 0 1 0-.707c.688-.688 1.673-.767 2.375-.72a5.92 5.92 0 0 1 1.013.16l3.134-3.133a2.772 2.772 0 0 1-.04-.461c0-.43.108-1.022.589-1.503a.5.5 0 0 1 .353-.146z" />'+
            '</svg>';
        var unpinned = '<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-pin-angle" viewBox="0 0 16 16">'+
            '<path d="M9.828.722a.5.5 0 0 1 .354.146l4.95 4.95a.5.5 0 0 1 0 .707c-.48.48-1.072.588-1.503.588-.177 0-.335-.018-.46-.039l-3.134 3.134a5.927 5.927 0 0 1 .16 1.013c.046.702-.032 1.687-.72 2.375a.5.5 0 0 1-.707 0l-2.829-2.828-3.182 3.182c-.195.195-1.219.902-1.414.707-.195-.195.512-1.22.707-1.414l3.182-3.182-2.828-2.829a.5.5 0 0 1 0-.707c.688-.688 1.673-.767 2.375-.72a5.92 5.92 0 0 1 1.013.16l3.134-3.133a2.772 2.772 0 0 1-.04-.461c0-.43.108-1.022.589-1.503a.5.5 0 0 1 .353-.146zm.122 2.112v-.002zm0-.002v.002a.5.5 0 0 1-.122.51L6.293 6.878a.5.5 0 0 1-.511.12H5.78l-.014-.004a4.507 4.507 0 0 0-.288-.076 4.922 4.922 0 0 0-.765-.116c-.422-.028-.836.008-1.175.15l5.51 5.509c.141-.34.177-.753.149-1.175a4.924 4.924 0 0 0-.192-1.054l-.004-.013v-.001a.5.5 0 0 1 .12-.512l3.536-3.535a.5.5 0 0 1 .532-.115l.096.022c.087.017.208.034.344.034.114 0 .23-.011.343-.04L9.927 2.028c-.029.113-.04.23-.04.343a1.779 1.779 0 0 0 .062.46z"></path>'+
            '</svg>'
        var target = $(e.currentTarget);
        var hired = target.data('status');
        var seeker = target.data('seekerId');
        var task = target.data('taskId');

        target.addClass('disabled-link');
        $('.applicants-list').css('cursor', 'progress');
        target.closest('tr').children('td').first().removeClass('new-applicant');

        $.ajax({
            url: '<?php echo option("site_uri") . url_for("workspace/task/applicant/pin") ?>',
            method: 'POST',
            data: { 'hired': hired, 'seeker': seeker, 'task': task }
        }).done(function(response){
            if( response.status === 'success' ){
                if( target.find('.bi-pin-angle').length ){
                    target.find('span.applicant-pin-icon').html(pinned);
                }else{
                    target.find('span.applicant-pin-icon').html(unpinned);
                }
            }
            $('.applicants-list').removeAttr('style');
            target.removeClass('disabled-link');
        });

    });

    $('#tcTabList').on('click', '.nav-item a', function(e){
       var tab = $(e.currentTarget).data('section');
       var url = '<?php echo url_for("workspace/task-centre/{$section}/{$view}") ?>' + document.location.search;
       var stateUrl = url.replace('<?php echo $section ?>', tab);
       var calendarHtml = $('.card-body-taskcentre.card-body-taskcentre-progress').html();

        history.pushState({'calendarHtml': calendarHtml}, '', stateUrl);
    });

    $('.dropleft a.hdmy-link').on('click', function(e){
        e.preventDefault();
        var segments = document.location.pathname.replace('/', '').split('/');
        var section = segments[segments.length - 2] || 'applied';
        var href = $(e.currentTarget).attr('href');
        href = href.replace('<?php echo $section ?>', section);

        document.location.href = href;
    });

    $(".btn-tc-mode input").on("change", function (e) {
        var segments = document.location.pathname.replace('/', '').split('/');
        var section = segments[segments.length - 2] || 'applied';
	    var url = '';
        if ($(this).prop("checked") == true) {
            $('body').addClass('tc-list-mode');
            $('body').removeClass('tc-calendar-mode');
            url = '<?php echo option("site_uri") . url_for("workspace/task-centre/{$section}/calendar") ?>'.replace('<?php echo $section ?>', section);
            setTimeout(function() {
                window.location.assign(url);
            },200);
        } else if ($(this).prop("checked") == false) {
            $('body').addClass('tc-calendar-mode');
            $('body').removeClass('tc-list-mode');
            url = '<?php echo option("site_uri") . url_for("workspace/task-centre/{$section}/list") ?>'.replace('<?php echo $section ?>', section);
            setTimeout(function() {
                window.location.assign(url);
            },200);
        }
    });

    $('.active-posted-tasks-view a, .inactive-posted-tasks-view a, .active-applied-tasks-view a, .inactive-applied-tasks-view a').on('click', function(e){
        e.preventDefault();
        const target = $(e.currentTarget);
        const parent = target.parent().parent();

        target.parent().find('a').removeClass('active');
        target.parent().prev().find('span.drop-val').text(target.text());
        target.addClass('active');

        target.parent().prev().dropdown('toggle');

        if( parent.hasClass('active-posted-tasks-view') ) {
            if (target.hasClass('expand-all')) {
                $('.active-tc-row .posted-tasks-list').find('.multi-collapse.collapse').collapse('show');
            } else {
                $('.active-tc-row .posted-tasks-list').find('.multi-collapse.collapse').collapse('hide');
            }
        }else if( parent.hasClass('inactive-posted-tasks-view') ){
            if (target.hasClass('expand-all')) {
                $('.inactive-tc-row .posted-tasks-list').find('.multi-collapse.collapse').collapse('show');
            } else {
                $('.inactive-tc-row .posted-tasks-list').find('.multi-collapse.collapse').collapse('hide');
            }
        }else if( parent.hasClass('active-applied-tasks-view') ){
            if (target.hasClass('expand-all')) {
                $('.active-tc-row .applied-tasks-list').find('.multi-collapse.collapse').collapse('show');
            } else {
                $('.active-tc-row .applied-tasks-list').find('.multi-collapse.collapse').collapse('hide');
            }
        }else if( parent.hasClass('inactive-applied-tasks-view') ){
            if (target.hasClass('expand-all')) {
                $('.inactive-tc-row .applied-tasks-list').find('.multi-collapse.collapse').collapse('show');
            } else {
                $('.inactive-tc-row .applied-tasks-list').find('.multi-collapse.collapse').collapse('hide');
            }
        }
    });

    <?php if( !is_null(session('payment_status')) && !is_null(session('payment_task_id')) && !is_null(session('payment_seeker_id')) ): ?>
    $("a.applicant-progress-link[data-task-id='<?php echo strtoupper(session('payment_task_id')) ?>'][data-seeker-id='<?php echo session('payment_seeker_id') ?>']").trigger('click');
    <?php
    session('payment_status', null, true);
    session('payment_task_id', null, true);
    session('payment_seeker_id', null, true);
    ?>
    <?php endif ?>

</script>

<!-- Task Centre - Synchronize Scroll -->
<script>
    $(document).ready(function() {
        $("#tcInProgressApplied #colLeftCard .tbl-body-calendar").scroll(function() {
            $("#tcInProgressApplied #colRightCard .tbl-body-calendar-gantt").scrollTop($("#tcInProgressApplied #colLeftCard .tbl-body-calendar").scrollTop());
        });
        $("#tcInProgressApplied #colRightCard .tbl-body-calendar-gantt").scroll(function() {
            $("#tcInProgressApplied #colLeftCard .tbl-body-calendar").scrollTop($("#tcInProgressApplied #colRightCard .tbl-body-calendar-gantt").scrollTop());
        });
        $("#tcInProgressPosted #colLeftCard .tbl-body-calendar").scroll(function() {
            $("#tcInProgressPosted #colRightCard .tbl-body-calendar-gantt").scrollTop($("#tcInProgressPosted #colLeftCard .tbl-body-calendar").scrollTop());
        });
        $("#tcInProgressPosted #colRightCard .tbl-body-calendar-gantt").scroll(function() {
            $("#tcInProgressPosted #colLeftCard .tbl-body-calendar").scrollTop($("#tcInProgressPosted #colRightCard .tbl-body-calendar-gantt").scrollTop());
        });
    });

</script>
<!-- Task Centre - Synchronize Scroll -->

<!-- Gantt Chart Collapse Height Function -->
<script>
    // To set gantt chart collapsible height same with task details height

    // Applied Task Active Section
    $('#tcInProgressApplied #colLeftCard .tbl-group-calendar-awarded-row .multi-collapse').on('show.bs.collapse', function() {
        var index = 0;
        index = $(this).parent().index();
        var collapseLeftCardHeight = $(this).outerHeight(true);
        //console.log("Height: " + collapseLeftCardHeight);
        var collapseRightCard = $('#tcInProgressApplied #colRightCard .tbl-group-calendar-awarded-row .tbl-group-calendar-gantt-bar-wrapper').children().eq(index);
        var rightIndex = collapseRightCard.find('.multi-collapse .bar-row-gantt-chart-height');
        rightIndex.css('height', collapseLeftCardHeight + "px");
        collapseRightCard.find('.multi-collapse').collapse("show");

    });
    $('#tcInProgressApplied #colLeftCard .tbl-group-calendar-awarded-row .multi-collapse').on('hide.bs.collapse', function() {
        var index = 0;
        index = $(this).parent().index();
        var collapseRightCard = $('#tcInProgressApplied #colRightCard .tbl-group-calendar-awarded-row .tbl-group-calendar-gantt-bar-wrapper').children().eq(index);
        collapseRightCard.find('.multi-collapse').collapse("hide");
    });

    // Applied Task Inactive Section
    $('#tcInProgressApplied #colLeftCard .tbl-group-calendar-created-row .multi-collapse').on('show.bs.collapse', function() {
        var index = 0;
        index = $(this).parent().index();
        var collapseLeftCardHeight = $(this).outerHeight();
        var collapseRightCard = $('#tcInProgressApplied #colRightCard .tbl-group-calendar-created-row .tbl-group-calendar-gantt-bar-wrapper').children().eq(index);
        var rightIndex = collapseRightCard.find('.multi-collapse .bar-row-gantt-chart-height');
        rightIndex.css('height', collapseLeftCardHeight + "px");
        collapseRightCard.find('.multi-collapse').collapse("show");

    });
    $('#tcInProgressApplied #colLeftCard .tbl-group-calendar-created-row .multi-collapse').on('hide.bs.collapse', function() {
        var index = 0;
        index = $(this).parent().index();
        var collapseRightCard = $('#tcInProgressApplied #colRightCard .tbl-group-calendar-created-row .tbl-group-calendar-gantt-bar-wrapper').children().eq(index);
        collapseRightCard.find('.multi-collapse').collapse("hide");
    });

    // Posted Task Active Section
    $('#tcInProgressPosted #colLeftCard .tbl-group-calendar-awarded-row .multi-collapse').on('show.bs.collapse', function() {
        var index = 0;
        index = $(this).parent().index();
        var collapseLeftCardHeight = $(this).outerHeight(true);
        //console.log("Height: " + collapseLeftCardHeight);
        var collapseRightCard = $('#tcInProgressPosted #colRightCard .tbl-group-calendar-awarded-row .tbl-group-calendar-gantt-bar-wrapper').children().eq(index);
        var rightIndex = collapseRightCard.find('.multi-collapse .bar-row-gantt-chart-height');
        rightIndex.css('height', collapseLeftCardHeight + "px");
        collapseRightCard.find('.multi-collapse').collapse("show");

    });
    $('#tcInProgressPosted #colLeftCard .tbl-group-calendar-awarded-row .multi-collapse').on('hide.bs.collapse', function() {
        var index = 0;
        index = $(this).parent().index();
        var collapseRightCard = $('#tcInProgressPosted #colRightCard .tbl-group-calendar-awarded-row .tbl-group-calendar-gantt-bar-wrapper').children().eq(index);
        collapseRightCard.find('.multi-collapse').collapse("hide");
    });


    // Posted Task Inactive Section
    $('#tcInProgressPosted #colLeftCard .tbl-group-calendar-created-row .multi-collapse').on('show.bs.collapse', function() {
        var index = 0;
        index = $(this).parent().index();
        var collapseLeftCardHeight = $(this).outerHeight();
        var collapseRightCard = $('#tcInProgressPosted #colRightCard .tbl-group-calendar-created-row .tbl-group-calendar-gantt-bar-wrapper').children().eq(index);
        var rightIndex = collapseRightCard.find('.multi-collapse .bar-row-gantt-chart-height');
        rightIndex.css('height', collapseLeftCardHeight + "px");
        collapseRightCard.find('.multi-collapse').collapse("show");

    });
    $('#tcInProgressPosted #colLeftCard .tbl-group-calendar-created-row .multi-collapse').on('hide.bs.collapse', function() {
        var index = 0;
        index = $(this).parent().index();
        var collapseRightCard = $('#tcInProgressPosted #colRightCard .tbl-group-calendar-created-row .tbl-group-calendar-gantt-bar-wrapper').children().eq(index);
        collapseRightCard.find('.multi-collapse').collapse("hide");
    });

    window.addEventListener('popstate', function(e){
        if(e.state){
            $('.card-body-taskcentre.card-body-taskcentre-progress').html(e.state.calendarHtml);
        }
    });

</script>
<?php end_content_for(); ?>