<?php if(!empty($all['active_jobs'])): ?>
    <?php set('url', request_uri()); ?>
    <?php foreach ($favourites_list as $favourite): ?>
        <?php if( !in_array($favourite['job_id'], array_column($all['active_jobs'], 'task_id')) ) continue; ?>
		<div class="col-my-favourite-wrapper">
            <?php
            set('job', $favourite);
            set('favourites', ['ids' => $favourites]);
            set('jobs_applied', $applied);
            if( $favourite['type'] === 'internal_job' )
                echo partial('workspace/partial/task-listing/job.html.php');
            else
                echo partial('workspace/partial/task-listing/external_job.html.php');
            ?>
		</div>
    <?php endforeach; ?>
<?php /*else: */?><!--
	<div class="col-my-favourite-wrapper empty">
		<div class="col-task-row-container">
			<div class="task-row-col-top-flex job">
				<div class="empty-state-container">
					<div class="empty-img"><img src="<?php /*echo url_for("/assets/img/mtp-no-data-2.png") */?>" alt=""></div>
					<span class="empty-lbl"><?php /*echo lang('fav_no_task'); */?></span>
				</div>
			</div>
		</div>
	</div>-->
<?php endif ?>