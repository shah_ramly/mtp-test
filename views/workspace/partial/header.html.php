<div class="logo-sm">
	<a href="<?php echo url_for('dashboard') ?>" class="logo-icon-link">
		<span class="logo-icon-sm"><img src="<?php echo url_for('/assets/img/logoicon.png') ?>" alt=""></span>
	</a>
</div>
<ul class="col-account-header">
    <?php if((int)$user->info['type'] === 0 && (int)$completion < 100): ?>
	<li class="col-right-flex col-complete-profile">
		<button type="button" id="completeProfileBtn" class="btn-icon-full btn-complete-profile-header progress-<?php echo $completion ?>"
		        data-toggle="modal" data-target="#modal_complete_profile" data-text="<?php echo lang('header_complete_your_profile') ?>">
			<span class="btn-label"><?php echo $completion.'%'; ?></span>
		</button>
		<!--<span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="<?php /*echo lang('header_complete_your_profile_tooltip'); */?>">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
            </svg>
        </span>-->
	</li>
	<?php endif ?>
	<li class="col-right-flex col-post-task">
		<?php if((int)$user->info['type'] === 0 && (int)$completion < 100): ?>
		<button type="button" class="btn-header-post" id="post_task_btn" data-toggle="modal" data-target="#modal_complete_profile" data-text="<?php echo lang('header_post_a_task') ?>">
		<?php else: ?>
		<button type="button" class="btn-header-post" id="post_task_btn"
		        data-toggle="modal" data-backdrop="static" data-target="#post_task_od" data-text="<?php echo lang('header_post_a_task') ?>">
		<?php endif ?>
            <span class="plus-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round">
                    <line x1="12" y1="5" x2="12" y2="19"></line>
                    <line x1="5" y1="12" x2="19" y2="12"></line>
                </svg>
            </span>
			<span class="plus-icon-lbl"><?php echo lang('header_post_a_task'); ?></span>
		</button>
		<?php if((int)$user->info['type'] === 1): ?>
		<button type="button" class="btn-header-post" id="post_job_btn"
		        data-toggle="modal" data-backdrop="static" data-target="#post_job_ft" data-text="<?php echo lang('header_post_a_job') ?>">
            <span class="plus-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round">
                    <line x1="12" y1="5" x2="12" y2="19"></line>
                    <line x1="5" y1="12" x2="19" y2="12"></line>
                </svg>
            </span>
			<span class="plus-icon-lbl"><?php echo lang('header_post_a_job'); ?></span>
		</button>
		<?php /*else: */?><!--
		<button type="button" class="btn-header-post as-post-a-job" style="width:115px;display:none" id="post_job_btn" data-toggle="modal" data-backdrop="static" data-target="#post_task_od">
            <span class="plus-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round">
                    <line x1="12" y1="5" x2="12" y2="19"></line>
                    <line x1="5" y1="12" x2="19" y2="12"></line>
                </svg>
            </span>
			<span class="plus-icon-lbl">Post a Task</span>
		</button>-->
		<?php endif ?>
	</li>
    <?php echo partial('partial/notifications.html.php'); ?>
    <?php echo partial('partial/user.html.php'); ?>
</ul>

<?php if(!empty($_SESSION[WEBSITE_PREFIX.'FIRST_LOGIN'])){ ?>
<?php if( !isset($was_viewing) ): ?>
<!-- Complete Profile Popover Only First Time Landing Dashboard -->
<script>
    $(function() {
        var elem = '<div class="popover-complete-profile"><div class="popover-title"><?php echo lang('dashboard_popover_complete_profile_title'); ?></div><div class="popover-desc"><?php echo lang('dashboard_popover_complete_profile_desc'); ?></div><div class="popover-actions"><button id="close-popover" data-toggle="clickover" class="btn-txt-only btn-gotit" onclick="$(&quot;#completeProfileBtn&quot;).popover(&quot;dispose&quot;);"><?php echo lang('dashboard_popover_complete_profile_btn'); ?></button></div></div>';
	    var complete_profile_popup_displayed = localStorage.getItem('completeProfilePopup');
        $('#completeProfileBtn').popover({
                animation: true,
                content: elem,
                html: true,
                placement: 'bottom',
                sanitize: false
        });
        if(complete_profile_popup_displayed === null) {
            window.setTimeout("$('#completeProfileBtn').popover('show')", 300);
            localStorage.setItem('completeProfilePopup', true);
        }
    });
</script>
<!-- Complete Profile Popover Only First Time Landing Dashboard -->
<?php else: ?>
	<script>
		$(function(){
		    $('#modal_cont_view').on('hidden.bs.modal', function(e){
                history.pushState({}, '', '<?php echo option('site_uri') . url_for('/dashboard') ?>');
                var elem = '<div class="popover-complete-profile"><div class="popover-title"><?php echo lang('dashboard_popover_complete_profile_title'); ?></div><div class="popover-desc"><?php echo lang('dashboard_popover_complete_profile_desc'); ?></div><div class="popover-actions"><button id="close-popover" data-toggle="clickover" class="btn-txt-only btn-gotit" onclick="$(&quot;#completeProfileBtn&quot;).popover(&quot;dispose&quot;);"><?php echo lang('dashboard_popover_complete_profile_btn'); ?></button></div></div>';
                var complete_profile_popup_displayed = localStorage.getItem('completeProfilePopup');
                $('#completeProfileBtn').popover({
                    animation: true,
                    content: elem,
                    html: true,
                    placement: 'bottom',
                    sanitize: false
                });
                if(complete_profile_popup_displayed === null) {
                    window.setTimeout("$('#completeProfileBtn').popover('show')", 300);
                    localStorage.setItem('completeProfilePopup', true);
                }
		    });
		});
	</script>
<?php endif ?>
<?php unset($_SESSION[WEBSITE_PREFIX.'FIRST_LOGIN']); } ?>