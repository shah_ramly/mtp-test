<div class="tab-content" id="pills-tabSearchResultContent">
<?php echo partial("workspace/partial/task-listing/task-tab.html.php", ['listing' => $tasks_listing['listing']]) ?>
<?php echo partial("workspace/partial/task-listing/job-tab.html.php") ?>
<?php echo partial("workspace/partial/task-listing/talent-tab.html.php") ?>
</div>

<?php content_for('extra_scripts'); ?>
<script>
    $('#modal_remove_saved').on('show.bs.modal', function (event) {
        var link = $(event.relatedTarget);
        var search_id = link.data('searchId');
        var search_title = link.data('searchTitle');
        var modal = $(this);

        modal.find('span.highlighted').text("'"+search_title+"'");
        modal.find('.delete-search.btn-confirm').on('click', function(e){
            $.ajax({
                url: '<?php echo option('site_uri') . url_for('search/delete') ?>',
                method: 'POST',
                data: {'search_name': search_title, 'search_id': search_id}
            }).done(function(response){
                if(response.status === 'success'){
                    link.parent().remove();
                    t('s', response.message);
                }
            });
        });
    });
</script>
<?php end_content_for() ?>