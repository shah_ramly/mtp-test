<div class="card-body-searchresult">
<div class="row">
    <div class="col-xl-12 col-searcresult-container">
	<ul class="nav nav-pills nav-pills-searchresult" id="pills-tabSearchResult" role="tablist">
		<li class="nav-item">
			<a class="nav-link <?php echo $scope === 'task' ? 'active' : '' ?>" id="searchTaskTab-tab" href="<?php echo option('site_uri') . url_for('workspace/search') . '?scope=task' ?>" role="tab" aria-controls="searchTaskTab" aria-selected="true"><?php echo lang('dashboard_search_for_on_demand_tasks'); ?></a>
		</li>
		<li class="nav-item">
			<a class="nav-link <?php echo $scope === 'job' ? 'active' : '' ?>" id="searchJobTab-tab" href="<?php echo option('site_uri') . url_for('workspace/search') . '?scope=job' ?>" role="tab" aria-controls="searchJobTab" aria-selected="false"><?php echo lang('dashboard_search_for_full_time_work'); ?></a>
		</li>
		<li class="nav-item">
			<a class="nav-link <?php echo $scope === 'talent' ? 'active' : '' ?>" id="searchTalentTab-tab" href="<?php echo option('site_uri') . url_for('workspace/search') . '?scope=talent' ?>" role="tab" aria-controls="searchTalentTab" aria-selected="false"><?php echo lang('dashboard_search_for_talent'); ?></a>
		</li>
	</ul>
    <?php echo partial('workspace/partial/search/tabs-contents.html.php'); ?>
	</div>
</div>
</div>