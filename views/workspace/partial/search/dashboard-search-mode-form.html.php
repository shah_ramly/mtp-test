<div class="card-body-searchresult">
<div class="row">
    <div class="col-xl-12 col-searcresult-container">
	<ul class="nav nav-pills nav-pills-searchresult" id="pills-tabSearchResult" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="searchTaskTab-tab" data-toggle="pill" href="#searchTaskTab" role="tab" aria-controls="searchTaskTab" aria-selected="true"><?php echo lang('dashboard_search_for_on_demand_tasks'); ?></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="searchJobTab-tab" data-toggle="pill" href="#searchJobTab" role="tab" aria-controls="searchJobTab" aria-selected="false"><?php echo lang('dashboard_search_for_full_time_work'); ?></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="searchTalentTab-tab" data-toggle="pill" href="#searchTalentTab" role="tab" aria-controls="searchTalentTab" aria-selected="false"><?php echo lang('dashboard_search_for_talent'); ?></a>
		</li>
	</ul>
    <?php echo partial('workspace/partial/search/dashboard-tabs-contents.html.php'); ?>
	</div>
</div>
</div>