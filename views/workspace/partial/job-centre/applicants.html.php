<div id="table-listing-container-talent" class="table-listing-container">
    <table class="table">
        <thead>
        <tr>
            <th class="info"><?php echo lang('job_applicant_full_name') ?></th>
            <th class="info"><?php echo lang('job_application_status') ?></th>
            <th class="info"><?php echo lang('job_date_applied') ?></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($applicants as $applicant): ?>
        <?php 
        $resume = $cover = $certificate = '';
	    if( $applicant['docs'] !== '' ){
	    	$docs = explode(',', $applicant['docs']);
	    	foreach ($docs as $doc){
	    		if(strpos($doc, "/resume/") !== false){
	    			$resume = url_for($doc);
			    }elseif (strpos($doc, "/cover_letter/") !== false){
	    			$cover = url_for($doc);
			    }elseif (strpos($doc, "/cert/") !== false){
	    			$certificate = url_for($doc);
			    }
		    }
	    }    
        ?>
        <tr>
            <td class="info <?php if($applicant['viewed'] === 'N') echo 'new-active' ?>"><?php echo $applicant['name'] ?></td>
            <td class="info"><?php echo ($applicant['shortlisted'] === 'true') ? lang('job_shortlisted') : ( in_array($job['status'], ['cancelled', 'closed']) ? lang('job_unsuccessful') : lang('job_applied') ) ?></td>
            <td class="info"><?php echo $applicant['applied_at'] ?></td>
            <td class="col-docs">
                <span class="col-docs-resume" title="<?php echo lang('job_resume_file') ?>">
                    <a href="<?php echo $resume ?>" target="_blank" class="docs-resume <?php if($resume) echo 'active' ?>" title="<?php echo lang('job_resume_file') ?>">
                        <span class="docs-icon docs-resume-icon">
                            <svg width="24" height="24" viewBox="0 0 16 16" class="bi bi-file-earmark-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                              <path d="M4 0h5.5v1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V4.5h1V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2z"/>
                              <path d="M9.5 3V0L14 4.5h-3A1.5 1.5 0 0 1 9.5 3z"/>
                              <path fill-rule="evenodd" d="M8 11a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                              <path d="M8 12c4 0 5 1.755 5 1.755V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1v-.245S4 12 8 12z"/>
                            </svg>
                        </span>
                    </a>
                </span>
                <span class="col-docs-letter" title="<?php echo lang('job_cover_letter_file') ?>">
                    <a href="<?php echo $cover ?>" target="_blank" class="docs-letter <?php if($cover) echo 'active' ?>" title="<?php echo lang('job_cover_letter_file') ?>">
                        <span class="docs-icon docs-letter-icon">
                            <svg width="24" height="24" viewBox="0 0 16 16" class="bi bi-file-earmark-text" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                              <path d="M4 0h5.5v1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V4.5h1V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2z"/>
                              <path d="M9.5 3V0L14 4.5h-3A1.5 1.5 0 0 1 9.5 3z"/>
                              <path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5zm0-2a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0-2a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z"/>
                            </svg>
                        </span>
                    </a>
                </span>
                <span class="col-docs-cert" title="<?php echo lang('job_award_and_certification_files') ?>">
                    <a href="<?php echo $certificate ?>" target="_blank" class="docs-cert <?php if($certificate) echo 'active' ?>">
                        <span class="docs-icon docs-cert-icon">
                            <svg width="24" height="24" viewBox="0 0 16 16" class="bi bi-award" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                              <path fill-rule="evenodd" d="M9.669.864L8 0 6.331.864l-1.858.282-.842 1.68-1.337 1.32L2.6 6l-.306 1.854 1.337 1.32.842 1.68 1.858.282L8 12l1.669-.864 1.858-.282.842-1.68 1.337-1.32L13.4 6l.306-1.854-1.337-1.32-.842-1.68L9.669.864zm1.196 1.193l-1.51-.229L8 1.126l-1.355.702-1.51.229-.684 1.365-1.086 1.072L3.614 6l-.25 1.506 1.087 1.072.684 1.365 1.51.229L8 10.874l1.356-.702 1.509-.229.684-1.365 1.086-1.072L12.387 6l.248-1.506-1.086-1.072-.684-1.365z"/>
                              <path d="M4 11.794V16l4-1 4 1v-4.206l-2.018.306L8 13.126 6.018 12.1 4 11.794z"/>
                            </svg>
                        </span>
                    </a>
                </span>
            </td>
            <td><a href="#" data-toggle="modal" data-target="#modal_view_answer"
                   data-id="<?php echo $applicant['user_id'] ?>"
                   data-job-id="<?php echo $job['id'] ?>"
	            ><?php echo lang('job_view_answer') ?></a></td>
            <td><a href="#" data-toggle="modal"
                   data-id="<?php echo $applicant['user_id'] ?>"
                   data-job-id="<?php echo $job['id'] ?>"
                   data-task-status="<?php echo $applicant['shortlisted'] ?>"
                   data-target="#modal_talent_details"><?php echo lang('job_view_profile') ?></a></td>
            <td class="col-chat <?php if(!empty($job['has_new_chats']) && in_array($applicant['user_id'], $job['has_new_chats'])) echo 'active'; ?>">
	            <a href="#"
	               class="chatButton"
	               data-toggle="modal"
	               data-user-id="<?php echo $applicant['user_id'] ?>"
	               data-target="#modal_chat"
	               data-task-type="job"
	               data-task-id="<?php echo $job['id'] ?>"
	               data-task-slug="<?php echo $job['slug'] ?>"
	               data-task-closed="<?php echo (in_array($job['status'], ['cancelled', 'completed']) && \Carbon\Carbon::parse($job['updated_at'])->addDays(5)->isBefore(\Carbon\Carbon::now())) ? 'true' : 'false' ?>"
	               data-task-closed-date="<?php echo in_array($job['status'], ['cancelled', 'completed']) ? \Carbon\Carbon::parse($job['updated_at'])->addDays(5)->format("d F Y - h.ia") : 'false' ?>"
	               data-chat-enabled="true"><?php echo lang('job_chat') ?></a>
            </td>
        </tr>
        <?php endforeach ?>
        </tbody>
    </table>
</div>
<?php echo partial("/workspace/partial/job-centre/applicants-pagination.html.php",
    [
        'count'        => count($applicants),
        'job'          => $job,
        'current_page' => $page,
    ]
) ?>