<?php $post_id = $post['type'] === 'task' ? $post['task_id'] : $post['job_id'] ?>
<?php $company_number = (string)('TL' . (20100 + (int)$user->info['id'])) ?>
<div class="col-microsite-padding">
    <div class="col-microsite-row-container">
        <div class="task-row-col-top-flex">
            <div class="task-row-col-left">
                <a href="<?php echo url_for("workspace/{$post['type']}/{$post['slug']}/details") ?>" class="task-row-taskname">
                    <?php echo $post['title'] ?>
                </a>
                <div class="task-row-taskdesc">
                    <?php echo (strlen(strip_tags(str_replace('<', ' <', $post['description']))) > 235) ? substr(strip_tags(str_replace('<', ' <', $post['description'])), 0, 235) . ' ...' : strip_tags(str_replace('<', ' <', $post['description'])); ?>
                </div>
            </div>
            <div class="task-row-col-right">
                <?php $budget = ($post['type'] === 'task') ? explode('.', $post['budget']) : explode('.', $post['salary_range_max']); ?>
                <div class="task-row-taskamount">
                    <span class="taskamount-prefix">RM</span>
                    <span class="taskamount-val"><?php echo $budget[0] ?></span>
                    <span class="taskamount-suffix">.<?php echo $budget[1] ?></span>
                </div>
                <div class="task-row-taskduration">
                    <span class="taskduration-val">
                        <?php if($post['type'] === 'task'): ?>
                            <?php echo ($post['task_type'] === 'by-hour') ?
		                        $post['hours'] . "&nbsp;" . ($post['hours'] == 1 ? lang('task_hour') : lang('task_hours')) . "&nbsp;" . lang('task_details_task') : lang('task_details_lump_sum_task') ?>
                        <?php else: ?>
                            <?php echo lang('public_search_monthly_salary') ?>
                        <?php endif ?>
                    </span>
                </div>
            </div>
        </div>
        <div class="microsite-row-col-btm-flex">
            <div class="task-row-col-left">
                <div class="task-row-posterdetails">
                    <div class="task-row-posterdetails-val">
                        <a href="<?php echo url_for("/company/{$company_number}") ?>" class="posterdetail-link">
                            <div class="tbl-group-calendar-photo">
                                <img src="<?php echo imgCrop($user->info['photo'], 35, 35, 'assets/img/default-avatar.png') ?>" alt="Profile Photo">
                            </div>
                            <span class="posterdetail-name">
                                <?php echo $user->info['company']['name']; ?>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-profile-details">
                            <div class="task-details-row task-details-profile">
                                <div class="profile-avatar">
                                    <a href="#" class="task-details-poster-link">
                                        <img class="avatar-img" src="<?php echo imgCrop($post['photo'], 100, 100, 'assets/img/default-avatar.png') ?>" alt="">
                                    </a>
                                </div>
                                <a href="#" class="task-details-poster-link">
                                    <h5 class="profile-name">
                                        <?php echo $user->info['company']['name'] ?>
                                    </h5>
                                </a>
                                <div class="task-details-profile-location">
                                    <span class="task-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path></svg></span>
                                    <span class="task-details-profile-location-val">
                                        <?php echo $cms->getStateName($user->info['state']); ?>, <?php echo $cms->getCountryName($user->info['country']); ?>
                                    </span>
                                </div>
                                <div class="task-details-profile-rating">
                                    <div class="task-details-profile-rating-star">
                                        <?php $starred = (int)floor($post['rating']['overall']/2); $rest = 5 - $starred; ?>
                                        <?php if($starred > 0): ?>
                                            <?php foreach (range(1, $starred) as $star): ?>
                                                <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <?php if($rest > 0): ?>
                                            <?php foreach (range(1, $rest) as $star1): ?>
                                                <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="task-details-profile-rating-val">
                                        <span class="task-row-average-rating-val">
                                            <?php echo number_format((float)$post['rating']['overall'], 1); ?>
                                        </span>
                                        <span class="task-row-divider">/</span>
                                        <span class="task-row-total-rating-val">
                                            <?php echo $post['rating']['count'] . " " . ($post['rating']['count'] === 1 ? lang('task_details_review') : lang('task_details_reviews')) ?>
                                        </span>
                                    </div>
                                    <div class="task-details-profile-published">
                                        <span class="task-details-profile-label"><?php echo lang('task_published') ?>:</span>
                                        <span class="task-details-profile-val"><?php echo $post['published_count'] ?></span>
                                    </div>
                                    <div class="task-details-profile-dispute">
                                        <span class="task-details-profile-val"><?php echo lang('task_details_dispute_resolution') ?> </span>
                                        <span class="bi-tooltip" data-toggle="tooltip" data-placement="top"
                                              title="<?php echo lang('this_user_has_adopted_dispute_resolution_for') .' '. $post['disputed_count'] .' '. lang('dispute_resolution_out_of') .' '. $post['published_count'] .' '. lang('dispute_resolution_projects_undertaken') ?>">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                                            </svg>
                                        </span>
                                    </div>
                                    <div class="task-details-profile-membersince">
                                        <span class="task-details-profile-label"><?php echo lang('task_details_member_since') ?>:</span>
                                        <span class="task-details-profile-val">
                                            <?php echo \Carbon\Carbon::parse($user->info['date_created'])->format("F y") ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="task-row-rating-flex">
                        <div class="task-row-rating-star">
                            <?php $starred = (int)floor($post['rating']['overall']/2); $rest = 5 - $starred; ?>
                            <?php if($starred > 0): ?>
                                <?php foreach (range(1, $starred) as $star): ?>
                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php if($rest > 0): ?>
                                <?php foreach (range(1, $rest) as $star1): ?>
                                    <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <div class="task-row-rating-val">
                            <span class="task-row-average-rating-val">
                                <?php echo number_format((float)$post['rating']['overall'], 1); ?>
                            </span>
                            <span class="task-row-divider">/</span>
                            <span class="task-row-total-rating-val">
                                <?php echo $post['rating']['count'] . " " . ($post['rating']['count'] === 1 ? lang('task_details_review') : lang('task_details_reviews')) ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="task-row-col-right">
                <?php if( isset($current_user) && $current_user->logged && $current_user->info['id'] !== $user->info['id'] ): ?>
                    <div class="microsite-row-actions">
	                    <form method="post" action="<?php echo option('site_uri') . url_for("/workspace/{$post['type']}/favourite") ?>">
                            <?php echo html_form_token_field(); ?>
		                    <input type="hidden" name="<?php echo $post['type'] ?>_id" value="<?php echo $post_id; ?>">
		                    <input type="hidden" name="favourited" value="<?php echo in_array($post_id, $current_user->favourite_ids) ? "true":"false"; ?>">
		                    <input type="hidden" name="url" value="<?php echo request_uri(); ?>">
		                    <button type="submit" class="btn-icon-full <?php echo in_array($post_id, $current_user->favourite_ids) ? 'btn-unfav':'btn-fav' ?>">
			                    <span class="btn-label"><?php echo in_array($post_id, $current_user->favourite_ids) ? lang('task_remove_fav') : lang('task_save_fav') ?></span>
			                    <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                <path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path>
                            </svg>
                        </span>
		                    </button>
	                    </form>
                        <button class="btn-icon-full btn-apply"
                            <?php if(!empty($post['questions']) || (int)$completion < 100): ?>
                                type="button"
                                data-toggle="modal"
                                <?php if((int)$completion !== 100): ?>
                                data-toggle="modal" data-target="#modal_complete_profile"
                                <?php else: ?>
                                data-target="#modal_apply_question_<?php echo $post_id ?>"
                                <?php endif; ?>
                            <?php else: ?>
                                type="submit"
                            <?php endif; ?>
                            <?php if( in_array($post_id, $current_user->applied_ids) ): ?> disabled="disabled" <?php endif ?>
                        >

                            <span class="btn-label"><?php echo lang('public_search_apply') ?></span>
                            <span class="btn-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                    <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                </svg>
                            </span>
                        </button>
                    </div>
                <?php else: ?>
	                <div class="microsite-row-actions">
		                <button type="button" class="btn-icon-full btn-apply" onclick="location.href='<?php echo url_for('/sign_up') ?>';">
			                <span class="btn-label"><?php echo lang('public_search_apply') ?></span>
			                <span class="btn-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                    <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                </svg>
                            </span>
		                </button>
	                </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>

<?php if(!empty($post['questions']) && $post['user_id'] !== $current_user->info['id']  && (int)$completion === 100): ?>
    <!-- Modal - Seeker's Questionnaire on Task Application -->
    <div id="modal_apply_question_<?php echo $post_id ?>" class="modal modal-apply-question fade" aria-labelledby="modal_custom_widget" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?php echo lang('post_step_2_post_requested_participation_questionnaire'); ?></h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <form name="form_task_question_<?php echo $post_id ?>" id="form_task_question" method="post" action="<?php echo url_for("/workspace/{$post['type']}/apply") ?>">
                        <?php echo html_form_token_field(); ?>
                        <input type="hidden" name="<?php echo $post['type'] ?>_id" value="<?php echo $post_id ?>">
                        <input type="hidden" name="url" value="<?php echo request_uri(); ?>">
                        <?php $count = count($post['questions'][$post_id]); $counter = 1; ?>
                        <?php foreach($post['questions'][$post_id] as $question): ?>
                            <div id="apply_question_<?php echo $counter ?>" class="frm form-apply-question"
                                <?php echo ($counter >= 2) ? ' style="display:none;"' : '' ?>
                            >
                                <div class="modal-steps-title"><?php echo lang('task_question') ?> <?php echo $counter ?> <?php echo lang('task_question_of') ?> <?php echo $count ?></div>
                                <div class="task-question"><?php echo $question['name'] ?></div>
                                <textarea class="form-control form-control-input" name="answer[<?php echo $question['id'] ?>]" placeholder="<?php echo lang('task_question_your_answer') ?>" rows="3" id="taskQuestion<?php echo $counter ?>" required></textarea>
                                <div class="form-group button-container">
                                    <?php if($counter == 1) : ?>
                                        <?php if($count > 1): ?>
                                            <button type="button" class="btn-icon-full btn-step-next qa-open<?php echo $counter ?>">
                                                <span class="btn-label"><?php echo lang('post_next') ?></span>
                                                <span class="btn-icon">
		                                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
		                                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
		                                        </svg>
		                                    </span>
                                            </button>
                                        <?php else: ?>
                                            <button type="submit" class="btn-icon-full btn-step-next qa-open<?php echo $counter ?>">
                                                <span class="btn-label"><?php echo lang('post_complete') ?></span>
                                                <span class="btn-icon">
		                                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
		                                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
		                                        </svg>
		                                    </span>
                                            </button>
                                        <?php endif ?>
                                    <?php else: ?>
                                        <button type="button" class="btn-icon-full btn-step-prev qa-back<?php echo $counter ?>">
                                            <span class="btn-label"><?php echo lang('post_back') ?></span>
                                            <span class="btn-icon">
	                                    <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                        <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
	                                </span>
                                        </button>
                                        <?php if($counter < $count): ?>
                                            <button type="button" class="btn-icon-full btn-step-next qa-open<?php echo $counter ?>">
                                                <span class="btn-label"><?php echo lang('post_next') ?></span>
                                                <span class="btn-icon">
	                                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                                    </svg>
	                                </span>
                                            </button>
                                        <?php else: ?>
                                            <button type="submit" class="btn-icon-full btn-step-next">
                                                <span class="btn-label"><?php echo lang('post_complete') ?></span>
                                                <span class="btn-icon">
	                                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                                        </svg>
	                                    </span>
                                            </button>
                                        <?php endif ?>
                                    <?php endif ?>
                                </div>
                            </div>
                            <?php $counter++; ?>
                        <?php endforeach; ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
<?php endif ?>