<div id="<?php echo strtolower($job['id']) ?>" class="tbl-group-calendar-bar-row-wrapper unsuccessful-task-row">
    <div class="tbl-group-calendar-bar-flex">
        <div class="tbl-group-calendar-bar-row row-1">
            <button class="btn btn-link btn-arrow-bar collapsed" id="settings-button" data-toggle="collapse" aria-expanded="true"><i class="tim-icons icon-minimal-up"></i></button>
            <span class="tbl-group-calendar-bar-title"><?php echo $job['title'] ?></span>
        </div>
        <div class="tbl-group-calendar-bar-row row-2 job"><?php echo "RM{$job['salary_range_min']} - RM{$job['salary_range_max']}" ?></div>
        <div class="tbl-group-calendar-bar-row row-3">
            <span class="tbl-taskcentre-completeby"><?php echo "{$job['state']}, {$job['country']}" ?></span></div>
        <div class="tbl-group-calendar-bar-row row-4 job"><?php echo lang('job_unsuccessful') ?></div>
    </div>
    <div class="multi-collapse collapse">
        <div class="taskcentre-details taskcentre-details-applied">
            <div class="taskcentre-details-row row-taskdesc">
                <label class="taskcentre-details-lbl lbl-taskdesc"><?php echo lang('job_description') ?></label>
                <div class="taskcentre-details-val val-taskdesc">
                    <p class="taskcentre-details-para"><?php echo $job['description'] ?></p>
                </div>
                <div class="taskcentre-details-viewmore job">

                    <div class="task-row-taskid ">
                        <span class="task-row-taskid-lbl"><?php echo lang('job_id') ?>:</span>
                        <span class="task-row-taskid-val"><?php echo $job['number'] ?></span>
                    </div>
                </div>
            </div>
            <div class="taskcentre-details-row row-taskcomp">
                <div class="taskcentre-details-row row-dateapplied">
                    <label class="taskcentre-details-lbl lbl-dateapplied"><?php echo lang('job_experience_level') ?></label>
                    <div class="taskcentre-details-val val-dateapplied"><?php echo $job['experience_level'] ?></div>
                </div>
                <div class="taskcentre-details-row row-tasknature">
                    <div class="taskcentre-details-row-group row-group-tasknature">
                        <label class="taskcentre-details-lbl lbl-tasknature"><?php echo lang('job_posted_date') ?></label>
                        <div class="taskcentre-details-val val-tasknature"><?php echo $job['posted_date'] ?></div>
                    </div>
                    <div class="taskcentre-details-row-group row-group-estimatedhours">
                        <label class="taskcentre-details-lbl lbl-estimatedhours"><?php echo lang('job_closed_date') ?></label>
                        <div class="taskcentre-details-val val-estimatedhours"><?php echo $job['close_date'] ?></div>
                    </div>
                </div>

                <div class="taskcentre-details-row row-tasknature">
                    <a href="<?php echo url_for("/workspace/job/{$job['slug']}/details") ?>" class="viewmore-taskdetails-link">
                        <span class="link-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="arcs">
                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                <circle cx="12" cy="12" r="3"></circle>
                            </svg>
                        </span>
                        <span class="link-lbl"><?php echo lang('job_view_more') ?></span>
                    </a>
                </div>

                <div class="taskcentre-details-row row-posterdetail">
                    <label class="taskcentre-details-lbl lbl-posterdetail"><?php echo lang('job_company_detail') ?></label>
                    <div class="taskcentre-details-val val-posterdetail">
                        <a href="<?php echo url_for("/company/{$job['poster']['number']}") ?>" class="posterdetail-link">
                            <div class="tbl-group-calendar-photo">
                                <img src="<?php echo imgCrop($job['poster']['photo'], 35, 35, 'assets/img/default-avatar.png') ?>" alt="Profile Photo">
                            </div>
                            <span class="posterdetail-name"><?php echo $job['poster']['company']['name'] ?></span>
                        </a>
                    </div>
	                <div class="taskcentre-details-chat">
		                <button type="button" class="btn-icon-full btn-chat <?php if(!empty($job['has_new_chats']) && in_array($job['user_id'], $job['has_new_chats'])) echo 'active'; ?>"
		                        data-toggle="modal"
		                        data-target="#modal_chat"
		                        data-user-id="<?php echo $job['user_id'] ?>"
		                        data-task-type="job"
		                        data-task-id="<?php echo $job['id'] ?>"
		                        data-task-slug="<?php echo $job['slug'] ?>"
		                        data-task-closed="<?php echo (in_array($job['status'], ['cancelled', 'completed']) && \Carbon\Carbon::parse($job['updated_at'])->addDays(5)->isBefore(\Carbon\Carbon::now())) ? 'true' : 'false' ?>"
		                        data-task-closed-date="<?php echo in_array($job['status'], ['cancelled', 'completed']) ? \Carbon\Carbon::parse($job['updated_at'])->addDays(5)->format("d F Y - h.ia") : 'false' ?>"
		                        data-chat-enabled="false<?php //echo $chat_status ?>"
                            <?php if( ! $job['has_chats'] ): ?>
				                disabled="disabled"
                            <?php endif ?>>
			                <span class="btn-label"><?php echo lang('job_chat') ?></span>
			                <span class="btn-icon">
                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                </svg>
                            </span>
		                </button>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>