<div id="<?php echo strtolower($job['id']) ?>" class="tbl-group-calendar-bar-row-wrapper posted-task-row closed-task-row">
    <div class="tbl-group-calendar-bar-flex">
        <div class="tbl-group-calendar-bar-row row-1">
            <button class="btn btn-link btn-arrow-bar collapsed" id="settings-button" data-toggle="collapse" aria-expanded="true"><i class="tim-icons icon-minimal-up"></i></button>
            <span class="tbl-group-calendar-bar-title"><?php echo $job['title'] ?></span>
        </div>
        <div class="tbl-group-calendar-bar-row row-2 job"><?php echo "RM{$job['salary_range_min']} - RM{$job['salary_range_max']}" ?></div>
        <div class="tbl-group-calendar-bar-row row-3">
            <span class="tbl-taskcentre-completeby"><?php echo "{$job['state']}, {$job['country']}" ?></span>
        </div>
        <div class="tbl-group-calendar-bar-row row-4 job">
            <span class="task-centre-status"><?php echo lang('job_closed') ?></span>
        </div>
    </div>
    <div class="multi-collapse collapse" style="">
        <div class="taskcentre-details taskcentre-details-applied">
            <div class="taskcentre-details-row row-taskdesc">
                <label class="taskcentre-details-lbl lbl-taskdesc"><?php echo lang('job_description') ?></label>
                <div class="taskcentre-details-val val-taskdesc">
                    <p class="taskcentre-details-para"><?php echo $job['description'] ?></p>
                </div>
                <div class="taskcentre-details-viewmore job">

                    <div class="task-row-taskid ">
                        <span class="task-row-taskid-lbl"><?php echo lang('job_id') ?>:</span>
                        <span class="task-row-taskid-val"><?php echo $job['number'] ?></span>
                    </div>
                </div>
            </div>
            <div class="taskcentre-details-row row-taskcomp">

                <div class="taskcentre-details-row row-dateapplied">
                    <label class="taskcentre-details-lbl lbl-dateapplied"><?php echo lang('job_experience_level') ?></label>
                    <div class="taskcentre-details-val val-dateapplied"><?php echo $job['experience_level'] ?></div>
                </div>
                <div class="taskcentre-details-row row-tasknature">
                    <div class="taskcentre-details-row-group row-group-tasknature">
                        <label class="taskcentre-details-lbl lbl-tasknature"><?php echo lang('job_posted_date') ?></label>
                        <div class="taskcentre-details-val val-tasknature"><?php echo $job['posted_date'] ?></div>
                    </div>
                    <div class="taskcentre-details-row-group row-group-estimatedhours">
                        <label class="taskcentre-details-lbl lbl-estimatedhours"><?php echo lang('job_closed_date') ?></label>
                        <div class="taskcentre-details-val val-estimatedhours"><?php echo $job['close_date'] ?></div>
                    </div>
                </div>

                <div class="taskcentre-details-row row-tasknature">
                    <a href="<?php echo url_for("/workspace/job/{$job['slug']}/details") ?>" class="viewmore-taskdetails-link">
                        <span class="link-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="arcs">
                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                <circle cx="12" cy="12" r="3"></circle>
                            </svg>
                        </span>
                        <span class="link-lbl"><?php echo lang('job_view_more') ?></span>
                    </a>
                </div>
            </div>
            <?php if( !empty($job['applicants']) ): ?>
                <?php echo partial("/workspace/partial/job-centre/applicants.html.php",
                    [
                        'job'        => $job,
                        'applicants' => $job['applicants'],
                        'page'       => isset($page) ? (int)$page : 1
                    ]
                ) ?>
            <?php endif ?>
        </div>
    </div>
</div>