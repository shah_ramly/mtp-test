<?php
	$pages = (int)ceil($count / 5);
	$next_page = $current_page + 1;
	$prev_page = $current_page - 1;
?>
<div class="table-listing-footer-pagination pagination-<?php echo explode('-', $job['id'])[0] ?>">
    <div class="footer-total-page"><?php echo lang('applicants_pagination_page') ?> <?php echo $current_page ?> <?php echo lang('applicants_pagination_of') ?> <?php echo $pages ?> <?php echo lang('applicants_pagination_total_applicants') ?></div>
    <nav class="nav-tbl-pagination">
        <ul class="pagination">
            <li class="page-item page-item-arrow page-item-prev <?php echo $current_page <= 1 ? 'disabled' : '' ?>">
                <a class="page-link job-applicant-pagination"
                   data-job="<?php echo explode('-', $job['id'])[0] ?>"
                <?php if($current_page <= 1): ?>
	                href="#"
                <?php else: ?>
	                href="<?php echo url_for("/workspace/job/applicants/{$job['id']}/{$prev_page}") ?>"
                <?php endif ?>
                   tabindex="-1">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M15 18l-6-6 6-6" /></svg>
                </a>
            </li>
	        <?php foreach (range(1, $pages) as $p): ?>
            <li class="page-item <?php echo ((int)$p === $current_page) ? 'active' : '' ?>">
                <a class="page-link job-applicant-pagination"
                   data-job="<?php echo explode('-', $job['id'])[0] ?>"
                   <?php if((int)$p === $current_page): ?>
                   href="#"
                   <?php else: ?>
                   href="<?php echo url_for("/workspace/job/applicants/{$job['id']}/{$p}") ?>"
	               <?php endif ?>
                >
	                <?php echo $p ?>
	                <?php if((int)$p === $current_page): ?>
	                <span class="sr-only">(current)</span>
	                <?php endif; ?>
                </a>
            </li>
	        <?php endforeach; ?>
            <li class="page-item page-item-arrow page-item-next  <?php echo $current_page >= $pages ? 'disabled' : '' ?>">
                <a class="page-link job-applicant-pagination"
                   data-job="<?php echo explode('-', $job['id'])[0] ?>"
                <?php if($current_page >= $pages): ?>
		           href="#"
                <?php else: ?>
		           href="<?php echo url_for("/workspace/job/applicants/{$job['id']}/{$next_page}") ?>"
                <?php endif ?>
                >
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 18l6-6-6-6" /></svg>
                </a>
            </li>
        </ul>
    </nav>
</div>