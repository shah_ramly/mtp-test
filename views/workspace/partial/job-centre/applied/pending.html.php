<div class="tab-pane tc-Pending fade" id="tcPending" role="tabpanel" aria-labelledby="tcPending-tab">
	<div class="tbl-body-calendar">
		<div class="tbl-group-calendar-row">
			<div class="table-filter-container show">
				<div class="tbl-group-calendar-title tbl-group-collapse-title"><span class="title-collapse"><?php echo lang('job_applied_group_title') ?></span><i class="fa fa-caret-down"></i></div>
				<div class="table-filter-right-col">
					<div class="dropdown dropdown-filter-container">
						<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<label class="filter-btn-lbl"><?php echo lang('job_filter_title') ?></label> <span class="drop-val"><?php echo lang('job_view_all') ?></span>
						</button>
						<div class="dropdown-menu applied-tasks-filter" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item active" href="#"><?php echo lang('job_view_all') ?></a>
							<a class="dropdown-item" href="#"><?php echo lang('job_applied') ?></a>
						</div>
					</div>
				</div>
			</div>
			<div class="tbl-collapse-group collapse show applied-tasks-list">
                <?php if(!empty($jobs)): ?>
                    <?php foreach ($jobs as $job): ?>
                        <?php if(in_array($job['status'], ['published']) && !$job['shortlisted']): ?>
                            <?php echo partial('/workspace/partial/job-centre/status/applied.html.php', ['job' => $job]) ?>
                        <?php elseif($job['shortlisted']): ?>
                            <?php echo partial('/workspace/partial/job-centre/status/shortlisted.html.php', ['job' => $job]) ?>
                        <?php endif ?>
                    <?php endforeach ?>
                <?php else: ?>
					<div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
						<div class="tbl-group-calendar-bar-flex">
							<div class="tbl-group-calendar-bar-row row-1">
								<span class="tbl-group-calendar-bar-title"><?php echo lang('job_no_jobs_yet') ?></span>
							</div>
						</div>
					</div>
                <?php endif ?>
			</div>
		</div>
	</div>
</div>