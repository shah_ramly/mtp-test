<?php

$colors = [
    'gold-card', // 'gold'
    'green-card', // 'green'
    'red-card', // 'red'
    'orange-card', // 'orange'
    'maroon-card', // 'maroon'
    'purple-card', // 'purple'
    'cyan-card', // 'cyan'
    'blue-card' // 'blue'
];

 if(!empty($all['active_tasks'])): ?>
    <?php set('url', request_uri()); ?>
    <?php $counter = 0; ?>
    <?php foreach ($favourites_list as $favourite): ?>
        <?php if( !in_array($favourite['task_id'], array_column($all['active_tasks'], 'task_id')) ) continue; ?>
        <div class="col-my-favourite-wrapper <?php echo $colors[$counter] ?>">
            <?php
            set('task', $favourite);
            set('favourites', ['ids' => $favourites]);
            set('tasks_applied', $applied);
            if( $favourite['type'] === 'internal_task' )
                echo partial('workspace/partial/task-listing/task.html.php');
            else
                echo partial('workspace/partial/task-listing/external_task.html.php');
            ?>
        </div>
        <?php $counter++;
        if( $counter > (count($colors) - 1) ) $counter = 0; ?>
    <?php endforeach; ?>
<?php /*else: */?><!--
    <div class="col-my-favourite-wrapper empty">
        <div class="col-task-row-container">
            <div class="task-row-col-top-flex job">
                <div class="empty-state-container">
                    <div class="empty-img"><img src="<?php /*echo url_for("/assets/img/mtp-no-data-2.png") */?>" alt=""></div>
                    <span class="empty-lbl"><?php /*echo lang('fav_no_task'); */?></span>
                </div>
            </div>
        </div>
    </div>-->
<?php endif ?>