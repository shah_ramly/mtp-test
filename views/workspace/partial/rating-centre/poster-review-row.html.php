<?php $talent_id = 'TL' . (20100 + $review['reviewer_id']) ?>
<div class="col-ratingcentre-container">
    <div class="col-ratingcentre-bottom-wrapper">
        <div class="row">
            <div class="col-xl-12 col-ratings-bottom">
                <div class="col-rating-first-bottom-group">
                    <div class="col-rating-user-info">
                        <div class="rating-avatar">
                            <a href="<?php echo url_for(($review['company_name'] ? 'company' : 'talent') . "/{$talent_id}") ?>" class="rating-photo-link">
                                <div class="col-rating-photo">
                                    <img src="<?php echo imgCrop($review['photo'], 35, 35, 'assets/img/default-avatar.png') ?>" alt="Profile Photo">
                                </div>
                            </a>
                        </div>
                        <a href="<?php echo url_for(($review['company_name'] ? 'company' : 'talent') . "/{$talent_id}") ?>" class="rating-name-link"><?php echo is_null($review['company_name']) ? "{$review['firstname']} {$review['lastname']}" : $review['company_name']; ?></a>
                        <span class="task-details-comment-date">
                            <?php echo \Carbon\Carbon::parse($review['created_at'])->diffForHumans(); ?>
                        </span>
                    </div>
                    <div class="bottom-rating-description"><?php echo $review['review'] ?></div>
                </div>
                <div class="col-bottom-rating-stars-task">
                    <div class="rating-star-average-group">
                        <div class="task-row-rating-star">
                            <?php
                            $score   = (float)$review['total_rating'];
                            $starred = (int)floor($score / 2);
                            $rest    = 5 - $starred;
                            ?>
                            <?php if($starred > 0): ?>
                                <?php foreach (range(1, $starred) as $star): ?>
                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <?php if($rest > 0): ?>
                                <?php foreach (range(1, $rest) as $star): ?>
                                    <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <div class="task-row-rating-val">
                            <span class="task-row-average-rating-val"><?php echo number_format($score, 1) ?></span>
                        </div>
                    </div>
                    <div class="task-row-taskid">
                        <a href="<?php echo url_for("workspace/task/{$review['slug']}/details"); ?>" class="task-id-link">
                            <span class="task-row-taskid-lbl"><?php echo lang('task_id') ?>:</span>
                            <span class="task-row-taskid-val"><?php echo $review['task_number'] ?></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>