<div class="col-xl-6">
    <div class="row">
        <div class="col-xl-12 col-ratingcentre-top">
            <div class="col-ratingcentre-top-wrapper">
                <div class="row">
                    <div class="col-xl-12 col-ratingcentre">
                        <div class="rating-centre-container">
                            <div class="sub-col-title"><?php echo lang('rating_overall_ratings_as_a_talent'); ?></div>
                            <div class="left">
                                <div class="stars-group">
                                    <div class="total-stars">
                                        <div class="left-stars">
                                            <span class="rating-centre-numbers">
                                                 <?php echo count($reviews['as_talent']) ? number_format(array_sum(array_column($reviews['as_talent'], 'total_rating'))/count($reviews['as_talent']), 1) : number_format(0,1); ?>
                                            </span>
                                            <span class="rating-centre-star-icon">
                                                <svg viewBox="0 0 16 16" class="bi bi-star-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                                </svg>
                                            </span>
                                        </div>
                                        <div class="left-rating">
                                            <?php echo lang('rating_based_on') .' '. count($reviews['as_talent']) .' '. (count($reviews['as_talent']) == 1 ? lang('rating_word'):lang('ratings_word')); ?>
                                        </div>
                                    </div>
                                    <?php if(!is_null($reviews['as_talent_since'])): ?>
                                        <div class="left-rating-since">
                                            <?php echo lang('rating_since'); ?> <?php echo \Carbon\Carbon::parse($reviews['as_talent_since'])->formatLocalized('%d %B %Y'); ?>
                                        </div>
                                    <?php endif; ?>
									<?php echo is_null($reviews['as_talent_since']) ? lang('rating_no_rating_received_yet') : ''; ?>
                                </div>
                                <div class="left-top-rating"><?php echo lang('rating_top_rating'); ?>
                                    <span class="top-rating-highlight">
                                        <?php echo $reviews['as_talent_top_rating'] ?> <?php echo lang('rating_out_of_ten'); ?>
                                    </span>
                                </div>
                            </div>
                            <div class="right">
								<label class="task-details-lbl task-rating-progress-lbl"><?php echo lang('rating_by_criteria'); ?></label>
                                <div class="feedback-rating">
                                    <div class="rating-stars-value"><?php echo lang('rating_timeliness'); ?></div>
                                    <div class="rating-stars-desc">
                                        <div id="progress_1" class="progressbar-chart_ratings"></div>
                                    </div>
                                </div>
                                <div class="feedback-rating">
                                    <div class="rating-stars-value"><?php echo lang('rating_expertise'); ?></div>
                                    <div class="rating-stars-desc">
                                        <div id="progress_2" class="progressbar-chart_ratings"></div>
                                    </div>
                                </div>
                                <div class="feedback-rating">
                                    <div class="rating-stars-value"><?php echo lang('rating_satisfactory_completion'); ?></div>
                                    <div class="rating-stars-desc">
                                        <div id="progress_3" class="progressbar-chart_ratings"></div>
                                    </div>
                                </div>
                                <div class="feedback-rating">
                                    <div class="rating-stars-value"><?php echo lang('rating_easy_to_work_with'); ?></div>
                                    <div class="rating-stars-desc">
                                        <div id="progress_4" class="progressbar-chart_ratings"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
                </div>
            </div>
        </div>
		<?php if(is_null($reviews['as_talent_since'])): ?>
		<div class="col-xl-12 col-ratingcentre-bottom">
			<div class="col-ratingcentre-container empty-state">
				<div class="col-ratingcentre-bottom-wrapper">
					<div class="row">
						<div class="col-xl-12 col-ratings-bottom">
							<div class="empty-state-container">
								<div class="empty-img"><img src="<?php echo url_for("/assets/img/mtp-no-data-2.png") ?>" alt="" /></div>
								<span class="empty-lbl"><?php echo lang('rating_no_review_received_yet'); ?></span>
								<span class="empty-desc"><?php echo lang('rating_apply_a_task_to_receive_review'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php endif ?>
        <div class="col-xl-12 col-ratingcentre-bottom">
            <?php if(!empty($reviews['as_talent'])): ?>
				<label class="task-details-lbl task-rating-list-lbl"><?php echo lang('rating_individual_ratings_and_feedback'); ?></label>
                <?php foreach($reviews['as_talent'] as $review): ?>
                    <?php echo partial('workspace/partial/rating-centre/talent-review-row.html.php', ['review' => $review]) ?>
                <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
</div>