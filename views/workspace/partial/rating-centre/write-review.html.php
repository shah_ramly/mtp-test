<div class="col-xl-12 col-write-review-poster-seeker">
	<div class="col-xl-12">
		<div class="row">
			<div class="col-xl-12 col-ratingcentre-top">
				<div class="col-ratingcentre-top-wrapper">
					<div class="row">
						<div class="col-xl-12 col-ratingcentre">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="col-xl-12">
        <div class="row">
            <div class="col-xl-12 col-ratingcentre-bottom">
	            <?php if( !empty( $written_reviews ) ): ?>
	            <?php foreach( $written_reviews as $review ): ?>
		            <?php if($review['review_for'] === 'poster'): ?>
		            <?php echo partial('workspace/partial/rating-centre/poster-review-row.html.php', ['review' => $review]); ?>
		            <?php else: ?>
		            <?php echo partial('workspace/partial/rating-centre/talent-review-row.html.php', ['review' => $review]); ?>
		            <?php endif ?>
	            <?php endforeach ?>
	            <?php else: ?>
	            <div class="col-ratingcentre-container empty-state">
		            <div class="col-ratingcentre-bottom-wrapper">
			            <div class="row">
				            <div class="col-xl-12 col-ratings-bottom">
					            <div class="empty-state-container">
						            <div class="empty-img"><img src="<?php echo url_for('/assets/img/mtp-no-data-2.png') ?>" alt="" /></div>
						            <span class="empty-lbl"><?php echo lang('rating_no_reviews_have_been_given_yet') ?></span>
						            <span class="empty-desc"><?php echo lang('rating_apply_or_post_task_to_write_review') ?>.</span>
					            </div>
				            </div>
			            </div>
		            </div>
	            </div>
	            <?php endif ?>
            </div>
        </div>
    </div>
</div>