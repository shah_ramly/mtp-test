<!-- Reset Review Form when opened -->
<script>
	$('#modal_write_review').on('show.bs.modal', function(e){
        var modal = $(this),
            button = $(e.relatedTarget),
            taskId = button.data('taskId');
            seekerId = button.data('seekerId');

	    modal.find('form')[0].reset();
        $('.form-group-rating-poster').hide();
        $('.form-group-rating-talent').hide();
        $('#modal_write_review .row-starrating label').hide();

        if(taskId){
            var hiddenInput = $('<input type="hidden" name="task" />');
            hiddenInput.val(taskId);
            modal.find('form').prepend(hiddenInput);
            modal.find('select#select_task_posted_performed').val(taskId).trigger('change');
            if(seekerId) {
                modal.find('#select_candidate.task_'+(taskId).replaceAll('-', '_'))
                    .prop('disabled', false)
                    .show()
                    .val(seekerId);
            }
        }
	});
</script>
<!-- Reset Review Form when opened -->
<!-- Write Review Modal - Rating Star Function -->
<script>
    $(document).ready(function() {

        $("#timeliness_rating_group .btn-rating").on('click', (function(e) {

            var previous_value = $("#timeliness_rating").val();

            var selected_value = $(this).attr("data-attr");
            $("#timeliness_rating").val(selected_value);

            $(".timeliness-rating").empty();
            $(".timeliness-rating").html(selected_value);


            for (i = 1; i <= selected_value; ++i) {
                $("#timeliness_rating_group #rating-star-" + i).toggleClass('btn-staractive');
                $("#timeliness_rating_group #rating-star-" + i).toggleClass('btn-stardefault');
            }

            for (ix = 1; ix <= previous_value; ++ix) {
                $("#timeliness_rating_group #rating-star-" + ix).toggleClass('btn-staractive');
                $("#timeliness_rating_group #rating-star-" + ix).toggleClass('btn-stardefault');
            }

        }));

        $("#quality_rating_group .btn-rating").on('click', (function(e) {

            var previous_value = $("#quality_rating").val();

            var selected_value = $(this).attr("data-attr");
            $("#quality_rating").val(selected_value);

            $(".quality-rating").empty();
            $(".quality-rating").html(selected_value);


            for (i = 1; i <= selected_value; ++i) {
                $("#quality_rating_group #rating-star-" + i).toggleClass('btn-staractive');
                $("#quality_rating_group #rating-star-" + i).toggleClass('btn-stardefault');
            }

            for (ix = 1; ix <= previous_value; ++ix) {
                $("#quality_rating_group #rating-star-" + ix).toggleClass('btn-staractive');
                $("#quality_rating_group #rating-star-" + ix).toggleClass('btn-stardefault');
            }

        }));

        $("#communication_rating_group .btn-rating").on('click', (function(e) {

            var previous_value = $("#communication_rating").val();

            var selected_value = $(this).attr("data-attr");
            $("#communication_rating").val(selected_value);

            $(".communication-rating").empty();
            $(".communication-rating").html(selected_value);


            for (i = 1; i <= selected_value; ++i) {
                $("#communication_rating_group #rating-star-" + i).toggleClass('btn-staractive');
                $("#communication_rating_group #rating-star-" + i).toggleClass('btn-stardefault');
            }

            for (ix = 1; ix <= previous_value; ++ix) {
                $("#communication_rating_group #rating-star-" + ix).toggleClass('btn-staractive');
                $("#communication_rating_group #rating-star-" + ix).toggleClass('btn-stardefault');
            }

        }));
        $("#responsiveness_rating_group .btn-rating").on('click', (function(e) {

            var previous_value = $("#responsiveness_rating").val();

            var selected_value = $(this).attr("data-attr");
            $("#responsiveness_rating").val(selected_value);

            $(".responsiveness-rating").empty();
            $(".responsiveness-rating").html(selected_value);


            for (i = 1; i <= selected_value; ++i) {
                $("#responsiveness_rating_group #rating-star-" + i).toggleClass('btn-staractive');
                $("#responsiveness_rating_group #rating-star-" + i).toggleClass('btn-stardefault');
            }

            for (ix = 1; ix <= previous_value; ++ix) {
                $("#responsiveness_rating_group #rating-star-" + ix).toggleClass('btn-staractive');
                $("#responsiveness_rating_group #rating-star-" + ix).toggleClass('btn-stardefault');
            }

        }));

        $('.post-review-form').on('change', 'input, select, textarea', function(e){
            var active = true;
            $('.post-review-form').find('input, textarea, select').not(':hidden').each(function(idx, item){
                if( ! $(item).val() ){
                    active = false;
                }
            });

            if( $('#select_task_posted_performed').children('optgroup').children(':selected').parent().attr('id') === 'review_task_posted' ){
                var type = [];
                $('.form-group-rating-talent').find('input').each(function(idx, input){
	                if(input.checked){
                        const regex = /\[([a-z]+)\]/;
                        const str = input.name;
                        let m;

                        if ((m = regex.exec(str)) !== null) {
                            if( type.indexOf(m[1]) === -1 ){
                                type.push(m[1]);
                            }
                        }
	                }
                });

                if( type.length < 4 ){
                    active = false;
                }
            }else{
                var type = [];
                $('.form-group-rating-poster').find('input').each(function(idx, input){
                    if(input.checked){
                        const regex = /\[([a-z]+)\]/;
                        const str = input.name;
                        let m;

                        if ((m = regex.exec(str)) !== null) {
                            if( type.indexOf(m[1]) === -1 ){
                                type.push(m[1]);
                            }
                        }
                    }
                });
                if( type.length < 4 ){
                    active = false;
                }
            }

            if( active ){
                $('.post-review-form').find('.btn-confirm').removeClass('btn-icon-disabled').prop('disabled', false);
            }else{
                $('.post-review-form').find('.btn-confirm').addClass('btn-icon-disabled').prop('disabled', true);
            }
        });

    });

</script>
<!-- Write Review Modal - Rating Star Function -->

<!-- Write Review Modal - Change Number Rating Based On Task Posted/Performed -->
<script>
    $("#select_task_posted_performed").change(function() {
        var selected = $("option:selected", this);
        if (selected.parent()[0].id === "review_task_posted") {
            $('#modal_write_review .row-starrating label').show();
            $('#modal_write_review #select_candidate').hide();
            $('#modal_write_review #select_candidate').prop('disabled', true);
            $('#modal_write_review #select_candidate.task_'+(selected.val()).replaceAll('-', '_'))
	            .prop('disabled', false)
                .show()
	            .children().first().prop('selected', true);
            $('.form-group-rating-talent').show();
            $('.form-group-rating-poster').hide();
            $('.form-group-rating-poster').find('input[type="radio"]').prop('disabled', true);
            $('.form-group-rating-talent').find('input[type="radio"]').prop('disabled', false);
        } else if (selected.parent()[0].id === "review_task_performed") {
            $('#modal_write_review .row-starrating label').show();
            $('.form-group-rating-poster').show();
            $('.form-group-rating-talent').hide();
            $('.form-group-rating-talent').find('input[type="radio"]').prop('disabled', true);
            $('.form-group-rating-poster').find('input[type="radio"]').prop('disabled', false);
            $('#modal_write_review #select_candidate').prop('disabled', true);
            $('#modal_write_review #select_candidate').hide();
        }
    });

    $('form.post-review-form').on('submit', function(e){
       e.preventDefault();
       $(this).find('button').prop('disabled', true);
       this.submit();
    });

</script>

<!-- Progress Bar Function -->
<script>
    // progressbar.js@1.0.0 version is used
    // Docs: https://progressbarjs.readthedocs.org/en/1.0.0/
    ///////////////// AS TALENT /////////////////
    if( $('#progress_1').length ) {
        var bar1 = new ProgressBar.Line(progress_1, {
            strokeWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            color: '#655f93',
            trailColor: '#eee',
            trailWidth: 1,
            svgStyle: {
                width: '100%',
                height: '100%'
            },
            text: {
                style: {
                    // Text color.
                    // Default: same as stroke color (options.color)
                },
                autoStyleContainer: false
            },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) / 10);
            }
        });

        bar1.animate(<?php echo $reviews['timeliness'] / 10 ?>); // Number from 0.0 to 1.0
    }
    if( $('#progress_2').length ) {
        var bar2 = new ProgressBar.Line(progress_2, {
            strokeWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            color: '#007cf0',
            trailColor: '#eee',
            trailWidth: 1,
            svgStyle: {
                width: '100%',
                height: '100%'
            },
            text: {
                style: {
                    // Text color.
                    // Default: same as stroke color (options.color)
                },
                autoStyleContainer: false
            },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) / 10);
            }
        });

        bar2.animate(<?php echo $reviews['expertise'] / 10 ?>); // Number from 0.0 to 1.0
    }
    if( $('#progress_3').length ) {
        var bar3 = new ProgressBar.Line(progress_3, {
            strokeWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            color: '#00c3bc',
            trailColor: '#eee',
            trailWidth: 1,
            svgStyle: {
                width: '100%',
                height: '100%'
            },
            text: {
                style: {
                    // Text color.
                    // Default: same as stroke color (options.color)
                },
                autoStyleContainer: false
            },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) / 10);
            }
        });

        bar3.animate(<?php echo $reviews['satisfactory'] / 10 ?>); // Number from 0.0 to 1.0
    }
    if( $('#progress_4').length ) {
        var bar4 = new ProgressBar.Line(progress_4, {
            strokeWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            color: '#ff9b9b',
            trailColor: '#eee',
            trailWidth: 1,
            svgStyle: {
                width: '100%',
                height: '100%'
            },
            text: {
                style: {
                    // Text color.
                    // Default: same as stroke color (options.color)
                },
                autoStyleContainer: false
            },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) / 10);
            }
        });

        bar4.animate(<?php echo $reviews['t_easy'] / 10 ?>); // Number from 0.0 to 1.0
    }
    ///////////////// AS POSTER /////////////////
    if( $('#progress_5').length ) {
        var bar5 = new ProgressBar.Line(progress_5, {
            strokeWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            color: '#655f93',
            trailColor: '#eee',
            trailWidth: 1,
            svgStyle: {
                width: '100%',
                height: '100%'
            },
            text: {
                style: {
                    // Text color.
                    // Default: same as stroke color (options.color)
                },
                autoStyleContainer: false
            },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) / 10);
            }
        });

        bar5.animate(<?php echo $reviews['p_easy'] / 10 ?>); // Number from 0.0 to 1.0
    }
    if( $('#progress_6').length ) {
        var bar6 = new ProgressBar.Line(progress_6, {
            strokeWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            color: '#007cf0',
            trailColor: '#eee',
            trailWidth: 1,
            svgStyle: {
                width: '100%',
                height: '100%'
            },
            text: {
                style: {
                    // Text color.
                    // Default: same as stroke color (options.color)
                },
                autoStyleContainer: false
            },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) / 10);
            }
        });

        bar6.animate(<?php echo $reviews['clear'] / 10 ?>); // Number from 0.0 to 1.0
    }
    if( $('#progress_7').length ) {
        var bar7 = new ProgressBar.Line(progress_7, {
            strokeWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            color: '#00c3bc',
            trailColor: '#eee',
            trailWidth: 1,
            svgStyle: {
                width: '100%',
                height: '100%'
            },
            text: {
                style: {
                    // Text color.
                    // Default: same as stroke color (options.color)
                },
                autoStyleContainer: false
            },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) / 10);
            }
        });

        bar7.animate(<?php echo $reviews['flexibility'] / 10 ?>); // Number from 0.0 to 1.0
    }
    if( $('#progress_8').length ) {
        var bar8 = new ProgressBar.Line(progress_8, {
            strokeWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            color: '#ff9b9b',
            trailColor: '#eee',
            trailWidth: 1,
            svgStyle: {
                width: '100%',
                height: '100%'
            },
            text: {
                style: {
                    // Text color.
                    // Default: same as stroke color (options.color)
                },
                autoStyleContainer: false
            },
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) / 10);
            }
        });

        bar8.animate(<?php echo $reviews['trustworthiness'] / 10 ?>); // Number from 0.0 to 1.0
    }
</script>
<!-- Progress Bar Function -->

<?php if( isset($_SESSION['review_id']) ): ?>
	<script>
        var writeReview = $('<a id="write-review" data-task-id="<?php echo $_SESSION['review_id'] ?>" <?php if(isset($_SESSION['review_seeker_id'])): ?> data-seeker-id="<?php echo $_SESSION['review_seeker_id'] ?>" <?php endif ?> data-toggle="modal" data-target="#modal_write_review" href="#" />');
        $('#pills-rc-writereview').append(writeReview);
        writeReview.trigger('click');
	</script>
    <?php unset($_SESSION['review_id'], $_SESSION['review_seeker_id']) ?>
<?php endif ?>