<!-- Modal - Write a Review -->
<div id="modal_write_review" class="modal modal-write-review fade" aria-labelledby="modal_write_review" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><?php echo lang('rating_post_a_rating_modal'); ?></div>
            <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                    <line x1="18" y1="6" x2="6" y2="18"></line>
                    <line x1="6" y1="6" x2="18" y2="18"></line>
                </svg>
            </button>
	        <form  method="post" class="post-review-form" action="<?php echo url_for('/workspace/rating-centre/submit-review') ?>">
                <?php echo html_form_token_field(); ?>
	            <div class="modal-body">
	                <div class="modal-body-container">
	                    <div class="form-group">
		                    <div class="para-note">
			                    <?php echo lang('rating_post_a_rating_modal_description'); ?>
		                    </div>
	                        <select id="select_task_posted_performed" name="task" class="form-control form-control-input">
	                            <option disabled selected=""><?php echo lang('rating_post_a_rating_modal_select_task'); ?></option>
		                        <?php if( !empty($posted) ): ?>
	                            <optgroup label="<?php echo lang('rating_post_a_rating_modal_task_you_posted'); ?>" id="review_task_posted">
		                            <?php foreach($posted as $task): ?>
	                                <option value="<?php echo $task['id'] ?>"><?php echo $task['title'] ?></option>
	                                <?php endforeach ?>
	                            </optgroup>
		                        <?php endif ?>
		                        <?php if( !empty($performed) ): ?>
	                            <optgroup label="<?php echo lang('rating_post_a_rating_modal_select_task_label_task_you_performed'); ?>" id="review_task_performed">
                                    <?php foreach($performed as $task): ?>
			                            <option value="<?php echo $task['id'] ?>"><?php echo $task['title'] ?></option>
                                    <?php endforeach ?>
	                            </optgroup>
		                        <?php endif ?>
	                        </select>
	                    </div>
		                <div class="form-group">
			                <?php foreach ($posted as $task): ?>
			                <?php if( !empty($task['seekers']) ): ?>
			                <select id="select_candidate" name="seeker" disabled class="form-control form-control-input task_<?php echo str_replace('-', '_', $task['id']) ?>" style="display:none;">
				                <option disabled="" selected="">Select Seeker</option>
                                <?php foreach ($task['seekers'] as $seeker): ?>
				                <option value="<?php echo $seeker['id'] ?>"><?php echo $seeker['name'] ?></option>
                                <?php endforeach ?>
			                </select>
			                <?php endif ?>
			                <?php endforeach ?>
		                </div>
	                    <div class="form-group">
	                        <textarea class="form-control form-control-input" name="review" placeholder="<?php echo lang('rating_post_a_rating_modal_write_a_review'); ?>" rows="3"></textarea>
	                    </div>
	                    <div class="form-group row-starrating">
	                        <label class="input-lbl input-lbl-block" style="display: none;">
	                            <span class="input-label-txt"><?php echo lang('rating_post_a_rating_modal_how_was_your_overall_experience'); ?>?</span>
	                        </label>
	                        <div class="form-group-rating-talent" style="display: none;">
	                            <div class="form-group form-group-flex" id="timeliness_rating_group">
	                                <div class="ratingstar-label"><?php echo lang('rating_timeliness'); ?></div>
	                                <div class="num-rating">
	                                    <input type="radio" id="10-num-1-talent" name="talent[timeliness]" value="10" />
	                                    <label for="10-num-1-talent" class="num last">10</label>
	                                    <input type="radio" id="9-num-1-talent" name="talent[timeliness]" value="9" />
	                                    <label for="9-num-1-talent" class="num">9</label>
	                                    <input type="radio" id="8-num-1-talent" name="talent[timeliness]" value="8" />
	                                    <label for="8-num-1-talent" class="num">8</label>
	                                    <input type="radio" id="7-num-1-talent" name="talent[timeliness]" value="7" />
	                                    <label for="7-num-1-talent" class="num">7</label>
	                                    <input type="radio" id="6-num-1-talent" name="talent[timeliness]" value="6" />
	                                    <label for="6-num-1-talent" class="num">6</label>
	                                    <input type="radio" id="5-num-1-talent" name="talent[timeliness]" value="5" />
	                                    <label for="5-num-1-talent" class="num">5</label>
	                                    <input type="radio" id="4-num-1-talent" name="talent[timeliness]" value="4" />
	                                    <label for="4-num-1-talent" class="num">4</label>
	                                    <input type="radio" id="3-num-1-talent" name="talent[timeliness]" value="3" />
	                                    <label for="3-num-1-talent" class="num">3</label>
	                                    <input type="radio" id="2-num-1-talent" name="talent[timeliness]" value="2" />
	                                    <label for="2-num-1-talent" class="num">2</label>
	                                    <input type="radio" id="1-num-1-talent" name="talent[timeliness]" value="1" />
	                                    <label for="1-num-1-talent" class="num first">1</label>
	                                </div>
	                            </div>
	                            <div class="form-group form-group-flex" id="quality_rating_group">
	                                <div class="ratingstar-label"><?php echo lang('rating_expertise'); ?></div>
	                                <div class="num-rating">
	                                    <input type="radio" id="10-num-2-talent" name="talent[expertise]" value="10" />
	                                    <label for="10-num-2-talent" class="num last">10</label>
	                                    <input type="radio" id="9-num-2-talent" name="talent[expertise]" value="9" />
	                                    <label for="9-num-2-talent" class="num">9</label>
	                                    <input type="radio" id="8-num-2-talent" name="talent[expertise]" value="8" />
	                                    <label for="8-num-2-talent" class="num">8</label>
	                                    <input type="radio" id="7-num-2-talent" name="talent[expertise]" value="7" />
	                                    <label for="7-num-2-talent" class="num">7</label>
	                                    <input type="radio" id="6-num-2-talent" name="talent[expertise]" value="6" />
	                                    <label for="6-num-2-talent" class="num">6</label>
	                                    <input type="radio" id="5-num-2-talent" name="talent[expertise]" value="5" />
	                                    <label for="5-num-2-talent" class="num">5</label>
	                                    <input type="radio" id="4-num-2-talent" name="talent[expertise]" value="4" />
	                                    <label for="4-num-2-talent" class="num">4</label>
	                                    <input type="radio" id="3-num-2-talent" name="talent[expertise]" value="3" />
	                                    <label for="3-num-2-talent" class="num">3</label>
	                                    <input type="radio" id="2-num-2-talent" name="talent[expertise]" value="2" />
	                                    <label for="2-num-2-talent" class="num">2</label>
	                                    <input type="radio" id="1-num-2-talent" name="talent[expertise]" value="1" />
	                                    <label for="1-num-2-talent" class="num first">1</label>
	                                </div>
	                            </div>
	                            <div class="form-group form-group-flex" id="communication_rating_group">
	                                <div class="ratingstar-label"><?php echo lang('rating_satisfactory_completion'); ?></div>
	                                <div class="num-rating">
	                                    <input type="radio" id="10-num-3-talent" name="talent[satisfactory]" value="10" />
	                                    <label for="10-num-3-talent" class="num last">10</label>
	                                    <input type="radio" id="9-num-3-talent" name="talent[satisfactory]" value="9" />
	                                    <label for="9-num-3-talent" class="num">9</label>
	                                    <input type="radio" id="8-num-3-talent" name="talent[satisfactory]" value="8" />
	                                    <label for="8-num-3-talent" class="num">8</label>
	                                    <input type="radio" id="7-num-3-talent" name="talent[satisfactory]" value="7" />
	                                    <label for="7-num-3-talent" class="num">7</label>
	                                    <input type="radio" id="6-num-3-talent" name="talent[satisfactory]" value="6" />
	                                    <label for="6-num-3-talent" class="num">6</label>
	                                    <input type="radio" id="5-num-3-talent" name="talent[satisfactory]" value="5" />
	                                    <label for="5-num-3-talent" class="num">5</label>
	                                    <input type="radio" id="4-num-3-talent" name="talent[satisfactory]" value="4" />
	                                    <label for="4-num-3-talent" class="num">4</label>
	                                    <input type="radio" id="3-num-3-talent" name="talent[satisfactory]" value="3" />
	                                    <label for="3-num-3-talent" class="num">3</label>
	                                    <input type="radio" id="2-num-3-talent" name="talent[satisfactory]" value="2" />
	                                    <label for="2-num-3-talent" class="num">2</label>
	                                    <input type="radio" id="1-num-3-talent" name="talent[satisfactory]" value="1" />
	                                    <label for="1-num-3-talent" class="num first">1</label>
	                                </div>
	                            </div>
	                            <div class="form-group form-group-flex" id="responsiveness_rating_group">
	                                <div class="ratingstar-label"><?php echo lang('rating_easy_to_work_with'); ?></div>
	                                <div class="num-rating">
	                                    <input type="radio" id="10-num-4-talent" name="talent[easy]" value="10" />
	                                    <label for="10-num-4-talent" class="num last">10</label>
	                                    <input type="radio" id="9-num-4-talent" name="talent[easy]" value="9" />
	                                    <label for="9-num-4-talent" class="num">9</label>
	                                    <input type="radio" id="8-num-4-talent" name="talent[easy]" value="8" />
	                                    <label for="8-num-4-talent" class="num">8</label>
	                                    <input type="radio" id="7-num-4-talent" name="talent[easy]" value="7" />
	                                    <label for="7-num-4-talent" class="num">7</label>
	                                    <input type="radio" id="6-num-4-talent" name="talent[easy]" value="6" />
	                                    <label for="6-num-4-talent" class="num">6</label>
	                                    <input type="radio" id="5-num-4-talent" name="talent[easy]" value="5" />
	                                    <label for="5-num-4-talent" class="num">5</label>
	                                    <input type="radio" id="4-num-4-talent" name="talent[easy]" value="4" />
	                                    <label for="4-num-4-talent" class="num">4</label>
	                                    <input type="radio" id="3-num-4-talent" name="talent[easy]" value="3" />
	                                    <label for="3-num-4-talent" class="num">3</label>
	                                    <input type="radio" id="2-num-4-talent" name="talent[easy]" value="2" />
	                                    <label for="2-num-4-talent" class="num">2</label>
	                                    <input type="radio" id="1-num-4-talent" name="talent[easy]" value="1" />
	                                    <label for="1-num-4-talent" class="num first">1</label>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="form-group-rating-poster" style="display: none;">
	                            <div class="form-group form-group-flex" id="ezwork_rating_group">
	                                <div class="ratingstar-label"><?php echo lang('rating_easy_to_work_with'); ?></div>
	                                <div class="num-rating">
	                                    <input type="radio" id="10-num-1-poster" name="poster[easy]" value="10" />
	                                    <label for="10-num-1-poster" class="num last">10</label>
	                                    <input type="radio" id="9-num-1-poster" name="poster[easy]" value="9" />
	                                    <label for="9-num-1-poster" class="num">9</label>
	                                    <input type="radio" id="8-num-1-poster" name="poster[easy]" value="8" />
	                                    <label for="8-num-1-poster" class="num">8</label>
	                                    <input type="radio" id="7-num-1-poster" name="poster[easy]" value="7" />
	                                    <label for="7-num-1-poster" class="num">7</label>
	                                    <input type="radio" id="6-num-1-poster" name="poster[easy]" value="6" />
	                                    <label for="6-num-1-poster" class="num">6</label>
	                                    <input type="radio" id="5-num-1-poster" name="poster[easy]" value="5" />
	                                    <label for="5-num-1-poster" class="num">5</label>
	                                    <input type="radio" id="4-num-1-poster" name="poster[easy]" value="4" />
	                                    <label for="4-num-1-poster" class="num">4</label>
	                                    <input type="radio" id="3-num-1-poster" name="poster[easy]" value="3" />
	                                    <label for="3-num-1-poster" class="num">3</label>
	                                    <input type="radio" id="2-num-1-poster" name="poster[easy]" value="2" />
	                                    <label for="2-num-1-poster" class="num">2</label>
	                                    <input type="radio" id="1-num-1-poster" name="poster[easy]" value="1" />
	                                    <label for="1-num-1-poster" class="num first">1</label>
	                                </div>
	                            </div>
	                            <div class="form-group form-group-flex" id="instruction_rating_group">
	                                <div class="ratingstar-label"><?php echo lang('rating_clear_instructions'); ?></div>
	                                <div class="num-rating">
	                                    <input type="radio" id="10-num-2-poster" name="poster[clear]" value="10" />
	                                    <label for="10-num-2-poster" class="num last">10</label>
	                                    <input type="radio" id="9-num-2-poster" name="poster[clear]" value="9" />
	                                    <label for="9-num-2-poster" class="num">9</label>
	                                    <input type="radio" id="8-num-2-poster" name="poster[clear]" value="8" />
	                                    <label for="8-num-2-poster" class="num">8</label>
	                                    <input type="radio" id="7-num-2-poster" name="poster[clear]" value="7" />
	                                    <label for="7-num-2-poster" class="num">7</label>
	                                    <input type="radio" id="6-num-2-poster" name="poster[clear]" value="6" />
	                                    <label for="6-num-2-poster" class="num">6</label>
	                                    <input type="radio" id="5-num-2-poster" name="poster[clear]" value="5" />
	                                    <label for="5-num-2-poster" class="num">5</label>
	                                    <input type="radio" id="4-num-2-poster" name="poster[clear]" value="4" />
	                                    <label for="4-num-2-poster" class="num">4</label>
	                                    <input type="radio" id="3-num-2-poster" name="poster[clear]" value="3" />
	                                    <label for="3-num-2-poster" class="num">3</label>
	                                    <input type="radio" id="2-num-2-poster" name="poster[clear]" value="2" />
	                                    <label for="2-num-2-poster" class="num">2</label>
	                                    <input type="radio" id="1-num-2-poster" name="poster[clear]" value="1" />
	                                    <label for="1-num-2-poster" class="num first">1</label>
	                                </div>
	                            </div>
	                            <div class="form-group form-group-flex" id="flexibility_rating_group">
	                                <div class="ratingstar-label"><?php echo lang('rating_flexibility'); ?></div>
	                                <div class="num-rating">
	                                    <input type="radio" id="10-num-3-poster" name="poster[flexibility]" value="10" />
	                                    <label for="10-num-3-poster" class="num last">10</label>
	                                    <input type="radio" id="9-num-3-poster" name="poster[flexibility]" value="9" />
	                                    <label for="9-num-3-poster" class="num">9</label>
	                                    <input type="radio" id="8-num-3-poster" name="poster[flexibility]" value="8" />
	                                    <label for="8-num-3-poster" class="num">8</label>
	                                    <input type="radio" id="7-num-3-poster" name="poster[flexibility]" value="7" />
	                                    <label for="7-num-3-poster" class="num">7</label>
	                                    <input type="radio" id="6-num-3-poster" name="poster[flexibility]" value="6" />
	                                    <label for="6-num-3-poster" class="num">6</label>
	                                    <input type="radio" id="5-num-3-poster" name="poster[flexibility]" value="5" />
	                                    <label for="5-num-3-poster" class="num">5</label>
	                                    <input type="radio" id="4-num-3-poster" name="poster[flexibility]" value="4" />
	                                    <label for="4-num-3-poster" class="num">4</label>
	                                    <input type="radio" id="3-num-3-poster" name="poster[flexibility]" value="3" />
	                                    <label for="3-num-3-poster" class="num">3</label>
	                                    <input type="radio" id="2-num-3-poster" name="poster[flexibility]" value="2" />
	                                    <label for="2-num-3-poster" class="num">2</label>
	                                    <input type="radio" id="1-num-3-poster" name="poster[flexibility]" value="1" />
	                                    <label for="1-num-3-poster" class="num first">1</label>
	                                </div>
	                            </div>
	                            <div class="form-group form-group-flex" id="trust_rating_group">
	                                <div class="ratingstar-label"><?php echo lang('rating_trustworthiness'); ?></div>
	                                <div class="num-rating">
	                                    <input type="radio" id="10-num-4-poster" name="poster[trustworthiness]" value="10" />
	                                    <label for="10-num-4-poster" class="num last">10</label>
	                                    <input type="radio" id="9-num-4-poster" name="poster[trustworthiness]" value="9" />
	                                    <label for="9-num-4-poster" class="num">9</label>
	                                    <input type="radio" id="8-num-4-poster" name="poster[trustworthiness]" value="8" />
	                                    <label for="8-num-4-poster" class="num">8</label>
	                                    <input type="radio" id="7-num-4-poster" name="poster[trustworthiness]" value="7" />
	                                    <label for="7-num-4-poster" class="num">7</label>
	                                    <input type="radio" id="6-num-4-poster" name="poster[trustworthiness]" value="6" />
	                                    <label for="6-num-4-poster" class="num">6</label>
	                                    <input type="radio" id="5-num-4-poster" name="poster[trustworthiness]" value="5" />
	                                    <label for="5-num-4-poster" class="num">5</label>
	                                    <input type="radio" id="4-num-4-poster" name="poster[trustworthiness]" value="4" />
	                                    <label for="4-num-4-poster" class="num">4</label>
	                                    <input type="radio" id="3-num-4-poster" name="poster[trustworthiness]" value="3" />
	                                    <label for="3-num-4-poster" class="num">3</label>
	                                    <input type="radio" id="2-num-4-poster" name="poster[trustworthiness]" value="2" />
	                                    <label for="2-num-4-poster" class="num">2</label>
	                                    <input type="radio" id="1-num-4-poster" name="poster[trustworthiness]" value="1" />
	                                    <label for="1-num-4-poster" class="num first">1</label>
	                                </div>
	                            </div>

	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
	                    <span class="btn-label"><?php echo lang('rating_cancel'); ?></span>
	                    <span class="btn-icon">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                                <line x1="18" y1="6" x2="6" y2="18"></line>
	                                <line x1="6" y1="6" x2="18" y2="18"></line>
	                            </svg>
	                        </span>
	                </button>
	                <button type="submit" class="btn-icon-full btn-confirm btn-icon-disabled" disabled>
	                    <span class="btn-label"><?php echo lang('rating_submit'); ?></span>
	                    <span class="btn-icon">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                                <polyline points="20 6 9 17 4 12"></polyline>
	                            </svg>
	                        </span>
	                </button>
	            </div>
	        </form>
        </div>
    </div>
</div>
<!-- /.modal -->