<?php
    /*$status = $task['status'] === 'completed' ? $task['status'] :
              (($task['status'] === 'published' && is_null($task['assigned'])) ?
              'pending' : 'active');*/
?>
<?php $user = ($section === 'created') ? $task['talent'] : $task['user'] ?>
<div class="tbl-group-calendar-bar-row-wrapper <?php echo $task['status'] ?>-status <?php if( $task['has_new_updates'] ) echo 'active-row' ?>">
    <div class="tbl-group-calendar-bar-flex">
        <div class="tbl-group-calendar-bar-row row-1">
            <button class="btn btn-link btn-arrow-bar collapsed" id="settings-button" aria-expanded="false"><i class="tim-icons icon-minimal-up"></i></button>
            <span class="tbl-group-calendar-bar-title"><?php echo $task['title'] ?></span>
        </div>
        <div class="tbl-group-calendar-bar-row row-2">
            <?php echo 'RM' . number_format($task['budget'], 2);?>
        </div>
        <div class="tbl-group-calendar-bar-row row-3">
            <?php echo isset($task['hours']) ? ($task['type'] == 'lump-sum' ? '-' : $task['hours'] .' '. ((int)$task['hours'] === 1 ? lang('task_hour') : lang('task_hours'))) : lang('task_details_lump_sum_task'); ?>
        </div>
        <div class="tbl-group-calendar-bar-row row-4">
            <div class="tbl-group-calendar-photo">
                <img src="<?php echo imgCrop($task['user']['photo'], 35, 35, 'assets/img/default-avatar.png'); ?>" alt="Profile Photo">
            </div>
	        <div class="draft-more-opt dropdown">
		        <a href="#" class="dropdown-toggle gtm-tc-calendar-triple-dot" data-toggle="dropdown" aria-expanded="false">
                    <span class="draft-more-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round">
                            <circle cx="12" cy="12" r="1"></circle>
                            <circle cx="19" cy="12" r="1"></circle>
                            <circle cx="5" cy="12" r="1"></circle>
                        </svg>
                    </span>
		        </a>
                <?php $chat_status = $task['has_chats'] && is_array($task['has_chats']) && in_array($task['user_id'], $task['has_chats']) ? "true" : "false"; ?>
		        <ul class="dropdown-menu dropdown-navbar">
			        <li class="header-dropdown-link-list">
				        <a href="<?php echo url_for("/workspace/task/{$task['slug']}/billing") ?>"
				           class="nav-item dropdown-item gtm-tc-calendar-triple-dot-billing <?php echo ($task['is_accepted'] || $task['is_rejected'] || $task['is_disputed'] || $task['is_marked_completed'] ) ? 'active-list' : '' ?>">
					        <?php echo lang('task_billing_details'); ?>
				        </a>
			        </li>
			        <li class="header-dropdown-link-list">
				        <a href="#" class="nav-item dropdown-item gtm-tc-calendar-triple-dot-chat <?php echo ($task['has_new_chats']) ? 'active-list' : '' ?>"
				           data-user-id="<?php echo $task['user_id'] === $current_user['id'] ? $task['assigned_to'] : $task['user_id'] ?>"
				           data-task-id="<?php echo $task['id'] ?>"
				           data-task-slug="<?php echo $task['slug'] ?>"
				           data-chat-enabled="<?php echo ($section === 'created') ? 'true' : $chat_status ?>"
				           data-toggle="modal" data-target="#modal_chat"><?php echo lang('task_chat'); ?></a>
			        </li>
			        <?php if( $section === 'awarded' && $task['status'] === 'in-progress' ): ?>
			        <li class="header-dropdown-link-list">
				        <a href="<?php echo url_for("/workspace/task/{$task['slug']}/billing") ?>"
				           class="nav-item dropdown-item gtm-tc-calendar-triple-dot-mark-complete"><?php echo lang('task_mark_as_complete'); ?></a>
			        </li>
			        <?php endif ?>
		        </ul>
	        </div>
        </div>
    </div>
    <div class="collapse multi-collapse" id="inpro_left_applied_<?php echo $counter ?>">
        <div class="taskcentre-details taskcentre-details-applied">
            <div class="taskcentre-details-row row-taskdesc">
                <label class="taskcentre-details-lbl lbl-taskdesc"><?php echo lang('task_description'); ?></label>
                <div class="taskcentre-details-val val-taskdesc">
                    <p class="taskcentre-details-para"><?php echo $task['description']; ?></p>
                </div>
	            <div class="taskcentre-details-viewmore">
		            <a href="<?php echo url_for("/workspace/task/{$task['slug']}/details"); ?>"
		               class="viewmore-taskdetails-link gtm-tc-view-more-btn <?php echo $task['has_new_comments']  ? 'active' : '' ?>">
                        <span class="link-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="arcs"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                        </span>
			            <span class="link-lbl"><?php echo lang('task_view_more'); ?></span>
		            </a>
		            <div class="task-row-taskid">
			            <span class="task-row-taskid-lbl"><?php echo lang('task_id'); ?>:</span>
			            <span class="task-row-taskid-val"><?php echo $task['number']; ?></span>
		            </div>
	            </div>
            </div>
	        <div class="taskcentre-details-row row-taskcomp">
	            <div class="taskcentre-details-row row-downloadfiles">
	                <label class="taskcentre-details-lbl lbl-downloadfiles"><?php echo lang('task_attachment'); ?></label>
	                <div class="taskcentre-details-val val-downloadfiles">
	                    <?php if( !empty($attachments[$task['id']]) ): ?>
						<?php $count = 1; ?>
	                        <?php foreach($attachments[$task['id']] as $attachment): ?>
	                            <?php
	                            $file_name = explode('/', trim($attachment['name'], '/'));
	                            $file_name = end($file_name);
	                            if(strlen($file_name) > 30){
	                                $first = substr($file_name, 0, 14);
	                                $end = substr($file_name, -13);
	                                $short_name = "{$first}...{$end}";
	                            }
	                            ?>
	                            <span class="downloadfiles-wrapper">
	                                <a href="<?php echo url_for("workspace/{$task['slug']}/attachment/{$file_name}") ?>"
	                                   title="<?php echo $file_name; ?>" class="downloadfiles-link gtm-tc-download-attachment">
	                                    <?php //echo $short_name ?? $file_name; ?>
	                                    <?php echo $count; ?>
	                                </a>
	                            </span>
	                            <?php $file_name = $short_name = null; ?>
								<?php $count += 1; ?>
	                        <?php endforeach; ?>
	                    <?php endif; ?>
	                </div>
	            </div>

	            <div class="taskcentre-details-row row-dateapplied">
	                <label class="taskcentre-details-lbl lbl-dateapplied"><?php echo lang('task_date_applied'); ?></label>
	                <div class="taskcentre-details-val val-dateapplied">
	                    <?php echo Carbon\Carbon::parse($task['applied_at'])->format('d M Y'); ?>
	                </div>
	            </div>

	            <div class="taskcentre-details-row row-tasknature">
	                <label class="taskcentre-details-lbl lbl-tasknature"><?php echo lang('task_nature_of_task'); ?></label>
	                <div class="taskcentre-details-val val-tasknature">
	                    <?php echo ($task['type'] === 'by-hour') ? lang('task_by_the_hour') : lang('task_lump_sum'); ?>
	                </div>
	            </div>

	            <div class="taskcentre-details-row row-posterdetail">
                <label class="taskcentre-details-lbl lbl-posterdetail"><?php echo ($section === 'created') ? lang('task_talent_details') : lang('task_poster_detail') ?></label>
                <div class="taskcentre-details-val val-posterdetail">
	                <?php $poster = ($task['user']['type'] === '1' && $section !== 'created') ? 'company' : 'talent'; ?>
                    <a href="<?php echo url_for("/{$poster}/{$user['number']}")?>" class="posterdetail-link gtm-tc-poster-detail">
                        <div class="tbl-group-calendar-photo">
                            <img src="<?php echo imgCrop($user['photo'], 35, 35, 'assets/img/default-avatar.png') ?>"
                                 alt="Profile Photo">
                        </div>
                        <span class="posterdetail-name">
	                        <?php echo ($task['user']['type'] === '1' && $section !== 'created') ? $task['user']['company']['name'] : "{$user['firstname']} {$user['lastname']}"; ?>
                        </span>
                    </a>
                </div>
            </div>
	        </div>
        </div>
    </div>
</div>