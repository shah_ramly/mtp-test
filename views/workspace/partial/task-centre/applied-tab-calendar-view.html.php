<?php
$current_month = $dateObject->month;
$date = $dateObject->copy()->startOfYear();
?>
<div class="tab-pane tc-InProgress tc-InProgress-Month fade <?php echo $section === 'applied' ? 'show active' : '' ?>"
     id="tcInProgressApplied" role="tabpanel" aria-labelledby="tcInProgressApplied-tab">
    <div class="row">
        <div id="colLeftCard" class="col-sm-6 col-left-card-flex">
            <div class="tbl-body-calendar">
                <div class="tbl-group-calendar-row tbl-group-calendar-awarded-row tbl-group-calendar-active-row">
                    <div class="tbl-group-calendar-title"><?php echo lang('task_centre_active_task') ?></div>
                    <div class="tbl-group-calendar-bar-wrapper">
                        <?php if(!empty($applied_active)): ?>
                            <?php if(!isset($counter)) $counter = 1; ?>
                            <?php foreach($applied_active as $task): ?>
                                <?php echo partial('/workspace/partial/task-centre/applied-task-calendar-view.html.php', compact('task', 'counter')) ?>
                                <?php $counter++ ?>
                            <?php endforeach ?>
                        <?php else: ?>
                            <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
                                <div class="tbl-group-calendar-bar-flex">
                                    <div class="tbl-group-calendar-bar-row row-1">
                                        <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_applied_task') ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>

                    </div>
                </div>
                <div class="tbl-group-calendar-row tbl-group-calendar-created-row tbl-group-calendar-inactive-row">
                    <div class="tbl-group-calendar-title"><?php echo lang('task_centre_inactive_task') ?></div>
                    <div class="tbl-group-calendar-bar-wrapper">
                        <?php if(!empty($applied_inactive)): ?>
                            <?php if(!isset($counter)) $counter = 1; ?>
                            <?php foreach($applied_inactive as $task): ?>
                                <?php echo partial('/workspace/partial/task-centre/applied-task-calendar-view.html.php', compact('task', 'counter')); ?>
                                <?php $counter++ ?>
                            <?php endforeach ?>
                        <?php else: ?>
                            <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
                                <div class="tbl-group-calendar-bar-flex">
                                    <div class="tbl-group-calendar-bar-row row-1">
                                        <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_applied_task') ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="colRightCard" class="col-sm-6 col-right-card-flex card-calendar-month">
	        <?php $counter = ! isset($counter) ? 1 : $counter ?>
            <?php if( $calendar_view === 'month' ): ?>
                <?php echo partial('/workspace/partial/task-centre/calendar/applied-month-calendar.html.php', compact('applied_active', 'applied_inactive', 'date', 'counter', 'current_month', 'dateObject')) ?>
            <?php elseif ($calendar_view === 'week') : ?>
                <?php echo partial('/workspace/partial/task-centre/calendar/applied-week-calendar.html.php', compact('applied_active', 'applied_inactive', 'date', 'counter', 'current_month', 'dateObject')) ?>
            <?php elseif ($calendar_view === 'day'): ?>
                <?php echo partial('/workspace/partial/task-centre/calendar/applied-day-calendar.html.php', compact('applied_active', 'applied_inactive', 'date', 'counter', 'current_month', 'dateObject')) ?>
            <?php elseif ($calendar_view === 'hour') : ?>
                <?php echo partial('/workspace/partial/task-centre/calendar/applied-hour-calendar.html.php', compact('applied_active', 'applied_inactive', 'date', 'counter', 'current_month', 'dateObject')) ?>
            <?php endif ?>
        </div>
    </div>
</div>

<?php if( isset($applied_start_day) ): ?>
	<script>
        window.addEventListener('load', function() {
            setTimeout(function(){
                document.querySelector('#tcInProgressApplied .dragscroll.test').scrollTo({
                    top:0, left:<?php echo ($applied_start_day <= 0) ? 0 : $applied_start_day * ($calendar_view === 'hour' ? 70 : 100) ?>, behavior:'smooth'
                });
            }, 500);
        });
	</script>
<?php endif ?>