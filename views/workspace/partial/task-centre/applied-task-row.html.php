<?php
$status_map = [
    'appointed'   => ['class' => '1', 'id' => '1'],
    'confirmed'   => ['class' => '2', 'id' => '2'],
    'waiting'     => ['class' => '3', 'id' => '3a'],
    'in-progress' => ['class' => '3', 'id' => '3b'],
    'mark-completed' => ['class' => '3', 'id' => '3c'],
    'completed'   => ['class' => '4', 'id' => '4'],
    'payment'     => ['class' => '5', 'id' => '5a'],
    'dispute'    => ['class' => '5', 'id' => '5b'],
    'rate'        => ['class' => '5', 'id' => '5a'],
    'closed'      => ['class' => '7', 'id' => '7-1'],
    'declined'    => ['class' => '7', 'id' => '7-2'],
    'cancelled'   => ['class' => '7', 'id' => '7-2'],
    'unfulfilled' => ['class' => '7', 'id' => '7-3'],
    'disputed'    => ['class' => '7', 'id' => '7-4'],
];

$applicant_task_status = $task['apply_status'];
$action = $task['actions'];//array_values(array_filter($milestone_actions, function($action)use($task, $current_user){ return $action['for_id'] === $current_user['id'] && $task['id'] === $action['task_id']; }));
$required_action = array_column($action, 'required_action');

if( $task['apply_status'] === 'in-progress' && $task['poster_status'] !== 'completed' && (\Carbon\Carbon::parse($task['complete_by']))->isPast() ){
    $milestone_status = '3c';
}elseif($task['apply_status'] === 'disputed' && $task['poster_status'] === 'completed'){
	$milestone_status = '7';
}elseif($task['apply_status'] === 'in-progress' && $task['poster_status'] === 'completed'){
	$milestone_status = $status_map[$task['poster_status']]['id'];
}elseif(isset($task['seeker_status']) && $task['seeker_status'] === 'rate'){
	$milestone_status = $status_map[$task['seeker_status']]['id'];
}elseif(isset($required_action[0]) && $required_action[0] === 'rate'){
	$milestone_status = '5a';
}elseif(isset($task['seeker_status']) && $task['seeker_status'] === 'closed'){
	$milestone_status = $status_map[$task['seeker_status']]['id'];
}elseif($task['poster_status'] && $task['poster_status'] === $task['seeker_status']){
    $milestone_status = $status_map[$task['poster_status']]['id'] ?? $task['apply_status'];
}elseif( isset($task['poster_status']) && ($task['poster_status'] === 'waiting' || $task['poster_status'] === 'confirmed') ){
	$milestone_status = $status_map[$task['poster_status']]['id'];
}elseif($applicant_task_status !== 'applied'){
	$milestone_status = $status_map[$applicant_task_status]['id'];
}

?>
<div id="<?php echo strtolower($task['id']) ?>" class="tbl-group-calendar-bar-row-wrapper applied-task-row
<?php echo str_replace('-', '', $task['apply_status']) ?>-task-row <?php echo $task['apply_status'] ?>-task-row- <?php echo $task['has_new_updates'] ? 'active-row' : ''?>">
    <div class="tbl-group-calendar-bar-flex">
        <div class="tbl-group-calendar-bar-row row-1">
            <button class="btn btn-link btn-arrow-bar collapsed" id="settings-button" data-toggle="collapse" aria-expanded="true"><i class="tim-icons icon-minimal-up"></i></button>
            <span class="tbl-group-calendar-bar-title"><?php echo $task['title'] ?></span>
        </div>
	    <div class="tbl-group-calendar-bar-row row-2">
		    <span class="tbl-group-calendar-bar-date"><?php echo date($cms->settings()['date_format'], strtotime($task['start_by'])) .' - '. date($cms->settings()['date_format'], strtotime($task['complete_by'])); ?></span>
		    <span class="clock-icon">
                <span class="bi-tooltip" data-toggle="tooltip" data-placement="auto"
                      data-template="<div class='tooltip tooltip-fancy tooltip-tc-date' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>"
                      <?php if( \Carbon\Carbon::parse($task['start_by'])->isFuture() ): ?>
                      data-title="<?php echo 'Starts in: ' . dateRangeToDays(\Carbon\Carbon::now()->toDateTimeString(), $task['start_by'], true) ?>" data-original-title="" title="">
	                <?php elseif( in_array($task['status'], ['published', 'in-progress', 'marked-completed']) ): ?>
	                    <?php if( \Carbon\Carbon::parse($task['complete_by'])->isFuture() ): ?>
                      data-title="<?php echo dateRangeToDays(Carbon\Carbon::now()->toDateTimeString(), $task['complete_by'], true) . ' to deadline' ?>" data-original-title="" title="">
	                    <?php else: ?>
                      data-title="<?php echo 'Deadline has passed ' . dateRangeToDays(\Carbon\Carbon::now()->toDateTimeString(), $task['complete_by'], true) . ' ago' ?>" data-original-title="" title="">
	                    <?php endif ?>
	                <?php else: ?>
		              data-title="<?php echo 'This task has been closed ' . dateRangeToDays(\Carbon\Carbon::now()->toDateTimeString(), $task['complete_by'], true) . ' ago' ?>" data-original-title="" title="">
	                <?php endif ?>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-hourglass" viewBox="0 0 16 16">
                        <path d="M2 1.5a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-1v1a4.5 4.5 0 0 1-2.557 4.06c-.29.139-.443.377-.443.59v.7c0 .213.154.451.443.59A4.5 4.5 0 0 1 12.5 13v1h1a.5.5 0 0 1 0 1h-11a.5.5 0 1 1 0-1h1v-1a4.5 4.5 0 0 1 2.557-4.06c.29-.139.443-.377.443-.59v-.7c0-.213-.154-.451-.443-.59A4.5 4.5 0 0 1 3.5 3V2h-1a.5.5 0 0 1-.5-.5zm2.5.5v1a3.5 3.5 0 0 0 1.989 3.158c.533.256 1.011.791 1.011 1.491v.702c0 .7-.478 1.235-1.011 1.491A3.5 3.5 0 0 0 4.5 13v1h7v-1a3.5 3.5 0 0 0-1.989-3.158C8.978 9.586 8.5 9.052 8.5 8.351v-.702c0-.7.478-1.235 1.011-1.491A3.5 3.5 0 0 0 11.5 3V2h-7z"></path>
                    </svg>
                </span>
            </span>
	    </div>
        <div class="tbl-group-calendar-bar-row row-3">
	        <span class="tbl-group-calendar-bar-totalsum"><?php echo "RM" . number_format($task['budget'], 2) ?></span>
        </div>
        <div class="tbl-group-calendar-bar-row row-4">
	        <?php $main_status = in_array($task['apply_status'], ['payment', 'rate', 'disputed']) ? (($task['seeker_status'] === 'disputed' || $task['seeker_status'] === 'rate') && ($task['poster_status'] === 'disputed' || $task['poster_status'] === 'rate') ? 'disputed' : 'completed') : $task['apply_status'] // ( $task['seeker_status'] === 'disputed' && $task['was_rated'] ? 'disputed' : 'completed') ?>
	        <span class="task-centre-status"><?php echo ucwords(str_replace('-', ' ', $main_status)) ?></span>
	        <!--<span class="task-centre-status"><?php /*echo lang('task_' . str_replace('-', '_', ($task['apply_status'] === 'payment' ? 'completed' : $task['apply_status']))); */?></span>-->
	        <ul class="tbl-applicant-list">
		        <li>
			        <a href="#" class="avatar-link">
				        <div class="tbl-group-calendar-photo">
					        <img src="<?php echo imgCrop($task['user']['photo'], 35, 35, 'assets/img/default-avatar.png') ?>" alt="Profile Photo">
				        </div>
			        </a>
		        </li>
	        </ul>
	        <div class="draft-more-opt dropdown <?php echo $task['apply_status'] !== 'applied' ? 'disabled' : '' ?>">
		        <a href="#" class="dropdown-toggle gtm-tc-applied-row-triple-dot" data-toggle="dropdown" aria-expanded="false">
                    <span class="draft-more-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                            <circle cx="12" cy="12" r="1"></circle>
                            <circle cx="12" cy="5" r="1"></circle>
                            <circle cx="12" cy="19" r="1"></circle>
                        </svg>
                    </span>
		        </a>
		        <ul class="dropdown-menu dropdown-navbar">
			        <li class="header-dropdown-link-list">
				        <a href="#" data-toggle="modal" data-target="#modal_tc_retract"
				           class="nav-item dropdown-item gtm-tc-applied-row-triple-dot-retract"
				           data-task-id="<?php echo $task['id'] ?>"
				           data-user-id="<?php echo $current_user['id'] ?>"
				        ><?php echo lang('task_centre_retract_application'); ?></a>
			        </li>
		        </ul>
	        </div>
        </div>
    </div>
    <div class="multi-collapse collapse">
        <div class="taskcentre-details taskcentre-details-applied">
	        <div class="taskcentre-details-expanded-container">
		        <div class="tbl-group-calendar-bar-row row-1">
                    <div class="taskcentre-details-row row-taskdesc">
		                <label class="taskcentre-details-lbl lbl-taskdesc"><?php echo lang('task_description'); ?></label>
		                <div class="taskcentre-details-val val-taskdesc">
		                    <p class="taskcentre-details-para"
		                    ><?php echo strlen(rip_tags($task['description'])) > 510 ? substr(htmlspecialchars(rip_tags($task['description'])), 0, 510) . ' ...' : htmlspecialchars(rip_tags($task['description'])); ?></p>
		                </div>
	                </div>
			        <div class="taskcentre-details-row row-taskcat">
				        <span class="taskcentre-details-val val-taskcat"><?php echo $task['category_name'] ?></span>
				        <span class="taskcentre-details-val val-taskcatsymbol">&#62;</span>
				        <span class="taskcentre-details-val val-tasksubcat"><?php echo $task['subcategory_name'] ?></span>
			        </div>
			        <div class="taskcentre-details-row row-taskloc">
				        <div class="task-details-title-components">
					        <?php if($task['location'] !== 'remotely'): ?>
					        <span class="task-details-location-pills"><?php echo lang('task_centre_on_site'); ?></span>
					        <span class="task-details-location">
                                <span class="task-details-location-icon">
	                                <svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                    </svg>
                                </span>
                                <span class="task-details-location-val">
	                                <a target="_blank" class="gtm-tc-googlemaps-external-link"
	                                   href="https://www.google.com/maps/search/<?php echo urlencode($task['location']) ?>"
	                                ><?php echo $task['location'] ?></a>
                                </span>
                            </span>
					        <?php else: ?>
					        <span class="task-details-location-pills"><?php echo lang('task_centre_remote'); ?></span>
					        <?php endif ?>
				        </div>
			        </div>
		        </div>
		        <div class="tbl-group-calendar-bar-row row-2">
		            <div class="taskcentre-details-row row-downloadfiles">
		                <label class="taskcentre-details-lbl lbl-downloadfiles"><?php echo lang('task_attachment'); ?></label>
		                <div class="taskcentre-details-val val-downloadfiles">
		                    <?php if( !empty($attachments[$task['id']]) ): ?>
							<?php $count = 1; ?>
		                        <?php foreach($attachments[$task['id']] as $attachment): ?>
		                            <?php
		                            $file_name = explode('/', trim($attachment['name'], '/'));
		                            $file_name = end($file_name);
		                            if(strlen($file_name) > 30){
		                                $first = substr($file_name, 0, 14);
		                                $end = substr($file_name, -13);
		                                $short_name = "{$first}...{$end}";
		                            }
		                            ?>
					                <span class="downloadfiles-wrapper">
		                                <a href="<?php echo url_for("workspace/{$task['slug']}/attachment/{$file_name}") ?>"
		                                   title="<?php echo $file_name; ?>" class="downloadfiles-link gtm-tc-download-file">
		                                    <?php echo $count; ?>
		                                </a>
		                            </span>
		                            <?php $file_name = $short_name = null; ?>
									<?php $count += 1; ?>
		                        <?php endforeach; ?>
		                    <?php endif; ?>
		                </div>
		            </div>
		        </div>
		        <div class="tbl-group-calendar-bar-row row-3">
			        <div class="taskcentre-details-row row-taskcostbreakdown">
				        <div class="taskcentre-details-row row-taskhourlyrate">
					        <label class="taskcentre-details-lbl lbl-taskhourlyrate"><?php echo lang('task_centre_cost_breakdown'); ?></label>
					        <div class="taskcentre-details-val val-taskbythehour">RM<?php echo $task['hours'] > 0 ? number_format($task['budget']/$task['hours'], 2) . '/hr' : number_format($task['budget'], 2) ?></div>
					        <?php if( $task['type'] === 'by-hour' ): ?>
					        <div class="taskcentre-details-val val-taskhourlyrate">
						        <span class="subval-taskcentre val-taskhourlyrate">x <?php echo $task['hours'] ?></span>
						        <span class="subicon-taskcentre clock-icon">
                                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="auto"
                                          data-template="<div class='tooltip tooltip-fancy tooltip-tc-date' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>"
                                          data-title="<?php echo $task['hours'] .' '. ($task['hours'] == 1 ? lang('task_hour') : lang('task_hours')) ?>" data-original-title="" title="">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-clock" viewBox="0 0 16 16">
                                            <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"></path>
                                            <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"></path>
                                        </svg>
                                    </span>
                                </span>
					        </div>
					        <?php endif ?>
				        </div>
			        </div>
			        <div class="taskcentre-details-row row-taskqna">
				        <label class="taskcentre-details-lbl lbl-taskqna"><?php echo lang('task_centre_question_answer'); ?>(<?php echo $task['comments_count'] ?>)</label>
				        <div class="taskcentre-details-val val-taskqna"><?php echo lang('task_centre_need_clarify'); ?><br>
					        <a href="<?php echo url_for("workspace/task/{$task['slug']}/details") ?>" class="postqna-link gtm-tc-applied-postqna"><?php echo lang('task_centre_post_a_question'); ?></a> <?php echo lang('task_centre_on_qa_for_this_task'); ?>.
				        </div>
			        </div>
		        </div>
		        <div class="tbl-group-calendar-bar-row row-4">
			        <div class="taskcentre-details-row row-posterdetail">
				        <div class="taskcentre-details-row row-taskaction">
					        <form method="get" action="<?php echo url_for("/workspace/task/{$task['slug']}/details"); ?>">
						        <button type="submit" class="btn-icon-full btn-viewmore gtm-tc-view-more-btn" style="margin-bottom:10px">
							        <span class="btn-label"><?php echo lang('task_centre_view_more'); ?></span>
							        <span class="btn-icon">
	                                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                                    </svg>
	                                </span>
						        </button>
					        </form>
                            <?php $chat_status = $task['has_chats'] ? "true" : "false"; ?>
					        <button type="button" class="btn-icon-full btn-chat gtm-tc-chat-btn <?php echo !empty($task['has_new_chats']) && in_array($task['user_id'], $task['has_new_chats']) ? 'active' : '' ?>"
					                data-toggle="modal"
					                data-target="#modal_chat"
					                data-user-id="<?php echo $task['user_id'] ?>"
					                data-task-id="<?php echo $task['id'] ?>"
					                data-task-slug="<?php echo $task['slug'] ?>"
					                data-task-closed="<?php echo (in_array($task['apply_status'], ['cancelled', 'closed', 'disputed']) && \Carbon\Carbon::parse($task['updated_at'])->addDays(5)->isBefore(\Carbon\Carbon::now())) ? 'true' : 'false' ?>"
					                data-task-closed-date="<?php echo in_array($task['apply_status'], ['cancelled', 'closed', 'disputed']) ? \Carbon\Carbon::parse($task['updated_at'])->addDays(5)->format("d F Y - h.ia") : 'false' ?>"
					                data-chat-enabled="true<?php //echo $chat_status ?>"
                                <?php if( in_array($task['apply_status'], ['cancelled', 'lost']) || $chat_status === "false" ): ?>
							        disabled="disabled"
                                <?php endif ?>
					        >
						        <span class="btn-label"><?php echo lang('task_chat'); ?></span>
						        <span class="btn-icon">
                                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                    </svg>
                                </span>
					        </button>
					        <?php if( $task['apply_status'] !== 'applied' /*&& ($task['apply_status'] === 'declined' && $task['poster_status'])*/ ): ?>
					        <button type="button" class="btn-icon-full btn-progress gtm-tc-progress-btn <?php echo !empty($action) ? 'active' : '' ?>" data-toggle="modal" data-target="#modal_milestone_seeker"
					                data-id="seeker-progress-<?php echo $milestone_status ?>"
					                data-task-id="<?php echo $task['id'] ?>"
					                data-user-id="<?php echo $current_user['id'] ?>"
					                data-task-slug="<?php echo $task['slug'] ?>"
					                data-name="<?php echo isset($task['user']['company']) ? $task['user']['company']['name'] : "{$task['user']['firstname']} {$task['user']['lastname']}" ?>"
					                data-start-date="<?php echo Carbon\Carbon::parse($task['start_by'])->formatLocalized('%d %B %Y') ?>"
					                data-end-date="<?php echo Carbon\Carbon::parse($task['complete_by'])->formatLocalized('%d %B %Y') ?>"
                                <?php if( in_array($applicant_task_status, ['cancelled', 'declined', 'rejected', 'disputed']) ): ?>
							        data-task-state="<?php echo $applicant_task_status ?>"
                                <?php endif ?>
					        >
						        <span class="btn-label"><?php echo lang('modal_progress'); ?></span>
						        <span class="btn-icon">
                                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                    </svg>
                                </span>
					        </button>
					        <?php else: ?>
					        <button type="button" class="btn-icon-full btn-progress" disabled="disabled">
						        <span class="btn-label"><?php echo lang('modal_progress'); ?></span>
						        <span class="btn-icon">
                                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                    </svg>
                                </span>
					        </button>
					        <?php endif ?>
				        </div>
			        </div>
			        <div class="taskcentre-details-viewmore">
				        <div class="task-row-taskid">
					        <span class="task-row-taskid-lbl"><?php echo lang('task_id'); ?>:</span>
					        <span class="task-row-taskid-val"><?php echo $task['number'] ?></span>
				        </div>
			        </div>
		        </div>
	        </div>
        </div>
    </div>
</div>