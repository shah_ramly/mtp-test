<?php
if( array_key_exists($task['id'], $selected_applicants) ){
	$selection = array_filter($selected_applicants[$task['id']], function ($applicant){
		return !in_array($applicant['status'], ['appointed', 'confirmed', 'cancelled', 'declined']);
	});
	$selected_count = count($selection);
}else{
	$selected_count = 0;
}
$task_applicants = array_values(array_filter($tasks['applicants'], function($applicant) use($task){ return $applicant['task_id'] === $task['id']; }));
$actions = array_filter($milestone_actions, function($action) use($task, $current_user){
	return $action['task_id'] === $task['id'] && $action['for_id'] === $current_user['id'] && $task['status'] !== 'closed';
});

$payments = array_filter($payments, function($payment) use($task){
	return $payment['task_id'] === $task['id'] && in_array($task['status'], ['published', 'in-progress']) && $payment['status'] != '1';
});

?>
<?php $task_status = $task['status'] === 'marked-completed' ? 'in-progress' : $task['status'];//(!is_null($task['assigned']) && !is_null($task['assigned_to']) && in_array($task['status'], ['in-progress', 'marked-completed'])) ? 'in-progress' : $task['status']; ?>
<div id="<?php echo strtolower($task['id']) ?>"
     class="tbl-group-calendar-bar-row-wrapper posted-task-row <?php echo $task_status ?>-task-row <?php echo str_replace('-', '', $task_status) ?>-task-row
<?php echo (array_key_exists($task['id'], $tasks['notifications']) && in_array('true', array_values($tasks['notifications'][$task['id']]))) || !empty($actions) || !empty($payments) || $task['has_new_updates'] ? 'active-row' : ''; ?>">
	<div class="tbl-group-calendar-bar-flex">
		<div class="tbl-group-calendar-bar-row row-1">
			<button class="btn btn-link btn-arrow-bar collapsed" id="settings-button" data-toggle="collapse" aria-expanded="true"><i class="tim-icons icon-minimal-up"></i></button>
			<span class="tbl-group-calendar-bar-title"><?php echo $task['title'] ?></span>
		</div>
		<?php //if((int)$task['applicants'] === 0 || !in_array($task_status, ['published', 'in-progress']) ): ?>
		<div class="tbl-group-calendar-bar-row row-2">
			<span class="tbl-group-calendar-bar-date"><?php echo date($cms->settings()['date_format'], strtotime($task['start_by'])) .' - '. date($cms->settings()['date_format'], strtotime($task['complete_by'])); ?></span>
			<span class="clock-icon">
                <span class="bi-tooltip" data-toggle="tooltip" data-placement="auto"
                      data-template="<div class='tooltip tooltip-fancy tooltip-tc-date' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>"
                      <?php if( Carbon\Carbon::parse($task['start_by'])->isFuture() ): ?>
                      data-title="<?php echo lang('milestone_starts_in').': '. dateRangeToDays(Carbon\Carbon::now()->toDateTimeString(), $task['start_by'], true) ?>" data-original-title="" title="">
	                <?php elseif( in_array($task['status'], ['published', 'in-progress', 'marked-completed']) ): ?>
                        <?php if( Carbon\Carbon::parse($task['complete_by'])->isFuture() ): ?>
			                data-title="<?php echo dateRangeToDays(Carbon\Carbon::now()->toDateTimeString(), $task['complete_by'], true) . ' to deadline' ?>" data-original-title="" title="">
                        <?php else: ?>
			                data-title="<?php echo lang('milestone_deadline_has_passed')  . dateRangeToDays(Carbon\Carbon::now()->toDateTimeString(), $task['complete_by'], true) . ' ago' ?>" data-original-title="" title="">
                        <?php endif ?>
                    <?php elseif( in_array($task['status'], ['draft']) ): ?>
                        <?php if( Carbon\Carbon::parse($task['start_by'])->isFuture() ): ?>
			                data-title="<?php echo lang('milestone_starts_in').': ' . dateRangeToDays(Carbon\Carbon::now()->toDateTimeString(), $task['start_by'], true) ?>" data-original-title="" title="">
                        <?php else: ?>
			                data-title="<?php echo lang('milestone_this_task_has_been_expired_please_change') ?>." data-original-title="" title="">
                        <?php endif ?>
                    <?php else: ?>
		                data-title="<?php echo lang('milestone_this_task_has_been_closed') . dateRangeToDays(Carbon\Carbon::now()->toDateTimeString(), $task['complete_by'], true) . ' ago' ?>" data-original-title="" title="">
                    <?php endif ?>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-hourglass" viewBox="0 0 16 16">
                        <path d="M2 1.5a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-1v1a4.5 4.5 0 0 1-2.557 4.06c-.29.139-.443.377-.443.59v.7c0 .213.154.451.443.59A4.5 4.5 0 0 1 12.5 13v1h1a.5.5 0 0 1 0 1h-11a.5.5 0 1 1 0-1h1v-1a4.5 4.5 0 0 1 2.557-4.06c.29-.139.443-.377.443-.59v-.7c0-.213-.154-.451-.443-.59A4.5 4.5 0 0 1 3.5 3V2h-1a.5.5 0 0 1-.5-.5zm2.5.5v1a3.5 3.5 0 0 0 1.989 3.158c.533.256 1.011.791 1.011 1.491v.702c0 .7-.478 1.235-1.011 1.491A3.5 3.5 0 0 0 4.5 13v1h7v-1a3.5 3.5 0 0 0-1.989-3.158C8.978 9.586 8.5 9.052 8.5 8.351v-.702c0-.7.478-1.235 1.011-1.491A3.5 3.5 0 0 0 11.5 3V2h-7z"></path>
                    </svg>
                </span>
            </span>
		</div>
		<div class="tbl-group-calendar-bar-row row-3">
			<span class="tbl-group-calendar-bar-totalsum">
				<?php echo "RM" . number_format($selected_count ? (float)$task['budget']*$selected_count : $task['budget'], 2) ?>
			</span>
		</div>

		<div class="tbl-group-calendar-bar-row row-4">
			<span class="task-centre-status"><?php echo ucwords(str_replace('-', ' ', $task_status)); ?></span>
			<!--<span class="task-centre-status"><?php /*echo lang('task_' . str_replace('-', '_', $task_status)); */?></span>-->
            <?php if('draft' === $task['status']): ?>
	            <ul class="tbl-applicant-list">
		            <li>
			            <div class="tbl-group-calendar-photo">
				            <img src="<?php echo imgCrop($task['user']['photo'], 35, 35, 'assets/img/default-avatar.png') ?>" alt="Profile Photo">
			            </div>
		            </li>
	            </ul>
	            <div class="draft-more-opt dropdown">
		            <a href="#" class="dropdown-toggle gtm-tc-calendar-triple-dot" data-toggle="dropdown" aria-expanded="false">
                        <span class="draft-more-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                            <circle cx="12" cy="12" r="1"></circle>
                            <circle cx="12" cy="5" r="1"></circle>
                            <circle cx="12" cy="19" r="1"></circle>
                        </svg>
                        </span>
		            </a>
		            <ul class="dropdown-menu dropdown-navbar">
			            <li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item edit-task gtm-tc-calendar-triple-dot-edit" data-task-id="<?php echo $task['slug'] ?>" data-backdrop="static" data-toggle="modal" data-target="#post_task_od"><?php echo lang('task_edit'); ?></a></li>
			            <li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item delete-task gtm-tc-calendar-triple-dot-delete" data-task-id="<?php echo $task['id'] ?>" data-toggle="modal" data-target="#modal_confirm_delete_od"><?php echo lang('task_delete'); ?></a></li>
		            </ul>
	            </div>
            <?php endif; ?>
			<?php //if(in_array($task_status, ['published', 'in-progress'])): ?>
			<?php if(!in_array($task_status, ['draft'])): ?>
				<ul class="tbl-applicant-list">
					<?php if( count($task_applicants) > 0 ): ?>
						<?php $total_task_applicants = count($task_applicants); ?>
						<?php if($total_task_applicants > 3): ?>
                            <?php foreach ($task_applicants as $key => $selected_applicant): ?>
							<li>
								<div class="tbl-group-calendar-photo">
									<img src="<?php echo imgCrop($selected_applicant['photo'], 35, 35, 'assets/img/default-avatar.png') ?>" alt="Profile Photo">
								</div>
							</li>
							<?php if( $key+1 === 2 ) break; ?>
                            <?php endforeach ?>
							<li>
								<div class="tbl-group-calendar-no">
									<span class="total-applicant-count">+<?php echo ($total_task_applicants - 2) ?></span>
								</div>
							</li>
						<?php else: ?>
							<?php foreach ($task_applicants as $selected_applicant): ?>
							<li>
								<div class="tbl-group-calendar-photo">
									<img src="<?php echo imgCrop($selected_applicant['photo'], 35, 35, 'assets/img/default-avatar.png') ?>" alt="Profile Photo">
								</div>
							</li>
							<?php endforeach ?>
						<?php endif ?>
					<?php else: ?>
					<li>
						<div class="tbl-group-calendar-no">
							<span class="total-applicant-count">0</span>
						</div>
					</li>
					<?php endif ?>
				</ul>
				<div class="draft-more-opt dropdown <?php echo (!in_array($task['status'], ['draft', 'published'])) ? 'disabled' : '' ?>">
					<a href="#" class="dropdown-toggle gtm-tc-calendar-triple-dot" data-toggle="dropdown" aria-expanded="false">
                        <span class="draft-more-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                            <circle cx="12" cy="12" r="1"></circle>
                            <circle cx="12" cy="5" r="1"></circle>
                            <circle cx="12" cy="19" r="1"></circle>
                        </svg>
                        </span>
					</a>
					<ul class="dropdown-menu dropdown-navbar">
						<?php if( in_array($task['status'], ['draft', 'published']) ): ?>
						<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item edit-task gtm-tc-calendar-triple-dot-edit" data-task-id="<?php echo $task['slug'] ?>" data-backdrop="static" data-toggle="modal" data-target="#post_task_od"><?php echo lang('task_edit'); ?></a></li>
						<li class="header-dropdown-link-list"><a href="#" data-task-id="<?php echo $task['id'] ?>" data-toggle="modal" data-target="#modal_confirm_cancel_od" class="nav-item dropdown-item gtm-tc-calendar-triple-dot-cancel"><?php echo lang('task_cancel_task'); ?></a></li>
						<?php endif ?>
					</ul>
				</div>
			<?php endif ?>
		</div>
	</div>
	<div class="multi-collapse collapse" style="">
		<div class="taskcentre-details taskcentre-details-applied">
			<div class="taskcentre-details-expanded-container">
				<div class="tbl-group-calendar-bar-row row-1">
					<div class="taskcentre-details-row row-taskdesc">
						<label class="taskcentre-details-lbl lbl-taskdesc"><?php echo lang('task_description'); ?></label>
						<div class="taskcentre-details-val val-taskdesc">
							<p class="taskcentre-details-para"
							><?php echo strlen(rip_tags($task['description'])) > 510 ? substr(htmlspecialchars(rip_tags($task['description'])), 0, 510) . ' ...' : htmlspecialchars(rip_tags($task['description'])); ?></p>
						</div>
						<div class="taskcentre-details-row row-taskcat">
							<span class="taskcentre-details-val val-taskcat"><?php echo $task['category_name'] ?></span>
							<span class="taskcentre-details-val val-taskcatsymbol">&#62;</span>
							<span class="taskcentre-details-val val-tasksubcat"><?php echo $task['subcategory_name'] ?></span>
						</div>
						<div class="taskcentre-details-row row-taskloc">
							<div class="task-details-title-components">
                                <?php if($task['location'] !== 'remotely'): ?>
									<span class="task-details-location-pills"><?php echo lang('task_centre_on_site'); ?></span>
									<span class="task-details-location">
                                <span class="task-details-location-icon">
	                                <svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                    </svg>
                                </span>
                                <span class="task-details-location-val">
	                                <a target="_blank"
	                                   href="https://www.google.com/maps/search/<?php echo urlencode($task['location']) ?>"
	                                ><?php echo $task['location'] ?></a>
                                </span>
                            </span>
                                <?php else: ?>
									<span class="task-details-location-pills"><?php echo lang('task_centre_remote'); ?></span>
                                <?php endif ?>
							</div>
						</div>
					</div>
				</div>
				<div class="tbl-group-calendar-bar-row row-2">
					<div class="taskcentre-details-row row-downloadfiles">
						<label class="taskcentre-details-lbl lbl-downloadfiles"><?php echo lang('task_attachment'); ?></label>
						<div class="taskcentre-details-val val-downloadfiles">
                            <?php if( !empty($attachments[$task['id']]) ): ?>
                                <?php $count = 1; ?>
                                <?php foreach($attachments[$task['id']] as $attachment): ?>
                                    <?php
                                    $file_name = explode('/', trim($attachment['name'], '/'));
                                    $file_name = end($file_name);
                                    if(strlen($file_name) > 30){
                                        $first = substr($file_name, 0, 14);
                                        $end = substr($file_name, -13);
                                        $short_name = "{$first}...{$end}";
                                    }
                                    ?>
									<span class="downloadfiles-wrapper">
		                                <a href="<?php echo url_for("workspace/{$task['slug']}/attachment/{$file_name}") ?>"
		                                   title="<?php echo $file_name; ?>" class="downloadfiles-link gtm-tc-download-attachment">
		                                    <?php echo $count; ?>
		                                </a>
		                            </span>
                                    <?php $file_name = $short_name = null; ?>
                                    <?php $count += 1; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
						</div>
					</div>
				</div>
				<div class="tbl-group-calendar-bar-row row-3">
					<div class="taskcentre-details-row row-taskcostbreakdown">
						<div class="taskcentre-details-row row-taskhourlyrate">
							<label class="taskcentre-details-lbl lbl-taskhourlyrate">Cost Breakdown</label>
							<div class="taskcentre-details-val val-taskbythehour">RM<?php echo ( (int) $task['hours'] > 0 && (int) $task['budget'] > 0) ? number_format($task['budget']/$task['hours'], 2) . '/hr' : number_format($task['budget'], 2) ?></div>
                            <?php if( $task['type'] === 'by-hour' ): ?>
							<div class="taskcentre-details-val val-taskhourlyrate">
								<span class="subval-taskcentre val-taskhourlyrate">x <?php echo $task['hours'] ?></span>
								<span class="subicon-taskcentre clock-icon">
                                <span class="bi-tooltip" data-toggle="tooltip" data-placement="auto"
                                      data-template="<div class='tooltip tooltip-fancy tooltip-tc-date' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>"
                                      data-title="<?php echo $task['hours'] .' '. ($task['hours'] == 1 ? lang('task_hour') : lang('task_hours')) ?>" data-original-title="" title="">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-clock" viewBox="0 0 16 16">
                                        <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"></path>
                                        <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"></path>
                                    </svg>
                                </span>
                            </span>
							</div>
                            <?php endif ?>
							<div class="taskcentre-details-val val-tasktotalhired">
								<span class="subval-taskcentre val-tasktotalhired">x <?php echo $selected_count ?></span>
								<span class="subicon-taskcentre person-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                        <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
                                    </svg>
                                </span>
							</div>
						</div>
					</div>
					<div class="taskcentre-details-row row-taskqna">
						<label class="taskcentre-details-lbl lbl-taskqna"><?php echo lang('task_centre_question_answer'); ?> (<?php echo $task['comments_count'] ?>)</label>
						<div class="taskcentre-details-val val-taskqna"><?php echo $task['comments_count'] ? "You have {$task['unanswered_comments_count']} unanswered questions" : lang('no_questions_posted') ?>.</div>
					</div>
				</div>
				<div class="tbl-group-calendar-bar-row row-4">
					<div class="taskcentre-details-row row-posterdetail">
						<div class="taskcentre-details-row row-taskaction">
							<form method="get" action="<?php echo url_for("/workspace/task/{$task['slug']}/details"); ?>">
								<button type="submit" class="btn-icon-full btn-viewmore gtm-tc-view-more-btn <?php echo $task['has_new_comments'] > 0 ? 'active' : '' ?>" style="margin-bottom:10px">
									<span class="btn-label"><?php echo lang('task_view_more'); ?></span>
									<span class="btn-icon">
	                                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                                    </svg>
	                                </span>
								</button>
							</form>
						</div>
					</div>
					<div class="taskcentre-details-viewmore">
						<div class="task-row-taskid">
							<span class="task-row-taskid-lbl"><?php echo lang('task_id'); ?>:</span>
							<span class="task-row-taskid-val"><?php echo $task['number'] ?></span>
						</div>
					</div>
				</div>
				<?php //if(((int)$task['applicants'] > 0 && !empty($tasks['applicants'])) && in_array($task_status, ['published', 'in-progress'])): ?>
				<?php if(((int)$task['applicants'] > 0 && !empty($tasks['applicants'])) && !in_array($task_status, ['draft'])): ?>
				<div class="applicants-<?php echo explode('-', $task['id'])[0] ?>" style="width:100%">
				<?php echo partial('/workspace/partial/task-centre/applicants.html.php',
						[
							'applicants' => $tasks['applicants'],
	                        'task'       => $task,
							'count'      => $tasks['applicants_count'][$task['id']],
							'page'       => isset($page) ? (int)$page : 1,
							'actions'    => $actions,
							'payments'    => $payments,
	                    ]
					) ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>