<!-- Modal - Task Planning -->
<div id="modal_task_planning" class="modal modal-task-planning fade" aria-labelledby="modal_custom_widget" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <form method="post" action="<?php echo url_for('/workspace/task/time_planning') ?>">
            <?php echo html_form_token_field(); ?>
            <input type="hidden" name="task_id" id="task-id" value="" />
            <div class="modal-content">
	            <div class="modal-header"><?php echo lang('task_planning_modal_main_title') ?>
		            <span class="bi-tooltip" data-toggle="tooltip" data-placement="top"
		                  title="<?php echo lang('task_planning_modal_main_title_tooltip') ?>">
					    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill"
					         fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					        <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
					    </svg>
					</span>
	            </div>
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="modal-body-container">
                        <p class="modal-para"><?php echo lang('create_up_to_5_time_segment_to_complete_this_task') ?></p>
                        <p class="modal-para"><?php echo lang('task_planning_segment_must_be_between') ?> <strong id="start-date"><?php echo lang('post_step_4_start_date') ?></strong> and <strong id="end-date"><?php echo lang('post_step_4_end_date') ?></strong>.</p>
                        <div class="form-group form-total-hours-allocated">
                            <div class="form-flex">
                                <label class="input-lbl">
                                    <span class="input-label-txt"><?php echo lang('task_planning_total_hours_allocated') ?>:</span>
                                </label>
                                <input type="number" name="" class="form-control form-control-input" min="0" id="total-hours" placeholder="8" value="8" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group form-earning-per-hours">
                            <div class="form-flex">
                                <label class="input-lbl">
                                    <span class="input-label-txt"><?php echo lang('task_planning_earning_per_hour') ?></span>
                                </label>
                                <input type="number" name="" class="form-control form-control-input" min="0" id="task-earning" placeholder="10" value="10" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-date-taskplan-rows-container">
                            <div class="form-date-taskplan-row">
                                <div class="form-group form-col-taskplan-date">
                                    <div class="input-group input-group-datetimepicker date" id="form_taskplan_date" data-target-input="nearest">
                                        <input type="text" name="date[]" class="form-control datetimepicker-input" data-target="#form_taskplan_date" placeholder="<?php echo lang('select_date') ?>" />
                                        <span class="input-group-addon" data-target="#form_taskplan_date" data-toggle="datetimepicker">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group form-col-taskplan-time">
                                    <div class="form-col-taskplan-time-from">
                                        <div class="input-group input-group-datetimepicker time start" id="form_taskplan_time_from" data-target-input="nearest">
                                            <input type="text" name="start[]" class="form-control datetimepicker-input" data-target="#form_taskplan_time_from" placeholder="<?php echo lang('start_time') ?>" />
                                            <span class="input-group-addon" data-target="#form_taskplan_time_from" data-toggle="datetimepicker">
                                                <span class="fa fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-col-taskplan-time-to">
                                        <div class="input-group input-group-datetimepicker time end" id="form_taskplan_time_to" data-target-input="nearest">
                                            <input type="text" name="end[]" class="form-control datetimepicker-input" data-target="#form_taskplan_time_to" placeholder="<?php echo lang('end_time') ?>" />
                                            <span class="input-group-addon" data-target="#form_taskplan_time_to" data-toggle="datetimepicker">
                                                <span class="fa fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-col-taskplan-remove-link">
                                    <a class="taskplan-row-remove text-danger btn-link" href="#">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="addmore-actions">
                            <button type="button" class="btn-icon-full btn-addmore gtm-tc-task-plan-addmore">
                                <span class="btn-label"><?php echo lang('post_step_5_add_more') ?></span>
                                <span class="btn-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round">
                                        <line x1="12" y1="5" x2="12" y2="19"></line>
                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                    </svg>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel gtm-tc-task-plan-cancel" data-dismiss="modal" data-toggle="modal">
                        <span class="btn-label"><?php echo lang('profile_cancel') ?></span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <button type="submit" class="btn-icon-full btn-confirm gtm-tc-task-plan-confirm" data-orientation="next" disabled="disabled">
                        <span class="btn-label"><?php echo lang('profile_save') ?></span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /.modal -->