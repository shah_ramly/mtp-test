<?php $current_week = $dateObject->weekOfYear ?>
<?php $current_day = $dateObject->today() ?>
<?php $counter -= count($posted_active) + count($posted_inactive) ?>
<div class="tbl-header-calendar-gantt-body-overflow-wrapper dragscroll test">
    <div class="tbl-header-calendar-gantt-body-overflow">
        <div class="tbl-header-calendar-gantt-body-overflow-container">
            <div class="tbl-header-calendar-gantt-wrapper">
                <div class="tbl-header-calendar-gantt-month">
                    <div class="tbl-header-calendar-gantt-week-row-container">
                        <div class="tbl-header-calendar-gantt-week-row week-<?php echo $dateObject->copy()->subWeeks(2)->weekOfYear ?>">Week <?php echo $dateObject->copy()->subWeeks(2)->weekOfYear ?></div>
                        <div class="tbl-header-calendar-gantt-day">
                            <?php $weekStart = $dateObject->copy()->subWeeks(2)->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
                                <div class="tbl-header-calendar-gantt-day-row day-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?>">
                                    <?php echo $weekStart->localeDayOfWeek ?><!-- <sup>(<?php //echo $weekStart->day ?>)</sup> -->
                                </div>
                                <?php
                                $weekStart->addDay();
                                ?>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="tbl-header-calendar-gantt-week-row-container">
                        <div class="tbl-header-calendar-gantt-week-row week-<?php echo $dateObject->copy()->subWeek()->weekOfYear ?>">Week <?php echo $dateObject->copy()->subWeek()->weekOfYear ?></div>
                        <div class="tbl-header-calendar-gantt-day">
                            <?php $weekStart = $dateObject->copy()->subWeek()->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
                                <div class="tbl-header-calendar-gantt-day-row day-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?>">
                                    <?php echo $weekStart->localeDayOfWeek ?><!-- <sup>(<?php //echo $weekStart->day ?>)</sup> -->
                                </div>
                                <?php
                                $weekStart->addDay();
                                ?>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="tbl-header-calendar-gantt-week-row-container">
                        <div class="tbl-header-calendar-gantt-week-row week-<?php echo $dateObject->weekOfYear ?>">Week <?php echo $dateObject->weekOfYear ?></div>
                        <div class="tbl-header-calendar-gantt-day">
                            <?php $weekStart = $dateObject->copy()->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
                                <div class="tbl-header-calendar-gantt-day-row day-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?> <?php echo $weekStart->isSameDay($current_day->startOfDay()) ? 'active now' : '' ?> <?php echo $weekStart->isYesterday() ? 'prev' : '' ?> <?php echo $weekStart->isTomorrow() ? 'next' : '' ?>">
                                    <?php echo $weekStart->localeDayOfWeek ?><!-- <sup>(<?php //echo $weekStart->day ?>)</sup> -->
                                </div>
                                <?php
                                $weekStart->addDay();
                                ?>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="tbl-header-calendar-gantt-week-row-container">
                        <div class="tbl-header-calendar-gantt-week-row week-<?php echo $dateObject->copy()->addWeek()->weekOfYear ?>">Week <?php echo $dateObject->copy()->addWeek()->weekOfYear ?></div>
                        <div class="tbl-header-calendar-gantt-day">
                            <?php $weekStart = $dateObject->copy()->addWeek()->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
                                <div class="tbl-header-calendar-gantt-day-row day-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?>">
                                    <?php echo $weekStart->localeDayOfWeek ?><!-- <sup>(<?php //echo $weekStart->day ?>)</sup> -->
                                </div>
                                <?php
                                $weekStart->addDay();
                                ?>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="tbl-header-calendar-gantt-week-row-container">
                        <div class="tbl-header-calendar-gantt-week-row week-<?php echo $dateObject->copy()->addWeeks(2)->weekOfYear ?>">Week <?php echo $dateObject->copy()->addWeeks(2)->weekOfYear ?></div>
                        <div class="tbl-header-calendar-gantt-day">
                            <?php $weekStart = $dateObject->copy()->addWeeks(2)->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
                                <div class="tbl-header-calendar-gantt-day-row day-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?>">
                                    <?php echo $weekStart->localeDayOfWeek ?><!-- <sup>(<?php //echo $weekStart->day ?>)</sup> -->
                                </div>
                                <?php
                                $weekStart->addDay();
                                ?>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tbl-body-calendar-gantt">
                <div class="tbl-group-calendar-gantt-row tbl-group-calendar-awarded-row tbl-group-calendar-active-row">
                    <div class="tbl-group-calendar-gantt-grid-wrapper">
                        <div class="tbl-group-calendar-gantt-grid">
                            <?php $weekStart = $dateObject->copy()->subWeeks(2)->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
		                        <div class="gantt-grid-day-row gantt-grid-row-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?>"></div>
                                <?php $weekStart->addDay() ?>
                            <?php endfor; ?>
                            <?php $weekStart = $dateObject->copy()->subWeek()->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
		                        <div class="gantt-grid-day-row gantt-grid-row-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?>"></div>
                                <?php $weekStart->addDay() ?>
                            <?php endfor; ?>
                            <?php $weekStart = $dateObject->copy()->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
		                        <div class="gantt-grid-day-row gantt-grid-row-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?> <?php echo $weekStart->isSameDay($current_day->startOfDay()) ? 'active now' : '' ?> <?php echo $weekStart->isYesterday() ? 'prev' : '' ?> <?php echo $weekStart->isTomorrow() ? 'next' : '' ?>"></div>
                                <?php $weekStart->addDay() ?>
                            <?php endfor; ?>
                            <?php $weekStart = $dateObject->copy()->addWeek()->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
		                        <div class="gantt-grid-day-row gantt-grid-row-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?>"></div>
                                <?php $weekStart->addDay() ?>
                            <?php endfor; ?>
                            <?php $weekStart = $dateObject->copy()->addWeeks(2)->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
		                        <div class="gantt-grid-day-row gantt-grid-row-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?>"></div>
                                <?php $weekStart->addDay() ?>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="tbl-group-calendar-gantt-bar-wrapper">
                        <?php if(!empty($posted_active)): ?>
                            <?php set('tab_section', 'posted') ?>
                            <?php if(!isset($counter)) $counter = 1; ?>
                            <?php foreach($posted_active as $task): ?>
                                <?php
                                $current_date = $date->copy()->parse($data['current_date']);//->firstOfMonth()->startOfDay();
                                $start_by = $date->copy()->parse($task['start_by'])->startOfDay();
                                $complete_by = $date->copy()->parse($task['complete_by'])->endOfDay();
                                ?>
                                <?php /*if(
                                $start_by->isBetween($dateObject->copy()->subWeeks(2)->startOfWeek(), $dateObject->copy()->addWeeks(2)->endOfWeek())
                                ): */?>
                                    <?php
                                    if(
                                    $start_by->isBetween($dateObject->copy()->subWeeks(2)->startOfWeek(), $dateObject->copy()->addWeeks(2)->endOfWeek())
                                    ) {
                                        $start_day = ($start_by->copy()->diffInDays($dateObject->copy()->subWeeks(2)->startOfWeek())) + 1;
                                    }else{
                                        $start_day = $dateObject->copy()->subWeeks(2)->startOfWeek()->diffInDays($start_by) * -1;
                                    }

                                    $length = ceil($complete_by->diffInHours($start_by) / 24);
                                    ?>
                                    <?php echo partial('workspace/partial/task-centre/calendar/day-calendar-task-bar.html.php',
                                        [
                                            'task'         => $task,
                                            'counter'      => $counter,
                                            'start_day'    => $start_day,
                                            'current_date' => $current_date,
                                            'length'       => $length,
                                            'time_plans'   => $time_plans[$task['id']] ?? '',
                                            'pref_times'   => $preferred_times[$task['id']] ?? '',
                                        ]) ?>
                                    <?php $counter++; ?>
                                <?php /*endif; */?>
                            <?php endforeach; ?>
                        <?php else: ?>
		                    <div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-completed bar-row-1">
			                    <div class="tbl-group-calendar-gantt-bar-total-wrapper empty-bar"></div>
		                    </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="tbl-group-calendar-gantt-row tbl-group-calendar-created-row tbl-group-calendar-inactive-row">
                    <div class="tbl-group-calendar-gantt-grid-wrapper">
                        <div class="tbl-group-calendar-gantt-grid">
                            <?php $weekStart = $dateObject->copy()->subWeeks(2)->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
		                        <div class="gantt-grid-day-row gantt-grid-row-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?>"></div>
                                <?php $weekStart->addDay() ?>
                            <?php endfor; ?>
                            <?php $weekStart = $dateObject->copy()->subWeek()->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
		                        <div class="gantt-grid-day-row gantt-grid-row-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?>"></div>
                                <?php $weekStart->addDay() ?>
                            <?php endfor; ?>
                            <?php $weekStart = $dateObject->copy()->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
		                        <div class="gantt-grid-day-row gantt-grid-row-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?> <?php echo $weekStart->isSameDay($current_day->startOfDay()) ? 'active now' : '' ?> <?php echo $weekStart->isYesterday() ? 'prev' : '' ?> <?php echo $weekStart->isTomorrow() ? 'next' : '' ?>"></div>
                                <?php $weekStart->addDay() ?>
                            <?php endfor; ?>
                            <?php $weekStart = $dateObject->copy()->addWeek()->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
		                        <div class="gantt-grid-day-row gantt-grid-row-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?>"></div>
                                <?php $weekStart->addDay() ?>
                            <?php endfor; ?>
                            <?php $weekStart = $dateObject->copy()->addWeeks(2)->startOfWeek() ?>
                            <?php for ($i=0; $i<7;$i++): ?>
		                        <div class="gantt-grid-day-row gantt-grid-row-<?php echo $i+1 ?> <?php echo $weekStart->isDayOfWeek(\Carbon\Carbon::SATURDAY) || $weekStart->isDayOfWeek(\Carbon\Carbon::SUNDAY) ? 'inactive' : '' ?>"></div>
                                <?php $weekStart->addDay() ?>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="tbl-group-calendar-gantt-bar-wrapper">
                        <?php if(!empty($posted_inactive)): ?>
                            <?php set('tab_section', 'posted') ?>
                            <?php if(!isset($counter)) $counter = 1; ?>
                            <?php foreach($posted_inactive as $task): ?>
                                <?php
                                $current_date = $date->copy()->parse($data['current_date']);//->firstOfMonth()->startOfDay();
                                $start_by = $date->copy()->parse($task['start_by'])->startOfDay();
                                $complete_by = $date->copy()->parse($task['complete_by'])->endOfDay();
                                ?>
                                <?php /*if(
                                $start_by->isBetween($dateObject->copy()->subWeeks(2)->startOfWeek(), $dateObject->copy()->addWeeks(2)->endOfWeek())
                                ): */?>
                                    <?php
	                                if(
	                                $start_by->isBetween($dateObject->copy()->subWeeks(2)->startOfWeek(), $dateObject->copy()->addWeeks(2)->endOfWeek())
	                                ) {
	                                    $start_day = ($start_by->copy()->diffInDays($dateObject->copy()->subWeeks(2)->startOfWeek())) + 1;
	                                }else{
	                                    $start_day = $dateObject->copy()->subWeeks(2)->startOfWeek()->diffInDays($start_by) * -1;
	                                }
                                    $length = ceil($complete_by->diffInHours($start_by) / 24);
                                    ?>
                                    <?php echo partial('workspace/partial/task-centre/calendar/day-calendar-task-bar.html.php',
                                        [
                                            'task'         => $task,
                                            'counter'      => $counter,
                                            'start_day'    => $start_day,
                                            'current_date' => $current_date,
                                            'length'       => $length,
                                            'time_plans'   => $time_plans[$task['id']] ?? '',
                                            'pref_times'   => $preferred_times[$task['id']] ?? '',
                                        ]) ?>
                                    <?php $counter++; ?>
                                <?php /*endif; */?>
                            <?php endforeach; ?>
                        <?php else: ?>
		                    <div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-completed bar-row-1">
			                    <div class="tbl-group-calendar-gantt-bar-total-wrapper empty-bar"></div>
		                    </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
