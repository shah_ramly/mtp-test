<?php
$status = $task['status'] === 'completed' ? $task['status'] :
    (($task['status'] === 'published' && is_null($task['assigned'])) ?
        'pending' : 'active');
$from = \Carbon\Carbon::parse($task['start_by'])->startOfDay();
$to = \Carbon\Carbon::parse($task['complete_by'])->endOfDay();

$task_hours = $total_hours = ($task['hours'] > 0) ? $task['hours'] : $to->diffInRealSeconds($from)/3600;
?>
<div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-<?php echo $status ?> bar-row-<?php echo $counter ?>">
    <div class="tbl-group-calendar-gantt-bar-total-wrapper">
        <div class="tbl-group-calendar-gantt-bar-total"
             id="dropdownTotal<?php echo $counter ?>"
             data-toggle="dropdown"
             aria-haspopup="true"
             aria-expanded="false"
             style="
		             width:<?php echo ($from->isSameMonth($to)) ? 100 : (abs($to->month - $from->month) + 1) * 100 ?>px;
		             margin-left:<?php echo $from->isSameYear($current_date) ? ($from->month - 1) * 100 : $from->diffInMonths($current_date) * -100 ?>px;
		             height:29.19px;overflow:hidden;
		             "
             title="<?php echo  $from->format("d M Y").' - ' . $to->format("d M Y") ?>"
        >
            <div class="tbl-group-calendar-gantt-bar-total-caption">
	            <?php if((abs($to->month - $from->month) + 1) > 1): ?>
                <?php echo $from->format("d M Y") . ' - ' . $to->format("d M Y") ?>
                <?php else: ?>
		            <span style="width:20px;height:29.19px;color:white;display:block;margin:-1px auto 0;">
			            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="CurrentColor"
			                 stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round">
				            <circle cx="12" cy="12" r="1"></circle>
				            <circle cx="19" cy="12" r="1"></circle>
				            <circle cx="5" cy="12" r="1"></circle>
                        </svg>
		            </span>
                <?php endif ?>
	            <!--Jan 3, 2020 - Jan 15, 2020-->
            </div>
        </div>
        <div class="dropdown-menu" aria-labelledby="dropdownTotal<?php echo $counter ?>">
            <div class="dropdown-menu-wrapper">
	            <div class="dropdown-menu-top">
		            <div class="dropdown-date-calendar">
			            <div class="dropdown-lbl-container dropdown-duration-lbl-container">
				            <i class="tim-icons icon-calendar-60"></i>
				            <span class="dropdown-duration-lbl"><?php echo lang('task_duration') ?></span>
			            </div>
			            <span class="date-calendar-txt dropdown-date-val">
	                        <!--3 Jan 2020 - 15 Jan 2020-->
	                        <?php echo $from->format("d M Y") . ' - ' . $to->format("d M Y") ?>
                        </span>
		            </div>
		            <div class="dropdown-time-calendar">
                        <?php if((!isset($section) || $section !== 'posted') && isset($preferred_times[$task['id']])): ?>
			            <div class="dropdown-lbl-container dropdown-time-lbl-container">
				            <i class="tim-icons icon-time-alarm"></i>
				            <span class="dropdown-duration-lbl"><?php echo lang('task_your_preferred_working_time') ?></span>
			            </div>
			            <div class="dropdown-preferred-time-rows-container">
                        <?php foreach ($preferred_times[$task['id']] as $time): ?>
                        <?php $total_hours = $time['hours'] ?>
				            <div class="dropdown-preferred-time-row dropdown-date-val">
					            <span class="preferred-date"><?php echo $time['date']->format('d M Y') ?></span>
					            <span class="dash-seperator">-</span>
					            <span class="preferred-time-from"><?php echo $time['start']->format('h.iA') ?></span>
					            <span class="preferred-time-divider"><?php echo lang('task_to') ?></span>
					            <span class="preferred-time-to"><?php echo $time['end']->format('h.iA') ?></span>
				            </div>
                        <?php endforeach; ?>
			            </div>
                        <?php endif ?>
			            <div class="dropdown-tbl-wrapper">
				            <div class="dropdown-tbl-flex dropdown-tbl-row-1">
					            <div class="dropdown-tbl-flex-lbl"><?php echo lang('task_total_hours') ?></div>
					            <div class="dropdown-tbl-flex-val">
                                    <?php //if(!isset($total_hours)){ $total_hours = $task_hours; }
                                    echo number_format($total_hours) .' '. ($total_hours === 1 ? lang('task_hour'): lang('task_hours')) ?>
					            </div>
				            </div>
				            <div class="dropdown-tbl-flex dropdown-tbl-row-2">
					            <div class="dropdown-tbl-flex-lbl">
                                    <?php echo (!isset($section) || $section !== 'posted') ? lang('task_your_time_is_paying') : lang('task_you_are_paying')  ?>
					            </div>
					            <div class="dropdown-tbl-flex-val">RM<?php echo number_format($task['budget']/$total_hours, 2) ?>/<?php echo lang('task_hour') ?></div>
				            </div>
			            </div>
		            </div>
	            </div>
                <?php if((!isset($tab_section) || $tab_section !== 'posted') && ! isset($inactive)): ?>
	            <?php //$total_hours = ($task['hours'] > 0) ? $task['hours'] : $to->diffInHours($from) + ($to->diffInMinutes($from)/60) ?>
	            <div class="dropdown-menu-btm">
		            <button class="dropdown-item-btm" type="button"
		                    data-toggle="modal"
		                    data-task-id="<?php echo $task['id'] ?>"
		                    data-task-hours="<?php echo number_format($task_hours) ?>"
		                    data-task-earning="<?php echo number_format($task['budget']/$task_hours, 2) ?>"
		                    data-task-start="<?php echo $from->toDateString() ?>"
		                    data-task-end="<?php echo $to->toDateString() ?>"
		                    data-target="#modal_task_planning"><?php echo lang('task_change_this') ?></button>
	            </div>
	            <?php endif ?>
            </div>
        </div>
    </div>
    <div class="collapse multi-collapse" id="multiCollapse<?php echo $counter ?>">
        <div class="bar-row-gantt-chart-height"></div>
    </div>
</div>