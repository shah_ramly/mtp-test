<?php $current_week = $dateObject->weekOfYear ?>
<?php $counter -= count($posted_active) + count($posted_inactive) ?>
<div class="tbl-header-calendar-gantt-body-overflow-wrapper dragscroll test">
    <div class="tbl-header-calendar-gantt-body-overflow">
        <div class="tbl-header-calendar-gantt-body-overflow-container">
            <div class="tbl-header-calendar-gantt-wrapper">
                <div class="tbl-header-calendar-gantt-month">
                    <div class="tbl-header-calendar-gantt-month-row-container">
                        <div class="tbl-header-calendar-gantt-month-row month-<?php echo $dateObject->copy()->subMonth()->month ?>"><?php echo $dateObject->copy()->subMonth()->localeMonth ?></div>
                        <div class="tbl-header-calendar-gantt-week">
	                        <?php $weekNumber = $dateObject->copy()->subMonth()->weekOfYear ?>
	                        <?php $range = range($weekNumber, $weekNumber+3) ?>
	                        <?php foreach ($range as $key => $week): ?>
                            <div class="tbl-header-calendar-gantt-week-row week-<?php echo $key+1 ?> <?php echo $week === ($current_week - 1) ? 'prev' : ($week === ($current_week + 1) ? 'next' : ($week === $current_week ? 'active now':'')) ?>">W<?php echo $week ?></div>
	                        <?php endforeach ?>
                        </div>
                    </div>
                    <div class="tbl-header-calendar-gantt-month-row-container">
                        <div class="tbl-header-calendar-gantt-month-row month-<?php echo $dateObject->month ?>"><?php echo $dateObject->localeMonth ?></div>
                        <div class="tbl-header-calendar-gantt-week">
                            <?php $weekNumber = $dateObject->weekOfYear ?>
	                        <?php $range = range($weekNumber, $weekNumber+3) ?>
                            <?php foreach ($range as $key => $week): ?>
		                        <div class="tbl-header-calendar-gantt-week-row week-<?php echo $key+1 ?> <?php echo $week === ($current_week - 1) ? 'prev' : ($week === ($current_week + 1) ? 'next' : ($week === $current_week ? 'active now':'')) ?>">W<?php echo $week ?></div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div class="tbl-header-calendar-gantt-month-row-container">
                        <div class="tbl-header-calendar-gantt-month-row month-5">May</div>
                        <div class="tbl-header-calendar-gantt-week">
                            <?php $weekNumber = $dateObject->copy()->addMonth()->weekOfYear ?>
	                        <?php $range = range($weekNumber, $weekNumber+3) ?>
                            <?php foreach ($range as $key => $week): ?>
		                        <div class="tbl-header-calendar-gantt-week-row week-<?php echo $key+1 ?> <?php echo $week === ($current_week - 1) ? 'prev' : ($week === ($current_week + 1) ? 'next' : ($week === $current_week ? 'active now':'')) ?>">W<?php echo $week ?></div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tbl-body-calendar-gantt">
                <div class="tbl-group-calendar-gantt-row tbl-group-calendar-awarded-row tbl-group-calendar-active-row">
                    <div class="tbl-group-calendar-gantt-grid-wrapper">
                        <div class="tbl-group-calendar-gantt-grid">
                            <?php $weekNumber = $dateObject->copy()->subMonth()->weekOfYear ?>
                            <?php $range = range($weekNumber, $weekNumber+3) ?>
                            <?php foreach ($range as $key => $week): ?>
		                        <div class="gantt-grid-week-row gantt-grid-row-<?php echo $key+1 ?> <?php echo $week === ($current_week - 1) ? 'prev' : ($week === ($current_week + 1) ? 'next' : ($week === $current_week ? 'active now':'')) ?>"></div>
                            <?php endforeach ?>
                            <?php $weekNumber = $dateObject->weekOfYear ?>
                            <?php $range = range($weekNumber, $weekNumber+3) ?>
                            <?php foreach ($range as $key => $week): ?>
	                            <div class="gantt-grid-week-row gantt-grid-row-<?php echo $key+1 ?> <?php echo $week === ($current_week - 1) ? 'prev' : ($week === ($current_week + 1) ? 'next' : ($week === $current_week ? 'active now':'')) ?>"></div>
                            <?php endforeach ?>
                            <?php $weekNumber = $dateObject->copy()->addMonth()->weekOfYear ?>
                            <?php $range = range($weekNumber, $weekNumber+3) ?>
                            <?php foreach ($range as $key => $week): ?>
	                            <div class="gantt-grid-week-row gantt-grid-row-<?php echo $key+1 ?> <?php echo $week === ($current_week - 1) ? 'prev' : ($week === ($current_week + 1) ? 'next' : ($week === $current_week ? 'active now':'')) ?>"></div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div class="tbl-group-calendar-gantt-bar-wrapper">
                        <?php if(!empty($posted_active)): ?>
                            <?php set('tab_section', 'posted') ?>
                            <?php if(!isset($counter)) $counter = 1; ?>
                            <?php foreach($posted_active as $task): ?>
                                <?php
                                $current_date = $date->copy()->parse($data['current_date']);//->firstOfMonth()->startOfDay();
                                $start_by = $date->copy()->parse($task['start_by'])->startOfDay();
                                $complete_by = $date->copy()->parse($task['complete_by'])->endOfDay();
                                ?>
                                <?php /*if(
                                $start_by->isBetween($dateObject->copy()->subMonth()->startOfMonth(), $dateObject->copy()->addMonth()->endOfMonth())
                                ): */?>
                                    <?php $start_day = ($start_by->weekOfYear - $dateObject->copy()->subMonth()->weekOfYear) + 1;
                                    $length = $complete_by->isSameMonth($start_by) ? 1 :
                                        ( $complete_by->isNextMonth() ? 2 : 3 );
                                    ?>
                                    <?php echo partial('workspace/partial/task-centre/calendar/week-calendar-task-bar.html.php',
                                        [
                                            'task'         => $task,
                                            'counter'      => $counter,
                                            'start_day'    => $start_day,
                                            'current_date' => $current_date,
                                            'length'       => $length
                                        ]) ?>
                                    <?php $counter++; ?>
                                <?php /*endif; */?>
                            <?php endforeach; ?>
                        <?php else: ?>
		                    <div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-completed bar-row-1">
			                    <div class="tbl-group-calendar-gantt-bar-total-wrapper empty-bar"></div>
		                    </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="tbl-group-calendar-gantt-row tbl-group-calendar-created-row tbl-group-calendar-inactive-row">
                    <div class="tbl-group-calendar-gantt-grid-wrapper">
                        <div class="tbl-group-calendar-gantt-grid">
                            <?php $weekNumber = $dateObject->copy()->subMonth()->weekOfYear ?>
                            <?php $range = range($weekNumber, $weekNumber+3) ?>
                            <?php foreach ($range as $key => $week): ?>
		                        <div class="gantt-grid-week-row gantt-grid-row-<?php echo $key+1 ?> <?php echo $week === ($current_week - 1) ? 'prev' : ($week === ($current_week + 1) ? 'next' : ($week === $current_week ? 'active now':'')) ?>"></div>
                            <?php endforeach ?>
                            <?php $weekNumber = $dateObject->weekOfYear ?>
                            <?php $range = range($weekNumber, $weekNumber+3) ?>
                            <?php foreach ($range as $key => $week): ?>
		                        <div class="gantt-grid-week-row gantt-grid-row-<?php echo $key+1 ?> <?php echo $week === ($current_week - 1) ? 'prev' : ($week === ($current_week + 1) ? 'next' : ($week === $current_week ? 'active now':'')) ?>"></div>
                            <?php endforeach ?>
                            <?php $weekNumber = $dateObject->copy()->addMonth()->weekOfYear ?>
                            <?php $range = range($weekNumber, $weekNumber+3) ?>
                            <?php foreach ($range as $key => $week): ?>
		                        <div class="gantt-grid-week-row gantt-grid-row-<?php echo $key+1 ?> <?php echo $week === ($current_week - 1) ? 'prev' : ($week === ($current_week + 1) ? 'next' : ($week === $current_week ? 'active now':'')) ?>"></div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div class="tbl-group-calendar-gantt-bar-wrapper">
                        <?php if(!empty($posted_inactive)): ?>
                            <?php if(!isset($counter)) $counter = 1; ?>
                            <?php foreach($posted_inactive as $task): ?>
                                <?php
                                $current_date = $date->copy()->parse($data['current_date']);//->firstOfMonth()->startOfDay();
                                $start_by = $date->copy()->parse($task['start_by'])->startOfDay();
                                $complete_by = $date->copy()->parse($task['complete_by'])->endOfDay();
                                ?>
                                <?php /*if(
                                $start_by->isBetween($dateObject->copy()->subMonth()->startOfMonth(), $dateObject->copy()->addMonth()->endOfMonth())
                                ): */?>
                                    <?php set('tab_section', 'posted') ?>
                                    <?php $start_day = ($start_by->weekOfYear - $dateObject->copy()->subMonth()->weekOfYear) + 1;
                                    $length = $complete_by->isSameMonth($start_by) ? 1 :
                                        ( $complete_by->isNextMonth() ? 2 : 3 );
                                    ?>
                                    <?php echo partial('workspace/partial/task-centre/calendar/week-calendar-task-bar.html.php',
                                        [
                                            'task'         => $task,
                                            'counter'      => $counter,
                                            'start_day'    => $start_day,
                                            'current_date' => $current_date,
                                            'length'       => $length
                                        ]) ?>
                                    <?php $counter++; ?>
                                <?php /*endif; */?>
                            <?php endforeach; ?>
                        <?php else: ?>
		                    <div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-completed bar-row-1">
			                    <div class="tbl-group-calendar-gantt-bar-total-wrapper empty-bar"></div>
		                    </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>