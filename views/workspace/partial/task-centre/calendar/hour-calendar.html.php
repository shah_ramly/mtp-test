<?php $current_day = $dateObject->today() ?>
<?php $counter -= count($posted_active) + count($posted_inactive) ?>

<div class="tbl-header-calendar-gantt-body-overflow-wrapper dragscroll test">
	<div class="tbl-header-calendar-gantt-body-overflow">
		<div class="tbl-header-calendar-gantt-body-overflow-container">
			<div class="tbl-header-calendar-gantt-wrapper">
				<div class="tbl-header-calendar-gantt-month">
					<div class="tbl-header-calendar-gantt-day-row-container">
						<div class="tbl-header-calendar-gantt-day-row day-yesterday"><?php echo $dateObject->copy()->subDays(2)->format($cms->settings()['date_format']) ?></div>
						<div class="tbl-header-calendar-gantt-hour">
							<?php $prev = $dateObject->copy()->subDays(2)->startOfDay() ?>
							<?php foreach (range(1, $prev::HOURS_PER_DAY) as $key => $hour): ?>
							<div class="tbl-header-calendar-gantt-hour-row hour-<?php echo $key - 1 ?> <?php echo $prev->hour < 8 ? 'inactive' : '' ?>"><?php echo $prev->format('g.iA') ?></div>
							<?php $prev->addHour() ?>
							<?php endforeach ?>
						</div>
					</div>
					<div class="tbl-header-calendar-gantt-day-row-container">
						<div class="tbl-header-calendar-gantt-day-row day-yesterday">Yesterday</div>
						<div class="tbl-header-calendar-gantt-hour">
							<?php $yesterday = $dateObject->copy()->subDay()->startOfDay() ?>
							<?php foreach (range(1, $yesterday::HOURS_PER_DAY) as $key => $hour): ?>
							<div class="tbl-header-calendar-gantt-hour-row hour-<?php echo $key - 1 ?> <?php echo $yesterday->hour < 8 ? 'inactive' : '' ?>"><?php echo $yesterday->format('g.iA') ?></div>
							<?php $yesterday->addHour() ?>
							<?php endforeach ?>
						</div>
					</div>
					<div class="tbl-header-calendar-gantt-day-row-container">
						<div class="tbl-header-calendar-gantt-day-row day-today">Today</div>
						<div class="tbl-header-calendar-gantt-hour">
                            <?php $today = $dateObject->copy()->startOfDay() ?>
                            <?php $current_hour = $dateObject->copy()::now()->hour ?>
                            <?php foreach (range(1, $today::HOURS_PER_DAY) as $key => $hour): ?>
								<div class="tbl-header-calendar-gantt-hour-row hour-<?php echo $key - 1 ?> <?php echo $today->hour < 8 ? 'inactive' : ($today->hour === $current_hour ? 'active now' : ($today->hour + 1 === $current_hour ? 'prev': ($today->hour - 1 === $current_hour ? 'next' : ''))) ?>"><?php echo $today->format('g.iA') ?></div>
                                <?php $today->addHour() ?>
                            <?php endforeach ?>
						</div>
					</div>
					<div class="tbl-header-calendar-gantt-day-row-container">
						<div class="tbl-header-calendar-gantt-day-row day-tomorrow">Tomorrow</div>
						<div class="tbl-header-calendar-gantt-hour">
                            <?php $tomorrow = $dateObject->copy()->addDay()->startOfDay() ?>
                            <?php foreach (range(1, $tomorrow::HOURS_PER_DAY) as $key => $hour): ?>
								<div class="tbl-header-calendar-gantt-hour-row hour-<?php echo $key - 1 ?> <?php echo $tomorrow->hour < 8 ? 'inactive' : '' ?>"><?php echo $tomorrow->format('g.iA') ?></div>
                                <?php $tomorrow->addHour() ?>
                            <?php endforeach ?>
						</div>
					</div>
					<div class="tbl-header-calendar-gantt-day-row-container">
						<div class="tbl-header-calendar-gantt-day-row day-yesterday"><?php echo $dateObject->copy()->addDays(2)->format($cms->settings()['date_format']) ?></div>
						<div class="tbl-header-calendar-gantt-hour">
                            <?php $next = $dateObject->copy()->addDays(2)->startOfDay() ?>
                            <?php foreach (range(1, $next::HOURS_PER_DAY) as $key => $hour): ?>
								<div class="tbl-header-calendar-gantt-hour-row hour-<?php echo $key - 1 ?> <?php echo $next->hour < 8 ? 'inactive' : '' ?>"><?php echo $next->format('g.iA') ?></div>
                                <?php $next->addHour() ?>
                            <?php endforeach ?>
						</div>
					</div>
				</div>
			</div>
			<div class="tbl-body-calendar-gantt tbl-body-calendar-gantt-hour">
				<div class="tbl-group-calendar-gantt-row tbl-group-calendar-awarded-row tbl-group-calendar-active-row">
					<div class="tbl-group-calendar-gantt-grid-wrapper">
						<div class="tbl-group-calendar-gantt-grid">
                            <?php $prev = $dateObject->copy()->subDays(2)->startOfDay() ?>
                            <?php foreach (range(1, $prev::HOURS_PER_DAY) as $key => $hour): ?>
	                            <div class="gantt-grid-hour-row gantt-grid-row-<?php echo $key - 1 ?> <?php echo $prev->hour < 8 ? 'inactive' : '' ?>"></div>
                                <?php $prev->addHour() ?>
                            <?php endforeach ?>
                            <?php $yesterday = $dateObject->copy()->subDay()->startOfDay() ?>
                            <?php foreach (range(1, $yesterday::HOURS_PER_DAY) as $key => $hour): ?>
	                            <div class="gantt-grid-hour-row gantt-grid-row-<?php echo $key - 1 ?> <?php echo $yesterday->hour < 8 ? 'inactive' : '' ?>"></div>
                                <?php $yesterday->addHour() ?>
                            <?php endforeach ?>
                            <?php $today = $dateObject->copy()->startOfDay() ?>
                            <?php $current_hour = $dateObject->copy()::now()->hour ?>
                            <?php foreach (range(1, $today::HOURS_PER_DAY) as $key => $hour): ?>
	                            <div class="gantt-grid-hour-row gantt-grid-row-<?php echo $key - 1 ?> <?php echo $today->hour < 8 ? 'inactive' : ($today->hour === $current_hour ? 'active now' : ($today->hour + 1 === $current_hour ? 'prev': ($today->hour - 1 === $current_hour ? 'next' : ''))) ?>"></div>
                                <?php $today->addHour() ?>
                            <?php endforeach ?>
                            <?php $tomorrow = $dateObject->copy()->addDay()->startOfDay() ?>
                            <?php foreach (range(1, $tomorrow::HOURS_PER_DAY) as $key => $hour): ?>
	                            <div class="gantt-grid-hour-row gantt-grid-row-<?php echo $key - 1 ?> <?php echo $tomorrow->hour < 8 ? 'inactive' : '' ?>"></div>
                                <?php $tomorrow->addHour() ?>
                            <?php endforeach ?>
                            <?php $next = $dateObject->copy()->addDays(2)->startOfDay() ?>
                            <?php foreach (range(1, $next::HOURS_PER_DAY) as $key => $hour): ?>
	                            <div class="gantt-grid-hour-row gantt-grid-row-<?php echo $key - 1 ?> <?php echo $next->hour < 8 ? 'inactive' : '' ?>"></div>
                                <?php $next->addHour() ?>
                            <?php endforeach ?>
						</div>
					</div>
					<div class="tbl-group-calendar-gantt-bar-wrapper">
                        <?php if(!empty($posted_active)): ?>
                            <?php set('tab_section', 'posted') ?>
                            <?php if(!isset($counter)) $counter = 1; ?>
                            <?php foreach($posted_active as $task): ?>
                                <?php
                                $current_date = $date->copy()->parse($data['current_date']);
                                $start_by = $date->copy()->parse($task['start_by'])->startOfDay();
                                $complete_by = $date->copy()->parse($task['complete_by'])->endOfDay();
                                ?>
                                <?php /*if(
                                $start_by->isBetween($dateObject->copy()->subDay()->startOfDay(), $dateObject->copy()->addDay()->endOfDay())
                                ): */?>
                                    <?php
                                    if(
                                    $start_by->isBetween($dateObject->copy()->subDays(2)->startOfDay(), $dateObject->copy()->addDays(2)->endOfDay())
                                    ) {
                                        $start_day = $start_by->copy()->diffInHours($dateObject->copy()->subDays(2)->startOfDay());
                                    }else{
                                        $start_day = $start_by->copy()->diffInHours($dateObject->copy()->subDays(2)->startOfDay()->addHour()) * -1;
                                    }

                                    $length = $complete_by->diffInHours($start_by) + 1;
                                    $hours = $task['type'] === 'by-hour' ? (int)$task['hours'] : $complete_by->copy()->diffInMinutes($start_by)/60;
                                    ?>
                                    <?php echo partial('workspace/partial/task-centre/calendar/hour-calendar-task-bar.html.php',
                                        [
                                            'task'         => $task,
                                            'counter'      => $counter,
                                            'start_day'    => $start_day,
                                            'current_date' => $current_date,
                                            'length'       => $length,
                                            'hours'        => $hours,
                                            'time_plans'   => $time_plans[$task['id']] ?? '',
                                            'pref_times'   => $preferred_times[$task['id']] ?? '',
                                        ]) ?>
                                    <?php $counter++; ?>
                                <?php /*endif; */?>
                            <?php endforeach; ?>
                        <?php else: ?>
							<div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-completed bar-row-1">
								<div class="tbl-group-calendar-gantt-bar-total-wrapper empty-bar"></div>
							</div>
                        <?php endif; ?>
					</div>
				</div>
				<div class="tbl-group-calendar-gantt-row tbl-group-calendar-created-row tbl-group-calendar-inactive-row">
					<div class="tbl-group-calendar-gantt-grid-wrapper">
						<div class="tbl-group-calendar-gantt-grid">
                            <?php $prev = $dateObject->copy()->subDays(2)->startOfDay() ?>
                            <?php foreach (range(1, $prev::HOURS_PER_DAY) as $key => $hour): ?>
								<div class="gantt-grid-hour-row gantt-grid-row-<?php echo $key - 1 ?> <?php echo $prev->hour < 8 ? 'inactive' : '' ?>"></div>
                                <?php $prev->addHour() ?>
                            <?php endforeach ?>
                            <?php $yesterday = $dateObject->copy()->subDay()->startOfDay() ?>
                            <?php foreach (range(1, $yesterday::HOURS_PER_DAY) as $key => $hour): ?>
								<div class="gantt-grid-hour-row gantt-grid-row-<?php echo $key - 1 ?> <?php echo $yesterday->hour < 8 ? 'inactive' : '' ?>"></div>
                                <?php $yesterday->addHour() ?>
                            <?php endforeach ?>
                            <?php $today = $dateObject->copy()->startOfDay() ?>
                            <?php $current_hour = $dateObject->copy()::now()->hour ?>
                            <?php foreach (range(1, $today::HOURS_PER_DAY) as $key => $hour): ?>
								<div class="gantt-grid-hour-row gantt-grid-row-<?php echo $key - 1 ?> <?php echo $today->hour < 8 ? 'inactive' : ($today->hour === $current_hour ? 'active now' : ($today->hour + 1 === $current_hour ? 'prev': ($today->hour - 1 === $current_hour ? 'next' : ''))) ?>"></div>
                                <?php $today->addHour() ?>
                            <?php endforeach ?>
                            <?php $tomorrow = $dateObject->copy()->addDay()->startOfDay() ?>
                            <?php foreach (range(1, $tomorrow::HOURS_PER_DAY) as $key => $hour): ?>
								<div class="gantt-grid-hour-row gantt-grid-row-<?php echo $key - 1 ?> <?php echo $tomorrow->hour < 8 ? 'inactive' : '' ?>"></div>
                                <?php $tomorrow->addHour() ?>
                            <?php endforeach ?>
                            <?php $next = $dateObject->copy()->addDays(2)->startOfDay() ?>
                            <?php foreach (range(1, $next::HOURS_PER_DAY) as $key => $hour): ?>
								<div class="gantt-grid-hour-row gantt-grid-row-<?php echo $key - 1 ?> <?php echo $next->hour < 8 ? 'inactive' : '' ?>"></div>
                                <?php $next->addHour() ?>
                            <?php endforeach ?>
						</div>
					</div>
					<div class="tbl-group-calendar-gantt-bar-wrapper">
                        <?php if(!empty($posted_inactive)): ?>
                            <?php set('tab_section', 'posted') ?>
                            <?php if(!isset($counter)) $counter = 1; ?>
                            <?php foreach($posted_inactive as $task): ?>
                                <?php
                                $current_date = $date->copy()->parse($data['current_date']);
                                $start_by = $date->copy()->parse($task['start_by'])->startOfDay();
                                $complete_by = $date->copy()->parse($task['complete_by'])->endOfDay();
                                ?>
                                <?php /*if(
                                $start_by->isBetween($dateObject->copy()->subDay()->startOfDay(), $dateObject->copy()->addDay()->endOfDay())
                                ): */?>
                                    <?php
	                                if(
	                                $start_by->isBetween($dateObject->copy()->subDays(2)->startOfDay(), $dateObject->copy()->addDays(2)->endOfDay())
	                                ) {
	                                    $start_day = $start_by->copy()->diffInHours($dateObject->copy()->subDays(2)->startOfDay());
	                                }else{
	                                    $start_day = $start_by->copy()->diffInHours($dateObject->copy()->subDays(2)->startOfDay()->addHour()) * -1;
	                                }
                                    $length = $complete_by->diffInHours($start_by) + 1;
                                    $hours = $task['type'] === 'by-hour' ? (int)$task['hours'] : $complete_by->copy()->diffInMinutes($start_by)/60;
                                    ?>
                                    <?php echo partial('workspace/partial/task-centre/calendar/hour-calendar-task-bar.html.php',
                                        [
                                            'task'         => $task,
                                            'counter'      => $counter,
                                            'start_day'    => $start_day,
                                            'current_date' => $current_date,
                                            'length'       => $length,
                                            'hours'        => $hours,
                                            'time_plans'   => $time_plans[$task['id']] ?? '',
                                            'pref_times'   => $preferred_times[$task['id']] ?? '',
                                        ]) ?>
                                    <?php $counter++; ?>
                                <?php /*endif; */?>
                            <?php endforeach; ?>
                        <?php else: ?>
							<div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-completed bar-row-1">
								<div class="tbl-group-calendar-gantt-bar-total-wrapper empty-bar"></div>
							</div>
                        <?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>