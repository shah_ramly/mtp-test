<?php $counter -= count($applied_active) + count($applied_inactive) ?>
<div class="tbl-header-calendar-gantt-body-overflow-wrapper dragscroll test">
	<div class="tbl-header-calendar-gantt-body-overflow">
		<div class="tbl-header-calendar-gantt-body-overflow-container">
			<div class="tbl-header-calendar-gantt-wrapper">
				<div class="tbl-header-calendar-gantt-month">
					<div class="tbl-header-calendar-gantt-year-row-container">
						<div class="tbl-header-calendar-gantt-year-row year-<?php echo $date->year ?>">
                            <?php echo $date->year ?>
						</div>
						<div class="tbl-header-calendar-gantt-month">
                            <?php foreach (range(1, 12) as $month): ?>
								<div class="tbl-header-calendar-gantt-month-row month-<?php echo $month ?> <?php echo $month === ($current_month - 1) ? 'prev' : ($month === ($current_month + 1) ? 'next' : ($month === $current_month ? 'active now':'')) ?>">
                                    <?php echo $date->shortLocaleMonth; $date->addMonth() ?>
								</div>
                            <?php endforeach ?>
						</div>
					</div>
				</div>
			</div>
			<div class="tbl-body-calendar-gantt">
				<div class="tbl-group-calendar-gantt-row tbl-group-calendar-awarded-row tbl-group-calendar-active-row">
					<div class="tbl-group-calendar-gantt-grid-wrapper">
						<div class="tbl-group-calendar-gantt-grid">
                            <?php $date->startOfYear() // Reset back the date to first month ?>
                            <?php foreach (range(1, 12) as $month): ?>
								<div class="gantt-grid-month-row gantt-grid-row-<?php echo $month ?> <?php echo $month === ($current_month - 1) ? 'prev' : ($month === ($current_month + 1) ? 'next' : ($month === $current_month ? 'active now':'')) ?>">
								</div>
                                <?php $date->addMonth() ?>
                            <?php endforeach ?>
						</div>
					</div>
					<div class="tbl-group-calendar-gantt-bar-wrapper">
                        <?php if(!empty($applied_active)): ?>
                            <?php set('tab_section', 'awarded') ?>
                            <?php if(!isset($counter)) $counter = 1; ?>
                            <?php foreach($applied_active as $task): ?>
                                <?php
                                $current_date = $date->copy()->parse($data['current_date'])->firstOfMonth()->startOfDay();
                                $start_by = $date->copy()->parse($task['start_by'])->startOfDay();
                                $complete_by = $date->copy()->parse($task['complete_by'])->endOfDay();
                                ?>
                                <?php /*if(
                                $current_date->isBetween($start_by->copy()->startOfYear(), $complete_by->copy()->endOfYear())
                                ): */?>
                                    <?php $start_day = $start_by->isSameQuarter($current_date) ? $start_by->day : 1; ?>
                                    <?php set('section', 'awarded') ?>
                                    <?php echo partial('workspace/partial/task-centre/calendar/month-calendar-task-bar.html.php',
                                        [
                                            'task'      => $task,
                                            'counter'   => $counter,
                                            'start_day' => $start_day,
                                            'current_date' => $current_date
                                        ]) ?>
                                    <?php $counter++; ?>
                                <?php /*endif; */?>
                            <?php endforeach; ?>
                        <?php else: ?>
							<div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-completed bar-row-1">
								<div class="tbl-group-calendar-gantt-bar-total-wrapper empty-bar"></div>
							</div>
                        <?php endif; ?>
					</div>
				</div>
				<div class="tbl-group-calendar-gantt-row tbl-group-calendar-created-row tbl-group-calendar-inactive-row">
					<div class="tbl-group-calendar-gantt-grid-wrapper">
						<div class="tbl-group-calendar-gantt-grid">
                            <?php $date->startOfYear() // Reset back the date to first month ?>
                            <?php foreach (range(1, 12) as $month): ?>
								<div class="gantt-grid-month-row gantt-grid-row-<?php echo $month ?> <?php echo $month === ($current_month - 1) ? 'prev' : ($month === ($current_month + 1) ? 'next' : ($month === $current_month ? 'active now':'')) ?>">
								</div>
                                <?php $date->addMonth() ?>
                            <?php endforeach ?>
						</div>
					</div>
					<div class="tbl-group-calendar-gantt-bar-wrapper">
                        <?php if(!empty($applied_inactive)): ?>
                            <?php set('tab_section', 'awarded') ?>
                            <?php set('inactive', true) ?>
                            <?php if(!isset($counter)) $counter = 1; ?>
                            <?php foreach($applied_inactive as $task): ?>
                                <?php
                                $current_date = $date->copy()->parse($data['current_date'])->firstOfMonth()->startOfDay();
                                $start_by = $date->copy()->parse($task['start_by'])->startOfDay();
                                $complete_by = $date->copy()->parse($task['complete_by'])->endOfDay();
                                ?>
                                <?php /*if(
                                $current_date->isBetween($start_by->copy()->startOfYear(), $complete_by->copy()->endOfYear())
                                ): */?>
                                    <?php $start_day = $start_by->isSameQuarter($current_date) ? $start_by->day : 1; ?>
                                    <?php set('section', 'awarded') ?>
                                    <?php echo partial('workspace/partial/task-centre/calendar/month-calendar-task-bar.html.php',
                                        [
                                            'task'      => $task,
                                            'counter'   => $counter,
                                            'start_day' => $start_day,
                                            'current_date' => $current_date
                                        ]) ?>
                                    <?php $counter++; ?>
                                <?php /*endif; */?>
                            <?php endforeach; ?>
                        <?php else: ?>
							<div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-completed bar-row-1">
								<div class="tbl-group-calendar-gantt-bar-total-wrapper empty-bar"></div>
							</div>
                        <?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>