<?php if( $shownTab === 'applied' ): ?>
	<?php if( $selectedSection === 'active' ): ?>

		<?php if( !empty( $tasks['all_applied']['active'] ) ): ?>
		    <?php foreach ($tasks['all_applied']['active'] as $task): ?>
		        <?php echo partial('workspace/partial/task-centre/applied-task-row.html.php', ['task' => $task]); ?>
		    <?php endforeach ?>
		<?php else: ?>
		    <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
		        <div class="tbl-group-calendar-bar-flex">
		            <div class="tbl-group-calendar-bar-row row-1">
		                <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_applied_task') ?></span>
		            </div>
		        </div>
		    </div>
		<?php endif ?>

	<?php elseif ($selectedSection === 'inactive'): ?>

		<?php if( !empty( $tasks['all_applied']['inactive'] ) ): ?>
		    <?php foreach ($tasks['all_applied']['inactive'] as $task): ?>
		        <?php echo partial('workspace/partial/task-centre/applied-task-row.html.php', ['task' => $task]); ?>
		    <?php endforeach ?>
		<?php else: ?>
		    <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
		        <div class="tbl-group-calendar-bar-flex">
		            <div class="tbl-group-calendar-bar-row row-1">
		                <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_applied_task') ?></span>
		            </div>
		        </div>
		    </div>
		<?php endif ?>

	<?php endif ?>

<?php elseif ($shownTab === 'posted'): ?>
    <?php if( $selectedSection === 'active' ): ?>

		<?php if( !empty( $tasks['all_posted']['active'] ) ): ?>
		    <?php foreach ($tasks['all_posted']['active'] as $task): ?>
		        <?php $task['questions'] = !empty($questions[$task['id']]) ? $questions[$task['id']][$task['id']] : []; ?>
		        <?php echo partial('workspace/partial/task-centre/posted-task-row.html.php', ['task' => $task]); ?>
		    <?php endforeach ?>
		<?php else: ?>
		    <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
		        <div class="tbl-group-calendar-bar-flex">
		            <div class="tbl-group-calendar-bar-row row-1">
		                <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_posted_task') ?></span>
		            </div>
		        </div>
		    </div>
		<?php endif ?>

    <?php elseif ($selectedSection === 'inactive'): ?>

		<?php if( !empty( $tasks['all_posted']['inactive'] ) ): ?>
		    <?php foreach ($tasks['all_posted']['inactive'] as $task): ?>
		        <?php $task['questions'] = !empty($questions[$task['id']]) ? $questions[$task['id']][$task['id']] : []; ?>
		        <?php echo partial('workspace/partial/task-centre/posted-task-row.html.php', ['task' => $task]); ?>
		    <?php endforeach ?>
		<?php else: ?>
		    <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
		        <div class="tbl-group-calendar-bar-flex">
		            <div class="tbl-group-calendar-bar-row row-1">
		                <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_posted_task') ?></span>
		            </div>
		        </div>
		    </div>
		<?php endif ?>

	<?php endif ?>

<?php endif ?>