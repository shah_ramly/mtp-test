<div class="modal-body">
    <div class="milestone-steps-container">
        <div class="milestone-indicator-container">
            <div class="milestone-step milestone-step-1">
                    <span class="milestone-complete-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                        </svg>
                    </span>
                <div class="milestone-label"><?php echo lang('milestone_appointed'); ?></div>
                <div class="milestone-icon"></div>
            </div>
            <div class="milestone-line milestone-line-1"></div>
            <div class="milestone-step milestone-step-2">
                    <span class="milestone-complete-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                        </svg>
                    </span>
                <div class="milestone-label"><?php echo lang('milestone_confirmed'); ?></div>
                <div class="milestone-icon"></div>
            </div>
            <div class="milestone-line milestone-line-2">
            </div>
            <div class="milestone-step milestone-step-3">
                    <span class="milestone-complete-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                        </svg>
                    </span>
                <div class="milestone-label milestone-label-waiting"><?php echo lang('milestone_waiting'); ?></div>
                <div class="milestone-label milestone-label-inprogress"><?php echo lang('milestone_in_progress'); ?></div>
                <div class="milestone-icon in-progress"></div>
            </div>
            <div class="milestone-line milestone-line-3"></div>
            <div class="milestone-step milestone-step-4">
                    <span class="milestone-complete-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                        </svg>
                    </span>
                <div class="milestone-label"><?php echo lang('milestone_completed'); ?></div>
                <div class="milestone-icon"></div>
            </div>
            <div class="milestone-line milestone-line-4"></div>
            <div class="milestone-step milestone-step-5">
                    <span class="milestone-complete-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                        </svg>
                    </span>
                <div class="milestone-label"><?php echo lang('milestone_rate'); ?></div>
                <div class="milestone-icon"></div>
            </div>
            <div class="milestone-line milestone-line-5"></div>
            <div class="milestone-step milestone-step-6">
                    <span class="milestone-complete-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                        </svg>
                    </span>
                <div class="milestone-label"><?php echo lang('milestone_closed'); ?></div>
                <div class="milestone-icon"></div>
            </div>
        </div>
    </div>
    <div class="milestone-content milestone-content-1">
        <div class="milestone-title"><?php echo sprintf(lang('milestone_to_accept_decline_task'), $seeker['name']); ?></div>
        <p class="modal-para"><?php echo $seeker['name'] ?> <?php echo lang('milestone_seeker_has_24_hours_to_accept'); ?>.</p>
    </div>
    <div class="milestone-content milestone-content-2">
        <div class="milestone-title"><?php echo $seeker['name'] ?> <?php echo lang('milestone_has_accepted_your_task'); ?></div>
        <p class="modal-para"><?php echo $seeker['name'] ?> <?php echo lang('milestone_is_ready_to_start'); ?>.
            <span class="milestone-task-amount">
                <span class="milestone-task-row">
                    <span class="milestone-task-lbl"><?php echo lang('milestone_task_value'); ?> :</span>
                    <span class="milestone-task-val">RM<?php echo number_format($task['budget'], 2) ?></span>
                </span>
                <span class="milestone-task-row">
                    <span class="milestone-task-lbl"><?php echo lang('milestone_processing_fee'); ?> :</span>
                    <span class="milestone-task-val">RM<?php echo number_format($task['service_charge'], 2) ?></span>
                </span>
                <span class="milestone-task-row">
                    <span class="milestone-task-lbl"><?php echo lang('milestone_total_amount'); ?> :</span>
                    <span class="milestone-task-val">RM<?php echo number_format($task['budget'] + $task['service_charge'], 2) ?></span>
                </span>
            </span>
            <span class="milestone-payment-failed <?php echo (!isset($task['payment_status']) || (isset($task['payment_status']) && $task['payment_status'])) ? 'hide' : '' ?>">
	            <?php echo lang('milestone_payment_failed_please_try_again'); ?>.
	            <?php if( isset($task['payment']['message']) ): ?>
	            <br><span class="font-weight-normal">Reason: <?php echo $task['payment']['message'] ?? ''; ?></span>
	            <?php endif ?>
            </span>
        </p>
    </div>
    <div class="milestone-content milestone-content-3a">
        <div class="milestone-title"><?php echo lang('milestone_your_payment_has_been_received'); ?>.</div>
        <p class="modal-para"><?php echo lang('milestone_this_task_will_commence_on'); ?> <?php echo $task['start_date'] ?></p>
    </div>
    <div class="milestone-content milestone-content-3b">
        <div class="milestone-title"><?php echo lang('milestone_this_task_has_commenced'); ?>.</div>
        <p class="modal-para"><?php echo lang('milestone_this_task_is_currently_in_progress'); ?>.</p>
    </div>
    <div class="milestone-content milestone-content-3c">
        <div class="milestone-title"><?php echo lang('milestone_this_task_has_commenced'); ?>.</div>
        <p class="modal-para"><?php echo lang('milestone_you_reached_the_end_date_of_the_task'); ?></p>
        <div class="milestone-radio-container">
            <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="posterMilestoneYes" value="yes" checked name="milestone_close_poster">
                <label class="custom-control-label" for="posterMilestoneYes"><?php echo lang('milestone_yes_release_payment_now'); ?></label>
            </div>
            <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="posterMilestoneNo" value="no" name="milestone_close_poster">
                <label class="custom-control-label" for="posterMilestoneNo"><?php echo lang('milestone_no'); ?></label>
            </div>
        </div>
        <div class="milestone-radio-dispute-options" id="milestoneDisputeOpt" style="display:none;">
            <p class="modal-para"><?php echo lang('milestone_please_tell_us_more_about_this'); ?>:</p>
            <select class="form-control form-control-input" id="selectRejectOption">
                <option disabled="" selected=""><?php echo lang('milestone_please_select'); ?></option>
                <option value="milestone-reject-1"><?php echo lang('milestone_the_seeker_has_gone_missing'); ?></option>
                <option value="milestone-reject-2"><?php echo lang('milestone_the_work_is_completed_but_not'); ?>.</option>
            </select>
        </div>
        <div class="milestone-radio-dispute-percentage" id="milestoneDisputePercent" style="display:none;">
            <div class="form-flex form-rejection-rate-numerator">
                <span class="prelbl-percent-value"><?php echo lang('milestone_id_like_to_pay_the_seeker'); ?> </span>
	            <input type="hidden" name="task_price" id="task_price" value="<?php echo str_replace([',', '.'], '', number_format($task['budget'], 0)) ?>" />
                <select class="form-control form-control-input" id="disputePoster_rejectionRateNumerator">
                    <option disabled="" selected=""><?php echo lang('milestone_select'); ?></option>
                    <option value="0">0</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option value="40">40</option>
                    <option value="50">50</option>
                    <option value="60">60</option>
                    <option value="70">70</option>
                    <option value="80">80</option>
                    <option value="90">90</option>
                    <option value="100">100</option>
                </select>
                <span class="postlbl-percent-value"><?php echo lang('milestone_of_work_the_task_value'); ?>.</span>
            </div>
	        <p class="modal-para"><?php echo lang('milestone_note'); ?>: <span class="to-be-paid"><?php echo $cms->price($task['budget']) ?></span> <?php echo lang('milestone_will_be_paid_to_the_seeker'); ?> <span class="balance"><?php echo $cms->price(0) ?></span> <?php echo lang('milestone_will_refunded_to_you'); ?>.</p>
            <p class="modal-para"><?php echo lang('milestone_if_you_choose_to_proceed'); ?>.</p>
        </div>
    </div>
    <div class="milestone-content milestone-content-4">
        <div class="milestone-title"><?php echo $seeker['name'] ?> <?php echo lang('milestone_has_marked_this_task_as_completed'); ?>.</div>
        <p class="modal-para"><?php echo lang('milestone_your_response_is_required'); ?>.</p>
    </div>
    <div class="milestone-content milestone-content-5a">
        <div class="milestone-title">Rate <?php echo $seeker['name'] ?>!</div>
        <p class="modal-para"><?php echo sprintf(lang('milestone_know_what_you_think_about'), $seeker['name']); ?>!</p>
        <p class="modal-para"><?php echo lang('milestone_you_have_60_hours_to_rate'); ?>.</p>
    </div>
    <div class="milestone-content milestone-content-5b">
        <div class="milestone-title"><?php echo lang('milestone_this_task_is_disputed'); ?></div>
        <p class="modal-para"><?php echo lang('milestone_maketimepay_has_released'); ?> $disputedPercentagePoster% <?php echo lang('milestone_of_the_task_value'); ?> (<?php echo number_format($task['budget'], 2) ?>) <?php echo lang('milestone_to_you_and_balance_to'); ?> <?php echo $seeker['name'] ?>. <?php echo lang('milestone_please_take_some_time_to_rate'); ?> <?php echo $seeker['name'] ?>.</p>
    </div>
    <div class="milestone-content milestone-content-7">
        <div class="milestone-title"><?php echo lang('milestone_waiting_for') .' '. $seeker['name'] .' '. lang('milestone_to_accept_your_dispute_resolution') ?>.</div>
        <p class="modal-para"><?php echo lang('milestone_the_seeker_has_60_hours'); ?>.</p>
    </div>
    <div class="milestone-content milestone-content-7-1">
        <div class="milestone-title"><?php echo lang('milestone_this_task_is_closed'); ?></div>
        <p class="modal-para"><?php echo lang('milestone_this_task_has_been_successfully_completed'); ?></p>
    </div>
    <div class="milestone-content milestone-content-7-2">
        <div class="milestone-title"><?php echo lang('milestone_task_has_been') .' '. lang('task_' . $task['status']) ?></div>
        <p class="modal-para"><?php echo ! $task['other_person'] ? lang('milestone_you_have') : lang('milestone_you_task_seeeker_has') ?> <?php echo $task['status'] ?> <?php echo lang('milestone_this_task'); ?>.</p> <!-- ($endState could be Cancelled, Declined or Rejected) -->
    </div>
    <div class="milestone-content milestone-content-7-3">
        <div class="milestone-title"><?php echo lang('milestone_this_is_unfulfilled_by'); ?> <?php echo $seeker['name'] ?></div>
        <p class="modal-para"><?php echo lang('milestone_as'); ?>As <?php echo $seeker['name'] ?> <?php echo lang('milestone_did_not_participate'); ?>.</p>
    </div>
    <div class="milestone-content milestone-content-7-4">
        <div class="milestone-title"><?php echo lang('milestone_task_has_been') .' '. lang('task_' . $task['status']) ?></div>
        <p class="modal-para"><?php echo lang('milestone_this_task_has_been_disputed_and_resolved') ?>.</p>
    </div>
</div>

<div class="modal-footer">
	<div class="milestone-footer modal-footer-content-1">
		<div class="modal-footer modal-footer-milestone">
			<form action="<?php echo url_for('workspace/task/retract') ?>" method="post">
                <?php echo html_form_token_field() ?>
				<input type="hidden" name="task_id" value="<?php echo $task['task_id'] ?>" />
				<input type="hidden" name="user_id" value="<?php echo $seeker['m_id'] ?>" />
				<button type="submit" class="btn-icon-full btn-cancel btn-retract gtm-milestone-poster-retract" data-orientation="next">
					<span class="btn-label"><?php echo lang('milestone_retract_selection') ?></span>
					<span class="btn-icon">
                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                        </svg>
                    </span>
				</button>
			</form>
		</div>
	</div>
	<div class="milestone-footer modal-footer-content-2">
		<div class="modal-footer modal-footer-milestone">
			<form action="<?php echo url_for('workspace/task/retract') ?>" method="post">
                <?php echo html_form_token_field() ?>
				<input type="hidden" name="task_id" value="<?php echo $task['task_id'] ?>" />
				<input type="hidden" name="user_id" value="<?php echo $seeker['m_id'] ?>" />
				<button type="submit" class="btn-icon-full btn-cancel btn-retract gtm-milestone-poster-retract" data-orientation="next">
					<span class="btn-label"><?php echo lang('milestone_retract_selection') ?></span>
					<span class="btn-icon">
                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                        </svg>
                    </span>
				</button>
			</form>
			<form action="<?php echo url_for('/workspace/task/assign') ?>" method="post">
                <?php echo html_form_token_field() ?>
				<input type="hidden" name="task_id" value="<?php echo $task['task_id'] ?>" />
				<input type="hidden" name="user_id" value="<?php echo $seeker['m_id'] ?>" />
				<button type="submit" class="btn-icon-full btn-confirm gtm-milestone-poster-paynow" data-orientation="next">
					<span class="btn-label"><?php echo lang('task_payment_details_pay_now') ?></span>
					<span class="btn-icon">
                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                        </svg>
                    </span>
				</button>
			</form>
		</div>
	</div>
    <div class="milestone-footer modal-footer-content-3b">
        <div class="modal-footer modal-footer-milestone">
	        <form method="get" action="<?php echo url_for("/workspace/task/{$task['slug']}/details") ?>">
	            <button type="submit" class="btn-icon-full btn-cancel btn-viewtask gtm-milestone-poster-viewtask" data-orientation="next">
	                <span class="btn-label"><?php echo lang('milestone_view_task') ?></span>
	                <span class="btn-icon">
	                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                    </svg>
	                </span>
	            </button>
	        </form>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-3c">
        <div class="modal-footer modal-footer-milestone">
	        <form method="get" action="<?php echo url_for("/workspace/task/{$task['slug']}/details") ?>">
	            <button type="submit" class="btn-icon-full btn-cancel btn-viewtask gtm-milestone-poster-viewtask" data-orientation="next">
	                <span class="btn-label"><?php echo lang('milestone_view_task') ?></span>
	                <span class="btn-icon">
	                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                    </svg>
	                </span>
	            </button>
	        </form>
	        <form method="post" action="<?php echo url_for("/workspace/task/dispute") ?>">
		        <?php echo html_form_token_field() ?>
		        <input type="hidden" name="percentage" value="100" />
		        <input type="hidden" name="reason" value="" />
		        <input type="hidden" name="dispute" value="no" />
		        <input type="hidden" name="task_id" value="<?php echo $task['task_id'] ?>" />
		        <input type="hidden" name="seeker_id" value="<?php echo $seeker['m_id'] ?>" />
	            <button type="submit" class="btn-icon-full btn-closetask gtm-milestone-poster-proceed" data-orientation="next">
	                <span class="btn-label"><?php echo lang('proceed') ?></span>
	                <span class="btn-icon">
	                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                    </svg>
	                </span>
	            </button>
	        </form>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-4">
        <div class="modal-footer modal-footer-milestone">
	        <form method="post" action="<?php echo url_for('/workspace/task/accept') ?>">
                <?php echo html_form_token_field() ?>
		        <input type="hidden" name="task_id" value="<?php echo $task['task_id'] ?>" />
		        <input type="hidden" name="user_id" value="<?php echo $seeker['m_id'] ?>" />
	            <button type="submit" class="btn-icon-full btn-yes gtm-milestone-poster-accept-release-payment">
	                <span class="btn-label"><?php echo lang('milestone_accept_and_release_payment') ?></span>
	                <span class="btn-icon">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                        <polyline points="20 6 9 17 4 12"></polyline>
	                    </svg>
	                </span>
	            </button>
	        </form>
	        <form method="post" action="<?php echo url_for('/workspace/task/reject') ?>">
		        <?php echo html_form_token_field() ?>
		        <input type="hidden" name="task_id" value="<?php echo $task['task_id'] ?>" />
		        <input type="hidden" name="user_id" value="<?php echo $seeker['m_id'] ?>" />
	            <button type="submit" class="btn-icon-full btn-cancel btn-no gtm-milestone-poster-reject">
	                <span class="btn-label"><?php echo lang('task_reject') ?></span>
	                <span class="btn-icon">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                        <line x1="18" y1="6" x2="6" y2="18"></line>
	                        <line x1="6" y1="6" x2="18" y2="18"></line>
	                    </svg>
	                </span>
	            </button>
	        </form>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-5a">
        <div class="modal-footer modal-footer-milestone">
	        <form method="get" action="<?php echo url_for("/workspace/billings/spent") ?>">
	            <button type="submit" class="btn-icon-full btn-paymentdetail gtm-milestone-poster-view-payment-detail">
	                <span class="btn-label"><?php echo lang('milestone_view_payment_detail') ?></span>
	                <span class="btn-icon">
	                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                    </svg>
	                </span>
	            </button>
	        </form>
	        <form method="get" action="<?php echo url_for("/workspace/rating-centre") ?>">
		        <input type="hidden" name="review" value="<?php echo $task['task_id'] ?>" />
		        <input type="hidden" name="seeker" value="<?php echo $seeker['m_id'] ?>" />
	            <button type="submit" class="btn-icon-full btn-ratingctr gtm-milestone-poster-rating-centre">
	                <span class="btn-label"><?php echo lang('milestone_rating_centre') ?></span>
	                <span class="btn-icon">
	                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                    </svg>
	                </span>
	            </button>
	        </form>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-5b">
        <div class="modal-footer modal-footer-milestone">
	        <form method="get" action="<?php echo url_for("/workspace/billings/spent") ?>">
	            <button type="submit" class="btn-icon-full btn-paymentdetail gtm-milestone-poster-view-payment-detail">
	                <span class="btn-label"><?php echo lang('milestone_view_payment_detail') ?></span>
	                <span class="btn-icon">
	                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                    </svg>
	                </span>
	            </button>
	        </form>
	        <form method="get" action="<?php echo url_for("/workspace/rating-centre") ?>">
		        <input type="hidden" name="review" value="<?php echo $task['task_id'] ?>" />
		        <input type="hidden" name="seeker" value="<?php echo $seeker['m_id'] ?>" />
	            <button type="submit" class="btn-icon-full btn-ratingctr gtm-milestone-poster-rating-centre">
	                <span class="btn-label"><?php echo lang('milestone_rating_centre') ?></span>
	                <span class="btn-icon">
	                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                    </svg>
	                </span>
	            </button>
	        </form>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-7-1">
        <div class="modal-footer modal-footer-milestone">
            <button type="button" class="btn-icon-full btn-close gtm-milestone-poster-close" data-dismiss="modal" data-orientation="next">
                <span class="btn-label"><?php echo lang('close') ?></span>
                <span class="btn-icon">
                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                    </svg>
                </span>
            </button>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-7-2">
        <div class="modal-footer modal-footer-milestone">
            <button type="button" class="btn-icon-full btn-close gtm-milestone-poster-close" data-dismiss="modal" data-orientation="next">
                <span class="btn-label"><?php echo lang('close') ?></span>
                <span class="btn-icon">
                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                    </svg>
                </span>
            </button>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-7-3">
        <div class="modal-footer modal-footer-milestone">
            <button type="button" class="btn-icon-full btn-close gtm-milestone-poster-close" data-dismiss="modal" data-orientation="next">
                <span class="btn-label"><?php echo lang('close') ?></span>
                <span class="btn-icon">
                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                    </svg>
                </span>
            </button>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-7-4">
        <div class="modal-footer modal-footer-milestone">
            <button type="button" class="btn-icon-full btn-close gtm-milestone-poster-close" data-dismiss="modal" data-orientation="next">
                <span class="btn-label"><?php echo lang('close') ?></span>
                <span class="btn-icon">
                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                    </svg>
                </span>
            </button>
        </div>
    </div>
</div>