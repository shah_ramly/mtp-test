<div id="<?php echo strtolower($task['id']) ?>" class="tbl-group-calendar-bar-row-wrapper applied-task-row
<?php echo str_replace('-', '', $task['apply_status']) ?>-task-row <?php echo $task['apply_status'] ?>-task-row- <?php echo $task['has_new_updates'] ? 'active-row' : ''?>">
    <div class="tbl-group-calendar-bar-flex">
        <div class="tbl-group-calendar-bar-row row-1">
            <button class="btn btn-link btn-arrow-bar collapsed" id="settings-button" aria-expanded="false"><i class="tim-icons icon-minimal-up"></i></button>
            <span class="tbl-group-calendar-bar-title"><?php echo $task['title'] ?></span>
        </div>
        <div class="tbl-group-calendar-bar-row row-2"><?php echo $cms->price($task['budget']) ?></div>
        <div class="tbl-group-calendar-bar-row row-3"></div>
        <div class="tbl-group-calendar-bar-row row-4">
            <ul class="tbl-applicant-list">
                <li>
                    <a href="#" class="avatar-link">
                        <div class="tbl-group-calendar-photo">
                            <img src="<?php echo imgCrop($task['user']['photo'], 35, 35, 'assets/img/default-avatar.png') ?>" alt="Profile Photo">
                        </div>
                    </a>
                </li>
            </ul>
            <div class="draft-more-opt dropdown">
                <a href="#" class="dropdown-toggle gtm-tc-calendar-triple-dot" data-toggle="dropdown" aria-expanded="false">
	                <span class="draft-more-icon">
	                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
	                        <circle cx="12" cy="12" r="1"></circle>
	                        <circle cx="12" cy="5" r="1"></circle>
	                        <circle cx="12" cy="19" r="1"></circle>
	                    </svg>
	                </span>
                </a>
                <?php $chat_status = $task['has_chats'] && is_array($task['has_chats']) && in_array($task['user_id'], $task['has_chats']) ? "true" : "false" ?>
                <ul class="dropdown-menu dropdown-navbar">
                    <li class="header-dropdown-link-list">
	                    <a href="<?php echo url_for("/workspace/task/{$task['slug']}/billing") ?>"
	                       class="nav-item dropdown-item gtm-tc-calendar-triple-dot-billing <?php echo ($task['is_accepted'] || $task['is_rejected'] || $task['is_disputed'] || $task['is_marked_completed'] ) ? 'active-list' : '' ?>">
                            <?php echo lang('task_billing_details') ?>
	                    </a>
                    </li>

	                <li class="header-dropdown-link-list">
		                <a href="#" class="nav-item dropdown-item gtm-tc-calendar-triple-dot-chat <?php echo ($task['has_new_chats']) ? 'active-list' : '' ?>"
		                   data-user-id="<?php echo $task['user_id'] === $current_user['id'] ? $task['assigned_to'] : $task['user_id'] ?>"
		                   data-task-id="<?php echo $task['id'] ?>"
		                   data-task-slug="<?php echo $task['slug'] ?>"
		                   data-chat-enabled="<?php echo ($section === 'created') ? 'true' : $chat_status ?>"
		                   data-toggle="modal" data-target="#modal_chat"><?php echo lang('task_chat'); ?></a>
	                </li>
                    <?php if( $section === 'awarded' && $task['apply_status'] === 'in-progress' ): ?>
		                <li class="header-dropdown-link-list">
			                <a href="<?php echo url_for("/workspace/task/{$task['slug']}/billing") ?>"
			                   class="nav-item dropdown-item gtm-tc-calendar-triple-dot-mark-complete "><?php echo lang('task_mark_as_complete'); ?></a>
		                </li>
                    <?php endif ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="collapse multi-collapse" id="inpro_left_applied_<?php echo $counter ?>">
        <div class="taskcentre-details taskcentre-details-applied">
            <div class="taskcentre-details-expanded-container">
                <div class="taskcentre-details-calendar-row row-1">
                    <div class="taskcentre-details-row row-taskdesc">
                        <label class="taskcentre-details-lbl lbl-taskdesc"><?php echo lang('task_description') ?></label>
                        <div class="taskcentre-details-val val-taskdesc">
                            <p class="taskcentre-details-para"><?php echo $task['description'] ?></p>
                        </div>
                    </div>
                    <div class="taskcentre-details-row row-taskcat">
                        <span class="taskcentre-details-val val-taskcat"><?php echo $task['category_name'] ?></span>
                        <span class="taskcentre-details-val val-taskcatsymbol">&gt;</span>
                        <span class="taskcentre-details-val val-tasksubcat"><?php echo $task['subcategory_name'] ?></span>
                    </div>
                    <div class="taskcentre-details-row row-taskloc">
                        <div class="task-details-title-components">
                            <span class="task-details-location-pills"><?php echo $task['location'] === 'remotely' ? lang('task_centre_remote') : lang('task_centre_on_site') ?></span>
                            <?php if($task['location'] !== 'remotely'): ?>
                            <span class="task-details-location">
                                <span class="task-details-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                    </svg></span>
                                <span class="task-details-location-val"><?php echo $task['state_country'] ?></span>
                            </span>
	                        <?php endif ?>
                        </div>
                    </div>
                </div>
                <div class="taskcentre-details-calendar-row row-2">
                    <div class="taskcentre-details-row row-taskcomp">
                        <div class="taskcentre-details-row row-downloadfiles">
                            <label class="taskcentre-details-lbl lbl-downloadfiles"><?php echo lang('task_attachment') ?></label>
                            <div class="taskcentre-details-val val-downloadfiles">
                                <?php if( !empty($attachments[$task['id']]) ): ?>
                                    <?php $count = 1 ?>
                                    <?php foreach($attachments[$task['id']] as $attachment): ?>
                                        <?php
                                        $file_name = explode('/', trim($attachment['name'], '/'));
                                        $file_name = end($file_name);
                                        if(strlen($file_name) > 30){
                                            $first = substr($file_name, 0, 14);
                                            $end = substr($file_name, -13);
                                            $short_name = "{$first}...{$end}";
                                        }
                                        ?>
			                            <span class="downloadfiles-wrapper">
		                                <a href="<?php echo url_for("workspace/{$task['slug']}/attachment/{$file_name}") ?>"
		                                   title="<?php echo $file_name ?>" class="downloadfiles-link gtm-tc-download-attachment">
		                                    <?php echo $count ?>
		                                </a>
		                            </span>
                                        <?php $file_name = $short_name = null; ?>
                                        <?php $count ++ ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="taskcentre-details-row row-posterdetail">
                        <div class="taskcentre-details-row row-taskaction">
	                        <form method="get" action="<?php echo url_for("/workspace/task/{$task['slug']}/details") ?>">
		                        <button type="submit" class="btn-icon-full btn-viewmore gtm-tc-view-more-btn" style="margin-bottom:10px">
			                        <span class="btn-label"><?php echo lang('task_centre_view_more') ?></span>
			                        <span class="btn-icon">
	                                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                                    </svg>
	                                </span>
		                        </button>
	                        </form>
                        </div>
                        <div class="taskcentre-details-viewmore">
                            <div class="task-row-taskid">
                                <span class="task-row-taskid-lbl"><?php echo lang('task_id') ?>:</span>
                                <span class="task-row-taskid-val"><?php echo $task['number'] ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
