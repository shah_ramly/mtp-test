<?php
$status = $task['status'] === 'completed' ? $task['status'] :
    (($task['status'] === 'published' && is_null($task['assigned'])) ?
        'pending' : 'active');

if( isset($time_plans) ){
    $task_from = \Carbon\Carbon::parse($task['start_by'])->startOfDay();
    $task_to   = \Carbon\Carbon::parse($task['complete_by'])->endOfDay(); //((int)$task['hours'] !== 0) ? $task_from->copy()->addHours((int)$task['hours']) :
    $task_hours = $total_hours = ($task['hours'] > 0) ? $task['hours'] : $task_to->diffInRealSeconds($task_from)/3600;
}else {
    $from = $task_from = \Carbon\Carbon::parse($task['start_by'])->startOfDay();
    $to = $task_to = ((int)$task['hours'] !== 0) ? $from->copy()->addHours((int)$task['hours']) : \Carbon\Carbon::parse($task['complete_by']);

    $length = ($hours >= 24) ?
        ($current_date->isSameDay($to) ? 24 - $from->hour :
            $current_date->isSameDay($from) ? $to->copy()->diffInMinutes($from->copy()->endOfDay())/60 : $to->copy()->diffInMinutes($from)/60) :
        ($current_date->isSameDay($to) ? $to->copy()->diffInMinutes($from->copy()->endOfDay())/60 :
            $from->copy()->diffInMinutes($from->copy()->endOfDay())/60);


    $length = $current_date->isSameDay($from) ?
        ( $from->format('a') === 'am' ? (12 + (abs(12 - $from->hour) - ($from->minute/60))) :
            (($from->hour <= 12) ? $from->hour : ( (12 - ($from->hour - 12)) - ($from->minute/60))) ) :
        (($current_date->copy()->diffInMinutes($to)/60) > 24 ? 24 : ($current_date->copy()->diffInMinutes($to)/60) );

    $task_hours = $hours = $total_hours = $task['hours'] > 0 ? $task['hours'] : $to->diffInRealSeconds($from) / 3600;
}

?>
<?php if( isset($time_plans) && !empty($time_plans) ): ?>
<div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-<?php echo $status ?> bar-row-<?php echo $counter ?>">
	<div class="tbl-group-calendar-gantt-bar-total-wrapper">
<?php foreach ($time_plans as $plan): ?>
<?php
    $from = $plan['start'];
    $to = $plan['end'];

    $start_day = $from->hour + ($from->minute/60);
    $length = $to->diffInMinutes($from)/60;
    $total_hours = $plan['hours'];
?>
        <div class="tbl-group-calendar-gantt-bar-total gtm-tc-calendar-bar"
             id="dropdownTotal"
             data-toggle="dropdown"
             aria-haspopup="true"
             aria-expanded="false"
             style="
		             width:<?php echo $length * 35.5 ?>px;
		             margin-left:<?php echo $current_date->isSameDay($from) ? $start_day * 35.5 : 0 ?>px;
		             height:29.19px;overflow:hidden"
             title="<?php echo $from->format("h:iA") . ' - ' . $to->format("h:iA") ?>"
        >
            <div class="tbl-group-calendar-gantt-bar-total-caption">
	            <?php if(( $length * 35.5) >= 127): ?>
                <?php echo $from->format("h:iA") . ' - ' . $to->format("h:iA") ?>
                <?php else: ?>
		            <span style="width:20px;height:29.19px;color:white;display:block;margin:-1px auto 0;">
			            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="CurrentColor"
			                 stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round">
				            <circle cx="12" cy="12" r="1"></circle>
				            <circle cx="19" cy="12" r="1"></circle>
				            <circle cx="5" cy="12" r="1"></circle>
                        </svg>
		            </span>
                <?php endif ?>
	            <!--Jan 3, 2020 - Jan 15, 2020-->
            </div>
        </div>
        <div class="dropdown-menu" aria-labelledby="dropdownTotal<?php echo $counter ?>">
            <div class="dropdown-menu-wrapper">
                <div class="dropdown-menu-top">
                    <div class="dropdown-date-calendar">
	                    <div class="dropdown-lbl-container dropdown-duration-lbl-container">
		                    <i class="tim-icons icon-calendar-60"></i>
		                    <span class="dropdown-duration-lbl"><?php echo lang('task_duration'); ?></span>
	                    </div>
                        <span class="date-calendar-txt dropdown-date-val">
	                        <!--3 Jan 2020 - 15 Jan 2020-->
	                        <?php echo $task_from->format("d M Y") . ' - ' . $task_to->format("d M Y") ?>
                        </span>
                    </div>
	                <div class="dropdown-time-calendar">
                        <?php if(!isset($section) || $section !== 'posted'): ?>
		                <div class="dropdown-lbl-container dropdown-time-lbl-container">
			                <i class="tim-icons icon-time-alarm"></i>
			                <span class="dropdown-duration-lbl"><?php echo lang('task_your_preferred_working_time'); ?></span>
		                </div>
		                <div class="dropdown-preferred-time-rows-container">
			                <?php foreach ($pref_times as $time): ?>
			                <div class="dropdown-preferred-time-row dropdown-date-val">
				                <span class="preferred-date"><?php echo $time['date']->format('d M Y') ?></span>
				                <span class="dash-seperator">-</span>
				                <span class="preferred-time-from"><?php echo $time['start']->format('h:iA') ?></span>
				                <span class="preferred-time-divider">to</span>
				                <span class="preferred-time-to"><?php echo $time['end']->format('h:iA') ?></span>
			                </div>
			                <?php endforeach; ?>
		                </div>
                        <?php endif ?>
		                <div class="dropdown-tbl-wrapper">
			                <div class="dropdown-tbl-flex dropdown-tbl-row-1">
				                <div class="dropdown-tbl-flex-lbl"><?php echo lang('task_total_hours'); ?></div>
				                <div class="dropdown-tbl-flex-val">
                                    <?php echo  number_format($total_hours, 1) .' '. ($total_hours === 1 ? lang('task_hour'): lang('task_hours')) ?>
				                </div>
			                </div>
			                <div class="dropdown-tbl-flex dropdown-tbl-row-2">
				                <div class="dropdown-tbl-flex-lbl"><?php echo lang('task_your_time_is_paying'); ?></div>
				                <div class="dropdown-tbl-flex-val">RM<?php echo number_format($task['budget']/$total_hours, 2) ?>/<?php echo lang('task_hour'); ?></div>
			                </div>
		                </div>
	                </div>
                </div>
                <?php if(!isset($section) || $section !== 'posted'): ?>
                <?php $task_hours = ($task['hours'] > 0) ? $task['hours'] : $task_to->diffInRealSeconds($task_from)/3600 ?>
	                <div class="dropdown-menu-btm">
		                <button class="dropdown-item-btm gtm-tc-calendar-bar-change-this-btn" type="button"
		                        data-toggle="modal"
		                        data-task-id="<?php echo $task['id'] ?>"
		                        data-task-hours="<?php echo number_format($task_hours) ?>"
		                        data-task-earning="<?php echo number_format($task['budget']/$task_hours, 2) ?>"
		                        data-task-start="<?php echo $task_from->toDateString() ?>"
		                        data-task-end="<?php echo $task_to->toDateString() ?>"
		                        data-target="#modal_task_planning"><?php echo lang('task_change_this'); ?></button>
	                </div>
	            <?php endif ?>
            </div>
        </div>

	    <div class="collapse multi-collapse" id="multiCollapse<?php echo $counter ?>">
	        <div class="bar-row-gantt-chart-height"></div>
	    </div>
<?php endforeach; ?>
	</div>
</div>
<?php else: ?>
	<?php if( $section === 'awarded' ): ?>
	<div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-<?php echo $status ?> bar-row-<?php echo $counter ?>">
		<div class="tbl-group-calendar-gantt-bar-total-wrapper empty-bar"></div>
		<div class="collapse multi-collapse" id="multiCollapse<?php echo $counter ?>">
			<div class="bar-row-gantt-chart-height"></div>
		</div>
	</div>
	<?php else: ?>
	<div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-<?php echo $status ?> bar-row-<?php echo $counter ?>">
		<div class="tbl-group-calendar-gantt-bar-total-wrapper">
			<div class="tbl-group-calendar-gantt-bar-total gtm-tc-calendar-bar"
			     id="dropdownTotal"
			     data-toggle="dropdown"
			     aria-haspopup="true"
			     aria-expanded="false"
			     style="
					     width:<?php echo $length * 35.5 ?>px;
					     margin-left:<?php echo $current_date->isSameDay($from) ? $start_day * 35.5 : 0 ?>px;
					     height:29.19px;overflow:hidden"
			     title="<?php echo $from->format("h:iA") . ' - ' . $to->format("h:iA") ?>"
			>
				<div class="tbl-group-calendar-gantt-bar-total-caption">
                    <?php if(( $length * 35.5) >= 127): ?>
                        <?php echo $from->format("h:iA") . ' - ' . $to->format("h:iA") ?>
                    <?php else: ?>
	                    <span style="width:20px;height:29.19px;color:white;display:block;margin:-1px auto 0;">
			            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="CurrentColor"
			                 stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round">
				            <circle cx="12" cy="12" r="1"></circle>
				            <circle cx="19" cy="12" r="1"></circle>
				            <circle cx="5" cy="12" r="1"></circle>
                        </svg>
		            </span>
                    <?php endif ?>
					<!--Jan 3, 2020 - Jan 15, 2020-->
				</div>
			</div>
			<div class="dropdown-menu" aria-labelledby="dropdownTotal<?php echo $counter ?>">
				<div class="dropdown-menu-wrapper">
					<div class="dropdown-menu-top">
						<div class="dropdown-date-calendar">
							<div class="dropdown-lbl-container dropdown-duration-lbl-container">
								<i class="tim-icons icon-calendar-60"></i>
								<span class="dropdown-duration-lbl"><?php echo lang('task_duration'); ?></span>
							</div>
							<span class="date-calendar-txt dropdown-date-val">
	                        <!--3 Jan 2020 - 15 Jan 2020-->
	                        <?php echo $from->format("d M Y") . ' - ' . $to->format("d M Y") ?>
                        </span>
						</div>
						<div class="dropdown-time-calendar">
                            <?php if(!isset($section) || $section !== 'posted'): ?>
							<div class="dropdown-lbl-container dropdown-time-lbl-container">
								<i class="tim-icons icon-time-alarm"></i>
								<span class="dropdown-duration-lbl"><?php echo lang('task_your_preferred_working_time'); ?></span>
							</div>
							<div class="dropdown-preferred-time-rows-container">
                                <?php foreach ($pref_times as $time): ?>
                                <?php $total_hours = $time['hours'] ?>
									<div class="dropdown-preferred-time-row dropdown-date-val">
										<span class="preferred-date"><?php echo $time['date']->format('d M Y') ?></span>
										<span class="dash-seperator">-</span>
										<span class="preferred-time-from"><?php echo $time['start']->format('h:iA') ?></span>
										<span class="preferred-time-divider"><?php echo lang('task_to'); ?></span>
										<span class="preferred-time-to"><?php echo $time['end']->format('h:iA') ?></span>
									</div>
                                <?php endforeach; ?>
							</div>
                            <?php endif ?>
							<div class="dropdown-tbl-wrapper">
								<div class="dropdown-tbl-flex dropdown-tbl-row-1">
									<div class="dropdown-tbl-flex-lbl"><?php echo lang('task_total_hours'); ?></div>
									<div class="dropdown-tbl-flex-val">
                                        <?php echo  number_format($total_hours, 1) .' '. ($total_hours === 1 ? lang('task_hour'): lang('task_hours')) ?>
									</div>
								</div>
								<div class="dropdown-tbl-flex dropdown-tbl-row-2">
									<div class="dropdown-tbl-flex-lbl">
                                        <?php echo (!isset($section) || $section !== 'posted') ? lang('task_your_time_is_paying') : lang('task_you_are_paying')  ?>
									</div>
									<div class="dropdown-tbl-flex-val">RM<?php echo number_format($task['budget']/$total_hours, 2) ?>/<?php echo lang('task_hour'); ?></div>
								</div>
							</div>
						</div>
					</div>
                    <?php if(!isset($section) || $section !== 'posted'): ?>
			        <?php $task_hours = ($task['hours'] > 0) ? $task['hours'] : $task_to->diffInRealSeconds($task_from)/3600 ?>
					<div class="dropdown-menu-btm">
						<button class="dropdown-item-btm gtm-tc-calendar-bar-change-this-btn" type="button"
						        data-toggle="modal"
						        data-task-id="<?php echo $task['id'] ?>"
						        data-task-hours="<?php echo number_format($task_hours) ?>"
						        data-task-earning="<?php echo number_format($task['budget']/$task_hours, 2) ?>"
						        data-task-start="<?php echo $task_from->toDateString() ?>"
						        data-task-end="<?php echo $task_to->toDateString() ?>"
						        data-target="#modal_task_planning"><?php echo lang('task_change_this'); ?></button>
					</div>
			        <?php endif ?>
				</div>
			</div>
		</div>
		<div class="collapse multi-collapse" id="multiCollapse<?php echo $counter ?>">
			<div class="bar-row-gantt-chart-height"></div>
		</div>
	</div>
	<?php endif ?>
<?php endif ?>
