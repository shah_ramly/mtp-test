<?php
$date                 = \Carbon\Carbon::parse($data['current_date']);
$applied_in_progress  = array_filter($tasks['applied'], function ($task) use ($current_user) { return $task['assigned_to'] === $current_user['id'] && in_array($task['status'], ['in-progress', 'marked-completed']); });
$assigned_in_progress = array_filter($tasks['assigned'], function ($task) { return in_array($task['status'], ['in-progress', 'marked-completed']); });
if( !isset($time_plans) ) $time_plans = [];
$applied_in_progress  = array_filter($applied_in_progress, function ($task) use ($date, $time_plans) {
    return $date->between(\Carbon\Carbon::parse($task['assigned'])->startOfDay(), \Carbon\Carbon::parse($task['complete_by'])->endOfDay()) /*&& isset($time_plans[$task['id']])*/;
});
$assigned_in_progress = array_filter($assigned_in_progress, function ($task) use ($date) {
	return $date->between(\Carbon\Carbon::parse($task['assigned'])->startOfDay(), \Carbon\Carbon::parse($task['complete_by'])->endOfDay());
});
if( isset($time_plans) && empty($time_plans) ) unset($time_plans);
?>
<div class="tab-pane tc-InProgress tc-InProgress-Day fade <?php echo isset($current_tab) && $current_tab === '#tcInProgressDay' ? 'show active' : '' ?>" id="tcInProgressDay" role="tabpanel" aria-labelledby="tcInProgressDay-tab">
    <div class="row">
        <div id="colLeftCard" class="col-sm-6 col-left-card-flex">
            <div class="tbl-header-calendar tbl-header-flex">
                <div class="tbl-header-row row-1"><?php echo lang('task_title'); ?></div>
                <div class="tbl-header-row row-2"><?php echo lang('task_value'); ?>
                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('task_value_description'); ?>">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                        </svg>
                    </span>
                </div>
                <div class="tbl-header-row row-3"><?php echo lang('task_duration'); ?>
                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('task_duration_description'); ?>">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                        </svg>
                    </span>
                </div>
                <div class="tbl-header-row row-4"><?php /*echo lang('task_pix'); */?>
                    <!--span class="bi-tooltip" data-toggle="tooltip" data-placement="bottom" title="Person in Charge. The owner or the person assigned to complete the task">
                        <svg class="bi bi-question-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                            <path d="M5.25 6.033h1.32c0-.781.458-1.384 1.36-1.384.685 0 1.313.343 1.313 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.007.463h1.307v-.355c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.326 0-2.786.647-2.754 2.533zm1.562 5.516c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z" />
                        </svg>
                    </span-->
                </div>
            </div>
            <div class="tbl-body-calendar">
                <div class="tbl-group-calendar-row tbl-group-calendar-awarded-row">
                    <div class="tbl-group-calendar-title"><?php echo lang('task_view_awarded_tasks'); ?></div>
                    <div class="tbl-group-calendar-bar-wrapper">
                        <?php if(!empty($applied_in_progress)): ?>
                            <?php $counter = 1; ?>
                            <?php foreach($applied_in_progress as $task): ?>
                                <?php echo partial('workspace/partial/task-centre/in-progress-task-row.html.php',
			                                        [
                                                        'task'    => $task,
                                                        'counter' => $counter,
                                                        'section' => 'awarded'
			                                        ]
							                      ); ?>
                                <?php $counter++; ?>
                            <?php endforeach; ?>
                        <?php else: ?>
	                        <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
		                        <div class="tbl-group-calendar-bar-flex">
			                        <div class="tbl-group-calendar-bar-row row-1">
				                        <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_applied_task'); ?></span>
			                        </div>
		                        </div>
	                        </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="tbl-group-calendar-row tbl-group-calendar-created-row">
                    <div class="tbl-group-calendar-title"><?php echo lang('task_view_posted_task'); ?></div>
                    <div class="tbl-group-calendar-bar-wrapper">
                        <?php if(!empty($assigned_in_progress)): ?>
                            <?php if(!isset($counter)) $counter = 1; ?>
                            <?php foreach($assigned_in_progress as $task): ?>
                                <?php echo partial('workspace/partial/task-centre/in-progress-task-row.html.php', ['task' => $task, 'counter' => $counter, 'section' => 'created']); ?>
                                <?php $counter++; ?>
                            <?php endforeach; ?>
                        <?php else: ?>
	                        <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
		                        <div class="tbl-group-calendar-bar-flex">
			                        <div class="tbl-group-calendar-bar-row row-1">
				                        <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_posted_task'); ?></span>
			                        </div>
		                        </div>
	                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="colRightCard" class="col-sm-6 col-right-card-flex card-calendar-day">
            <div class="tbl-header-calendar-gantt-qtr">
	            <a href="<?php echo $data['prev_day'] ?>" class="r1-icon r1-icon-left">
		            <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			            <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/>
		            </svg>
	            </a>
	            <div class="tbl-header-calendar-gantt-qtr-week-row qtr-1"> <?php echo $data['current_date'] ?></div>
	            <a href="<?php echo $data['next_day'] ?>" class="r1-icon r1-icon-left">
		            <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
		            </svg>
	            </a>
            </div>
            <div class="tbl-header-calendar-gantt-body-overflow-wrapper dragscroll test">
                <div class="tbl-header-calendar-gantt-body-overflow">
                    <div class="tbl-header-calendar-gantt-body-overflow-container">
                        <div class="tbl-header-calendar-gantt-wrapper">
                            <div class="tbl-header-calendar-gantt-month">
                                <div class="calendar-gantt-week-container">
                                    <div class="tbl-header-calendar-gantt-time-row">AM</div>
                                    <div class="tbl-header-calendar-gantt-day">
                                        <div class="tbl-header-calendar-gantt-time-row-day day-1">12</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-2">1</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-3">2</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-4">3</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-5">4</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-6">5</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-7">6</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-2">7</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-3">8</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-4">9</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-5">10</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-6">11</div>
                                    </div>
                                </div>
                                <div class="calendar-gantt-week-container">
                                    <div class="tbl-header-calendar-gantt-time-row">PM</div>
                                    <div class="tbl-header-calendar-gantt-day">
                                        <div class="tbl-header-calendar-gantt-time-row-day day-1">12</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-2">1</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-3">2</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-4">3</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-5">4</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-6">5</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-7">6</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-2">7</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-3">8</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-4">9</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-5">10</div>
                                        <div class="tbl-header-calendar-gantt-time-row-day day-6">11</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tbl-body-calendar-gantt">
                            <div class="tbl-group-calendar-gantt-row tbl-group-calendar-awarded-row">
                                <div class="tbl-group-calendar-gantt-grid-wrapper">
                                    <div class="tbl-group-calendar-gantt-grid">
                                        <div class="gantt-grid-time-day-row gantt-grid-row-1"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-2"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-3"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-4"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-5"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-6"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-7"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-8"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-9"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-10"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-11"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-12"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-13"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-14"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-15"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-16"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-17"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-18"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-19"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-20"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-21"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-22"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-23"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-24"></div>
                                    </div>
                                </div>
                                <div class="tbl-group-calendar-gantt-bar-wrapper">
                                    <?php if(!empty($applied_in_progress)): ?>
                                        <?php if(!isset($counter)) $counter = 1; ?>
                                        <?php foreach($applied_in_progress as $task): ?>
                                            <?php
                                            $created_at = \Carbon\Carbon::parse($task['created_at']);
                                            $start_at = \Carbon\Carbon::parse($task['start_by']);
                                            $complete_by = \Carbon\Carbon::parse($task['complete_by']);
                                            $assigned_at = \Carbon\Carbon::parse($task['assigned']);
                                            $current_date = \Carbon\Carbon::parse($data['current_date']);
                                            $complete_by = $complete_by->copy()->endOfDay();
                                            $start_day = $assigned_at->hour + ($assigned_at->minute/\Carbon\Carbon::MINUTES_PER_HOUR);
                                            $hours = $task['type'] === 'by-hour' ? (int)$task['hours'] : $complete_by->copy()->diffInMinutes($assigned_at)/60; ?>
	                                        <?php set('section', 'awarded') ?>
                                            <?php echo partial('workspace/partial/task-centre/day-calendar-task-bar.html.php',
                                                [
                                                    'task'         => $task,
                                                    'counter'      => $counter,
                                                    'start_day'    => $start_day,
                                                    'hours'        => $hours,
                                                    'current_date' => $current_date,
                                                    'time_plans' => $time_plans[$task['id']] ?? '',
                                                    'pref_times' => $preferred_times[$task['id']] ?? '',
                                                ]) ?>
                                            <?php $counter++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
	                                    <div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-completed bar-row-1">
		                                    <div class="tbl-group-calendar-gantt-bar-total-wrapper empty-bar"></div>
	                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="tbl-group-calendar-gantt-row tbl-group-calendar-created-row">
                                <div class="tbl-group-calendar-gantt-grid-wrapper">
                                    <div class="tbl-group-calendar-gantt-grid">
                                        <div class="gantt-grid-time-day-row gantt-grid-row-1"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-2"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-3"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-4"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-5"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-6"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-7"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-8"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-9"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-10"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-11"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-12"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-13"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-14"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-15"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-16"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-17"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-18"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-19"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-20"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-21"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-22"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-23"></div>
                                        <div class="gantt-grid-time-day-row gantt-grid-row-24"></div>
                                    </div>
                                </div>
                                <div class="tbl-group-calendar-gantt-bar-wrapper">
                                    <?php if(!empty($assigned_in_progress)): ?>
                                        <?php if(!isset($counter)) $counter = 1; ?>
                                        <?php foreach($assigned_in_progress as $task): ?>
                                            <?php
                                            $created_at = \Carbon\Carbon::parse($task['created_at']);
                                            $start_at = \Carbon\Carbon::parse($task['start_by']);
                                            $complete_by = \Carbon\Carbon::parse($task['complete_by']);
                                            $assigned_at = \Carbon\Carbon::parse($task['assigned']);
                                            $current_date = \Carbon\Carbon::parse($data['current_date']);
                                            $complete_by = $complete_by->copy()->endOfDay();
                                            $start_day = $assigned_at->hour + ($assigned_at->minute/\Carbon\Carbon::MINUTES_PER_HOUR);
                                            $hours = $task['type'] === 'by-hour' ? (int)$task['hours'] : $complete_by->copy()->diffInMinutes($assigned_at)/60; ?>
                                            <?php set('section', 'posted') ?>
                                            <?php echo partial('workspace/partial/task-centre/day-calendar-task-bar.html.php',
                                                [
                                                    'task'         => $task,
                                                    'counter'      => $counter,
                                                    'start_day'    => $start_day,
                                                    'hours'        => $hours,
                                                    'current_date' => $current_date,
                                                    'time_plans'   => null,
                                                    'pref_times'   => null,
                                                ]) ?>
                                            <?php $counter++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
	                                    <div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-completed bar-row-1">
		                                    <div class="tbl-group-calendar-gantt-bar-total-wrapper empty-bar"></div>
	                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php content_for('modals') ?>
<?php echo partial('/workspace/partial/task-centre/task-planning-modal.html.php') ?>
<?php end_content_for() ?>
