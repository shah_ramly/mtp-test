<div id="table-listing-container-talent" class="table-listing-container applicants-list">
    <table class="table">
        <thead>
        <tr>
	        <th class="info"><?php echo lang('task_applicant_talent_name') ?></th>
	        <?php if( !isset($calendar) ): ?>
	        <th class="info"><?php echo lang('task_applicant_review') ?></th>
	        <th class="info"><?php echo lang('task_applicant_task_history') ?></th>
	        <?php endif ?>
	        <th></th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($applicants as $applicant): ?>
            <?php if((string)$applicant['task_id'] !== (string)$task['id']) continue; ?>
            <?php echo partial('workspace/partial/task-centre/applicant-row.html.php',
                [
                    'applicant' => $applicant,
                    'task' => $task,
                    'selected' => array_key_exists($task['id'], $selected_applicants) && in_array($applicant['user_id'], array_column($selected_applicants[$task['id']], 'seeker_id')),
                    'action' => array_values(array_filter($actions, function($action)use($applicant){ return $action['by_id'] === $applicant['user_id']; })),
                    'payment' => array_values(array_filter($payments, function($payment)use($applicant){ return $payment['assigned_to'] === $applicant['user_id']; })),
                    'calendar' => isset($calendar)
                ]); ?>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php if( $count > 10 ): ?>
<?php echo partial('/workspace/partial/task-centre/applicants-pagination.html.php',
	[
		'count' => $count,
		'task'  => $task,
		'current_page'  => $page,
	]) ?>
<?php endif ?>