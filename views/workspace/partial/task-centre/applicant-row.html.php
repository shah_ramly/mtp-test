<?php
$status_map = [
    'appointed'   => ['class' => '1', 'id' => '1'],
    'confirmed'   => ['class' => '2', 'id' => '2'],
    'waiting'     => ['class' => '3', 'id' => '3a'],
    'in-progress' => ['class' => '3', 'id' => '3b'],
    'in-progress-end' => ['class' => '3', 'id' => '3c'],
    'completed'   => ['class' => '4', 'id' => '7'],
    'payment'     => ['class' => '5', 'id' => '5a'],
    'dispute'    => ['class' => '5', 'id' => '5b'],
    'rate'        => ['class' => '5', 'id' => '5a'],
    'closed'      => ['class' => '7', 'id' => '7-1'],
    'declined'    => ['class' => '7', 'id' => '7-2'],
    'cancelled'   => ['class' => '7', 'id' => '7-2'],
    'unfulfilled' => ['class' => '7', 'id' => '7-3'],
    'disputed'    => ['class' => '7', 'id' => '7-4'],
];

$task_applicants = array_values(array_filter($tasks['applicants'], function($applicant) use($task){ return $applicant['task_id'] === $task['id']; }));
$not_selected_applicants = array_filter($task_applicants, function($unapplicant) use($selected_applicants, $task, $applicant){
	return ! in_array($unapplicant['user_id'], @array_keys($selected_applicants[$task['id']])) &&
		$applicant['user_id'] === $unapplicant['user_id'] &&
		\Carbon\Carbon::parse($task['complete_by'])->lte( \Carbon\Carbon::now() );
});

$not_selected_applicants = array_values($not_selected_applicants);

if(in_array($task['id'], array_column($not_selected_applicants, 'task_id')) && in_array($applicant['user_id'], array_column($not_selected_applicants, 'user_id'))){
	if($not_selected_applicants[0]['status'] !== 'declined') Task::init($user->db, $user)->set_seeker_declined($task['id'], $applicant['user_id']);
	$applicant_task_status = 'declined';
}else {

    $applicant_task_status = isset($selected_applicants[$task['id']], $selected_applicants[$task['id']][$applicant['user_id']]) ?
        $selected_applicants[$task['id']][$applicant['user_id']]['status'] : $applicant['status'];

    if (isset($selected_applicants[$task['id']][$applicant['user_id']]) && $selected_applicants[$task['id']][$applicant['user_id']]['status'] === 'in-progress') { //!\Carbon\Carbon::parse($task['complete_by'])->isFuture() &&
        $applicant_task_status = 'in-progress-end';
    }

    if (isset($selected_applicants[$task['id']][$applicant['user_id']])) {

        if ($selected_applicants[$task['id']][$applicant['user_id']]['poster_status'] === 'completed' && $selected_applicants[$task['id']][$applicant['user_id']]['seeker_status'] === 'in-progress') {
            $status_map['completed']['id'] = '4';
        }
    }
}
?>
<tr>
	<td class="info <?php echo $applicant['viewed'] === 'N' && $task['status'] !== 'closed' ? 'new-applicant' : '' ?> <?php echo ($task['status'] !== 'closed') && (!empty($action) || $applicant['viewed'] === 'N' || $applicant['has_new_updates']) ? 'new-active' : '' ?>">
		<a href="#" class="applicant-link gtm-tc-applicant-list-avatar" data-toggle="modal"
		   data-id="<?php echo $applicant['user_id'] ?>"
		   data-task-id="<?php echo $task['id'] ?>"
		   data-task-status="<?php echo in_array($task['status'], ['published', 'in-progress']) ?>"
		   data-target="#modal_talent_details">
			<div class="tbl-group-calendar-photo">
				<img src="<?php echo imgCrop($applicant['photo'], 35, 35, 'assets/img/default-avatar.png') ?>" alt="Profile Photo">
			</div>
			<span class="applicant-name"><?php echo "{$applicant['firstname']} {$applicant['lastname']}" ?></span>
		</a>
	</td>
	<?php if( !isset($calendar) || !$calendar ): ?>
	<td class="info">
		<div class="task-row-rating-flex">
			<div class="task-row-rating-star">
                <?php $starred = (int)floor($applicant['rating']['overall']/2); $rest = 5 - $starred; ?>
                <?php if($starred > 0): ?>
                    <?php foreach (range(1, $starred) as $star): ?>
						<span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php if($rest > 0): ?>
                    <?php foreach (range(1, $rest) as $star1): ?>
						<span class="rating-star-icon"><i class="fa fa-star"></i></span>
                    <?php endforeach; ?>
                <?php endif; ?>
			</div>
			<div class="task-row-rating-val">
				<span class="task-row-average-rating-val">
					<?php echo number_format((float)$applicant['rating']['overall'], 1); ?>
				</span>
				<span class="task-row-divider">/</span>
				<span class="task-row-total-rating-val">
					<?php echo $applicant['rating']['count'] . " " . ( $applicant['rating']['count'] == 1 ? lang('task_applicant_review') : lang('task_applicant_reviews')); ?>
				</span>
			</div>
		</div>
	</td>
	<td class="info col-taskcounthour">
		<div class="taskcounthour-flex">
            <span class="col-taskcount">
                <span class="col-taskcount-icon bi-tooltip" data-toggle="tooltip" data-placement="auto"
                      data-template="<div class='tooltip tooltip-fancy tooltip-tc-date' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>"
                      data-title="Completed Task" data-original-title="" title="">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-card-checklist" viewBox="0 0 16 16">
                        <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z" />
                        <path d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0zM7 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z" />
                    </svg>
                </span>
                <span class="col-taskcount-val"><?php echo (int)$applicant['completed'] ?></span>
            </span>
			<span class="col-taskhours">
                <span class="col-taskhours-icon bi-tooltip" data-toggle="tooltip" data-placement="auto"
                      data-template="<div class='tooltip tooltip-fancy tooltip-tc-date' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>"
                      data-title="Total Hours Completed" data-original-title="" title="">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-clock" viewBox="0 0 16 16">
                        <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z" />
                        <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z" />
                    </svg>
                </span>
                <span class="col-taskhours-val"><?php echo (int)$applicant['hours_completed'] ?></span>
            </span>
		</div>
	</td>
	<?php endif ?>
	<td class="col-milestone">
		<div class="col-applicant-actions-flex">
			<div class="col-applicant-pin">

				<?php $pinned = $selected ? (bool)$selected_applicants[$task['id']][$applicant['user_id']]['pinned'] : (bool) $applicant['pinned'] ?>
				<a href="#" class="applicant-pin-link gtm-tc-applicant-list-pin" title="<?php echo $pinned ? 'Unpin':'Pin' ?>"
				   data-status="<?php echo $selected ? 'true' : 'false' ?>"
				   data-seeker-id="<?php echo $applicant['user_id'] ?>"
				   data-task-id="<?php echo $task['id'] ?>"
				>
                    <span class="applicant-pin-icon">
	                    <?php if( $pinned ): ?>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pin-angle-fill" viewBox="0 0 16 16">
                            <path d="M9.828.722a.5.5 0 0 1 .354.146l4.95 4.95a.5.5 0 0 1 0 .707c-.48.48-1.072.588-1.503.588-.177 0-.335-.018-.46-.039l-3.134 3.134a5.927 5.927 0 0 1 .16 1.013c.046.702-.032 1.687-.72 2.375a.5.5 0 0 1-.707 0l-2.829-2.828-3.182 3.182c-.195.195-1.219.902-1.414.707-.195-.195.512-1.22.707-1.414l3.182-3.182-2.828-2.829a.5.5 0 0 1 0-.707c.688-.688 1.673-.767 2.375-.72a5.92 5.92 0 0 1 1.013.16l3.134-3.133a2.772 2.772 0 0 1-.04-.461c0-.43.108-1.022.589-1.503a.5.5 0 0 1 .353-.146z" />
                        </svg>
	                    <?php else: ?>
	                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-pin-angle" viewBox="0 0 16 16">
                            <path d="M9.828.722a.5.5 0 0 1 .354.146l4.95 4.95a.5.5 0 0 1 0 .707c-.48.48-1.072.588-1.503.588-.177 0-.335-.018-.46-.039l-3.134 3.134a5.927 5.927 0 0 1 .16 1.013c.046.702-.032 1.687-.72 2.375a.5.5 0 0 1-.707 0l-2.829-2.828-3.182 3.182c-.195.195-1.219.902-1.414.707-.195-.195.512-1.22.707-1.414l3.182-3.182-2.828-2.829a.5.5 0 0 1 0-.707c.688-.688 1.673-.767 2.375-.72a5.92 5.92 0 0 1 1.013.16l3.134-3.133a2.772 2.772 0 0 1-.04-.461c0-.43.108-1.022.589-1.503a.5.5 0 0 1 .353-.146zm.122 2.112v-.002zm0-.002v.002a.5.5 0 0 1-.122.51L6.293 6.878a.5.5 0 0 1-.511.12H5.78l-.014-.004a4.507 4.507 0 0 0-.288-.076 4.922 4.922 0 0 0-.765-.116c-.422-.028-.836.008-1.175.15l5.51 5.509c.141-.34.177-.753.149-1.175a4.924 4.924 0 0 0-.192-1.054l-.004-.013v-.001a.5.5 0 0 1 .12-.512l3.536-3.535a.5.5 0 0 1 .532-.115l.096.022c.087.017.208.034.344.034.114 0 .23-.011.343-.04L9.927 2.028c-.029.113-.04.23-.04.343a1.779 1.779 0 0 0 .062.46z"></path>
                        </svg>
	                    <?php endif ?>
                    </span>
				</a>
			</div>
			<div class="col-applicant-answer" title="View Answer">
				<a href="#" class="applicant-answer-link gtm-tc-applicant-list-qna <?php echo empty($task['questions']) ? 'disabled' : '' ?>" title="View Answer"
				   data-toggle="modal"
				   data-id="<?php echo $applicant['user_id'] ?>"
				   data-task-id="<?php echo $task['id'] ?>"
				   data-target="#modal_view_answer">
                    <span class="applicant-answer-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-clipboard-check" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M10.854 7.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 9.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
                            <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z" />
                            <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z" />
                        </svg>
                    </span>
				</a>
			</div>
			<div class="col-applicant-chat">
				<a class="chatButton gtm-tc-applicant-list-chat" href="#"
				   data-toggle="modal"
				   data-user-id="<?php echo $applicant['user_id'] ?>"
				   data-target="#modal_chat"
				   data-task-id="<?php echo $task['id'] ?>"
				   data-task-slug="<?php echo $task['slug'] ?>"
				   data-task-closed="<?php echo (in_array($task['status'], ['cancelled', 'closed', 'disputed']) && \Carbon\Carbon::parse($task['updated_at'])->addDays(5)->isBefore(\Carbon\Carbon::now())) ? 'true' : 'false' ?>"
				   data-task-closed-date="<?php echo in_array($task['status'], ['cancelled', 'closed', 'disputed']) ? \Carbon\Carbon::parse($task['updated_at'])->addDays(5)->format("d F Y - h.ia") : 'false' ?>"
				   data-chat-enabled="true">
                    <span class="applicant-chat-icon <?php echo (!empty($task['has_new_chats']) && in_array((int)$applicant['user_id'], $task['has_new_chats'])) ? 'active' : '' ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-chat" viewBox="0 0 16 16">
                            <path d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z" />
                        </svg>
                    </span>
				</a>
			</div>
			<div class="col-applicant-hiremilestone">
				<?php if( ($selected && empty($action)) || in_array($applicant['user_id'], array_column($not_selected_applicants, 'user_id')) ): ?>
				<a href="#" data-toggle="modal"
				   data-target="#modal_milestone_poster"
				   class="applicant-progress-link gtm-tc-applicant-list-milestone-bar"
				   data-name="<?php echo "{$applicant['firstname']} {$applicant['lastname']}" ?>"
				   data-task-id="<?php echo $task['id'] ?>"
				   data-seeker-id="<?php echo $applicant['user_id'] ?>"
				   data-payment-status="<?php echo isset($payment[0]) && $payment[0]['status'] === '999' ?>"
				   data-task-value="<?php echo "RM" . number_format($task['budget'], 2) ?>"
				   data-task-total="<?php echo "RM" . number_format((int)$task['budget']+4, 2) ?>"
				   data-start-date="<?php echo Carbon\Carbon::parse($task['start_by'])->formatLocalized('%d %B %Y') ?>"
				   <?php if( in_array($applicant_task_status, ['cancelled', 'declined', 'rejected', 'disputed']) ): ?>
				   data-task-state="<?php echo $applicant_task_status ?>"
				   <?php endif ?>
				   data-id="poster-progress-<?php echo $status_map[$applicant_task_status]['id'] ?>">
                    <span class="applicant-progress-flex">
                        <span class="applicant-milestone-indicator-block">
                            <span class="applicant-milestone-indicator-val-flex">
	                            <?php $temp_status = $applicant_task_status === 'unfulfilled' ? 'cancelled' : $applicant_task_status ?>
	                            <?php $temp_status = $applicant_task_status === 'in-progress-end' ? rtrim($temp_status, "-end") : $applicant_task_status ?>
                                <span class="applicant-milestone-indicator-val"><?php echo ucwords(str_replace('-', ' ', $temp_status)) ?></span>
                                <span class="open-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-box-arrow-up-right" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"></path>
                                        <path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"></path>
                                    </svg>
                                </span>
                            </span>
                            <span class="applicant-milestone-indicator progress-<?php echo $status_map[$applicant_task_status]['class'] ?>"></span>
                        </span>
                    </span>
				</a>
				<?php elseif( !empty($action) || (isset($action[0]) && !empty($payment) && $action[0]['required_action'] === 'confirmed') ): ?>
				<a href="#" data-toggle="modal"
				   data-target="#modal_milestone_poster"
				   class="applicant-progress-link gtm-tc-applicant-list-milestone-action-required"
				   data-name="<?php echo "{$applicant['firstname']} {$applicant['lastname']}" ?>"
				   data-task-id="<?php echo $task['id'] ?>"
				   data-seeker-id="<?php echo $applicant['user_id'] ?>"
				   data-payment-status="<?php echo isset($payment[0]) && $payment[0]['status'] === '999' ?>"
				   data-task-value="<?php echo "RM" . number_format($task['budget'], 2) ?>"
				   data-task-total="<?php echo "RM" . number_format((int)$task['budget']+4, 2) ?>"
				   data-start-date="<?php echo Carbon\Carbon::parse($task['start_by'])->formatLocalized('%d %B %Y') ?>"
                    <?php if( in_array($applicant_task_status, ['cancelled', 'declined', 'rejected', 'disputed']) ): ?>
						data-task-state="<?php echo $applicant_task_status ?>"
                    <?php endif ?>
                   data-id="poster-progress-<?php echo $status_map[$action[0]['required_action']]['id'] ?>"
				>
                    <span class="applicant-progress-flex">
                        <span class="applicant-milestone-indicator-block">
                            <span class="applicant-milestone-indicator-val-flex">
                                <span class="milestone-action-required-txt progress-4"><?php echo lang('milestone_action_required') ?></span>
                                <span class="milestone-action-required">&#62;</span>
                            </span>
                        </span>
                    </span>
				</a>
				<?php else: ?>
				<a href="#" class="applicant-hire-link gtm-tc-applicant-list-milestone-published" data-toggle="modal"
				   data-id="<?php echo $applicant['user_id'] ?>"
				   data-task-id="<?php echo $task['id'] ?>"
				   data-task-status="<?php echo in_array($task['status'], ['published', 'in-progress']) ?>"
				   data-hire-link="true"
				   data-target="#modal_confirm_talent">
					<span class="applicant-hiremilestone-lbl"><?php echo lang('milestone_hire_this_candidate') ?></span>
					<span class="open-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-box-arrow-up-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"></path>
                            <path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"></path>
                        </svg>
                    </span>
				</a>
				<?php endif ?>
			</div>
		</div>
	</td>
</tr>