<div class="modal-body">
    <div class="milestone-steps-container">
        <div class="milestone-indicator-container">
            <div class="milestone-step milestone-step-1">
                <span class="milestone-complete-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                    </svg>
                </span>
                <div class="milestone-label"><?php echo lang('milestone_appointed'); ?></div>
                <div class="milestone-icon"></div>
            </div>
            <div class="milestone-line milestone-line-1"></div>
            <div class="milestone-step milestone-step-2">
                <span class="milestone-complete-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                    </svg>
                </span>
                <div class="milestone-label"><?php echo lang('milestone_confirmed'); ?></div>
                <div class="milestone-icon"></div>
            </div>
            <div class="milestone-line milestone-line-2">
            </div>
            <div class="milestone-step milestone-step-3">
                <span class="milestone-complete-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                    </svg>
                </span>
                <div class="milestone-label milestone-label-waiting"><?php echo lang('milestone_waiting'); ?></div>
                <div class="milestone-label milestone-label-inprogress"><?php echo lang('milestone_in_progress'); ?></div>
                <div class="milestone-icon in-progress"></div>
            </div>
            <div class="milestone-line milestone-line-3"></div>
            <div class="milestone-step milestone-step-4">
                <span class="milestone-complete-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                    </svg>
                </span>
                <div class="milestone-label"><?php echo lang('milestone_completed'); ?></div>
                <div class="milestone-icon"></div>
            </div>
            <div class="milestone-line milestone-line-4"></div>
            <div class="milestone-step milestone-step-5">
                <span class="milestone-complete-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                    </svg>
                </span>
                <div class="milestone-label"><?php echo lang('milestone_rate'); ?></div>
                <div class="milestone-icon"></div>
            </div>
            <div class="milestone-line milestone-line-5"></div>
            <div class="milestone-step milestone-step-6">
                <span class="milestone-complete-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                    </svg>
                </span>
                <div class="milestone-label"><?php echo lang('milestone_closed'); ?></div>
                <div class="milestone-icon"></div>
            </div>
        </div>
    </div>
    <div class="milestone-content milestone-content-1">
        <div class="milestone-title"><?php echo lang('milestone_you_have_been_appointed'); ?></div>
        <p class="modal-para"><?php echo lang('milestone_you_have_24_hours_to_accept_this_appointment'); ?>.</p>
    </div>
    <div class="milestone-content milestone-content-2">
        <div class="milestone-title"><?php echo $poster['name'] ?> <?php echo lang('milestone_has_been_notified_of_your_acceptance'); ?></div>
        <p class="modal-para"><?php echo lang('milestone_mtp_is_requesting_for_the_task_payment'); ?>.</p>
    </div>
    <div class="milestone-content milestone-content-3a">
        <div class="milestone-title"><?php echo $poster['name'] ?> <?php echo lang('milestone_has_confirmed_you_for_this_task'); ?>.</div>
        <p class="modal-para"><?php echo lang('milestone_we_have_received_the_payment_on_your_behalf'); ?> <?php echo $task['start_date'] ?></p>
    </div>
    <div class="milestone-content milestone-content-3b">
        <div class="milestone-title"><?php echo lang('milestone_this_task_is_in_progress'); ?>.</div>
        <p class="modal-para"><?php echo lang('milestone_you_will_be_able_to_mark_this_task'); ?> <?php echo $task['end_date'] ?></p>
    </div>
    <div class="milestone-content milestone-content-3c">
        <div class="milestone-title"><?php echo lang('milestone_this_task_is_in_progress'); ?>.</div>
        <p class="modal-para"><?php echo lang('milestone_you_will_be_able_to_mark_this_task_as_completed'); ?> <?php echo $task['end_date'] ?></p>
    </div>
    <div class="milestone-content milestone-content-4">
        <div class="milestone-title"><?php echo lang('milestone_you_are_marking_this_task_as_completed'); ?>.</div>
        <p class="modal-para"><?php echo lang('milestone_waiting_for'); ?> <?php echo $poster['name'] ?> <?php echo lang('milestone_to_approve_your_request'); ?>.</p>
        <p class="modal-para"><?php echo $poster['name'] ?> <?php echo lang('milestone_has_60_hours_to_respond_to_this'); ?>.</p>
    </div>
    <div class="milestone-content milestone-content-5a">
        <div class="milestone-title"><?php echo lang('milestone_rate'); ?> <?php echo $poster['name'] ?>!</div>
        <p class="modal-para"><?php echo lang('milestone_payment_status'); ?></p>
        <p class="modal-para"><?php echo lang('milestone_let'); ?> <?php echo $poster['name'] ?> <?php echo lang('milestone_know_what_you_think_about_his'); ?>!</p>
        <p class="modal-para"><?php echo lang('milestone_you_have_60_hours_to_rate'); ?> <?php echo $poster['name'] ?><?php echo lang('milestone_rating_of_you_will_be_posted'); ?>.</p>
    </div>
    <div class="milestone-content milestone-content-5b">
        <div class="milestone-title"><?php echo lang('milestone_rate'); ?> <?php echo $poster['name'] ?>!</div>
        <p class="modal-para"><?php echo lang('milestone_maketimepay_has_released'); ?> <?php echo isset($dispute['percentage']) ? $dispute['percentage'] : '' ?><?php echo lang('milestone_of_work_the_task_value'); ?> (<?php echo $cms->price($task['budget']) ?>) <?php echo lang('milestone_to_you_and_the_balance'); ?> <?php echo $poster['name'] ?>. <?php echo lang('milestone_please_take_some_time_to_rate_your_experience'); ?> <?php echo $poster['name'] ?>.</p>
        <p class="modal-para"><?php echo lang('milestone_you_have_60_hours_to_rate_otherwise'); ?> <?php echo $poster['name'] ?><?php echo lang('milestone_rating_of_you_will_be_posted'); ?>.</p>
    </div>
    <div class="milestone-content milestone-content-7">
        <div class="milestone-title"><?php echo $poster['name'] ?> <?php echo lang('milestone_has_requested_for_a_dispute'); ?>.</div>
	    <p class="modal-para"><?php echo $poster['name'] ?> <?php echo lang('milestone_has_set_a_dispute_rate_of'); ?> <?php echo isset($dispute['percentage']) ? $dispute['percentage'] : '' ?><?php echo lang('milestone_percent_of'); ?> <?php echo $cms->price($task['budget']) ?> (<?php echo isset($dispute['amount']) ? $cms->price($dispute['amount']) : '0' ?>)</p>
        <p class="modal-para"><?php echo lang('milestone_do_you_accept_this'); ?></p>
    </div>
    <div class="milestone-content milestone-content-7-1">
        <div class="milestone-title"><?php echo lang('milestone_this_task_has_been_completed'); ?></div>
        <p class="modal-para"><?php echo lang('milestone_you_have_successfully_completed_this_task'); ?>!</p>
    </div>
    <div class="milestone-content milestone-content-7-2">
        <div class="milestone-title"><?php echo lang('milestone_task_has_been') .' '. lang('task_' . $task['status']) ?></div>
	    <p class="modal-para"><?php echo ! $task['other_person'] ? lang('milestone_you_have') : $poster['name'] . ' has' ?> <?php echo $task['status'] ?> <?php echo lang('milestone_this_task'); ?>.</p>
    </div>
    <div class="milestone-content milestone-content-7-3">
        <div class="milestone-title"><?php echo lang('milestone_you_did_not_fullfill_this_task'); ?></div>
        <p class="modal-para"><?php echo lang('milestone_task_has_been'); ?><?php echo lang('milestone_task_is_cancelled_due_to_inactivity'); ?>.</p>
    </div>
    <div class="milestone-content milestone-content-7-4">
        <div class="milestone-title"><?php echo lang('milestone_task_has_been') .' '. lang('task_' . $task['status']) ?></div>
        <p class="modal-para"><?php echo lang('milestone_this_task_has_been_disputed_and_resolved'); ?>.</p>
    </div>
	<?php if( isset($seeker['hide-status']) && $seeker['hide-status'] ): ?>
	<span class="hide hide-status"></span>
	<?php endif ?>
</div>
<div class="modal-footer">
    <div class="milestone-footer modal-footer-content-1">
        <div class="modal-footer modal-footer-milestone">
	        <form class="modal-footer" style="padding:0" method="post" action="<?php echo url_for("workspace/task/offer") ?>">
                <?php echo html_form_token_field() ?>
		        <input type="hidden" name="task_id" value="<?php echo $task['task_id'] ?>" />
		        <input type="hidden" name="user_id" value="<?php echo $seeker['m_id'] ?>" />
		        <button type="submit" name="accept" class="btn-icon-full btn-accept gtm-milestone-seeker-accept">
			        <span class="btn-label"><?php echo lang('task_accept') ?></span>
			        <span class="btn-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                            <polyline points="20 6 9 17 4 12"></polyline>
                        </svg>
                    </span>
		        </button>
		        <button type="submit" name="decline" class="btn-icon-full btn-cancel btn-decline gtm-milestone-seeker-decline">
			        <span class="btn-label"><?php echo lang('task_decline') ?></span>
			        <span class="btn-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </span>
		        </button>
	        </form>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-3b">
        <div class="modal-footer modal-footer-milestone">
	        <form method="get" action="<?php echo url_for("/workspace/task/{$task['slug']}/details") ?>">
		        <button type="submit" class="btn-icon-full btn-viewtask gtm-milestone-seeker-viewtask" data-orientation="next">
			        <span class="btn-label"><?php echo lang('milestone_view_task'); ?></span>
			        <span class="btn-icon">
                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                        </svg>
                    </span>
		        </button>
	        </form>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-3c">
        <div class="modal-footer modal-footer-milestone">
	        <form method="get" action="<?php echo url_for("/workspace/task/{$task['slug']}/details") ?>">
		        <button type="submit" class="btn-icon-full btn-viewtask gtm-milestone-seeker-viewtask" data-orientation="next">
			        <span class="btn-label"><?php echo lang('milestone_view_task'); ?></span>
			        <span class="btn-icon">
                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                        </svg>
                    </span>
		        </button>
	        </form>
	        <form method="post" action="<?php echo url_for('workspace/task/complete') ?>">
                <?php echo html_form_token_field() ?>
		        <input type="hidden" name="task_id" value="<?php echo $task['task_id'] ?>" />
		        <input type="hidden" name="user_id" value="<?php echo $seeker['m_id'] ?>" />
		        <button type="submit" class="btn-icon-full btn-markcomplete gtm-milestone-seeker-markcomplete" data-orientation="next">
			        <span class="btn-label"><?php echo lang('task_mark_as_complete') ?></span>
			        <span class="btn-icon">
                        <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                        </svg>
                    </span>
		        </button>
	        </form>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-5a">
        <div class="modal-footer modal-footer-milestone">
	        <form method="get" action="<?php echo url_for("/workspace/billings/earning") ?>">
	            <button type="submit" class="btn-icon-full btn-paymentdetail gtm-milestone-seeker-view-payment-detail">
	                <span class="btn-label"><?php echo lang('milestone_view_payment_detail') ?></span>
	                <span class="btn-icon">
	                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                    </svg>
	                </span>
	            </button>
	        </form>
	        <form method="get" action="<?php echo url_for("/workspace/rating-centre") ?>">
		        <input type="hidden" name="review" value="<?php echo $task['task_id'] ?>" />
	            <button type="submit" class="btn-icon-full btn-ratingctr gtm-milestone-seeker-rating-centre">
	                <span class="btn-label"><?php echo lang('milestone_rating_centre') ?></span>
	                <span class="btn-icon">
	                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                    </svg>
	                </span>
	            </button>
	        </form>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-5b">
        <div class="modal-footer modal-footer-milestone">
	        <form method="get" action="<?php echo url_for("/workspace/billings/earning") ?>">
	            <button type="submit" class="btn-icon-full btn-paymentdetail gtm-milestone-seeker-view-payment-detail">
	                <span class="btn-label"><?php echo lang('milestone_view_payment_detail') ?></span>
	                <span class="btn-icon">
	                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                    </svg>
	                </span>
	            </button>
	        </form>
	        <form method="get" action="<?php echo url_for("/workspace/rating-centre") ?>">
		        <input type="hidden" name="review" value="<?php echo $task['task_id'] ?>" />
	            <button type="submit" class="btn-icon-full btn-ratingctr gtm-milestone-seeker-rating-centre">
	                <span class="btn-label"><?php echo lang('milestone_rating_centre') ?></span>
	                <span class="btn-icon">
	                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
	                    </svg>
	                </span>
	            </button>
	        </form>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-7">
        <div class="modal-footer modal-footer-milestone">
	        <form class="modal-footer" method="post" action="<?php echo url_for('workspace/task/dispute') ?>" style="padding:0">
		        <?php echo html_form_token_field() ?>
		        <input type="hidden" name="task_id" value="<?php echo $task['task_id'] ?>" />
		        <input type="hidden" name="seeker_id" value="<?php echo $seeker['m_id'] ?>" />
	            <button type="submit" name="accept" class="btn-icon-full btn-accept gtm-milestone-seeker-accept-dispute">
	                <span class="btn-label"><?php echo lang('task_accept') ?></span>
	                <span class="btn-icon">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                        <polyline points="20 6 9 17 4 12"></polyline>
	                    </svg>
	                </span>
	            </button>
	            <button type="submit" name="decline" class="btn-icon-full btn-cancel btn-decline gtm-milestone-seeker-decline-dispute">
	                <span class="btn-label"><?php echo lang('task_decline') ?></span>
	                <span class="btn-icon">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                        <line x1="18" y1="6" x2="6" y2="18"></line>
	                        <line x1="6" y1="6" x2="18" y2="18"></line>
	                    </svg>
	                </span>
	            </button>
	        </form>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-7-1">
        <div class="modal-footer modal-footer-milestone">
            <button type="button" class="btn-icon-full btn-close gtm-milestone-seeker-close" data-dismiss="modal" data-orientation="next">
                <span class="btn-label"><?php echo lang('close') ?></span>
                <span class="btn-icon">
                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                    </svg>
                </span>
            </button>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-7-2">
        <div class="modal-footer modal-footer-milestone">
            <button type="button" class="btn-icon-full btn-close gtm-milestone-seeker-close" data-dismiss="modal" data-orientation="next">
                <span class="btn-label"><?php echo lang('close') ?></span>
                <span class="btn-icon">
                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                    </svg>
                </span>
            </button>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-7-3">
        <div class="modal-footer modal-footer-milestone">
            <button type="button" class="btn-icon-full btn-close gtm-milestone-seeker-close" data-dismiss="modal" data-orientation="next">
                <span class="btn-label"><?php echo lang('close') ?></span>
                <span class="btn-icon">
                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                    </svg>
                </span>
            </button>
        </div>
    </div>
    <div class="milestone-footer modal-footer-content-7-4">
        <div class="modal-footer modal-footer-milestone">
            <button type="button" class="btn-icon-full btn-close gtm-milestone-seeker-close" data-dismiss="modal" data-orientation="next">
                <span class="btn-label"><?php echo lang('close') ?></span>
                <span class="btn-icon">
                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                    </svg>
                </span>
            </button>
        </div>
    </div>
</div>