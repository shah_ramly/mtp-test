<?php
$applied_closed = array_filter($tasks['applied'], function($task) use($current_user){
    return ($task['apply_status'] === 'lost' && !is_null($task['assigned'])) ||
	       in_array($task['status'], ['cancelled', 'completed', 'closed', 'disputed']);
});
?>
<div class="tab-pane tc-Closed fade" id="tcClosed" role="tabpanel" aria-labelledby="tcClosed-tab">
    <div class="tbl-body-calendar">
		<?php if((int)$user->info['type'] === 0): ?>
        <div class="tbl-group-calendar-row">
	        <div class="table-filter-container show">
		        <div class="tbl-group-calendar-title tbl-group-collapse-title"><span class="title-collapse"><?php echo lang('task_centre_view_applicant_task'); ?></span><i class="fa fa-caret-down"></i></div>
		        <div class="table-filter-right-col">
			        <div class="dropdown dropdown-filter-container">
				        <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					        <label class="filter-btn-lbl"><?php echo lang('task_centre_filter'); ?></label> <span class="drop-val"><?php echo lang('task_all'); ?></span>
				        </button>
				        <div class="dropdown-menu applied-tasks-filter" aria-labelledby="dropdownMenuButton">
					        <a class="dropdown-item active" href="#"><?php echo lang('task_all'); ?></a>
					        <a class="dropdown-item" href="#"><?php echo lang('task_completed'); ?></a>
					        <a class="dropdown-item" href="#"><?php echo lang('task_lost'); ?></a>
					        <a class="dropdown-item" href="#"><?php echo lang('task_cancelled'); ?></a>
					        <a class="dropdown-item" href="#"><?php echo lang('task_disputed'); ?></a>
				        </div>
			        </div>
		        </div>
	        </div>
	        <div class="tbl-collapse-group collapse show applied-tasks-list">
            <?php $no_tasks = true; ?>
	        <?php if(!empty($applied_closed)): ?>
                <?php foreach($applied_closed as $task): ?>
                    <?php echo partial('workspace/partial/task-centre/applied-task-row.html.php', ['task' => $task]); ?>
                <?php endforeach; ?>
	        <?php else: ?>
	            <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
	                <div class="tbl-group-calendar-bar-flex">
		                <div class="tbl-group-calendar-bar-row row-1">
			                <span class="tbl-group-calendar-bar-title"><?php echo lang('task_centre_no_applied_task_yet'); ?>.</span>
		                </div>
	                </div>
                </div>
	        <?php endif ?>
	        </div>
        </div>
		<?php endif ?>
        <div class="tbl-group-calendar-row">
	        <div class="table-filter-container show">
		        <div class="tbl-group-calendar-title tbl-group-collapse-title"><span class="title-collapse"><?php echo lang('task_centre_view_posted_tasks'); ?></span><i class="fa fa-caret-down"></i></div>
		        <div class="table-filter-right-col">
			        <div class="dropdown dropdown-filter-container">
				        <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					        <label class="filter-btn-lbl"><?php echo lang('task_centre_filter'); ?></label> <span class="drop-val"><?php echo lang('task_all'); ?></span>
				        </button>
				        <div class="dropdown-menu posted-tasks-filter" aria-labelledby="dropdownMenuButton">
					        <a class="dropdown-item active" href="#"><?php echo lang('task_all'); ?></a>
					        <a class="dropdown-item" href="#"><?php echo lang('task_completed'); ?></a>
					        <a class="dropdown-item" href="#"><?php echo lang('task_cancelled'); ?></a>
					        <a class="dropdown-item" href="#"><?php echo lang('task_disputed'); ?></a>
				        </div>
			        </div>
		        </div>
	        </div>
	        <div class="tbl-collapse-group collapse show posted-tasks-list">
            <?php $no_tasks = true; ?>
            <?php if(!empty($tasks['closed'])): ?>
                <?php foreach($tasks['closed'] as $task): ?>
	            <?php if(in_array($task['status'], ['completed', 'cancelled', 'disputed', 'closed'])): ?>
		            <?php echo partial('workspace/partial/task-centre/posted-task-row.html.php', ['task' => $task]); ?>
                    <?php $no_tasks = false; ?>
	            <?php endif ?>
                <?php endforeach; ?>
            <?php endif ?>
            <?php if($no_tasks): ?>
		        <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
	                <div class="tbl-group-calendar-bar-flex">
		                <div class="tbl-group-calendar-bar-row row-1">
			                <span class="tbl-group-calendar-bar-title"><?php echo lang('task_centre_no_posted_task_yet'); ?>.</span>
		                </div>
	                </div>
                </div>
            <?php endif; ?>
	        </div>
        </div>
    </div>

</div>