<div class="tab-content" id="pills-tcListView">
    <div class="tab-pane tc-All fade <?php echo $section === 'applied' ? 'show active' : '' ?>" id="tcAll" role="tabpanel" aria-labelledby="tcAll-tab">
        <div class="tbl-body-calendar">
            <div class="tbl-group-calendar-row active-tc-row">
                <div class="table-filter-container show">
                    <div class="tbl-group-calendar-title tbl-group-collapse-title gtm-applied-active-title">
                        <span class="title-collapse"><?php echo lang('task_centre_active_task') ?></span><i class="fa fa-caret-down"></i>
                    </div>
                    <div class="table-filter-right-col tc-filter-collapse-mobile">
                        <button class="btn-filter-search btn-tc-filter collapsed gtm-applied-active-filter" type="button" data-toggle="collapse" data-target="#appliedActive" aria-expanded="false" aria-controls="appliedActive">
                            <span class="slider-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sliders" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M11.5 2a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM9.05 3a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0V3h9.05zM4.5 7a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM2.05 8a2.5 2.5 0 0 1 4.9 0H16v1H6.95a2.5 2.5 0 0 1-4.9 0H0V8h2.05zm9.45 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm-2.45 1a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0v-1h9.05z"></path>
                                </svg>
                            </span>
                            <i class="fa fa-caret-down"></i>
                        </button>
                        <div class="dropdown-menu-tc-filter collapse" id="appliedActive">
                            <div class="dropdown-menu-tc-filter-flex">
                                <div class="dropdown dropdown-filter-container dropdown-view-container active-applied-tasks-view">
                                    <button class="btn btn-dropdown-label dropdown-toggle gtm-active-view-btn" type="button" id="dropdownAppViewButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <label class="filter-btn-lbl">View</label> <span class="drop-val">All</span>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownAppViewButton">
                                        <a class="dropdown-item active gtm-active-view-all all" href="#">All</a>
                                        <a class="dropdown-item gtm-active-view-expand-all expand-all" href="#">Expand All</a>
                                        <a class="dropdown-item gtm-active-view-collapse-all collapse-all" href="#">Collapse All</a>
                                    </div>
                                </div>
                                <div class="dropdown dropdown-filter-container">
                                    <button class="btn btn-dropdown-label dropdown-toggle gtm-active-filter-btn" type="button" id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <label class="filter-btn-lbl">Filter</label>
                                        <?php if (session('applied_active_filter') === 'applied'): ?>
                                            <span class='drop-val'><?php echo lang('task_applied') ?></span>
                                        <?php elseif (session('applied_active_filter') === 'appointed'): ?>
                                            <span class='drop-val'><?php echo lang('task_appointed') ?></span>
                                        <?php elseif (session('applied_active_filter') === 'in-progress'): ?>
                                            <span class='drop-val'><?php echo lang('task_in_progress') ?></span>
                                        <?php else: ?>
                                            <span class="drop-val"><?php echo lang('task_all'); ?></span>
                                        <?php endif ?>
                                    </button>
                                    <div class="dropdown-menu active-applied-tasks-filter" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item <?php echo !session('applied_active_filter') || session('applied_active_filter') === 'all' ? 'active': '' ?> gtm-active-filter-all" data-filter="view-all" href="#"><?php echo lang('task_all'); ?></a>
                                        <a class="dropdown-item <?php echo session('applied_active_filter') === 'applied' ? 'active': '' ?> gtm-active-filter-applied" href="#"><?php echo lang('task_applied'); ?></a>
                                        <a class="dropdown-item <?php echo session('applied_active_filter') === 'appointed' ? 'active': '' ?> gtm-active-filter-appointed" href="#"><?php echo lang('task_appointed'); ?></a>
                                        <a class="dropdown-item <?php echo session('applied_active_filter') === 'in-progress' ? 'active' : '' ?> gtm-active-filter-inprogress" href="#"><?php echo lang('task_in_progress'); ?></a>
                                    </div>
                                </div>
                                <div class="dropdown dropdown-filter-container dropdown-sort-container">
                                    <button class="btn btn-dropdown-label dropdown-toggle gtm-active-sort-btn" type="button" id="dropdownSortButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <label class="filter-btn-lbl"><?php echo lang('home_sort_by') ?></label>
                                        <?php if (session('applied_active_order') === 'value_high_to_low'): ?>
                                            <span class='drop-val'><?php echo lang('value_high_to_low') ?></span>
                                        <?php elseif (session('applied_active_order') === 'value_low_to_high'): ?>
                                            <span class='drop-val'><?php echo lang('value_low_to_high') ?></span>
                                        <?php else: ?>
                                            <span class="drop-val"><?php echo lang('task_centre_latest') ?></span>
                                        <?php endif ?>
                                    </button>
                                    <div class="dropdown-menu active-applied-tasks-order" aria-labelledby="dropdownSortButton">
                                        <a class="dropdown-item <?php echo !session('applied_active_order') || session('applied_active_order') === 'latest' ? 'active' : '' ?> gtm-active-sort-latest" data-sortby="latest" href="#"><?php echo lang('task_centre_latest'); ?></a>
                                        <a class="dropdown-item <?php echo session('applied_active_order') === 'value_high_to_low' ? 'active' : '' ?> gtm-active-sort-val-high-low" data-sortby="value_high_to_low" href="#"><?php echo lang('value_high_to_low'); ?></a>
                                        <a class="dropdown-item <?php echo session('applied_active_order') === 'value_low_to_high' ? 'active' : '' ?> gtm-active-sort-val-low-high" data-sortby="value_low_to_high" href="#"><?php echo lang('value_low_to_high'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tbl-collapse-group collapse show applied-tasks-list position-relative">
	                <div class='loader position-absolute hide'
	                     style='padding:0;width:100%;height:100%;background:rgba(221,221,221,35%);border-radius:10px;z-index:1;display:flex;align-items:center;align-content:center;justify-content:center'>
		                <div class='spinner-border spinner-border position-relative'
		                     style='width:50px;height:50px;border-color:#00003f;border-right-color:transparent'
		                     role='status'>
			                <span class='sr-only'><?php
                                echo lang('modal_loading') ?></span>
		                </div>
	                </div>
	                <div class="tasks-contents">
	                    <?php if( !empty( $tasks['all_applied']['active'] ) ): ?>
	                        <?php foreach ($tasks['all_applied']['active'] as $task): ?>
	                            <?php echo partial('workspace/partial/task-centre/applied-task-row.html.php', ['task' => $task]); ?>
	                        <?php endforeach ?>
	                    <?php else: ?>
		                    <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
			                    <div class="tbl-group-calendar-bar-flex">
				                    <div class="tbl-group-calendar-bar-row row-1">
					                    <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_applied_task') ?></span>
				                    </div>
			                    </div>
		                    </div>
	                    <?php endif ?>
	                </div>
	                <button class='btn-icon-full btn-load-more more-applied-tasks <?php
                    echo ($tasks['all_applied']['active_total'] <= 10) ? 'hide' : '' ?>' data-next-page='2'
	                        data-pages="<?php
                            echo ceil((int)$tasks['all_applied']['active_total'] / 10) ?>" data-text="<?php
                    echo lang('search_load_more_loading') ?>">
	                <span class='btn-label' id='loadMoreLbl' data-text="<?php
                    echo lang('public_search_load_more'); ?>">
		                <?php
                        echo lang('public_search_load_more'); ?>
	                </span>
		                <span class="btn-icon load-plus-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                            <line x1="12" y1="5" x2="12" y2="19"></line>
                            <line x1="5" y1="12" x2="19" y2="12"></line>
                        </svg>
                    </span>
		                <span class="btn-icon load-load-icon" style="display:none;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                            <line x1="12" y1="2" x2="12" y2="6"></line>
                            <line x1="12" y1="18" x2="12" y2="22"></line>
                            <line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line>
                            <line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line>
                            <line x1="2" y1="12" x2="6" y2="12"></line>
                            <line x1="18" y1="12" x2="22" y2="12"></line>
                            <line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line>
                            <line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line>
                        </svg>
                    </span>
	                </button>
	                <button class="btn-icon-full btn-load-more tasks-back-to-top" style="display:none"
	                        data-text="Loading...">
	                <span class="btn-label" id="loadMoreLbl" data-text="Load More">
		                <span><?php
                            echo lang('end_of_list') ?></span> - <?php
                        echo lang('back_to_top') ?>
	                </span>
		                <span class="btn-icon arrowtop-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                             class="bi bi-arrow-up-short" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                  d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5z"></path>
                        </svg>
                    </span>
		                <span class="btn-icon load-load-icon" style="display:none;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                            <line x1="12" y1="2" x2="12" y2="6"></line>
                            <line x1="12" y1="18" x2="12" y2="22"></line>
                            <line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line>
                            <line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line>
                            <line x1="2" y1="12" x2="6" y2="12"></line>
                            <line x1="18" y1="12" x2="22" y2="12"></line>
                            <line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line>
                            <line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line>
                        </svg>
                    </span>
	                </button>
                </div>
            </div>
            <div id="inactive-applied-tc-row" class="tbl-group-calendar-row inactive-tc-row">
                <div class="table-filter-container show">
                    <div class="tbl-group-calendar-title tbl-group-collapse-title gtm-applied-inactive-title">
                        <span class="title-collapse"><?php echo lang('task_centre_inactive_task') ?></span><i class="fa fa-caret-down"></i>
                    </div>
                    <div class="table-filter-right-col tc-filter-collapse-mobile">
                        <button class="btn-filter-search btn-tc-filter collapsed gtm-applied-inactive-filter" type="button" data-toggle="collapse" data-target="#appliedInactive" aria-expanded="false" aria-controls="appliedInactive">
                            <span class="slider-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sliders" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M11.5 2a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM9.05 3a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0V3h9.05zM4.5 7a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM2.05 8a2.5 2.5 0 0 1 4.9 0H16v1H6.95a2.5 2.5 0 0 1-4.9 0H0V8h2.05zm9.45 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm-2.45 1a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0v-1h9.05z"></path>
                                </svg>
                            </span>
                            <i class="fa fa-caret-down"></i>
                        </button>
                        <div class="dropdown-menu-tc-filter collapse" id="appliedInactive">
                            <div class="dropdown-menu-tc-filter-flex">
                                <div class="dropdown dropdown-filter-container dropdown-view-container inactive-applied-tasks-view">
                                    <button class="btn btn-dropdown-label dropdown-toggle gtm-inactive-view-btn" type="button" id="dropdownAppViewButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <label class="filter-btn-lbl">View</label> <span class="drop-val">All</span>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownAppViewButton">
                                        <a class="dropdown-item active gtm-inactive-view-all all" href="#">All</a>
                                        <a class="dropdown-item gtm-inactive-view-expand-all expand-all" href="#">Expand All</a>
                                        <a class="dropdown-item gtm-inactive-view-collapse-all collapse-all" href="#">Collapse All</a>
                                    </div>
                                </div>
                                <div class="dropdown dropdown-filter-container">
                                    <button class="btn btn-dropdown-label dropdown-toggle gtm-inactive-filter-btn" type="button" id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <label class="filter-btn-lbl"><?php echo lang('task_filter_title'); ?></label>
                                        <?php if (session('applied_inactive_filter') === 'completed'): ?>
                                            <span class='drop-val'><?php echo lang('task_completed') ?></span>
                                        <?php elseif (session('applied_inactive_filter') === 'closed'): ?>
                                            <span class='drop-val'><?php echo lang('task_closed') ?></span>
                                        <?php elseif (session('applied_inactive_filter') === 'cancelled'): ?>
                                            <span class='drop-val'><?php echo lang('task_cancelled') ?></span>
                                        <?php elseif (session('applied_inactive_filter') === 'declined'): ?>
                                            <span class='drop-val'><?php echo lang('task_declined') ?></span>
                                        <?php elseif (session('applied_inactive_filter') === 'disputed'): ?>
                                            <span class='drop-val'><?php echo lang('task_disputed') ?></span>
                                        <?php else: ?>
                                            <span class="drop-val"><?php echo lang('task_all'); ?></span>
                                        <?php endif ?>
                                    </button>
                                    <div class="dropdown-menu inactive-applied-tasks-filter" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item <?php echo !session('applied_inactive_filter') || session('applied_inactive_filter') === 'all' ? 'active' : '' ?> gtm-inactive-filter-all" data-filter="view-all" href="#"><?php echo lang('task_all'); ?></a>
                                        <a class="dropdown-item <?php echo session('applied_inactive_filter') === 'completed' ? 'active' : '' ?> gtm-inactive-filter-completed" href="#"><?php echo lang('task_completed'); ?></a>
                                        <a class="dropdown-item <?php echo session('applied_inactive_filter') === 'closed' ? 'active' : '' ?> gtm-inactive-filter-closed" href="#"><?php echo lang('task_closed'); ?></a>
                                        <a class="dropdown-item <?php echo session('applied_inactive_filter') === 'cancelled' ? 'active' : '' ?> gtm-inactive-filter-cancelled" href="#"><?php echo lang('task_cancelled'); ?></a>
                                        <a class="dropdown-item <?php echo session('applied_inactive_filter') === 'declined' ? 'active' : '' ?> gtm-inactive-filter-declined" href="#"><?php echo lang('task_declined'); ?></a>
                                        <a class="dropdown-item <?php echo session('applied_inactive_filter') === 'disputed' ? 'active' : '' ?> gtm-inactive-filter-disputed" href="#"><?php echo lang('task_disputed'); ?></a>
                                    </div>
                                </div>
                                <div class="dropdown dropdown-filter-container dropdown-sort-container">
                                    <button class="btn btn-dropdown-label dropdown-toggle gtm-inactive-sort-btn" type="button" id="dropdownSortButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <label class="filter-btn-lbl"><?php echo lang('home_sort_by'); ?></label>
                                        <?php if (session('applied_inactive_order') === 'value_high_to_low'): ?>
                                            <span class='drop-val'><?php echo lang('value_high_to_low') ?></span>
                                        <?php elseif (session('applied_inactive_order') === 'value_low_to_high'): ?>
                                            <span class='drop-val'><?php echo lang('value_low_to_high') ?></span>
                                        <?php else: ?>
                                            <span class="drop-val"><?php echo lang('task_centre_latest') ?></span>
                                        <?php endif ?>
                                    </button>
                                    <div class="dropdown-menu inactive-applied-tasks-order" aria-labelledby="dropdownSortButton">
                                        <a class="dropdown-item <?php echo !session('applied_inactive_order') || session('applied_inactive_order') === 'latest' ? 'active' : '' ?> gtm-inactive-sort-latest" data-sortby="latest" href="#"><?php echo lang('task_centre_latest'); ?></a>
                                        <a class="dropdown-item <?php echo session('applied_inactive_order') === 'value_high_to_low' ? 'active' : '' ?> gtm-inactive-sort-val-high-low" data-sortby="value_high_to_low" href="#"><?php echo lang('value_high_to_low'); ?></a>
                                        <a class="dropdown-item <?php echo session('applied_inactive_order') === 'value_low_to_high' ? 'active' : '' ?> gtm-inactive-sort-val-low-high" data-sortby="value_low_to_high" href="#"><?php echo lang('value_low_to_high'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tbl-collapse-group collapse show applied-tasks-list position-relative">
	                <div class='loader position-absolute hide'
	                     style='padding:0;width:100%;height:100%;background:rgba(221,221,221,35%);border-radius:10px;z-index:1;display:flex;align-items:center;align-content:center;justify-content:center'>
		                <div class='spinner-border spinner-border position-relative'
		                     style='width:50px;height:50px;border-color:#00003f;border-right-color:transparent'
		                     role='status'>
			                <span class='sr-only'><?php
                                echo lang('modal_loading') ?></span>
		                </div>
	                </div>
	                <div class="tasks-contents">
	                    <?php if( !empty( $tasks['all_applied']['inactive'] ) ): ?>
	                        <?php foreach ($tasks['all_applied']['inactive'] as $task): ?>
	                            <?php echo partial('workspace/partial/task-centre/applied-task-row.html.php', ['task' => $task]); ?>
	                        <?php endforeach ?>
	                    <?php else: ?>
		                    <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
			                    <div class="tbl-group-calendar-bar-flex">
				                    <div class="tbl-group-calendar-bar-row row-1">
					                    <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_applied_task') ?></span>
				                    </div>
			                    </div>
		                    </div>
	                    <?php endif ?>
	                </div>
	                <button class='btn-icon-full btn-load-more more-applied-tasks <?php
                    echo ($tasks['all_applied']['inactive_total'] <= 10) ? 'hide' : '' ?>' data-next-page='2'
	                        data-pages="<?php
                            echo ceil((int)$tasks['all_applied']['inactive_total'] / 10) ?>" data-text="<?php
                    echo lang('search_load_more_loading') ?>">
	                <span class='btn-label' id='loadMoreLbl' data-text="<?php
                    echo lang('public_search_load_more'); ?>">
		                <?php
                        echo lang('public_search_load_more'); ?>
	                </span>
		                <span class="btn-icon load-plus-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                            <line x1="12" y1="5" x2="12" y2="19"></line>
                            <line x1="5" y1="12" x2="19" y2="12"></line>
                        </svg>
                    </span>
		                <span class="btn-icon load-load-icon" style="display:none;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                            <line x1="12" y1="2" x2="12" y2="6"></line>
                            <line x1="12" y1="18" x2="12" y2="22"></line>
                            <line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line>
                            <line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line>
                            <line x1="2" y1="12" x2="6" y2="12"></line>
                            <line x1="18" y1="12" x2="22" y2="12"></line>
                            <line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line>
                            <line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line>
                        </svg>
                    </span>
	                </button>
	                <button class="btn-icon-full btn-load-more tasks-back-to-top" style="display:none"
	                        data-scroll-to="#inactive-applied-tc-row" data-text="Loading...">
	                <span class="btn-label" id="loadMoreLbl" data-text="Load More">
		                <span><?php
                            echo lang('end_of_list') ?></span> - <?php
                        echo lang('back_to_top') ?>
	                </span>
		                <span class="btn-icon arrowtop-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                             class="bi bi-arrow-up-short" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                  d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5z"></path>
                        </svg>
                    </span>
		                <span class="btn-icon load-load-icon" style="display:none;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                            <line x1="12" y1="2" x2="12" y2="6"></line>
                            <line x1="12" y1="18" x2="12" y2="22"></line>
                            <line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line>
                            <line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line>
                            <line x1="2" y1="12" x2="6" y2="12"></line>
                            <line x1="18" y1="12" x2="22" y2="12"></line>
                            <line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line>
                            <line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line>
                        </svg>
                    </span>
	                </button>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane tc-All fade <?php echo $section === 'posted' ? 'show active' : '' ?>" id="tcInProgressMonth" role="tabpanel" aria-labelledby="tcInProgressMonth-tab">
        <div class="tbl-body-calendar">
            <div class="tbl-group-calendar-row active-tc-row">
                <div class="table-filter-container show">
                    <div class="tbl-group-calendar-title tbl-group-collapse-title gtm-posted-active-title"><span class="title-collapse"><?php echo lang('task_centre_active_task') ?></span><i class="fa fa-caret-down"></i></div>
                    <div class="table-filter-right-col tc-filter-collapse-mobile">
                        <button class="btn-filter-search btn-tc-filter collapsed gtm-posted-active-filter" type="button" data-toggle="collapse" data-target="#postedActive" aria-expanded="false" aria-controls="postedActive">
                            <span class="slider-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sliders" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M11.5 2a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM9.05 3a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0V3h9.05zM4.5 7a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM2.05 8a2.5 2.5 0 0 1 4.9 0H16v1H6.95a2.5 2.5 0 0 1-4.9 0H0V8h2.05zm9.45 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm-2.45 1a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0v-1h9.05z"></path>
                                </svg>
                            </span>
                            <i class="fa fa-caret-down"></i>
                        </button>
                        <div class="dropdown-menu-tc-filter collapse" id="postedActive">
                            <div class="dropdown-menu-tc-filter-flex">
                                <div class="dropdown dropdown-filter-container dropdown-view-container active-posted-tasks-view">
                                    <button class="btn btn-dropdown-label dropdown-toggle gtm-active-view-btn" type="button" id="dropdownPostViewButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <label class="filter-btn-lbl">View</label> <span class="drop-val">All</span>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownPostViewButton">
                                        <a class="dropdown-item active gtm-active-view-all all" href="#">All</a>
                                        <a class="dropdown-item gtm-active-view-expand-all expand-all" href="#">Expand All</a>
                                        <a class="dropdown-item gtm-active-view-collapse-all collapse-all" href="#">Collapse All</a>
                                    </div>
                                </div>
                                <div class="dropdown dropdown-filter-container">
                                    <button class="btn btn-dropdown-label dropdown-toggle gtm-active-filter-btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <label class="filter-btn-lbl"><?php echo lang('task_filter_title'); ?></label>
                                        <?php if (session('posted_active_filter') === 'published'): ?>
                                        <span class='drop-val'><?php echo lang('task_published') ?></span>
                                        <?php elseif (session('posted_active_filter') === 'in-progress'): ?>
                                        <span class='drop-val'><?php echo lang('task_in_progress') ?></span>
                                        <?php elseif (session('posted_active_filter') === 'completed'): ?>
                                        <span class='drop-val'><?php echo lang('task_completed') ?></span>
                                        <?php else: ?>
                                        <span class="drop-val"><?php echo lang('task_all'); ?></span>
                                        <?php endif ?>
                                    </button>
                                    <div class="dropdown-menu active-posted-tasks-filter" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item <?php echo !session('posted_active_filter') || session('posted_active_filter') === 'all' ? 'active' : '' ?> gtm-active-filter-view-all" data-filter="view-all" href="#"><?php echo lang('task_all'); ?></a>
                                        <a class="dropdown-item <?php echo session('posted_active_filter') === 'published' ? 'active' : '' ?> gtm-active-filter-published" href="#"><?php echo lang('task_published'); ?></a>
                                        <a class="dropdown-item <?php echo session('posted_active_filter') === 'in-progress' ? 'active' : '' ?> gtm-active-filter-inprogress" href="#"><?php echo lang('task_in_progress'); ?></a>
                                        <a class='dropdown-item <?php echo session('posted_active_filter') === 'completed' ? 'active' : '' ?> gtm-inactive-filter-completed' href='#'><?php echo lang('task_completed'); ?></a>
                                    </div>
                                </div>
                                <div class="dropdown dropdown-filter-container dropdown-sort-container">
                                    <button class="btn btn-dropdown-label dropdown-toggle gtm-active-sort-btn" type="button" id="dropdownSortButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <label class="filter-btn-lbl"><?php echo lang('home_sort_by'); ?></label>
                                        <?php if (session('posted_active_order') === 'value_high_to_low'): ?>
                                            <span class='drop-val'><?php echo lang('value_high_to_low') ?></span>
                                        <?php elseif (session('posted_active_order') === 'value_low_to_high'): ?>
                                            <span class='drop-val'><?php echo lang('value_low_to_high') ?></span>
                                        <?php elseif (session('posted_active_order') === 'applicants_high_to_low'): ?>
                                            <span class='drop-val'><?php echo lang('applicants_high_to_low') ?></span>
                                        <?php elseif (session('posted_active_order') === 'applicants_low_to_high'): ?>
                                            <span class='drop-val'><?php echo lang('applicants_low_to_high') ?></span>
                                        <?php else: ?>
                                            <span class="drop-val"><?php echo lang('task_centre_latest') ?></span>
                                        <?php endif ?>
                                    </button>
                                    <div class="dropdown-menu active-posted-tasks-order" aria-labelledby="dropdownSortButton">
                                        <a class="dropdown-item <?php echo !session('posted_active_order') || session('posted_active_order') === 'latest' ? 'active' : '' ?> gtm-active-sort-latest" data-sortby="latest" href="#"><?php echo lang('task_centre_latest'); ?></a>
                                        <a class="dropdown-item <?php echo session('posted_active_order') === 'value_high_to_low' ? 'active' : '' ?> gtm-active-sort-val-high-low" data-sortby="value_high_to_low" href="#"><?php echo lang('value_high_to_low'); ?></a>
                                        <a class="dropdown-item <?php echo session('posted_active_order') === 'value_low_to_high' ? 'active' : '' ?> gtm-active-sort-val-low-high" data-sortby="value_low_to_high" href="#"><?php echo lang('value_low_to_high'); ?></a>
                                        <a class="dropdown-item <?php echo session('posted_active_order') === 'applicants_high_to_low' ? 'active' : '' ?> gtm-active-sort-applicants-high-low" data-sortby="applicants_high_to_low" href="#"><?php echo lang('applicants_high_to_low'); ?></a>
                                        <a class="dropdown-item <?php echo session('posted_active_order') === 'applicants_low_to_high' ? 'active' : '' ?> gtm-active-sort-applicants-low-high" data-sortby="applicants_low_to_high" href="#"><?php echo lang('applicants_low_to_high'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tbl-collapse-group collapse show posted-tasks-list position-relative">
	                <div class="loader position-absolute hide"
	                     style='padding:0;width:100%;height:100%;background:rgba(221,221,221,35%);border-radius:10px;z-index:1;display:flex;align-items:center;align-content:center;justify-content:center'>
		                <div class='spinner-border spinner-border position-relative' style='width:50px;height:50px;border-color:#00003f;border-right-color:transparent' role='status'>
			                <span class='sr-only'><?php
	                            echo lang('modal_loading') ?></span>
		                </div>
	                </div>
	                <div class="tasks-contents">
                    <?php if( !empty( $tasks['all_posted']['active'] ) ): ?>
                        <?php foreach ($tasks['all_posted']['active'] as $task): ?>
                            <?php $task['questions'] = !empty($questions[$task['id']]) ? $questions[$task['id']][$task['id']] : []; ?>
                            <?php echo partial('workspace/partial/task-centre/posted-task-row.html.php', ['task' => $task]); ?>
                        <?php endforeach ?>
                    <?php else: ?>
	                    <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
		                    <div class="tbl-group-calendar-bar-flex">
			                    <div class="tbl-group-calendar-bar-row row-1">
				                    <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_posted_task') ?></span>
			                    </div>
		                    </div>
	                    </div>
                    <?php endif ?>
	                </div>
	                <button class='btn-icon-full btn-load-more more-posted-tasks <?php echo ($tasks['all_posted']['active_total'] <= 10) ? 'hide' : '' ?>' data-next-page="2"
	                        data-pages="<?php echo ceil((int)$tasks['all_posted']['active_total']/10) ?>"
	                        data-text="<?php echo lang('search_load_more_loading') ?>">
	                <span class='btn-label' id='loadMoreLbl' data-text="<?php echo lang('public_search_load_more'); ?>">
		                <?php echo lang('public_search_load_more'); ?>
	                </span>
		                <span class="btn-icon load-plus-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                             stroke-linejoin="round">
                            <line x1="12" y1="5" x2="12" y2="19"></line>
                            <line x1="5" y1="12" x2="19" y2="12"></line>
                        </svg>
                    </span>
		                <span class="btn-icon load-load-icon" style="display:none;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                             stroke-linejoin="round">
                            <line x1="12" y1="2" x2="12" y2="6"></line>
                            <line x1="12" y1="18" x2="12" y2="22"></line>
                            <line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line>
                            <line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line>
                            <line x1="2" y1="12" x2="6" y2="12"></line>
                            <line x1="18" y1="12" x2="22" y2="12"></line>
                            <line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line>
                            <line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line>
                        </svg>
                    </span>
	                </button>
	                <button class="btn-icon-full btn-load-more tasks-back-to-top" style="display:none" data-text="Loading...">
	                <span class="btn-label" id="loadMoreLbl" data-text="Load More">
		                <span><?php echo lang('end_of_list') ?></span> - <?php echo lang('back_to_top') ?>
	                </span>
		                <span class="btn-icon arrowtop-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                             class="bi bi-arrow-up-short" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                  d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5z"></path>
                        </svg>
                    </span>
		                <span class="btn-icon load-load-icon" style="display:none;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                             stroke-linejoin="round">
                            <line x1="12" y1="2" x2="12" y2="6"></line>
                            <line x1="12" y1="18" x2="12" y2="22"></line>
                            <line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line>
                            <line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line>
                            <line x1="2" y1="12" x2="6" y2="12"></line>
                            <line x1="18" y1="12" x2="22" y2="12"></line>
                            <line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line>
                            <line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line>
                        </svg>
                    </span>
	                </button>
                </div>
            </div>
            <div id="inactive-tc-row" class="tbl-group-calendar-row inactive-tc-row">
                <div class="table-filter-container show">
                    <div class="tbl-group-calendar-title tbl-group-collapse-title gtm-posted-inactive-title"><span class="title-collapse"><?php echo lang('task_centre_inactive_task') ?></span><i class="fa fa-caret-down"></i></div>
                    <div class="table-filter-right-col tc-filter-collapse-mobile">
                        <button class="btn-filter-search btn-tc-filter collapsed gtm-posted-inactive-filter" type="button" data-toggle="collapse" data-target="#postedInactive" aria-expanded="false" aria-controls="postedInactive">
                            <span class="slider-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sliders" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M11.5 2a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM9.05 3a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0V3h9.05zM4.5 7a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM2.05 8a2.5 2.5 0 0 1 4.9 0H16v1H6.95a2.5 2.5 0 0 1-4.9 0H0V8h2.05zm9.45 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm-2.45 1a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0v-1h9.05z"></path>
                                </svg>
                            </span>
                            <i class="fa fa-caret-down"></i>
                        </button>
                        <div class="dropdown-menu-tc-filter collapse" id="postedInactive">
                            <div class="dropdown-menu-tc-filter-flex">
                                <div class="dropdown dropdown-filter-container dropdown-view-container inactive-posted-tasks-view">
                                    <button class="btn btn-dropdown-label dropdown-toggle gtm-inactive-view-btn" type="button" id="dropdownPostViewButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <label class="filter-btn-lbl">View</label> <span class="drop-val">All</span>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownPostViewButton">
                                        <a class="dropdown-item active gtm-inactive-view-all all" href="#">All</a>
                                        <a class="dropdown-item gtm-inactive-view-expand-all expand-all" href="#">Expand All</a>
                                        <a class="dropdown-item gtm-inactive-view-collapse-all collapse-all" href="#">Collapse All</a>
                                    </div>
                                </div>
                                <div class="dropdown dropdown-filter-container">
                                    <button class="btn btn-dropdown-label dropdown-toggle gtm-inactive-filter-btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <label class="filter-btn-lbl"><?php echo lang('task_filter_title'); ?></label>
                                        <?php if (session('posted_inactive_filter') === 'draft'): ?>
                                            <span class='drop-val'><?php echo lang('task_draft') ?></span>
                                        <?php elseif (session('posted_inactive_filter') === 'closed'): ?>
                                            <span class='drop-val'><?php echo lang('task_closed') ?></span>
                                        <?php elseif (session('posted_inactive_filter') === 'cancelled'): ?>
                                            <span class='drop-val'><?php echo lang('task_cancelled') ?></span>
                                        <?php elseif (session('posted_inactive_filter') === 'disputed'): ?>
                                            <span class='drop-val'><?php echo lang('task_disputed') ?></span>
                                        <?php else: ?>
                                            <span class="drop-val"><?php echo lang('task_all'); ?></span>
                                        <?php endif ?>
                                    </button>
                                    <div class="dropdown-menu inactive-posted-tasks-filter" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item <?php echo !session('posted_inactive_filter') || session('posted_inactive_filter') === 'all' ? 'active' : '' ?> gtm-inactive-filter-all" data-filter="view-all" href="#"><?php echo lang('task_all'); ?></a>
                                        <a class='dropdown-item <?php echo session('posted_inactive_filter') === 'draft' ? 'active' : '' ?> gtm-active-filter-draft' href='#'><?php echo lang('task_draft'); ?></a>
                                        <a class="dropdown-item <?php echo session('posted_inactive_filter') === 'closed' ? 'active' : '' ?> gtm-inactive-filter-closed" href="#"><?php echo lang('task_closed'); ?></a>
                                        <a class="dropdown-item <?php echo session('posted_inactive_filter') === 'cancelled' ? 'active' : '' ?> gtm-inactive-filter-cancelled" href="#"><?php echo lang('task_cancelled'); ?></a>
                                        <a class="dropdown-item <?php echo session('posted_inactive_filter') === 'disputed' ? 'active' : '' ?> gtm-inactive-filter-disputed" href="#"><?php echo lang('task_disputed'); ?></a>
                                    </div>
                                </div>
                                <div class="dropdown dropdown-filter-container dropdown-sort-container">
                                    <button class="btn btn-dropdown-label dropdown-toggle gtm-inactive-sort-btn" type="button" id="dropdownSortButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <label class="filter-btn-lbl"><?php echo lang('home_sort_by'); ?></label>
                                        <?php if (session('posted_inactive_order') === 'value_high_to_low'): ?>
                                            <span class='drop-val'><?php echo lang('value_high_to_low') ?></span>
                                        <?php elseif (session('posted_inactive_order') === 'value_low_to_high'): ?>
                                            <span class='drop-val'><?php echo lang('value_low_to_high') ?></span>
                                        <?php elseif (session('posted_inactive_order') === 'applicants_high_to_low'): ?>
                                            <span class='drop-val'><?php echo lang('applicants_high_to_low') ?></span>
                                        <?php elseif (session('posted_inactive_order') === 'applicants_low_to_high'): ?>
                                            <span class='drop-val'><?php echo lang('applicants_low_to_high') ?></span>
                                        <?php else: ?>
                                            <span class="drop-val"><?php echo lang('task_centre_latest') ?></span>
                                        <?php endif ?>
                                    </button>
                                    <div class="dropdown-menu inactive-posted-tasks-order" aria-labelledby="dropdownSortButton">
                                        <a class="dropdown-item <?php echo !session('posted_inactive_order') || session('posted_inactive_order') === 'latest' ? 'active' : '' ?> gtm-inactive-sort-latest" data-sortby="latest" href="#"><?php echo lang('task_centre_latest'); ?></a>
                                        <a class="dropdown-item <?php echo session('posted_inactive_order') === 'value_high_to_low' ? 'active' : '' ?> gtm-inactive-sort-val-high-low" data-sortby="value_high_to_low" href="#"><?php echo lang('value_high_to_low'); ?></a>
                                        <a class="dropdown-item <?php echo session('posted_inactive_order') === 'value_low_to_high' ? 'active' : '' ?> gtm-inactive-sort-val-low-high" data-sortby="value_low_to_high" href="#"><?php echo lang('value_low_to_high'); ?></a>
                                        <a class="dropdown-item <?php echo session('posted_inactive_order') === 'applicants_high_to_low' ? 'active' : '' ?> gtm-inactive-sort-applicants-high-low" data-sortby="applicants_high_to_low" href="#"><?php echo lang('applicants_high_to_low'); ?></a>
                                        <a class="dropdown-item <?php echo session('posted_inactive_order') === 'applicants_low_to_high' ? 'active' : '' ?> gtm-inactive-sort-applicants-low-high" data-sortby="applicants_low_to_high" href="#"><?php echo lang('applicants_low_to_high'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tbl-collapse-group collapse show posted-tasks-list position-relative">
	                <div class='loader position-absolute hide'
	                     style='padding:0;width:100%;height:100%;background:rgba(221,221,221,35%);border-radius:10px;z-index:1;display:flex;align-items:center;align-content:center;justify-content:center'>
		                <div class='spinner-border spinner-border position-relative'
		                     style='width:50px;height:50px;border-color:#00003f;border-right-color:transparent'
		                     role='status'>
			                <span class='sr-only'><?php
                                echo lang('modal_loading') ?></span>
		                </div>
	                </div>
	                <div class='tasks-contents'>
                    <?php if( !empty( $tasks['all_posted']['inactive'] ) ): ?>
                        <?php foreach ($tasks['all_posted']['inactive'] as $task): ?>
                            <?php $task['questions'] = !empty($questions[$task['id']]) ? $questions[$task['id']][$task['id']] : []; ?>
                            <?php echo partial('workspace/partial/task-centre/posted-task-row.html.php', ['task' => $task]); ?>
                        <?php endforeach ?>
                    <?php else: ?>
	                    <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
		                    <div class="tbl-group-calendar-bar-flex">
			                    <div class="tbl-group-calendar-bar-row row-1">
				                    <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_posted_task') ?></span>
			                    </div>
		                    </div>
	                    </div>
                    <?php endif ?>
	                </div>
	                <button class='btn-icon-full btn-load-more more-posted-tasks <?php echo ($tasks['all_posted']['inactive_total'] <= 10) ? 'hide' : '' ?>' data-next-page="2"
	                        data-pages="<?php echo ceil((int)$tasks['all_posted']['inactive_total']/10) ?>"
	                        data-text="<?php echo lang('search_load_more_loading') ?>">
		                <span class='btn-label' id='loadMoreLbl' data-text="<?php echo lang('public_search_load_more'); ?>">
			                <?php echo lang('public_search_load_more'); ?>
		                </span>
		                <span class="btn-icon load-plus-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round">
                                <line x1="12" y1="5" x2="12" y2="19"></line>
                                <line x1="5" y1="12" x2="19" y2="12"></line>
                            </svg>
                        </span>
		                <span class="btn-icon load-load-icon" style="display:none;">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round">
                                <line x1="12" y1="2" x2="12" y2="6"></line>
                                <line x1="12" y1="18" x2="12" y2="22"></line>
                                <line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line>
                                <line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line>
                                <line x1="2" y1="12" x2="6" y2="12"></line>
                                <line x1="18" y1="12" x2="22" y2="12"></line>
                                <line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line>
                                <line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line>
                            </svg>
                        </span>
	                </button>
	                <button class="btn-icon-full btn-load-more tasks-back-to-top" data-scroll-to="#inactive-tc-row" style="display:none" data-text="Loading...">
		                <span class="btn-label" id="loadMoreLbl" data-text="Load More">
			                <span><?php echo lang('end_of_list') ?></span> - <?php echo lang('back_to_top') ?>
		                </span>
		                <span class="btn-icon arrowtop-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                                 class="bi bi-arrow-up-short" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                      d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5z"></path>
                            </svg>
                        </span>
		                <span class="btn-icon load-load-icon" style="display:none;">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round">
	                            <line x1="12" y1="2" x2="12" y2="6"></line>
	                            <line x1="12" y1="18" x2="12" y2="22"></line>
	                            <line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line>
	                            <line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line>
	                            <line x1="2" y1="12" x2="6" y2="12"></line>
	                            <line x1="18" y1="12" x2="22" y2="12"></line>
	                            <line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line>
	                            <line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line>
                            </svg>
                        </span>
	                </button>
                </div>
            </div>
        </div>
    </div>
</div>