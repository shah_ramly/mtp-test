<?php
$applied_pending = array_filter($tasks['applied'], function($task) use($current_user){
	return $task['apply_status'] !== 'applied' && !is_null($task['assigned']) &&
		   $task['assigned_to'] === $current_user['id'] && in_array($task['status'], ['completed', 'cancelled']);
});
?>
<div class="tab-pane tc-Pending fade" id="tcPending" role="tabpanel" aria-labelledby="tcPending-tab">
    <div class="tbl-body-calendar">
		<?php if((int)$user->info['type'] === 0): ?>
        <div class="tbl-group-calendar-row">
	        <div class="table-filter-container show">
		        <div class="tbl-group-calendar-title tbl-group-collapse-title"><span class="title-collapse"><?php echo lang('task_centre_view_applied_task'); ?></span><i class="fa fa-caret-down"></i></div>
	        </div>
	        <div class="tbl-collapse-group collapse show">
            <?php if(!empty($applied_pending)): ?>
                <?php foreach($applied_pending as $task): ?>
                    <?php echo partial('workspace/partial/task-centre/applied-task-row.html.php', ['task' => $task]); ?>
                <?php endforeach; ?>
            <?php else: ?>
		        <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
	                <div class="tbl-group-calendar-bar-flex">
		                <div class="tbl-group-calendar-bar-row row-1">
			                <span class="tbl-group-calendar-bar-title"><?php echo lang('task_centre_no_applied_task_yet'); ?>.</span>
		                </div>
	                </div>
                </div>
            <?php endif; ?>
	        </div>
        </div>
		<?php endif; ?>

        <div class="tbl-group-calendar-row">
	        <div class="table-filter-container show">
		        <div class="tbl-group-calendar-title tbl-group-collapse-title"><span class="title-collapse"><?php echo lang('task_centre_view_posted_tasks'); ?></span><i class="fa fa-caret-down"></i></div>
		        <div class="table-filter-right-col">
			        <div class="dropdown dropdown-filter-container">
				        <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					        <label class="filter-btn-lbl"><?php echo lang('task_centre_filter'); ?></label> <span class="drop-val"><?php echo lang('task_all'); ?></span>
				        </button>
				        <div class="dropdown-menu posted-tasks-filter" aria-labelledby="dropdownMenuButton">
					        <a class="dropdown-item active" href="#"><?php echo lang('task_all'); ?></a>
					        <a class="dropdown-item" href="#"><?php echo lang('task_draft'); ?></a>
					        <a class="dropdown-item" href="#"><?php echo lang('task_published'); ?></a>
				        </div>
			        </div>
		        </div>
	        </div>
	        <div class="tbl-collapse-group collapse show posted-tasks-list">
            <?php $no_tasks = true; ?>
	        <?php if(!empty($tasks['all'])): ?>
	            <?php foreach($tasks['all'] as $task): ?>
		        <?php if(!is_null($task['assigned']) || in_array($task['status'], ['completed', 'cancelled'])) continue; ?>
                    <?php echo partial('workspace/partial/task-centre/posted-task-row.html.php', ['task' => $task]); ?>
		            <?php $no_tasks = false; ?>
	            <?php endforeach; ?>
	        <?php endif ?>
	        <?php if( $no_tasks || empty($tasks['all']) ): ?>
	            <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
	                <div class="tbl-group-calendar-bar-flex">
		                <div class="tbl-group-calendar-bar-row row-1">
			                <span class="tbl-group-calendar-bar-title"><?php echo lang('task_centre_no_posted_task_yet'); ?>.</span>
		                </div>
	                </div>
	            </div>
	        <?php endif; ?>
	        </div>
        </div>
    </div>
</div>