<?php
$dateObject = $data['date'];
$now       = $dateObject->copy()->now();
$sameDay   = $now->isSameDay($dateObject->copy()->parse($data['current_date']));
$sameMonth = $now->isSameMonth($dateObject->copy()->parse($data['current_date']));
$sameYear  = $now->isSameYear($dateObject->copy()->parse($data['current_date']));
$current_date = $dateObject->copy()->parse($data['current_date'])->firstOfMonth()->startOfDay();

$applied_active = array_filter($tasks['all_applied']['active'], function($task){ return in_array($task['apply_status'], ['in-progress', 'marked-completed', 'completed']); });
$applied_inactive = array_filter($tasks['all_applied']['inactive'], function($task) { return in_array($task['apply_status'], ['closed', 'disputed']); });
$posted_active = array_filter($tasks['all_posted']['active'], function($task){ return in_array($task['status'], ['in-progress', 'marked-completed', 'completed']); });
$posted_inactive = array_filter($tasks['all_posted']['inactive'], function($task){ return in_array($task['status'], ['closed', 'disputed']); });

?>

<div class="tab-content" id="pills-tcCalendarView">
	<?php echo partial('/workspace/partial/task-centre/applied-tab-calendar-view.html.php', compact('applied_active', 'applied_inactive', 'dateObject', 'section')) ?>
	<?php echo partial('/workspace/partial/task-centre/posted-tab-calendar-view.html.php', compact('posted_active', 'posted_inactive', 'dateObject', 'section')) ?>
</div>
<div class="calendar-float-filter">
	<div class="btn-group btn-hdmy-filter dropleft">
		<button type="button" class="btn btn-secondary dropdown-toggle gtm-tc-calendar-filter-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="calender-week-icon"></span>
		</button>
		<div class="dropdown-menu">
			<div class="hdmy-title">View by</div>
			<a href="<?php echo url_for("/workspace/task-centre/{$section}/calendar") ?>?show=hour" class="hdmy-link hourly-link gtm-tc-calendar-filter-hour <?php echo $calendar_view === 'hour' ? 'active' : '' ?>">Hour</a>
			<a href="<?php echo url_for("/workspace/task-centre/{$section}/calendar") ?>?show=day" class="hdmy-link daily-link gtm-tc-calendar-filter-day <?php echo $calendar_view === 'day' ? 'active' : '' ?>">Day</a>
			<a href="<?php echo url_for("/workspace/task-centre/{$section}/calendar") ?>?show=week" class="hdmy-link weekly-link gtm-tc-calendar-filter-week <?php echo $calendar_view === 'week' ? 'active' : '' ?>">Week</a>
			<a href="<?php echo url_for("/workspace/task-centre/{$section}/calendar") ?>" class="hdmy-link monthly-link gtm-tc-calendar-filter-month <?php echo $calendar_view === 'month' ? 'active' : '' ?>">Month</a>
		</div>
	</div>
</div>

<?php content_for('modals') ?>
<?php echo partial('/workspace/partial/task-centre/task-planning-modal.html.php') ?>
<?php end_content_for() ?>