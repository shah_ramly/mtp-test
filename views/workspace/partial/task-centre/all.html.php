<div class="tab-pane tc-All fade <?php echo !isset($current_tab) || $current_tab === '#tcAll' ? 'show active' : '' ?>" id="tcAll" role="tabpanel" aria-labelledby="tcAll-tab">
    <div class="tbl-body-calendar">
		<?php if((int)$user->info['type'] === 0): ?>
        <div class="tbl-group-calendar-row">
            <div class="table-filter-container show">
                <div class="tbl-group-calendar-title tbl-group-collapse-title"><span class="title-collapse"><?php echo lang('task_view_applied_task'); ?></span><i class="fa fa-caret-down"></i></div>
                <div class="table-filter-right-col">
                    <div class="dropdown dropdown-filter-container">
                        <button class="btn btn-dropdown-label dropdown-toggle gtm-view-applied-filter-btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <label class="filter-btn-lbl"><?php echo lang('task_filter_title'); ?></label> <span class="drop-val"><?php echo lang('task_all'); ?></span>
                        </button>
                        <div class="dropdown-menu applied-tasks-filter" aria-labelledby="dropdownMenuButton">
	                        <a class="dropdown-item active gtm-view-applied-filter-all" data-filter="view-all" href="#"><?php echo lang('task_all'); ?></a>
	                        <a class="dropdown-item gtm-view-applied-filter-applied" href="#"><?php echo lang('task_applied'); ?></a>
	                        <a class="dropdown-item gtm-view-applied-filter-inprogress" href="#"><?php echo lang('task_in_progress'); ?></a>
	                        <a class="dropdown-item gtm-view-applied-filter-completed" href="#"><?php echo lang('task_completed'); ?></a>
	                        <a class="dropdown-item gtm-view-applied-filter-closed" href="#"><?php echo lang('task_closed'); ?></a>
	                        <a class="dropdown-item gtm-view-applied-filter-cancelled" href="#"><?php echo lang('task_cancelled'); ?></a>
	                        <!--<a class="dropdown-item" href="#"><?php /*echo lang('task_lost'); */?></a>-->
	                        <a class="dropdown-item gtm-view-applied-filter-disputed" href="#"><?php echo lang('task_disputed'); ?></a>
                        </div>
                    </div>
	                <div class="dropdown dropdown-filter-container dropdown-sort-container">
		                <button class="btn btn-dropdown-label dropdown-toggle gtm-view-applied-sort-btn" type="button" id="dropdownSortButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			                <label class="filter-btn-lbl"><?php echo lang('home_sort_by'); ?></label> <span class="drop-val">Latest</span>
		                </button>
		                <div class="dropdown-menu" aria-labelledby="dropdownSortButton">
			                <a class="dropdown-item active gtm-view-applied-sort-latest" href="#"><?php echo lang('task_centre_latest'); ?></a>
			                <a class="dropdown-item gtm-view-applied-sort-val-high-low" href="#"><?php echo lang('value_high_to_low'); ?></a>
			                <a class="dropdown-item gtm-view-applied-sort-val-low-high" href="#"><?php echo lang('value_low_to_high'); ?></a>
		                </div>
	                </div>
                </div>
            </div>
            <div class="tbl-collapse-group collapse show applied-tasks-list">
                <?php if(!empty($tasks['applied'])): ?>
                    <?php foreach($tasks['applied'] as $task): ?>
                        <?php echo partial('workspace/partial/task-centre/applied-task-row.html.php', ['task' => $task]); ?>
                    <?php endforeach; ?>
                <?php else: ?>
	                <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
		                <div class="tbl-group-calendar-bar-flex">
			                <div class="tbl-group-calendar-bar-row row-1">
				                <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_task'); ?></span>
			                </div>
		                </div>
	                </div>
                <?php endif; ?>
            </div>
        </div>
		<?php endif; ?>

        <div class="tbl-group-calendar-row">
            <div class="table-filter-container show">
                <div class="tbl-group-calendar-title tbl-group-collapse-title"><span class="title-collapse"><?php echo lang('task_view_posted_task'); ?></span><i class="fa fa-caret-down"></i></div>
                <div class="table-filter-right-col">
                    <div class="dropdown dropdown-filter-container">
                        <button class="btn btn-dropdown-label dropdown-toggle gtm-view-posted-filter-btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <label class="filter-btn-lbl"><?php echo lang('task_filter_title'); ?></label> <span class="drop-val"><?php echo lang('task_all'); ?></span>
                        </button>
                        <div class="dropdown-menu posted-tasks-filter" aria-labelledby="dropdownMenuButton">
	                        <a class="dropdown-item active gtm-view-posted-filter-all" data-filter="view-all" href="#"><?php echo lang('task_all'); ?></a>
	                        <a class="dropdown-item gtm-view-posted-filter-draft" href="#"><?php echo lang('task_draft'); ?></a>
	                        <a class="dropdown-item gtm-view-posted-filter-published" href="#"><?php echo lang('task_published'); ?></a>
	                        <a class="dropdown-item gtm-view-posted-filter-inprogress" href="#"><?php echo lang('task_in_progress'); ?></a>
	                        <a class="dropdown-item gtm-view-posted-filter-completed" href="#"><?php echo lang('task_completed'); ?></a>
	                        <a class="dropdown-item gtm-view-posted-filter-closed" href="#"><?php echo lang('task_closed'); ?></a>
	                        <a class="dropdown-item gtm-view-posted-filter-cancelled" href="#"><?php echo lang('task_cancelled'); ?></a>
	                        <a class="dropdown-item gtm-view-posted-filter-disputed" href="#"><?php echo lang('task_disputed'); ?></a>
                        </div>
                    </div>
	                <div class="dropdown dropdown-filter-container dropdown-sort-container">
		                <button class="btn btn-dropdown-label dropdown-toggle gtm-view-posted-sort-btn" type="button" id="dropdownSortButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			                <label class="filter-btn-lbl"><?php echo lang('home_sort_by'); ?></label> <span class="drop-val"><?php echo lang('task_centre_latest'); ?></span>
		                </button>
		                <div class="dropdown-menu" aria-labelledby="dropdownSortButton">
			                <a class="dropdown-item active gtm-view-posted-latest" href="#"><?php echo lang('task_centre_latest'); ?></a>
			                <a class="dropdown-item gtm-view-posted-val-high-low" href="#"><?php echo lang('value_high_to_low'); ?></a>
			                <a class="dropdown-item gtm-view-posted-val-low-high" href="#"><?php echo lang('value_low_to_high'); ?></a>
			                <a class="dropdown-item gtm-view-posted-applicants-high-low" href="#"><?php echo lang('applicants_high_to_low'); ?></a>
			                <a class="dropdown-item gtm-view-posted-applicants-low-high" href="#"><?php echo lang('applicants_low_to_high'); ?></a>
		                </div>
	                </div>
                </div>
            </div>
            <div class="tbl-collapse-group show posted-tasks-list">
                <?php if(!empty($tasks['all'])): ?>
                    <?php foreach($tasks['all'] as $task): ?>
	                <?php $task['questions'] = !empty($questions[$task['id']]) ? $questions[$task['id']][$task['id']] : []; ?>
                        <?php echo partial('workspace/partial/task-centre/posted-task-row.html.php', ['task' => $task]); ?>
                    <?php endforeach; ?>
                <?php else: ?>
	                <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
		                <div class="tbl-group-calendar-bar-flex">
			                <div class="tbl-group-calendar-bar-row row-1">
				                <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_posted_task') ?></span>
			                </div>
		                </div>
	                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php content_for('extra_modals'); ?>
<div id="modal_tc_retract" class="modal modal-milestone-opt modal-tc-retract fade show" aria-labelledby="modal_tc_retract" tabindex="-1" role="dialog" aria-modal="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php echo lang('task_centre_retract_application') ?></div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<p class="modal-para"><?php echo lang('you_are_retracting_your_application') ?></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-icon-full btn-cancel gtm-modal-retract-cancel" data-dismiss="modal" data-toggle="modal">
					<span class="btn-label"><?php echo lang('modal_cancel') ?></span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
				</button>
				<form method="post" action="<?php echo url_for('workspace/task/retract_application') ?>">
					<?php echo html_form_token_field() ?>
					<input type="hidden" name="task_id" value="" />
					<input type="hidden" name="user_id" value="" />
					<button type="submit" class="btn-icon-full btn-confirm gtm-modal-retract-confirm" data-orientation="next">
						<span class="btn-label"><?php echo lang('modal_confirm') ?></span>
						<span class="btn-icon">
	                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                            <polyline points="20 6 9 17 4 12"></polyline>
	                        </svg>
	                    </span>
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Modal - Milestone Seeker -->
<div id="modal_milestone_seeker" class="modal modal-milestone fade" aria-labelledby="modal_milestone_seeker" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php echo lang('modal_progress') ?></div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="milestone-progress-content">
				<div class="modal-body" style="padding-top:7.2rem;padding-bottom:7.2rem;text-align:center">
					<div class="spinner-border spinner-border" style="width:5rem;height:5rem" role="status">
						<span class="sr-only"><?php echo lang('modal_loading') ?></span>
					</div>
				</div>
				<div class="modal-footer">

				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal - Milestone Seeker -->

<!-- Modal - Milestone Poster -->
<div id="modal_milestone_poster" class="modal modal-milestone fade" aria-labelledby="modal_milestone_poster" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php echo lang('modal_progress') ?></div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="milestone-progress-content">
				<div class="modal-body" style="padding-top:7.2rem;padding-bottom:7.2rem;text-align:center">
					<div class="spinner-border spinner-border" style="width:5rem;height:5rem" role="status">
                        <span class="sr-only"><?php echo lang('modal_loading') ?></span>
                    </div>
				</div>
				<div class="modal-footer">

				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal - Milestone Poster -->
<?php end_content_for(); ?>

<?php content_for('extra_scripts'); ?>
<script>
    $('.applied-tasks-list').on("click", ".btn-progress", function() {
        var button = $(this);
        var progressSeekerID = button.data('id');
        var task = button.data('taskId');
        var poster = button.data('userId');
        var task_status = button.data('taskState');
        var originalContent = $($('#modal_milestone_seeker .milestone-progress-content').html()).clone();

        $("#modal_milestone_seeker .modal-dialog").addClass(progressSeekerID);

        $.ajax({
            method: 'POST',
            url: '<?php echo url_for("workspace/milestone-progress") ?>',
            data: {'task': task, 'poster': poster, 'task_status': task_status, 'progress_status': progressSeekerID },
        }).done(function(response){
            $('#modal_milestone_seeker .milestone-progress-content').html(response);
            if( $(response).find('span.hide-status').length ){
                button.removeClass('active');
                if( button.siblings('.active').length === 0 && button.parent().find('.active').length === 0 ) {
                    button.closest('.applied-task-row').removeClass('active-row');
                }
            }
        });

        $('#modal_milestone_poster').on('hidden.bs.modal', function() {
            $(".modal-dialog").removeClass(progressSeekerID);
            $('#modal_milestone_seeker .milestone-progress-content').html(originalContent);
        });

        $('#modal_milestone_seeker').on('hidden.bs.modal', function() {
            $("#modal_milestone_seeker .modal-dialog").removeClass(progressSeekerID);
            $('#modal_milestone_seeker .milestone-progress-content').html(originalContent);
        });
    });

    $('.posted-tasks-list').on("click", ".applicant-progress-link", function(e) {
        var $this = $(this);
        var progressPosterID = $this.data('id');
        var task = $this.data('taskId');
        var seeker = $this.data('seekerId');
        var task_status = $this.data('taskState');
        var originalContent = $($('#modal_milestone_poster .milestone-progress-content').html()).clone();

        $("#modal_milestone_poster .modal-dialog").addClass(progressPosterID);
        $this.closest('tr').children('td').first().removeClass('new-applicant');

        $.ajax({
	        method: 'POST',
	        url: '<?php echo url_for("workspace/milestone-progress") ?>',
	        data: {'task': task, 'seeker': seeker, 'task_status': task_status },
        }).done(function(response){
            $('#modal_milestone_poster .milestone-progress-content').html(response);
        });

        $('#modal_milestone_poster').on('hidden.bs.modal', function() {
            $("#modal_milestone_poster .modal-dialog").removeClass(progressPosterID);
            $('#modal_milestone_poster .milestone-progress-content').html(originalContent);
        });
    });

	$('#modal_milestone_poster').on('show.bs.modal', function(e){
	    var modal = $(this);
	    var target = $(e.relatedTarget);
	    var id = target.data('id');
	    var content = modal.find('.modal-dialog');
	    window.poster_modal = content.clone();
	    content.addClass(id);
    });

	$('#modal_milestone_poster').on('hidden.bs.modal', function(e){
	    var modal = $(this);
	    var content = window.poster_modal;
	    window.poster_modal = undefined;
	    modal.find('.modal-dialog').html(content.html());
	});

	$('#modal_milestone_seeker').on('show.bs.modal', function(e){
	    var modal = $(this);
	    var target = $(e.relatedTarget);
	    var id = target.data('id');
        var content = modal.find('.modal-dialog');
        window.seeker_modal = content.clone();
	    content.addClass(id);
    });

	$('#modal_milestone_seeker').on('hidden.bs.modal', function(e){
	    var modal = $(this);
	    var content = window.seeker_modal;
	    window.seeker_modal = undefined;
	    modal.find('.modal-dialog').html(content.html());
	});

	$('#modal_tc_retract').on('show.bs.modal', function(e){
        var modal = $(this);
        var target = $(e.relatedTarget);
        var task = target.data('taskId');
        var user = target.data('userId');

        modal.find('input[name="task_id"]').val(task);
        modal.find('input[name="user_id"]').val(user);
	});

    $('.modal-dialog').on('change', '[name="milestone_close_poster"]', function() {
        milestonePercentage(this);
    });

    $('.modal-dialog').on('change', '#disputePoster_rejectionRateNumerator', function() {
        var percentage = $(this).val();
        var content = $(this).closest('.milestone-radio-dispute-percentage');
        var price = content.find('[name="task_price"]').val();
        $('.modal-dialog').find('[name="percentage"]').val(percentage);
        if( percentage > 0 && price ){
            var pay = (percentage/100) * price;
            content.find('.to-be-paid').text('RM ' + pay.toFixed(2));
            content.find('.balance').text('RM ' + (price - pay).toFixed(2));
        }else{
            content.find('.to-be-paid').text('RM 0.00');
            content.find('.balance').text('RM ' + parseInt(price, 10).toFixed(2));
        }
    });

    function milestonePercentage(ele) {
        var text = $(ele).closest('.milestone-content').find('#milestoneDisputeOpt');
        var content = $(ele).closest('.milestone-progress-content');

        if ($(ele).prop("checked") == true && $(ele).val() == 'no') {
            content.find('[name="dispute"]').val('yes');
            text.show();
        } else {
            content.find('[name="dispute"]').val('no');
            text.hide();
            $(ele).closest('.milestone-content').find('#milestoneDisputePercent').hide();
            $(ele).closest('.milestone-content').find('#selectRejectOption').children().first().prop('selected', true);
        }
    }

    $('.modal-dialog').on('change', '#selectRejectOption', (function() {
        var val = $(this).val();
        if (val === "milestone-reject-1") {
            $('#milestoneDisputePercent').show();
        } else if (val === "milestone-reject-2") {
            $('#milestoneDisputePercent').show();
        }
        $('.modal-dialog').find('[name="reason"]').val(val);
    }));

	$('.applicants-list').on('click', '.applicant-pin-link', function(e){
	   e.preventDefault();
	   var pinned = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pin-angle-fill" viewBox="0 0 16 16">'+
                        '<path d="M9.828.722a.5.5 0 0 1 .354.146l4.95 4.95a.5.5 0 0 1 0 .707c-.48.48-1.072.588-1.503.588-.177 0-.335-.018-.46-.039l-3.134 3.134a5.927 5.927 0 0 1 .16 1.013c.046.702-.032 1.687-.72 2.375a.5.5 0 0 1-.707 0l-2.829-2.828-3.182 3.182c-.195.195-1.219.902-1.414.707-.195-.195.512-1.22.707-1.414l3.182-3.182-2.828-2.829a.5.5 0 0 1 0-.707c.688-.688 1.673-.767 2.375-.72a5.92 5.92 0 0 1 1.013.16l3.134-3.133a2.772 2.772 0 0 1-.04-.461c0-.43.108-1.022.589-1.503a.5.5 0 0 1 .353-.146z" />'+
                    '</svg>';
	   var unpinned = '<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-pin-angle" viewBox="0 0 16 16">'+
				         '<path d="M9.828.722a.5.5 0 0 1 .354.146l4.95 4.95a.5.5 0 0 1 0 .707c-.48.48-1.072.588-1.503.588-.177 0-.335-.018-.46-.039l-3.134 3.134a5.927 5.927 0 0 1 .16 1.013c.046.702-.032 1.687-.72 2.375a.5.5 0 0 1-.707 0l-2.829-2.828-3.182 3.182c-.195.195-1.219.902-1.414.707-.195-.195.512-1.22.707-1.414l3.182-3.182-2.828-2.829a.5.5 0 0 1 0-.707c.688-.688 1.673-.767 2.375-.72a5.92 5.92 0 0 1 1.013.16l3.134-3.133a2.772 2.772 0 0 1-.04-.461c0-.43.108-1.022.589-1.503a.5.5 0 0 1 .353-.146zm.122 2.112v-.002zm0-.002v.002a.5.5 0 0 1-.122.51L6.293 6.878a.5.5 0 0 1-.511.12H5.78l-.014-.004a4.507 4.507 0 0 0-.288-.076 4.922 4.922 0 0 0-.765-.116c-.422-.028-.836.008-1.175.15l5.51 5.509c.141-.34.177-.753.149-1.175a4.924 4.924 0 0 0-.192-1.054l-.004-.013v-.001a.5.5 0 0 1 .12-.512l3.536-3.535a.5.5 0 0 1 .532-.115l.096.022c.087.017.208.034.344.034.114 0 .23-.011.343-.04L9.927 2.028c-.029.113-.04.23-.04.343a1.779 1.779 0 0 0 .062.46z"></path>'+
				      '</svg>'
	   var target = $(e.currentTarget);
	   var hired = target.data('status');
	   var seeker = target.data('seekerId');
	   var task = target.data('taskId');

	   target.addClass('disabled-link');
	   $('.applicants-list').css('cursor', 'progress');
	   target.closest('tr').children('td').first().removeClass('new-applicant');

	   $.ajax({
		   url: '<?php echo option("site_uri") . url_for("workspace/task/applicant/pin") ?>',
		   method: 'POST',
		   data: { 'hired': hired, 'seeker': seeker, 'task': task }
	   }).done(function(response){
	       if( response.status === 'success' ){
               if( target.find('.bi-pin-angle').length ){
                   target.find('span.applicant-pin-icon').html(pinned);
               }else{
                   target.find('span.applicant-pin-icon').html(unpinned);
               }
	       }
           $('.applicants-list').removeAttr('style');
           target.removeClass('disabled-link');
	   });

	});
</script>
<?php end_content_for(); ?>
