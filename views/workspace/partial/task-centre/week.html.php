<?php
$now       = \Carbon\Carbon::now();
$sameMonth = $now->isSameMonth(\Carbon\Carbon::parse($data['current_date']));
$sameYear  = $now->isSameYear(\Carbon\Carbon::parse($data['current_date']));
$date      = \Carbon\Carbon::parse($data['current_date'])->startOfMonth();
$applied_in_progress = array_filter($tasks['applied'], function($task) use($current_user){ return $task['assigned_to'] === $current_user['id'] && in_array($task['status'], ['in-progress', 'marked-completed']); });
$assigned_in_progress = array_filter($tasks['assigned'], function($task){ return in_array($task['status'], ['in-progress', 'marked-completed']); });

$applied_in_progress = array_filter($applied_in_progress, function($task) use ($date){ return $date->isBetween(\Carbon\Carbon::parse($task['start_by'])->startOfMonth(), \Carbon\Carbon::parse($task['complete_by'])->endOfMonth()); });
$assigned_in_progress = array_filter($assigned_in_progress, function($task) use ($date){ return $date->isBetween(\Carbon\Carbon::parse($task['start_by'])->startOfMonth(), \Carbon\Carbon::parse($task['complete_by'])->endOfMonth()); });

$week_start_date = isset($week_start_date) ? $week_start_date : null;
?>
<div class="tab-pane tc-InProgress tc-InProgress-Week fade <?php echo isset($current_tab) && $current_tab === '#tcInProgressWeek' ? 'show active' : '' ?>" id="tcInProgressWeek" role="tabpanel" aria-labelledby="tcInProgressWeek-tab">
    <div class="row">
        <div id="colLeftCard" class="col-sm-6 col-left-card-flex">
            <div class="tbl-header-calendar tbl-header-flex">
                <div class="tbl-header-row row-1"><?php echo lang('task_title'); ?></div>
                <div class="tbl-header-row row-2"><?php echo lang('task_value'); ?>
                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('task_value_description'); ?>">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                        </svg>
                    </span>
                </div>
                <div class="tbl-header-row row-3"><?php echo lang('task_duration'); ?>
                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('task_duration_description'); ?>">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                        </svg>
                    </span>
                </div>
                <div class="tbl-header-row row-4"><?php /*echo lang('task_pix'); */?>
                    <!--span class="bi-tooltip" data-toggle="tooltip" data-placement="bottom" title="Person in Charge. The owner or the person assigned to complete the task">
                        <svg class="bi bi-question-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                            <path d="M5.25 6.033h1.32c0-.781.458-1.384 1.36-1.384.685 0 1.313.343 1.313 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.007.463h1.307v-.355c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.326 0-2.786.647-2.754 2.533zm1.562 5.516c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z" />
                        </svg>
                    </span-->
                </div>
            </div>
            <div class="tbl-body-calendar">
				<?php if((int)$user->info['type'] === 0): ?>
                <div class="tbl-group-calendar-row tbl-group-calendar-awarded-row">
                    <div class="tbl-group-calendar-title"><?php echo lang('task_view_awarded_tasks'); ?></div>
                    <div class="tbl-group-calendar-bar-wrapper">
                        <?php if(!empty($applied_in_progress)): ?>
                            <?php $counter = 1; ?>
                            <?php foreach($applied_in_progress as $task): ?>
	                        <?php
                                $current_date = \Carbon\Carbon::parse($data['current_date'])->firstOfMonth()->startOfDay();
                                $start_by = \Carbon\Carbon::parse($task['start_by'])->startOfDay();
                                $complete_by = \Carbon\Carbon::parse($task['complete_by'])->endOfDay();
	                        ?>
                                <?php if(
                                        $current_date->isBetween($start_by->copy()->startOfMonth(), $complete_by->copy()->endOfMonth())
                                		/*($start_by->isSameMonth($current_date) && $start_by->isSameYear($current_date)) ||
                                        ($complete_by->isSameMonth($current_date) && $complete_by->isSameYear($current_date))*/
		                        ): ?>
                                    <?php echo partial('workspace/partial/task-centre/in-progress-task-row.html.php', ['task' => $task, 'counter' => $counter, 'section' => 'awarded']); ?>
                                    <?php $counter++; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php else: ?>
	                        <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
				                <div class="tbl-group-calendar-bar-flex">
					                <div class="tbl-group-calendar-bar-row row-1">
						                <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_applied_task'); ?></span>
					                </div>
				                </div>
			                </div>
                        <?php endif; ?>
                    </div>
                </div>
				<?php endif; ?>
                <div class="tbl-group-calendar-row tbl-group-calendar-created-row">
                    <div class="tbl-group-calendar-title"><?php echo lang('task_view_posted_task'); ?></div>
                    <div class="tbl-group-calendar-bar-wrapper">
                        <?php if(!empty($assigned_in_progress)): ?>
                            <?php if(!isset($counter)) $counter = 1; ?>
                            <?php foreach($assigned_in_progress as $task): ?>
                                <?php
                                $current_date = \Carbon\Carbon::parse($data['current_date'])->firstOfMonth()->startOfDay();
                                $start_by = \Carbon\Carbon::parse($task['start_by'])->startOfDay();
                                $complete_by = \Carbon\Carbon::parse($task['complete_by'])->endOfDay();
                                ?>
                                <?php if(
                                		$current_date->isBetween($start_by->copy()->startOfMonth(), $complete_by->copy()->endOfMonth())
                                		/*($start_by->isSameMonth($current_date) && $start_by->isSameYear($current_date)) ||
			                             ($complete_by->isSameMonth($current_date) && $complete_by->isSameYear($current_date))*/
		                        ): ?>
                                    <?php echo partial('workspace/partial/task-centre/in-progress-task-row.html.php', ['task' => $task, 'counter' => $counter, 'section' => 'created']); ?>
                                    <?php $counter++; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php else: ?>
	                        <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
				                <div class="tbl-group-calendar-bar-flex">
					                <div class="tbl-group-calendar-bar-row row-1">
						                <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_posted_task'); ?></span>
					                </div>
				                </div>
			                </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="colRightCard" class="col-sm-6 col-right-card-flex card-calendar-week">
            <div class="tbl-header-calendar-gantt-qtr">
	            <a class="r1-icon r1-icon-left" href="<?php echo $data['prev_month'] ?>">
		            <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			            <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
		            </svg>
	            </a>
	            <div class="tbl-header-calendar-gantt-qtr-row qtr-1" style="width:auto"><?php echo "{$data['current_month_name']}, {$data['current_year']}"; ?></div>
	            <a class="r1-icon r1-icon-left" href="<?php echo $data['next_month'] ?>">
		            <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
		            </svg>
	            </a>
            </div>
            <div class="tbl-header-calendar-gantt-body-overflow-wrapper dragscroll test">
                <div class="tbl-header-calendar-gantt-body-overflow">
                    <div class="tbl-header-calendar-gantt-body-overflow-container">
                        <div class="tbl-header-calendar-gantt-wrapper">
                            <div class="tbl-header-calendar-gantt-month">
                                <?php
                                $temp_date = $date->copy()->startOfMonth();
                                $weeks_in_month = $temp_date->copy()->endOfMonth()->weekNumberInMonth;
                                foreach (range(1, $weeks_in_month) as $week): ?>
                                    <?php if($temp_date->month !== $date->month) break; ?>
		                            <div class="calendar-gantt-week-container">
			                            <div class="tbl-header-calendar-gantt-week-row"><?php echo lang('task_week'); ?> <?php echo $week ?></div>
			                            <div class="tbl-header-calendar-gantt-day">
                                            <?php for ($i=0; $i<7;$i++): ?>
					                            <div class="tbl-header-calendar-gantt-week-row-day day-<?php echo $temp_date->day ?>">
                                                    <?php echo $temp_date->localeDayOfWeek ?> <sup>(<?php echo $temp_date->day ?>)</sup>
					                            </div>
                                                <?php
	                                            $temp_date->addDay();
	                                            if(!$temp_date->isSameMonth($date)) break;
                                                if($temp_date->isDayOfWeek(\Carbon\Carbon::SATURDAY)) continue;
                                                ?>
                                            <?php endfor; ?>
			                            </div>
		                            </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="tbl-body-calendar-gantt">
							<?php if((int)$user->info['type'] === 0): ?>
                            <div class="tbl-group-calendar-gantt-row tbl-group-calendar-awarded-row">
                                <div class="tbl-group-calendar-gantt-grid-wrapper">
                                    <div class="tbl-group-calendar-gantt-grid">
	                                    <?php $temp_date = $date->copy()->startOfMonth(); ?>
                                        <?php foreach (range(1, \Carbon\Carbon::parse($data['current_date'])->daysInMonth) as $day): ?>
		                                    <div class="gantt-grid-week-day-row gantt-grid-row-<?php echo $day ?> <?php if(($day === $now->day) && $sameMonth && $sameYear) echo 'active' ?>"></div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="tbl-group-calendar-gantt-bar-wrapper">
                                    <?php if(!empty($applied_in_progress)): ?>
                                        <?php if(!isset($counter)) $counter = 1; ?>
                                        <?php foreach($applied_in_progress as $task): ?>
                                            <?php
                                            $current_date = \Carbon\Carbon::parse($data['current_date'])->startOfMonth()->startOfDay();
                                            $start_by = \Carbon\Carbon::parse($task['start_by'])->startOfDay();
                                            $complete_by = \Carbon\Carbon::parse($task['complete_by'])->endOfDay();
                                            ?>
                                            <?php if(
                                            		$current_date->isBetween($start_by->copy()->startOfMonth(), $complete_by->copy()->endOfMonth())
                                            		/*($start_by->isSameMonth($current_date) && $start_by->isSameYear($current_date)) ||
			                                        ($complete_by->isSameMonth($current_date) && $complete_by->isSameYear($current_date))*/
		                                    ): ?>
                                                <?php
                                                $start_day = $start_by->isSameMonth($current_date) ? $start_by->day : 1;
                                                $length = $start_by->isSameMonth($current_date) ? $complete_by->diffInRealHours($start_by)/24 :
                                                    ( $complete_by->isSameMonth($current_date) ? $complete_by->day : $current_date->daysInMonth );
                                                ?>
                                                <?php echo partial('workspace/partial/task-centre/week-calendar-task-bar.html.php',
                                                    [
                                                        'task'      => $task,
                                                        'counter'   => $counter,
                                                        'start_day' => $start_day,
                                                        'length'    => $length
                                                    ]) ?>
                                                <?php $counter++; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
	                                    <div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-completed bar-row-1">
		                                    <div class="tbl-group-calendar-gantt-bar-total-wrapper empty-bar"></div>
	                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
							<?php endif; ?>
                            <div class="tbl-group-calendar-gantt-row tbl-group-calendar-created-row">
                                <div class="tbl-group-calendar-gantt-grid-wrapper">
                                    <div class="tbl-group-calendar-gantt-grid">
	                                    <?php $temp_date = $date->copy()->startOfMonth(); ?>
                                        <?php foreach (range(1, \Carbon\Carbon::parse($data['current_date'])->daysInMonth) as $day): ?>
		                                    <div class="gantt-grid-week-day-row gantt-grid-row-<?php echo $day ?> <?php if(($day === $now->day) && $sameMonth && $sameYear) echo 'active' ?>"></div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="tbl-group-calendar-gantt-bar-wrapper">
                                    <?php if(!empty($assigned_in_progress)): ?>
                                        <?php if(!isset($counter)) $counter = 1; ?>
                                        <?php foreach($assigned_in_progress as $task): ?>
                                            <?php
                                            $current_date = \Carbon\Carbon::parse($data['current_date'])->firstOfMonth()->startOfDay();
                                            $start_by = \Carbon\Carbon::parse($task['start_by'])->startOfDay();
                                            $complete_by = \Carbon\Carbon::parse($task['complete_by'])->endOfDay();
                                            ?>
                                            <?php if(
                                            		$current_date->isBetween($start_by->copy()->startOfMonth(), $complete_by->copy()->endOfMonth())
                                            		/*($start_by->isSameMonth($current_date) && $start_by->isSameYear($current_date)) ||
			                                        ($complete_by->isSameMonth($current_date) && $complete_by->isSameYear($current_date))*/
		                                    ): ?>
                                                <?php
			                                    $start_day = $start_by->isSameMonth($current_date) ? $start_by->day : 1;
			                                    $length = $start_by->isSameMonth($current_date) ? $complete_by->diffInRealHours($start_by)/24 :
				                                          ( $complete_by->isSameMonth($current_date) ? $complete_by->day : $current_date->daysInMonth );
			                                    ?>
                                                <?php set('section', 'posted') ?>
                                                <?php echo partial('workspace/partial/task-centre/week-calendar-task-bar.html.php',
                                                    [
                                                        'task'      => $task,
                                                        'counter'   => $counter,
                                                        'start_day' => $start_day,
                                                        'length'    => $length
                                                    ]) ?>
                                                <?php $counter++; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
	                                    <div class="tbl-group-calendar-gantt-bar-row-wrapper bar-row-completed bar-row-1">
		                                    <div class="tbl-group-calendar-gantt-bar-total-wrapper empty-bar"></div>
	                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php content_for('modals') ?>
<?php echo partial('/workspace/partial/task-centre/task-planning-modal.html.php') ?>
<?php end_content_for() ?>

<?php if( isset($week_start_date) && $week_start_date->isSameYear($current_date) ): ?>
<script>
    window.addEventListener('load', function() {
        setTimeout(function(){
            $('.dragscroll.test')[0].scrollTo({
                top:0, left:<?php echo ($week_start_date->copy()->day === 1) ? 0 : abs($week_start_date->copy()->subDay()->day - $temp_date->day) * 100 ?>, behavior:'smooth'
            });
        }, 500);
    });
</script>
<?php endif ?>
