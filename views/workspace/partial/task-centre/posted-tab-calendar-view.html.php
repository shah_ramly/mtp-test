<?php
$current_month = $dateObject->month;
$date = $dateObject->copy()->startOfYear();
$counter = $counter ?? 1;
/*if( $calendar_view === 'hour' ){
	$posted_active = array_filter($posted_active, function($task) use($dateObject){ return $dateObject->copy()->parse($task['start_by'])->isBetween($dateObject->copy()->subDay()->startOfDay(), $dateObject->copy()->addDay()->endOfDay()); });
	$posted_inactive = array_filter($posted_inactive, function($task) use($dateObject){ return $dateObject->copy()->parse($task['start_by'])->isBetween($dateObject->copy()->subDay()->startOfDay(), $dateObject->copy()->addDay()->endOfDay()); });
}elseif( $calendar_view === 'day' ){
	$posted_active = array_filter($posted_active, function($task) use($dateObject){ return $dateObject->copy()->parse($task['start_by'])->isBetween($dateObject->copy()->subWeeks(2)->startOfDay(), $dateObject->copy()->addWeeks(2)->endOfDay()); });
	$posted_inactive = array_filter($posted_inactive, function($task) use($dateObject){ return $dateObject->copy()->parse($task['start_by'])->isBetween($dateObject->copy()->subWeeks(2)->startOfDay(), $dateObject->copy()->addWeeks(2)->endOfDay()); });
}elseif( $calendar_view === 'week' ){
	$posted_active = array_filter($posted_active, function($task) use($dateObject){ return $dateObject->copy()->parse($task['start_by'])->isBetween($dateObject->copy()->subMonth()->startOfDay(), $dateObject->copy()->addMonth()->endOfDay()); });
	$posted_inactive = array_filter($posted_inactive, function($task) use($dateObject){ return $dateObject->copy()->parse($task['start_by'])->isBetween($dateObject->copy()->subMonth()->startOfDay(), $dateObject->copy()->addMonth()->endOfDay()); });
}*/

?>
<div class="tab-pane tc-InProgress tc-InProgress-Month fade <?php echo $section === 'posted' ? 'show active' : '' ?>"
     id="tcInProgressPosted" role="tabpanel" aria-labelledby="tcInProgressPosted-tab">
    <div class="row">
        <div id="colLeftCard" class="col-sm-6 col-left-card-flex">
            <div class="tbl-body-calendar">
                <div class="tbl-group-calendar-row tbl-group-calendar-awarded-row tbl-group-calendar-active-row">
                    <div class="tbl-group-calendar-title"><?php echo lang('task_centre_active_task') ?></div>
                    <div class="tbl-group-calendar-bar-wrapper posted-tasks-list">
                        <?php if(!empty($posted_active)): ?>
                            <?php if(!isset($counter)) $counter = 1; ?>
                            <?php foreach($posted_active as $task): ?>
                                <?php echo partial('/workspace/partial/task-centre/posted-task-calendar-view.html.php', compact('task', 'counter')) ?>
                                <?php $counter++ ?>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
                                <div class="tbl-group-calendar-bar-flex">
                                    <div class="tbl-group-calendar-bar-row row-1">
                                        <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_posted_task') ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
                <div class="tbl-group-calendar-row tbl-group-calendar-created-row tbl-group-calendar-inactive-row">
                    <div class="tbl-group-calendar-title"><?php echo lang('task_centre_inactive_task') ?></div>
                    <div class="tbl-group-calendar-bar-wrapper posted-tasks-list">
                        <?php if(!empty($posted_inactive)): ?>
                        <?php if(!isset($counter)) $counter = 1 ?>
                        <?php foreach($posted_inactive as $task): ?>
                                <?php echo partial('/workspace/partial/task-centre/posted-task-calendar-view.html.php', compact('task', 'counter')) ?>
                                <?php $counter++ ?>
                            <?php endforeach ?>
                        <?php else: ?>
                            <div class="tbl-group-calendar-bar-row-wrapper empty-task-row">
                                <div class="tbl-group-calendar-bar-flex">
                                    <div class="tbl-group-calendar-bar-row row-1">
                                        <span class="tbl-group-calendar-bar-title"><?php echo lang('task_no_posted_task') ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="colRightCard" class="col-sm-6 col-right-card-flex card-calendar-month">
	        <?php if( $calendar_view === 'month' ): ?>
	        <?php echo partial('/workspace/partial/task-centre/calendar/month-calendar.html.php', compact('posted_active', 'posted_inactive', 'date', 'counter', 'current_month', 'dateObject')) ?>
	        <?php elseif ($calendar_view === 'week') : ?>
	        <?php echo partial('/workspace/partial/task-centre/calendar/week-calendar.html.php', compact('posted_active', 'posted_inactive', 'date', 'counter', 'current_month', 'dateObject')) ?>
	        <?php elseif ($calendar_view === 'day'): ?>
            <?php echo partial('/workspace/partial/task-centre/calendar/day-calendar.html.php', compact('posted_active', 'posted_inactive', 'date', 'counter', 'current_month', 'dateObject')) ?>
	        <?php elseif ($calendar_view === 'hour') : ?>
            <?php echo partial('/workspace/partial/task-centre/calendar/hour-calendar.html.php', compact('posted_active', 'posted_inactive', 'date', 'counter', 'current_month', 'dateObject')) ?>
            <?php endif ?>
        </div>
    </div>
</div>

<?php if( isset($posted_start_day) ): ?>
	<script>
        window.addEventListener('load', function() {
            setTimeout(function(){
                document.querySelector('#tcInProgressPosted .dragscroll.test').scrollTo({
                    top:0, left:<?php echo ($posted_start_day <= 0) ? 0 : $posted_start_day * ($calendar_view === 'hour' ? 70 : 100) ?>, behavior:'smooth'
                });
            }, 500);
        });
	</script>
<?php endif ?>