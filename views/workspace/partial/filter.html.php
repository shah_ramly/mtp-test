<div class="btn-switch-container btn-switch-tc-mode">
	<label class="btn-switch btn-color-mode-switch btn-tc-mode">
		<input type="checkbox" name="tc_mode" id="tc_mode" <?php echo $view !== 'list' ? 'checked="checked"' : '' ?> value="2">
		<label for="tc_mode" data-on="Calendar View" data-off="List View" class="btn-color-mode-switch-inner"></label>
	</label>
</div>