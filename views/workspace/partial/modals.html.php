
<!-- Modal - OD Post Task Steps -->
<div id="post_task_od" class="modal modal-steps fade" aria-labelledby="post_task_od" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target="#modal_confirm_od" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<div class="modal-form-wrapper">
					<form name="form_post_task_od" id="form_post_task_od" method="post" action="<?php echo url_for('/workspace/task/create'); ?>" enctype="multipart/form-data">
                        <?php echo html_form_token_field(); ?>
						<div id="sf1" class="frm modal-step-flex">
							<div class="modal-col modal-col-left">
								<div class="modal-pat-keyart"><img src="<?php echo url_for('/assets/img/Steps-select-category.png') ?>" alt="" /> </div>
							</div>
							<div class="modal-col modal-col-right">
								<fieldset>
									<div class="modal-steps-title"><?php echo lang('post_step_1'); ?></div>
									<div class="form-group">
										<select class="form-control form-control-input category" placeholder="<?php echo lang('post_step_1_category'); ?>" id="od_category" name="od_category" data-type="task" data-target="od_subcategory">
											<option disabled selected><?php echo lang('post_step_1_category'); ?></option>
											<?php foreach($cms->getCategories(0, 'task_category') as $key){ ?>
											<option value="<?php echo $key['id']; ?>"><?php echo $key['title']; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group">
										<select class="form-control form-control-input" placeholder="<?php echo lang('post_step_1_sub_category'); ?>" id="od_subcategory" name="od_subcategory">
											<option disabled selected><?php echo lang('post_step_1_sub_category'); ?></option>
										</select>
									</div>
									<div class="form-group form-others-subcategory">
										<input type="text" class="form-control form-control-input" placeholder="<?php echo lang('post_step_1_sub_category_placeholder'); ?>"  name="od_subcategory_other">
									</div>
									<p class="help-notes"><?php echo lang('post_step_1_note'); ?></p>
								</fieldset>
								<div class="form-group button-container">
									<button type="button" class="btn-icon-full btn-step-next od-open1">
										<span class="btn-label"><?php echo lang('post_next'); ?></span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
									</button>
								</div>
							</div>
						</div>
						<div id="sf2" class="frm modal-step-flex" style="display: none;">
							<div class="modal-col modal-col-left">
								<div class="modal-pat-keyart"><img src="<?php echo url_for('/assets/img/Steps-task-details.png') ?>" alt="" /> </div>
							</div>
							<div class="modal-col modal-col-right">
								<fieldset>
									<div class="modal-steps-title"><?php echo lang('post_step_2'); ?></div>
									<div class="form-group">
										<input type="text" class="form-control form-control-input" placeholder="<?php echo lang('post_step_2_task_title'); ?>" id="od_tasktitle" name="od_tasktitle" maxlength="80">
									</div>
									<div class="form-group">
										<textarea class="form-control form-control-input"
										          placeholder="<?php echo lang('post_step_2_task_description'); ?>"
										          id="od_taskdesc"  data-msg="<?php echo lang('js_field_is_required') ?>"
										          name="od_taskdesc" rows="3" maxlength=""></textarea>
									</div>
									<div class="form-group">
										<div class="dropzone dropzone-previews" id="my-awesome-dropzone" data-text="<?php echo lang('drop_files_here_to_upload') ?>" data-max-file="<?php echo $cms->settings()['task_max_file']; ?>" data-error-max-file="<?php echo sprintf(lang('sys_upload_max_file_error'), $cms->settings()['task_max_file']); ?>"></div>
										<div class="attachments" style="display:none"></div>
									</div>
								</fieldset>
								<div class="form-group button-container">
									<button type="button" class="btn-icon-full btn-step-prev od-back2">
										<span class="btn-label"><?php echo lang('post_back'); ?></span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
									</button>
									<button type="button" class="btn-icon-full btn-step-next od-open2">
										<span class="btn-label"><?php echo lang('post_next'); ?></span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
									</button>
								</div>
							</div>
						</div>
						<div id="sf3" class="frm modal-step-flex" style="display: none;">
							<div class="modal-col modal-col-left">
								<div class="modal-pat-keyart"><img src="<?php echo url_for('/assets/img/Steps-task-location.png') ?>" alt="" /> </div>
								<div class="mapSearchBox"><input type="text" class="form-control form-control-input map-search-input input-sm" placeholder="<?php echo lang('post_step_3_search_address_placeholder'); ?>"></div>
								<div id="mapTaskLoc" style="display:none;"></div>
							</div>
							<div class="modal-col modal-col-right">
								<fieldset>
									<div class="modal-steps-title"><?php echo lang('post_step_3'); ?></div>
									<div class="form-group">
										<label for="creditcard" class="input-lbl">
											<span class="input-label-txt"><?php echo lang('post_step_3_task_location'); ?></span>
											<span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo lang('post_step_3_task_location_placeholder'); ?>">
                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                </svg>
                                            </span>
										</label>
										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input" id="tl_remote" name="od_location" value="remotely">
											<label class="custom-control-label" for="tl_remote"><?php  echo lang('post_step_3_remotely'); ?></label>
										</div>
										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input" id="tl_travel" name="od_location" value="travel">
											<label class="custom-control-label" for="tl_travel"><?php echo lang('post_step_3_requires_travelling'); ?></label>
										</div>
									</div>
									<div class="form-check-selection" id="form_travelrequired" style="display:none;">
										<input id="loc" type="text" name="od_location_address" class="form-control form-control-input" readonly aria-readonly="true" placeholder="<?php echo lang('post_step_3_drag_pin'); ?>" />
										<input id="state_country" type="hidden" name="od_state_country" class="form-control form-control-input" />
									</div>
									<div class="clearfix" style="height: 10px;clear: both;"></div>

								</fieldset>
								<div class="form-group button-container">
									<button type="button" class="btn-icon-full btn-step-prev od-back3">
										<span class="btn-label"><?php echo lang('post_back'); ?></span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
									</button>
									<button type="button" class="btn-icon-full btn-step-next od-open3">
										<span class="btn-label"><?php echo lang('post_next'); ?></span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
									</button>
								</div>
							</div>
						</div>
						<div id="sf4" class="frm modal-step-flex" style="display: none;">
							<div class="modal-col modal-col-left">
								<div class="modal-pat-keyart"><img src="<?php echo url_for('/assets/img/Steps-task-estimates.png') ?>" alt="" /> </div>
							</div>
							<div class="modal-col modal-col-right">
								<fieldset>
									<div class="modal-steps-title"><?php echo lang('post_step_4'); ?></div>
									<div class="form-group">
										<label class="input-lbl">
											<span class="input-label-txt"><?php echo lang('post_step_4_nature'); ?></span>
										</label>
										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input" id="not_bythehour" name="od_type" value="by-hour" maxlength="20">
											<label class="custom-control-label" for="not_bythehour"><?php echo lang('post_step_4_nature_by_hour'); ?></label>
										</div>
										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input" id="not_lumpsum" name="od_type" value="lump-sum"  maxlength="20">
											<label class="custom-control-label" for="not_lumpsum"><?php echo lang('post_step_4_nature_lump_sum'); ?></label>
										</div>
									</div>
									<div class="form-check-selection" id="form_byhour" style="display: none;">
										<div class="form-flex form-estimated-rate-hourly">
											<div class="form-group form-estimated-hours">
											<label class="input-lbl">
												<span class="input-label-txt"><?php echo lang('post_step_4_estimated'); ?></span>
											</label>
											<div class="form-flex">
												<input type="number" name="od_hours[by-hour]" id="bthTotalHours" class="form-control form-control-input" min="0" placeholder="0" onkeyup="totalTaskValue()" onchange="totalTaskValue()" maxlength="20" />
												<span class="postlbl-hours"><?php echo lang('post_step_4_estimated_hours'); ?></span>
											</div>
											</div>
											<div class="form-group form-hourly-rate">
												<label class="input-lbl">
													<span class="input-label-txt"><?php echo lang('post_step_4_hourly_rate'); ?></span>
												</label>
												<div class="form-flex">
													<span class="postlbl-task-value">RM</span>
													<input type="number" name="" id="bthHourlyRate" class="form-control form-control-input" min="0" placeholder="0" onkeyup="totalTaskValue()" onchange="totalTaskValue()" maxlength="20" />
												</div>
											</div>
											<div class="form-group form-value-hours">
												<label class="input-lbl">
													<span class="input-label-txt"><?php echo lang('post_step_4_task_value'); ?></span>
												</label>
												<div class="form-flex">
													<span class="postlbl-task-value">RM</span>
													<input type="number" name="od_budget[by-hour]" class="form-control form-control-input" id="bthTaskValue" min="0" placeholder="0" readonly/>

												</div>
											</div>
										</div>
										<div class="form-flex form-complete-date-hour">
											<div class="form-group form-complete-by-hour-start">
												<label class="input-lbl">
													<span class="input-label-txt"><?php echo lang('post_step_4_start_date'); ?></span>
												</label>
												<div class="form-group">
													<div class="input-group input-group-datetimepicker date" id="form_datetime_hour" data-target-input="nearest">
														<input type="text" name="od_start_by[by-hour]" class="form-control datetimepicker-input"
														       data-text="<?php echo lang('start_date_is_required') ?>"
														       data-target="#form_datetime_hour" placeholder="<?php echo lang('post_step_4_start_date'); ?>" />
														<span class="input-group-addon" data-target="#form_datetime_hour" data-toggle="datetimepicker">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
													</div>
												</div>
											</div>
											<div class="form-group form-complete-by-hour-end">
												<label class="input-lbl">
													<span class="input-label-txt"><?php echo lang('post_step_4_end_date'); ?></span>
												</label>
												<div class="form-group">
													<div class="input-group input-group-datetimepicker date" id="form_datetime_hour_completeby" data-target-input="nearest">
														<input type="text" name="od_complete_by[by-hour]" class="form-control datetimepicker-input"
														       data-text="<?php echo lang('complete_by_date_is_required') ?>"
														       data-target="#form_datetime_hour_completeby" placeholder="<?php echo lang('post_step_4_end_date'); ?>" />
														<span class="input-group-addon" data-target="#form_datetime_hour_completeby" data-toggle="datetimepicker">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group form-mark-urgent hide">
											<div class="form-block-task-filter">
												<div class="custom-control custom-checkbox">
													<input type="hidden" name="od_urgent[by-hour]" value="0" />
													<input type="checkbox" name="od_urgent[by-hour]" value="1" class="custom-control-input" id="urgent_hour">
													<label class="custom-control-label" for="urgent_hour"><?php echo lang('post_step_4_mark_urgent'); ?> <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo lang('post_step_4_mark_urgent_tooltip'); ?> ">
                                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                                </svg>
                                                            </span>
													</label>
												</div>
											</div>
										</div>
										<!--<div class="form-group form-rejection-rate">
											<label class="input-lbl">
                                                    <span class="input-label-txt"><?php /*echo lang('post_step_4_rejection_rate'); */?>
	                                                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="top"
	                                                          title="<?php /*echo lang('post_step_4_rejection_rate_tooltip'); */?>">
                                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                            </svg>
                                                        </span></span>
											</label>
											<div class="form-flex form-rejection-rate-flex">
												<div class="form-flex form-rejection-rate-numerator">
													<select name="rejection_rate[by-hour]" class="form-control form-control-input" id="byhour_rejectionRateNumerator">
														<option value="10" selected>10</option>
														<option value="20">20</option>
														<option value="30">30</option>
														<option value="40">40</option>
														<option value="50">50</option>
														<option value="60">60</option>
														<option value="70">70</option>
														<option value="80">80</option>
														<option value="90">90</option>
													</select>
													<span class="postlbl-percent-value">%</span>
												</div>
												<span class="rejection-divider">/</span>
												<div class="form-flex form-rejection-rate-denominator">
													<select class="form-control form-control-input" id="byhour_rejectionRateDenominator" disabled>
														<option value="10">10</option>
														<option value="20">20</option>
														<option value="30">30</option>
														<option value="40">40</option>
														<option value="50">50</option>
														<option value="60">60</option>
														<option value="70">70</option>
														<option value="80">80</option>
														<option value="90" selected>90</option>
													</select>
													<span class="postlbl-percent-value">%</span>
												</div>
											</div>
										</div>-->
									</div>
									<div class="form-check-selection" id="form_bylumpsum" style="display: none;">
										<div class="form-group form-value-hours">
											<label for="creditcard" class="input-lbl">
												<span class="input-label-txt"><?php echo lang('post_step_4_task_value'); ?></span>
											</label>
											<div class="form-flex">
												<span class="postlbl-task-value">RM</span>
												<input type="number" name="od_budget[lump-sum]" class="form-control form-control-input" min="0" id="" placeholder="0" maxlength="20" />

											</div>
										</div>
										<div class="form-flex form-complete-date-lumpsum">
											<div class="form-group form-complete-by-lumpsum-start">
												<label for="creditcard" class="input-lbl">
													<span class="input-label-txt"><?php echo lang('post_step_4_start_date'); ?></span>
												</label>
												<div class="form-group">
													<div class="input-group input-group-datetimepicker date" id="form_datetime_lumpsum" data-target-input="nearest">
														<input type="text" name="od_start_by[lump-sum]" class="form-control datetimepicker-input"
														       data-text="<?php echo lang('start_date_is_required') ?>"
														       data-target="#form_datetime_lumpsum" placeholder="<?php echo lang('post_step_4_start_date'); ?>" />
														<span class="input-group-addon" data-target="#form_datetime_lumpsum" data-toggle="datetimepicker">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
													</div>
												</div>
											</div>
											<div class="form-group form-complete-by-lumpsum-end">
												<label for="creditcard" class="input-lbl">
													<span class="input-label-txt"><?php echo lang('post_step_4_end_date'); ?></span>
												</label>
												<div class="form-group">
													<div class="input-group input-group-datetimepicker date" id="form_datetime_lumpsum_completeby" data-target-input="nearest">
														<input type="text" name="od_complete_by[lump-sum]" class="form-control datetimepicker-input"
														       data-text="<?php echo lang('complete_by_date_is_required') ?>"
														       data-target="#form_datetime_lumpsum_completeby" placeholder="<?php echo lang('post_step_4_end_date'); ?>" />
														<span class="input-group-addon" data-target="#form_datetime_lumpsum_completeby" data-toggle="datetimepicker">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group form-mark-urgent hide">
											<div class="form-block-task-filter">
												<div class="custom-control custom-checkbox">
													<input type="hidden" name="od_urgent[lump-sum]" value="0" />
													<input type="checkbox" name="od_urgent[lump-sum]" value="1" class="custom-control-input" id="urgent_lumpsum">
													<label class="custom-control-label" for="urgent_lumpsum"><?php echo lang('post_step_4_mark_urgent'); ?> <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo lang('post_step_4_mark_urgent_tooltip'); ?> ">
                                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                                </svg>
                                                            </span>
													</label>
												</div>
											</div>
										</div>
										<!--<div class="form-group form-rejection-rate">
											<label class="input-lbl">
                                                    <span class="input-label-txt"><?php /*echo lang('post_step_4_rejection_rate'); */?>
	                                                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="top"
	                                                          title="<?php /*echo lang('post_step_4_rejection_rate_tooltip'); */?>">
                                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                            </svg>
                                                        </span></span>
											</label>
											<div class="form-flex form-rejection-rate-flex">
												<div class="form-flex form-rejection-rate-numerator">
													<select name="rejection_rate[lump-sum]" class="form-control form-control-input" id="lumpsum_rejectionRateNumerator">
														<option value="10" selected>10</option>
														<option value="20">20</option>
														<option value="30">30</option>
														<option value="40">40</option>
														<option value="50">50</option>
														<option value="60">60</option>
														<option value="70">70</option>
														<option value="80">80</option>
														<option value="90">90</option>
													</select>
													<span class="postlbl-percent-value">%</span>
												</div>
												<span class="rejection-divider">/</span>
												<div class="form-flex form-rejection-rate-denominator">
													<select class="form-control form-control-input" id="lumpsum_rejectionRateDenominator" disabled>
														<option value="10">10</option>
														<option value="20">20</option>
														<option value="30">30</option>
														<option value="40">40</option>
														<option value="50">50</option>
														<option value="60">60</option>
														<option value="70">70</option>
														<option value="80">80</option>
														<option value="90" selected>90</option>
													</select>
													<span class="postlbl-percent-value">%</span>
												</div>
											</div>
										</div>-->
									</div>
									<div class="clearfix" style="height: 10px;clear: both;"></div>
								</fieldset>
								<div class="form-group button-container">
									<button type="button" class="btn-icon-full btn-step-prev od-back4">
										<span class="btn-label"><?php echo lang('post_back'); ?></span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
									</button>
									<button type="button" class="btn-icon-full btn-step-next od-open4">
										<span class="btn-label"><?php echo lang('post_next'); ?></span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
									</button>
								</div>
							</div>
						</div>
						<div id="sf5" class="frm modal-step-flex" style="display: none;">
							<div class="modal-col modal-col-left">
								<div class="modal-pat-keyart"><img src="<?php echo url_for('/assets/img/Steps-seeker-shortlisting.png') ?>" alt="" /> </div>
							</div>
							<div class="modal-col modal-col-right">
								<fieldset>
									<div class="modal-steps-title"><?php echo lang('post_step_5'); ?></div>
									<div class="form-group form-important-skills">
										<label class="input-lbl">
											<span class="input-label-txt"><?php echo lang('post_step_5_important_skills'); ?></span>
										</label>
										<div class="bs-example form-tags-input-suggestion">
											<input type="text" name="skills" id="tag1" class="form-control form-control-input" />
										</div>
									</div>
									<div class="form-group form-qna">
										<label for="creditcard" class="input-lbl">
											<span class="input-label-txt"><?php echo lang('post_step_5_questions'); ?></span>
										</label>
										<div class="od-qna-field-wrapper">
											<div class="qna-wrapper od-qna-wrapper " data-question-text="<?php echo lang('post_question') ?>" data-remove-text="<?php echo lang('question_field_remove') ?>">
												<div class="form-flex">
													<span class="qna-lbl od-qna-lbl">1.</span>
													<input type="text" name="od_qna[]" class="form-control form-control-input" placeholder="<?php echo lang('post_question') ?>" maxlength="100" />
												</div>
											</div>
											<a href="javascript:void(0);" class="od-add-button" title="Add field"><?php echo lang('post_step_5_add_more'); ?></a>
										</div>
									</div>
									<div class="clearfix" style="height: 10px;clear: both;"></div>
								</fieldset>
								<div class="form-group button-container button-container-last">
									<div class="modal-form-actions modal-form-actions-publish">
										<button type="button" class="btn-icon-full btn-step-prev od-back5">
											<span class="btn-label"><?php echo lang('post_back'); ?></span>
											<span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
										</button>
										<button type="button" class="btn-icon-full btn-step-save-draft">
											<span class="btn-label"><?php echo lang('post_step_5_save_draft'); ?></span>
											<span class="btn-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                    <path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path>
                                                    <polyline points="17 21 17 13 7 13 7 21"></polyline>
                                                    <polyline points="7 3 7 8 15 8"></polyline>
                                                </svg>
                                            </span>
										</button>
										<?php if((int)$completion === 100 || (int)$user->info['type'] === 1): ?>
										<button type="submit" class="btn-icon-full btn-step-submit">
											<span class="btn-label"><?php echo lang('post_step_5_publish'); ?></span>
											<span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
										</button>
										<?php endif ?>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>

			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal - FT Post Job Steps -->
<div id="post_job_ft" class="modal modal-steps fade" aria-labelledby="post_job_ft" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target="#modal_confirm_ft" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<div class="modal-form-wrapper">
					<form name="form_post_job_ft" id="form_post_job_ft" method="post" action="<?php echo url_for('/workspace/job/create'); ?>">
                        <?php echo html_form_token_field(); ?>
						<div id="ft_sf1" class="frm-ft modal-step-flex">
							<div class="modal-col modal-col-left">
								<div class="modal-pat-keyart"><img src="<?php echo url_for('/assets/img/mtp-placeholder-2.png') ?>" alt="" /> </div>
							</div>
							<div class="modal-col modal-col-right">
								<fieldset>
									<div class="modal-steps-title"><?php echo lang('post_step_1'); ?></div>
									<div class="form-group">
										<select class="form-control form-control-input category" placeholder="Category" id="ft_category" name="ft_category" data-target="ft_subcategory">
											<option disabled selected><?php echo lang('post_step_1_category'); ?></option>
											<?php foreach($cms->getCategories() as $key){ ?>
											<option value="<?php echo $key['id']; ?>"><?php echo $key['title']; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group">
										<select class="form-control form-control-input" placeholder="Sub Category" id="ft_subcategory" name="ft_subcategory">
											<option disabled selected><?php echo lang('post_step_1_sub_category'); ?></option>
										</select>
									</div>
								</fieldset>
								<div class="form-group button-container">
									<button type="button" class="btn-icon-full btn-step-next ft-open1">
										<span class="btn-label"><?php echo lang('post_next'); ?></span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
									</button>
								</div>
							</div>

						</div>

						<div id="ft_sf2" class="frm-ft modal-step-flex" style="display: none;">
							<div class="modal-col modal-col-left">
								<div class="modal-pat-keyart"><img src="<?php echo url_for('/assets/img/mtp-placeholder-2.png') ?>" alt="" /> </div>
							</div>
							<div class="modal-col modal-col-right">
								<fieldset>
									<div class="modal-steps-title"><?php echo lang('post_job_step_2'); ?></div>
									<div class="form-group">
										<input type="text" class="form-control form-control-input" placeholder="<?php echo lang('post_job_step_2_job_title') ?>" id="ft_jobtitle" name="ft_jobtitle" maxlength="80">
									</div>
									<div class="form-group">
										<textarea class="form-control form-control-input" placeholder="<?php echo lang('post_job_step_2_job_description') ?>" id="ft_jobdesc" name="ft_jobdesc" rows="3" maxlength="80"></textarea>
									</div>
									<div class="form-group">
										<div class="input-group input-group-datetimepicker date" id="form_datetime_closing" data-target-input="nearest">
											<input type="text" class="form-control datetimepicker-input" name="ft_close_date" data-target="#form_datetime_closing" placeholder="<?php echo lang('job_closing_date') ?>" />
											<span class="input-group-addon" data-target="#form_datetime_closing" data-toggle="datetimepicker">
                                                <span class="fa fa-calendar"></span>
                                            </span>
										</div>
									</div>
								</fieldset>
								<div class="form-group button-container">
									<button type="button" class="btn-icon-full btn-step-prev ft-back2">
										<span class="btn-label"><?php echo lang('post_back'); ?></span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
									</button>
									<button type="button" class="btn-icon-full btn-step-next ft-open2">
										<span class="btn-label"><?php echo lang('post_next'); ?></span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
									</button>
								</div>
							</div>
						</div>

						<div id="ft_sf3" class="frm-ft modal-step-flex" style="display: none;">
							<div class="modal-col modal-col-left">
								<div class="modal-pat-keyart"><img src="<?php echo url_for('/assets/img/mtp-placeholder-2.png') ?>" alt="" /> </div>
							</div>
							<div class="modal-col modal-col-right">
								<fieldset>
									<div class="modal-steps-title"><?php echo lang('post_job_step_3') ?></div>

									<div class="form-group">
										<textarea class="form-control form-control-input summernote" placeholder="<?php echo lang('post_job_step_3_job_requirements') ?>" id="ft_jobrequirement" name="ft_jobrequirement" rows="3" maxlength="80"></textarea>
									</div>
									<div class="form-group form-important-skills">
										<label class="input-lbl">
											<span class="input-label-txt"><?php echo lang('post_job_step_3_job_skills') ?></span>
										</label>
										<div class="bs-example form-tags-input-suggestion">
											<input type="text" name="skills" id="tag2" class="form-control form-control-input" />
										</div>
									</div>
									<div class="form-group form-qna">
										<label for="creditcard" class="input-lbl">
											<span class="input-label-txt"><?php echo lang('post_step_5_questions'); ?></span>
										</label>
										<div class="ft-qna-field-wrapper">
											<div class="qna-wrapper ft-qna-wrapper " data-question-text="<?php echo lang('post_question') ?>" data-remove-text="<?php echo lang('question_field_remove') ?>">
												<div class="form-flex">
													<span class="qna-lbl ft-qna-lbl">1.</span>
													<input type="text" name="ft_qna[]" class="form-control form-control-input" placeholder="<?php echo lang('post_question'); ?>" maxlength="100" />
												</div>

											</div>
											<a href="javascript:void(0);" class="ft-add-button" title="<?php echo lang('post_add_field'); ?>"><?php echo lang('post_step_5_add_more'); ?></a>
										</div>
									</div>

									<div class="form-group">
										<select class="form-control form-control-input" placeholder="<?php echo lang('post_job_experience_level'); ?>" id="ft_jobxp" name="ft_jobexperience">
											<option disabled selected><?php echo lang('post_job_experience_level'); ?></option>
											<option value="intern"><?php echo lang('post_job_experience_internship') ?></option>
											<option value="entry"><?php echo lang('post_job_experience_entry_level') ?></option>
											<option value="associate"><?php echo lang('post_job_experience_associate') ?></option>
											<option value="senior"><?php echo lang('post_job_experience_mid_senior_level') ?></option>
											<option value="manager"><?php echo lang('post_job_experience_manager') ?></option>
											<option value="executive"><?php echo lang('post_job_experience_executive') ?></option>
										</select>
									</div>



								</fieldset>
								<div class="form-group button-container">
									<button type="button" class="btn-icon-full btn-step-prev ft-back3">
										<span class="btn-label"><?php echo lang('post_back'); ?></span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
									</button>
									<button type="button" class="btn-icon-full btn-step-next ft-open3">
										<span class="btn-label"><?php echo lang('post_next'); ?></span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
									</button>
								</div>
							</div>
						</div>

						<div id="ft_sf4" class="frm-ft modal-step-flex" style="display: none;">
							<div class="modal-col modal-col-left">
								<div class="modal-pat-keyart"><img src="<?php echo url_for('/assets/img/mtp-placeholder-2.png') ?>" alt="" /> </div>
								<div class="mapSearchBox"><input type="text" class="form-control form-control-input map-search-input input-sm" placeholder="<?php echo lang('post_job_search_location') ?>"></div>
								<div id="mapJobLoc" style="width:100%;height:100%"></div>
							</div>
							<div class="modal-col modal-col-right">
								<fieldset>
									<div class="modal-steps-title"><?php echo lang('post_job_step_4') ?></div>
									<div class="form-group">
										<select class="form-control form-control-input country" placeholder="<?php echo lang('post_job_country') ?>" id="ft_jobcountry" name="ft_jobcountry" data-target="ft_jobstate">
											<option disabled selected><?php echo lang('post_job_country') ?></option>
                                            <?php foreach ($countries as $country): ?>
												<option value="<?php echo $country['id'] ?>" ><?php echo $country['name'] ?></option>
                                            <?php endforeach; ?>
										</select>
									</div>
									<div class="form-group">
										<select class="form-control form-control-input" placeholder="<?php echo lang('post_job_state') ?>" id="ft_jobstate" name="ft_jobstate">
											<option disabled selected><?php echo lang('post_job_state') ?></option>

										</select>
									</div>
									<div class="form-group">
										<input type="text" class="form-control form-control-input" placeholder="Job Location" id="ft_joblocation" name="ft_joblocation" readonly>
									</div>



								</fieldset>
								<div class="form-group button-container">
									<button type="button" class="btn-icon-full btn-step-prev ft-back4">
										<span class="btn-label"><?php echo lang('post_back'); ?></span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
									</button>
									<button type="button" class="btn-icon-full btn-step-next ft-open4">
										<span class="btn-label"><?php echo lang('post_next'); ?></span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
									</button>
								</div>
							</div>
						</div>



						<div id="ft_sf5" class="frm-ft modal-step-flex" style="display: none;">
							<div class="modal-col modal-col-left">
								<div class="modal-pat-keyart"><img src="<?php echo url_for('/assets/img/mtp-placeholder-2.png') ?>" alt="" /> </div>
							</div>
							<div class="modal-col modal-col-right">
								<fieldset>

									<div class="modal-steps-title"><?php echo lang('post_job_step_5') ?></div>
									<div class="form-group form-salary-range">
										<label for="creditcard" class="input-lbl">
											<span class="input-label-txt"><?php echo lang('post_job_salary_range') ?></span>
										</label>
										<div class="form-flex">
											<span class="estimated-lbl estimated-from">RM1200</span>
											<input id="range_budget_salary" name="ft_salaryrange" type="text" />
											<span class="estimated-lbl estimated-to">RM12000</span>
										</div>
									</div>
									<div class="form-group form-benefits">
										<label for="creditcard" class="input-lbl">
											<span class="input-label-txt"><?php echo lang('post_job_benefits') ?></span>
										</label>
										<div class="form-block">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" name="ft_jobbenefits[]" value="Medical" id="benefits_1">
												<label class="custom-control-label" for="benefits_1"><?php echo lang('post_job_benefits_medical') ?></label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" name="ft_jobbenefits[]" value="Parking" id="benefits_2">
												<label class="custom-control-label" for="benefits_2"><?php echo lang('post_job_benefits_parking') ?></label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" name="ft_jobbenefits[]" value="Claimable expenses" id="benefits_3">
												<label class="custom-control-label" for="benefits_3"><?php echo lang('post_job_benefits_claimable_expenses') ?></label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" name="ft_jobbenefits[]" value="Dental" id="benefits_4">
												<label class="custom-control-label" for="benefits_4"><?php echo lang('post_job_benefits_dental') ?></label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" name="ft_jobbenefits[]" value="Meals" id="benefits_5">
												<label class="custom-control-label" for="benefits_5"><?php echo lang('post_job_benefits_meals') ?></label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" name="ft_jobbenefits[]" value="Pantry" id="benefits_6">
												<label class="custom-control-label" for="benefits_6"><?php echo lang('post_job_benefits_pantry') ?></label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" name="ft_jobbenefits[]" value="Commission" id="benefits_7">
												<label class="custom-control-label" for="benefits_7"><?php echo lang('post_job_benefits_commission') ?></label>
											</div>
										</div>
									</div>

								</fieldset>
								<div class="form-group button-container modal-form-actions modal-form-actions-publish">
									<button type="button" class="btn-icon-full btn-step-prev ft-back5">
										<span class="btn-label"><?php echo lang('post_back'); ?></span>
										<span class="btn-icon">
                                            <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"></path></svg>
                                        </span>
									</button>
									<button type="button" class="btn-icon-full btn-step-save-draft">
										<span class="btn-label"><?php echo lang('post_step_5_save_draft') ?></span>
										<span class="btn-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                    <path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path>
                                                    <polyline points="17 21 17 13 7 13 7 21"></polyline>
                                                    <polyline points="7 3 7 8 15 8"></polyline>
                                                </svg>
                                            </span>
									</button>
                                    <?php if($user->info['type'] == 1): ?>
									<button type="submit" class="btn-icon-full btn-step-submit">
										<span class="btn-label"><?php echo lang('post_step_5_publish') ?></span>
										<span class="btn-icon">
                                            <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                            </svg>
                                        </span>
									</button>
									<?php endif ?>
								</div>
								</div>
							</div>
						</div>
					</form>
				</div>

			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
<!-- /.modal -->

<!-- Modal - OD Confirm Discard -->
<div id="modal_confirm_od" class="modal modal-confirm fade" aria-labelledby="modal_confirm_od" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<p class="modal-para"><?php echo lang('post_discard'); ?></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_task_od">
					<span class="btn-label"><?php echo lang('post_discard_cancel'); ?></span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
				</button>
				<button type="button" class="btn-icon-full btn-confirm-task-discard" data-dismiss="modal" data-orientation="next">
					<span class="btn-label"><?php echo lang('post_discard_confirm'); ?></span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
				</button>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->

<!-- Modal - FT Confirm Discard -->
<div id="modal_confirm_ft" class="modal modal-confirm fade" aria-labelledby="modal_confirm_ft" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<p class="modal-para"><?php echo lang('post_discard_confirm_msg'); ?></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_job_ft">
					<span class="btn-label"><?php echo lang('post_discard_cancel'); ?></span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
				</button>
				<button type="button" class="btn-icon-full btn-confirm-job-discard" data-dismiss="modal" data-orientation="next">
					<span class="btn-label"><?php echo lang('post_discard_confirm'); ?></span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
				</button>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->

<!-- Modal - OD Confirm Delete Task -->
<div id="modal_confirm_delete_od" class="modal modal-confirm fade" aria-labelledby="modal_confirm_delete_od" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<p class="modal-para"><?php echo lang('task_delete_are_you_sure'); ?> <span class="text-danger font-weight-bold"><?php echo lang('task_delete_caps'); ?></span> <?php echo lang('task_delete_this_task'); ?></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
					<span class="btn-label"><?php echo lang('task_cancel'); ?></span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
				</button>
				<form method="post" action="<?php echo url_for('/workspace/task/delete') ?>">
                    <?php echo html_form_token_field(); ?>
					<input type="hidden" name="task_id" value="" />
					<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
						<span class="btn-label"><?php echo lang('task_confirm'); ?></span>
						<span class="btn-icon">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                                <polyline points="20 6 9 17 4 12"></polyline>
	                            </svg>
	                        </span>
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->

<!-- Modal - OD Confirm Cancel Task -->
<div id="modal_confirm_cancel_od" class="modal modal-confirm fade" aria-labelledby="modal_confirm_cancel_od" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<p class="modal-para"><?php echo lang('task_cancel_are_you_sure'); ?> <span class="text-warning font-weight-bold"><?php echo lang('task_cancel_caps'); ?></span> <?php echo lang('task_cancel_this_task'); ?></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
					<span class="btn-label"><?php echo lang('task_cancel'); ?></span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
				</button>
				<form method="post" action="<?php echo url_for('/workspace/task/cancel') ?>">
                    <?php echo html_form_token_field(); ?>
					<input type="hidden" name="task_id" value="" />
					<button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
						<span class="btn-label"><?php echo lang('task_confirm'); ?></span>
						<span class="btn-icon">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
	                                <polyline points="20 6 9 17 4 12"></polyline>
	                            </svg>
	                        </span>
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->

<!-- Modal - Complete Your Profile -->
<div id="modal_complete_profile" class="modal modal-complete-profile fade" aria-labelledby="modal_complete_profile" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php echo lang('header_complete_your_profile'); ?></div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<div class="complete-profile-progress-header">
					<div class="complete-profile-progress-header-flex">
						<span class="complete-profile-lbl"><?php echo lang('header_profile_completeness_title'); ?></span>
						<span class="complete-profile-val"><?php echo $completion ?>%</span>
					</div>
					<div id="progress_complete-profile" class="progressbar-chart">
						<svg viewBox="0 0 100 1" preserveAspectRatio="none" style="width: 100%; height: 100%;">
							<path d="M 0,0.5 L 100,0.5" stroke="#eee" stroke-width="1" fill-opacity="0"></path>
							<path d="M 0,0.5 L 100,0.5" stroke="#3644ad" stroke-width="1" fill-opacity="0" style="stroke-dasharray: 100, 100; stroke-dashoffset: <?php echo 100 - (int)$completion ?>;"></path></svg>
					</div>
				</div>
				<p class="modal-para"><?php echo lang('header_profile_completeness_decription'); ?></p>
				<div class="complete-profile-progress-container">
                                        <?php if(!$user->info['country'] || !$user->info['state'] || !$user->info['city'] || !$user->info['nric'] || !$user->info['dob'] || !$user->info['skills'] || !$user->info['language']){ ?>
					                    <div class="complete-profile-group complete-profile-myprofile-group">
						                    <button class="btn-link btn-complete-profile btn-flex is-clicked" type="button" aria-expanded="false">
							                    <label class="task-filter-lbl"><?php echo lang('header_profile_completeness_my_profile'); ?></label>
							                    <span class="chevron-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                        <path d="M6 9l6 6 6-6"></path>
                                                    </svg>
                                                </span>
						                    </button>
						                    <div class="form-filter-collapse collapse show">
											<?php if(!$user->info['country']){ ?>
								                    <div class="row-complete-profile row-cp-country">
									                    <a href="<?php echo url_for('/my_profile'); ?>" class="complete-profile-link">
										                    <span class="complete-profile-icon"></span>
										                    <span class="complete-profile-label"><?php echo lang('header_profile_completeness_my_profile_add_your_country'); ?></span>
									                    </a>
									                    <span class="complete-profile-status">+5%</span>
								                    </div>
                                                <?php } ?>
												<?php if(!$user->info['state'] || $user->info['state'] == 0){ ?>
								                    <div class="row-complete-profile row-cp-country">
									                    <a href="<?php echo url_for('/my_profile'); ?>" class="complete-profile-link">
										                    <span class="complete-profile-icon"></span>
										                    <span class="complete-profile-label"><?php echo lang('header_profile_completeness_my_profile_add_your_state'); ?></span>
									                    </a>
									                    <span class="complete-profile-status">+5%</span>
								                    </div>
                                                <?php } ?>
												<?php if(!$user->info['city']){ ?>
								                    <div class="row-complete-profile row-cp-country">
									                    <a href="<?php echo url_for('/my_profile'); ?>" class="complete-profile-link">
										                    <span class="complete-profile-icon"></span>
										                    <span class="complete-profile-label"><?php echo lang('header_profile_completeness_my_profile_add_your_city'); ?></span>
									                    </a>
									                    <span class="complete-profile-status">+5%</span>
								                    </div>
                                                <?php } ?>
                                                <?php if(!$user->info['nric']){ ?>
								                    <div class="row-complete-profile row-cp-mykad">
									                    <a href="<?php echo url_for('/my_profile'); ?>" class="complete-profile-link">
										                    <span class="complete-profile-icon"></span>
										                    <span class="complete-profile-label"><?php echo lang('header_profile_completeness_my_profile_add_your_mykad'); ?></span>
									                    </a>
									                    <span class="complete-profile-status">+5%</span>
								                    </div>
                                                <?php } ?>
                                                <?php if(!$user->info['dob']){ ?>
								                    <div class="row-complete-profile row-cp-dob">
									                    <a href="<?php echo url_for('/my_profile'); ?>" class="complete-profile-link">
										                    <span class="complete-profile-icon"></span>
										                    <span class="complete-profile-label"><?php echo lang('header_profile_completeness_my_profile_add_your_dob'); ?></span>
									                    </a>
									                    <span class="complete-profile-status">+5%</span>
								                    </div>
                                                <?php } ?>
						                    </div>
					                    </div>
                                        <?php } ?>
										
										<?php if(!$user->info['skills'] || !$user->info['language']){ ?>
					                    <div class="complete-profile-group complete-profile-myprofile-group">
						                    <button class="btn-link btn-complete-profile btn-flex is-clicked" type="button" aria-expanded="false">
							                    <label class="task-filter-lbl"><?php echo lang('header_profile_completeness_my_cv_title'); ?></label>
							                    <span class="chevron-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                        <path d="M6 9l6 6 6-6"></path>
                                                    </svg>
                                                </span>
						                    </button>
						                    <div class="form-filter-collapse collapse show">
                                                <?php if(!$user->info['skills']){ ?>
								                    <div class="row-complete-profile row-cp-skills">
									                    <a href="<?php echo url_for('/my_cv#skill'); ?>" class="complete-profile-link">
										                    <span class="complete-profile-icon"></span>
										                    <span class="complete-profile-label"><?php echo lang('header_profile_completeness_my_cv_add_your_skills'); ?></span>
									                    </a>
									                    <span class="complete-profile-status">+10%</span>
								                    </div>
                                                <?php } ?>
                                                <?php if(!$user->info['language']){ ?>
								                    <div class="row-complete-profile row-cp-lang">
									                    <a href="<?php echo url_for('/my_cv#language'); ?>" class="complete-profile-link">
										                    <span class="complete-profile-icon"></span>
										                    <span class="complete-profile-label"><?php echo lang('header_profile_completeness_my_profile_add_your_language'); ?></span>
									                    </a>
									                    <span class="complete-profile-status">+10%</span>
								                    </div>
                                                <?php } ?>
						                    </div>
					                    </div>
                                        <?php } ?>
										
										<?php if(!$user->info['preference']['bank'] || !$user->info['preference']['acc_name'] || !$user->info['preference']['acc_num'] ||
										!$user->info['preference']['work_day'] || !$user->info['preference']['working_hours'] || !$user->info['preference']['work_load_hours'] || !$user->info['preference']['expected_earning']){ ?>
					                    <div class="complete-profile-group complete-profile-settings-group">
						                    <button class="btn-link btn-complete-profile btn-flex is-clicked" type="button" aria-expanded="false">
							                    <label class="task-filter-lbl"><?php echo lang('header_profile_completeness_settings_title'); ?></label>
							                    <span class="chevron-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                        <path d="M6 9l6 6 6-6"></path>
                                                    </svg>
                                                </span>
						                    </button>
						                    <div class="form-filter-collapse collapse show">
                                                <?php if(!$user->info['preference']['bank'] || !$user->info['preference']['acc_name'] || !$user->info['preference']['acc_num']){ ?>
								                    <div class="row-complete-profile row-cp-bankinfo">
									                    <a href="<?php echo url_for('/settings'); ?>" class="complete-profile-link">
										                    <span class="complete-profile-icon"></span>
										                    <span class="complete-profile-label"><?php echo lang('header_profile_completeness_settings_bank_info'); ?></span>
									                    </a>
									                    <span class="complete-profile-status">+10%</span>
								                    </div>
                                                <?php } ?>
												
                                                <?php //if(!$user->info['preference']['work_day'] || !$user->info['preference']['working_hours']){ ?>
                                                <?php if(!$user->info['preference']['work_day']){ ?>
								                    <div class="row-complete-profile row-cp-workdays">
									                    <a href="<?php echo url_for('/settings#workdays'); ?>" class="complete-profile-link">
										                    <span class="complete-profile-icon"></span>
										                    <span class="complete-profile-label"><?php echo lang('header_profile_completeness_settings_work_days'); ?></span>
									                    </a>
									                    <span class="complete-profile-status">+10%</span>
								                    </div>
                                                <?php } ?>
                                                <?php //if(!$user->info['preference']['work_load_hours'] || !$user->info['preference']['expected_earning']){ ?>
                                                <?php if(!$user->info['preference']['work_load_hours']){ ?>
								                    <div class="row-complete-profile row-cp-workload">
									                    <a href="<?php echo url_for('/settings#workload'); ?>" class="complete-profile-link">
										                    <span class="complete-profile-icon"></span>
										                    <span class="complete-profile-label"><?php echo lang('header_profile_completeness_settings_work_load'); ?></span>
									                    </a>
									                    <span class="complete-profile-status">+10%</span>
								                    </div>
                                                <?php } ?>
						                    </div>
					                    </div>
                                        <?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->

<?php if( isset($modals) ) echo $modals; ?>
<?php if( isset($extra_modals) ) echo $extra_modals; ?>

<!-- Modal - Talent Details -->
<div id="modal_talent_details" class="modal modal-talent-details fade" aria-labelledby="modal_talent_details" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<div class="col-talent-modal-button-wrapper">
					<?php if(!stripos(request_uri(), 'job-centre')): ?>
					<form name="assign-task" id="assign-task" method="post" action="<?php echo option('site_uri') . url_for('/workspace/task/hire') ?>">
					<?php else: ?>
					<form name="shortlist-user" id="shortlist-user" method="post" action="<?php echo option('site_uri') . url_for('/workspace/job/shortlist') ?>">
					<?php endif ?>
                        <?php echo html_form_token_field(); ?>
						<?php if(!stripos(request_uri(), 'job-centre')): ?>
						<input type="hidden" name="task_id" id="task_id" value="" />
						<?php else: ?>
						<input type="hidden" name="job_id" id="job_id" value="" />
						<?php endif ?>
						<input type="hidden" name="user_id" id="candidate_id" value="" />
						<?php if(!stripos(request_uri(), 'job-centre')): ?>
						<button class="btn btn-ico btn-primary btn-send-msg" data-dismiss="modal" data-toggle="modal" data-target="#modal_confirm_talent"><?php echo lang('select_this_candidates') ?></button>
						<?php else: ?>
						<button class="btn btn-ico btn-primary btn-send-msg" data-dismiss="modal" data-toggle="modal" data-target="#modal_confirm_talent"><?php echo lang('shortlist_this_candidates') ?></button>
						<?php endif ?>
					</form>
				</div>
				<div class="col-talent-details-wrapper">
					<div class="row">
						<div class="col-talent-details-top-wrapper">
							<div class="col-left-card">
								<div class="col-personal-info-card-container">
									<div class="talent-details-personal-info-flex">
										<div class="talent-details-avatar-container">
											<div class="profile-avatar">
												<img class="avatar-img" src="<?php echo imgCrop('assets/img/default-avatar.png', 150, 150) ?>" alt="">
											</div>
										</div>
										<div class="talent-details-personal-info">
											<h2 class="talent-details-name"></h2>
											<div class="talent-details-profile-location">
                                                    <span class="talent-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
                                                        </svg></span>
												<span class="talent-details-profile-location-val"></span>
											</div>
											<div class="task-row-rating-flex">
												<div class="task-row-rating-star talent-rating-stars">
													<span class="rating-star-icon active"><i class="fa fa-star"></i></span>
													<span class="rating-star-icon"><i class="fa fa-star"></i></span>
												</div>
												<div class="task-row-rating-val">
													<span class="task-row-average-rating-val talent-rating-overall"></span>
													<span class="task-row-divider">/</span>
													<span class="task-row-total-rating-val talent-rating-reviews-count"></span>
												</div>
											</div>
											<div class="talent-row-skills">
												<label class="talent-details-lbl"><?php echo lang('talent_professional_skills') ?></label>
												<div class="task-details-deadline-block talent-skills">
													<span class="skills-label-link"><span class="tag label label-info bubble-lbl"></span></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-right-card">
								<div class="col-activity-info-card-container">
									<div class="talent-total-completed-task-hour">
										<label class="talent-details-lbl"><?php echo lang('talent_activity'); ?></label>
										<div class="talent-extra-info talent-total-completed-task">
											<span class="talent-profile-label"><?php echo lang('talent_completed_task'); ?>:</span>
											<span class="talent-profile-val tasks-completed">0</span>
										</div>
										<div class="talent-extra-info  talent-total-ongoing-task">
											<span class="talent-profile-label"><?php echo lang('talent_ongoing_task'); ?>:</span>
											<span class="talent-profile-val tasks-on-going">0</span>
										</div>
										<div class="talent-extra-info  talent-total-completed-hour">
											<span class="talent-profile-label"><?php echo lang('talent_total_hours_completed'); ?>:</span>
											<span class="talent-profile-val total-hours">0</span>
										</div>
									</div>
									<div class="talent-history">
										<label class="talent-details-lbl"><?php echo lang('talent_information'); ?></label>
										<div class="talent-extra-info talent-last-login">
											<span class="talent-profile-label"><?php echo lang('talent_last_login'); ?>:</span>
											<span class="talent-profile-val last-login"></span>
										</div>
										<div class="talent-extra-info talent-extra-info talent-member-since">
											<span class="talent-profile-label"><?php echo lang('talent_member_since'); ?>:</span>
											<span class="talent-profile-val member-since"></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-talent-details-btm-wrapper" style="width:100%">
							<div class="col-left-card">
								<div class="col-talent-details-aboutme-container">
									<label class="talent-details-lbl"><?php echo lang('talent_personal_statement'); ?></label>
									<div class="talent-details-about"></div>
								</div>
								<div class="col-talent-details-docs-container">
									<div class="talent-details-documents"></div>
								</div>
								<div class="col-talent-details-workxp-container">
									<label class="talent-details-lbl"><?php echo lang('talent_work_experience'); ?></label>
									<div class="talent-details-workxp-rows">
										<div class="talent-details-workxp-row">
											<div class="talent-details-workxp-compname"></div>
											<div class="talent-details-workxp-pos-duration">
												<div class="talent-details-workxp-pos"></div>
											</div>
											<div class="talent-details-workxp-pos-duration">
												<div class="talent-details-workxp-location"></div>
												<div class="talent-details-workxp-duration"></div>
											</div>
											<div class="talent-details-workxp-resp">
												<span class="talent-details-workxp-list-lbl"><?php echo lang('talent_job_descriptions'); ?></span>
												<div class="job-desc"></div>
											</div>
											<div class="talent-details-workxp-resp">
												<span class="talent-details-workxp-list-lbl"><?php echo lang('talent_responsibilities'); ?></span>
												<ul class="talent-details-workxp-list">

												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="col-talent-details-edu-container">
									<label class="talent-details-lbl"><?php echo lang('talent_education'); ?></label>
									<div class="talent-details-edu-rows">
										<div class="talent-details-edu-row">
											<div class="talent-details-edu-insname"></div>
											<div class="talent-details-edu-level-fieldstudy-grade">
												<div class="talent-details-edu-level"></div>
												<div class="talent-details-edu-fieldstudy"></div>
												<div class="talent-details-edu-grade"></div>
											</div>
											<div class="talent-details-edu-location-year">
												<div class="talent-details-edu-location"></div>
												<div class="talent-details-edu-year"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-talent-details-language-container">
									<label class="talent-details-lbl"><?php echo lang('talent_languages'); ?></label>
									<div class="task-details-deadline-block talent-languages">
										<span class="skills-label-link"><span class="tag label label-info bubble-lbl"></span></span>
									</div>

								</div>
								<div class="col-rating-list-container">
									<label class="talent-details-lbl"><?php echo lang('talent_individual_ratings_and_feedback'); ?></label>
									<ul class="col-rating-list">
										<li>
											<div class="col-xl-12 col-ratings-bottom">
												<div class="col-rating-first-bottom-group">
													<div class="col-rating-user-info">
														<div class="rating-avatar">
															<a href="#" class="rating-photo-link">
																<div class="col-rating-photo">
																	<img src="<?php echo url_for('/assets/img/james.jpg') ?>" alt="Profile Photo">
																</div>
															</a>
														</div>
														<a href="#" class="rating-name-link"></a>
														<span class="task-details-comment-date"></span>
													</div>
													<div class="bottom-rating-description"></div>
												</div>
												<div class="col-bottom-rating-stars-task">
													<div class="rating-star-average-group">
														<div class="task-row-rating-star">
															<span class="rating-star-icon active"><i class="fa fa-star"></i></span>
															<span class="rating-star-icon"><i class="fa fa-star"></i></span>
														</div>
														<div class="task-row-rating-val">
															<span class="task-row-average-rating-val">0.0</span>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-right-card">
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<!-- /.modal -->

<!-- Modal - Confirm Talent -->
<div id="modal_confirm_talent" class="modal modal-confirm-talent fade" aria-labelledby="modal_confirm_talent" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php echo lang('task_select_candidate_confirmation_title') ?></div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<?php echo lang('task_select_candidate_confirmation_body') ?>
			</div>
			<div class="modal-footer">
				<div class="form-group button-container button-container-last">
					<div class="modal-form-actions">
						<!--<button type="button" class="btn-icon-full btn-step-prev" data-dismiss="modal" data-toggle="modal" data-target="#modal_talent_details" >
							<span class="btn-label"><?php /*echo lang('task_details_back') */?></span>
							<span class="btn-icon">
                                    <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"></path>
                                    </svg>
                                </span>
						</button>-->
						<button type="submit" id="assign-the-task" class="btn-icon-full btn-step-submit">
							<span class="btn-label"><?php echo lang('proceed') ?></span>
							<span class="btn-icon">
                                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                    </svg>
                                </span>
						</button>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<!-- /.modal -->

<!-- Modal - View Answer -->
<div id="modal_view_answer" class="modal modal-view-answer fade" aria-labelledby="modal_view_answer" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php echo lang('task_view_answer') ?></div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<div class="modal-body-container">
					<div class="qna-rows-container">
						<div class="spinner-border" style="width:4.5rem;height:4.5rem;display:block;margin:0 auto;" role="status">
							<span class="sr-only">Loading...</span>
						</div>
						<ol class="qna-list hide">
							<li class="hide">
								<div class="qna-question"></div>
								<div class="qna-answer"></div>
							</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
					<span class="btn-label"><?php echo lang('sign_up_terms_of_use_close') ?></span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
				</button>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->

<!-- Modal - Chat -->
<div id="modal_chat" class="modal modal-chat fade" aria-labelledby="modal_chat" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<div class="chat-main chat-main-visible">
					<div id="chat-1" class="chat dropzone-form-js">
						<!--- Chat: body --->
						<div class="chat-body">
							<!-- Chat: Header -->
							<div class="chat-header border-bottom">
								<div class="container-xxl">
									<div class="row align-items-center">
										<!-- Close chat(mobile) -->
										<div class="close-chat-chevron-container col-3 d-xl-none">
											<ul class="list-inline mb-0">
												<li class="list-inline-item">
													<a class="text-muted px-0" href="#" data-chat="open">
	                                                    <span class="chevron-icon">
	                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
	                                                            <path d="M15 18l-6-6 6-6" /></svg>
	                                                    </span>
													</a>
												</li>
											</ul>
										</div>

										<!-- Chat photo -->
										<div class="col-header-photo col-6 col-xl-8">
											<div class="media text-center text-xl-left">
												<div class="avatar avatar-online avatar-sm receiver-avatar">
													<img src="<?php echo url_for('/assets/img/default-avatar.png') ?>" class="avatar-img" alt="">
												</div>

												<div class="media-body align-self-center text-truncate">
													<h6 class="chat-body-name text-truncate mb-n1">...</h6>
													<small class="text-muted"></small>
												</div>
											</div>
										</div>

										<!-- Chat toolbar -->
										<div class="col-header-settings col-3 col-xl-4">
											<ul class="nav justify-content-end">
												<!--<li class="nav-item nav-item-options list-inline-item d-xl-block dropdown dropdown-chat-header">
													<a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="three-dots-icon">
                                                            <svg class="bi bi-three-dots" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                            </svg>
                                                        </span>
													</a>
													<div class="dropdown-menu">
														<a class="dropdown-item view-profile" href="#"><?php /*echo lang('task_view_profile'); */?></a>
														<a class="dropdown-item view-task-details" href="#"><?php /*echo lang('task_view_task_details'); */?></a>
													</div>
												</li>-->
												<li class="nav-item nav-items-chevron list-inline-item d-xl-block">
													<a class="nav-link" href="#" data-chat-sidebar-toggle="#chat-1-info">
                                                        <span class="chevron-chat-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                <path d="M15 18l-6-6 6-6" /></svg>
                                                        </span>
													</a>
												</li>
											</ul>
										</div>
									</div><!-- .row -->
								</div>
							</div>
							<!-- Chat: Header -->

							<!-- Chat: Content -->
							<div class="chat-content">
								<div class="container-xxl">

								</div>
								<div class="end-of-chat"></div>
							</div>
							<!-- Chat: Content -->

							<!-- Chat: Footer -->
							<div class="chat-footer">
								<form id="chat-id-1-form"  data-emoji-form="">
									<div class="form-row form-footer-reply d-none">
										<!-- Sample Reply - Text -->
										<div class="message-reply-content d-none" data-type="text">
											<div class="message-reply-content-wrapper">
												<div class="message-content-attachment"></div>
											</div>
										</div>
										<!-- Sample Reply - Text -->
										
										<!-- Sample Reply - Attachment -->
										<div class="message-reply-content d-none" data-type="attachment">
											<div class="message-reply-content-wrapper">
												<div class="message-content-attachment">
												<?php /*<h6 class="mb-2">Maria Valbuena shared 3 photos:</h6>*/ ?>
													<div class="form-row">
													<?php /*
														<div class="col">
															<img class="img-fluid rounded" src="https://themes.2the.me/Boomerang/1.2/assets/images/team/1.jpg" alt="">
														</div>
														<div class="col">
															<img class="img-fluid rounded" src="https://themes.2the.me/Boomerang/1.2/assets/images/team/2.jpg" alt="">
														</div>
														<div class="col">
															<img class="img-fluid rounded" src="https://themes.2the.me/Boomerang/1.2/assets/images/team/3.jpg" alt="">
														</div>
														*/ ?>
													</div>
												</div>
											</div>
										</div>
										<!-- Sample Reply - Attachment -->

										<?php /*
										<!-- Sample Reply - File -->
										<!--<div class="message-reply-content" data-type="file">
											<div class="message-reply-content-wrapper">
												<div class="media">
													<span class="icon-shape">
														<span class="non-icon-download">PDF</span>
													</span>
													<div class="media-body overflow-hidden flex-fill">
														<span class="d-block text-truncate font-medium text-reset">bootstrap.pdf</span>
														<ul class="list-inline small mb-0">
															<li class="list-inline-item">
																<span class="t">79.2 KB</span>
															</li>
															<li class="list-inline-item">
																<span class="text-uppercase">pdf</span>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>-->
										<!-- Sample Reply - File -->
										*/ ?>										

										<div id="closeReply" class="close-reply">
											<span class="close-icon">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
													<line x1="18" y1="6" x2="6" y2="18"></line>
													<line x1="6" y1="6" x2="18" y2="18"></line>
												</svg>
											</span>
										</div>
									</div>
									<!-- /chat-reply -->

									<div class="form-row align-items-center disableProp">
										<div class="col">
											<div class="input-group">

												<!-- Textarea -->
												<textarea id="chat-id-input" class="form-control bg-transparent border-0" placeholder="<?php echo lang('chat_type_your_message') ?>" rows="1" data-emoji-input="" data-autosize="true"></textarea>
												<div class="input-group-append">
													<input type='text' name="replyID" id="replyID" hidden />	
													<input type='file' name="files" id="hiddenUploadButton" hidden multiple />
													<div id="fileList"></div>
													<button id="chat-upload-btn" class="btn btn-chat-upload dropzone-button-js dz-clickable" type="button">
														<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-paperclip injected-svg">
															<path d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path>
														</svg>
													</button>
												</div>

											</div>

										</div>

										<!-- Submit button -->
										<div class="col-auto">
											<button class="btn btn-ico btn-primary btn-send-msg" type="button">
												<?php echo lang('chat_send') ?>
											</button>
										</div>

									</div>

								</form>
							</div>
							<!-- Chat: Footer -->

						</div>
						<!--- Chat: body --->

						<!--- Chat Details --->
						<div id="chat-1-info" class="chat-sidebar-details">
							<div class="d-flex h-100 flex-column">
								
							</div>
						</div>
						<!--- Chat Details --->
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="closing-mark" class="message-divider my-9 mx-lg-5" style="display:none">
		<div class="row align-items-center">
			<div class="col"><hr></div>
			<div class="col-auto">
				<small class="text-muted"><?php echo lang('task_chat_conversation_has_been_closed') ?>." %ClosedDate%"</small>
			</div>
			<div class="col"><hr></div>
		</div>
	</div>
</div>

 <!-- Modal - Chat Image-->
<div id="modal_chat_img" class="modal modal-chat-img fade" aria-labelledby="modal_chat_img" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="download-btn" aria-label="Download" title="Download">
					<a href="#" download>
                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                        <path d="M3 15v4c0 1.1.9 2 2 2h14a2 2 0 0 0 2-2v-4M17 9l-5 5-5-5M12 12.8V2.5" /></svg></a>
                </button>
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="img-zoom">
                        <img src="https://themes.2the.me/Boomerang/1.2/assets/images/team/1.jpg" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- /.modal -->

<!-- Modal - Language BM -->
<div id="modal_language_bm" class="modal modal-language-bm fade" aria-labelledby="modal_language_bm" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Coming Soon</div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<p class="modal-para">Malay language will be coming very soon.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
					<span class="btn-label">Got It</span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
				</button>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->

<!-- Modal - Feedback -->
<div id="modal_feedback" class="modal modal-feedback fade" aria-labelledby="modal_feedback" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php echo lang('feedback_title'); ?></div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
				     stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body-tab">
				<ul class="nav nav-pills nav-pills-feedback" id="pills-tabFeedback" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="pills-submitfeedback-tab" data-toggle="pill"
						   href="#pills-submitfeedback" role="tab" aria-controls="pills-submitfeedback"
						   aria-selected="true"><?php echo lang('feedback_submit'); ?></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="pills-previousfeedback-tab" data-toggle="pill"
						   href="#pills-previousfeedback" role="tab" aria-controls="pills-previousfeedback"
						   aria-selected="false"><?php echo lang('feedback_previous_feedback'); ?></a>
					</li>
				</ul>
				<div class="tab-content" id="pills-tabFinancialContent">
					<div class="tab-pane fade show active" id="pills-submitfeedback" role="tabpanel">
						<div class="modal-body">
							<div class="feedback-title-desc-container">
								<label class="input-lbl input-lbl-block">
									<span class="input-label-txt"><?php echo lang('feedback_help_improve'); ?>:</span>
								</label>
								<div class="para-note"><?php echo lang('feedback_help_improve_content'); ?></div>
							</div>
							<form id="form-feedback" class="form-feedback" method="post" action="<?php echo url_for('/feedback') ?>">
								<?php echo html_form_token_field() ?>
								<input type="hidden" name="report" id="to-report" value="" />
								<div class="input-group-flex input-group-row row-feebackarea">
									<label class="input-lbl input-lbl-block">
										<span class="input-label-txt"><?php echo lang('feedback_area'); ?>:</span>
									</label>
									<div class="input-select-group">
										<select class="form-control form-control-input" name="feedback_area" id="select_feedback_area">
											<option disabled selected>- <?php echo lang('feedback_please_select'); ?> -</option>
											<option value="Overall Experience"><?php echo lang('feedback_overall_experience'); ?></option>
											<option value="User Interface"><?php echo lang('feedback_user_interface'); ?></option>
											<option value="Functionality"><?php echo lang('feedback_functionality'); ?></option>
											<option value="Bugs & Errors"><?php echo lang('feedback_bugs_and_errors'); ?></option>
											<option value="Report Inappropriate Content"><?php echo lang('report_inappropriate_content')?></option>
											<option value="Report User"><?php echo lang('report_user')?></option>
											<option value="feedback_others"><?php echo lang('feedback_others'); ?></option>
										</select>
										<input type="text" id="feedbackOthersInput" name="feedback_area" disabled
										       class="form-control form-control-input" placeholder="<?php echo lang('feedback_let_us_know_more_placeholder'); ?>"
										       style="display:none;">
									</div>
								</div>
								<div class="input-group-flex input-group-row row-feebackbody">
									<label class="input-lbl input-lbl-block">
										<span class="input-label-txt"><?php echo lang('feedback_title'); ?>:</span>
									</label>
									<textarea class="form-control form-control-input" name="feedback"
									          placeholder="<?php echo lang('feedback_describe_you_experience_placeholder'); ?>" rows="5"></textarea>
								</div>
								<div class="input-group-flex input-group-row row-feebackattachment">
									<label class="input-lbl input-lbl-block">
										<span class="input-label-txt"><?php echo lang('feedback_attachment'); ?>:</span>
									</label>
									<div class="input-select-group">
										<div class="feedback-attachments"></div>
										<div class="dropzone dropzone-previews" id="feedback-dropzone" data-text="<?php echo lang('drop_files_here_to_upload') ?>"></div>
										<p class="help-notes"><?php echo lang('feedback_attachment_maximum'); ?>: 3</p>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal"
							        data-toggle="modal">
								<span class="btn-label"><?php echo lang('cancel'); ?></span>
								<span class="btn-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round"
                                             stroke-linejoin="arcs">
                                            <line x1="18" y1="6" x2="6" y2="18"></line>
                                            <line x1="6" y1="6" x2="18" y2="18"></line>
                                        </svg>
                                    </span>
							</button>
							<button type="submit" id="feedback-submit" class="btn-icon-full btn-confirm" data-dismiss="modal"
							        data-orientation="next">
								<span class="btn-label"><?php echo lang('submit_and_close'); ?></span>
								<span class="btn-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round"
                                             stroke-linejoin="arcs">
                                            <polyline points="20 6 9 17 4 12"></polyline>
                                        </svg>
                                    </span>
							</button>
						</div>
					</div>
					<div class="tab-pane fade show" id="pills-previousfeedback" role="tabpanel">
						<div class="modal-body">
							<div id="table-listing-container-feedback" class="table-listing-container">
								<?php echo partial('partial/feedback-table.html.php', ['feedbacks' => $feedbacks ?? null]) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->

<!-- Modal - Help -->
<div id="modal_help" class="modal modal-help fade" aria-labelledby="modal_help" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php echo lang('help'); ?></div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<div class="help-flex">
					<div class="help-col help-col-left">
						<div class="help-title"><?php echo lang('help_couldnt_find'); ?></div>
						<p class="body-para help-body"><?php echo lang('help_email_us'); ?> <a href="mailto:support@maketimepay.com">support@maketimepay.com</a> <?php echo lang('help_email_us2'); ?></p>
					</div>
					<div class="help-col help-col-right">
						<ul class="nav nav-pills nav-pills-help" id="pills-tabHelp" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="helpGeneralTab-tab" data-toggle="pill" href="#helpGeneralTab" role="tab" aria-controls="helpGeneralTab" aria-selected="true"><?php echo lang('help_general'); ?></a>
							</li>
							<!-- <li class="nav-item">
								<a class="nav-link" id="helpFunctionalityTab-tab" data-toggle="pill" href="#helpFunctionalityTab" role="tab" aria-controls="helpFunctionalityTab" aria-selected="false"><?php echo lang('help_functionality'); ?></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="helpPaymentTab-tab" data-toggle="pill" href="#helpPaymentTab" role="tab" aria-controls="helpPaymentTab" aria-selected="false"><?php echo lang('help_payment'); ?></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="helpRegistrationTab-tab" data-toggle="pill" href="#helpRegistrationTab" role="tab" aria-controls="helpRegistrationTab" aria-selected="false"><?php echo lang('help_registration'); ?></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="helpSigninTab-tab" data-toggle="pill" href="#helpSigninTab" role="tab" aria-controls="helpSigninTab" aria-selected="false"><?php echo lang('help_sign_in'); ?></a>
							</li>-->
						</ul>
						<div class="tab-content" id="pills-tabContentHelp">
							<div class="tab-pane fade show active" id="helpGeneralTab" role="tabpanel">
								<div class="help-rows-container">
									<?php for($i = 1; $i <= 20; $i++){ ?>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc"><?php echo lang('faq_general_title_' . $i); ?></div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para"><?php echo lang('faq_general_desc_' . $i); ?></p>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
								<div class="help-actions">
									<a href="#"><button type="button" class="btn-main btn-help btn-see-more"><?php echo lang('help_see_more'); ?></button></a>
								</div>
							</div>
							<div class="tab-pane fade show" id="helpFunctionalityTab" role="tabpanel">
								<div class="help-rows-container">
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">How does the subscription plan work?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">But I must explain to you how all this mistaken idea of denouncing pleasure?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">Praising pain was born and I will give you a complete account of the system?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">Expound the actual teachings of the great-builder of human happiness?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">No one rejects, dislikes, or avoids pleasure itself, because it is pleasure?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">Because those who do not rationally encounter consequences?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="help-actions">
									<a href="#"><button type="button" class="btn-main btn-help btn-see-more"><?php echo lang('help_see_more'); ?></button></a>
								</div>
							</div>
							<div class="tab-pane fade show" id="helpPaymentTab" role="tabpanel">
								<div class="help-rows-container">
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">How does the subscription plan work?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">But I must explain to you how all this mistaken idea of denouncing pleasure?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">Praising pain was born and I will give you a complete account of the system?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">Expound the actual teachings of the great-builder of human happiness?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">No one rejects, dislikes, or avoids pleasure itself, because it is pleasure?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">Because those who do not rationally encounter consequences?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="help-actions">
									<a href="#"><button type="button" class="btn-main btn-help btn-see-more"><?php echo lang('help_see_more'); ?></button></a>
								</div>
							</div>
							<div class="tab-pane fade show" id="helpRegistrationTab" role="tabpanel">
								<div class="help-rows-container">
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">How does the subscription plan work?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">But I must explain to you how all this mistaken idea of denouncing pleasure?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">Praising pain was born and I will give you a complete account of the system?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">Expound the actual teachings of the great-builder of human happiness?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">No one rejects, dislikes, or avoids pleasure itself, because it is pleasure?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">Because those who do not rationally encounter consequences?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="help-actions">
									<a href="#"><button type="button" class="btn-main btn-help btn-see-more"><?php echo lang('help_see_more'); ?></button></a>
								</div>
							</div>
							<div class="tab-pane fade show" id="helpSigninTab" role="tabpanel">
								<div class="help-rows-container">
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">How does the subscription plan work?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">But I must explain to you how all this mistaken idea of denouncing pleasure?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">Praising pain was born and I will give you a complete account of the system?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">Expound the actual teachings of the great-builder of human happiness?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">No one rejects, dislikes, or avoids pleasure itself, because it is pleasure?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
									<div class="help-row">
										<div class="help-title">
											<button class="btn-collapser" type="button" data-toggle="collapse" aria-expanded="false">
												<div class="help-title-desc">Because those who do not rationally encounter consequences?</div>
												<span class="help-icon"><img src="<?php echo url_for("/assets/img/faq-plus.svg") ?>" alt=""></span>
											</button>
										</div>
										<div class="collapse">
											<div class="help-body">
												<p class="body-para help-para">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="help-actions">
									<a href="#"><button type="button" class="btn-main btn-help btn-see-more"><?php echo lang('help_see_more'); ?></button></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<!-- /.modal -->

<!-- Modal - Remove Saved Search Confirmation -->
<div id="modal_remove_saved" class="modal modal-remove-saved fade" aria-labelledby="modal_remove_saved" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?php echo lang('modal_delete_this_search'); ?></div>
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<p class="modal-para"><?php echo sprintf(lang('modal_are_you_sure_you_want_to_remove_this_x_saved_search'), '<span class="highlighted">x</span>'); ?></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal">
					<span class="btn-label"><?php echo lang('modal_cancel'); ?></span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
				</button>
				<button type="button" class="btn-icon-full delete-search btn-confirm" data-dismiss="modal" data-orientation="next">
					<span class="btn-label"><?php echo lang('modal_confirm'); ?></span>
					<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
				</button>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->

<!-- Modal - Continue Viewing -->
<div id="modal_cont_view" class="modal modal-cont-view fade" aria-labelledby="modal_cont_view" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
					<line x1="18" y1="6" x2="6" y2="18"></line>
					<line x1="6" y1="6" x2="18" y2="18"></line>
				</svg>
			</button>
			<div class="modal-body">
				<div class="modal-left-container">
					<div class="modal-cont-view-keyart"><img src="<?php echo url_for('/assets/img/MTP-Web-Illus-Indicator-take-a-break.png') ?>" alt="" /> </div>
				</div>
				<div class="modal-right-container">
					<div class="modal-task-details">
						<div class="modal-header"><?php echo lang('dashboard_you_were_viewing'); ?></div>
						<div class="modal-task-title"><?php echo isset($was_viewing) ? $was_viewing['title'] : '' ?></div>
						<div class="modal-task-val">
						<div class="amount-val">
								<span class="amount-prefix">RM</span>
								<span class="amount-val"><?php echo isset($was_viewing) && $was_viewing['p_type'] === 'task' ? number_format($was_viewing['budget']) : (isset($was_viewing['salary_range_max']) ? number_format($was_viewing['salary_range_max']) : '') ?></span>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<a href="<?php echo isset($was_viewing) ? url_for("/workspace/{$was_viewing['p_type']}/{$was_viewing['slug']}/details") . $was_viewing['external'] : '#'  ?>">
						<button type="button" class="btn-icon-full btn-view">
							<span class="btn-label"><?php echo lang('dashboard_view_detail'); ?></span>
							<span class="btn-icon">
								<svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
								</svg>
							</span>
						</button>
						</a>
						<div class="modal-fav-btn-container">
							<span class="btn-fav-label"><?php echo lang('dashboard_or_fav_it_for_later'); ?></span>
							<form method="post" class="favourite-form" action="<?php echo isset($was_viewing) ? url_for("/workspace/{$was_viewing['p_type']}/favourite") : '' ?>">
                                <?php echo html_form_token_field(); ?>
								<input type="hidden" name="<?php echo isset($was_viewing) && $was_viewing['p_type'] === 'task' ? 'task_id' : 'job_id' ?>" value="<?php echo isset($was_viewing) ? ($was_viewing['p_type'] === 'task' ? $was_viewing['task_id'] : $was_viewing['job_id']) : '' ?>">
								<?php if( !empty($was_viewing['external']) ): ?>
								<input type="hidden" name="external" value="true">
								<?php endif ?>
								<input type="hidden" name="favourited" value="false">
								<button type="submit" class="btn-icon-heart btn-fav">
									<span class="fav-icon"></span>
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->

<?php if( isset($was_viewing) ): ?>
 <script>
	$(document).ready(function() {

        $('#modal_cont_view').on('hidden.bs.modal', function(e){
            history.pushState({}, '', '<?php echo option('site_uri') . url_for('/dashboard') ?>');
        });

		setTimeout(function(){
			$('#modal_cont_view').modal({show:true});
		}, 1000);
	});
</script>
<?php endif ?>

<script> window.task_upload = "<?php echo option("site_uri") . url_for("/workspace/task/upload") ?>"; </script>
<script> window.feedback_upload = "<?php echo option("site_uri") . url_for("/feedback/upload") ?>"; </script>

<script>
    function totalTaskValue() {
        var x=(document.getElementById("bthTotalHours").value);
        var y=(document.getElementById("bthHourlyRate").value);
        var totalXY = Number(x)*Number(y);
        //if (totalXY > 0)
        document.getElementById("bthTaskValue").value=totalXY;
    }

    $('#assign-the-task').on('click', function(e){
        <?php if(!stripos(request_uri(), 'job-centre')): ?>
        $('#assign-task')[0].submit();
        //hire_candidate( $('#assign-task') );
        //$('#modal_confirm_talent').modal('hide');
    <?php else: ?>
        $('#shortlist-user')[0].submit();
        <?php endif ?>
    });

    function hire_candidate(e){
        var form = e.serialize();
        $.ajax({
	        url: e.attr('action'),
	        method: 'POST',
	        data: form
        }).done(function(response){
           if(response !== undefined){
               t(response.status === 'success' ? 's' : 'e',
	               response.message,
                   response.status === 'success' ? '<?php echo lang('success') ?>' : '<?php echo lang('error') ?>'
               );

               if( response.status === 'success' ){
                   var link = window.hire_link;
                   link.find('span.applicant-hiremilestone-lbl').text('Candidate has been notified');
                   link.addClass('disabled-link');
               }

           }
           window.hire_link = undefined;
        });

    }

    $('.btn-confirm-task-discard').on('click', function (event) {
        var taskForm = $('form#form_post_task_od');
        taskForm[0].reset();
        taskForm.find('input[type="checkbox"], input[type="radio"]').prop('checked', false);
        taskForm.find(".mapSearchBox").hide();
        taskForm.find(".frm").hide();
        taskForm.find("#sf1").show();
        taskForm.find('#form_bylumpsum, #form_byhour, #form_travelrequired, #mapTaskLoc').hide();
        taskForm.find('#sf3 .modal-pat-keyart').show();
        taskForm.find('#tag1').tagsinput('removeAll');
        taskForm.find('input[name*="od_qna[]"]').each(function(idx, ele){
            if(idx === 0){
                ele.value = '';
            }else{
                $(ele).parent().parent().remove();
            }
        });
        taskForm.find('.summernote').summernote('reset');
        taskForm.find('.summernote').summernote('destroy');
    });

    $('.btn-confirm-job-discard').on('click', function (event) {
        var jobForm = $('form#form_post_job_ft');
        jobForm[0].reset();
        jobForm.find('input[type="checkbox"], input[type="radio"]').prop('checked', false);
        jobForm.find(".frm-ft").hide();
        jobForm.find("#ft_sf1").show();
        jobForm.find('#ft_joblocation').removeData('editJob');
        jobForm.find('input[name*="ft_qna[]"]').each(function(idx, ele){
            if(idx === 0){
                ele.value = '';
            }else{
                $(ele).parent().parent().remove();
            }
        });
        jobForm.find('.summernote').summernote('reset');
        jobForm.find('.summernote').summernote('destroy');
    });

    $('#modal_feedback').on('show.bs.modal', function(e){
       var button = $(e.relatedTarget);
       var option = button.data('select');
       var report = button.data('report');
       if( option ){
	       $(this).find('#select_feedback_area').val( option );
       }
       if( report ){
           $(this).find('#to-report').val( report );
       }
    });

    $('#modal_feedback').on('hidden.bs.modal', function(e){
        $(this).find('#select_feedback_area').children().first().prop('selected', true);
        $(this).find('#to-report').val('');
    });

    $('#feedback-submit').on('click', function(e){
        var data = $('#form-feedback').serialize();
        var url = '<?php echo option('site_uri') . url_for('/feedback') ?>';
        $.ajax({
	        url: url,
	        data: data,
	        method: 'POST'
        }).done( function(response){
            t(response.status, response.message);
            window.feedbackdropzone.submitted = true;
            window.feedbackdropzone[0].dropzone.removeAllFiles(true);
            $('#form-feedback')[0].reset();
            window.feedbackdropzone.submitted = undefined;
            $('#table-listing-container-feedback').html(response.feedback);
            $('#modal_feedback a.page-link').off('click').on('click', function(e){ feedback_paginate(e) });
        });
    });

    $('.btn-cancel, .close').on('click', function(e){
        window.feedbackdropzone[0].dropzone.removeAllFiles(true);
        $('#form-feedback')[0].reset();
    });

    $('#modal_feedback a.page-link').on('click', function(e){ feedback_paginate(e) });

    function feedback_paginate(e){
        e.preventDefault();
        $.ajax({
            url: e.currentTarget.href
        }).done(function(response){
            $('#table-listing-container-feedback').html(response);
            $('#modal_feedback a.page-link').off('click').on('click', function(e){ feedback_paginate(e) });
        });
    }

    $('#form_post_task_od').find('input').on('keypress', function(e){
        if(e.keyCode == 13 && e.target.type !== 'submit') {
            e.preventDefault();
            var inputs = $(this).closest('form').find(':input:visible');
            $(e.target).blur();
            inputs.eq( inputs.index(this)+ 1 ).focus();
        }
    });
</script>
<!-- Post Task Modal - Google Map Drop Pin -->
<script src="https://maps.google.com/maps/api/js?libraries=&v=weekly&key=AIzaSyCKkk8Hj4PMcGn7bheNToZyDVJixuLsVL8&libraries=places"></script>

<script>
    //the maps api is setup above

    $(window).on("load", function() {
        var latlng = new google.maps.LatLng(3.110651, 101.666760); //Set the default location of map
        var map = new google.maps.Map(document.getElementById('mapTaskLoc'), {
            center: latlng,
            zoom: 15, //The zoom value for map
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var jobMap = new google.maps.Map(document.getElementById('mapJobLoc'), {
            center: latlng,
            zoom: 15, //The zoom value for map
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: 'Place the marker for your location!', //The title on hover to display
            draggable: true //this makes it drag and drop
		});

        var jobMarker = new google.maps.Marker({
            position: latlng,
            map: jobMap,
            title: 'Place the marker for your location!', //The title on hover to display
            draggable: true //this makes it drag and drop
		});

		const infowindow = new google.maps.InfoWindow();
		const geocoder = new google.maps.Geocoder();
        google.maps.event.addListener(marker, 'dragend', function(a) {
            //const infowindow = new google.maps.InfoWindow();
            const latlng = {
                lat: a.latLng.lat(),
                lng: a.latLng.lng(),
            };
            geocodeLatLng(latlng);
            //document.getElementById('loc').value = a.latLng.lat().toFixed(4) + ', ' + a.latLng.lng().toFixed(4); //Place the value in input box
        });
        google.maps.event.addListener(jobMarker, 'dragend', function(a) {
            //const infowindow = new google.maps.InfoWindow();
            const latlng = {
                lat: a.latLng.lat(),
                lng: a.latLng.lng(),
            };
            jobGeocodeLatLng(latlng);
            //document.getElementById('loc').value = a.latLng.lat().toFixed(4) + ', ' + a.latLng.lng().toFixed(4); //Place the value in input box
        });

        function geocodeLatLng(latLng) {
            const latlng = latLng;
            geocoder.geocode({ location: latlng }, (results, status) => {
                if (status === "OK") {
                    if (results[0]) {
                        var comps = results[0].address_components;
                        var state_country = [];
                        comps.forEach(function(comp, id){
                            if(comp.types.includes('administrative_area_level_1') || comp.types.includes('country')) {
                                if(state_country.length > 0 && comp.types.includes('administrative_area_level_1')){
                                    state_country.unshift(comp.long_name);
                                }else{
                                    state_country.push(comp.long_name);
                                }
                            }
                        });
                        map.setZoom(15);
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                        document.getElementById('loc').value = results[0].formatted_address;
                        document.getElementById('state_country').value = state_country.join(', ');
                    } else {
                        window.alert("No results found");
                    }
                } else {
                    window.alert("Geocoder failed due to: " + status);
                }
            });
		}

        function jobGeocodeLatLng(latLng) {
            const latlng = latLng;
            geocoder.geocode({ location: latlng }, (results, status) => {
                if (status === "OK") {
                    if (results[0]) {
                        if($('#ft_joblocation').data('editJob') === undefined){
	                        var comps = results[0].address_components;
	                        var state_country = [];
	                        comps.forEach(function(comp, id){
	                            if(comp.types.includes('administrative_area_level_1') || comp.types.includes('country')) {
	                                if(state_country.length > 0 && comp.types.includes('administrative_area_level_1')){
	                                    if(comp.long_name === "Wilayah Persekutuan Kuala Lumpur" || comp.long_name === "Federal Territory of Kuala Lumpur") {
	                                        state_country.unshift("Kuala Lumpur");
	                                    } else if(comp.long_name === "Pulau Pinang" || comp.long_name === "Pulau Penang" || comp.long_name === "Pinang" ){
                                            state_country.unshift("Penang");
	                                    } else if(comp.long_name === "Johor Bahru" ){
                                            state_country.unshift("Johor");
	                                    } else {
	                                        state_country.unshift(comp.long_name);
	                                    }
	                                }else{
	                                    if(comp.long_name === "Wilayah Persekutuan Kuala Lumpur" || comp.long_name === "Federal Territory of Kuala Lumpur") {
	                                        state_country.push("Kuala Lumpur");
	                                    } else if(comp.long_name === "Pulau Pinang" || comp.long_name === "Pulau Penang" || comp.long_name === "Pinang" ){
                                            state_country.unshift("Penang");
                                        } else if(comp.long_name === "Johor Bahru" ){
                                            state_country.unshift("Johor");
                                        } else {
	                                        state_country.push(comp.long_name);
	                                    }
	                                }
	                            }
	                        });

	                        var c = [];
	                        var countries = Array.from(document.querySelector('#ft_jobcountry').options).map(function(e){ c[e.value] = e.innerText });
	                        var country = c.findIndex(function(el){ return el == state_country[1] });
	                        if(country){
	                            var country_el = document.querySelector('#ft_jobcountry');
	                            country_el.value = country;
	                            $(country_el).trigger('change');
                                $('#ft_jobstate').on('countryUpdated', function(e){
                                    var _options = this.options;
	                                var s = [];
	                                var states = Array.from(_options).map(function(e){ s[e.value] = e.innerText });
	                                var state = s.findIndex(function(el){ return el == state_country[0] });
	                                if( state > 0 ){
	                                    this.value = state;
	                                }else{
	                                    this.options[0].selected = true;
	                                }
                                })

	                        }
                            document.getElementById('ft_joblocation').value = results[0].formatted_address;
                        }
                        //document.querySelector('#ft_jobcountry').value
                        jobMap.setZoom(15);
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(jobMap, jobMarker);
                    } else {
                        window.alert("No results found");
                    }
                } else {
                    window.alert("Geocoder failed due to: " + status);
                }
            });
		}

		$(document).on('change', '#tl_travel', function () {
            if (this.checked) {
                $('#mapTaskLoc').fadeIn('fast');
                $('#form_travelrequired').show();
                $('.mapSearchBox').show();
                $('#sf3 .modal-pat-keyart').hide();
                $('.map-search-input').keydown(function(e){
                    if(e.keyCode === 13){
                        e.preventDefault();
                        return;
                    }
                });

                if($('#loc').val().length === 0) {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (location) {
                            latlng = {lat: location.coords.latitude, lng: location.coords.longitude};
                            marker.setPosition(new google.maps.LatLng(latlng));
                            map.panTo(new google.maps.LatLng(latlng));
                            geocodeLatLng(latlng);
                        });
                    }
                }else{
                    var address = $('#loc').val();
                    var locationAddress = encodeURI(address);
                    var locationRequestURI = "https://maps.googleapis.com/maps/api/geocode/json?address="+locationAddress+"&key=AIzaSyCKkk8Hj4PMcGn7bheNToZyDVJixuLsVL8";
                    $.ajax({
                        url: locationRequestURI
                    }).done(function(response){
                        if(response.status === "OK"){
                            var location = response.results[0].geometry.location;
                            marker.setPosition(new google.maps.LatLng(location));
                            map.panTo(new google.maps.LatLng(location));
                            geocodeLatLng(location);
                        }
                    })
                }
            }
        });

		$('.ft-open3').on('click', function () {
            $('#mapJobLoc').fadeIn('fast');
            $('#form_travelrequired').show();
            $('.mapSearchBox').show();
            $('#ft_sf4 .modal-pat-keyart').hide();
            $('.map-search-input').keydown(function(e){
                if(e.keyCode === 13){
                    e.preventDefault();
                    return;
                }
            })

            if($('#ft_joblocation').val().length === 0) {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (location) {
                        latlng = {lat: location.coords.latitude, lng: location.coords.longitude};
                        jobMarker.setPosition(new google.maps.LatLng(latlng));
                        jobMap.panTo(new google.maps.LatLng(latlng));
                        jobGeocodeLatLng(latlng);
                    });
                }
            }else{
                var address = $('#ft_joblocation').val();
                var locationAddress = encodeURI(address);
                var locationRequestURI = "https://maps.googleapis.com/maps/api/geocode/json?address="+locationAddress+"&key=AIzaSyCKkk8Hj4PMcGn7bheNToZyDVJixuLsVL8";
                $.ajax({
                    url: locationRequestURI
                }).done(function(response){
                    if(response.status === "OK"){
                        var location = response.results[0].geometry.location;
                        jobMarker.setPosition(new google.maps.LatLng(location));
                        jobMap.panTo(new google.maps.LatLng(location));
                        jobGeocodeLatLng(location);
                    }
                })
            }
        });
		
		var inputs = document.getElementsByClassName('map-search-input');
		var autocomplete = [];
		for (var i = 0; i < inputs.length; i++) {
			id = 'autocomplete-' + i;
			input = inputs[i];
			$(input).attr('id', id);

			autocomplete = new google.maps.places.Autocomplete(input, {componentRestrictions: {'country': ['my']}});
			autocomplete.inputId = id;
			autocomplete.addListener('place_changed', getLatLng);
		}

		function getLatLng(){
			var input = '#' + this.inputId;
			var place = this.getPlace();
			lat = place.geometry.location.lat(),
			lng = place.geometry.location.lng();
			latlng = { lat: lat, lng: lng };

			if( this.inputId === 'autocomplete-0' ) {
                marker.setPosition(new google.maps.LatLng(latlng));
                map.panTo(new google.maps.LatLng(latlng));
                geocodeLatLng(latlng);
            }else{
                jobMarker.setPosition(new google.maps.LatLng(latlng));
                jobMap.panTo(new google.maps.LatLng(latlng));
                jobGeocodeLatLng(latlng);
            }

			$(input).val('');
		}
	});

    document.getElementById('chat-upload-btn').addEventListener('click', openDialog);

	function openDialog() {
	  document.getElementById('hiddenUploadButton').click();
	}

    function removeMesssage(id){
    	 var url = window.location.pathname;
    	
       $.ajax({
	      url: rootPath + 'ajax/chat/removeChatMesssage/' + id ,
	      type :"POST",
	      success: function(response){

	      },
	   });
	}

	function replyMessage(el){
		is_me = $(el).closest('.message').hasClass('message-right');
		if(is_me){
			msg_from = 'You';
		}else{
			msg_from = $('h6.chat-body-name').text();
		}

		reply_msg = $(el).parent().parent().prev().prev().length;
		if(reply_msg){
			reply_msg = $(el).parent().parent().prev().prev().get(0).outerHTML;
		}else{
			reply_msg = $(el).parent().parent().prev().get(0).outerHTML;
		}
		$('.message-reply-content[data-type="text"]').removeClass('d-none').show();
		$('.message-reply-content[data-type="text"] > div > div').html('<b>' + msg_from + '</b><br>' + reply_msg);
		$('.form-footer-reply').removeClass('d-none').show();
		$('.chat-footer').addClass('is-reply');
		$('#replyID').val(el.id);
		$('#chat-id-input').focus();
	}

	function copyToClipboard(el) {
		var $temp = $('<input>');
		$('#modal_chat').append($temp);
		$temp.val($(el).text()).select();
		document.execCommand('copy');
		$temp.remove();
	}

	function goToMessage(el){
		$('.chat-content').animate({
			scrollTop: $('.chat-content')[0].scrollTop + $(el).offset().top - $('.chat-header').outerHeight() - 60,
		}, 500, function(){
			$(el).find('.message-content').effect("highlight", {color: '#00c3bc'}, 1500);
		});
	}

	function bytesToSize(bytes) {
		var sizes = ['B', 'KB', 'MB', 'GB', 'TB'];
		if (bytes == 0) return '0 Byte';
		var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
		return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}
</script>
<!-- Post Task Modal - Google Map Drop Pin -->

<!-- Chat Footer - Close Reply -->
<script>
	$(function(){
		$(document).on('click', '#closeReply', function() {
			$(this).parent().hide();
			$(this).closest('.chat-footer').removeClass('is-reply');
			$('.chat-footer .message-reply-content').addClass('d-none')
			$('#replyID').val('');
			$('#hiddenUploadButton').val('');
			tmp_files = Array();
		});

		tmp_files = Array();
		max_file = <?php echo $settings['chat_max_file'] ? $settings['chat_max_file'] : 0; ?>;
		file_max_size = <?php echo $cms->settings()['chat_max_size'] ? $cms->settings()['chat_max_size'] * 1048576 : 0; ?>;
		file_extensions = <?php echo $cms->settings()['chat_file_extensions'] ? json_encode(explode(',', $cms->settings()['chat_file_extensions'])) : json_encode(array()); ?>;
		
        $(document).on('change', $('#hiddenUploadButton')[0].files, function(){
			files = $('#hiddenUploadButton')[0].files;
			$('.message-reply-content[data-type="attachment"] .form-row').html('');
			if(files.length){
				$(files).each(function(i, v){
					exists = false;
					ext = v.name.slice(v.name.lastIndexOf('.') + 1);
					$(tmp_files).each(function(j, v2){
						if(v2.name == v.name){
							exists = true;
						}
					});
					if(max_file == 0 || (tmp_files.length + 1) <= max_file){
						if(!exists){
							if(v.size <= file_max_size || file_max_size == 0){
								if( $.inArray(ext, file_extensions) > -1 ){
									tmp_files.push(v);
									
								}else{
									t('e', '<?php echo sprintf(lang('sys_upload_extension_error'), '', ''); ?>');
								}
							}else{
								t('e', '<?php echo sprintf(lang('sys_upload_max_size_error'), $cms->settings()['chat_max_size'] . 'MB'); ?>');
							}
						}
					}else if(tmp_files.length + 1 > max_file){
						t('e', '<?php echo lang('sys_upload_max_file_error'); ?>');
					}
				});
				previewAttachment();
			}			
		});

		previewAttachment = function(){
			if(tmp_files.length){
				$(tmp_files).each(function(i, v){
					src = URL.createObjectURL(v);
					
					if (v.type.match(/image/)) {
						$('.message-reply-content[data-type="attachment"] .form-row').append('<div class="col middle attachment-preview" data-title="' + v.name + ' (' + bytesToSize(v.size) + ')"><img class="img-fluid rounded" src="' + src + '"><a href="#" class="remove-attachment-file" data-id="' + i + '"><i class="fa fa-trash"></i></a></div>');

					}else{
						ext = v.name.slice(v.name.lastIndexOf('.') + 1);
						name = v.name.length < (12 + ext.length) ? v.name : v.name.substring(0, 12) + '...' + ext;
						$('.message-reply-content[data-type="attachment"] .form-row').append('<div class="col middle attachment-preview file" data-title="' + v.name + '"><div><span>' + name + '</span><br><small>' + bytesToSize(v.size) + '</small></div><a href="#" class="remove-attachment-file" data-id="' + i + '"><i class="fa fa-trash"></i></a></div>');
					}
				});
				$('.message-reply-content[data-type="attachment"]').removeClass('d-none').show();
				$('.form-footer-reply').removeClass('d-none').show();
				$('.chat-footer').addClass('is-reply');
				$('#chat-id-input').focus();
				$('[data-title]').tooltip();
			}
		}

		$(document).on('click', '.remove-attachment-file', function(){
			el = $(this);
			id = el.data('id');

			$('[data-title]').tooltip('dispose');

			el.parent().slideUp(function(){
				el.parent().remove();
				$('[data-title]').tooltip();
				tmp_files.splice(id, 1);
				if(!tmp_files.length){
					$('#closeReply').click();
				}
			});			
		});

		$(document).on('click', '[data-action="zoom"]', function(e){
			e.preventDefault();
			el = $(this);
			href = el.data('href');
			$('#modal_chat_img .img-zoom img').attr('src', href);
			$('#modal_chat_img .download-btn a').attr('href', href);
			$('#modal_chat_img').modal('show');
		});

		$(document).on('show.bs.modal', '#modal_chat_img', function(){
			$('#modal_chat').css('opacity', '0.3');
		});

		$(document).on('hide.bs.modal', '#modal_chat_img', function(){
			$('#modal_chat').css('opacity', '1');
		});
	});
</script>
<!-- Chat Footer - Close Reply -->

<script>
$(function(){
	paginationLimit = 5;
	total = Array();
	totalPage = Array();

	$('#pills-tabContentHelp .tab-pane').each(function(i, e){
		id = $(this).attr('id');
		
		total[i] = $('#' + id + ' .help-rows-container .help-row').length;
		totalPage[i] = Math.ceil(total[i] / paginationLimit);
		
		$('#' + id + ' .help-rows-container .help-row:nth-child(' + paginationLimit + ')').nextAll().hide();

		if(totalPage[i] > 1){
			$('#' + id + ' .btn-see-more').data({'id': id, 'next': 2, 'total': totalPage[i] });
		}else{
			$('#' + id + ' .btn-see-more').hide();
		}
	});

	$(document).on('click', '#modal_help .btn-see-more', function(){
		btnMore = $(this);
		id = btnMore.data('id');
		total = btnMore.data('total');
		next = btnMore.data('next');

		if(next <= total){
			visible = $('#' + id + ' .help-rows-container .help-row:visible').length;

			for(i = 1; i <= paginationLimit; i++){
				if( $('#' + id + ' .help-rows-container .help-row:nth-child(' + (visible + i) + ')').length ){
					$('#' + id + ' .help-rows-container .help-row:nth-child(' + (visible + i) + ')').slideDown();
				}
			}

			btnMore.data('next', next + 1);
			if((next + 1) > total){
				btnMore.hide();	
			}

		}else{
			btnMore.hide();
		}
	});
});
</script>