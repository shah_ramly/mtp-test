<div class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-chart">
				<div class="card-header">
					<div class="row">
						<div class="col-sm-12 col-header-filter">
							<h2 class="card-title"><?php echo 'Search'; ?></h2>
							<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
								<svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/></svg>
							</button>
                            <?php echo partial('workspace/partial/header.html.php'); ?>
						</div>
					</div>
				</div>

				<?php echo partial('workspace/partial/tasks-listing.html.php'); ?>
			</div>
		</div>
	</div>
</div>
