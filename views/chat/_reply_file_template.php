
<a href="#" class="message-reply-content-link" download>
    <div class="message-reply-content" onClick="goToMessage('#msg-<?php echo $message['id']; ?>')">
        <div class="message-reply-content-wrapper">
            <h6><?php echo $message['sender_id'] == $user->id ? 'You' : $member['name']; ?></h6>
            <div class="media">
                <span class="icon-shape">
                    <span class="non-icon-download"><?php echo strtoupper($message['file_extension']) ?></span>
                </span>
                <div class="media-body overflow-hidden flex-fill">
                    <span class="d-block text-truncate font-medium text-reset"><?php echo basename($message['contents']); ?></span>
                    <ul class="list-inline small mb-0">
                        <li class="list-inline-item">
                            <span class="t"><?php echo bytesToSize($message['size']); ?></span>
                        </li>
                        <li class="list-inline-item">
                            <span class="text-uppercase"><?php echo $message['file_extension'] ?></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
     
</a>