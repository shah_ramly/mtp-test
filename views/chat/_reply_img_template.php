 
<?php $imagePath = $cms->getImageBatch($message['batch_id']); ?>
 <a href="#" class="message-reply-content-link">
    <div class="message-reply-content" onClick="goToMessage('#msg-<?php echo $message['id']; ?>')">
        <div class="message-reply-content-wrapper">
            <div class="message-content-attachment">
                <h6 class="mb-2"><?php echo $message['sender_id'] == $user->id ? sprintf(lang('chat_you_shared_x_photos'), count($imagePath)) : $member['name'] .' '. sprintf(lang('chat_shared_x_photos'), count($imagePath)) ?>:</h6>
                <div class="form-row">
                    <?php for($i =0 ; $i<count($imagePath);$i++){?>
                        <div class="col">
                            <img class="img-fluid rounded" src="<?php echo imgCrop($imagePath[$i]['contents'], 80, 80); ?>">
                        </div>
                    <?php } ?>
                </div>
                <?php
                    if($message['type'] == 'textMix'){
                        echo $message['contents'];
                    }
                ?>
            </div>
        </div>
    </div>
</a>