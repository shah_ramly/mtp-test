<?php $message = $cms->getPinnedMessageContent($pinnedItems[$i]['message_id']); ?>

                                                            <li class="list-pinned-item">
                                                                <div class="pin-item<?php echo $message['sender_id'] != $user->id ? ' member' : ''; ?>">
                                                                    <div class="media">
                                                                        <a href="<?php echo url_for('/' . $message['contents']); ?>" class="icon-shape" download>
                                                                            <span class="non-icon-download"><?php echo $message['file_extension'] ?></span>
                                                                        </a>
                                                                        <div class="media-body overflow-hidden flex-fill">
                                                                            <a href="<?php echo url_for('/' . $message['contents']); ?>" class="d-block text-truncate font-medium text-reset" download><?php echo basename($message['contents']) ?></a>
                                                                            <ul class="list-inline small mb-0">
                                                                                <li class="list-inline-item">
                                                                                    <span class="t"><?php echo $cms->fileSize($message['size']) ?></span>
                                                                                </li>
                                                                                <li class="list-inline-item">
                                                                                    <span class="text-uppercase"><?php echo $message['file_extension'] ?></span>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mt-1">
                                                                        <small class="opacity-65"><?php echo timeAgo($pinnedItems[$i]['pinned_time']); ?></small>
                                                                    </div>
                                                                    <div class="dropdown dropdown-chat">
                                                                        <a class="message-row-dropdown-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                <circle cx="12" cy="12" r="1"></circle>
                                                                                <circle cx="19" cy="12" r="1"></circle>
                                                                                <circle cx="5" cy="12" r="1"></circle>
                                                                            </svg>
                                                                        </a>

                                                                        <div class="dropdown-menu">
                                                                            <a class="dropdown-item" onClick="unpinMessage(<?php echo $pinnedItems[$i]['id']; ?>, this)">
                                                                                <?php echo lang('chat_unpin') ?>
                                                                            </a>
                                                                            <a class="dropdown-item" onclick="goToMessage('#msg-<?php echo $message['id']; ?>')">
                                                                                <?php echo lang('chat_go_to_message') ?>
                                                                            </a>
                                                                            <a class="dropdown-item" onclick="copyToClipboard('#c<?php echo $message['id']; ?>')">
                                                                                <?php echo lang('chat_copy') ?>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>