<a href="#" class="message-reply-content-link">
	<div class="message-reply-content" onClick="goToMessage('#msg-<?php echo $message['id']; ?>')">
	    <div class="message-reply-content-wrapper">
	        <div>
				<b><?php echo $message['sender_id'] == $user->id ? 'You' : $member['name']; ?></b><br>
				<?php echo $message['contents'] ?>
			</div>
	    </div>
	</div>
</a>

