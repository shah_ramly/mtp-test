<?php 
if($cms->getImageBatch($message['batch_id'])){
    include "_reply_img_template.php";
}

$filePath = $cms->getFileBatch($message['batch_id']); 
for($i =0 ; $i<count($filePath);$i++){ ?>

<a href="#" class="message-reply-content-link" download>
    <div class="message-reply-content" style="border-left: 6px solid #525F7F;" onClick="goToMessage('#msg-<?php echo $message['id']; ?>')">
        <div class="message-reply-content-wrapper">
            <h6><?php echo $message['sender_id'] == $user->id ? 'You' : $member['name']; ?></h6>
            <div class="media">
                <span class="icon-shape">
                    <span class="non-icon-download"><?php echo strtoupper($filePath[$i]['file_extension']);?></span>
                </span>
                <div class="media-body overflow-hidden flex-fill">
                    <span class="d-block text-truncate font-medium text-reset"><?php echo basename($filePath[$i]['contents']);?></span>
                    <ul class="list-inline small mb-0">
                        <li class="list-inline-item">
                            <span class="t"><?php echo bytesToSize($filePath[$i]['size']); ?></span>
                        </li>
                        <li class="list-inline-item">
                            <span class="text-uppercase">pdf</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</a>
<?php } ?>


