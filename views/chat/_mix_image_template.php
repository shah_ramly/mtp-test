<a href="#" class="message-reply-content-link">
    <div class="message-reply-content" style="border-left: 0px solid #007aff;">
        <div class="message-reply-content-wrapper">
            <div class="message-content-attachment">
                <h6 class="mb-2"><?php echo $imagePath[0]['sender_id'] == $user->id ? sprintf(lang('chat_you_shared_x_photos'), count($imagePath)) : $member['name'] .' '. sprintf(lang('chat_shared_x_photos'), count($imagePath)); ?>:</h6>
                <div class="form-row">
                 <?php for($i =0 ; $i<count($imagePath);$i++){?>
                    <div class="col">
                        <a href="#" data-action="zoom" data-href="<?php echo imgCrop($imagePath[$i]['contents']); ?>">
                            <img class="img-fluid rounded" src="<?php echo imgCrop($imagePath[$i]['contents'], 80, 80); ?>">
                        </a>
                    </div>
                  <?php } ?>
                   
                </div>
            </div>


        </div>
    </div>   
</a>