<!-- Body -->
<div class="hide-scrollbar flex-fill">

	<div class="chat-sidebar-details-inner">
		<div class="chat-sidebar-details-card chat-sidebar-details-back">
			<a class="nav-chat-details-link nav-link text-muted px-0" href="#" data-chat-sidebar-close="">
                <span class="chevron-chat-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                        <path d="M15 18l-6-6 6-6"></path>
                    </svg>
                </span>
			</a>
		</div>
		<div class="chat-sidebar-details-card chat-sidebar-details-profile-card">
			<!-- Photo -->
			<div class="profile-avatar">
				<img class="avatar-img" src="<?php echo imgCrop($member['photo'], 100, 100, 'assets/img/default-avatar.png'); ?>">
			</div>
			
			<a class="side-bar-name"  href="#"><h5 class="profile-card-name"><?php echo $member['type'] === '1' ? $member['company']['name'] : $member['name']; ?></h5></a>
			<p class="profile-card-desc"></p>
		</div>

		<div class="chat-sidebar-details-card chat-sidebar-details-profile-images-card">
            <button class="btn btn-flex btn-collapse collapsed" type="button" data-toggle="collapse"
                    data-target="#collapseImages" aria-expanded="false" aria-controls="collapseImages">
	            <?php echo lang('chat_image') ?> (<?php echo count($imageByTask) ?>)
	            <span class="chevron-collapse-icon">
		            <svg class="bi bi-chevron-down" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                         <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"></path>
		            </svg>
	            </span>
            </button>
            <div class="collapse" id="collapseImages" style="">
                <div class="collapse-content collapse-content-images">
                    <ul class="chat-details-images-list">
						<?php for($i = 0; $i <count($imageByTask); $i++){ ?>
                        <!-- Image -->
                        <li class="list-images-item">
                            <a href="#" class="images-media-link" data-action="zoom" data-href="<?php echo imgCrop($imageByTask[$i]['contents']); ?>">
                                <img class="img-fluid rounded" src="<?php echo imgCrop($imageByTask[$i]['contents'], 80, 80); ?>" >
                            </a>
                        </li>
                        <!-- Image -->      
						<?php } ?>                                             
                    </ul>
                </div>
            </div>
    	</div>

		<div class="chat-sidebar-details-card chat-sidebar-details-profile-files-card">
            <button class="btn btn-flex btn-collapse collapsed" type="button" data-toggle="collapse"
                    data-target="#collapseFiles" aria-expanded="false" aria-controls="collapseFiles">
	            <?php echo lang('chat_files') ?> (<?php echo count($fileByTask) ?>)
	            <span class="chevron-collapse-icon">
		            <svg class="bi bi-chevron-down" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"></path>
		            </svg>
	            </span>
            </button>
            <div class="collapse" id="collapseFiles" style="">
                <div class="collapse-content collapse-content-files">
                    <ul class="chat-details-files-list">
                    	<?php for($i = 0; $i <count($fileByTask); $i++) {  ?>
                            <!-- File -->
                            <li class="list-file-item">
                                <div class="files-media">
                                    <div class="file-icon-shape">
                                        <span class="file-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-paperclip injected-svg">
                                                <path d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path>
                                            </svg>
                                        </span>
                                    </div>
                                    <div class="files-media-body">
                                        <h6 class="files-media-title">
                                            <a href="<?php echo $fileByTask[$i]['contents'] ?>" class="files-media-title-link" title="<?php echo basename($fileByTask[$i]['contents']) ?>" download > <?php echo basename($fileByTask[$i]['contents']) ?></a>
                                        </h6>
                                        <ul class="files-media-size-format">
                                            <li class="files-list-item files-list-item-size">
                                                <span class="text-muted"><?php echo $cms->fileSize($fileByTask[$i]['size']) ?></span>
                                            </li>
                                            <li class="files-list-item files-list-item-format">
                                                <span class="text-muted"><?php echo $fileByTask[$i]['file_extension'] ?></span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="files-more-opt">
                                        <div class="dropdown">
                                            <a href="#" class="files-more-opt-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="three-dots-v-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                                                </span>
                                            </a>
                                            <div class="dropdown-menu" style="">
                                                <a class="dropdown-item d-flex align-items-center" href="<?php echo $fileByTask[$i]['contents'] ?>" download>
                                                    <?php echo lang('chat_download') ?> <span class="ml-auto fe-download"></span>
                                                </a>
                                                <a class="dropdown-item d-flex align-items-center" onclick="goToMessage('#msg-<?php echo $fileByTask[$i]['go_to_id']; ?>')">
                                                    <?php echo lang('chat_go_to_message') ?> <span class="ml-auto fe-trash-2"></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!-- File -->
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>

		<div class="chat-sidebar-details-card chat-sidebar-details-profile-pinned-card">
            <button class="btn btn-flex btn-collapse collapsed" type="button" data-toggle="collapse"
                    data-target="#collapsePinned" aria-expanded="false" aria-controls="collapsePinned">
	            <?php echo lang('chat_pinned_items') ?> (<?php echo count($pinnedItems); ?>)
	            <span class="chevron-collapse-icon">
		            <svg class="bi bi-chevron-down" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"></path>
		            </svg>
	            </span>
            </button>
            <div class="collapse" id="collapsePinned" style="">
                <div class="collapse-content collapse-content-pinned">
        	        <ul class="chat-details-pinned-list chat-details-files-list">
                        <?php for($i=0; $i< count($pinnedItems);$i++){ 
                            if($pinnedItems[$i]['message_type']=="text"){
                                include  '_pinned_text.php';
                            }else if($pinnedItems[$i]['message_type']=="mix"){
                                include  '_pinned_mix.php';
                            }else if($pinnedItems[$i]['message_type']=="image"){
                                include  '_pinned_image.php';
                            }else if($pinnedItems[$i]['message_type']=="file"){
                                include  '_pinned_file.php';
                            }
                        } ?>
                    </ul>
                </div>
            </div>
        </div>
	</div>
</div>
<!-- Body -->
<script type="text/javascript">
     $('.side-bar-name').attr('href', '<?php echo $member['type'] === '1' ? url_for(str_replace('talent', 'company', $member['url'])) : url_for($member['url']); ?>');
</script>