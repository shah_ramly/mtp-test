<!-- Message -->
<div class="message message-right" id="msg-<?php echo $key['id']; ?>">
    <!-- Avatar -->
    <div class="avatar avatar-online avatar-sm ml-4 ml-lg-5 d-none d-lg-block">
        <img class="avatar-img" src="<?php echo imgCrop($user->info['photo'], 50, 50, 'assets/img/default-avatar.png') ?>" alt="">
    </div>

    <!-- Message: body -->
    <div class="message-body">

        <!-- Message: row -->
        <div class="message-row">
            <div class="message-row-flex message-row-flex-end">
                <!-- Message: content -->
                <div class="message-content bg-blue">
                    <?php 
                    if($key['reply_id'] != "" ) {
                        $message = $cms->returnMessageType($key['reply_id']);
                        ($message['type'] == "text") ? include '_reply_text_template.php' : "";
                        ($message['type'] == "file") ? include '_reply_file_template.php' : "";
                        ($message['type'] == "image") ? include '_reply_img_template.php' : "";
                        (strpos($message['type'], "Mix") !== false) ? include '_reply_mix_template.php' : "";
                        }
                        ?>   


                    <div id="c<?php echo $key['id']; ?>" class="content-text"><?php echo $key['contents']; ?></div>

                    <div class="mt-1">
                        <small class="opacity-65"><?php echo date_format($date, $cms->settings()['time_format']); ?></small>
						<?php if($key['viewed']){ ?>
						<span class="float-right">
						<i class="fa fa-check" title="Seen on <?php echo date($cms->settings()['time_format'], strtotime($key['viewed_date'])); ?>"></i>
						</span>
						<?php } ?>
                    </div>
                    <!-- Message: dropdown -->
                    <div class="dropdown dropdown-chat">
                        <a class="message-row-dropdown-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                <circle cx="12" cy="12" r="1"></circle>
                                <circle cx="19" cy="12" r="1"></circle>
                                <circle cx="5" cy="12" r="1"></circle>
                            </svg>
                        </a>
                        <?php //ar_dump($_SESSION) ?>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" onclick="pinMessage(<?php echo $key['id']; ?>,'text')">
                                <?php echo lang('chat_pin') ?>
                            </a>
                            <a class="dropdown-item" onClick="copyToClipboard('#c<?php echo $key['id']; ?>')">
                                <?php echo lang('chat_copy_text') ?>
                            </a>
                            <a class="dropdown-item" onclick="replyMessage(this)" id="<?php echo $key['id']; ?>"> 
                                <?php echo lang('chat_reply') ?>
                            </a>
                            <a class="dropdown-item" onclick="removeMesssage(this.id)" id="<?php echo $key['id']; ?>">
	                            <?php echo lang('chat_remove') ?>
                            </a>
                                
                            
                        </div>
                    </div>
                    <!-- Message: dropdown -->
                </div>
                <!-- Message: content -->



            </div>
        </div>
        <!-- Message: row -->

    </div>
    <!-- Message: body -->
</div>
<!-- Message -->








