<?php
$message = $cms->getPinnedMessageContent($pinnedItems[$i]['message_id']);
$imagePath = $cms->getImageBatch($message['batch_id']); 
$filePath = $cms->getFileBatch($message['batch_id']); 
?>

                                                            <li class="list-pinned-item">
                                                                <div class="pin-item<?php echo $message['sender_id'] != $user->id ? ' member' : ''; ?>">
                                                                    <ul class="chat-details-images-list">
                                                                        <?php foreach($imagePath as $image){ ?>
                                                                        <!-- Image -->
                                                                        <li class="list-images-item">
                                                                            <a href="#" data-action="zoom" data-href="<?php echo imgCrop($image['contents']); ?>" class="images-media-link">
                                                                                <img class="img-fluid rounded" src="<?php echo imgCrop($image['contents'], 80, 80); ?>">
                                                                            </a>
                                                                        </li>
                                                                        <!-- Image -->
                                                                        <?php } ?>
                                                                    </ul>
                                                                    
                                                                    <?php if($message['type'] == 'fileMix'){ ?>
                                                                    <div class="media">
                                                                        <?php foreach($filePath as $file){ ?>
                                                                        <a href="<?php echo url_for('/' . $file['contents']); ?>" class="icon-shape" download>
                                                                            <span class="non-icon-download"><?php echo $file['file_extension'] ?></span>
                                                                        </a>
                                                                        <div class="media-body overflow-hidden flex-fill">
                                                                            <a href="<?php echo url_for('/' . $file['contents']); ?>" class="d-block text-truncate font-medium text-reset" download><?php echo basename($file['contents']) ?></a>
                                                                            <ul class="list-inline small mb-0">
                                                                                <li class="list-inline-item">
                                                                                    <span class="t"><?php echo $cms->fileSize($file['size']) ?></span>
                                                                                </li>
                                                                                <li class="list-inline-item">
                                                                                    <span class="text-uppercase"><?php echo $file['file_extension'] ?></span>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <?php }else{ ?>
                                                                    <div class="pin-txt"><?php echo $message['contents']; ?></div>
                                                                    <?php } ?>
                                                                    
                                                                    <div class="mt-1">
                                                                        <small class="opacity-65"><?php echo timeAgo($pinnedItems[$i]['pinned_time']); ?></small>
                                                                    </div>
                                                                    <div class="dropdown dropdown-chat">
                                                                        <a class="message-row-dropdown-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                <circle cx="12" cy="12" r="1"></circle>
                                                                                <circle cx="19" cy="12" r="1"></circle>
                                                                                <circle cx="5" cy="12" r="1"></circle>
                                                                            </svg>
                                                                        </a>

                                                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(-84px, 26px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                            <a class="dropdown-item" onClick="unpinMessage(<?php echo $pinnedItems[$i]['id']; ?>, this)">
                                                                                <?php echo lang('chat_unpin') ?>
                                                                            </a>
                                                                            <a class="dropdown-item" onclick="goToMessage('#msg-<?php echo $message['id']; ?>')">
                                                                                <?php echo lang('chat_go_to_message') ?>
                                                                            </a>
                                                                            <a class="dropdown-item" onclick="copyToClipboard('#c<?php echo $message['id']; ?>')">
                                                                                <?php echo lang('chat_copy') ?>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
