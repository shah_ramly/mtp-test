<div class="container-xxl">
	<div>
        <?php
 		$previous_batch_id = "";
        $divider_day_displayed[] = array();

        foreach ($chats as $key) {
            $date = date_create($key['created_by']);
            $divider_day = date('Y-m-d', strtotime($key['created_by']));
            
            if(!in_array($divider_day, $divider_day_displayed)){
                $divider_day_displayed[] = $divider_day;
                $divider_date = $divider_day == date('Y-m-d') ? 'Today' : date('l, d F Y', strtotime($divider_day));

                $divider = '<div class="message-divider my-9 mx-lg-5">
                                <div class="row align-items-center">
                                <div class="col"><hr></div>
                                <div class="col-auto"><small class="text-muted">' . $divider_date . '</small></div>
                                <div class="col"><hr></div>
                                </div>
                            </div>';

                echo $divider;
            }
            
            if ($key['sender_id'] == $user->id) {
            	//Text UI
            	if($key['type'] == "text"){
                    include '_sender_text_template.php';
                    
 				}else if($key['type'] == "file"){ //attachment icon 
                    include '_sender_files_template.php';
                    
				 }else if($key['type'] == "image"){
  				 	if($key['batch_id']!=$previous_batch_id){
  				 		include '_sender_img_template.php';
  				 		$previous_batch_id = $key['batch_id'];
                    }
                       
				}else if(strpos($key['type'], "Mix") !== false){
                    if($key['batch_id']!=$previous_batch_id){
                        include '_sender_mix_template.php';
                        $previous_batch_id = $key['batch_id'];
                    }
                 }
            } else {
                if ($key['type'] == "text") {
                    include '_receiver_text_template.php';

                }else if ($key['type'] == "file") {
                    include '_receiver_files_template.php';

                }else if ($key['type'] == "image") {
                    if ($key['batch_id'] != $previous_batch_id) {
                        include '_receiver_img_template.php';
                        $previous_batch_id = $key['batch_id'];
                    }
                    
                }else if(strpos($key['type'], "Mix") !== false){
                    if($key['batch_id']!=$previous_batch_id){
                        include '_receiver_mix_template.php';
                        $previous_batch_id = $key['batch_id'];
                    }
                }
        	}
        }
        ?>
	</div>
</div>
<!-- Chat: Content -->

<script type="text/javascript">
$(document).ready(function(){
    $('#chat-1-info').load(rootPath + 'ajax/chat/sidebar/<?php echo $member['id']; ?>/<?php echo $task['id']; ?>/<?php echo $taskType ?>');
    $('#modal_chat').find('h6.chat-body-name').next().text('<?php echo $last_sent_msg; ?>');

    modal = $('#modal_chat');
    modal.find('.view-task-details').attr('href', '<?php echo url_for("/workspace/{$taskType}/{$task['slug']}/details"); ?>');
    //modal.find('.view-task-details').text("View <?php echo ucfirst($taskType) ?> Details");
    modal.find('.receiver-avatar img').attr('src', '<?php echo imgCrop($member['photo'], 80, 80, 'assets/img/default-avatar.png'); ?>');
    modal.find('h6.chat-body-name').text('<?php echo $member['type'] === '1' ? $member['company']['name'] : $member['name']; ?>');
    modal.find('.view-profile').attr('href', '<?php echo $member['type'] === '1' ? url_for(str_replace('talent', 'company', $member['url'])) : url_for($member['url']); ?>');
});
</script>


