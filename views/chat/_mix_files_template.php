<?php for($i =0 ; $i<count($filePath);$i++){?>
<a href="<?php echo url_for('/' . $filePath[$i]['contents']); ?>" class="message-reply-content-link" download>
    <div class="message-reply-content" style="border-left: 0px solid #007aff;" onClick="goToMessage('#msg-<?php echo $imagePath[$i]['id']; ?>')">
        <div class="message-reply-content-wrapper">
            <div class="media">
                <span class="icon-shape">
                    <span class="non-icon-download"><?php echo strtoupper($filePath[$i]['file_extension']);?></span>
                </span>
                <div class="media-body overflow-hidden flex-fill">
                    <span class="d-block text-truncate font-medium text-reset"><?php echo basename($filePath[$i]['contents']);?></span>
                    <ul class="list-inline small mb-0">
                        <li class="list-inline-item">
                            <span class="t"><?php echo bytesToSize($filePath[$i]['size']); ?></span>
                        </li>
                        <li class="list-inline-item">
                            <span class="text-uppercase">pdf</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</a>
<?php } ?>
