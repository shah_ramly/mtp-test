<div class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-chart">
				<div class="card-header">
					<div class="row">
						<div class="col-sm-12 col-header-filter">
							<h2 class="card-title-new"><?php echo lang('my_company_profile_main_title') ?></h2>
                            <?php echo partial('workspace/partial/header.html.php'); ?>
						</div>
					</div>
				</div>
                <div class="card-body-companyprofile">
				<ul class="nav nav-pills nav-pills-mycompanyprofile">
					<li class="nav-item">
						<a class="nav-link active"><?php echo lang('my_company_profile_main_title') ?></a>
					</li>
				</ul>
            <div class="row">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-xl-12 col-report-dashboard-top">
                            <div class="col-report-dashboard-top-wrapper">
                                <div class="row">
                                    <div class="col-xl-12 col-myprofile">
                                        <form id="redirectForm" class="form-companyprofile">
                                        <div class="form-collapse-row row-collapse-pic">
                                            <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('person_in_charge_section_title') ?></span></div>
                                            <div class="form-flex">
                                                <div class="input-group-flex row-cpfirstname">
                                                    <input type="text" id="firstName" class="form-control form-control-input"
                                                           placeholder="<?php echo lang('profile_first_name_placeholder') ?>" required="required" autofocus="" name="firstname" onkeypress="return blockSpecialChar(event)" value="<?php echo $user->info['firstname']; ?>" maxlength="80">
                                                </div>
                                                <div class="input-group-flex row-cplastname">
                                                    <input type="text" id="lastName" class="form-control form-control-input"
                                                           placeholder="<?php echo lang('profile_last_name_placeholder') ?>" required="required" name="lastname" onkeypress="return blockSpecialChar(event)" value="<?php echo $user->info['lastname']; ?>" maxlength="80">
                                                </div>
                                            </div>
                                            <div class="form-flex">
                                                <div class="input-group-flex row-cpid">
                                                    <input type="text" id="identification" class="form-control form-control-input"
                                                           placeholder="<?php echo lang('profile_mykad_placeholder') ?>" required="" name="nric" value="<?php echo $user->info['nric']; ?>">
                                                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo lang('profile_mykad_placeholder_tooltip') ?>" maxlength="30">
                                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div class="input-group-flex row-cpmobile">
                                                    <span class="tel-prefix">
                                                        <input type="tel" id="contactMobile" class="form-control form-control-input intl-tel-input"
                                                               name="mobile_number" placeholder="<?php echo lang('company_contact_mobile_number') ?>"
                                                               value="<?php echo $user->info['mobile_number']; ?>" required maxlength="20">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-flex">
                                                <div class="input-group-flex row-cpemail">
                                                    <input type="email" id="inputEmail" class="form-control form-control-input"
                                                           placeholder="<?php echo lang('profile_email_placeholder') ?>" name="email"
                                                           value="<?php echo $user->info['email']; ?>" readonly maxlength="100">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-collapse-row row-collapse-compdetails">
                                            <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('my_company_details_section_title') ?></span></div>
                                            <div class="form-flex">
                                                <div class="input-group-flex row-cpcompname">
                                                    <input type="text" id="companyName" class="form-control form-control-input"
                                                           placeholder="<?php echo lang('company_name') ?>" required="" name="company_name" value="<?php echo $user->info['company']['name']; ?>" maxlength="300">
                                                </div>
                                                <div class="input-group-flex row-cpcompreg">
                                                    <input type="text" id="companyReg" class="form-control form-control-input"
                                                           placeholder="<?php echo lang('company_registration') ?>" required="" name="reg_num" value="<?php echo $user->info['company']['reg_num']; ?>" maxlength="20">
                                                </div>
                                                <div class="input-group-flex row-cpcompaddress">
                                                    <input type="text" id="companyAddress" class="form-control form-control-input"
                                                           placeholder="<?php echo lang('address') ?>" required="" name="address1" value="<?php echo $user->info['address1']; ?>" maxlength="255">
                                                </div>
                                                <div class="input-group-flex row-cpcompaddress">
													<input type="text" id="companyAddress2" class="form-control form-control-input"
													       placeholder="<?php echo lang('address_2') ?>" required="" name="address2" value="<?php echo $user->info['address2']; ?>">
                                                </div>
                                                <div class="input-group-flex row-cpcomppostal">
                                                    <input type="text" id="companyPostal" class="form-control form-control-input"
                                                           placeholder="<?php echo lang('company_postcode') ?>" required="" name="zip" value="<?php echo $user->info['zip']; ?>" maxlength="10">
                                                </div>
	                                            <div class="input-group-flex row-cpcompcountry">
		                                            <select class="form-control form-control-input country"
		                                                    placeholder="<?php echo lang('profile_country_placeholder') ?>" name="country" data-target="state">
			                                            <option disabled selected><?php echo lang('profile_country_placeholder') ?></option>
                                                        <?php foreach ($countries as $key) { ?>
				                                            <option value="<?php echo $key['id']; ?>"<?php echo $user->info['country'] == $key['id'] ? " selected" : ""; ?>><?php echo $key['name']; ?></option>
                                                        <?php } ?>
		                                            </select>
	                                            </div>
                                                <div class="input-group-flex row-cpcomppostal">
	                                                <select class="form-control form-control-input state"
	                                                        placeholder="<?php echo lang('profile_state_placeholder') ?>" name="state" data-target="city" data-selected="<?php echo $user->info['state']; ?>" required>
		                                                <option disabled selected><?php echo lang('profile_state_placeholder') ?></option>
                                                        <?php foreach($cms->states($user->info['country']) as $state){ ?>
			                                                <option value="<?php echo $state['id']; ?>" <?php echo $user->info['state'] == $state['id'] ? "selected" : ""; ?>><?php echo $state['name']; ?></option>
                                                        <?php } ?>
	                                                </select>
                                                </div>
                                                <div class="input-group-flex row-cpcompstate">
	                                                <select class="form-control form-control-input city"  name="city"
	                                                        placeholder="<?php echo lang('profile_town_placeholder') ?>" data-selected="<?php echo $user->info['city']; ?>">
		                                                <option disabled selected><?php echo lang('profile_town_placeholder') ?></option>
                                                        <?php foreach($cms->cities($user->info['state']) as $city){ ?>
			                                                <option value="<?php echo $city['id']; ?>" <?php echo $user->info['city'] == $city['id'] ? "selected" : ""; ?>><?php echo $city['name']; ?></option>
                                                        <?php } ?>
	                                                </select>
                                                </div>
                                                <div class="input-group-flex row-cpcompemail">
                                                    <input type="email" id="companyEmail" class="form-control form-control-input"
                                                           placeholder="<?php echo lang('company_email') ?>" required="" autofocus="" name="company_email" value="<?php echo $user->info['company']['email']; ?>">
                                                </div>
                                                <div class="input-group-flex row-cpcompphone">
                                                    <span class="tel-prefix">
                                                        <input type="tel" id="companyPhone" class="form-control form-control-input intl-tel-input"
                                                               placeholder="<?php echo lang('company_contact_fixed_line') ?>" value="<?php echo $user->info['company']['company_phone']; ?>" required>
                                                    </span>
                                                </div>
                                                <div class="input-group-flex row-cpcompindustry">
                                                    <select class="form-control form-control-input" name="industry" placeholder="<?php echo lang('sign_up_company_industry') ?>">
                                                        <option disabled selected><?php echo lang('sign_up_company_industry') ?></option>
                                                        <?php foreach($cms->getIndustries2Level() as $industry){ if($industry['subs']){ ?>
                                                        <optgroup label="<?php echo $industry['title']; ?>">
                                                        <?php foreach($industry['subs'] as $sub){ ?>
                                                        <option value="<?php echo $sub['id']; ?>"<?php echo $sub['id'] == $user->info['company']['industry'] ? ' selected' : ''; ?>><?php echo $sub['title']; ?></option>
                                                        <?php } ?>
                                                        </optgroup>
                                                        <?php }} ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-collapse-row row-collapse-compstatement">
                                            <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('company_statement_section_title') ?></span></div>
                                            <div class="form-flex">
                                                <div class="input-group-flex row-cpcompabout">
                                                    <textarea class="form-control form-control-input" placeholder="<?php echo lang('about_the_company') ?>" rows="3" name="about_us" maxlength="500"><?php echo $user->info['company']['about_us']; ?> </textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-collapse-row row-collapse-compprofilephoto">
                                            <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('company_profile_photo_section_title_not_required') ?></span></div>
                                            <div class="form-flex">
                                                <div class="input-group-block row-cpcompavatar">
                                                    <div class="profile-photo">
                                                        <?php if($user->info['photo']){ ?><img src="<?php echo imgCrop($user->info['photo'], 100, 100); ?>" class="profile-img"><?php } ?>
                                                        <button type="button" class="btn btn-default btn-sm mt-1" onClick="$(this).next().click();"><span class="fa fa-upload"></span> <?php echo lang('profile_photo_upload') ?></button>
                                                        <input type="file" name="file" id="profileImage" class="d-none">
                                                        <input type="hidden" name="photo" value="<?php echo $user->info['photo']; ?>">
                                                    </div>
                                                    <p class="help-notes"><?php echo sprintf(lang('profile_photo_upload_limit'), $cms->settings()['avatar_max_size'] . 'MB', strtoupper(str_replace(',', ', ', $cms->settings()['avatar_file_extensions']))); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-collapse-row row-collapse-compprofileurl">
                                            <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('direct_access_to_company_microsite') ?></span></div>
                                            <div class="form-flex">
                                                <div class="input-group-flex row-compurl">
                                                    <span class="groupurl">
                                                        <label class="input-lbl input-lbl-block label-mtp-url">
                                                            <span class="input-label-txt">www.maketimepay.com/</span>
                                                        </label>
                                                        <input type="text" id="companyUrl" name="url" autocomplete="off"
                                                               value="<?php echo $user->info['company']['url'] ?>"
                                                               class="form-control form-control-input" placeholder="Your Preferred URL" />
                                                    </span>
                                                    <div class="edit-in-modal"><a id="check-availability" href="#"><?php echo lang('check_availability') ?></a></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-collapse-row row-collapse-compimagegallery">
                                            <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('company_image_gallery_section_title') ?></span></div>
                                            <div class="form-flex">
                                                <div class="input-group-block row-cpcompgallery">
                                                    <div class="uploads">
                                                        <ul class="image-list company_gallery">
                                                            <?php if($user->info['company']['photos']){ foreach($user->info['company']['photos'] as $photo){ ?>
                                                            <li><input type="hidden" name="photos[]" value="<?php echo $photo; ?>"><img src="<?php echo imgCrop($photo, 125, 100); ?>"><a href="#" class="remove-image"><i class="fa fa-times"></i></a></li>
                                                            <?php }} ?>
                                                        </ul>
                                                        <button type="button" class="btn btn-default btn-sm mt-1" onClick="$(this).next().click();"><span class="fa fa-upload"></span> <?php echo lang('profile_photo_upload') ?></button>
                                                        <input type="file" name="file" id="profile-company_gallery" class="d-none" multiple>
                                                    </div>
                                                    <p class="help-notes"><?php echo sprintf(lang('profile_photo_upload_limit'), $cms->settings()['avatar_max_size'] . 'MB', strtoupper(str_replace(',', ', ', $cms->settings()['avatar_file_extensions']))); ?></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-collapse-row row-collapse-bank-info">
                                            <div class="collapse-title-lbl">
	                                            <span class="title-collapse"><?php echo lang('pref_bank'); ?></span>
	                                            <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title=""
	                                                  data-original-title="<?php echo lang('pref_bank_tooltips'); ?>">
                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                                                    </svg>
                                                </span>
                                            </div>
                                            <div class="form-flex">
                                                <div class="input-group-block row-bankacc">
	                                                <select class="form-control form-control-input" name="preference[bank]">
		                                                <option disabled selected><?php echo lang('pref_select_bank') ?></option>
		                                                <option value="PHBMMYKL" <?php echo $user->info['preference']['bank'] == "PHBMMYKL" ? ' selected' : ''; ?>>Affin Bank</option>
		                                                <option value="MFBBMYKL" <?php echo $user->info['preference']['bank'] == "MFBBMYKL" ? " selected" : ''; ?>>Alliance Bank</option>
		                                                <option value="ARBKMYKL" <?php echo $user->info['preference']['bank'] == "ARBKMYKL" ? " selected" : ''; ?>>AmBank</option>
		                                                <option value="BIMBMYKL" <?php echo $user->info['preference']['bank'] == "BIMBMYKL" ? " selected" : ''; ?>>Bank Islam</option>
		                                                <option value="BKRMMYKL" <?php echo $user->info['preference']['bank'] == "BKRMMYKL" ? " selected" : ''; ?>>Bank Kerjasama Rakyat Malaysia</option>
		                                                <option value="BMMBMYKL" <?php echo $user->info['preference']['bank'] == "BMMBMYKL" ? " selected" : ''; ?>>Bank Muamalat</option>
		                                                <option value="BSNAMYK1" <?php echo $user->info['preference']['bank'] == "BSNAMYK1" ? " selected" : ''; ?>>Bank Simpanan Nasional</option>
		                                                <option value="CIBBMYKL" <?php echo $user->info['preference']['bank'] == "CIBBMYKL" ? " selected" : ''; ?>>CIMB Bank</option>
		                                                <option value="HLBBMYKL" <?php echo $user->info['preference']['bank'] == "HLBBMYKL" ? " selected" : ''; ?>>Hong Leong Bank</option>
		                                                <option value="HBMBMYKL" <?php echo $user->info['preference']['bank'] == "HBMBMYKL" ? " selected" : ''; ?>>HSBC</option>
		                                                <option value="KFHOMYKL" <?php echo $user->info['preference']['bank'] == "KFHOMYKL" ? " selected" : ''; ?>>Kuwait Finance House Bank</option>
		                                                <option value="MBBEMYKL" <?php echo $user->info['preference']['bank'] == "MBBEMYKL" ? " selected" : ''; ?>>Maybank</option>
		                                                <option value="OCBCMYKL" <?php echo $user->info['preference']['bank'] == "OCBCMYKL" ? " selected" : ''; ?>>OCBC Bank</option>
		                                                <option value="PBBEMYKL" <?php echo $user->info['preference']['bank'] == "PBBEMYKL" ? " selected" : ''; ?>>Public Bank</option>
		                                                <option value="RHBBMYKL" <?php echo $user->info['preference']['bank'] == "RHBBMYKL" ? " selected" : ''; ?>>RHB Bank</option>
		                                                <option value="SCBLMYKX" <?php echo $user->info['preference']['bank'] == "SCBLMYKX" ? " selected" : ''; ?>>Standard Chartered</option>
		                                                <option value="UOVBMYK" <?php echo $user->info['preference']['bank'] == "UOVBMYK" ? " selected" : ''; ?>>UOB</option>
	                                                </select>
                                                </div>
                                            </div>
                                            <div class="form-flex" id="work_location">
                                                <div class="input-group-flex row-accholdername">
                                                    <input type="text" class="form-control form-control-input"
                                                           placeholder="<?php echo lang('pref_account_name') ?>" name="preference[acc_name]" value="<?php echo  $user->info['preference']['acc_name']; ?>" required>
                                                </div>
                                                <div class="input-group-flex row-accno">
                                                    <input type="text" class="form-control form-control-input"
                                                           placeholder="<?php echo lang('pref_account_num') ?>" name="preference[acc_num]" value="<?php echo  $user->info['preference']['acc_num']; ?>" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-flex" id="myPreferredLang">
                                            <label class="input-lbl">
                                                <span class="input-label-txt"><?php echo lang('pref_lang_for_site') ?></span>
                                            </label>
                                            <div class="form-flex">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input" id="langEng" name="preference[language]" value="1" <?php echo $user->info['preference']['language'] === '1' ? 'checked' : '' ?>>
                                                    <label class="custom-control-label" for="langEng">English</label>
                                                </div>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input" id="langBM" name="preference[language]" value="2" <?php echo $user->info['preference']['language'] === '2' ? 'checked' : '' ?>>
                                                    <label class="custom-control-label" for="langBM">BM</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="footer-form-action">
                                            <button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                                                <span class="btn-label"><?php echo lang('pref_save') ?></span>
                                                <span class="btn-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                        <polyline points="20 6 9 17 4 12"></polyline>
                                                    </svg>
                                                </span>
                                            </button>
                                            <button type="button" class="btn-icon-full btn-cancel" onClick="location.href='<?php echo url_for('/dashboard'); ?>'">
                                                <span class="btn-label"><?php echo lang('pref_cancel') ?></span>
                                                <span class="btn-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                                    </svg>
                                                </span>
                                            </button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
		</div>
	</div>
</div>

<?php content_for('scripts'); ?>
<script>
function blockSpecialChar(event) {
	var regex = new RegExp("^[-@/ a-zA-Z]");
	var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
	if (!regex.test(key)) {
		event.preventDefault();
		return false;
	}
}
	
$(function(){
    $('.card-title').text('My Company Profile');

    $(document).on('click', '.remove-image', function(){
        el = $(this);
        el.parent().slideUp(function(){
            el.parent().remove();
        });
    });

    $('#companyUrl').on('keydown', function(e){

        if(e.keyCode === 13){
            e.preventDefault();
            checkAvailability(e);
        }

    }).on('keyup', function(e){

        var input = $(e.currentTarget);
        $(e.currentTarget).removeClass('border-success border-danger');

        var forbiddenChars = new RegExp("[^a-zA-Z\-]", 'g');
        if (forbiddenChars.test(input.val())) {
            input.val(input.val().replace(forbiddenChars, ''));
        }

    });

    $('#check-availability').on('click', function(e){
        e.preventDefault();
        checkAvailability(e);
    });

	function checkAvailability(e) {
	    var check = $('#check-availability').text();
	    $('#check-availability').html('<div class="spinner-border spinner-border-sm" role="status">'+
            '<span class="sr-only">Loading...</span>'+
        '</div>');
	    e.preventDefault();
	    $.ajax({
	        url: '<?php echo option('site_uri') . url_for('checkAvailability') ?>',
	        method: 'POST',
	        data: {'company_name': $('#companyUrl').val()}
	    }).done(function (response) {
	        $('#check-availability').html(check);
	        if (response.available) {
	            $('.groupurl').removeClass('url-not-available');
	            $('.groupurl').addClass('url-available');
                $('#companyUrl').addClass('border-success');
	            //link.before('<span class="glyphicon glyphicon-ok-sign text-success"></span>' )
	        } else {
	            $('.groupurl').removeClass('url-available');
	            $('.groupurl').addClass('url-not-available');
	            $('#companyUrl').addClass('border-danger');
	            //link.before('<span class="glyphicon glyphicon-remove-sign text-danger"></span>' )
	        }
	    });
	}
});
</script>

    <!-- Mobile Phone Number - IntlTelInput -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/intlTelInput.min.js"></script>
    <script>
        
        var inputMobile = document.querySelector("#contactMobile");
        window.intlTelInput(inputMobile, {
           autoHideDialCode: false,
           autoPlaceholder: "off",
           initialCountry: "my",
           preferredCountries: ['my'],
           separateDialCode: true,
           utilsScript: rootPath + "assets/js/plugins/utils.js",
           hiddenInput: "mobile_number",
        });

        var inputCompany = document.querySelector("#companyPhone");
        window.intlTelInput(inputCompany, {
           autoHideDialCode: false,
           autoPlaceholder: "off",
           initialCountry: "my",
           preferredCountries: ['my'],
           separateDialCode: true,
           utilsScript: rootPath + "assets/js/plugins/utils.js",
           hiddenInput: "company_phone",
        });
    </script>
    <!-- Mobile Phone Number - IntTelInput -->

<?php end_content_for(); ?>