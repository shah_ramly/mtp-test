<!DOCTYPE html>
<html lang="en">

<body class="white-content od-mode">
<div class="card-body-chats">
    <div class="row">
        <div class="col-xl-12">
            <div class="row">
                <div class="chat-sidebar">

                    <div class="tab-content h-100" role="tablist">
                        <div class="tab-pane fade h-100 show active" id="tab-content-dialogs" role="tabpanel">
                            <div class="d-flex flex-column h-100">

                                <div class="hide-scrollbar">
                                    <div class="container-fluid py-6">
                                        <div class="new-conversation-flex">
                                            <div class="new-conversation-container">
                                                <a href="#" class="new-conversation-link">
                                                    <span class="plus-conv-icon"><svg viewBox="0 0 16 16" class="bi bi-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                                            <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                                        </svg></span>
                                                    <span class="new-conversation-lbl">New Conversation</span></a>
                                            </div>
                                            <a href="#" class="dropdown">
                                                <span class="three-dots-icon">
                                                    <svg class="bi bi-three-dots" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z" />
                                                    </svg>
                                                </span>
                                            </a>
                                        </div>
                                        <!-- Search -->
                                        <form class="mb-6">
                                            <div class="input-group">
                                                <input type="text" class="form-control form-control-lg" placeholder="Search here" aria-label="Search here">
                                                <div class="input-group-append">
                                                    <button class="btn btn-lg btn-ico btn-secondary btn-minimal"  type="submit">
                                                        <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- Search -->

                                        <!-- Chats -->
                                        <nav class="nav d-block list-discussions-js mb-n6">

                                            <?php foreach ($memberDetails as $key) {  ?>
                                                 
                                            <a class="text-reset nav-link p-0 mb-6 " href="#">
                                                <div class="chat-card" id="<?php echo $key['id'] ?>">
                                                    <div class="chat-card-body">

                                                        <div class="media">


                                                            <div class="avatar avatar-away mr-5">
                                                                <img class="avatar-img" src="/mtp-dev/assets/img/james.jpg" alt="">
                                                            </div>

                                                            <div class="media-body overflow-hidden">
                                                                <div class="d-flex align-items-center mb-1">
                                                                    <h6 class="text-truncate chat-sidebar-name mb-0 mr-auto"><?php echo $key['lastname']." ".$key['firstname'] ?></h6>
                                                                </div>
                                                                <div class="chat-sidebar-lastseen text-truncate">32 mins ago</div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </a>
                                            <!-- Chat link -->
                                             
                                             <?php } ?>
                                           
                                           
                                         

                                        </nav>
                                        <!-- Chats -->

                                    </div>
                                </div>

                            </div>
                        </div>




                    </div>

                </div>
                
                 <div id="chat-right" class="chat-main "></div>

                


                <!-- Body -->
            </div>
        </div>
        <!--- Chat Details --->

    </div>
</div>
                                   
    
    <!-- Modal - OD Post Task Steps -->
    <div id="post_task_od" class="modal modal-steps fade" aria-labelledby="post_task_od" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target="#modal_confirm_od" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="modal-form-wrapper">
                        <form name="form_post_task_od" id="form_post_task_od" method="post" action="">

                            <div id="sf1" class="frm modal-step-flex">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 1 of 5: Select Category</div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Category" id="od_category" name="od_category">
                                                <option disabled selected>Category</option>
                                                <option value="1">Option 1</option>
                                                <option value="2">Option 2</option>
                                                <option value="3">Option 3</option>
                                                <option value="4">Option 4</option>
                                                <option value="5">Option 5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Sub Category" id="od_subcategory" name="od_subcategory">
                                                <option disabled selected>Sub Category</option>
                                                <option value="1">Option 1</option>
                                                <option value="2">Option 2</option>
                                                <option value="3">Option 3</option>
                                                <option value="4">Option 4</option>
                                                <option value="5">Option 5</option>
                                            </select>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-next od-open1">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div id="sf2" class="frm modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 2 of 5: Task Details</div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-input" placeholder="Task Title" id="od_tasktitle" name="od_tasktitle">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control form-control-input" placeholder="Task Description" id="od_taskdesc" name="od_taskdesc" rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <div class="dropzone dropzone-previews" id="my-awesome-dropzone"></div>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev od-back2">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next od-open2">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div id="sf3" class="frm modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left">
                                    <div id="mapTaskLoc" style="display:none;"></div>
                                </div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 3 of 5: Task Location</div>
                                        <div class="form-group">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Task Location</span>
                                            </label>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="tl_remote" name="tlRadioSelect">
                                                <label class="custom-control-label" for="tl_remote">Can be done remotely</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="tl_travel" name="tlRadioSelect">
                                                <label class="custom-control-label" for="tl_travel">Requires travelling</label>
                                            </div>
                                        </div>
                                        <div class="form-check-selection" id="form_travelrequired" style="display:none;">
                                            <input id="loc" type="text" class="form-control form-control-input" placeholder="Enter your address or drag the pin on map" />
                                        </div>
                                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev od-back3">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next od-open3">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div id="sf4" class="frm modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 4 of 5: Task Estimates</div>
                                        <div class="form-group">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Nature of Task</span>
                                            </label>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="not_bythehour" name="defaultExampleRadios">
                                                <label class="custom-control-label" for="not_bythehour">By the hour</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="not_lumpsum" name="defaultExampleRadios">
                                                <label class="custom-control-label" for="not_lumpsum">Lump sum, complete by deadline</label>
                                            </div>
                                        </div>
                                        <div class="form-check-selection" id="form_byhour" style="display: none;">
                                            <div class="form-group form-estimated-hours">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Estimated hours</span>
                                                </label>
                                                <div class="form-flex">
                                                    <input type="number" name="" class="form-control form-control-input" min="0" id="" />
                                                    <span class="postlbl-hours">hours</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-estimated-hours">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Budget Per Hour</span>
                                                </label>
                                                <div class="form-flex">
                                                    <span class="estimated-lbl estimated-from">RM50</span>
                                                    <input id="range_budget_hour" type="text" />
                                                    <span class="estimated-lbl estimated-to">RM100</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-total-project-budget">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Total Project Budget</span>
                                                </label>
                                                <div class="form-flex">
                                                    <span class="postlbl-cur">RM150 - RM300</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-complete-by">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Start date</span>
                                                </label>
                                                <div class="form-group">
                                                    <div class="input-group input-group-datetimepicker date" id="form_datetime_hour" data-target-input="nearest">
                                                        <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_hour" />
                                                        <span class="input-group-addon" data-target="#form_datetime_hour" data-toggle="datetimepicker">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-complete-by-hour">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Completed by</span>
                                                </label>
                                                <div class="form-group">
                                                    <div class="custom-control custom-radio radio-complete-day">
                                                        <input type="radio" class="custom-control-input" id="not_bythehour_day" name="byTheHourDayRadio" checked="checked">
                                                        <label class="custom-control-label" for="not_bythehour_day"></label>
                                                        <div class="form-flex">
                                                            <input type="number" name="" class="form-control form-control-input" min="0" id="" />
                                                            <span class="postlbl-hours">days</span>
                                                        </div>
                                                    </div>
                                                    <div class="custom-control custom-radio radio-complete-date">
                                                        <input type="radio" class="custom-control-input" id="not_bythehour_date" name="byTheHourDayRadio">
                                                        <label class="custom-control-label" for="not_bythehour_date"></label>
                                                        <div class="input-group input-group-datetimepicker date" id="form_datetime_hour_completeby" data-target-input="nearest">
                                                            <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_hour_completeby" readonly="readonly" />
                                                            <span class="input-group-addon" data-target="#form_datetime_hour_completeby" data-toggle="datetimepicker">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="task-filter-row task-filter-row-location">
                                                    <div class="form-block-task-filter">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="urgent_hour">
                                                            <label class="custom-control-label" for="urgent_hour">Mark this task as urgent <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="Within 5-day between posted date and start date ">
                                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                        <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                                    </svg>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-check-selection" id="form_bylumpsum" style="display: none;">
                                            <div class="form-group form-estimated-hours">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Budget For Task</span>
                                                </label>
                                                <div class="form-flex">
                                                    <span class="estimated-lbl estimated-from">RM200</span>
                                                    <input id="range_budget_lumpsum" type="text" />
                                                    <span class="estimated-lbl estimated-to">RM300</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-complete-by">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Start Date</span>
                                                </label>
                                                <div class="form-group">
                                                    <div class="input-group input-group-datetimepicker date" id="form_datetime_lumpsum" data-target-input="nearest">
                                                        <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_lumpsum" />
                                                        <span class="input-group-addon" data-target="#form_datetime_lumpsum" data-toggle="datetimepicker">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-complete-by-lumpsum">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Completed by</span>
                                                </label>
                                                <div class="form-group">
                                                    <div class="custom-control custom-radio radio-complete-day">
                                                        <input type="radio" class="custom-control-input" id="not_bylumpsum_day" name="byLumpsumDayRadio" checked="checked">
                                                        <label class="custom-control-label" for="not_bylumpsum_day"></label>
                                                        <div class="form-flex">
                                                            <input type="number" name="" class="form-control form-control-input" min="0" id="" />
                                                            <span class="postlbl-hours">days</span>
                                                        </div>
                                                    </div>
                                                    <div class="custom-control custom-radio radio-complete-date">
                                                        <input type="radio" class="custom-control-input" id="not_bylumpsum_date" name="byLumpsumDayRadio">
                                                        <label class="custom-control-label" for="not_bylumpsum_date"></label>
                                                        <div class="input-group input-group-datetimepicker date" id="form_datetime_lumpsum_completeby" data-target-input="nearest">
                                                            <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_lumpsum_completeby" readonly="readonly" />
                                                            <span class="input-group-addon" data-target="#form_datetime_lumpsum_completeby" data-toggle="datetimepicker">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="task-filter-row task-filter-row-location">
                                                    <div class="form-block-task-filter">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="urgent_lumpsum">
                                                            <label class="custom-control-label" for="urgent_lumpsum">Mark this task as urgent <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="Within 5-day between posted date and start date ">
                                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                        <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                                    </svg>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev od-back4">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next od-open4">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div id="sf5" class="frm modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 5 of 5: Seeker Shortlisting</div>
                                        <div class="form-group form-important-skills">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Important skills for task</span>
                                            </label>
                                            <div class="bs-example">
                                                <input type="text" name="tags" value="Tag1,Tag2,Tag3" data-role="tagsinput" />
                                            </div>
                                        </div>
                                        <div class="form-group form-qna">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Questions</span>
                                            </label>
                                            <div class="od-qna-field-wrapper">
                                                <div class="qna-wrapper od-qna-wrapper ">
                                                    <div class="form-flex">
                                                        <span class="qna-lbl od-qna-lbl">1.</span>
                                                        <input type="text" name="od_qna_field[]" class="form-control form-control-input" placeholder="Question" />
                                                    </div>
                                                </div>
                                                <a href="javascript:void(0);" class="od-add-button" title="Add field">Add More</a>
                                            </div>
                                        </div>
                                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev od-back5">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="submit" class="btn-icon-full btn-step-submit">
                                            <span class="btn-label">Submit</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <!-- Modal - FT Post Job Steps -->
    <div id="post_job_ft" class="modal modal-steps fade" aria-labelledby="post_job_ft" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target="#modal_confirm_ft" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="modal-form-wrapper">
                        <form name="form_post_job_ft" id="form_post_job_ft" method="post" action="">

                            <div id="ft_sf1" class="frm-ft modal-step-flex">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 1 of 5: Select Category</div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Category" id="ft_category" name="ft_category">
                                                <option disabled selected>Category</option>
                                                <option value="1">Option 1</option>
                                                <option value="2">Option 2</option>
                                                <option value="3">Option 3</option>
                                                <option value="4">Option 4</option>
                                                <option value="5">Option 5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Sub Category" id="ft_subcategory" name="ft_subcategory">
                                                <option disabled selected>Sub Category</option>
                                                <option value="1">Option 1</option>
                                                <option value="2">Option 2</option>
                                                <option value="3">Option 3</option>
                                                <option value="4">Option 4</option>
                                                <option value="5">Option 5</option>
                                            </select>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-next ft-open1">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>

                            </div>

                            <div id="ft_sf2" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 2 of 5: The Job</div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-input" placeholder="Job Title" id="ft_jobtitle" name="ft_jobtitle">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control form-control-input" placeholder="Job Description" id="ft_jobdesc" name="ft_jobdesc" rows="3"></textarea>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back2">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next ft-open2">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div id="ft_sf3" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 3 of 5: The Applicant</div>

                                        <div class="form-group">
                                            <textarea class="form-control form-control-input" placeholder="The Requirements" id="ft_jobrequirement" name="ft_jobrequirement" rows="3"></textarea>
                                        </div>

                                        <div class="form-group form-qna">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Questions</span>
                                            </label>
                                            <div class="ft-qna-field-wrapper">
                                                <div class="qna-wrapper ft-qna-wrapper ">
                                                    <div class="form-flex">
                                                        <span class="qna-lbl ft-qna-lbl">1.</span>
                                                        <input type="text" name="ft_qna_field[]" class="form-control form-control-input" placeholder="Question" />
                                                    </div>

                                                </div>
                                                <a href="javascript:void(0);" class="ft-add-button" title="Add field">Add More</a>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Experience Level" id="ft_jobxp" name="ft_jobxp">
                                                <option disabled selected>Experience Level</option>
                                                <option value="1">Option 1</option>
                                                <option value="2">Option 2</option>
                                                <option value="3">Option 3</option>
                                                <option value="4">Option 4</option>
                                                <option value="5">Option 5</option>
                                            </select>
                                        </div>



                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back3">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next ft-open3">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div id="ft_sf4" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 4 of 5: The Location</div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Country" id="ft_jobcountry" name="ft_jobcountry">
                                                <option disabled selected>Country</option>
                                                <option value="1">Malaysia</option>
                                                <option value="2">Singapore</option>
                                                <option value="3">Thailand</option>
                                                <option value="4">Option 4</option>
                                                <option value="5">Option 5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="State" id="ft_jobstate" name="ft_jobstate">
                                                <option disabled selected>State</option>
                                                <option value="Johor">Johor</option>
                                                <option value="Kedah">Kedah</option>
                                                <option value="Kelantan">Kelantan</option>
                                                <option value="Kuala Lumpur">Kuala Lumpur</option>
                                                <option value="Labuan">Labuan</option>
                                                <option value="Malacca">Melaka</option>
                                                <option value="Negeri Sembilan">Negeri Sembilan</option>
                                                <option value="Pahang">Pahang</option>
                                                <option value="Perak">Perak</option>
                                                <option value="Perlis">Perlis</option>
                                                <option value="Penang">Penang</option>
                                                <option value="Sabah">Sabah</option>
                                                <option value="Sarawak">Sarawak</option>
                                                <option value="Selangor">Selangor</option>
                                                <option value="Terengganu">Terengganu</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-input" placeholder="Job Location" id="ft_joblocation" name="ft_joblocation">
                                        </div>



                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back4">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next ft-open4">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>



                            <div id="ft_sf5" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>

                                        <div class="modal-steps-title">Step 5 of 5: Remuneration</div>
                                        <div class="form-group form-salary-range">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Salary Range</span>
                                            </label>
                                            <div class="form-flex">
                                                <span class="estimated-lbl estimated-from">RM1200</span>
                                                <input id="range_budget_salary" type="text" />
                                                <span class="estimated-lbl estimated-to">RM12000</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-benefits">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Benefits</span>
                                            </label>
                                            <div class="form-block">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_1">
                                                    <label class="custom-control-label" for="benefits_1">Medical</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_2">
                                                    <label class="custom-control-label" for="benefits_2">Parking</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_3">
                                                    <label class="custom-control-label" for="benefits_3">Claimable expenses</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_4">
                                                    <label class="custom-control-label" for="benefits_4">Dental</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_5">
                                                    <label class="custom-control-label" for="benefits_5">Meals</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_6">
                                                    <label class="custom-control-label" for="benefits_6">Pantry</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_7">
                                                    <label class="custom-control-label" for="benefits_7">Commission</label>
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back5">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="submit" class="btn-icon-full btn-step-submit">
                                            <span class="btn-label">Submit</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <!-- Modal - OD Confirm Discard -->
    <div id="modal_confirm_od" class="modal modal-confirm fade" aria-labelledby="modal_confirm" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p class="modal-para">This will discard all the detail you have entered.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_task_od">
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                        <span class="btn-label">Confirm</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <!-- Modal - FT Confirm Discard -->
    <div id="modal_confirm_ft" class="modal modal-confirm fade" aria-labelledby="modal_confirm" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p class="modal-para">This will discard all the detail you have entered.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_job_ft">
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                        <span class="btn-label">Confirm</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->


<script type="text/javascript">
    var url = window.location.pathname;

    //this will attach the class to every target 
    $(".chat-card").click(function(){
        $(".chat-card").removeClass("card-active-listener");
        $(this).addClass("card-active-listener");
        $(this).attr("id");

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                if(this.responseText!= ''){
                    document.getElementById("chat-right").innerHTML = this.responseText;
                    var element = document.getElementById("chats_content");
                    element.scrollTop = element.scrollHeight;
                }
            }
        };
        xhttp.open('GET',  rootPath + 'ajax/chat/content/' +  $(this).attr('id'), true);
        xhttp.send();
    });

    setInterval(function(){ 
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                if(this.responseText!= ''){
                    document.getElementById("chat-right").innerHTML = this.responseText;
                    var element = document.getElementById("chats_content");
                    element.scrollTop = element.scrollHeight;
                }
            }
        };
        xhttp.open("GET",  rootPath + "ajax/chat/count/" + document.getElementById('chat-id-1-input').getAttribute('data-value'), true);
        xhttp.send();
    }, 1000);

    function send(){
        var element = document.getElementById("chats_content");
        var content = $("#chat-id-1-input").val();
        var val = document.getElementById('chat-id-1-input').getAttribute('data-value');
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

            }
        };
        taskID = '';
        xhttp.open("GET", rootPath + "ajax/chat/send/" + val + "/" + taskID + "?content=" + content, true);
        xhttp.send();
        $('#chat-id-1-input').val('');  
        document.getElementById('chat-id-1-input').focus();
        element.scrollTop = element.scrollHeight;
        $('#closeReply').click();
    }

    function additonalInfoStyle(){
        var infoSection = document.getElementById("chat-1-info");
    
        if(window.getComputedStyle(infoSection).visibility == "hidden"){
            infoSection.style.width = "550px";
            infoSection.style.visibility = "visible";
        }else{
            infoSection.style.width = "0px";
            infoSection.style.visibility = "hidden";
        }
    }
</script>
   
</body>
</html>
