<!DOCTYPE html>
<html lang="en">


<body class="white-content od-mode">
<div class="card-body-dashboard card-body-my-cv">
	<ul class="nav nav-pills nav-pills-mycv">
		<li class="nav-item">
			<a class="nav-link active">My CV</a>
		</li>
	</ul>
    <div class="row">
        <div class="col-xl-12">
            <div class="row">
                <div class="col-xl-12 col-report-dashboard-top">
                    <div class="col-report-dashboard-top-wrapper">
                        <div class="row">
                            <div class="col-xl-12 col-myprofile">
                                 <form class="form-individualprofile" id="redirectForm">
                                    <div class="form-collapse-row row-collapse-uploads">
                                        <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('cv_uploads'); ?></span></div>
                                        <div class="form-flex">
                                            <div class="input-group-block row">
                                                <label class="input-lbl input-lbl-block">
                                                    <span class="input-label-txt"><?php echo lang('cv_resume'); ?></span>
                                                </label>
                                                <div class="uploads">
                                                    <ul class="my-docs resume">
                                                        <?php if(!empty($docs['resume'])){ foreach($docs['resume'] as $i => $resume){ ?>
                                                        <?php
                                                            $src = explode('/', $resume);
                                                            $filename = end($src);
                                                            $original = explode('.', $filename);
                                                            $ext = array_pop($original);
                                                            $name = strlen($filename) < 25 ? $filename : substr($original[0], 0, 25) . '...';
                                                        ?>
                                                        <li>
                                                            <a href="<?php echo url_for($resume); ?>" target="_blank" data-title="<?php echo $filename; ?>"><?php echo ($i+1); ?></a>
                                                            <input type="hidden" name="docs[resume][]" value="<?php echo $resume; ?>">
                                                            <a href="#" class="remove-docs" data-toggle="tooltip" title="<?php echo lang('cv_remove'); ?>"><i class="far fa-trash-alt"></i></a>
                                                        </li>
                                                        <?php }} ?>
                                                    </ul>
                                                    <button type="button" class="btn btn-default btn-sm mt-1" onClick="$(this).next().click();"><span class="fa fa-upload"></span> <?php echo lang('cv_upload_button'); ?></button>
                                                    <input type="file" name="file" id="profile-resume" class="d-none">
                                                    <p class="help-notes"><?php echo sprintf(lang('cv_upload_format'), strtoupper($cms->settings()['docs_file_extensions'])); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-flex mt-4">
                                            <div class="input-group-block row">
                                                <label class="input-lbl input-lbl-block">
                                                    <span class="input-label-txt"><?php echo lang('cv_cover_letter'); ?></span>
                                                </label>
                                                <div class="uploads">
                                                    <ul class="my-docs cover_letter">
                                                        <?php if(!empty($docs['cover_letter'])){ foreach($docs['cover_letter'] as $i => $cover_letter){ ?>
                                                        <?php
                                                            $src = explode('/', $cover_letter);
                                                            $filename = end($src);
                                                            $original = explode('.', $filename);
                                                            $ext = array_pop($original);
                                                            $name = strlen($filename) < 25 ? $filename : substr($original[0], 0, 25) . '...';
                                                        ?>
                                                        <li>
                                                            <a href="<?php echo url_for($cover_letter); ?>" target="_blank" data-title="<?php echo $filename; ?>"><?php echo ($i+1); ?></a>
                                                            <input type="hidden" name="docs[cover_letter][]" value="<?php echo $cover_letter; ?>">
                                                            <a href="#" class="remove-docs" data-toggle="tooltip" title="<?php echo lang('cv_remove'); ?>"><i class="far fa-trash-alt"></i></a>
                                                        </li>
                                                        <?php }} ?>
                                                    </ul>
                                                    <button type="button" class="btn btn-default btn-sm mt-1" onClick="$(this).next().click();"><span class="fa fa-upload"></span> <?php echo lang('cv_upload_button'); ?></button>
                                                    <input type="file" name="file" id="profile-cover_letter" class="d-none">
                                                    <p class="help-notes"><?php echo sprintf(lang('cv_upload_format'), strtoupper($cms->settings()['docs_file_extensions'])); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-flex mt-4">
                                            <div class="input-group-block row">
                                                <label class="input-lbl input-lbl-block">
                                                    <span class="input-label-txt"><?php echo lang('cv_awards'); ?></span>
                                                </label>
                                                <div class="uploads">
                                                    <ul class="my-docs cert">
                                                        <?php if(!empty($docs['cert'])){ foreach($docs['cert'] as $i => $cert){ ?>
                                                        <?php
                                                            $src = explode('/', $cert);
                                                            $filename = end($src);
                                                            $original = explode('.', $filename);
                                                            $ext = array_pop($original);
                                                            $name = strlen($filename) < 25 ? $filename : substr($original[0], 0, 25) . '...';
                                                        ?>
                                                        <li>
                                                            <a href="<?php echo url_for($cert); ?>" target="_blank" data-title="<?php echo $filename; ?>"><?php echo ($i+1); ?></a>
                                                            <input type="hidden" name="docs[cert][]" value="<?php echo $cert; ?>">
                                                            <a href="#" class="remove-docs" data-toggle="tooltip" title="Remove"><i class="far fa-trash-alt"></i></a>
                                                        </li>
                                                        <?php }} ?>
                                                    </ul>
                                                    <button type="button" class="btn btn-default btn-sm mt-1" onClick="$(this).next().click();"><span class="fa fa-upload"></span> <?php echo lang('cv_upload_button'); ?></button>
                                                    <input type="file" name="file" id="profile-cert" class="d-none">
                                                    <p class="help-notes"><?php echo sprintf(lang('cv_upload_format'), strtoupper($cms->settings()['docs_file_extensions'])); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-collapse-row row-collapse-employment">
                                        <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('cv_employment'); ?></span></div>

                                        <!-- Empty data field -->
                                        <!--<div class="row-form-empty">No employment to show. Add your employment now with the Add New button below.</div>-->
                                        <!-- Empty data field -->
                                        <div class="row-form-data-container employment-container">
                                            <?php 
                                    		foreach($employment_details as $employment){ ?>
                                            <div class="row-form-data" id="emp-<?php echo $employment['id']; ?>">
                                                <div class="talent-details-workxp-row">
                                                    <div class="talent-details-workxp-compname"><?php echo $employment['company_name']; ?></div>
                                                    <div class="talent-details-workxp-pos"><?php echo $employment['position']; ?>, <?php echo $cms->getIndustryName($employment['industry']); ?></div>
                                                    <div class="talent-details-workxp-pos-duration">
                                                        <div class="talent-details-workxp-location"><?php echo $employment['state'] ? $cms->getStateName($employment['state']) . ', '  : ''; ?><?php echo $cms->getCountryName($employment['country']); ?></div>
                                                        <div class="talent-details-workxp-duration"><?php echo dateRangeToDays($employment['date_start'], $employment['date_end']); ?></div>
                                                    </div>

                                                    <div class="talent-details-workxp-resp">
                                                        <span class="talent-details-workxp-list-lbl"><b><?php echo lang('cv_description'); ?></b></span>
                                                        <div>
                                                            <?php echo nl2br($employment['job_desc']); ?>
                                                        </div>
                                                    </div>
                                                    <div class="talent-details-workxp-resp">
                                                        <span class="talent-details-workxp-list-lbl"><b><?php echo lang('cv_responsibilities'); ?></b></span>
                                                        <div>
                                                            <?php echo nl2br($employment['job_responsibilities']); ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                        $employment['date_start'] = date('d/m/Y', strtotime($employment['date_start']));
                                                        $employment['date_end'] = date('d/m/Y', strtotime($employment['date_end']));
                                                    ?>
                                                    <div class="edit-in-modal">
                                                        <a href="#" class="edit-employment" data-data="<?php echo htmlspecialchars(json_encode($employment)); ?>"><?php echo lang('cv_edit'); ?></a>
                                                        <a href="#" class="remove-employment" data-id="<?php echo $employment['id']; ?>"><i class="far fa-trash-alt"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <div class="add-new-action add-new-education-action">
                                            <button type="button" class="btn-icon-full btn-add add-employment" data-toggle="modal" data-target="#modal_add_employment">
                                                <span class="btn-label"><?php echo lang('cv_add_new'); ?></span>
                                                <span class="btn-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                        <line x1="12" y1="5" x2="12" y2="19"></line>
                                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                                    </svg>
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-collapse-row row-collapse-education">
                                        <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('cv_education'); ?></span></div>
                                        <!-- Empty data field -->
                                        <!--<div class="row-form-empty">No education to show. Add your education now with the Add New button below.</div>-->
                                        <!-- Empty data field -->
                                        <div class="row-form-data-container education-container">
                                            <?php foreach($educations as $education){ ?>
                                            <div class="row-form-data" id="edu-<?php echo $education['id']; ?>">
                                                <div class="talent-details-edu-row">
                                                    <div class="talent-details-edu-insname"><?php echo $education['edu_institution']; ?></div>
                                                    <div class="talent-details-edu-level-fieldstudy-grade">
                                                        <div class="talent-details-edu-level"><?php echo $education['highest_edu_level']; ?></div>
                                                        <div class="talent-details-edu-fieldstudy"><?php echo $education['field']; ?></div>
                                                        <div class="talent-details-edu-fieldstudy"><?php echo $education['major']; ?></div>
                                                        <div class="talent-details-edu-grade"><?php echo $education['grade']; ?></div>
                                                    </div>
                                                    <div class="talent-details-edu-location-year">
                                                        <div class="talent-details-edu-location"><?php echo $education['state'] ? $cms->getStateName($education['state']) . ', '  : ''; ?><?php echo $cms->getCountryName($education['country']); ?></div>
                                                        <div class="talent-details-edu-year"><?php echo $education['grad_year']; ?></div>
                                                    </div>
                                                    <div class="edit-in-modal">
                                                        <a href="#" class="edit-education" data-data="<?php echo htmlspecialchars(json_encode($education)); ?>"><?php echo lang('cv_edit'); ?></a>
                                                        <a href="#" class="remove-education" data-id="<?php echo $education['id']; ?>"><i class="far fa-trash-alt"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <div class="add-new-action add-new-education-action">
                                            <button type="button" class="btn-icon-full btn-add add-education" data-toggle="modal" data-target="#modal_add_education">
                                                <span class="btn-label"><?php echo lang('cv_add_new'); ?></span>
                                                <span class="btn-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                        <line x1="12" y1="5" x2="12" y2="19"></line>
                                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                                    </svg>
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-collapse-row row-collapse-skills" id="skill">
                                        <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('cv_skills'); ?></span></div>
                                        <div class="form-flex">
                                            <div class="input-group-flex row-indskills">
                                                <div class="bs-example">
                                                    <input type="text" name="member[skills]" class="bootstrap-tagsinput skills" value="<?php echo $member['skills'] ?> " />
                                                    <p class="help-notes"><?php echo lang('cv_skills_important'); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-collapse-row row-collapse-skills row-collapse-interests" id="interests">
                                        <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('cv_interests'); ?></span></div>
                                        <div class="form-flex">
                                            <div class="input-group-flex row-indskills">
                                                <div class="bs-example interest">
                                                    <input type="text" name="member[interests]" class="bootstrap-tagsinput interests" value="<?php echo $user->info['interests'] ?> " />
                                                    <p class="help-notes"><?php echo lang('cv_interests_important'); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-collapse-row row-collapse-languages" id="language">
                                        <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('cv_language'); ?></span></div>
                                        <div class="form-flex">
                                            <div class="input-group-flex row-indlang">
                                                <div class="bs-example">
                                                    <input type="text" class="language-input bootstrap-tagsinput" name="member[language]" value="<?php echo $member['language'] ?>" />
                                                    <p class="help-notes"><?php echo lang('cv_language_desc'); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer-form-action">
                                        <button type="submit" class="btn-icon-full btn-confirm"  data-orientation="next" >
                                            <span class="btn-label"><?php echo lang('profile_save'); ?></span>
                                            <span class="btn-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                    <polyline points="20 6 9 17 4 12"></polyline>
                                                </svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-cancel" onClick="location.href='<?php echo url_for('/dashboard'); ?>'">
                                            <span class="btn-label"><?php echo lang('profile_cancel'); ?></span>
                                            <span class="btn-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <!-- Modal - Add Employment -->
    <div id="modal_add_employment" class="modal modal-add-employment modal-w-footer fade" aria-labelledby="modal_add_employment" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form class="form-individualprofile" id="addEmployment">
            <div class="modal-content">
                <div class="modal-header"><?php echo lang('add_employment_title') ?></div>
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="modal-body-container">
                            <input type="hidden" name="employment[id]">
                            <div class="form-flex">
                                <div class="input-group-flex row-indcompname">
                                    <input type="text" id="companyName" class="form-control form-control-input" placeholder="<?php echo lang('employment_company_name') ?>" name="employment[company_name]" required="" maxlength="200">
                                </div>
                                <div class="input-group-flex row-indrole">
                                    <input type="text" id="positionRole" class="form-control form-control-input" placeholder="<?php echo lang('employment_position_role') ?>" name="employment[position]" required="" maxlength="100">
                                </div>
                                <div class="input-group-flex row-indworkstart">
                                    <div class="form-group">
                                        <div class="input-group input-group-datetimepicker date_start" id="form_dateworkstart" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input" data-target="#form_dateworkstart" placeholder="<?php echo lang('employment_start_date') ?>" name="employment[date_start]">
                                            <span class="input-group-addon" data-target="#form_dateworkstart" data-toggle="datetimepicker">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                        <div class="input-group input-group-datetimepicker date_end" id="form_dateworkend" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input" data-target="#form_dateworkend" placeholder="<?php echo lang('employment_end_date') ?>" name="employment[date_end]">
                                            <span class="input-group-addon" data-target="#form_dateworkend" data-toggle="datetimepicker" id="workend">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group-flex row-indworkend">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" id="currently_working" class="custom-control-input" name="employment[currently_working]" value="1">
                                        <label class="custom-control-label" for="currently_working"><?php echo lang('employment_currently_working_on_this_role') ?></label>
                                    </div>
                                </div>
                                <div class="input-group-flex row-inddept">
                                    <input type="text" id="department" class="form-control form-control-input" placeholder="<?php echo lang('employment_department') ?>" name="employment[department]" required="" maxlength="200">
                                </div>
                                <div class="input-group-flex row-indindustry">
                                    <select class="form-control form-control-input" placeholder="Industry" name="employment[industry]">
                                        <option disabled selected><?php echo lang('employment_industry') ?></option>
                                        <?php foreach($cms->getIndustries2Level() as $industry){ if($industry['subs']){ ?>
                                        <optgroup label="<?php echo $industry['title']; ?>">
                                        <?php foreach($industry['subs'] as $sub){ ?>
                                        <option value="<?php echo $sub['id']; ?>"><?php echo $sub['title']; ?></option>
                                        <?php } ?>
                                        </optgroup>
                                        <?php }} ?>
                                    </select>
                                </div>
                                <div class="input-group-flex row-indjd">
                                    <textarea class="form-control form-control-input summernote" placeholder="<?php echo lang('employment_job_description') ?>" id="od_jobd" rows="3" name="employment[job_desc]" maxlength="500" ></textarea>
                                </div>
                                <div class="input-group-flex row-indrespon">
                                    <textarea class="form-control form-control-input summernote" placeholder="<?php echo lang('employment_responsibilities') ?>" id="od_jobr" rows="3" name="employment[job_responsibilities]" maxlength="500"></textarea>
                                </div>
                                <div class="input-group-flex row-indremunerate">
                                    <input type="text" id="remuneration" class="form-control form-control-input" placeholder="<?php echo lang('employment_remuneration_last_drawn') ?>" name="employment[remuneration]" required="" maxlength="6">
                                </div>
                                <div class="input-group-flex row-indlocation">
                                    <select class="form-control form-control-input" placeholder="<?php echo lang('employment_location') ?>" name="employment[state]" data-selected="<?php echo $user->info['state']; ?>" required>
                                        <option disabled selected><?php echo lang('employment_state') ?></option>
                                        <?php foreach($cms->states($user->info['country']) as $key){ ?>
                                        <option value="<?php echo $key['id']; ?>"<?php echo $key['id'] == $user->info['state'] ? ' selected' : '' ?>><?php echo $key['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="input-group-flex row-indlocation">
                                    <select class="form-control form-control-input country" placeholder="<?php echo lang('employment_country') ?>" name="employment[country]" data-target="employment[state]" data-selected="<?php echo $user->info['country']; ?>" required>
                                        <option disabled selected><?php echo lang('employment_country') ?></option>
                                        <?php foreach($countries as $key){ ?>
                                        <option value="<?php echo $key['id']; ?>"<?php echo $key['id'] == $user->info['country'] ? ' selected' : '' ?>><?php echo $key['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
                        <span class="btn-label"><?php echo lang('cancel') ?></span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <button type="submit" class="btn-icon-full btn-confirm">
                        <span class="btn-label"><?php echo lang('submit') ?></span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- /.modal -->

    <!-- Modal - Add Education -->
    <div id="modal_add_education" class="modal modal-add-education modal-w-footer fade" aria-labelledby="modal_add_education" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-individualprofile" id="addEducation">
                <div class="modal-header"><?php echo lang('add_education_title') ?></div>
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="modal-body-container">
                            <input type="hidden" name="education[id]">
                            <div class="form-flex">
                                <div class="input-group-flex row-indeduins">
                                    <input type="text" id="eduIn" class="form-control form-control-input" placeholder="<?php echo lang('education_institution') ?>" name="education[edu_institution]" required="" maxlength="200">
                                </div>
                                <div class="input-group-flex row-indedulevel">
                                    <select class="form-control form-control-input" placeholder="<?php echo lang('highest_education_level') ?>" name="education[highest_edu_level]">
                                        <option disabled selected><?php echo lang('highest_education_level') ?></option>
                                        <option value="Elementary"><?php echo lang('education_elementary') ?></option>
                                        <option value="Secondary"><?php echo lang('education_secondary') ?></option>
                                        <option value="Certification"><?php echo lang('education_certification') ?></option>
                                        <option value="Diploma"><?php echo lang('education_diploma') ?></option>
                                        <option value="Degree"><?php echo lang('education_degree') ?></option>
                                        <option value="Master"><?php echo lang('education_master') ?></option>
                                        <option value="PhD"><?php echo lang('education_phd') ?></option>
                                    </select>
                                </div>
                                <div class="input-group-flex row-indfieldstudy">
                                    <select class="form-control form-control-input" placeholder="<?php echo lang('education_field_of_study') ?>" name="education[field]">
                                        <option disabled selected><?php echo lang('education_field_of_study') ?></option>
                                        <!--<option value="Business"><?php /*echo lang('education_study_field_business') */?></option>
                                        <option value="Computer Science"><?php /*echo lang('education_study_field_computer_science') */?></option>
                                        <option value="Social Work"><?php /*echo lang('education_study_field_social_work') */?></option>
                                        <option value="Engineering"><?php /*echo lang('education_study_field_engineering') */?></option>-->
	                                    <option value="Advertising/Media">Advertising/Media</option>
	                                    <option value="Agriculture/Aquaculture/Forestry">Agriculture/Aquaculture/Forestry</option>
	                                    <option value="Airline Operation/Airport Management">Airline Operation/Airport Management</option>
	                                    <option value="Architecture">Architecture</option>
	                                    <option value="Art/Design/Creative Multimedia">Art/Design/Creative Multimedia</option>
	                                    <option value="Biology">Biology</option>
	                                    <option value="BioTechnology">BioTechnology</option>
	                                    <option value="Business Studies/Administration/Management">Business Studies/Administration/Management</option>
	                                    <option value="Chemistry">Chemistry</option>
	                                    <option value="Commerce">Commerce</option>
	                                    <option value="Computer Science/Information Technology">Computer Science/Information Technology</option>
	                                    <option value="Dentistry">Dentistry</option>
	                                    <option value="Economics">Economics</option>
	                                    <option value="Education/Teaching/Training">Education/Teaching/Training</option>
	                                    <option value="Engineering (Aviation/Aeronautics/Astronautics)">Engineering (Aviation/Aeronautics/Astronautics)</option>
	                                    <option value="Engineering (Bioengineering/Biomedical)">Engineering (Bioengineering/Biomedical)</option>
	                                    <option value="Engineering (Chemical)">Engineering (Chemical)</option>
	                                    <option value="Engineering (Civil)">Engineering (Civil)</option>
	                                    <option value="Engineering (Computer/Telecommunication)">Engineering (Computer/Telecommunication)</option>
	                                    <option value="Engineering (Electrical/Electronic)">Engineering (Electrical/Electronic)</option>
	                                    <option value="Engineering (Environmental/Health/Safety)">Engineering (Environmental/Health/Safety)</option>
	                                    <option value="Engineering (Industrial)">Engineering (Industrial)</option>
	                                    <option value="Engineering (Marine)">Engineering (Marine)</option>
	                                    <option value="Engineering (Material Science)">Engineering (Material Science)</option>
	                                    <option value="Engineering (Mechanical)">Engineering (Mechanical)</option>
	                                    <option value="Engineering (Mechatronic/Electromechanical)">Engineering (Mechatronic/Electromechanical)</option>
	                                    <option value="Engineering (Metal Fabrication/Tool/Die/Welding)">Engineering (Metal Fabrication/Tool/Die/Welding)</option>
	                                    <option value="Engineering (Mining/Mineral)">Engineering (Mining/Mineral)</option>
	                                    <option value="Engineering (Others)">Engineering (Others)</option>
	                                    <option value="Engineering (Petroleum/Oil/Gas)">Engineering (Petroleum/Oil/Gas)</option>
	                                    <option value="Finance/Accountancy/Banking">Finance/Accountancy/Banking</option>
	                                    <option value="Food and Beverage Services Management">Food and Beverage Services Management</option>
	                                    <option value="Food Technology/Nutrition/Dietetics">Food Technology/Nutrition/Dietetics</option>
	                                    <option value="Geographical Science">Geographical Science</option>
	                                    <option value="Geology/Geophysics">Geology/Geophysics</option>
	                                    <option value="History">History</option>
	                                    <option value="Hospitality/Tourism/Hotel Management">Hospitality/Tourism/Hotel Management</option>
	                                    <option value="Human Resource Management">Human Resource Management</option>
	                                    <option value="Humanities/Liberal Arts">Humanities/Liberal Arts</option>
	                                    <option value="Journalism">Journalism</option>
	                                    <option value="Law">Law</option>
	                                    <option value="Library Management">Library Management</option>
	                                    <option value="Linguistics/Languages">Linguistics/Languages</option>
	                                    <option value="Logistic/Transportation">Logistic/Transportation</option>
	                                    <option value="Maritime Studies">Maritime Studies</option>
	                                    <option value="Marketing">Marketing</option>
	                                    <option value="Mass Communications">Mass Communications</option>
	                                    <option value="Mathematics">Mathematics</option>
	                                    <option value="Medical Science">Medical Science</option>
	                                    <option value="Medicine">Medicine</option>
	                                    <option value="Music/Performing Arts Studies">Music/Performing Arts Studies</option>
	                                    <option value="Nursing">Nursing</option>
	                                    <option value="Optometry">Optometry</option>
	                                    <option value="Personal Services">Personal Services</option>
	                                    <option value="Pharmacy/Pharmacology">Pharmacy/Pharmacology</option>
	                                    <option value="Philosophy">Philosophy</option>
	                                    <option value="Physical Therapy/Physiotherapy">Physical Therapy/Physiotherapy</option>
	                                    <option value="Physics">Physics</option>
	                                    <option value="Political Science">Political Science</option>
	                                    <option value="Property Development/Real Estate Management">Property Development/Real Estate Management</option>
	                                    <option value="Protective Services and Management">Protective Services and Management</option>
	                                    <option value="Psychology">Psychology</option>
	                                    <option value="Quantity Survey">Quantity Survey</option>
	                                    <option value="Science and Technology">Science and Technology</option>
	                                    <option value="Secretarial">Secretarial</option>
	                                    <option value="Social Science/Sociology">Social Science/Sociology</option>
	                                    <option value="Sports Science and Management">Sports Science and Management</option>
	                                    <option value="Textile/Fashion Design">Textile/Fashion Design</option>
	                                    <option value="Urban Studies/Town Planning">Urban Studies/Town Planning</option>
	                                    <option value="Veterinary">Veterinary</option>
	                                    <option value="Others">Others</option>
                                    </select>
                                </div>
                                <div class="input-group-flex row-indgradeyear">
                                    <div class="input-group input-group-datetimepicker date" id="grad_year" data-target-input="nearest" data-date-format="YYYY">
                                        <input type="text" class="form-control datetimepicker-input" placeholder="<?php echo lang('education_graduation_year') ?>" name="education[grad_year]">
                                        <span class="input-group-addon" data-target="#grad_year" data-toggle="datetimepicker">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                    <?php /*
                                    <select class="form-control form-control-input" placeholder="Graduation Year" name="education[grad_year]">
                                        <option disabled selected>Graduation Year</option>
                                        <option value="2020">2020</option>
                                        <option value="2019">2019</option>
                                        <option value="2018">2018</option>
                                        <option value="2017">2017</option>
                                        <option value="2016">2016</option>
                                        <option value="2015">2015</option>
                                    </select>
                                    */ ?>
                                </div>
                                <div class="input-group-flex row-indmajor">
                                    <input type="text" id="major" class="form-control form-control-input" placeholder="<?php echo lang('education_major') ?>" required="" name="education[major]" maxlength="200">
                                </div>
                                <div class="input-group-flex row-indgrade">
                                    <input type="text" id="grade" class="form-control form-control-input" placeholder="<?php echo lang('education_grade') ?>" required="" name="education[grade]" maxlength="200">
                                </div>
                                <div class="input-group-flex row-indlocation">
                                    <select class="form-control form-control-input" placeholder="<?php echo lang('education_location') ?>" name="education[state]" data-selected="<?php echo $user->info['state']; ?>" required>
                                        <option disabled selected><?php echo lang('education_state') ?></option>
                                        <?php foreach($cms->states($user->info['country']) as $key){ ?>
                                        <option value="<?php echo $key['id']; ?>"<?php echo $key['id'] == $user->info['state'] ? ' selected' : '' ?>><?php echo $key['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="input-group-flex row-indlocation">
                                    <select class="form-control form-control-input country" placeholder="<?php echo lang('education_location') ?>" name="education[country]" data-target="education[state]" data-selected="<?php echo $user->info['country']; ?>" required>
                                        <option disabled selected><?php echo lang('education_country') ?></option>
                                        <?php foreach($countries as $key){ ?>
                                        <option value="<?php echo $key['id']; ?>"<?php echo $key['id'] == $user->info['country'] ? ' selected' : '' ?>><?php echo $key['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
                        <span class="btn-label"><?php echo lang('cancel') ?></span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <button type="submit" class="btn-icon-full btn-confirm">
                        <span class="btn-label"><?php echo lang('submit') ?></span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal -->
</body>
    <!-- Mobile Phone Number - IntlTelInput -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/intlTelInput.min.js"></script>
    <script>
        var inputHome = document.querySelector("#contactPhone");
        if(inputHome) {
            window.intlTelInput(inputHome, {
                autoHideDialCode: false,
                autoPlaceholder: "off",
                initialCountry: "my",
                preferredCountries: ['my'],
                separateDialCode: true,
                utilsScript: rootPath + "assets/js/plugins/utils.js",
                hiddenInput: "member[contact_number]",
            });
        }
        
        var inputMobile = document.querySelector("#contactMobile");
        if(inputMobile) {
            window.intlTelInput(inputMobile, {
                autoHideDialCode: false,
                autoPlaceholder: "off",
                initialCountry: "my",
                preferredCountries: ['my'],
                separateDialCode: true,
                utilsScript: rootPath + "assets/js/plugins/utils.js",
                hiddenInput: "member[mobile_number]",
            });
        }
        
        var inputEmergency = document.querySelector("#emergencyContact");
        if(inputEmergency) {
            window.intlTelInput(inputEmergency, {
                autoHideDialCode: false,
                autoPlaceholder: "off",
                initialCountry: "my",
                preferredCountries: ['my'],
                separateDialCode: true,
                utilsScript: rootPath + "assets/js/plugins/utils.js",
                hiddenInput: "member[emergency_contact_number]",
            });
        }
    </script>
    <!-- Mobile Phone Number - IntTelInput -->

    <script>
    function blockSpecialChar(event) {
        var regex = new RegExp("^[-@/ a-zA-Z]");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    }
        
    $(function(){
        $('[data-title]').tooltip();
        $('.card-title').text('My Profile');

        if( $('.date_start').length ){
            $('.date_start').datetimepicker({
                format: 'DD/MM/YYYY',
                maxDate: '<?php echo date('Y-m-d', strtotime('-1 day')); ?>',
            });
        }

        if( $('.date_end').length ){
            $('.date_end').datetimepicker({
                format: 'DD/MM/YYYY',
            });
        }

        $(document).on('keypress', '.tt-input', function(e){
            if(e.which == 13) {
                e.preventDefault();
                e.stopPropagation();
            }
        });

        $('.language-input').tagsinput({
            freeInput: false,
            typeaheadjs: {
                source: function(q, sync) {
                    languages = <?php echo json_encode(languageList()); ?>;
                    results = Array();
                    $(languages).each(function(i, val){
                        if(val.toLowerCase().indexOf(q.toLowerCase()) != '-1'){
                            results.push(val);
                        }
                    });
                    sync(results);
                }
            }
        });

        $(document).on('change', '[name="employment[currently_working]"]', function(){
            if($(this).prop('checked')){
                $('[name="employment[date_end]"]').val('').prop('disabled', true);
            }else{
                $('[name="employment[date_end]"]').prop('disabled', false);
            }  
        });

        $(document).on('click', '.add-employment', function(){
            modal = $('#modal_add_employment');

            modal.find('.modal-header').text('<?php echo lang('add_employment_title') ?>');
            modal.modal('show');
        });

        $(document).on('click', '.edit-employment', function(){
            modal = $('#modal_add_employment');
            data = $(this).data('data');
            $.each(data, function(key, val){
                el = modal.find('[name="employment[' + key + ']"]');
                tag = el.prop('tagName');
                if(tag == 'INPUT'){
                    if(el.prop('type') == 'text' || el.prop('type') == 'hidden'){
                        el.val(val);
                    }else if(el.prop('type') == 'checkbox'){
                        el.val('1');
                        if(val == '1'){
                            el.prop('checked', true);
                        }else{
                            el.prop('checked', false);
                        }
                    }
                }else if(tag == 'TEXTAREA'){
                    el.val(val);
                }else if(tag == 'SELECT'){
                    if(key == 'state'){
                        el.data('selected', val);
                    }
                    el.val(val);
                }
            });

            modal.find('.modal-header').text('<?php echo lang('edit_employment_title') ?>');
            modal.modal('show');
        });

        $(document).on('click', '.remove-employment', function(){
            el = $(this);
            id = el.data('id');

            if(confirm('<?php echo lang('are_you_sure_you_want_to_delete_this_information') ?>')){
                $.ajax({
                    type: 'POST',
                    url: rootPath + 'my_profile/employment/delete',
                    dataType : 'json',
                    data: { id: id},
                    success: function(results){
                        if(results.error == true){
                            t('e', results.msg);
                        }else{
                            t('s', results.msg);
                            el.parent().parent().slideUp(function(){
                                el.parent().parent().remove();
                            });
                        }
                        return false;
                    },
                    error: function(error){
                        ajax_error(error);
                    },
                    complete: function(){}
                });
            }
        });
        
        $(document).on('submit', '#addEmployment', function(e){
            e.preventDefault();
            $("#footer-form-action").children().off('click');
            el = $(this);
            data = el.serializeArray();

            if(data[0].value){
                url = rootPath + 'my_profile/employment/edit'
            }else{
                url = rootPath + 'my_profile/employment/add';
            }

            $.ajax({
                type:'POST',
                url: url,
                dataType : 'json',
                data: data,
                success: function(results){
                    if(results.error == true){
                        t('e', results.msg);
                    }else{
                        $('#modal_add_employment').modal('hide');
                        t('s', results.msg);

                        if(data[0].value){
                            $('#emp-' + data[0].value).replaceWith(results.html);
                        }else{
                            $('.employment-container').append(results.html);
                        }
                    }
                    return false;
                },
                error: function(error){
                    ajax_error(error);
                },
                complete: function(){}
            });

            return false;
        });

        $(document).on('click', '.add-education', function(){
            modal = $('#modal_add_education');

            modal.find('.modal-header').text('<?php echo lang('add_education_title') ?>');
            modal.modal('show');
        });

        $(document).on('click', '.edit-education', function(){
            modal = $('#modal_add_education');
            data = $(this).data('data');
            $.each(data, function(key, val){
                el = modal.find('[name="education[' + key + ']"]');
                tag = el.prop('tagName');
                if(tag == 'INPUT'){
                    if(el.prop('type') == 'text' || el.prop('type') == 'hidden'){
                        el.val(val);
                    }else if(el.prop('type') == 'checkbox'){
                        el.val('1');
                        if(val == '1'){
                            el.prop('checked', true);
                        }else{
                            el.prop('checked', false);
                        }
                    }
                }else if(tag == 'TEXTAREA'){
                    el.val(val);
                }else if(tag == 'SELECT'){
                    if(key == 'state'){
                        el.data('selected', val);
                    }
                    el.val(val);
                }
            });

            modal.find('.modal-header').text('<?php echo lang('edit_education_title') ?>');
            modal.modal('show');
        });

        $(document).on('click', '.remove-education', function(){
            el = $(this);
            id = el.data('id');

            if(confirm('<?php echo lang('are_you_sure_you_want_to_delete_this_information') ?>')){
                $.ajax({
                    type: 'POST',
                    url: rootPath + 'my_profile/education/delete',
                    dataType : 'json',
                    data: { id: id},
                    success: function(results){
                        if(results.error == true){
                            t('e', results.msg);
                        }else{
                            t('s', results.msg);
                            el.parent().parent().slideUp(function(){
                                el.parent().parent().remove();
                            });
                        }
                        return false;
                    },
                    error: function(error){
                        ajax_error(error);
                    },
                    complete: function(){}
                });
            }
        });

        $(document).on('submit', '#addEducation', function(e){
            e.preventDefault();
            $("#footer-form-action").children().off('click');
            el = $(this);
            data = el.serializeArray();

            if(data[0].value){
                url = rootPath + 'my_profile/education/edit'
            }else{
                url = rootPath + 'my_profile/education/add';
            }

            $.ajax({
                type:'POST',
                url: url,
                dataType : 'json',
                data: data,
                success: function(results){
                    if(results.error == true){
                        t('e', results.msg);
                    }else{
                        $('#modal_add_education').modal('hide');
                        t('s', results.msg);

                        if(data[0].value){
                            $('#edu-' + data[0].value).replaceWith(results.html);
                        }else{
                            $('.education-container').append(results.html);
                        }
                    }
                    return false;
                },
                error: function(error){
                    ajax_error(error);
                },
                complete: function(){}
            });

            return false;
        });   
        
        $(document).on('change', '[name="member[nric]"]', function(){
            val = $(this).val();
            yb = <?php echo substr(date('Y'), 0, 2); ?>;
            yy = <?php echo substr(date('Y'), 2, 2); ?>;
            y = val.substr(0, 2);
            m = val.substr(2, 2);
            d = val.substr(4, 2);
            if(y && m <= 12 && d <= 31){
                if(y >= yy){
                    y = (yb-1) + y;
                }else{
                    y = yb + y;
                }
                $('[name="member[dob]"]').val(d + '/' + m + '/' + y);
            }
        });

        $('#modal_add_employment, #modal_add_education').on('show.bs.modal', function(){
            $(this).find('.country').change();
            $(this).find('[name="employment[currently_working]"]').change();
            $('.summernote').each(function(){
                val = $(this).val();
                placeholder = $(this).attr('placeholder');
                $(this).summernote({
                    tooltip: false,
                    placeholder: placeholder,
                    toolbar: [
                        ['style', ['bold', 'italic', 'underline']],
                        ['para', ['ul', 'ol']],
                    ],
                    height: 200,
                    width: '100%',
                });
                $(this).summernote('code', val);
            });
        });

        $('#modal_add_employment, #modal_add_education').on('hide.bs.modal', function(){
            $(this).find('[name]:not([type="checkbox"])').val('');
            $(document).find('select').each(function(){
                if($(this).data('selected')){
                    $(this).val($(this).data('selected'));
                }else{
                    $(this).val($(this).find('option:first').val());
                }
            });
        });

        var extra = document.location.pathname.slice('1').split('/').indexOf('staging') > -1 ? '/staging':'';
        if(extra === '') extra = document.location.pathname.slice('1').split('/').indexOf('translation') > -1 ? '/translation':'';

        var skills = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: window.location.origin + extra + '/ajax/skills.json?all=1'
        });
        skills.clearPrefetchCache();
        skills.initialize();

        var interests = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
                url: window.location.origin + extra + '/ajax/interests.json?all=1',
                filter: function(list){
                    return $.map(list, function(interest){
                        return interest
                    })
                }
            }
        });
        interests.clearPrefetchCache();
        interests.initialize();
        
        $('.skills').tagsinput({
            /*itemValue: 'id',
            itemText: 'name',*/
            itemValue: function(item){ return item.id !== undefined ? item.id : 'new:'+item },
            itemText: function(item){ return item.name || item },
            typeaheadjs: {
                name: 'skills',
                displayKey: 'name',
                source: skills.ttAdapter(),
                templates: {
                    empty: Handlebars.compile("<div class='tt-empty'>No skills found</div><span class='tt-suggestion-footer tt-selectable add-new-tag'>Add new '{{query}}'</span>")
                }
            },
            freeInput: true,
        });

        $('input.interests').tagsinput({
            itemValue: function(item){ return item.id !== undefined ? item.id : 'new:'+item },
            itemText: function(item){ return item.name || item },
            typeaheadjs: {
                name: 'interests',
                displayKey: 'name',
                source: interests.ttAdapter(),
                templates: {
                    empty: Handlebars.compile("<div class='tt-empty'>No interests found</div><span class='tt-suggestion-footer tt-selectable add-new-interest'>Add new '{{query}}'</span>")
                }
            },
            freeInput: true,
        });

        $('.bs-example.interest').bind('typeahead:render', function(e) {
            var input = e.currentTarget;
            var target = $(input).find('input[name="interest"]');
            if( target.length === 0 )
                target = $('input.interests');

            var tag_btn = $('.add-new-interest');
            if( tag_btn.length ){
                tag_btn.off('click');
                tag_btn.on('click', function(e){
                    const tag_text = $(e.currentTarget).text();
                    const regex = /\'([\w\d\s\&\.\|]+)\'$/mg;
                    let m;

                    m = regex.exec(tag_text);
                    if( m !== null && m[1] !== undefined && m[1].length >= 2 ) {
                        var tag = m[1].trim();
                        target.tagsinput('add', {id:'new:'+tag, name: tag});
                    }
                })
            }
        });

        $('.skills, .interests').on('beforeItemAdd', function(event) {
            if( typeof(event.item) === 'string' ){
                event.cancel = true;
                var target = $(this);
                target.tagsinput('add', {id:'new:'+event.item, name: event.item});
                //target.tagsinput('refresh');
            }
        });
        
        <?php if($selected_skills){ foreach($selected_skills as $skill){ ?>
        $('.skills').tagsinput('add', { 'id': '<?php echo $skill['id']; ?>', 'name': '<?php echo $skill['name']; ?>' });
        <?php }} ?>

        <?php if(isset($user->info['interests_list']) && !empty($user->info['interests_list'])){ foreach($user->info['interests_list'] as $interest){ ?>
        $('input.interests').tagsinput('add', { 'id': '<?php echo $interest['id']; ?>', 'name': '<?php echo $interest['name']; ?>' });
        <?php }} ?>
    });
    </script>
</html>
