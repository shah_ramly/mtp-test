<!DOCTYPE html>
<html lang="en">


<body class="white-content od-mode">
                           <div class="card-body-dashboard card-body-my-profile">
						   <ul class="nav nav-pills nav-pills-myprofile">
								<li class="nav-item">
									<a class="nav-link active">My Profile</a>
								</li>
							</ul>
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="row">
                                            <div class="col-xl-12 col-report-dashboard-top">
                                                <div class="col-report-dashboard-top-wrapper">
                                                    <div class="row">
                                                        <div class="col-xl-12 col-myprofile">
                                                             <form class="form-individualprofile" id="redirectForm">
                                                                <div class="form-collapse-row row-collapse-personal">
                                                                    <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('profile_personal'); ?></span></div>
                                                                    <div class="form-flex">
                                                                        <div class="input-group-flex row-indfirstname">
                                                                            <input type="text" name="member[firstname]" id="firstName" class="form-control form-control-input" value="<?php echo  $member['firstname']; ?>" placeholder="<?php echo lang('profile_first_name_placeholder'); ?>" required="" autofocus="" onkeypress="return blockSpecialChar(event)" maxlength="80" >
                                                                        </div>
                                                                        <div class="input-group-flex row-indlastname">
                                                                            <input type="text" name="member[lastname]" id="lastName" class="form-control form-control-input" value="<?php echo  $member['lastname']; ?>" placeholder="<?php echo lang('profile_last_name_placeholder'); ?>" required="" onkeypress="return blockSpecialChar(event)" maxlength="80" >
                                                                        </div>
                                                                        <div class="input-group-flex row-indid">
                                                                            <input type="text" value="<?php echo  $member['nric']; ?>"  name="member[nric]" id="identification" class="form-control form-control-input" placeholder="<?php echo lang('profile_mykad_placeholder'); ?>" >
	                                                                        <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo lang('profile_mykad_placeholder_tooltip'); ?>" maxlength="30">
                                                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                    <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                                                </svg>
                                                                            </span>
                                                                        </div>
                                                                        <div class="input-group-flex row-indgender">
                                                                            <select class="form-control form-control-input" name="member[gender]" placeholder="<?php echo lang('profile_gender'); ?>">
                                                                                <option disabled selected><?php echo lang('profile_gender'); ?></option>
                                                                                <option value="male" <?php echo ($member['gender']=="male") ?  "selected" : " " ;?>><?php echo lang('profile_gender_male'); ?></option>
                                                                                <option value="female" <?php echo ($member['gender']=="female") ?  "selected" : " " ;?>><?php echo lang('profile_gender_female'); ?></option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="input-group-flex row-indbirthdate">
                                                                            <div class="form-group">
                                                                                <div id="form_datebirth" class="input-group input-group-datetimepicker date" data-target-input="nearest">
                                                                                    <input type="text" value="<?php echo $member['dob'] ? date('d/m/Y', strtotime($member['dob'])) : ''; ?>" name="member[dob]" class="form-control datetimepicker-input" placeholder="<?php echo lang('profile_dob_placeholder'); ?>" data-tooltip title="<?php echo lang('profile_dob_title'); ?>" />
                                                                                    <span class="input-group-addon" data-target="#form_datebirth" data-toggle="datetimepicker">
                                                                                        <span class="fa fa-calendar"></span>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="input-group-flex row-indaddress">
                                                                            <input type="text" value="<?php echo  $member['address1']; ?>" name="member[address1]" id="indAddress" class="form-control form-control-input" placeholder="<?php echo lang('profile_address_placeholder'); ?>" maxlength="255">
                                                                        </div>
                                                                        <div class="input-group-flex row-indZip">
                                                                            <input type="text" value="<?php echo  $member['zip']; ?>" name="member[zip]" id="indZip" class="form-control form-control-input" placeholder="<?php echo lang('profile_postcode_placeholder'); ?>" >
                                                                        </div>
                                                                        <div class="input-group-flex row-indcountry">
                                                                            <select class="form-control form-control-input country" data-target="member[state]" name="member[country]" placeholder="Country">
                                                                                <option disabled selected><?php echo lang('profile_country_placeholder'); ?></option>
                                                                                <?php foreach ($countries as $key ) { ?>
                                                                                    <option value="<?php echo $key['id']; ?>"<?php echo $user->info['country'] == $key['id'] ? " selected" : ""; ?>><?php echo $key['name']; ?></option>
                                                                               <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                        <div id="getState" class="input-group-flex row-indstate position-relative">
	                                                                        <span class="state-loading spinner-border spinner-border-sm position-absolute hide"
											                                      style="z-index:1;top:30%"></span>
                                                                            <select class="form-control form-control-input state" data-target="member[city]" name="member[state]" placeholder="State" data-selected="<?php echo $user->info['state']; ?>">
                                                                                <option disabled selected><?php echo lang('profile_state_placeholder'); ?></option>
                                                                                <?php if( !empty($cms->states($user->info['country'])) ): ?>
                                                                                <?php foreach($cms->states($user->info['country']) as $state){ ?>
                                                                                <option value="<?php echo $state['id']; ?>" <?php echo $user->info['state'] == $state['id'] ? "selected" : ""; ?>><?php echo $state['name']; ?></option>
                                                                                <?php } ?>
	                                                                            <?php else: ?>
	                                                                            <option value="0" selected><?php echo lang('profile_nationality_malaysian_others') ?></option>
                                                                                <?php endif ?>
                                                                            </select>
                                                                        </div>
                                                                        <div id="getCity" class="input-group-flex row-indcity position-relative">
	                                                                        <span class="city-loading spinner-border spinner-border-sm position-absolute hide"
											                                      style="z-index:1;right:1rem;top:30%"></span>
                                                                            <select class="form-control form-control-input city"  name="member[city]" placeholder="City" data-selected="<?php echo $member['city']; ?>">
                                                                                <option disabled selected><?php echo lang('profile_town_placeholder'); ?></option>
	                                                                            <?php if( !empty($cms->cities($user->info['state'])) ): ?>
                                                                                <?php foreach($cms->cities($user->info['state']) as $city){ ?>
                                                                                    <option value="<?php echo $city['id']; ?>" <?php echo $user->info['city'] == $city['id'] ? "selected" : ""; ?>><?php echo $city['name']; ?></option>
                                                                               <?php } ?>
	                                                                           <?php else: ?>
		                                                                            <option value="0" selected><?php echo lang('profile_nationality_malaysian_others') ?></option>
	                                                                           <?php endif ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="input-group-flex row-indnationality">
                                                                            <select name="member[nationality]" class="form-control form-control-input" placeholder="<?php echo lang('profile_nationality'); ?>">
                                                                                <option disabled selected><?php echo lang('profile_nationality'); ?></option>
                                                                                <?php $nationalities = explode(',', $cms->settings()['nationality']); ?>
                                                                                <?php if($nationalities){ foreach($nationalities as $nationality){ ?>
                                                                                <option value="malaysian"<?php echo $member['nationality'] == $nationality ? ' selected' : ''; ?>><?php echo $nationality; ?></option>
                                                                                <?php }} ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="input-group-flex">
                                                                            <span class="tel-prefix">
                                                                                <input type="tel" id="contactPhone" class="form-control form-control-input intl-tel-input" placeholder="<?php echo lang('profile_contact_placeholder'); ?>" value="<?php echo $user->info['contact_number']; ?>" maxlength="20">
                                                                            </span>
                                                                        </div>
                                                                        <div class="input-group-flex">
                                                                            <span class="tel-prefix">
                                                                                <input type="tel" id="contactMobile" class="form-control form-control-input intl-tel-input" placeholder="<?php echo lang('profile_mobile_placeholder'); ?>" value="<?php echo $user->info['mobile_number']; ?>" maxlength="20">
                                                                            </span>
                                                                        </div>
                                                                        <div class="input-group-flex">
                                                                            <input type="email" name="member[email]" value="<?php echo $member['email']; ?>" id="inputEmail" class="form-control form-control-input" placeholder="<?php echo lang('profile_email_placeholder'); ?>"  autofocus="" disabled>
                                                                        </div>
                                                                        <div class="form-flex">
                                                                            <div class="input-group-block row-indsocmed">
                                                                                <label class="input-lbl input-lbl-block">
                                                                                    <span class="input-label-txt"><?php echo lang('profile_preferred'); ?></span>
                                                                                </label>
                                                                                <div class="form-block-flex">
                                                                                    <div class="form-block">
	                                                                                    <div class="custom-control custom-checkbox">
		                                                                                    <input id="cm-call" type="checkbox" class="custom-control-input" name="member[contact_method][]" value="call"<?php echo in_array('call', $user->info['contact_method']) ? ' checked' : ''; ?>>
		                                                                                    <label class="custom-control-label" for="cm-call"><?php echo lang('profile_call'); ?></label>
	                                                                                    </div>
                                                                                        <div class="custom-control custom-checkbox">
                                                                                            <input id="cm-whatsapp" type="checkbox" class="custom-control-input" name="member[contact_method][]" value="whatsapp"<?php echo in_array('whatsapp', $user->info['contact_method']) ? ' checked' : ''; ?>>
                                                                                            <label class="custom-control-label" for="cm-whatsapp"><?php echo lang('profile_whatsapp'); ?></label>
                                                                                        </div>
                                                                                        <!--<div class="custom-control custom-checkbox">
                                                                                            <input id="cm-telegram" type="checkbox" class="custom-control-input" name="member[contact_method][]" value="telegram"<?php /*echo in_array('telegram', $user->info['contact_method']) ? ' checked' : ''; */?>>
                                                                                            <label class="custom-control-label" for="cm-telegram"><?php /*echo lang('profile_telegram'); */?></label>
                                                                                        </div>
                                                                                        <div class="custom-control custom-checkbox">
                                                                                            <input id="cm-wechat" type="checkbox" class="custom-control-input" name="member[contact_method][]" value="wechat"<?php /*echo in_array('wechat', $user->info['contact_method']) ? ' checked' : ''; */?>>
                                                                                            <label class="custom-control-label" for="cm-wechat"><?php /*echo lang('profile_wechat'); */?></label>
                                                                                        </div>-->
                                                                                        <div class="custom-control custom-checkbox">
                                                                                            <input id="cm-email" type="checkbox" class="custom-control-input" name="member[contact_method][]" value="email"<?php echo in_array('email', $user->info['contact_method']) ? ' checked' : ''; ?>>
                                                                                            <label class="custom-control-label" for="cm-email"><?php echo lang('profile_email'); ?></label>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-block">
                                                                                        <div class="para-notes"></div>
                                                                                    </div>
                                                                                </div> 
                                                                            </div>
                                                                        </div>                                                                        
                                                                    </div>
                                                                </div>
                                                                



                                                                <div class="form-collapse-row row-collapse-profilephoto">
                                                                    <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('profile_photo'); ?></span></div>
                                                                    <div class="form-flex">
                                                                        <div class="input-group-block row-indavatar">
                                                                            <div class="profile-photo">
                                                                                <?php if($user->info['photo']){ ?><img src="<?php echo imgCrop($user->info['photo'], 100, 100); ?>" class="profile-img"><?php } ?>
                                                                                <button type="button" class="btn btn-default btn-sm mt-1" onClick="$(this).next().click();"><span class="fa fa-upload"></span> <?php echo lang('profile_photo_upload'); ?></button>
                                                                                <input type="file" name="file" id="profileImage" class="d-none">
                                                                                <input type="hidden" name="member[photo]" value="<?php echo  $member['photo']; ?>">
                                                                                <p class="help-notes"><?php echo sprintf(lang('profile_photo_upload_limit'), $cms->settings()['avatar_max_size'] . 'MB', strtoupper(str_replace(',', ', ', $cms->settings()['avatar_file_extensions']))); ?>
</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-collapse-row row-collapse-personalstatement">
                                                                    <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('profile_statement'); ?></span></div>
                                                                    <div class="form-flex">
                                                                        <div class="input-group-flex row-indabout">
                                                                            <textarea class="form-control form-control-input" placeholder="<?php echo lang('profile_about_me'); ?>" rows="3" name="member[about]" id="member[about]"><?php echo  $member['about']; ?></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-collapse-row row-collapse-emergencycontact">
                                                                    <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('profile_emergency'); ?></span></div>
                                                                    <div class="form-flex">
                                                                        <div class="input-group-flex row-indemergencyname">
                                                                            <input type="text" name="member[emergency_contact_name]"  id="emergencyName" class="form-control form-control-input" value="<?php echo $member['emergency_contact_name'] ?>"placeholder="<?php echo lang('profile_emergency_placeholder'); ?>" onkeypress="return blockSpecialChar(event)" maxlength="255">
                                                                        </div>
                                                                        <div class="input-group-flex row-indemergencycontact">
                                                                            <span class="tel-prefix">
                                                                                <input type="tel" id="emergencyContact" class="form-control form-control-input intl-tel-input" placeholder=" <?php echo lang('profile_emergency_contact_placeohlder'); ?>" value="<?php echo $user->info['emergency_contact_number']; ?>" maxlength="20">
                                                                            </span>
                                                                        </div>
                                                                        <div class="input-group-flex row-indemergencyrel">
                                                                            <input type="text" id="emergencyRelationship" class="form-control form-control-input" placeholder="<?php echo lang('profile_emergency_relationship'); ?>" value="<?php echo $user->info['emergency_contact_number']; ?>"  name="member[emergency_contact_relation]" maxlength="255">
                                                                        </div>
                                                                    </div>
                                                                </div>




                                                                  <div class="footer-form-action">
                                                                    <button type="submit" class="btn-icon-full btn-confirm"  data-orientation="next" >
                                                                        <span class="btn-label"><?php echo lang('profile_save'); ?></span>
                                                                        <span class="btn-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                                <polyline points="20 6 9 17 4 12"></polyline>
                                                                            </svg>
                                                                        </span>
                                                                    </button>
                                                                    <button type="button" class="btn-icon-full btn-cancel" onClick="location.href='<?php echo url_for('/dashboard'); ?>'">
                                                                        <span class="btn-label"><?php echo lang('profile_cancel'); ?></span>
                                                                        <span class="btn-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                                                                <line x1="6" y1="6" x2="18" y2="18"></line>
                                                                            </svg>
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal - Add Employment -->
    <div id="modal_add_employment" class="modal modal-add-employment modal-w-footer fade" aria-labelledby="modal_add_employment" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <form class="form-individualprofile" id="addEmployment">
            <div class="modal-content">
                <div class="modal-header">Add Employment</div>
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="modal-body-container">
                            <input type="hidden" name="employment[id]">
                            <div class="form-flex">
                                <div class="input-group-flex row-indcompname">
                                    <input type="text" id="companyName" class="form-control form-control-input" placeholder="Company Name" name="employment[company_name]" required="">
                                </div>
                                <div class="input-group-flex row-indrole">
                                    <input type="text" id="positionRole" class="form-control form-control-input" placeholder="Position / Role" name="employment[position]" required="">
                                </div>
                                <div class="input-group-flex row-indworkstart">
                                    <div class="form-group">
                                        <div class="input-group input-group-datetimepicker date_start" id="form_dateworkstart" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input" data-target="#form_dateworkstart" placeholder="Start Date" name="employment[date_start]">
                                            <span class="input-group-addon" data-target="#form_dateworkstart" data-toggle="datetimepicker">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                        <div class="input-group input-group-datetimepicker date_end" id="form_dateworkend" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input" data-target="#form_dateworkend" placeholder="End Date" name="employment[date_end]">
                                            <span class="input-group-addon" data-target="#form_dateworkend" data-toggle="datetimepicker" id="workend">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group-flex row-indworkend">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" id="currently_working" class="custom-control-input" name="employment[currently_working]" value="1">
                                        <label class="custom-control-label" for="currently_working">I am currently working on this role</label>
                                    </div>
                                </div>
                                <div class="input-group-flex row-inddept">
                                    <input type="text" id="department" class="form-control form-control-input" placeholder="Department" name="employment[department]" required="">
                                </div>
                                <div class="input-group-flex row-indindustry">
                                    <select class="form-control form-control-input" placeholder="Industry" name="employment[industry]">
                                        <option disabled selected>Industry</option>
                                        <?php foreach($cms->getIndustries2Level() as $industry){ if($industry['subs']){ ?>
                                        <optgroup label="<?php echo $industry['title']; ?>">
                                        <?php foreach($industry['subs'] as $sub){ ?>
                                        <option value="<?php echo $sub['id']; ?>"><?php echo $sub['title']; ?></option>
                                        <?php } ?>
                                        </optgroup>
                                        <?php }} ?>
                                    </select>
                                </div>
                                <div class="input-group-flex row-indjd">
                                    <textarea class="form-control form-control-input summernote" placeholder="Job Description" id="od_jobd" rows="3" name="employment[job_desc]"></textarea>
                                </div>
                                <div class="input-group-flex row-indrespon">
                                    <textarea class="form-control form-control-input summernote" placeholder="Responsibilities" id="od_jobr" rows="3" name="employment[job_responsibilities]"></textarea>
                                </div>
                                <div class="input-group-flex row-indlocation">
                                    <select class="form-control form-control-input country" placeholder="Location" name="employment[country]" data-target="employment[state]" data-selected="<?php echo $user->info['country']; ?>" required>
                                        <option disabled selected>Country</option>
                                        <?php foreach($countries as $key){ ?>
                                        <option value="<?php echo $key['id']; ?>"<?php echo $key['id'] == $user->info['country'] ? ' selected' : '' ?>><?php echo $key['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="input-group-flex row-indlocation">
                                    <select class="form-control form-control-input" placeholder="Location" name="employment[state]" data-selected="<?php echo $user->info['state']; ?>" required>
                                        <option disabled selected>State</option>
                                        <?php foreach($cms->states($user->info['country']) as $key){ ?>
                                        <option value="<?php echo $key['id']; ?>"<?php echo $key['id'] == $user->info['state'] ? ' selected' : '' ?>><?php echo $key['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="input-group-flex row-indremunerate">
                                    <input type="text" id="remuneration" class="form-control form-control-input" placeholder="Remuneration (Last Drawn)" name="employment[remuneration]" required="">
                                </div>

                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <button type="submit" class="btn-icon-full btn-confirm">
                        <span class="btn-label">Submit</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- /.modal -->

    <!-- Modal - Add Education -->
    <div id="modal_add_education" class="modal modal-add-education modal-w-footer fade" aria-labelledby="modal_add_education" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-individualprofile" id="addEducation">
                <div class="modal-header">Add Education</div>
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="modal-body-container">
                            <input type="hidden" name="education[id]">
                            <div class="form-flex">
                                <div class="input-group-flex row-indeduins">
                                    <input type="text" id="eduIn" class="form-control form-control-input" placeholder="Education Institution" name="education[edu_institution]" required="">
                                </div>
                                <div class="input-group-flex row-indedulevel">
                                    <select class="form-control form-control-input" placeholder="Highest Education Level" name="education[highest_edu_level]">
                                        <option disabled selected>Highest Education Level</option>
                                        <option value="Elementary">Elementary</option>
                                        <option value="Secondary">Secondary</option>
                                        <option value="Certification">Certification</option>
                                        <option value="Diploma">Diploma</option>
                                        <option value="Degree">Degree</option>
                                        <option value="Master">Master</option>
                                        <option value="PhD">PhD</option>
                                    </select>
                                </div>
                                <div class="input-group-flex row-indfieldstudy">
                                    <select class="form-control form-control-input" placeholder="Field of Study" name="education[field]">
                                        <option disabled selected>Field of Study</option>
                                        <option value="Business">Business</option>
                                        <option value="Computer Science">Computer Science</option>
                                        <option value="Social Work">Social Work</option>
                                        <option value="Engineering">Engineering</option>
                                    </select>
                                </div>
                                <div class="input-group-flex row-indgradeyear">
                                    <div class="input-group input-group-datetimepicker date" id="grad_year" data-target-input="nearest" data-date-format="YYYY">
                                        <input type="text" class="form-control datetimepicker-input" placeholder="Graduation Year" name="education[grad_year]">
                                        <span class="input-group-addon" data-target="#grad_year" data-toggle="datetimepicker">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                    <?php /*
                                    <select class="form-control form-control-input" placeholder="Graduation Year" name="education[grad_year]">
                                        <option disabled selected>Graduation Year</option>
                                        <option value="2020">2020</option>
                                        <option value="2019">2019</option>
                                        <option value="2018">2018</option>
                                        <option value="2017">2017</option>
                                        <option value="2016">2016</option>
                                        <option value="2015">2015</option>
                                    </select>
                                    */ ?>
                                </div>
                                <div class="input-group-flex row-indmajor">
                                    <input type="text" id="major" class="form-control form-control-input" placeholder="Major" required="" name="education[major]">
                                </div>
                                <div class="input-group-flex row-indgrade">
                                    <input type="text" id="grade" class="form-control form-control-input" placeholder="Grade" required="" name="education[grade]">
                                </div>
                                <div class="input-group-flex row-indlocation">
                                    <select class="form-control form-control-input country" placeholder="Location" name="education[country]" data-target="education[state]" data-selected="<?php echo $user->info['country']; ?>" required>
                                        <option disabled selected>Country</option>
                                        <?php foreach($countries as $key){ ?>
                                        <option value="<?php echo $key['id']; ?>"<?php echo $key['id'] == $user->info['country'] ? ' selected' : '' ?>><?php echo $key['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="input-group-flex row-indlocation">
                                    <select class="form-control form-control-input" placeholder="Location" name="education[state]" data-selected="<?php echo $user->info['state']; ?>" required>
                                        <option disabled selected>State</option>
                                        <?php foreach($cms->states($user->info['country']) as $key){ ?>
                                        <option value="<?php echo $key['id']; ?>"<?php echo $key['id'] == $user->info['state'] ? ' selected' : '' ?>><?php echo $key['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <button type="submit" class="btn-icon-full btn-confirm">
                        <span class="btn-label">Submit</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <!-- Mobile Phone Number - IntlTelInput -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/intlTelInput.min.js"></script>
    <script>
        var inputHome = document.querySelector("#contactPhone");
        window.intlTelInput(inputHome, {
           autoHideDialCode: false,
           autoPlaceholder: "off",
           initialCountry: "my",
           preferredCountries: ['my'],
           separateDialCode: true,
           utilsScript: rootPath + "assets/js/plugins/utils.js",
           hiddenInput: "member[contact_number]",
        });
        
        var inputMobile = document.querySelector("#contactMobile");
        window.intlTelInput(inputMobile, {
           autoHideDialCode: false,
           autoPlaceholder: "off",
           initialCountry: "my",
           preferredCountries: ['my'],
           separateDialCode: true,
           utilsScript: rootPath + "assets/js/plugins/utils.js",
           hiddenInput: "member[mobile_number]",
        });
        
        var inputEmergency = document.querySelector("#emergencyContact");
        window.intlTelInput(inputEmergency, {
           autoHideDialCode: false,
           autoPlaceholder: "off",
           initialCountry: "my",
           preferredCountries: ['my'],
           separateDialCode: true,
           utilsScript: rootPath + "assets/js/plugins/utils.js",
           hiddenInput: "member[emergency_contact_number]",
        });
    </script>
    <!-- Mobile Phone Number - IntTelInput -->

    <script>
	function blockSpecialChar(event) {
        var regex = new RegExp("^[-@/ a-zA-Z]");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    }
		
    $(function(){
        $('.card-title').text('My Profile');

        if( $('.date_start').length ){
            $('.date_start').datetimepicker({
                format: 'DD/MM/YYYY',
                maxDate: '<?php echo date('Y-m-d', strtotime('-1 day')); ?>',
            });
        }

        if( $('.date_end').length ){
            $('.date_end').datetimepicker({
                format: 'DD/MM/YYYY',
            });
        }

        $(document).on('keypress', '.tt-input', function(e){
            if(e.which == 13) {
                e.preventDefault();
                e.stopPropagation();
            }
        });

        $('.language-input').tagsinput({
            freeInput: false,
            typeaheadjs: {
                source: function(q, sync) {
                    languages = <?php echo json_encode(languageList()); ?>;
                    results = Array();
                    $(languages).each(function(i, val){
                        if(val.toLowerCase().indexOf(q.toLowerCase()) != '-1'){
                            results.push(val);
                        }
                    });
                    sync(results);
                }
            }
        });

        $(document).on('change', '[name="employment[currently_working]"]', function(){
            if($(this).prop('checked')){
                $('[name="employment[date_end]"]').val('').prop('disabled', true);
            }else{
                $('[name="employment[date_end]"]').prop('disabled', false);
            }  
        });

        $(document).on('click', '.add-employment', function(){
            modal = $('#modal_add_employment');

            modal.find('.modal-header').text('Add Employment');
            modal.modal('show');
        });

        $(document).on('click', '.edit-employment', function(){
            modal = $('#modal_add_employment');
            data = $(this).data('data');
            $.each(data, function(key, val){
                el = modal.find('[name="employment[' + key + ']"]');
                tag = el.prop('tagName');
                if(tag == 'INPUT'){
                    if(el.prop('type') == 'text' || el.prop('type') == 'hidden'){
                        el.val(val);
                    }else if(el.prop('type') == 'checkbox'){
                        el.val('1');
                        if(val == '1'){
                            el.prop('checked', true);
                        }else{
                            el.prop('checked', false);
                        }
                    }
                }else if(tag == 'TEXTAREA'){
                    el.val(val);
                }else if(tag == 'SELECT'){
                    if(key == 'state'){
                        el.data('selected', val);
                    }
                    el.val(val);
                }
            });

            modal.find('.modal-header').text('Edit Employment');
            modal.modal('show');
        });

        $(document).on('click', '.remove-employment', function(){
            el = $(this);
            id = el.data('id');

            if(confirm('Are you sure you want to delete this information?')){
                $.ajax({
                    type: 'POST',
                    url: rootPath + 'my_profile/employment/delete',
                    dataType : 'json',
                    data: { id: id},
                    success: function(results){
                        if(results.error == true){
                            t('e', results.msg);
                        }else{
                            t('s', results.msg);
                            el.parent().parent().slideUp(function(){
                                el.parent().parent().remove();
                            });
                        }
                        return false;
                    },
                    error: function(error){
                        ajax_error(error);
                    },
                    complete: function(){}
                });
            }
        });
        
        $(document).on('submit', '#addEmployment', function(e){
            e.preventDefault();
            $("#footer-form-action").children().off('click');
            sleep(5000);
            el = $(this);
            data = el.serializeArray();

            if(data[0].value){
                url = rootPath + 'my_profile/employment/edit'
            }else{
                url = rootPath + 'my_profile/employment/add';
            }

            $.ajax({
                type:'POST',
                url: url,
                dataType : 'json',
                data: data,
                success: function(results){
                    if(results.error == true){
                        t('e', results.msg);
                    }else{
                        $('#modal_add_employment').modal('hide');
                        t('s', results.msg);

                        if(data[0].value){
                            $('#emp-' + data[0].value).replaceWith(results.html);
                        }else{
                            $('.employment-container').append(results.html);
                        }
                    }
                    return false;
                },
                error: function(error){
                    ajax_error(error);
                },
                complete: function(){}
            });

            return false;
        });

        $(document).on('click', '.add-education', function(){
            modal = $('#modal_add_education');

            modal.find('.modal-header').text('Add Education');
            modal.modal('show');
        });

        $(document).on('click', '.edit-education', function(){
            modal = $('#modal_add_education');
            data = $(this).data('data');
            $.each(data, function(key, val){
                el = modal.find('[name="education[' + key + ']"]');
                tag = el.prop('tagName');
                if(tag == 'INPUT'){
                    if(el.prop('type') == 'text' || el.prop('type') == 'hidden'){
                        el.val(val);
                    }else if(el.prop('type') == 'checkbox'){
                        el.val('1');
                        if(val == '1'){
                            el.prop('checked', true);
                        }else{
                            el.prop('checked', false);
                        }
                    }
                }else if(tag == 'TEXTAREA'){
                    el.val(val);
                }else if(tag == 'SELECT'){
                    if(key == 'state'){
                        el.data('selected', val);
                    }
                    el.val(val);
                }
            });

            modal.find('.modal-header').text('Edit Education');
            modal.modal('show');
        });

        $(document).on('click', '.remove-education', function(){
            el = $(this);
            id = el.data('id');

            if(confirm('Are you sure you want to delete this information?')){
                $.ajax({
                    type: 'POST',
                    url: rootPath + 'my_profile/education/delete',
                    dataType : 'json',
                    data: { id: id},
                    success: function(results){
                        if(results.error == true){
                            t('e', results.msg);
                        }else{
                            t('s', results.msg);
                            el.parent().parent().slideUp(function(){
                                el.parent().parent().remove();
                            });
                        }
                        return false;
                    },
                    error: function(error){
                        ajax_error(error);
                    },
                    complete: function(){}
                });
            }
        });

        $(document).on('submit', '#addEducation', function(e){
            e.preventDefault();
            $("#footer-form-action").children().off('click');
            sleep(5000);
            el = $(this);
            data = el.serializeArray();

            if(data[0].value){
                url = rootPath + 'my_profile/education/edit'
            }else{
                url = rootPath + 'my_profile/education/add';
            }

            $.ajax({
                type:'POST',
                url: url,
                dataType : 'json',
                data: data,
                success: function(results){
                    if(results.error == true){
                        t('e', results.msg);
                    }else{
                        $('#modal_add_education').modal('hide');
                        t('s', results.msg);

                        if(data[0].value){
                            $('#edu-' + data[0].value).replaceWith(results.html);
                        }else{
                            $('.education-container').append(results.html);
                        }
                    }
                    return false;
                },
                error: function(error){
                    ajax_error(error);
                },
                complete: function(){}
            });

            return false;
        });   
        
        $(document).on('change', '[name="member[nric]"]', function(){
            val = $(this).val();
            yb = <?php echo substr(date('Y'), 0, 2); ?>;
            yy = <?php echo substr(date('Y'), 2, 2); ?>;
            y = val.substr(0, 2);
            m = val.substr(2, 2);
            d = val.substr(4, 2);
            if(y && m <= 12 && d <= 31){
                if(y >= yy){
                    y = (yb-1) + y;
                }else{
                    y = yb + y;
                }
                $('[name="member[dob]"]').val(d + '/' + m + '/' + y);
            }
        });

        $('#modal_add_employment, #modal_add_education').on('show.bs.modal', function(){
            $(this).find('.country').change();
            $(this).find('[name="employment[currently_working]"]').change();
            $('.summernote').each(function(){
                val = $(this).val();
                placeholder = $(this).attr('placeholder');
                $(this).summernote({
                    tooltip: false,
                    placeholder: placeholder,
                    toolbar: [
                        ['style', ['bold', 'italic', 'underline']],
                        ['para', ['ul', 'ol']],
                    ],
                    height: 200,
                    width: '100%',
                });
                $(this).summernote('code', val);
            });
        });

        $('#modal_add_employment, #modal_add_education').on('hide.bs.modal', function(){
            $(this).find('[name]:not([type="checkbox"])').val('');
            $(document).find('select').each(function(){
                if($(this).data('selected')){
                    $(this).val($(this).data('selected'));
                }else{
                    $(this).val($(this).find('option:first').val());
                }
            });
        });

        skills = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: rootPath + 'ajax/skills.json?all=1'
        });
        skills.clearPrefetchCache();
        skills.initialize();
        
        $('.skills').tagsinput({
            /*itemValue: 'id',
            itemText: 'name',*/
            itemValue: function(item){ return item.id !== undefined ? item.id : 'new:'+item },
            itemText: function(item){ return item.name || item },
            typeaheadjs: {
                name: 'skills',
                displayKey: 'name',
                source: skills.ttAdapter(),
                templates: {
                    empty: Handlebars.compile("<div class='tt-empty'>No skills found</div><span class='tt-suggestion-footer tt-selectable add-new-tag'>Add new '{{query}}'</span>")
                }
            },
	        freeInput: true,
        });
        
        <?php if($selected_skills){ foreach($selected_skills as $skill){ ?>
        $('.skills').tagsinput('add', { 'id': '<?php echo $skill['id']; ?>', 'name': '<?php echo $skill['name']; ?>' });
        <?php }} ?>
    });
    </script>
    

</body>

</html>
