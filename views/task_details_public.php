<div class="public-container">
	<div class="container">
        <div class="col-xl-12 col-taskdetails-top">
            <div class="col-task-details-wrapper">
                <div class="col-task-details-header">
                    <div class="task-details-title-wrapper">
                        <h3 class="task-details-title">Art style illustration design for motion graphics</h3>
                        <div class="task-details-row task-details-category">
                            <span class="taskcentre-details-val val-taskcat">Home Services</span>
                            <span class="taskcentre-details-val val-taskcatsymbol">&gt;</span>
                            <span class="taskcentre-details-val val-tasksubcat">Computer, Laptop, and Phone Service &amp; Repair</span>
                        </div>
                    </div>
                </div>
                <div class="col-task-details-flex col-task-details-box-container">
                    <div class="col-task-details-left">
                        <div class="col-task-details-box">
                            <div class="task-details-row task-details-desc-cost">
                                <div class="task-details-row task-details-about">
                                    <label class="task-details-lbl"><?php  echo lang('task_description')?></label>
                                    <div class="task-details-about-block">
                                        <p>We want to develop a series of animation videos for our radio script (Cantonese &amp; Mandarin).</p>
                                        <p>What we would need from your end, is to develop 2 options of illustration design art styles, suitable for each radio script language. You may understand further on the type of art style we would prefer in the following attachment. Do get back to me on which type of illustration art style you are comfortable.</p>
                                        <p>You may refer to the attached example for your referral. The duration for the whole animation video per language will within 1 minute.</p>
                                        <p>We would appreciate to share us your rates:</p>
                                        <ol type="i">
                                            <li>Complete version of a static storyboard (including working file)</li>
                                            <li>From static storyboard to a completed motion graphics (including working file)</li>
                                        </ol>
                                        <p>Please note that the deadline for the first draft of the mood board is 2 days upon confirmation.</p>
                                        <p>We look forward to receiving your reply. Hope to hear from you soon:)</p>
                                    </div>
                                </div>
                                <div class="task-details-row task-details-cost-breakdown">
                                    <label class="task-details-lbl"><?php echo lang('post_step_4_task_value') ?></label>
                                    <div class="col-task-details-amount">
                                        <div class="task-row-taskamount">
                                            <span class="taskamount-prefix">RM</span>
                                            <span class="taskamount-val">3,000</span>
                                            <span class="taskamount-suffix">.00</span>
                                        </div>
                                    </div>
                                    <div class="taskcentre-details-row row-taskcostbreakdown">
                                        <div class="taskcentre-details-row row-taskhourlyrate">
                                            <label class="taskcentre-details-lbl lbl-taskhourlyrate"><?php echo lang('task_details_cost_breakdown') ?></label>
                                            <div class="taskcentre-details-val val-taskbythehour">RM300.00/hr</div>
                                            <div class="taskcentre-details-val val-taskhourlyrate">
                                                <span class="subval-taskcentre val-taskhourlyrate">x 10</span>
                                                <span class="subicon-taskcentre clock-icon">
                                                    <span class="bi-tooltip" data-toggle="tooltip" data-placement="auto" data-template="<div class='tooltip tooltip-fancy tooltip-tc-date' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>" data-title="10 hours" data-original-title="" title="">
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-clock" viewBox="0 0 16 16">
                                                            <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"></path>
                                                            <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"></path>
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="task-details-title-components">
                                        <span class="task-details-type"><?php echo lang('task_on_demand') ?></span>
                                        <span class="task-details-posted">
                                            <span class="task-details-posted-icon"><svg viewBox="0 0 16 16" class="bi bi-calendar" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1zm1-3a2 2 0 0 0-2 2v11a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2z"></path>
                                                    <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1a.5.5 0 0 1-1 0V.5a.5.5 0 0 1 .5-.5zm9 0a.5.5 0 0 1 .5.5V1a.5.5 0 0 1-1 0V.5a.5.5 0 0 1 .5-.5z"></path></svg></span>
                                            <span class="task-details-location-val">Posted 1 day ago</span>
                                        </span>
                                    </div>
                                    <div class="task-details-row task-details-date">
                                        <div class="task-details-row task-details-startdate">
                                            <label class="task-details-lbl"><?php  echo lang('task_details_start_date')?> <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="This is the award date and commencement date of the task.">
                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                                                    </svg>
                                                </span></label>
                                            <div class="task-details-deadline-block">27 August 2020</div>
                                        </div>
                                        <div class="task-details-row task-details-deadline">
                                            <label class="task-details-lbl"><?php  echo lang('task_details_complete_by')?> <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="This is the final day the task needs to be completed and submitted.">
                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                                                    </svg>
                                                </span>
                                            </label>
                                            <div class="task-details-deadline-block">30 August 2020</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="task-details-row task-details-comment">
                                <label class="task-details-lbl" data-toggle="collapse" data-target="#taskDetailsQuestion" aria-expanded="true" aria-controls="taskDetailsQuestion">
                                    <span>Q & A (3)</span><i class="fa fa-caret-down"></i>
                                </label>
                                <div class="collapse show" id="taskDetailsQuestion">
                                    <div class="task-details-comment-container">
                                        <ul class="task-details-comments-list">
                                            <li class="task-details-comment-row">
                                                <a target="_blank" href="#" class="rf">
                                                    <div class="task-details-profile-photo"><img src="<?php echo url_for("https://i.pravatar.cc/150?img=52") ?>" alt="Profile Photo"></div>
                                                </a>
                                                <div class="task-details-comment-text">
                                                    <div class="task-details-comment-user-date-wrap">
                                                        <a class="task-details-username-link" href="#">Elon Musk</a>
                                                        <span class="task-details-comment-date">A day ago</span>
                                                    </div>
                                                    <div class="task-details-comment-text-wrap">
                                                        <div class="task-details-comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua?</div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="task-details-comment-row task-details-comment-answer-row">
                                                <a target="_blank" href="#" class="rf">
                                                    <div class="task-details-profile-photo"><img src="<?php echo url_for("https://i.pravatar.cc/150?img=60") ?>" alt="Profile Photo"></div>
                                                </a>
                                                <div class="task-details-comment-text">
                                                    <div class="task-details-comment-user-date-wrap">
                                                        <a class="task-details-username-link" href="#">Brian Lovin</a>
                                                        <span class="task-details-comment-date">2 days ago</span>
                                                    </div>
                                                    <div class="task-details-comment-text-wrap">
                                                        <div class="task-details-comment-text">Only poster can answer the question. Seeker will not see the 'Answer' button.</div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="task-details-comment-row">
                                                <a target="_blank" href="#" class="rf">
                                                    <div class="task-details-profile-photo"><img src="<?php echo url_for("https://i.pravatar.cc/150?img=67") ?>" alt="Profile Photo"></div>
                                                </a>
                                                <div class="task-details-comment-text">
                                                    <div class="task-details-comment-user-date-wrap">
                                                        <a class="task-details-username-link" href="#">Patrick Collison</a>
                                                        <span class="task-details-comment-date">2 days ago</span>
                                                    </div>
                                                    <div class="task-details-comment-text-wrap">
                                                        <div class="task-details-comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua?</div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="task-details-comment-row task-details-comment-answer-row">
                                                <a target="_blank" href="#" class="rf">
                                                    <div class="task-details-profile-photo"><img src="<?php echo url_for("https://i.pravatar.cc/150?img=60") ?>" alt="Profile Photo"></div>
                                                </a>
                                                <div class="task-details-comment-text">
                                                    <div class="task-details-comment-user-date-wrap">
                                                        <a class="task-details-username-link" href="#">Brian Lovin</a>
                                                        <span class="task-details-comment-date">2 days ago</span>
                                                    </div>
                                                    <div class="task-details-comment-text-wrap">
                                                        <div class="task-details-comment-text">Only poster can answer the question. Seeker will not see the 'Answer' button.</div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="task-details-comment-row">
                                                <a target="_blank" href="#" class="rf">
                                                    <div class="task-details-profile-photo"><img src="<?php echo url_for("https://i.pravatar.cc/150?img=47") ?>" alt="Profile Photo"></div>
                                                </a>
                                                <div class="task-details-comment-text">
                                                    <div class="task-details-comment-user-date-wrap">
                                                        <a class="task-details-username-link" href="#">Aubree Walsh</a>
                                                        <span class="task-details-comment-date">A week ago</span>
                                                    </div>
                                                    <div class="task-details-comment-text-wrap">
                                                        <div class="task-details-comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat?</div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-task-details-right">
                        <div class="col-task-details-box">
                            <div class="task-details-row task-details-profile">
                                <label class="task-details-lbl" data-toggle="collapse" data-target="#taskDetailsPoster" aria-expanded="true" aria-controls="taskDetailsPoster">
                                    <i class="fa fa-caret-down"></i>
                                    <span><?php echo lang('task_details_poster_details')?></span>
                                </label>
                                <div class="collapse show" id="taskDetailsPoster">
                                    <div class="profile-wrapper">
                                        <div class="profile-avatar-wrapper">
                                            <div class="profile-avatar">
                                                <a href="#" class="task-details-poster-link"><img class="avatar-img" src="<?php echo url_for("https://i.pravatar.cc/150?img=60") ?>" alt=""></a>
                                            </div>
                                            <div class="profile-avatar-details">
                                                <a href="#" class="task-details-poster-link">
                                                    <h5 class="profile-name">Brian Lovin</h5>
                                                </a>
                                                <div class="task-details-profile-location">
                                                    <span class="task-details-profile-location-icon"><svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path></svg></span>
                                                    <span class="task-details-profile-location-val">Kuala Lumpur, Malaysia</span>
                                                </div>
                                                <div class="task-details-profile-rating">
                                                    <div class="task-details-profile-rating-star">
                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                        <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                        <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                    </div>
                                                    <div class="task-details-profile-rating-val">
                                                        <span class="task-row-average-rating-val">4.2</span>
                                                        <span class="task-row-divider">/</span>
                                                        <span class="task-row-total-rating-val">5 reviews</span>
                                                    </div>
                                                </div>
                                                <div class="profile-info-details">
                                                    <div class="task-details-profile-published">
                                                        <span class="task-details-profile-label"><?php echo lang('task_published')?>:</span>
                                                        <span class="task-details-profile-val">38</span>
                                                    </div>
                                                    <div class="task-details-profile-dispute">
                                                        <span class="task-details-profile-val"><?php echo lang('task_details_dispute_resolution');?> </span>
                                                        <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="This user has adopted dispute resolution for 6 out of 38 projects undertaken">
                                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                                                            </svg>
                                                        </span>
                                                    </div>
                                                    <div class="task-details-profile-membersince">
                                                        <span class="task-details-profile-label"><?php  echo lang('task_details_member_since');?>:</span>
                                                        <span class="task-details-profile-val">August 2020</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="task-details-row task-details-alldetails">
                                <label class="task-details-lbl" data-toggle="collapse" data-target="#taskDetailsAll" aria-expanded="false" aria-controls="taskDetailsAll">
                                    <i class="fa fa-caret-down"></i>
                                    <span>Location</span>
                                </label>
                                <div class="collapse" id="taskDetailsAll">
                                    <div class="task-details-row task-details-taskloc">
                                        <!--<span class="task-details-location-val">This task can be done remotely.</span>-->
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1991.970104730827!2d101.6643407!3d3.1105221!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc4ba617b94b55%3A0xcdc435ee7451c3c0!2sThe%20Vertical%20Corporate%20Office%20Tower%20A!5e0!3m2!1sen!2smy!4v1612752004237!5m2!1sen!2smy" width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="task-details-row task-details-skills">
                                <label class="task-details-lbl" data-toggle="collapse" data-target="#taskDetailsSkills" aria-expanded="false" aria-controls="taskDetailsSkills">
                                    <i class="fa fa-caret-down"></i>
                                    <span><?php echo lang('task_details_skills')?> <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo lang('task_details_skills_tooltip') ?>">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                                            </svg>
                                        </span>
                                    </span>
                                </label>
                                <div class="collapse" id="taskDetailsSkills">
                                    <div class="task-details-deadline-block">
                                        <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Graphic Design</span></a>
                                        <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Illustration</span></a>
                                        <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe Illustrator</span></a>
                                        <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe Photoshop</span></a>
                                        <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Adobe After Effects</span></a>
                                        <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Animation</span></a>
                                        <a href="" class="skills-label-link"><span class="tag label label-info bubble-lbl">Motion Graphics</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="task-details-row task-details-actions">
                            <div class="task-details-actions-fav">
                                <button type="button" class="btn-icon-full btn-fav">
                                    <span class="btn-label"><?php echo lang('task_save_fav') ?></span>
                                    <span class="btn-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                            <path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path>
                                        </svg>
                                    </span>
                                </button>
                            </div>
                            <div class="task-details-actions-apply">
                                <button type="button" class="btn-icon-full btn-apply">
                                    <span class="btn-label"><?php echo lang('task_details_apply_this_task') ?></span>
                                    <span class="btn-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                            <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                            <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                        </svg>
                                    </span>
                                </button>
                            </div>
                            <div class="task-row-taskid task-row-taskid-taskdetails">
                                <span class="task-row-taskid-lbl"><?php echo lang('task_id')?>: </span>
                                <span class="task-row-taskid-val">2032211</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>