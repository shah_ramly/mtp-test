<div class="public-container">
	<div class="container">
		<div class="col-public-search-tab-wrapper">
			<ul class="nav nav-pills nav-pills-searchresult" id="pills-tabSearchResultPublic" role="tablist">
				<li class="nav-item">
					<a class="nav-link <?php echo $scope === 'task' ? 'active' : '' ?>" id="searchTaskTab-tab" href="<?php echo option('site_uri') . url_for('/search') . '?scope=task&type=all' ?>" role="tab" aria-controls="searchTaskTab" aria-selected="true"><?php echo lang('public_search_opportunities'); ?></a>
				</li>
				<li class="nav-item">
					<a class="nav-link <?php echo $scope === 'talent' ? 'active' : '' ?>" id="searchTalentTab-tab" href="<?php echo option('site_uri') . url_for('/search') . '?scope=talent' ?>" role="tab" aria-controls="searchTalentTab" aria-selected="false"><?php echo lang('public_search_talent'); ?></a>
				</li>
			</ul>
			<div class="tab-content" id="pills-tabSearchResultContentPublic">
                <?php if( $scope === 'task' ) : ?>
				<div class="tab-pane fade show <?php echo $scope === 'task' ? 'active' : '' ?>" id="searchTaskTab" role="tabpanel">
					<div class="col-xl-12">
						<div class="row">
							<div class="col-xl-12 col-tasklisting-top">
								<div class="col-public-search-wrapper">
									<form action="#" id="searchForm" class="search-public-form">
										<div class="search-dropdown-filter-top-wrapper form-flex">
											<div class="search-dropdown-filter-container">
												<input type="text" class="form-control form-control-input"
												       name="q"
												       value="<?php echo $search_term ?>"
												       placeholder="<?php echo lang('public_search_keyword_placeholder'); ?>"
												       id="inputBox">
											</div>
											<span class="search-dropdown-filter-btn">
                                                <button class="btn-search-public" type="submit">
                                                    <span class="btn-label">Search</span>
                                                    <span class="public-search-icon search-icon">
                                                        <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                                        </svg>
                                                    </span>
                                                </button>
                                            </span>
											<span class="bookmark-dropdown-filter-btn dropdown">
                                                <button class="btn-bookmark dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                                    <span class="btn-label"><?php echo lang('public_search_save_search_button'); ?></span>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-saved-search">
                                                    <div class="modal-advance-filter-container-left">
                                                        <div class="modal-advanced-filter-container">
                                                            <div class="keyword-filter-container">
                                                                <label><?php echo lang('public_search_save_search_title'); ?></label>
                                                                <span data-text="<?php echo lang('public_search_save_search_content') ?>"><?php echo lang('public_search_save_search_content'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
                                                            <div class="modal-filter-action form-block-filter-flex">
                                                                <button type="button" class="btn-public-sign-up btn-confirm" data-dismiss="modal" data-orientation="next" onclick="location.href='<?php echo url_for('sign_up') ?>';">
                                                                    <span class="btn-label"><?php echo lang('public_search_signup'); ?></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </span>
											<span class="bookmark-dropdown-addfilter-btn dropdown">
                                                <button class="btn-addbookmark dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                                    <span class="public-search-icon addsearch-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                                                            <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                                                        </svg>
                                                    </span>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-saved-search dropdown-menu-add-saved-search">
                                                    <div class="modal-advance-filter-container-left">
                                                        <div class="modal-advanced-filter-container">
                                                            <div class="keyword-filter-container">
                                                                <label><?php echo lang('public_search_save_search_title'); ?></label>
                                                                <span data-text="<?php echo lang('public_search_save_search_content') ?>"><?php echo lang('public_search_save_search_content'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
                                                            <div class="modal-filter-action form-block-filter-flex">
                                                                <button type="button" class="btn-public-sign-up btn-confirm" data-dismiss="modal" data-orientation="next" onclick="location.href='<?php echo url_for('sign_up') ?>';">
                                                                    <span class="btn-label"><?php echo lang('public_search_signup'); ?></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </span>
										</div>
										<div class="col-search-terms-result" style="<?php echo (($keywords === '' && request('scope') === 'task') || ($keywords === '')) ? "display:none" : "" ?>">
											<div class="col-search-terms-result-title"><?php echo lang('public_search_keyword_word'); ?> :</div>
											<div class="col-search-terms-result-value"><?php echo request('scope') === 'task' ? $keywords : '' ?></div>
										</div>
										<div class="search-advanced-filter-wrapper">
											<button class="btn-icon-invert-full btn-advanced-filter" type="button" data-toggle="collapse" data-target="#advancedFilterTaskCollapse" aria-expanded="false" aria-controls="collapseExample">
												<span class="btn-label"><?php echo lang('advanced_filter') ?></span>
												<span class="btn-icon-invert">
														<svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
														<path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
													</svg>
												</span>
											</button>
											<div class="collapse dropdown-menu-advanced-filter" id="advancedFilterTaskCollapse">
												<div class="search-dropdown-filter-btm-wrapper form-flex tasks-filter">
											<div class="dropdown dropdown-filter-container filter-dashboard-opportunitiestype">
												<button class="btn btn-dropdown-label dropdown-toggle <?php echo isset($filters['type']) ? 'border-info' : '' ?>"
												        type="button" id="dropdownOpportunitiesType" data-toggle="dropdown"
												        aria-haspopup="true" aria-expanded="false">
													<label class="filter-btn-lbl"><?php echo lang('public_search_filter_all_opportunities'); ?></label>
												</button>
												<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownOpportunitiesType">
													<div class="form-block-task-filter">
														<div class="custom-control custom-checkbox">
															<input type="checkbox" class="custom-control-input"
															       name="type"
															       id="opportunitiesType_0"
															       value="all"
                                                                <?php if(isset($filters['type']) && in_array('all', $filters['type'])):?>
																	data-input-status="true"
																	checked
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
															>
															<label class="custom-control-label" for="opportunitiesType_0"><?php echo lang('public_search_filter_all'); ?></label>
														</div>
														<div class="custom-control custom-checkbox">
															<input type="checkbox" class="custom-control-input"
															       name="type"
															       id="opportunitiesType_1"
															       value="task"
                                                                <?php if(isset($filters['type']) && in_array('task', $filters['type'])):?>
																	data-input-status="true"
																	checked
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
															>
															<label class="custom-control-label" for="opportunitiesType_1"><?php echo lang('public_search_filter_on_demand_task'); ?></label>
														</div>
														<div class="custom-control custom-checkbox">
															<input type="checkbox" class="custom-control-input"
															       name="type"
															       id="opportunitiesType_2"
															       value="job"
                                                                <?php if(isset($filters['type']) && in_array('job', $filters['type'])):?>
																	data-input-status="true"
																	checked
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
															>
															<label class="custom-control-label" for="opportunitiesType_2"><?php echo lang('public_search_filter_full_time_work'); ?></label>
														</div>
													</div>
												</div>
											</div>
											<div class="dropdown dropdown-filter-container filter-dashboard-taskcategory"
                                                <?php if(!isset($filters['type']) || (isset($filters['type']) && in_array('all', $filters['type']))): ?>
													style="display:none"
                                                <?php endif ?>
											>
												<button class="btn btn-dropdown-label dropdown-toggle <?php echo isset($filters['category']) ? 'border-info' : '' ?>"
												        type="button" id="dropdownTaskCategory" data-toggle="dropdown"
												        aria-haspopup="true" aria-expanded="false">
													<label class="filter-btn-lbl"><?php echo lang('public_search_category'); ?></label>
												</button>
												<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTaskCategory">
													<div class="form-block-task-filter categories-input">
														<div class="custom-control custom-checkbox">
															<input type="checkbox" class="custom-control-input select-all-category"
															       name="category"
															       id="filtercat_0"
															       value="0"
                                                                <?php if(isset($filters['category']) && ( in_array('0', explode(',', $filters['category'])) )): ?>
																	data-input-status="true"
																	checked
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
															>
															<label class="custom-control-label" for="filtercat_0"><?php echo lang('public_search_all_category'); ?></label>
														</div>
                                                        <?php if(isset($categories)): ?>
                                                        <?php foreach($categories as $category): ?>
														<div class="custom-control custom-checkbox">
															<input type="checkbox" class="custom-control-input"
															       name="category"
															       id="filtercat_<?php echo $category['id'] ?>"
															       value="<?php echo $category['id'] ?>"
                                                                <?php if(isset($filters['category']) && (in_array($category['id'], explode(',', $filters['category'])))): ?>
																	data-input-status="true"
																	checked
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
														    >
															<label class="custom-control-label" for="filtercat_<?php echo $category['id'] ?>"><?php echo $category['name'] ?></label>
														</div>
                                                        <?php endforeach ?>
                                                        <?php endif ?>
													</div>
												</div>
												<template id="task-categories">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input select-all-category"
														       name="category"
														       id="filtercat_0"
														       value="0"
                                                            <?php if(isset($filters['category']) && ( in_array('0', explode(',', $filters['category'])) )): ?>
																data-input-status="true"
																checked
                                                            <?php else: ?>
																data-input-status="false"
                                                            <?php endif; ?>
														>
														<label class="custom-control-label" for="filtercat_0"><?php echo lang('public_search_all_category'); ?></label>
													</div>
                                                    <?php if(isset($task_categories)): ?>
                                                    <?php foreach($task_categories as $category): ?>
														<div class="custom-control custom-checkbox">
															<input type="checkbox" class="custom-control-input" name="category"
                                                                <?php if(isset($filters['category']) && (in_array($category['id'], explode(',', $filters['category'])))): ?>
																	data-input-status="true"
																	checked
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
																   value="<?php echo $category['id'] ?>" id="filtercat_<?php echo $category['id'] ?>">
															<label class="custom-control-label" for="filtercat_<?php echo $category['id'] ?>"><?php echo $category['name'] ?></label>
														</div>
                                                    <?php endforeach ?>
                                                    <?php endif ?>
												</template>
												<template id="job-categories">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input select-all-category"
														       name="category"
														       id="filtercat_0"
														       value="0"
                                                            <?php if(isset($filters['category']) && ( in_array('0', explode(',', $filters['category'])) )): ?>
																data-input-status="true"
																checked
                                                            <?php else: ?>
																data-input-status="false"
                                                            <?php endif; ?>
														>
														<label class="custom-control-label" for="filtercat_0"><?php echo lang('public_search_all_category'); ?></label>
													</div>
                                                    <?php if(isset($job_categories)): ?>
                                                    <?php foreach($job_categories as $category): ?>
														<div class="custom-control custom-checkbox">
															<input type="checkbox" class="custom-control-input" name="category"
                                                                <?php if(isset($filters['category']) && (in_array($category['id'], explode(',', $filters['category'])))): ?>
																	data-input-status="true"
																	checked
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
																   value="<?php echo $category['id'] ?>" id="filtercat_<?php echo $category['id'] ?>">
															<label class="custom-control-label" for="filtercat_<?php echo $category['id'] ?>"><?php echo $category['name'] ?></label>
														</div>
                                                    <?php endforeach ?>
                                                    <?php endif ?>
												</template>
											</div>
											<div class="dropdown dropdown-filter-container filter-dashboard-tasklocation">
												<button class="btn btn-dropdown-label dropdown-toggle <?php echo isset($filters['location_by']) ? 'border-info' : '' ?>"
												        type="button" id="dropdownTaskLocation" data-toggle="dropdown"
												        aria-haspopup="true" aria-expanded="false">
													<label class="filter-btn-lbl"><?php echo lang('public_search_filter_location'); ?></label>
												</button>
												<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTaskLocation">
													<div class="form-block-task-filter">
														<div class="form-filter-select-by-location">
															<select class="form-control form-control-input" placeholder="State" name="location_by">
																<option value=""><?php echo lang('public_search_filter_location_select_by'); ?></option>
																<option <?php echo isset($filters['location_by']) && $filters['location_by'] === 'locbystate' ? 'selected' : '' ?>
																		value="locbystate"><?php echo lang('public_search_filter_location_select_by_state'); ?>
																</option>
																<option <?php echo isset($filters['location_by']) && $filters['location_by'] === 'locbynearest' ? 'selected' : '' ?>
																		value="locbynearest"><?php echo lang('public_search_filter_location_select_by_nearest'); ?>
																</option>
															</select>
														</div>
														<div class="form-filter-state" style="<?php echo !isset($filters['state']) ? 'display: none' : '' ?>">
															<select class="form-control form-control-input state" name="state" id="listing_state"
															        data-target="city"
                                                                <?php if(isset($filters['state'])):?>
																	data-input-status="true"
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
																    placeholder="State">
																<option value=""><?php echo lang('public_search_filter_location_select_by_state'); ?></option>
                                                                <?php foreach($states as $state): ?>
																	<option <?php echo $scope === 'task' && isset($filters['state']) && $state['id'] === $filters['state'] ? 'selected' : '' ?>
																			value="<?php echo $state['id'] ?>"><?php echo $state['name'] ?></option>
                                                                <?php endforeach; ?>
															</select>
														</div>
														<div class="form-filter-city" style="<?php echo !isset($filters['state']) ? 'display: none' : '' ?>">
															<select class="form-control form-control-input" name="city" id="listing_city"
                                                                <?php if(isset($filters['city'])):?>
																	data-input-status="true"
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
																    placeholder="City">
																<option value=""><?php echo lang('public_search_filter_location_select_by_town'); ?></option>
                                                                <?php foreach($cities as $city): ?>
																	<option <?php echo request('scope') === 'task' && isset($filters['city']) && $city['id'] === $filters['city'] ? 'selected' : '' ?>
																			value="<?php echo $city['id'] ?>">
                                                                        <?php echo $city['name'] ?>
																	</option>
                                                                <?php endforeach; ?>
															</select>
														</div>
														<div class="form-filter-within" style="<?php echo isset($filters['location_by']) && $filters['location_by'] === 'locbynearest' ? '' : 'display: none'; ?>">
															<select class="form-control form-control-input" name="within" placeholder="within">
																<option value="" selected><?php echo lang('public_search_filter_location_select_by_nearest_select_within'); ?></option>
																<option <?php echo isset($filters['within']) && $filters['within'] === '5km' ? 'selected' : '' ?> value="5km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance'); ?> 5km</option>
																<option <?php echo isset($filters['within']) && $filters['within'] === '10km' ? 'selected' : '' ?> value="10km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance'); ?> 10km</option>
																<option <?php echo isset($filters['within']) && $filters['within'] === '20km' ? 'selected' : '' ?> value="20km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance'); ?> 20km</option>
																<option <?php echo isset($filters['within']) && $filters['within'] === '30km' ? 'selected' : '' ?> value="30km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance'); ?> 30km</option>
																<option <?php echo isset($filters['within']) && $filters['within'] === '50km' ? 'selected' : '' ?> value="50km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance'); ?> 50km</option>
															</select>
														</div>
													</div>
												</div>
											</div>
											<div class="dropdown dropdown-filter-container filter-dashboard-taskdate"
                                                <?php if(!isset($filters['type']) || (isset($filters['type']) && in_array('all', $filters['type']))): ?>
													style="display:none"
                                                <?php endif ?>
											>
												<button class="btn btn-dropdown-label dropdown-toggle <?php echo isset($filters['date_type']) ? 'border-info' : '' ?>"
												        type="button" id="dropdownTaskDate" data-toggle="dropdown"
												        aria-haspopup="true" aria-expanded="false">
													<label class="filter-btn-lbl"><?php echo lang('public_search_date'); ?></label>
												</button>
												<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTaskDate">
													<div class="form-block-task-filter">
														<div class="form-filter-datepostedstart">
															<select class="form-control form-control-input" id="selectDatePostedStart" name="date_type"
                                                                <?php if(isset($filters['date_type'])):?>
																	data-input-status="true"
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
																    placeholder="<?php echo lang('public_search_posted_date'); ?>">
																<option value="" selected=""><?php echo lang('public_search_select'); ?></option>
																<option value="postedDate"
                                                                    <?php if(isset($filters['date_type']) && $filters['date_type'] === 'postedDate'):?>
																		selected
                                                                    <?php endif; ?>
																><?php echo lang('public_search_posted_date'); ?></option>
																<option value="<?php echo isset($filters['type']) && in_array('job', $filters['type']) ? 'closeDate':'startDate' ?>"
                                                                    <?php if(isset($filters['date_type']) && ($filters['date_type'] === 'startDate' || $filters['date_type'] === 'closeDate')):?>
																		selected
                                                                    <?php endif; ?>
																> <?php echo isset($filters['type']) && in_array('job', $filters['type']) ? lang('public_search_closing_date') : lang('public_search_start_date') ?></option>
															</select>
														</div>
														<div class="form-filter-dateposted" <?php echo isset($filters['date_type']) && $filters['date_type'] === 'postedDate' ? '': 'style="display:none"' ?>>
															<div class="form-filter-dateposted">
																<select class="form-control form-control-input"
																        name="date_period"
                                                                    <?php if(isset($filters['date_period'])):?>
																		data-input-status="true"
                                                                    <?php else: ?>
																		data-input-status="false"
                                                                    <?php endif; ?>
																	    id="search_filter_dateposted" placeholder="<?php echo lang('public_search_posted_date'); ?>">
																	<option value="" selected><?php echo lang('public_search_select'); ?></option>
																	<option value="any" <?php echo (isset($filters['date_period']) && $filters['date_period'] === 'any') ? 'selected' : '' ?>><?php echo lang('public_search_any_time'); ?></option>
																	<option value="24h" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '24h') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_last_x_hours'), 24); ?></option>
																	<option value="3d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '3d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_last_x_days'), 3); ?></option>
																	<option value="7d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '7d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_last_x_days'), 7); ?></option>
																	<option value="14d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '14d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_last_x_days'), 14); ?></option>
																	<option value="30d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '30d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_last_x_days'), 30); ?></option>
																	<option value="customrange" <?php echo (isset($filters['date_period']) && $filters['date_period'] === 'customrange') ? 'selected' : '' ?>><?php echo lang('public_search_custom_range'); ?></option>
																</select>
															</div>
															<div class="task-filter-row task-filter-row-datepicker-fromto" <?php echo isset($filters['date_type']) && $filters['date_type'] === 'postedDate' && !empty($filters['date_period']) && $filters['date_period'] === 'customrange' ? '': 'style="display:none"' ?>>
																<div class="form-group form-datetime-search-filter-from">
																	<div class="form-group">
																		<div class="input-group input-group-datetimepicker date" id="form_datetime_search_filter_from" data-target-input="nearest">
																			<input type="text" class="form-control datetimepicker-input" name="from"
																			       <?php if(isset($filters['from'])):?>
																						value="<?php echo $filters['from'] ?>"
																						data-input-status="true"
                                                                                    <?php else: ?>
																						data-input-status="false"
                                                                                    <?php endif; ?>
																			       data-target="#form_datetime_search_filter_from" placeholder="<?php echo lang('public_search_date_from'); ?>" />
																			<span class="input-group-addon" data-target="#form_datetime_search_filter_from" data-toggle="datetimepicker">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
																		</div>
																	</div>

																</div>
																<div class="form-group form-datetime-search-filter-to">
																	<div class="form-group">
																		<div class="input-group input-group-datetimepicker date" id="form_datetime_search_filter_to" data-target-input="nearest">
																			<input type="text" class="form-control datetimepicker-input" name="to"
																			       <?php if(isset($filters['to'])):?>
																						value="<?php echo $filters['to'] ?>"
																						data-input-status="true"
                                                                                    <?php else: ?>
																						data-input-status="false"
                                                                                    <?php endif; ?>
																			       data-target="#form_datetime_search_filter_to" placeholder="<?php echo lang('public_search_date_to'); ?>" />
																			<span class="input-group-addon" data-target="#form_datetime_search_filter_to" data-toggle="datetimepicker">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="form-filter-startdate" <?php echo isset($filters['date_type']) && in_array($filters['date_type'], ['startDate', 'closeDate']) ? '': 'style="display:none"' ?>>
															<div class="form-filter-startdate">
																<select class="form-control form-control-input" id="search_filter_startdate" name="date_period" placeholder="<?php echo isset($filters['type']) && in_array('job', $filters['type']) ? lang('public_search_closing_date') : lang('public_search_start_date'); ?>">
																	<option  value="" selected><?php echo lang('public_search_select'); ?></option>
																	<option value="any" <?php echo (isset($filters['date_period']) && $filters['date_period'] === 'any') ? 'selected' : '' ?>><?php echo lang('public_search_any_time'); ?></option>
																	<option value="24h" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '24h') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_next_x_hours'), 24); ?></option>
																	<option value="3d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '3d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_next_x_days'), 3); ?></option>
																	<option value="7d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '7d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_next_x_days'), 7); ?></option>
																	<option value="14d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '14d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_next_x_days'), 14); ?></option>
																	<option value="30d" <?php echo (isset($filters['date_period']) && $filters['date_period'] === '30d') ? 'selected' : '' ?>><?php echo sprintf(lang('public_search_next_x_days'), 30); ?></option>
																	<option value="customrange" <?php echo (isset($filters['date_period']) && $filters['date_period'] === 'customrange') ? 'selected' : '' ?>><?php echo lang('public_search_custom_range'); ?></option>
																</select>
															</div>
															<div class="task-filter-row task-filter-row-datepicker-fromto" <?php echo isset($filters['date_type']) && in_array($filters['date_type'], ['startDate', 'closeDate']) && !empty($filters['date_period']) && $filters['date_period'] === 'customrange' ? '': 'style="display:none"' ?>>
																<div class="form-group form-datetime-search-filter-from">
																	<div class="form-group">
																		<div class="input-group input-group-datetimepicker date" id="form_datetime_search_filter_from_start" data-target-input="nearest">
																			<input type="text" class="form-control datetimepicker-input" name="from"
                                                                                    <?php if(isset($filters['from'])):?>
																						value="<?php echo $filters['from'] ?>"
																						data-input-status="true"
                                                                                    <?php else: ?>
																						data-input-status="false"
                                                                                    <?php endif; ?>
																			       data-target="#form_datetime_search_filter_from_start" placeholder="<?php echo lang('public_search_date_from'); ?>" />
																			<span class="input-group-addon" data-target="#form_datetime_search_filter_from_start" data-toggle="datetimepicker">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
																		</div>
																	</div>

																</div>
																<div class="form-group form-datetime-search-filter-to">
																	<div class="form-group">
																		<div class="input-group input-group-datetimepicker date" id="form_datetime_search_filter_to_start" data-target-input="nearest">
																			<input type="text" class="form-control datetimepicker-input" name="to"
                                                                                    <?php if(isset($filters['to'])):?>
																						value="<?php echo $filters['to'] ?>"
																						data-input-status="true"
                                                                                    <?php else: ?>
																						data-input-status="false"
                                                                                    <?php endif; ?>
																			       data-target="#form_datetime_search_filter_to_start" placeholder="<?php echo lang('public_search_date_to'); ?>" />
																			<span class="input-group-addon" data-target="#form_datetime_search_filter_to_start" data-toggle="datetimepicker">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="dropdown dropdown-filter-container filter-dashboard-taskskills">
												<button class="btn btn-dropdown-label dropdown-toggle <?php echo isset($filters['skills']) ? 'border-info' : '' ?>"
												        type="button" id="dropdownTaskSkills" data-toggle="dropdown"
												        aria-haspopup="true" aria-expanded="false">
													<label class="filter-btn-lbl"><?php echo lang('public_search_filter_skills'); ?></label>
												</button>
												<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTaskSkills">
													<div class="form-block-task-filter">
														<div class="bstags-skills">
															<input type="text" name="skills" id="task-skills"
                                                                <?php if(request('scope') === 'task' && isset($filters['skills'])):?>
																	data-input-status="true"
																	checked
																	value="<?php echo implode(',', $filters['skills']) ?>"
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
																   data-role="tagsinput" class="bootstrap-tagsinput" placeholder="<?php echo lang('public_search_filter_skills_placeholder'); ?>" />
														</div>
													</div>
												</div>
											</div>
										</div>
											</div>
										</div>
									</form>
									<form action="<?php echo url_for('/search') ?>" id="hiddenSearchForm" class="search-public-form" method="get">
										<input type="hidden" name="scope" value="task" />
										<input type="hidden" name="q" value="<?php echo $search_term ?>" />
										<input type="hidden" name="perform-search" value="no" disabled />
										<input type="hidden" name="page" disabled value="<?php echo $current_page ?? 1 ?>" />
										<input type="hidden" name="type"
                                            <?php if(isset($filters['type']) ): ?>
											   value="<?php echo is_array($filters['type']) ? implode(',', $filters['type']) : $filters['type'] ?>"
                                            <?php else: ?>
											   disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="category"
                                            <?php if(isset($filters['category']) ): ?>
											   value="<?php echo $filters['category'] ?>"
                                            <?php else: ?>
											   disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="location_by"
                                            <?php if(isset($filters['location_by']) ): ?>
												value="<?php echo $filters['location_by'] ?>"
                                            <?php else: ?>
												disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="within"
                                            <?php if(isset($filters['within']) ): ?>
												value="<?php echo $filters['within'] ?>"
                                            <?php else: ?>
												disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="state"
                                            <?php if(isset($filters['state']) ): ?>
											   value="<?php echo $filters['state'] ?>"
                                            <?php else: ?>
											   disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="city"
                                            <?php if(isset($filters['city']) ): ?>
											   value="<?php echo $filters['city'] ?>"
                                            <?php else: ?>
											   disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="task_type"
                                            <?php if(isset($filters['task_type']) ): ?>
											   value="<?php echo is_array($filters['task_type']) ? implode(',', $filters['task_type']) : $filters['task_type'] ?>"
                                            <?php else: ?>
											   disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="task_budget"
                                            <?php if(isset($filters['budget_task']) ): ?>
											   value="<?php echo $filters['budget_task'] ?>"
                                            <?php else: ?>
											   disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="date_type"
                                            <?php if(isset($filters['date_type']) ): ?>
											   value="<?php echo $filters['date_type'] ?>"
                                            <?php else: ?>
											   disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="date_period"
                                            <?php if(isset($filters['date_period']) ): ?>
											   value="<?php echo $filters['date_period'] ?>"
                                            <?php else: ?>
											   disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="from"
                                            <?php if(isset($filters['from']) && isset($filters['date_period']) && $filters['date_period'] === 'customrange' ): ?>
											   value="<?php echo $filters['from'] ?>"
                                            <?php else: ?>
											   disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="to"
                                            <?php if(isset($filters['to']) && isset($filters['date_period']) && $filters['date_period'] === 'customrange' ): ?>
											   value="<?php echo $filters['to'] ?>"
                                            <?php else: ?>
											   disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="skills"
                                            <?php if(isset($filters['skills']) ): ?>
											   value="<?php echo implode(',', $filters['skills']) ?>"
                                            <?php else: ?>
											   disabled
                                            <?php endif ?>
										/>

									</form>
								</div>

								<div class="col-task-listing-wrapper">
									<div class="row">
										<div class="col-xl-12 col-task-row-wrapper">
											<div class="task-result-container">
                                            <?php if(!empty($listing)): ?>
                                                <?php $counter = 1; ?>
                                                <?php foreach($listing as $list): ?>
                                                    <?php 
                                                        switch ($counter) {
                                                            case 1:
                                                                $color = "gold";
                                                                break;
                                                            case 2:
                                                                $color = "green";
                                                                break;
                                                            case 3:
                                                                $color = "red";
                                                                break;
                                                            case 4:
                                                                $color = "orange";
                                                                break;
                                                            case 5:
                                                                $color = "maroon";
                                                                break;
                                                            case 6:
                                                                $color = "purple";
                                                                break;
                                                            case 7:
                                                                $color = "cyan";
                                                                break;
                                                            default:
                                                                $color = "blue";
                                                                $counter = 1;
                                                        }
                                                
                                                        $counter += 1;
                                                        
                                                        
                                                    ?>
                                                    <?php $type = in_array($list['type'], ['internal_job', 'external_job']) ? 'job' : 'task'; ?>
                                                    <?php $external = in_array($list['type'], ['external_job', 'external_task']) ? '?s=external' : '' ?>
                                                    <?php 
                                                        $todayminusthree = date('Y-m-d', strtotime('-3 days'));
                                                        $new = (!empty($list['created_at']) && $list['created_at'] <= $todayminusthree ? '':'<span class="new-highlight">New</span>'); 
                                                    ?>
                                                        <div class="<?php echo $color; ?>-card col-task-card-container <?php echo $type !== 'task' ? 'col-job-card-container' : '' ?>">
                                                            <?php if(isset($_SESSION['WEB_USER_ID'])): ?>
                                                            <a href="<?php echo option('site_uri') . url_for("/dashboard") . "?{$type}={$list['slug']}" ?>" class="task-row-taskname">
                                                            <?php else: ?>
                                                            <a href="<?php echo option('site_uri') . url_for('/sign_up') . "?redirect=" . url_for("/dashboard") . "?{$type}={$list['slug']}" ?>" class="task-row-taskname">
                                                            <?php endif ?>
                                                                <div class="recentpost-card">
                                                                    <div class="recentpost-info">
                                                                        <div class="recentpost-cat"><?php echo $list['category'] === 'others'  ? lang('other') : $list['category_name']; ?></div>
                                                                        <div class="recentpost-title"><?php echo $list['title'] ?></div>
                                                                        <div class="task-row-col-left">
                                                                        <?php if( $type === 'task' ): ?>
                                                                            <?php $state = explode(",", $list['state_country']); ?>
                                                                            <div class="recentpost-loc"><?php echo $list['location'] === 'remotely' || $list['location'] === '' ? lang('public_search_working_remotely') : "{$state[0]}"; ?></div>
                                                                            <div class="recentpost-id"><?php echo strtoupper($list['number']) ?></div>
                                                                            <?php echo $new; ?>
                                                                        <?php else: ?>
                                                                            <div class="recentpost-loc"><?php echo "{$list['job_state']}"; ?></div>
                                                                            <div class="recentpost-id"><?php echo strtoupper($list['number']); ?></div>
                                                                            <?php echo $new; ?>
                                                                        <?php endif ?>
                                                                    </div>
                                                                    </div>

                                                                    <div class="amount-val recentpost-amount">
                                                                    <?php if($type === 'task'): ?>
                                                                        <?php $budget = explode('.', $list['budget']) ?>
                                                                        <div class="task-row-taskamount">
                                                                            <span class="amount-prefix">RM</span>
                                                                            <span class="amount-val"><?php echo number_format($budget[0]) ?></span>
                                                                        </div>
                                                                    <?php elseif( $list['type'] === 'internal_job' ): ?>
                                                                        <?php $salary = explode('.', $list['salary_range_max']) ?>
                                                                        <div class="task-row-taskamount">
                                                                            <span class="amount-prefix">RM</span>
                                                                            <span class="amount-val"><?php echo $salary[0] ?></span>
                                                                        </div>
                                                                    <?php else: ?>
                                                                        <?php $salary = $list['salary_range_max'] != 0 ? explode('.', $list['salary_range_max']) :
                                                                            ($list['salary_range_min'] != 0 ? explode('.', $list['salary_range_min']) : 0) ?>

                                                                        <?php if(is_array($salary)): ?>
                                                                            <div class="task-row-taskamount">
                                                                                <span class="amount-prefix">RM</span>
                                                                                <span class="amount-val"><?php echo $salary[0] ?></span>
                                                                            </div>
                                                                        <?php endif ?>
                                                                    <?php endif ?>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                <?php endforeach ?>
                                            <?php else: ?>
												<div class="col-task-row-container">
													<div class="task-row-col-top-flex job">
														<div class="empty-state-container">
															<div class="empty-img"><img src="<?php echo url_for('assets/img/mtp-no-data-2.png') ?>" alt=""></div>
															<span class="empty-lbl"><?php echo lang('public_search_no_result'); ?></span>
														</div>
													</div>
												</div>
                                            <?php endif ?>
											</div>
											<?php if( isset($more_tasks) && $more_tasks ): ?>
											<button class="btn-icon-full btn-load-more more-tasks" data-text="<?php echo lang('search_load_more_loading') ?>">
												<span class="btn-label" id="loadMoreLbl" data-text="<?php echo lang('public_search_load_more'); ?>"><?php echo lang('public_search_load_more'); ?></span>
												<span class="btn-icon load-plus-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                                        </svg>
                                                    </span>
												<span class="btn-icon load-load-icon" style="display:none;">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                            <line x1="12" y1="2" x2="12" y2="6"></line>
                                                            <line x1="12" y1="18" x2="12" y2="22"></line>
                                                            <line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line>
                                                            <line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line>
                                                            <line x1="2" y1="12" x2="6" y2="12"></line>
                                                            <line x1="18" y1="12" x2="22" y2="12"></line>
                                                            <line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line>
                                                            <line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line>
                                                        </svg>
                                                    </span>
											</button>
											<?php endif ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php elseif ($scope === 'talent'): ?>
				<div class="tab-pane fade show <?php echo $scope === 'talent' ? 'active' : '' ?>" id="searchTalentTab" role="tabpanel">
					<div class="col-xl-12">
						<div class="row">
							<div class="col-xl-12 col-tasklisting-top">
								<div class="col-public-search-wrapper">
									<form action="<?php echo url_for('search') ?>" id="searchForm" class=" search-public-form">
										<div class="search-dropdown-filter-top-wrapper form-flex">
											<div class="search-dropdown-filter-container">
												<input type="text" class="form-control form-control-input"
												       name="q" placeholder="<?php echo lang('public_search_talent_by_keywords_placeholder'); ?>"
												       value="<?php echo $search_term ?>"
												       id="inputBox">
											</div>
											<span class="search-dropdown-filter-btn">
                                                <button class="btn-search-public" type="submit">
                                                    <span class="btn-label">Search</span>
                                                    <span class="public-search-icon search-icon">
                                                        <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                                        </svg>
                                                    </span>
                                                </button>
                                            </span>
											<span class="bookmark-dropdown-filter-btn dropdown">
                                                <button class="btn-bookmark dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                                    <span class="btn-label"><?php echo lang('public_search_save_search_button'); ?></span>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-saved-search">
                                                    <div class="modal-advance-filter-container-left">
                                                        <div class="modal-advanced-filter-container">
                                                            <div class="keyword-filter-container">
                                                                <label><?php echo lang('public_search_save_search_title'); ?></label>
                                                                <span data-text="<?php echo lang('public_search_save_search_content'); ?>"><?php echo lang('public_search_save_search_content'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
                                                            <div class="modal-filter-action form-block-filter-flex">
                                                                <button type="button" class="btn-public-sign-up btn-confirm" data-dismiss="modal" data-orientation="next" onclick="location.href='<?php echo option('site_uri') . url_for('sign_up') ?>';">
                                                                    <span class="btn-label"><?php echo lang('public_search_signup'); ?></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </span>
											<span class="bookmark-dropdown-addfilter-btn dropdown">
                                                <button class="btn-addbookmark dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                                    <span class="public-search-icon addsearch-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                                                            <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                                                        </svg>
                                                    </span>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-saved-search dropdown-menu-add-saved-search">
                                                    <div class="modal-advance-filter-container-left">
                                                        <div class="modal-advanced-filter-container">
                                                            <div class="keyword-filter-container">
                                                                <label><?php echo lang('public_search_save_search_title'); ?></label>
                                                                <span data-text="<?php echo lang('public_search_save_search_content'); ?>"><?php echo lang('public_search_save_search_title'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
                                                            <div class="modal-filter-action form-block-filter-flex">
                                                                <button type="button" class="btn-public-sign-up btn-confirm" data-dismiss="modal" data-orientation="next" onclick="location.href='<?php echo option('site_uri') . url_for('sign_up') ?>';">
                                                                    <span class="btn-label"><?php echo lang('public_search_signup'); ?></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </span>
										</div>
										<div class="col-search-terms-result" style="<?php echo (($keywords === '' && $scope === 'talent') || ($keywords === '')) ? 'display:none' : '' ?>">
											<div class="col-search-terms-result-title"><?php echo lang('public_search_keyword_word') ?> :</div>
											<div class="col-search-terms-result-value"><?php echo $scope === 'talent' ? $keywords : '' ?></div>
										</div>
										<div class="search-advanced-filter-wrapper">
											<button class="btn-icon-invert-full btn-advanced-filter" type="button" data-toggle="collapse" data-target="#advancedFilterTaskCollapse" aria-expanded="false" aria-controls="collapseExample">
												<span class="btn-label"><?php echo lang('advanced_filter') ?></span>
												<span class="btn-icon-invert">
														<svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
														<path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
													</svg>
												</span>
											</button>
											<div class="collapse dropdown-menu-advanced-filter" id="advancedFilterTaskCollapse">
												<div class="search-dropdown-filter-btm-wrapper form-flex talents-filter">
											<div class="dropdown dropdown-filter-container filter-dashboard-talentlocation">
												<button class="btn btn-dropdown-label dropdown-toggle <?php echo isset($filters['location_by']) ? 'border-info' : '' ?>" type="button" id="dropdownTalentLocation" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<label class="filter-btn-lbl"><?php echo lang('public_search_filter_location'); ?></label>
												</button>
												<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTalentLocation">
													<div class="form-block-task-filter">
														<div class="form-filter-select-by-location">
															<select class="form-control form-control-input" placeholder="State" name="location_by">
																<option value="" selected><?php echo lang('public_search_filter_location_select_by'); ?></option>
																<option <?php echo isset($filters['location_by']) && $filters['location_by'] === 'locbystate' ? 'selected' : '' ?> value="locbystate"><?php echo lang('public_search_filter_location_select_by_state'); ?></option>
																<option <?php echo isset($filters['location_by']) && $filters['location_by'] === 'locbynearest' ? 'selected' : '' ?> value="locbynearest"><?php echo lang('public_search_filter_location_select_by_nearest'); ?></option>
															</select>
														</div>
														<div class="form-filter-state" style="<?php echo !isset($filters['state']) ? 'display: none' : '' ?>">
															<select class="form-control form-control-input state" name="state" id="listing_state"
															        data-target="city"
                                                                <?php if(isset($filters['state'])):?>
																	data-input-status="true"
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
																    placeholder="State">
																<option value="" selected><?php echo lang('public_search_filter_location_select_by_state'); ?></option>
                                                                <?php foreach($states as $state): ?>
																	<option <?php echo $scope === 'talent' && isset($filters['state']) && $state['id'] === $filters['state'] ? 'selected' : '' ?>
																			value="<?php echo $state['id'] ?>"><?php echo $state['name'] ?></option>
                                                                <?php endforeach; ?>
															</select>
														</div>
														<div class="form-filter-city" style="<?php echo !isset($filters['city']) ? 'display: none' : '' ?>">
															<select class="form-control form-control-input" name="city" id="listing_city"
                                                                <?php if(isset($filters['city'])):?>
																	data-input-status="true"
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
																    placeholder="City">
																<option selected value=""><?php echo lang('public_search_filter_location_select_by_town'); ?></option>
                                                                <?php foreach($cities as $city): ?>
																	<option <?php echo request('scope') === 'talent' && isset($filters['city']) && $city['id'] === $filters['city'] ? 'selected' : '' ?>
																			value="<?php echo $city['id'] ?>">
                                                                        <?php echo $city['name'] ?>
																	</option>
                                                                <?php endforeach; ?>
															</select>
														</div>
														<div class="form-filter-within" style="<?php echo isset($filters['location_by']) && $filters['location_by'] === 'locbynearest' ? '' : 'display: none'; ?>">
															<select class="form-control form-control-input" name="within" placeholder="within">
																<option value="" selected><?php echo lang('public_search_filter_location_select_by_nearest_select_within'); ?></option>
																<option <?php echo isset($filters['within']) && $filters['within'] === '5km' ? 'selected' : '' ?> value="5km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance'); ?> 5km</option>
																<option <?php echo isset($filters['within']) && $filters['within'] === '10km' ? 'selected' : '' ?> value="10km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance'); ?> 10km</option>
																<option <?php echo isset($filters['within']) && $filters['within'] === '20km' ? 'selected' : '' ?> value="20km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance'); ?> 20km</option>
																<option <?php echo isset($filters['within']) && $filters['within'] === '30km' ? 'selected' : '' ?> value="30km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance'); ?> 30km</option>
																<option <?php echo isset($filters['within']) && $filters['within'] === '50km' ? 'selected' : '' ?> value="50km"><?php echo lang('public_search_filter_location_select_by_nearest_select_within_distance'); ?> 50km</option>
															</select>
														</div>
													</div>
												</div>
											</div>
											<div class="dropdown dropdown-filter-container filter-dashboard-talentskills">
												<button class="btn btn-dropdown-label dropdown-toggle <?php echo isset($filters['skills']) ? 'border-info' : '' ?>" type="button" id="dropdownTaskSkills" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<label class="filter-btn-lbl"><?php echo lang('public_search_filter_skills'); ?></label>
												</button>
												<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownTaskSkills">
													<div class="form-block-task-filter">
														<div class="bstags-skills">
															<input type="text" name="skills" id="talent-skills"
                                                                <?php if(request('scope') === 'talent' && isset($filters['skills'])):?>
																	data-input-status="true"
																	checked
																	value="<?php echo implode(',', $filters['skills']) ?>"
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
																   class="bootstrap-tagsinput" placeholder="<?php echo lang('public_search_filter_skills_placeholder'); ?>" />
														</div>
													</div>
												</div>
											</div>
											<div class="dropdown dropdown-filter-container filter-dashboard-completedtask">
												<button class="btn btn-dropdown-label dropdown-toggle <?php echo isset($filters['completed']) ? 'border-info' : '' ?>" type="button" id="dropdownCompletedTask" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<label class="filter-btn-lbl"><?php echo lang('public_search_talent_completed_task'); ?></label>
												</button>
												<div class="dropdown-menu dropdown-search-filter" aria-labelledby="dropdownCompletedTask">
													<div class="form-block-task-filter">
														<div class="custom-control custom-checkbox">
															<input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_1"
                                                                <?php if(request('scope') === 'talent' && isset($filters['completed']) && $filters['completed'] === 'all'):?>
																	data-input-status="true"
																	checked
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
																   value="all" />
															<label class="custom-control-label" for="filtertaskcompleted_1"><?php echo lang('public_search_talent_completed_task_all'); ?></label>
														</div>
														<div class="custom-control custom-checkbox">
															<input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_2"
                                                                <?php if(request('scope') === 'talent' && isset($filters['completed']) && $filters['completed'] === '1-10'):?>
																	data-input-status="true"
																	checked
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
																   value="1-10" />
															<label class="custom-control-label" for="filtertaskcompleted_2">1-10</label>
														</div>
														<div class="custom-control custom-checkbox">
															<input type="checkbox" name="completed" class="custom-control-input" id="filtertaskcompleted_3"
                                                                <?php if(request('scope') === 'talent' && isset($filters['completed']) && $filters['completed'] === '10+'):?>
																	data-input-status="true"
																	checked
                                                                <?php else: ?>
																	data-input-status="false"
                                                                <?php endif; ?>
																   value="10+" />
															<label class="custom-control-label" for="filtertaskcompleted_3">10+</label>
														</div>
													</div>
												</div>
											</div>
										</div>
											</div>
										</div>
									</form>
									<form action="<?php echo url_for('/search') ?>" id="hiddenSearchForm" class=" search-public-form" method="get">
										<input type="hidden" name="scope" value="talent" />
										<input type="hidden" name="q"
                                            <?php if(!is_null($search_term) && ($scope === 'talent' || request('scope') === 'talent')): ?>
												value="<?php echo $search_term ?>"
                                            <?php else: ?>
												disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="perform-search" value="no" disabled />
										<input type="hidden" name="page" disabled value="<?php echo $current_page ?? 1 ?>" />
										<input type="hidden" name="category"
                                            <?php if(isset($filters['category']) ): ?>
												value="<?php echo $filters['category'] ?>"
                                            <?php else: ?>
												disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="location_by"
                                            <?php if(isset($filters['location_by']) ): ?>
												value="<?php echo $filters['location_by'] ?>"
                                            <?php else: ?>
												disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="within"
                                            <?php if(isset($filters['within']) ): ?>
												value="<?php echo $filters['within'] ?>"
                                            <?php else: ?>
												disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="state"
                                            <?php if(isset($filters['state']) ): ?>
												value="<?php echo $filters['state'] ?>"
                                            <?php else: ?>
												disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="city"
                                            <?php if(isset($filters['city']) ): ?>
												value="<?php echo $filters['city'] ?>"
                                            <?php else: ?>
												disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="skills"
                                            <?php if(isset($filters['skills']) ): ?>
												value="<?php echo implode(',', $filters['skills']) ?>"
                                            <?php else: ?>
												disabled
                                            <?php endif ?>
										/>
										<input type="hidden" name="completed"
                                            <?php if(isset($filters['completed']) ): ?>
												value="<?php echo $filters['completed'] ?>"
                                            <?php else: ?>
												disabled
                                            <?php endif ?>
										/>
									</form>
								</div>
								<div class="col-task-listing-wrapper">
									<div class="row">
										<div class="col-xl-9 col-talent-row-wrapper">
											<div class="talent-result-container">
                                                <?php if(!empty($listing)): ?>
                                                    <?php foreach($listing as $talent): ?>
	                                                    <div class="col-talent-row-container">
		                                                    <div class="talent-row-col-top-flex">
			                                                    <div class="talent-row-col-left">
				                                                    <div class="talent-personal-info-container">
					                                                    <div class="talent-avatar-container">
						                                                    <div class="profile-avatar">
							                                                    <a href="<?php echo url_for("sign_up") ?>" class="talent-avatar-link">
								                                                    <img class="avatar-img" src="<?php echo $talent['photo'] ?>" alt="">
							                                                    </a>
						                                                    </div>
					                                                    </div>
					                                                    <div class="talent-personal-info">
						                                                    <a href="<?php echo url_for("sign_up") ?>" class="talent-name-link">
							                                                    <h5 class="profile-name"><?php echo hash_name(strtolower($talent['firstname'] . ' ' . $talent['lastname'])); ?></h5>
						                                                    </a>
						                                                    <div class="talent-details-profile-location">
														                        <span class="talent-details-profile-location-icon">
															                        <svg viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor"
															                             xmlns="http://www.w3.org/2000/svg">
														                                <path fill-rule="evenodd"
														                                      d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z">
														                                </path>
														                            </svg>
														                        </span>
							                                                    <span class="talent-details-profile-location-val">
															                        <?php echo ($talent['state'] && $talent['country']) ? "{$talent['state']}, {$talent['country']}" : 'N/A' ?>
														                        </span>
						                                                    </div>
						                                                    <div class="task-row-rating-flex">
							                                                    <div class="task-row-rating-star">
                                                                                    <?php $starred = (int)floor($talent['rating']['overall']/2); $rest = 5 - $starred; ?>
                                                                                    <?php if($starred > 0): ?>
                                                                                        <?php foreach (range(1, $starred) as $star): ?>
										                                                    <span class="rating-star-icon active"><i class="fa fa-star"></i></span>
                                                                                        <?php endforeach; ?>
                                                                                    <?php endif; ?>
                                                                                    <?php if($rest > 0): ?>
                                                                                        <?php foreach (range(1, $rest) as $star1): ?>
										                                                    <span class="rating-star-icon"><i class="fa fa-star"></i></span>
                                                                                        <?php endforeach; ?>
                                                                                    <?php endif; ?>
							                                                    </div>
							                                                    <div class="task-row-rating-val">
														                            <span class="task-row-average-rating-val">
															                            <?php echo number_format((float)$talent['rating']['overall'], 1); ?>
														                            </span>
								                                                    <span class="task-row-divider">/</span>
								                                                    <span class="task-row-total-rating-val">
															                            <?php echo $talent['rating']['count'] . "&nbsp;" . lang('public_search_review'); ?>
														                            </span>
							                                                    </div>
						                                                    </div>
					                                                    </div>
				                                                    </div>
				                                                    <div class="talent-about-container">
                                                                        <?php echo strlen($talent['about']) > 235 ? (substr($talent['about'], 0, 235) . ' ...') : $talent['about']; ?>
				                                                    </div>
				                                                    <div class="talent-row-skills">
                                                                        <?php if( !empty($talent['skills']) ): ?>
						                                                    <div class="task-details-deadline-block">
                                                                                <?php foreach ($talent['skills_list'] as $skill): ?>
								                                                    <a href="<?php echo url_for('search') . "?scope=talent&skills=" . $skill['id'] ?>" class="skills-label-link">
									                                                    <span class="tag label label-info bubble-lbl"><?php echo $skill['name'] ?></span>
								                                                    </a>
                                                                                <?php endforeach ?>
						                                                    </div>
                                                                        <?php endif ?>
				                                                    </div>
			                                                    </div>
			                                                    <div class="talent-row-col-right">
				                                                    <div class="talent-row-actions">
					                                                    <button type="button" class="btn-icon-full btn-view-profile"
					                                                            onclick="location.href='<?php echo url_for("sign_up") ?>';">
						                                                    <span class="btn-label"><?php echo lang("public_search_talent_view_profile") ?></span>
						                                                    <span class="btn-icon">
														                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
														                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
														                            <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
														                            <polyline points="22 4 12 14.01 9 11.01"></polyline>
														                        </svg>
														                    </span>
					                                                    </button>
				                                                    </div>
				                                                    <div class="talent-activity-tracking-details">
					                                                    <div class="talent-total-completed-task-hour">
						                                                    <div class="talent-extra-info talent-total-completed-task">
							                                                    <span class="talent-profile-label"><?php echo lang('public_search_talent_completed_task'); ?>:</span>
							                                                    <span class="talent-profile-val"><?php echo $talent['info']['completed'] ?></span>
						                                                    </div>
						                                                    <div class="talent-extra-info  talent-total-completed-hour">
							                                                    <span class="talent-profile-label"><?php echo lang('public_search_talent_total_hours_completed'); ?>:</span>
							                                                    <span class="talent-profile-val">
															                        <?php echo (int)$talent['info']['hours_completed'] ?>
														                        </span>
						                                                    </div>
					                                                    </div>
					                                                    <div class="talent-extra-info talent-history">
						                                                    <div class="talent-last-login">
							                                                    <span class="talent-profile-label"><?php echo lang('public_search_talent_last_login'); ?>:</span>
							                                                    <span class="talent-profile-val"><?php echo $talent['info']['last_login'] ?></span>
						                                                    </div>
						                                                    <div class="talent-extra-info talent-member-since">
							                                                    <span class="talent-profile-label"><?php echo lang('public_search_talent_member_since'); ?>:</span>
							                                                    <span class="talent-profile-val"><?php echo $talent['member_since'] ?></span>
						                                                    </div>
					                                                    </div>
				                                                    </div>
			                                                    </div>
		                                                    </div>
	                                                    </div>
                                                    <?php endforeach; ?>
                                                <?php else: ?>
													<div class="col-task-row-container">
														<div class="task-row-col-top-flex job">
															<div class="empty-state-container">
																<div class="empty-img"><img src="<?php echo url_for('assets/img/mtp-no-data-2.png') ?>" alt=""></div>
																<span class="empty-lbl"><?php echo lang('public_search_no_result'); ?></span>
															</div>
														</div>
													</div>
                                                <?php endif ?>
											</div>
                                            <?php if(isset($more_talents) && $more_talents): ?>
											<button class="btn-icon-full btn-load-more more-talents" data-text="<?php echo lang('search_load_more_loading') ?>">
												<span class="btn-label" id="loadMoreLbl" data-text="<?php echo lang('public_search_load_more'); ?>"><?php echo lang('public_search_load_more'); ?></span>
												<span class="btn-icon load-plus-icon">
				                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
				                                </span>
												<span class="btn-icon load-load-icon" style="display:none;">
				                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="2" x2="12" y2="6"></line><line x1="12" y1="18" x2="12" y2="22"></line><line x1="4.93" y1="4.93" x2="7.76" y2="7.76"></line><line x1="16.24" y1="16.24" x2="19.07" y2="19.07"></line><line x1="2" y1="12" x2="6" y2="12"></line><line x1="18" y1="12" x2="22" y2="12"></line><line x1="4.93" y1="19.07" x2="7.76" y2="16.24"></line><line x1="16.24" y1="7.76" x2="19.07" y2="4.93"></line></svg>
				                                </span>
											</button>
                                            <?php endif ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endif ?>
			</div>
		</div>
	</div>
</div>
<template id="loading-container">
	<div class="col-task-row-container" style="min-height:310px;height:310px;">
		<div class="task-row-col-top-flex job">
			<div class="empty-state-container">
				<div class="empty-img"><img src="<?php echo url_for('assets/img/mtp-no-data-2.png') ?>" alt=""></div>
				<span class="empty-lbl"><?php echo lang('public_search_loading_search_result'); ?></span>
			</div>
		</div>
	</div>
</template>

<?php content_for('scripts'); ?>
<script>

    <?php if(isset($filters['type']) && in_array('job', $filters['type'])): ?>
    $('.tasks-filter .categories-input').html( $('.tasks-filter #job-categories').html() );
    <?php else: ?>
    $('.tasks-filter .categories-input').html( $('.tasks-filter #task-categories').html() );
    <?php endif ?>

	$('#inputBox').on('keyup', function(e){
	    var q = $(e.currentTarget);
	    if(q.val().length )
	        q.closest('form').next('form').find('input[name="q"]').prop('disabled', false).val( q.val() );
	    else
            q.closest('form').next('form').find('input[name="q"]').prop('disabled', true).val('');
	});

    $('input[name="category"]').on('click', function (e) {
        var target = $(e.currentTarget);
        if (target.val() === '0') {
            target.parent().siblings().find('[type="checkbox"]').prop('checked', false);
        } else {
            target.parent().siblings().find('.select-all-category').prop('checked', false);
        }
    });

    $('input[name="completed"]').on('click', function(e){
        $(e.currentTarget).parent().siblings().find('input[name="completed"]').prop('checked', false);
        $(e.currentTarget).prop('checked', true);
    });

    $('input[name="type"]').on('click', function(e){
        if( !e.currentTarget.checked ) {
            e.preventDefault();
            return;
        }
        var loading = $('#loading-container').html();
        $('.task-result-container').html(loading);
        $(".btn-load-more.more-tasks").removeAttr('disabled').hide();
        $(e.currentTarget).parent().siblings().find('input[name="type"]').prop('disabled', true);
        $(e.currentTarget).parent().siblings().find('input[name="type"]').prop('checked', false);
        $(e.currentTarget).prop('checked', true);
        $(e.currentTarget).closest('form').find('input[name="category"]').prop('checked', false).data('inputStatus', false);
        $(e.currentTarget).closest('form').next('form').find('input[name="category"]').val('').prop('disabled', true);
        if( $(e.currentTarget).val() === 'task' ){
            $('.tasks-filter .filter-dashboard-taskcategory, .tasks-filter div.filter-dashboard-taskdate').removeAttr('style');
            $('.tasks-filter .categories-input').html( $('.tasks-filter #task-categories').html() );
            /*$('.tasks-filter .filter-dashboard-tasklocation')
                .find('select[name="location_by"]').val('').children(':last').prop('disabled', false);
            $('.tasks-filter .form-filter-city, .tasks-filter .form-filter-within').hide();*/
        }else if( $(e.currentTarget).val() === 'job' ){
            $('.tasks-filter .filter-dashboard-taskcategory, .tasks-filter div.filter-dashboard-taskdate').removeAttr('style');
            $('.tasks-filter .categories-input').html( $('.tasks-filter #job-categories').html() );
            /*$('.tasks-filter .filter-dashboard-tasklocation')
                .find('select[name="location_by"]').val('').children(':last').prop('disabled', false);
            $('.tasks-filter .form-filter-city, .tasks-filter .form-filter-within').hide();*/
        }else{
            $('.tasks-filter .filter-dashboard-taskcategory, .tasks-filter .filter-dashboard-taskdate').hide();
            /*$('.tasks-filter .filter-dashboard-tasklocation')
	            .find('select[name="location_by"]').val('').children(':last').prop('disabled', true);
            $('.tasks-filter .form-filter-city, .tasks-filter .form-filter-within').hide();*/
        }

        attachChangeEvents( $('.categories-input') );
    });

    function attachChangeEvents(elements = null) {
        if(elements === null){
            elements = $('.tasks-filter');
        }
        elements.find('input, select').off('change');
        elements.find('input, select').on('change', function (e) {
            $('#hiddenSearchForm input[name="page"]').val("1");
            if ($(e.currentTarget).attr('name') !== 'date_type') {
                if ($(e.currentTarget).attr('name') === 'date_period' && $(e.currentTarget).val() !== '') {
                    if ($(e.currentTarget).val() !== 'customrange') {
                        $('#hiddenSearchForm input[name="perform-search"]').val('yes');
                        update_task_filters($(e.currentTarget));
                    } else {
                        $('#hiddenSearchForm input[name="perform-search"]').val('no');
                        update_task_filters($(e.currentTarget));
                    }
                } else {
                    if ($(e.currentTarget).attr('name') === 'type') {
                        if ($(e.currentTarget).val() === 'job' && $(e.currentTarget).prop('checked')) {
                            $('.task-filter-row-nature, .task-filter-row-budget').addClass('hide').find('input, select').prop('disabled', true);
                            $('[name="date_type"]').children().last().text('<?php echo lang('public_search_closing_date'); ?>').val('closeDate');
                            $('#hiddenSearchForm input[name="perform-search"]').val('yes');
                        } else if ($(e.currentTarget).val() === 'task' && $(e.currentTarget).prop('checked')) {
                            $('.task-filter-row-nature, .task-filter-row-budget').removeClass('hide').find('input, select').prop('disabled', false);
                            $('[name="date_type"]').children().last().text('<?php echo lang('public_search_start_date'); ?>').val('startDate');
                            $('#hiddenSearchForm input[name="perform-search"]').val('yes');
                        } else {
                            $('.task-filter-row-nature, .task-filter-row-budget').addClass('hide').find('input, select').prop('disabled', true);
                            $('[name="date_type"]').children().last().text('<?php echo lang('public_search_start_date'); ?>').val('startDate');
                            $('#hiddenSearchForm input[name="perform-search"]').val('yes');
                        }
                    } else if ($(e.currentTarget).attr('name') === 'location_by' && $(e.currentTarget).val() === 'locbynearest') {
                        $(e.currentTarget).closest('form').find('select[name="state"], select[name="city"]').val('');
                        $('#hiddenSearchForm input[name="perform-search"]').val('no');
                    } else if ($(e.currentTarget).attr('name') === 'location_by' && $(e.currentTarget).val() === 'locbystate') {
                        $(e.currentTarget).closest('form').find('select[name="within"]').val('');
                        $('#hiddenSearchForm input[name="perform-search"]').val('no');
                    } else if ($(e.currentTarget).attr('name') === 'location_by' && $(e.currentTarget).val() === '') {
                        $(e.currentTarget).closest('form').find('select[name="state"], select[name="city"], select[name="within"]').val('');
                        $('#hiddenSearchForm input[name="perform-search"]').val('yes');
                    } else {
                        $('#hiddenSearchForm input[name="perform-search"]').val('yes');
                    }
                    update_task_filters($(e.currentTarget));
                }
            } else {
                if ($(e.currentTarget).attr('name') === 'date_type' && $(e.currentTarget).val() === '') {
                    $(e.currentTarget).closest('form').find('select[name="from"], select[name="to"]').val('');
                    $('#hiddenSearchForm input[name="perform-search"]').val('yes');
                } else if ($(e.currentTarget).attr('name') === 'date_type' && $('#searchForm input[name="from"]').val().length && $('#searchForm input[name="to"]').val().length) {
                    $('#hiddenSearchForm input[name="perform-search"]').val('yes');
                } else {
                    $('#hiddenSearchForm input[name="perform-search"]').val('yes');
                }
                update_task_filters($(e.currentTarget));
            }
        });

        $('input[name="category"]').on('click', function (e) {
            var target = $(e.currentTarget);
            if (target.val() === '0') {
                target.parent().siblings().find('[type="checkbox"]').prop('checked', false);
            } else {
                target.parent().siblings().find('.select-all-category').prop('checked', false);
            }
        });
    }

    attachChangeEvents();

    $('#form_datetime_search_filter_from').datetimepicker({
        format: 'DD-MMM-YYYY',
        useCurrent: false
    });

    $('#form_datetime_search_filter_to').datetimepicker({
        format: 'DD-MMM-YYYY',
        useCurrent: false
    });

    $('#form_datetime_search_filter_from').on("change.datetimepicker", function (e) {
        $('#form_datetime_search_filter_from').datetimepicker("maxDate", moment());
        $('#form_datetime_search_filter_to').datetimepicker("maxDate", moment());
    });

    $('#form_datetime_search_filter_from').on("hide.datetimepicker", function (e) {
        var nextDay = moment(e.date).add(1, 'days');
        $('#form_datetime_search_filter_to').datetimepicker("minDate", nextDay);
    });

    $('#form_datetime_search_filter_to').on("hide.datetimepicker", function (e) {
        var prevDay = moment(e.date).subtract(1, 'days');
        $('#form_datetime_search_filter_from').datetimepicker("maxDate", prevDay);
    });

    $('#form_datetime_search_filter_from').on('hide.datetimepicker', function(e){
        var target = $(e.currentTarget).find('input');
        target.data('inputStatus', true);
        if( $('input[name="to"]').val().length ) {
            $('#hiddenSearchForm input[name="perform-search"]').val('yes');
            update_task_filters(target);
            target = null;
        }
    });

    $('#form_datetime_search_filter_to').on('hide.datetimepicker', function(e){
        var target = $(e.currentTarget).find('input');
        target.data('inputStatus', true);
        update_task_filters(target);
        target = null;
        $('#hiddenSearchForm input[name="perform-search"]').val('yes');
        update_task_filters($('input[name="from"]'));
    });

    $('#form_datetime_search_filter_from, #form_datetime_search_filter_from_start, #job_form_datetime_search_filter_from, #job_form_datetime_search_filter_from_start, #form_datetime_search_filter_to, #form_datetime_search_filter_to_start, #job_form_datetime_search_filter_to, #job_form_datetime_search_filter_to_start').datetimepicker('destroy');
    $('#form_datetime_search_filter_from, #form_datetime_search_filter_to, #job_form_datetime_search_filter_from, #job_form_datetime_search_filter_to').datetimepicker({
        format: 'DD-MMM-YYYY',
        useCurrent: false,
        allowInputToggle: true,
        maxDate: moment()
    });

    $('#form_datetime_search_filter_from_start, #form_datetime_search_filter_to_start, #job_form_datetime_search_filter_from_start, #job_form_datetime_search_filter_to_start').datetimepicker({
        format: 'DD-MMM-YYYY',
        useCurrent: false,
        allowInputToggle: true
    });

    $('#form_datetime_search_filter_from, #job_form_datetime_search_filter_from').on("change.datetimepicker", function (e) {
        $('#form_datetime_search_filter_from, #job_form_datetime_search_filter_from').datetimepicker("maxDate", moment());
        $('#form_datetime_search_filter_to, #job_form_datetime_search_filter_to').datetimepicker("maxDate", moment());
    });

    $('#form_datetime_search_filter_from, #job_form_datetime_search_filter_from').on("hide.datetimepicker", function (e) {
        var nextDay = moment(e.date).add(1, 'days');
        $('#form_datetime_search_filter_to, #job_form_datetime_search_filter_to').datetimepicker("minDate", nextDay);
    });

    $('#form_datetime_search_filter_to, #job_form_datetime_search_filter_to').on("hide.datetimepicker", function (e) {
        var prevDay = moment(e.date).subtract(1, 'days');
        $('#form_datetime_search_filter_from, #job_form_datetime_search_filter_from').datetimepicker("maxDate", prevDay);
    });

    $('#form_datetime_search_filter_from, #form_datetime_search_filter_from_start, #job_form_datetime_search_filter_from, #job_form_datetime_search_filter_from_start').on('hide.datetimepicker', function(e){
        var target = $(e.currentTarget).find('input');
        target.data('inputStatus', true);
        if( $('input[name="to"]').val().length ) {
            $('#hiddenSearchForm input[name="perform-search"]').val('yes');
            update_task_filters(target);
            target = null;
        }
        /*var current_form = $(e.currentTarget).closest('form');
        var target_form = current_form.next('form');
        target.data('inputStatus', true);
        update_filters(target, current_form, target_form);
        target = null;*/
    });

    $('#form_datetime_search_filter_to, #form_datetime_search_filter_to_start, #job_form_datetime_search_filter_to, #job_form_datetime_search_filter_to_start').on('hide.datetimepicker', function(e){
        var target = $(e.currentTarget).find('input');
        update_task_filters(target);
        target = null;
        $('#hiddenSearchForm input[name="perform-search"]').val('yes');
        update_task_filters($('input[name="from"]'));
        /*var current_form = $(e.currentTarget).closest('form');
        var target_form = current_form.next('form');
        target.data('inputStatus', true);
        update_filters(target, current_form, target_form);
        target = null;*/
    });

    $('#filter-search').on('click', function(){
        var search_form = $('#searchTaskTab #hiddenSearchForm').clone();
        search_form.find('input[name="search_name"]').remove();
        search_form[0].submit();
    });

    $('.reset-task-filters').on('click', function(e){
        e.preventDefault();
        var q = $('#searchForm input[name="q"]').val();
        $("#searchForm button.btn-link-filter.is-clicked").trigger('click');
        $('#searchForm #task-skills').tagsinput('removeAll');
        document.getElementById('searchForm').reset();
        $('.tasks-filter input').val('');
        if(q.length) q = '&q=' + q;
        window.location = "<?php echo option('site_uri') . url_for('/search') . '?' ?>"+q;
    })

    <?php if(isset($filters['date_type']) && !empty($filters['date_type'])): ?>
    $('#hiddenSearchForm input[name="perform-search"]').val('no');
    <?php endif ?>

    <?php if(isset($filters['date_period']) && !empty($filters['date_period'])): ?>
    <?php if($filters['date_period'] === 'customrange'): ?>
    $("#search_filter_dateposted").parent().next().show();
    <?php endif ?>
    $('#hiddenSearchForm input[name="perform-search"]').val('no');
    <?php endif ?>

    var skills = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '<?php echo option('site_uri') . url_for('ajax/skills.json') ?>?all=1'
    });
    skills.clearPrefetchCache();
    skills.initialize();

    $('#task-skills').tagsinput({
        allowDuplicates: false,
        confirmKeys: [9, 13, 44, 188],
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'skills',
            displayKey: 'name',
            source: skills.ttAdapter()
        }
    });

    $('#talent-skills').tagsinput({
        allowDuplicates: false,
        confirmKeys: [9, 13, 44, 188],
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'skills',
            displayKey: 'name',
            source: skills.ttAdapter()
        }
    });

    <?php if(isset($filters['skills']) && !empty($filters['skills']) && $scope === 'task'): ?>
    <?php if(!is_array($filters['skills'])) $filters['skills'] = [$filters['skills']]; ?>
    $('.tasks-filter #task-skills').off('change');
    <?php echo json_encode($filters['skills_list']) ?>.forEach(function(item, idx){
        $('.tasks-filter #task-skills').tagsinput('add', item);
    });
    <?php endif ?>

    <?php if(isset($filters['skills']) && !empty($filters['skills']) && $scope === 'talent'): ?>
    <?php if(!is_array($filters['skills'])) $filters['skills'] = [$filters['skills']]; ?>
    $('#talent-skills').off('change');
    <?php echo json_encode($filters['skills_list']) ?>.forEach(function(item, idx){
        $('#talent-skills').tagsinput('add', item);
    });
    <?php endif ?>

    $('#task-skills, #talent-skills').on('change', function(e){
        if( $(e.currentTarget).attr('name') !== 'date_type' ) {
            if( $(e.currentTarget).attr('name') === 'date_period' && $(e.currentTarget).val() !== '' ){
                if( $(e.currentTarget).val() !== 'customrange' ){
                    $('#hiddenSearchForm input[name="perform-search"]').val('yes');
                }
            }else {
                $('#hiddenSearchForm input[name="perform-search"]').val('yes');
            }
        }
        $('#hiddenSearchForm input[name="page"]').val("1");
        update_task_filters($(e.currentTarget));
    });


    $(".btn-load-more.more-tasks").on('click', function(e){
        $(e.currentTarget).prop('disabled', true);
        var page = $('#hiddenSearchForm input[name="page"]').val();
        $('#hiddenSearchForm input[name="page"]').val(parseInt(page)+1);
        get_result('load_more');
    });

    $('#searchForm input[name="search_name"]').keydown(function(e){
        if(e.keyCode === 13){
            $('.save-search').trigger('click');
            return false;
        }
    })

    $('#searchForm').on('submit', function(e){
        $('input[name="search_name"]').attr('disabled', 'disabled');
        var target_form = $(e.currentTarget).next('form');
        target_form[0].submit();
        e.preventDefault();
        return false;
    });

    function update_task_filters(target){
        if(target === undefined){
            var inputs = $('.tasks-filter').find('input, select');
            var target_form = $(inputs[0]).closest('form').next('form');
        }else{
            var inputName = target.attr('name');
            var inputs = $('.tasks-filter').find('[name="'+inputName+'"]');
            var target_form = $(inputs).closest('form').next('form');
        }

        var taskType = '';
        var scope = '';
        var categories = '';
        var value = '';
        var input = target_form.find('input[name="'+inputName+'"]');

        if(inputName === 'location_by' && !$(inputs).val().length){
            input = $(target_form).find('input[name="location_by"], input[name="state"], input[name="city"], input[name="within"]');
        }else if(inputName === 'location_by' && $(inputs).val() === 'locbystate'){
            $(target_form).find('input[name="within"]')
                .val('').prop('disabled', true);
        }else if(inputName === 'location_by' && $(inputs).val() === 'locbynearest' ){
            <?php if( !isset($_SESSION['current_user_location']) ): ?>
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (location) {
                    latlng = {lat: location.coords.latitude, lng: location.coords.longitude};
                    if( window.user_location === undefined ) {
                        $.ajax({
                            url: "<?php echo option("site_uri") . url_for("/search/savelocation") ?>",
                            method: 'POST',
                            data: {'location': latlng}
                        }).done(function (response) {
                            window.user_location = true;
                            //document.location.href = document.location.href;
                        })
                    }
                }, function(){
                    $(inputs).val('').trigger('change');
                    t('w', '<?php echo lang('please_allow_for_location_to_get_nearby_result') ?>', '<?php echo lang('warning') ?>');//alert("Please click Allow for location to get nearby result");
                    return false;
                });
            }
            <?php endif ?>
        }

        if( inputName === 'date_type' && ! $(inputs).val().length ){
            input = $(target_form).find('input[name="date_type"], input[name="date_period"], input[name="from"], input[name="to"]');
        }

        if(inputName === 'within'){
            $(target_form).find('input[name="state"], input[name="city"]')
                .val('').prop('disabled', true);
        }

        inputs.each(function(idx, item){
            if(
                ((item.type === 'checkbox' && item.checked) ||
                    (item.type === 'text' && item.value !== '') ||
                    (item.type === 'select-one' && item.value !== ''))
            ) {
                if(item.name === 'type' || item.name === 'category' || item.name === 'task_type'){
                    if(item.name === 'category') {
                        if(categories === '')
                            categories += item.value;
                        else
                            categories +=','+item.value;
                    }else if(item.name === 'type') {
                        if(scope === '')
                            scope += item.value;
                        else
                            scope +=','+item.value;
                    }else{
                        if(taskType === '')
                            taskType += item.value;
                        else
                            taskType +=','+item.value;
                    }
                }else {
                    if(item.name === 'category') categories = item.value;
                    else if(item.name === 'type') scope = item.value;
                    else if(item.name === 'task_type') taskType = item.value;
                    else value = item.value;
                }

                if(scope !== '') value = scope;
                else if(categories !== '') value = categories;
                else if(taskType !== '') value = taskType;

                if(value.length) {
                    input.removeAttr('disabled');
                    input.val(value);
                    if(target !== undefined) target.data('inputStatus', true);
                    else $(item).data('inputStatus', true);
                }else{
                    input.val('');
                    input.attr('disabled', 'disabled');
                    if(target !== undefined) target.data('inputStatus', false);
                    else $(item).data('inputStatus', false);
                }
            }else{
                if(!value.length){
                    input.val('');
                    input.attr('disabled', 'disabled');
                    if(target !== undefined) target.data('inputStatus', false);
                    else $(item).data('inputStatus', false);
                }
            }
        });

        if(target_form.find('input[name="perform-search"]').val() === 'yes')
            get_result();
    }

    function request_talents(url, data, load_more = null, update = null){

        $.ajax({
            url: url,
            data: data,
        }).done(function(response){
            if(response.more){
                $(".btn-load-more.more-talents").removeAttr('disabled').show();
            }else{
                $(".btn-load-more.more-talents").removeAttr('disabled').hide();
            }
            if(load_more === null) {
                $('.talent-result-container').html(response.html);
            } else {
                var currentHeight = $('.talent-result-container').height();
                $('.talent-result-container').append(response.html);
                $('html')[0].scrollTo({ top: currentHeight+200, left: 0, behavior: 'smooth'});
            }
            if(response.keywords !== ''){
                $('#searchTalentTab .col-search-terms-result-value').text(response.keywords);
                $('#searchTalentTab .keyword-filter-container > span').text(response.keywords);
                $('#searchTalentTab .col-search-terms-result').show();
            }else{
                var text = $('#searchTalentTab .keyword-filter-container > span').data('text');
                $('#searchTalentTab .keyword-filter-container > span').text( text );
                $('#searchTalentTab .col-search-terms-result').hide();
            }
            if( $('.col-task-row-wrapper').css('display') === 'none' )
                $('.col-task-row-wrapper').fadeIn();

            var stateUrl = data.length > 0 ? url + '?' + data : url;
            stateUrl = decodeURI(stateUrl);
            url = decodeURI(url);
            if (update === null) {
                history.pushState({
                    'talentHtml': response.html,
                    'talentUrl': url,
                    'talentData': data
                }, '', stateUrl);
            }else {
                history.replaceState({
                    'talentHtml': response.html,
                    'talentUrl': url,
                    'talentData': data
                }, '', stateUrl);
            }
        });
    }

    function get_result(load_more = null){
        if( window.requested_result !== undefined ) return;
        window.requested_result = true;
        var search_form = $('#hiddenSearchForm').clone();
        search_form.find('input[name="search_name"]').remove();

        var page = $('#hiddenSearchForm input[name="page"]').val(),
            url = "<?php echo option('site_uri') . url_for('/search') ?>/"+page,
            data = search_form.serialize();
	    <?php if( $scope === 'task' ): ?>
        $.ajax({
            url: url,
            data: data,
        }).done(function(response){
            $('#searchForm').find('input[name="type"]').removeAttr('disabled');
            if(response.keywords !== ''){
                $('.col-search-terms-result-value').text(response.keywords);
                $('.keyword-filter-container > span').text(response.keywords);
                $('.col-search-terms-result').show();
            }else{
                var text = $('.keyword-filter-container > span').data('text');
                $('.keyword-filter-container > span').text( text );
                $('.col-search-terms-result').hide();
            }

            if(response.more){
                $(".btn-load-more.more-tasks").removeAttr('disabled').show();
            }else{
                $(".btn-load-more.more-tasks").removeAttr('disabled').hide();
            }

            if(load_more === null) {
                $('.task-result-container').html(response.html);
            } else {
                var currentHeight = $('.task-result-container').height();
                $('.task-result-container').append(response.html);
                $('html')[0].scrollTo({ top: currentHeight+200, left: 0, behavior: 'smooth'});
            }

            window.history.pushState({'taskHtml': response.html}, '', url + '?' + data);
            window.requested_result = undefined;
        });
        <?php elseif ($scope === 'talent'): ?>
        request_talents(url, data, load_more);
	    <?php endif ?>
    }

    $('.talents-filter').find('input, select').on('change', function(e){
        $('#hiddenSearchForm input[name="page"]').val("1");
        $('#hiddenSearchForm input[name="perform-search"]').val('yes');
        update_talents_filters($(e.currentTarget));
    });


    $(".btn-load-more.more-talents").on('click', function(e){
        $(e.currentTarget).prop('disabled', true);
        var page = $('#hiddenSearchForm input[name="page"]').val();
        $('#hiddenSearchForm input[name="page"]').val(parseInt(page)+1);
        get_talents_result('load_more');
    });

    $('#hiddenSearchForm input[name="search_name"]').keydown(function(e){
        if(e.keyCode === 13){
            return false;
        }
    })

    $('#hiddenSearchForm').on('submit', function(){
        $('input[name="search_name"]').prop('disabled', true);
    });


    function update_talents_filters(target){
        if(target === undefined){
            var inputs = $('.talents-filter').find('input, select');
            var target_form = $(inputs[0]).closest('form').next('form');
        }else{
            var inputName = target.attr('name');
            var inputs = $('.talents-filter').find('[name="'+inputName+'"]');
            var target_form = $(inputs).closest('form').next('form');
        }

        var value = '';
        var input = target_form.find('input[name="'+inputName+'"]');

        if(inputName === 'location_by' && !$(inputs).val().length){
            input = $(target_form).find('input[name="location_by"], input[name="state"], input[name="city"], input[name="within"]');
        }else if(inputName === 'location_by' && $(inputs).val() === 'locbystate'){
            $(target_form).find('input[name="within"]')
                .val('').prop('disabled', true);
        }else if(inputName === 'location_by' && $(inputs).val() === 'locbynearest' ){
            <?php if( !isset($_SESSION['current_user_lcation']) ): ?>
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (location) {
                    latlng = {lat: location.coords.latitude, lng: location.coords.longitude};
	                if( window.user_location === undefined ) {
                        $.ajax({
                            url: "<?php echo option("site_uri") . url_for("/search/savelocation") ?>",
                            method: 'POST',
                            data: {'location': latlng}
                        }).done(function (response) {
                            window.user_location = true;
                            //document.location.href = document.location.href;
                        })
                    }
                }, function(){
                    alert("Please click Allow for location to get nearby result");
                });
            }
            <?php endif ?>
        }

        if( inputName === 'date_type' && ! $(inputs).val().length ){
            input = $(target_form).find('input[name="date_type"], input[name="date_period"], input[name="from"], input[name="to"]');
        }

        if(inputName === 'within'){
            $(target_form).find('input[name="state"], input[name="city"]')
                .val('').prop('disabled', true);
        }

        inputs.each(function(idx, item){
            if( (item.type === 'checkbox' && item.checked) || (item.type === 'text' && item.value !== '') || (item.type === 'select-one' && item.value !== '') ){
                value = item.value;
                if(value.length) {
                    input.prop('disabled', false);
                    input.val(value);
                    if(target !== undefined) target.data('inputStatus', true);
                    else $(item).data('inputStatus', true);
                }else{
                    input.val('');
                    input.prop('disabled', true);
                    if(target !== undefined) target.data('inputStatus', false);
                    else $(item).data('inputStatus', false);
                }
            }else{
                if(!value.length){
                    input.val('');
                    input.attr('disabled', 'disabled');
                    if(target !== undefined) target.data('inputStatus', false);
                    else $(item).data('inputStatus', false);
                }
            }
        });

        if(target_form.find('input[name="perform-search"]').val() === 'yes')
            get_talents_result();
    }

    function get_talents_result(load_more = null){
        var search_form = $('#hiddenSearchForm').clone();
        search_form.find('input[name="search_name"]').remove();

        var page = $('#hiddenSearchForm input[name="page"]').val(),
            url = "<?php echo option('site_uri') . url_for('/search') ?>/"+page,
            data = search_form.serialize();

        $.ajax({
            url: url,
            data: data,
        }).done(function(response){
            if(response.more){
                $(".btn-load-more.more-talents").removeAttr('disabled').show();
            }else{
                $(".btn-load-more.more-talents").removeAttr('disabled').hide();
            }
            if(load_more === null) {
                $('.talent-result-container').html(response.html);
            } else {
                var lastChildHeight = $('.talent-result-container .col-talent-row-container:last-child').height();
                var currentHeight = $('.talent-result-container').height();
                $('.talent-result-container').append(response.html);
                $('html')[0].scrollTo({ top: currentHeight+lastChildHeight - 24, left: 0, behavior: 'smooth'});
            }
            if(response.keywords !== ''){
                $('.col-search-terms-result-value').text(response.keywords);
                $('.keyword-filter-container > span').text(response.keywords);
                $('.col-search-terms-result').show();
            }else{
                var text = $('.keyword-filter-container > span').data('text');
                $('.keyword-filter-container > span').text( text );
                $('.col-search-terms-result').hide();
            }
            window.history.pushState({'talentHtml': response.html}, '', url + '?' + data);
        });
    }

    <?php if( $scope === 'task' ): ?>
    window.onpopstate = function(e){
        if(e.state){
            $('.col-task-listing-wrapper .col-task-row-wrapper').html(e.state.taskHtml);
        }
    };
    <?php elseif( $scope === 'talent' ): ?>
    window.addEventListener('popstate', function(e){
        if(e.state){
            $('.talent-result-container').html(e.state.talentHtml);
            request_talents(e.state.talentUrl, e.state.talentData, null, 'update');
        }
    })
    <?php endif ?>

    $('.save-search').on('click', function(e){
        <?php if(isset($_SESSION['WEB_USER_ID'])): ?>
        var search_name = $('input[name="search_name"]');
        var search_form = $('#searchForm');
        var icon = '', title = '', className = '', message = '';
        if(search_name.val().length){
            $.ajax({
                url: "<?php echo option('site_uri') . url_for('/search/save') ?>",
                method: 'POST',
                data: search_form.serialize()
            }).done(function(response){
                search_name.val('');
                t('s', response.message);
            });
        }else{
            t('e', 'Please type a search name!');
        }
	    <?php else: ?>
        var search_form = $('#searchForm');
        var url = search_form.serialize();
        document.location = '<?php echo url_for('/sign_up') . '?redirect=' ?><?php echo $cms->site_host . ROOTPATH; ?>search?'+url;
        e.preventDefault();
        <?php endif ?>
    });

    $('.btn-fav').on('click', function(e){
        var url = $(e.currentTarget).data('url');
        var id = $(e.currentTarget).data('id');
        var type = $(e.currentTarget).data('type');
        <?php if(isset($_SESSION['WEB_USER_ID'])): ?>
        document.location = '<?php echo $cms->site_host ?>' + url+'?id='+id+'&type='+type;
        <?php else: ?>
        document.location = '<?php echo url_for('/sign_up') . '?redirect=' ?>'+btoa(encodeURI('<?php echo $cms->site_host; ?>' + url+'?id='+id+'&type='+type));
	    <?php endif ?>
        e.preventDefault();
    });

</script>
<?php end_content_for(); ?>
