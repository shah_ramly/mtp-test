<!DOCTYPE html>
<html lang="en">


<body class="white-content od-mode">
 
       
                            <div class="card-body-dashboard">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="row">
                                            <div class="col-xl-12 col-report-dashboard-top">
                                                <div class="col-report-dashboard-top-wrapper">
                                                    <div class="row">
                                                        <div class="col-xl-7 col-report-dashboard-top-left">
                                                            <div class="col-report-dashboard-top-left-wrapper">
                                                                <div class="col-report-dashboard-header">
                                                                    <div class="col-report-dashboard-header-left">
                                                                        <div class="report-dashboard-title">Weekly Average Earnings</div>
                                                                        <div class="report-dashboard-val">RM1,420</div>
                                                                    </div>
                                                                    <div class="col-report-dashboard-header-right">
                                                                        <div class="col-icon">
                                                                            <span class="arrow-up-right">
                                                                                <svg class="bi bi-arrow-up-right" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                  <path fill-rule="evenodd" d="M6.5 4a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 .5.5v5a.5.5 0 0 1-1 0V4.5H7a.5.5 0 0 1-.5-.5z"/>
                                                                                  <path fill-rule="evenodd" d="M12.354 3.646a.5.5 0 0 1 0 .708l-9 9a.5.5 0 0 1-.708-.708l9-9a.5.5 0 0 1 .708 0z"/>
                                                                                </svg>
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-stats">
                                                                            <div class="stats-filter">Last 7 days</div>
                                                                            <div class="stats-percent">+52.50%</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="barchart_container">
                                                                    <canvas id="barchart_canvas"></canvas>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-5 col-report-dashboard-top-right">
                                                            <div class="col-report-dashboard-top-right-wrapper">
                                                                <div class="col-report-dashboard-monthly-header">
                                                                    <div class="report-dashboard-monthly-title">Monthly Sales</div>
                                                                    <div class="report-dashboard-monthly-desc">Average total sales +25.5%</div>
                                                                </div>
                                                                <!--<svg id="donut_chart_svg"></svg>-->
                                                                <div id="donutchart_container">
                                                                    <div class="donutchart-center-container">
                                                                        <div class="donutchart-center-lbl">Total Price</div>
                                                                        <div class="donutchart-center-val">RM450</div>
                                                                    </div>
                                                                    <canvas id="doChart"></canvas>
                                                                </div>
                                                                <div class="donutchart-legend-container">
                                                                    <div class="donutchart-legend-flex">
                                                                        <div class="donutchart-legend-col">
                                                                            <span class="dot-icon dot-orange"></span>
                                                                            <div class="donutchart-lbl-container">
                                                                                <div class="dot-lbl">Credit Card</div>
                                                                                <div class="donutchart-legend-val">38% over</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="donutchart-legend-col">
                                                                            <span class="dot-icon dot-blue"></span>
                                                                            <div class="donutchart-lbl-container">
                                                                                <div class="dot-lbl">Bank Transfer</div>
                                                                                <div class="donutchart-legend-val">62% over</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12">
                                        <div class="row">
                                            <div class="col-xl-6 col-report-dashboard col-report-dashboard-left">
                                                <div class="col-dashboard">
                                                    <div class="sub-col-title bp">
														<div class="left">Best Products</div>
															<div class="right">Last Week  <span class="chevron-down-icon"><svg class="bi bi-chevron-down" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                  <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"></path>
                                                                </svg></span> </div>
													</div>
                                                    <div class="sub-col-content">
                                                        <div class="progressbar-row row-flex">
                                                            <div class="progress-lbl">
																<div class="main">Technology</div>
															<div class="sub">- Direct Sales</div></div>
                                                            <div id="progress_1" class="progressbar-chart"></div>
                                                        </div>
                                                        <div class="progressbar-row row-flex">
                                                            <div class="progress-lbl">
																<div class="main">Clothing</div>
															<div class="sub">- Credit Card</div></div>
                                                            <div id="progress_2" class="progressbar-chart"></div>
                                                        </div>
                                                        <div class="progressbar-row row-flex">
                                                            <div class="progress-lbl">
																<div class="main">Kid Toys</div>
																<div class="sub">- Bank Transfer</div>
															</div>
                                                            <div id="progress_3" class="progressbar-chart"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-report-dashboard col-report-dashboard-right">
                                                <div class="col-dashboard">
                                                    <div class="sub-col-title lo-title">Last Orders <span class="lo-three-dots-icon">
    <svg class="bi bi-three-dots" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z"></path>
    </svg>
</span></div>
                                                    <div class="lo table-responsive">
                                                        <table class="table tablesorter " id="">
                                                            <thead class=" text-primary">
                                                                <tr>
                                                                    <th>
                                                                        ID
                                                                    </th>
                                                                    <th>
                                                                        Name
                                                                    </th>
                                                                    <th>
                                                                        Date
                                                                    </th>
                                                                    <th class="text-center">
                                                                        Amount
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        35443
                                                                    </td>
                                                                    <td>
                                                                        <div class="table-profile-photo">
                                                                            <img src="assets/img/james.jpg" alt="Profile Photo">
                                                                        </div>
                                                                        <div class="table-profile-name">Marvin Black</div>
                                                                    </td>
                                                                    <td>
                                                                        2020-03-30
                                                                    </td>
                                                                    <td class="text-center">
                                                                        RM36.38
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        66545
                                                                    </td>
                                                                    <td>
                                                                        <div class="table-profile-photo">
                                                                            <img src="assets/img/emilyz.jpg" alt="Profile Photo">
                                                                        </div>
                                                                        <div class="table-profile-name">Audrey William</div>
                                                                    </td>
                                                                    <td>
                                                                       2020-02-22
                                                                    </td>
                                                                    <td class="text-center">
                                                                        RM237.89
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        44323
                                                                    </td>
                                                                    <td>
                                                                        <div class="table-profile-photo">
                                                                            <img src="assets/img/mike.jpg" alt="Profile Photo">
                                                                        </div>
                                                                        <div class="table-profile-name">Cameron Stew</div>
                                                                    </td>
                                                                    <td>
                                                                        2020-01-18
                                                                    </td>
                                                                    <td class="text-center">
                                                                        RM142.00
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        44322
                                                                    </td>
                                                                    <td>
                                                                        <div class="table-profile-photo">
                                                                            <img src="assets/img/james.jpg" alt="Profile Photo">
                                                                        </div>
                                                                        <div class="table-profile-name">Colleen Warren</div>
                                                                    </td>
                                                                    <td>
                                                                        2020-01-10
                                                                    </td>
                                                                    <td class="text-center">
                                                                        RM387.00
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <!-- Modal - OD Post Task Steps -->
    <div id="post_task_od" class="modal modal-steps fade" aria-labelledby="post_task_od" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target="#modal_confirm_od" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                </button>
                <div class="modal-body">
                    <div class="modal-form-wrapper">
                        <form name="form_post_task_od" id="form_post_task_od" method="post" action="">

                            <div id="sf1" class="frm modal-step-flex">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 1 of 4: Select Category</div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Category" id="od_category" name="od_category">
                                                <option disabled selected>Category</option>
                                                <option value="1" >Option 1</option>
                                                <option value="2" >Option 2</option>
                                                <option value="3" >Option 3</option>
                                                <option value="4" >Option 4</option>
                                                <option value="5" >Option 5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Sub Category" id="od_subcategory" name="od_subcategory">
                                                <option disabled selected>Sub Category</option>
                                                <option value="1" >Option 1</option>
                                                <option value="2" >Option 2</option>
                                                <option value="3" >Option 3</option>
                                                <option value="4" >Option 4</option>
                                                <option value="5" >Option 5</option>
                                            </select>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-next od-open1">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
                                            </span>
                                        </button>
                                    </div>            
                                </div>
                                
                            </div>

                            <div id="sf2" class="frm modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 2 of 4: Task Details</div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-input" placeholder="Task Title" id="od_tasktitle" name="od_tasktitle">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control form-control-input" placeholder="Task Description" id="od_taskdesc" name="od_taskdesc" rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <div class="dropzone dropzone-previews" id="my-awesome-dropzone"></div>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev od-back2">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next od-open2">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div id="sf3" class="frm modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 3 of 4: Task Estimates</div>
                                        <div class="form-group">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Nature of Task</span>
                                            </label>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="not_bythehour" name="defaultExampleRadios">
                                                <label class="custom-control-label" for="not_bythehour">By the hour</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="not_lumpsum" name="defaultExampleRadios">
                                                <label class="custom-control-label" for="not_lumpsum">Lump sum, complete by deadline</label>
                                            </div>
                                        </div>

                                        <div class="form-check-selection" id="form_byhour" style="display: none;">
                                            <div class="form-group form-estimated-hours">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Estimated hours</span>
                                                </label>
                                                <div class="form-flex">
                                                    <input type="number" name="" class="form-control form-control-input" min="0" id="" />
                                                    <span class="postlbl-hours">hours</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-estimated-hours">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Budget Per Hour</span>
                                                </label>
                                                <div class="form-flex">
                                                    <span class="estimated-lbl estimated-from">RM50</span>
                                                    <input id="range_budget_hour" type="text" /> 
                                                    <span class="estimated-lbl estimated-to">RM100</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-total-project-budget">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Total Project Budget</span>
                                                </label>
                                                <div class="form-flex">
                                                    <span class="postlbl-cur">RM150 - RM300</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-complete-by">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Complete By</span>
                                                </label>
                                                <div class="form-group">
                                                    <div class="input-group input-group-datetimepicker date" id="form_datetime_hour" data-target-input="nearest">
                                                        <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_hour" />
                                                        <span class="input-group-addon" data-target="#form_datetime_hour" data-toggle="datetimepicker">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="form-check-selection" id="form_bylumpsum" style="display: none;">
                                            <div class="form-group form-estimated-hours">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Budget For Task</span>
                                                </label>
                                                <div class="form-flex">
                                                    <span class="estimated-lbl estimated-from">RM200</span>
                                                    <input id="range_budget_lumpsum" type="text" /> 
                                                    <span class="estimated-lbl estimated-to">RM300</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-complete-by">
                                                <label for="creditcard" class="input-lbl">
                                                    <span class="input-label-txt">Complete By</span>
                                                </label>
                                                <div class="form-group">
                                                    <div class="input-group input-group-datetimepicker date" id="form_datetime_lumpsum" data-target-input="nearest">
                                                        <input type="text" class="form-control datetimepicker-input" data-target="#form_datetime_lumpsum" />
                                                        <span class="input-group-addon" data-target="#form_datetime_lumpsum" data-toggle="datetimepicker">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <!--<legend>Step 3 of 4</legend>
                                        <div class="form-group house-type-container">
                                            <div class="">
                                                <select class="form-control" name="house_type" id="sel1 house_type">
                                                    <option value="" class="select-disabled-type" selected disabled>What is your house type</option>
                                                    <option value="Apartment/Flat">Apartment/Flat</option>
                                                    <option value="Condo/Serivce Residence">Condo/Service Residence</option>
                                                    <option value="Terrace/Townhouse">Terrace/Townhouse</option>
                                                    <option value="Semi-D/Bungalow">Semi-D/Bungalow</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group house-size-container">
                                            <span class="form-label-house-size">What is your house size (sq.ft.):</span>

                                            <div class="">
                                                <input id="house_size" name="house_size" data-slider-id='ex2Slider' type="text" data-slider-min="0" data-slider-max="10000" data-slider-step="50" data-slider-value="800" />
                                            </div>
                                        </div>
    -->
                                        <div class="clearfix" style="height: 10px;clear: both;"></div>



                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev od-back3">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next od-open3">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div id="sf4" class="frm modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>

                                        <div class="modal-steps-title">Step 4 of 4: Seeker Shortlisting</div>

                                        <div class="form-group form-important-skills">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Important skills for task</span>
                                            </label>
                                            <div class="bs-example">
                                                <input type="text" name="tags" value="Tag1,Tag2,Tag3" data-role="tagsinput" />
                                            </div>
                                        </div>
                                        <div class="form-group form-qna">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Questions</span>
                                            </label>
                                            <div class="od-qna-field-wrapper">
                                                <div class="qna-wrapper od-qna-wrapper ">
                                                   <div class="form-flex">
                                                        <span class="qna-lbl od-qna-lbl">1.</span>
                                                        <input type="text" name="od_qna_field[]" class="form-control form-control-input"  placeholder="Question"/>
                                                    </div>
                                                    
                                                </div>
                                                <a href="javascript:void(0);" class="od-add-button" title="Add field">Add More</a>
                                            </div>
                                            
                                        </div>




                                        <!--<legend>Step 4 of 4</legend>

                                        <div class="form-group house-rental-container">
                                            <span class="form-label-house-rental">Choose one:</span>
                                            <div class="">
                                                <div data-toggle="buttons">
                                                    <div class="btn-group">
                                                        <label class="btn btn-signum-label">
                                                            <input type="radio" name="house_rental" id="house_rental" value="Own Stay" class="sr-only" required>Own Stay</label>
                                                        <label class="btn btn-signum-label">
                                                            <input type="radio" name="house_rental" id="house_rental" value="Rental" class="sr-only" required>Rental</label>
                                                        <label class="btn btn-signum-label">
                                                            <input type="radio" name="house_rental" id="house_rental" value="Airbnb" class="sr-only" required>Airbnb</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group house-location-container">
                                            <span class="form-label-house-location">Where is location of your house:</span>
                                            <div class="">
                                                <input type="text" placeholder="Eg: Damansara" id="house_location" name="house_location" class="form-control" autocomplete="off">
                                            </div>
                                        </div>

                                        <div class="form-group budget-container">
                                            <span class="form-label-budget">What is your budget (RM):</span>
                                            <div class="budget-range-slider">
                                                <input id="budget" name="budget" type="text" data-slider-ticks="[50000, 100000, 150000]" data-slider-ticks-snap-bounds="30" data-slider-ticks-labels='["< RM50k", "RM100k", "> RM100k"]' />
                                            </div>
                                        </div>-->


                                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                                        <!--<div class="g-recaptcha" data-sitekey="6LefLk4UAAAAAJJqnKhJX904t7Wg6FTTWOJMGgsI"></div>-->
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev od-back4">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/></svg>
                                            </span>
                                        </button>
                                        <button type="submit" class="btn-icon-full btn-step-submit">
                                            <span class="btn-label">Submit</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    
    
    <!-- Modal - FT Post Job Steps -->
    <div id="post_job_ft" class="modal modal-steps fade" aria-labelledby="post_job_ft" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target="#modal_confirm_ft" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                </button>
                <div class="modal-body">
                    <div class="modal-form-wrapper">
                        <form name="form_post_job_ft" id="form_post_job_ft" method="post" action="">

                            <div id="ft_sf1" class="frm-ft modal-step-flex">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 1 of 5: Select Category</div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Category" id="ft_category" name="ft_category">
                                                <option disabled selected>Category</option>
                                                <option value="1" >Option 1</option>
                                                <option value="2" >Option 2</option>
                                                <option value="3" >Option 3</option>
                                                <option value="4" >Option 4</option>
                                                <option value="5" >Option 5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Sub Category" id="ft_subcategory" name="ft_subcategory">
                                                <option disabled selected>Sub Category</option>
                                                <option value="1" >Option 1</option>
                                                <option value="2" >Option 2</option>
                                                <option value="3" >Option 3</option>
                                                <option value="4" >Option 4</option>
                                                <option value="5" >Option 5</option>
                                            </select>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-next ft-open1">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
                                            </span>
                                        </button>
                                    </div>            
                                </div>
                                
                            </div>

                            <div id="ft_sf2" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 2 of 5: The Job</div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-input" placeholder="Job Title" id="ft_jobtitle" name="ft_jobtitle">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control form-control-input" placeholder="Job Description" id="ft_jobdesc" name="ft_jobdesc" rows="3"></textarea>
                                        </div>
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back2">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" /></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next ft-open2">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div id="ft_sf3" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 3 of 5: The Applicant</div>
                                        
                                        <div class="form-group">
                                            <textarea class="form-control form-control-input" placeholder="The Requirements" id="ft_jobrequirement" name="ft_jobrequirement" rows="3"></textarea>
                                        </div>
                                        
                                        <div class="form-group form-qna">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Questions</span>
                                            </label>
                                            <div class="ft-qna-field-wrapper">
                                                <div class="qna-wrapper ft-qna-wrapper ">
                                                   <div class="form-flex">
                                                        <span class="qna-lbl ft-qna-lbl">1.</span>
                                                        <input type="text" name="ft_qna_field[]" class="form-control form-control-input"  placeholder="Question"/>
                                                    </div>
                                                    
                                                </div>
                                                <a href="javascript:void(0);" class="ft-add-button" title="Add field">Add More</a>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Experience Level" id="ft_jobxp" name="ft_jobxp">
                                                <option disabled selected>Experience Level</option>
                                                <option value="1" >Option 1</option>
                                                <option value="2" >Option 2</option>
                                                <option value="3" >Option 3</option>
                                                <option value="4" >Option 4</option>
                                                <option value="5" >Option 5</option>
                                            </select>
                                        </div>
                                        


                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back3">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next ft-open3">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="ft_sf4" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>
                                        <div class="modal-steps-title">Step 4 of 5: The Location</div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="Country" id="ft_jobcountry" name="ft_jobcountry">
                                                <option disabled selected>Country</option>
                                                <option value="1" >Malaysia</option>
                                                <option value="2" >Singapore</option>
                                                <option value="3" >Thailand</option>
                                                <option value="4" >Option 4</option>
                                                <option value="5" >Option 5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control form-control-input" placeholder="State" id="ft_jobstate" name="ft_jobstate">
                                                <option disabled selected>State</option>
                                                <option value="Johor">Johor</option>
                                                <option value="Kedah">Kedah</option>
                                                <option value="Kelantan">Kelantan</option>
                                                <option value="Kuala Lumpur">Kuala Lumpur</option>
                                                <option value="Labuan">Labuan</option>
                                                <option value="Malacca">Melaka</option>
                                                <option value="Negeri Sembilan">Negeri Sembilan</option>
                                                <option value="Pahang">Pahang</option>
                                                <option value="Perak">Perak</option>
                                                <option value="Perlis">Perlis</option>
                                                <option value="Penang">Penang</option>
                                                <option value="Sabah">Sabah</option>
                                                <option value="Sarawak">Sarawak</option>
                                                <option value="Selangor">Selangor</option>
                                                <option value="Terengganu">Terengganu</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-input" placeholder="Job Location" id="ft_joblocation" name="ft_joblocation">
                                        </div>
                                        


                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back4">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/></svg>
                                            </span>
                                        </button>
                                        <button type="button" class="btn-icon-full btn-step-next ft-open4">
                                            <span class="btn-label">Next</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            
                            

                            <div id="ft_sf5" class="frm-ft modal-step-flex" style="display: none;">
                                <div class="modal-col modal-col-left"></div>
                                <div class="modal-col modal-col-right">
                                    <fieldset>

                                        <div class="modal-steps-title">Step 5 of 5: Remuneration</div>
                                        <div class="form-group form-salary-range">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Salary Range</span>
                                            </label>
                                            <div class="form-flex">
                                                <span class="estimated-lbl estimated-from">RM1200</span>
                                                <input id="range_budget_salary" type="text" /> 
                                                <span class="estimated-lbl estimated-to">RM12000</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-benefits">
                                            <label for="creditcard" class="input-lbl">
                                                <span class="input-label-txt">Benefits</span>
                                            </label>
                                            <div class="form-block">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_1">
                                                    <label class="custom-control-label" for="benefits_1">Medical</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_2">
                                                    <label class="custom-control-label" for="benefits_2">Parking</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_3">
                                                    <label class="custom-control-label" for="benefits_3">Claimable expenses</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_4">
                                                    <label class="custom-control-label" for="benefits_4">Dental</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_5">
                                                    <label class="custom-control-label" for="benefits_5">Meals</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_6">
                                                    <label class="custom-control-label" for="benefits_6">Pantry</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="benefits_7">
                                                    <label class="custom-control-label" for="benefits_7">Commission</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </fieldset>
                                    <div class="form-group button-container">
                                        <button type="button" class="btn-icon-full btn-step-prev ft-back5">
                                            <span class="btn-label">Back</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/></svg>
                                            </span>
                                        </button>
                                        <button type="submit" class="btn-icon-full btn-step-submit">
                                            <span class="btn-label">Submit</span>
                                            <span class="btn-icon">
                                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    
    
    <!-- Modal - OD Confirm Discard -->
    <div id="modal_confirm_od" class="modal modal-confirm fade" aria-labelledby="modal_confirm_od" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p class="modal-para">This will discard all the detail you have entered.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_task_od" >
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                        </span>
                    </button>
                    <button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                        <span class="btn-label">Confirm</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs"><polyline points="20 6 9 17 4 12"></polyline></svg>
                        </span>
                    </button>
                </div>
            </div>
         </div>
    </div>
    <!-- /.modal -->
    
    <!-- Modal - FT Confirm Discard -->
    <div id="modal_confirm_ft" class="modal modal-confirm fade" aria-labelledby="modal_confirm_ft" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p class="modal-para">This will discard all the detail you have entered.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_job_ft" >
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                        </span>
                    </button>
                    <button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                        <span class="btn-label">Confirm</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs"><polyline points="20 6 9 17 4 12"></polyline></svg>
                        </span>
                    </button>
                </div>
            </div>
         </div>
    </div>
    <!-- /.modal -->
    
    
    
    

    
    <!-- Bar Chart Function -->
    <script>

        
        // Bar Chart Rounded Corner custom
        Chart.elements.Rectangle.prototype.draw = function() {
    
            var ctx = this._chart.ctx;
            var vm = this._view;
            var left, right, top, bottom, signX, signY, borderSkipped, radius;
            var borderWidth = vm.borderWidth;
            // Set Radius Here
            // If radius is large enough to cause drawing errors a max radius is imposed
            var cornerRadius = 12;

            if (!vm.horizontal) {
                // bar
                left = vm.x - vm.width / 2;
                right = vm.x + vm.width / 2;
                top = vm.y;
                bottom = vm.base;
                signX = 1;
                signY = bottom > top? 1: -1;
                borderSkipped = vm.borderSkipped || 'bottom';
            } else {
                // horizontal bar
                left = vm.base;
                right = vm.x;
                top = vm.y - vm.height / 2;
                bottom = vm.y + vm.height / 2;
                signX = right > left? 1: -1;
                signY = 1;
                borderSkipped = vm.borderSkipped || 'left';
            }

            // Canvas doesn't allow us to stroke inside the width so we can
            // adjust the sizes to fit if we're setting a stroke on the line
            if (borderWidth) {
                // borderWidth shold be less than bar width and bar height.
                var barSize = Math.min(Math.abs(left - right), Math.abs(top - bottom));
                borderWidth = borderWidth > barSize? barSize: borderWidth;
                var halfStroke = borderWidth / 2;
                // Adjust borderWidth when bar top position is near vm.base(zero).
                var borderLeft = left + (borderSkipped !== 'left'? halfStroke * signX: 0);
                var borderRight = right + (borderSkipped !== 'right'? -halfStroke * signX: 0);
                var borderTop = top + (borderSkipped !== 'top'? halfStroke * signY: 0);
                var borderBottom = bottom + (borderSkipped !== 'bottom'? -halfStroke * signY: 0);
                // not become a vertical line?
                if (borderLeft !== borderRight) {
                    top = borderTop;
                    bottom = borderBottom;
                }
                // not become a horizontal line?
                if (borderTop !== borderBottom) {
                    left = borderLeft;
                    right = borderRight;
                }
            }

            ctx.beginPath();
            ctx.fillStyle = vm.backgroundColor;
            ctx.strokeStyle = vm.borderColor;
            ctx.lineWidth = borderWidth;

            // Corner points, from bottom-left to bottom-right clockwise
            // | 1 2 |
            // | 0 3 |
            var corners = [
                [left, bottom],
                [left, top],
                [right, top],
                [right, bottom]
            ];

            // Find first (starting) corner with fallback to 'bottom'
            var borders = ['bottom', 'left', 'top', 'right'];
            var startCorner = borders.indexOf(borderSkipped, 0);
            if (startCorner === -1) {
                startCorner = 0;
            }

            function cornerAt(index) {
                return corners[(startCorner + index) % 4];
            }

            // Draw rectangle from 'startCorner'
            var corner = cornerAt(0);
            ctx.moveTo(corner[0], corner[1]);

            for (var i = 1; i < 4; i++) {
                corner = cornerAt(i);
                nextCornerId = i+1;
                if(nextCornerId == 4){
                    nextCornerId = 0
                }

                nextCorner = cornerAt(nextCornerId);

                width = corners[2][0] - corners[1][0];
                height = corners[0][1] - corners[1][1];
                x = corners[1][0];
                y = corners[1][1];

                var radius = cornerRadius;

                // Fix radius being too large
                if(radius > height/2){
                    radius = height/2;
                }if(radius > width/2){
                    radius = width/2;
                }

                ctx.moveTo(x + radius, y);
                ctx.lineTo(x + width - radius, y);
                ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
                ctx.lineTo(x + width, y + height - radius);
                ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
                ctx.lineTo(x + radius, y + height);
                ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
                ctx.lineTo(x, y + radius);
                ctx.quadraticCurveTo(x, y, x + radius, y);

            }

            ctx.fill();
            if (borderWidth) {
                ctx.stroke();
            }
        };
        // Bar Chart Rounded Corner custom
        
		window.onload = function() {
			var ctx = document.getElementById('barchart_canvas').getContext('2d');
            var gradient = ctx.createLinearGradient(0, 0, 0, 400);
                gradient.addColorStop(0, 'rgba(255,133,99,1)');   
                gradient.addColorStop(1, 'rgba(237,89,43,1)');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: {
                    labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
                    datasets: [{
                        data: [7, 11, 20, 19, 14, 10, 8],
                        backgroundColor: [
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                        ],
                        barPercentage: 0.5,
                    }]
                },
				options: {
					responsive: true,
                    maintainAspectRatio: false,
					legend: {
						display: false,
					},
					title: {
						display: false,
					},
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display:false,
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                borderDash: [8, 4],
                                color: "#eeeeee",
                                drawBorder: false,
                                lineWidth: 1,
                            },
                            ticks: {
                                display:false,
                                drawBorder: false
                            },
                        }]
                    }
				}
			});

		};
	</script>
    <!-- Bar Chart Function -->
    
    <!-- Donut Chart Function -->
    <script>
        
        var doughnut = document.getElementById("doChart");
        var myDoughnutChart = new Chart(doughnut, {
            type: 'doughnut',
            data: {
            labels:["Credit Card","Bank Transfer"],
            datasets: [{
                data: [4100, 2500],
                backgroundColor: ['#3644ad','#f0724c'],
                borderColor: ['#3644ad','#f0724c'],
                hoverBorderWidth: 10,
                hoverRadius: 1,
                borderWidth: 0,
             }]
           },
          options: {
              defaultFontFamily: Chart.defaults.global.defaultFontFamily = "'Poppins'",
              legend: {display: false,position: 'bottom'},
              cutoutPercentage: 70,
              maintainAspectRatio: false,
              responsive: true,
              tooltips: {
                  callbacks: {
                    label: function(tooltipItem, data) {
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                      var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                        return previousValue + currentValue;
                      });
                      var currentValue = dataset.data[tooltipItem.index];
                      var precentage = Math.floor(((currentValue/total) * 100)+0.5);         
                      return precentage + "%";
                    }
                  }
                }
          }
        });
        
    </script>
    <!-- Donut Chart Function -->

    <!-- Progress Bar Function -->
    <script>
        // progressbar.js@1.0.0 version is used
        // Docs: https://progressbarjs.readthedocs.org/en/1.0.0/

        var bar1 = new ProgressBar.Line(progress_1, {
          strokeWidth: 1,
          easing: 'easeInOut',
          duration: 1400,
          color: '#655f93',
          trailColor: '#eee',
          trailWidth: 1,
          svgStyle: {width: '100%', height: '100%'},
          text: {
            style: {
              // Text color.
              // Default: same as stroke color (options.color)
            },
            autoStyleContainer: false
          },
          step: (state, bar) => {
            bar.setText(Math.round(bar.value() * 100) + ' %');
          }
        });

        bar1.animate(0.25);  // Number from 0.0 to 1.0

        var bar2 = new ProgressBar.Line(progress_2, {
          strokeWidth: 1,
          easing: 'easeInOut',
          duration: 1400,
          color: '#007cf0',
          trailColor: '#eee',
          trailWidth: 1,
          svgStyle: {width: '100%', height: '100%'},
          text: {
            style: {
              // Text color.
              // Default: same as stroke color (options.color)
            },
            autoStyleContainer: false
          },
          step: (state, bar) => {
            bar.setText(Math.round(bar.value() * 100) + ' %');
          }
        });

        bar2.animate(0.62);  // Number from 0.0 to 1.0

        var bar3 = new ProgressBar.Line(progress_3, {
          strokeWidth: 1,
          easing: 'easeInOut',
          duration: 1400,
          color: '#00c3bc',
          trailColor: '#eee',
          trailWidth: 1,
          svgStyle: {width: '100%', height: '100%'},
          text: {
            style: {
              // Text color.
              // Default: same as stroke color (options.color)
            },
            autoStyleContainer: false
          },
          step: (state, bar) => {
            bar.setText(Math.round(bar.value() * 100) + ' %');
          }
        });

        bar3.animate(0.34);  // Number from 0.0 to 1.0
    </script>
    <!-- Progress Bar Function -->
    
    <!-- Sidebar Hover Function -->
    <script>
        // Sidebar Hover Function
        const $dropdown = $(".sidebar .dropdown");
        const $dropdownToggle = $(".sidebar .dropdown-toggle");
        const $dropdownMenu = $(".sidebar .dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function () {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                    function () {
                        const $this = $(this);
                        $this.addClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "true");
                        $this.find($dropdownMenu).addClass(showClass);
                    },
                    function () {
                        const $this = $(this);
                        $this.removeClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "false");
                        $this.find($dropdownMenu).removeClass(showClass);
                    }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });


        $('.card-header .dropdown-menu .nav-tabs .nav-link').on("click.bs.dropdown", function (e) {
            $(this).tab('show');
            e.stopPropagation();
        });
        $('.col-account-header .dropdown.col-account .switch-toggle .slider-toggler').on("click", function (e) {
            e.stopPropagation();
        });
    </script>
    <!-- Sidebar Hover Function -->


</body>

</html>
