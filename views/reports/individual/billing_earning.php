<div class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-chart">
				<div class="card-header">
					<div class="row">
						<div class="col-sm-12 col-header-filter">
							<h2 class="card-title"><?php echo 'Billings - Earning'; ?></h2>
                            <?php echo partial('workspace/partial/header.html.php'); ?>
						</div>
					</div>
				</div>
				<div class="card-body-billings-earning">
					<ul class="nav nav-pills nav-pills-billings" id="pills-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="pills-all-tab" data-toggle="pill" href="#pills-all" role="tab" aria-controls="pills-all" aria-selected="true">All Status</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-in-progress-tab" data-toggle="pill" href="#pills-in-progress" role="tab" aria-controls="pills-in-progress" aria-selected="false">In Progress</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-completed-tab" data-toggle="pill" href="#pills-completed" role="tab" aria-controls="pills-completed" aria-selected="false">Completed</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-closed-tab" data-toggle="pill" href="#pills-closed" role="tab" aria-controls="pills-closed" aria-selected="false">Closed</a>
						</li>
					</ul>
					<div class="tab-content" id="pills-tabContent">
						<div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
							<div class="col-xl-12">
								<div class="tab-container">
									<div class="tab-filter-container">
										<div id="table-filter-container" class="table-filter-container">
											<div class="table-filter-left-col">
												<div class="dropdown dropdown-filter-container">
													<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<label class="filter-btn-lbl"><?php echo lang('public_search_date'); ?></label> <span class="drop-val"><?php echo lang('public_search_all'); ?></span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
														<a class="dropdown-item" href="#"><?php echo lang('public_search_all'); ?></a>
														<a class="dropdown-item" href="#">This Month</a>
														<a class="dropdown-item" href="#">This Quarter</a>
														<a class="dropdown-item" href="#">This Year</a>
														<a class="dropdown-item" href="#">Custom</a>
													</div>
												</div>
												<div class="form-group form-group-filter-calendar filter-calendar-from">
													<div class="input-group input-group-datetimepicker date" id="calendar_allstatus_1" data-target-input="nearest">
														<input type="text" class="form-control datetimepicker-input" data-target="#calendar_allstatus_1" placeholder="Select Date"/>
														<span class="input-group-addon" data-target="#calendar_allstatus_1" data-toggle="datetimepicker">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
													</div>
												</div>

												<div class="form-group form-group-filter-calendar filter-calendar-to">
													<div class="input-group input-group-datetimepicker date" id="calendar_allstatus_2" data-target-input="nearest">
														<input type="text" class="form-control datetimepicker-input" data-target="#calendar_allstatus_2" placeholder="Select Date"/>
														<span class="input-group-addon" data-target="#calendar_allstatus_2" data-toggle="datetimepicker">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
													</div>
												</div>
											</div>
											<div class="table-filter-right-col">
												<div class="dropdown dropdown-filter-container">
													<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<label class="filter-btn-lbl">Export This List</label> <span class="drop-val">Select a Format</span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
														<a class="dropdown-item" href="#">PDF</a>
														<a class="dropdown-item" href="#">Excel</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-table-container">
										<table class="table table-billings active">
											<tbody>
											<tr>
												<td class="active check-col">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="select-item custom-control-input checkbox" name="select-item" id="defaultCheckedtblRow1" />
														<label class="custom-control-label" for="defaultCheckedtblRow1"></label>
													</div>
												</td>
												<td class="info namedesc-col">
													<div class="tbl-main-title tbl-task-name">Conduct an online pottery class</div>
													<div class="tbl-desc tbl-task-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
												</td>
												<td class="info idate-col">
													<div class="tbl-sub-title tbl-task-id">#78232</div>
													<div class="tbl-desc tbl-task-date">14 Jul 2020</div>
												</td>
												<td class="info amt-col">
													<div class="tbl-sub-title tbl-task-amt"> RM880.00</div>
													<div class="tbl-desc tbl-task-status">In Progress</div>
												</td>
												<td class="info status-col">
													<div id="progress_1" class="progressbar-chart"></div>
												</td>
												<td class="info more-opt-col">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                        <span class="tbl-more-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>
                                                                        </span>
													</a>
													<ul class="dropdown-menu dropdown-navbar">
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">View Task Detail</a></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option A</a></li>
														<li class="dropdown-divider"></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option B</a></li>
													</ul>
												</td>
											</tr>
											<tr>
												<td class="active check-col">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="select-item custom-control-input checkbox" name="select-item" id="defaultCheckedtblRow2" />
														<label class="custom-control-label" for="defaultCheckedtblRow2"></label>
													</div>
												</td>
												<td class="info namedesc-col">
													<div class="tbl-main-title tbl-task-name">Website content translation English to Malay</div>
													<div class="tbl-desc tbl-task-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
												</td>
												<td class="info idate-col">
													<div class="tbl-sub-title tbl-task-id">#792102</div>
													<div class="tbl-desc tbl-task-date">14 Jul 2020</div>
												</td>
												<td class="info amt-col">
													<div class="tbl-sub-title tbl-task-amt"> RM160.00</div>
													<div class="tbl-desc tbl-task-status">Completed</div>
												</td>
												<td class="info status-col">
													<div class="btn-icon-custom btn-chevron-right"><span class="btn-label">Request for Payment</span><span class="btn-icon"><svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg></span></div>
												</td>
												<td class="info more-opt-col">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                        <span class="tbl-more-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>
                                                                        </span>
													</a>
													<ul class="dropdown-menu dropdown-navbar">
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">View Task Detail</a></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option A</a></li>
														<li class="dropdown-divider"></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option B</a></li>
													</ul>
												</td>
											</tr>
											<tr>
												<td class="active check-col">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="select-item custom-control-input checkbox" name="select-item" id="defaultCheckedtblRow3" />
														<label class="custom-control-label" for="defaultCheckedtblRow3"></label>
													</div>
												</td>
												<td class="info namedesc-col">
													<div class="tbl-main-title tbl-task-name">Front end development for React web app</div>
													<div class="tbl-desc tbl-task-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
												</td>
												<td class="info idate-col">
													<div class="tbl-sub-title tbl-task-id">#723402</div>
													<div class="tbl-desc tbl-task-date">14 Jul 2020</div>
												</td>
												<td class="info amt-col">
													<div class="tbl-sub-title tbl-task-amt"> RM1200.00</div>
													<div class="tbl-desc tbl-task-status">Completed</div>
												</td>
												<td class="info status-col">
													<div class="btn-icon-custom btn-chevron-right"><span class="btn-label">Request for Payment</span><span class="btn-icon"><svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg></span></div>
												</td>
												<td class="info more-opt-col">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                        <span class="tbl-more-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>
                                                                        </span>
													</a>
													<ul class="dropdown-menu dropdown-navbar">
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">View Task Detail</a></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option A</a></li>
														<li class="dropdown-divider"></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option B</a></li>
													</ul>
												</td>
											</tr>
											</tbody>
										</table>

										<table class="table table-billings inactive">
											<tbody>

											<tr>
												<td class="active check-col">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="select-item custom-control-input checkbox" name="select-item" id="defaultCheckedtblRow5" />
														<label class="custom-control-label" for="defaultCheckedtblRow5"></label>
													</div>
												</td>
												<td class="info namedesc-col">
													<div class="tbl-main-title tbl-task-name">Effective business blog and article writing, SEO content</div>
													<div class="tbl-desc tbl-task-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
												</td>
												<td class="info idate-col">
													<div class="tbl-sub-title tbl-task-id">#712302</div>
													<div class="tbl-desc tbl-task-date">14 Jul 2020</div>
												</td>
												<td class="info amt-col">
													<div class="tbl-sub-title tbl-task-amt"> RM400.00</div>
													<div class="tbl-desc tbl-task-status">Closed</div>
												</td>
												<td class="info status-col">
													<div class="btn-icon-custom btn-chevron-right"><span class="btn-label">Payment Details</span><span class="btn-icon"><svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg></span></div>
												</td>
												<td class="info more-opt-col">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                        <span class="tbl-more-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>
                                                                        </span>
													</a>
													<ul class="dropdown-menu dropdown-navbar">
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">View Task Detail</a></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option A</a></li>
														<li class="dropdown-divider"></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option B</a></li>
													</ul>
												</td>
											</tr>
											<tr>
												<td class="active check-col">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="select-item custom-control-input checkbox" name="select-item" id="defaultCheckedtblRow6" />
														<label class="custom-control-label" for="defaultCheckedtblRow6"></label>
													</div>
												</td>
												<td class="info namedesc-col">
													<div class="tbl-main-title tbl-task-name">Select and modify Facebook ad images</div>
													<div class="tbl-desc tbl-task-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
												</td>
												<td class="info idate-col">
													<div class="tbl-sub-title tbl-task-id">#767102</div>
													<div class="tbl-desc tbl-task-date">14 Jul 2020</div>
												</td>
												<td class="info amt-col">
													<div class="tbl-sub-title tbl-task-amt"> RM550.00</div>
													<div class="tbl-desc tbl-task-status">Closed</div>
												</td>
												<td class="info status-col">
													<div class="btn-icon-custom btn-chevron-right"><span class="btn-label">Payment Details</span><span class="btn-icon"><svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg></span></div>
												</td>
												<td class="info more-opt-col">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                        <span class="tbl-more-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>
                                                                        </span>
													</a>
													<ul class="dropdown-menu dropdown-navbar">
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">View Task Detail</a></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option A</a></li>
														<li class="dropdown-divider"></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option B</a></li>
													</ul>
												</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>

							</div>







						</div>
						<div class="tab-pane fade" id="pills-in-progress" role="tabpanel" aria-labelledby="pills-in-progress-tab">
							<div class="col-xl-12">
								<div class="tab-container">
									<div class="tab-filter-container">
										<div id="table-filter-container" class="table-filter-container">
											<div class="table-filter-left-col">
												<div class="dropdown dropdown-filter-container">
													<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<label class="filter-btn-lbl"><?php echo lang('public_search_date'); ?></label> <span class="drop-val"><?php echo lang('public_search_all'); ?></span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
														<a class="dropdown-item" href="#"><?php echo lang('public_search_all'); ?></a>
														<a class="dropdown-item" href="#">This Month</a>
														<a class="dropdown-item" href="#">This Quarter</a>
														<a class="dropdown-item" href="#">This Year</a>
														<a class="dropdown-item" href="#">Custom</a>
													</div>
												</div>
												<div class="form-group form-group-filter-calendar filter-calendar-from">
													<div class="input-group input-group-datetimepicker date" id="calendar_inprogress_1" data-target-input="nearest">
														<input type="text" class="form-control datetimepicker-input" data-target="#calendar_inprogress_1" placeholder="Select Date"/>
														<span class="input-group-addon" data-target="#calendar_inprogress_1" data-toggle="datetimepicker">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
													</div>
												</div>

												<div class="form-group form-group-filter-calendar filter-calendar-to">
													<div class="input-group input-group-datetimepicker date" id="calendar_inprogress_2" data-target-input="nearest">
														<input type="text" class="form-control datetimepicker-input" data-target="#calendar_inprogress_2" placeholder="Select Date"/>
														<span class="input-group-addon" data-target="#calendar_inprogress_2" data-toggle="datetimepicker">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
													</div>
												</div>
											</div>
											<div class="table-filter-right-col">
												<div class="dropdown dropdown-filter-container">
													<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<label class="filter-btn-lbl">Export This List</label> <span class="drop-val">Select a Format</span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
														<a class="dropdown-item" href="#">PDF</a>
														<a class="dropdown-item" href="#">Excel</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-table-container">
										<table class="table table-billings active">
											<tbody>
											<tr>
												<td class="active check-col">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="select-item custom-control-input checkbox" name="select-item" id="defaultCheckedtblRow7" />
														<label class="custom-control-label" for="defaultCheckedtblRow7"></label>
													</div>
												</td>
												<td class="info namedesc-col">
													<div class="tbl-main-title tbl-task-name">Conduct an online pottery class</div>
													<div class="tbl-desc tbl-task-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
												</td>
												<td class="info idate-col">
													<div class="tbl-sub-title tbl-task-id">#78232</div>
													<div class="tbl-desc tbl-task-date">14 Jul 2020</div>
												</td>
												<td class="info amt-col">
													<div class="tbl-sub-title tbl-task-amt"> RM880.00</div>
													<div class="tbl-desc tbl-task-status">In Progress</div>
												</td>
												<td class="info status-col">
													<div id="progress_2" class="progressbar-chart"></div>
												</td>
												<td class="info more-opt-col">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                        <span class="tbl-more-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>
                                                                        </span>
													</a>
													<ul class="dropdown-menu dropdown-navbar">
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">View Task Detail</a></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option A</a></li>
														<li class="dropdown-divider"></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option B</a></li>
													</ul>
												</td>
											</tr>

											</tbody>
										</table>


									</div>
								</div>

							</div>







						</div>
						<div class="tab-pane fade" id="pills-completed" role="tabpanel" aria-labelledby="pills-completed-tab">
							<div class="col-xl-12">
								<div class="tab-container">
									<div class="tab-filter-container">
										<div id="table-filter-container" class="table-filter-container">
											<div class="table-filter-left-col">
												<div class="dropdown dropdown-filter-container">
													<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<label class="filter-btn-lbl"><?php echo lang('public_search_date'); ?></label> <span class="drop-val"><?php echo lang('public_search_all'); ?></span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
														<a class="dropdown-item" href="#"><?php echo lang('public_search_all'); ?></a>
														<a class="dropdown-item" href="#">This Month</a>
														<a class="dropdown-item" href="#">This Quarter</a>
														<a class="dropdown-item" href="#">This Year</a>
														<a class="dropdown-item" href="#">Custom</a>
													</div>
												</div>
												<div class="form-group form-group-filter-calendar filter-calendar-from">
													<div class="input-group input-group-datetimepicker date" id="calendar_completed_1" data-target-input="nearest">
														<input type="text" class="form-control datetimepicker-input" data-target="#calendar_completed_1" placeholder="Select Date"/>
														<span class="input-group-addon" data-target="#calendar_completed_1" data-toggle="datetimepicker">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
													</div>
												</div>

												<div class="form-group form-group-filter-calendar filter-calendar-to">
													<div class="input-group input-group-datetimepicker date" id="calendar_completed_2" data-target-input="nearest">
														<input type="text" class="form-control datetimepicker-input" data-target="#calendar_completed_2" placeholder="Select Date"/>
														<span class="input-group-addon" data-target="#calendar_completed_2" data-toggle="datetimepicker">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
													</div>
												</div>

											</div>
											<div class="table-filter-right-col">
												<div class="dropdown dropdown-filter-container">
													<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<label class="filter-btn-lbl">Export This List</label> <span class="drop-val">Select a Format</span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
														<a class="dropdown-item" href="#">PDF</a>
														<a class="dropdown-item" href="#">Excel</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-table-container">
										<table class="table table-billings active">
											<tbody>
											<tr>
												<td class="active check-col">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="select-item custom-control-input checkbox" name="select-item" id="defaultCheckedtblRow8" />
														<label class="custom-control-label" for="defaultCheckedtblRow8"></label>
													</div>
												</td>
												<td class="info namedesc-col">
													<div class="tbl-main-title tbl-task-name">Website content translation English to Malay</div>
													<div class="tbl-desc tbl-task-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
												</td>
												<td class="info idate-col">
													<div class="tbl-sub-title tbl-task-id">#792102</div>
													<div class="tbl-desc tbl-task-date">14 Jul 2020</div>
												</td>
												<td class="info amt-col">
													<div class="tbl-sub-title tbl-task-amt"> RM160.00</div>
													<div class="tbl-desc tbl-task-status">Completed</div>
												</td>
												<td class="info status-col">
													<div class="btn-icon-custom btn-chevron-right"><span class="btn-label">Request for Payment</span><span class="btn-icon"><svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg></span></div>
												</td>
												<td class="info more-opt-col">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                        <span class="tbl-more-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>
                                                                        </span>
													</a>
													<ul class="dropdown-menu dropdown-navbar">
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">View Task Detail</a></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option A</a></li>
														<li class="dropdown-divider"></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option B</a></li>
													</ul>
												</td>
											</tr>
											<tr>
												<td class="active check-col">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="select-item custom-control-input checkbox" name="select-item" id="defaultCheckedtblRow9" />
														<label class="custom-control-label" for="defaultCheckedtblRow9"></label>
													</div>
												</td>
												<td class="info namedesc-col">
													<div class="tbl-main-title tbl-task-name">Website content translation English to Malay</div>
													<div class="tbl-desc tbl-task-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
												</td>
												<td class="info idate-col">
													<div class="tbl-sub-title tbl-task-id">#792102</div>
													<div class="tbl-desc tbl-task-date">14 Jul 2020</div>
												</td>
												<td class="info amt-col">
													<div class="tbl-sub-title tbl-task-amt"> RM160.00</div>
													<div class="tbl-desc tbl-task-status">Completed</div>
												</td>
												<td class="info status-col">
													<div class="btn-icon-custom btn-chevron-right"><span class="btn-label">Request for Payment</span><span class="btn-icon"><svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg></span></div>
												</td>
												<td class="info more-opt-col">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                        <span class="tbl-more-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>
                                                                        </span>
													</a>
													<ul class="dropdown-menu dropdown-navbar">
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">View Task Detail</a></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option A</a></li>
														<li class="dropdown-divider"></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option B</a></li>
													</ul>
												</td>
											</tr>

											</tbody>
										</table>
									</div>
								</div>

							</div>







						</div>

						<div class="tab-pane fade" id="pills-closed" role="tabpanel" aria-labelledby="pills-closed-tab">
							<div class="col-xl-12">
								<div class="tab-container">
									<div class="tab-filter-container">
										<div id="table-filter-container" class="table-filter-container">
											<div class="table-filter-left-col">
												<div class="dropdown dropdown-filter-container">
													<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<label class="filter-btn-lbl"><?php echo lang('public_search_date'); ?></label> <span class="drop-val"><?php echo lang('public_search_all'); ?></span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
														<a class="dropdown-item" href="#"><?php echo lang('public_search_all'); ?></a>
														<a class="dropdown-item" href="#">This Month</a>
														<a class="dropdown-item" href="#">This Quarter</a>
														<a class="dropdown-item" href="#">This Year</a>
														<a class="dropdown-item" href="#">Custom</a>
													</div>
												</div>
												<div class="form-group form-group-filter-calendar filter-calendar-from">
													<div class="input-group input-group-datetimepicker date" id="calendar_closed_1" data-target-input="nearest">
														<input type="text" class="form-control datetimepicker-input" data-target="#calendar_closed_1" placeholder="Select Date"/>
														<span class="input-group-addon" data-target="#calendar_closed_1" data-toggle="datetimepicker">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
													</div>
												</div>

												<div class="form-group form-group-filter-calendar filter-calendar-to">
													<div class="input-group input-group-datetimepicker date" id="calendar_closed_2" data-target-input="nearest">
														<input type="text" class="form-control datetimepicker-input" data-target="#calendar_closed_2" placeholder="Select Date"/>
														<span class="input-group-addon" data-target="#calendar_closed_2" data-toggle="datetimepicker">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
													</div>
												</div>
											</div>
											<div class="table-filter-right-col">
												<div class="dropdown dropdown-filter-container">
													<button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<label class="filter-btn-lbl">Export This List</label> <span class="drop-val">Select a Format</span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
														<a class="dropdown-item" href="#">PDF</a>
														<a class="dropdown-item" href="#">Excel</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-table-container">

										<table class="table table-billings inactive">
											<tbody>


											<tr>
												<td class="active check-col">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="select-item custom-control-input checkbox" name="select-item" id="defaultCheckedtblRow10" />
														<label class="custom-control-label" for="defaultCheckedtblRow10"></label>
													</div>
												</td>
												<td class="info namedesc-col">
													<div class="tbl-main-title tbl-task-name">Select and modify Facebook ad images</div>
													<div class="tbl-desc tbl-task-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
												</td>
												<td class="info idate-col">
													<div class="tbl-sub-title tbl-task-id">#767102</div>
													<div class="tbl-desc tbl-task-date">14 Jul 2020</div>
												</td>
												<td class="info amt-col">
													<div class="tbl-sub-title tbl-task-amt"> RM550.00</div>
													<div class="tbl-desc tbl-task-status">Closed</div>
												</td>
												<td class="info status-col">
													<div class="btn-icon-custom btn-chevron-right"><span class="btn-label">Payment Details</span><span class="btn-icon"><svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg></span></div>
												</td>
												<td class="info more-opt-col">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                        <span class="tbl-more-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>
                                                                        </span>
													</a>
													<ul class="dropdown-menu dropdown-navbar">
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">View Task Detail</a></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option A</a></li>
														<li class="dropdown-divider"></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option B</a></li>
													</ul>
												</td>
											</tr>
											<tr>
												<td class="active check-col">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="select-item custom-control-input checkbox" name="select-item" id="defaultCheckedtblRow11" />
														<label class="custom-control-label" for="defaultCheckedtblRow11"></label>
													</div>
												</td>
												<td class="info namedesc-col">
													<div class="tbl-main-title tbl-task-name">Select and modify Facebook ad images</div>
													<div class="tbl-desc tbl-task-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
												</td>
												<td class="info idate-col">
													<div class="tbl-sub-title tbl-task-id">#767102</div>
													<div class="tbl-desc tbl-task-date">14 Jul 2020</div>
												</td>
												<td class="info amt-col">
													<div class="tbl-sub-title tbl-task-amt"> RM550.00</div>
													<div class="tbl-desc tbl-task-status">Closed</div>
												</td>
												<td class="info status-col">
													<div class="btn-icon-custom btn-chevron-right"><span class="btn-label">Payment Details</span><span class="btn-icon"><svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg></span></div>
												</td>
												<td class="info more-opt-col">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                        <span class="tbl-more-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>
                                                                        </span>
													</a>
													<ul class="dropdown-menu dropdown-navbar">
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">View Task Detail</a></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option A</a></li>
														<li class="dropdown-divider"></li>
														<li class="header-dropdown-link-list"><a href="#" class="nav-item dropdown-item">Option B</a></li>
													</ul>
												</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<!-- Filter Datepicker Function -->
    <script>
        
        $('#calendar_allstatus_1').datetimepicker({
           format: 'DD/MM/YYYY',
        });
        
        $('#calendar_allstatus_2').datetimepicker({
           format: 'DD/MM/YYYY',
        });
        
        $('#calendar_inprogress_1').datetimepicker({
           format: 'DD/MM/YYYY',
        });
        
        $('#calendar_inprogress_2').datetimepicker({
           format: 'DD/MM/YYYY',
        });
        
        $('#calendar_completed_1').datetimepicker({
           format: 'DD/MM/YYYY',
        });
        
        $('#calendar_completed_2').datetimepicker({
           format: 'DD/MM/YYYY',
        });
        
        $('#calendar_closed_1').datetimepicker({
           format: 'DD/MM/YYYY',
        });
        
        $('#calendar_closed_2').datetimepicker({
           format: 'DD/MM/YYYY',
        });
        
    </script>
    <!-- Filter Datepicker Function -->

