    <section class="section-404">
        <div class="container">
            <a href="<?php echo url_for('/'); ?>">
                <div class="logo"><img src="<?php echo url_for('assets/img/mtp_logo_dark.png') ?>" /></div>
            </a>
            <div class="main-wrapper">
                <div class="box-flex">
                    <div class="box-col col-left">
                        <div class="subcol-desc">
                            <h3 class="main-title"><span><?php echo lang('no_worries'); ?></span> <span><?php echo lang('just_a_hiccup'); ?></span></h3>
                            <p><?php echo lang('the_page_youre_looking_for_is_probably_outdated'); ?></p>
                        </div>
                        <span class="arrow-right"></span>
                    </div>
                    <div class="box-col col-right">
                        <div class="subcol-home-report">
                            <div class="subcol-left subcol-home">
                                <p><?php echo lang('click_below_to_head_back_to_our_homepage'); ?></p>
                                <button type="submit" class="btn-icon-full btn-404-backhome gtm-404-backhome" onclick="location.href='<?php echo url_for('/'); ?>'">
                                    <span class="btn-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/>
                                        </svg>
                                    </span>
                                    <span class="btn-label"><?php echo lang('back_to_home'); ?></span>
                                </button>
                            </div>
                            <div class="subcol-right subcol-report">
                                <p><?php echo lang('tell_us_briefly_what_led_you_to_this_page'); ?></p>
                                <form id="feedback-404" method="post" action="<?php echo option('site_uri') . url_for('feedback') ?>">
	                                <?php echo html_form_token_field() ?>
	                                <input type="hidden" name="feedback_area" value="Page 404" />
	                                <input type="hidden" name="data" value='<?php echo json_encode(['url' => request_uri()]) ?>' />
                                    <textarea name="feedback" class="form-control form-control-input" placeholder="<?php echo lang('form_404_placeholder'); ?>" rows="3" ></textarea>
                                    <button type="submit" class="btn-icon-full btn-404-submitreport gtm-404-submitreport">
                                        <span class="btn-label"><?php echo lang('submit_report'); ?></span>
                                        <span class="btn-icon">
                                            <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                            </svg>
                                        </span>
                                    </button>
                                </form>
	                            <h3 class="d-none" style="margin-bottom:6.9rem"><?php echo lang('thank_you_for_your_feedback') ?></h3>
                            </div>
                        </div>
                        <span class="arrow-right"></span>
                    </div>
                </div>
            </div>
            <div class="footer-404">
                <div class="footer-copyright">
                    <span class="copyright-year"><?php echo date("Y"); ?> MakeTimePay <i>-</i> </span>
                    <span class="copyright-desc"><?php echo lang('all_right_reserved'); ?></span>
                </div>
            </div>
        </div>
    </section>
    