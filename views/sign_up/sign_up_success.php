<div class="sidenav">
    <a href="<?php echo url_for('/'); ?>">
        <div class="sidenav-logo">
            <img src="<?php echo url_for("/assets/img/mtp_logo_dark.png") ?>" alt="" />
        </div>
    </a>
    <div class="sidenav-illus-container side-nav-sign-up">
        <div class="sidenav-illus-img">
			<img src="<?php echo url_for("/assets/img/MTP-Web-Illus-Sign up.png") ?>" alt="" />
        </div>
    </div>
</div>
<div class="sign-in-main">
    <div class="col-md-12 col-sm-12">
        <div class=" col-md-12 sign-up-form-container sign-up-success-form-container">
            <h1 class="main-title sign-in-title"><?php echo lang('sign_up_success'); ?></h1>
            <p class="body-para"><?php echo lang('sign_up_success_thank_you'); ?></p>
            <button class="btn-icon-full btn-chevron-right gtm-signup-ind" type="button">
                <span class="btn-label"><?php echo lang('sign_up_back_to_homepage'); ?></span>
                <span class="btn-icon">
                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                    </svg>
                </span>
            </button>
            <span class="body-para sign-up-success-help-text"><?php echo lang('sign_up_success_notes_check_spam_folder'); ?></span>
        </div>
    </div>
</div>

<script>
	$(function(){
	   $('.sign-up-success-form-container > button').on('click', function(e){
	       e.preventDefault();
	       $(this).prop('disabled', true);
	       document.location = "<?php echo option('site_uri') . url_for('/') ?>";
	   });
	});
</script>