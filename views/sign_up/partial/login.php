<!DOCTYPE html>
<html lang="en">



<body class="white-content page-sign-in">
	<div class="sidenav">
        <a href="<?php echo url_for('/'); ?>">
            <div class="sidenav-logo">
                <img src="<?php echo url_for("/assets/img/mtp_logo_dark.png") ?>" alt="" />
            </div>
        </a>
        <div class="sidenav-illus-container side-nav-sign-up">
            <div class="sidenav-illus-img">
				<img src="<?php echo url_for("/assets/img/MTP-Web-Illus-Sign-in.png") ?>" alt="" />
            </div>
        </div>
    </div>
    <div class="sign-in-main">
        <div class="col-md-12 col-sm-12">
            <div class=" col-md-12 login-form">
                <form class="form-signin" id="loginForm">
                    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
                    <h1 class="main-title sign-in-title"> <?php echo lang('sign_in'); ?></h1>
                    <div class="social-login">
                        <button class="btn facebook-btn social-btn gtm-signin-fb" type="button" onclick="location.href='<?php echo url_for('/login/fb'); ?>'"><span><i class="fab fa-facebook-f"></i> <?php echo lang('sign_in_with_facebook'); ?></span> </button>
                        <button class="btn google-btn social-btn gtm-signin-google" type="button" onclick="location.href='<?php echo url_for('/login/google'); ?>'"><span><i class="fab fa-google"></i> <?php echo lang('sign_in_with_google'); ?></span> </button>
                    </div>
                    <p class="login-divider"><?php echo lang('sign_in_with_or'); ?></p>
                    <input type="email" id="inputEmail" class="form-control form-control-input" name="email" placeholder="<?php echo lang('sign_in_with_email_placeholder'); ?>" required="" autofocus="">
                    <input type="password" id="inputPassword" class="form-control form-control-input" name="password" placeholder="<?php echo lang('sign_in_password_placeholder'); ?>" required="">
                    
                    <div class="form-actions">
                        <button class="btn-icon-custom btn-chevron-right btn-icon-disabled gtm-signin-submit" type="submit" disabled><span class="btn-label"><?php echo lang('sign_in'); ?></span><span class="btn-icon"><svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg></span></button>
                        <a href="<?php echo url_for('/forgot_password'); ?>" class="gtm-signin-forgotpass" id="forgot_pswd"><?php echo lang('sign_in_forgot_password'); ?></a>
                    </div>
					<div class="form-footer"><?php echo lang('sign_in_dont_have_account_title'); ?> <a href="<?php echo url_for('/sign_up') . '?redirect=' . urlencode($redirect) ; ?>" id="log-in-link" class="gtm-signin-signup-link"><?php echo lang('sign_in_sign_up'); ?></a></div>
                </form>
            </div>
        </div>
    </div>
   

    <script>
        $(function(){
            $(document).on('keyup change', '#loginForm [name]', function(){
                fill = 0;
                $('#loginForm [name]').each(function(){
                    if($(this).val()){
                        fill += 1;
                    }
                });

                if(fill >= 3){
                    $('#loginForm [type="submit"]').prop('disabled', false).removeClass('btn-icon-disabled');
                }else{
                    $('#loginForm [type="submit"]').prop('disabled', true).addClass('btn-icon-disabled');
                }
            });
			
            $(document).on('submit', '#loginForm', function(){
                $.ajax({
                    type:'POST',
                    url: rootPath + 'login',
                    dataType : 'json',
                    data: $('#loginForm ').serialize(),
                    success: function(results){
                        if(results.error == true){
                            t('e', results.msg);
                        }else{
                            //t('s', results.msg);
                            //setTimeout(function(){
                                location.href = results.url;
                            //}, 1000);
                        }
                    },
                    error: function(error){
                        ajax_error(error);
                    },
                    complete: function(){}
                });
                return false;
            });
        });
        
        const $dropdown = $(".sidebar .dropdown");
        const $dropdownToggle = $(".sidebar .dropdown-toggle");
        const $dropdownMenu = $(".sidebar .dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function() {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                    function() {
                        const $this = $(this);
                        $this.addClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "true");
                        $this.find($dropdownMenu).addClass(showClass);
                    },
                    function() {
                        const $this = $(this);
                        $this.removeClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "false");
                        $this.find($dropdownMenu).removeClass(showClass);
                    }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });


        $('.card-header .dropdown-menu .nav-tabs .nav-link').on("click.bs.dropdown", function(e) {
            $(this).tab('show');
            e.stopPropagation();
        });

    </script>

    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip();
            <?php if(!empty($error)){ ?>
            t('e', '<?php echo $error['description']; ?>');
            <?php } ?>
        })

        /*var widthOverflow = $('.tbl-header-calendar-gantt-body-overflow')[0].scrollWidth;
        var widthColParent = $('.col-right-card-flex').width();
        var maxDrag = 0;
        maxDrag = widthOverflow - widthColParent;
        console.log(maxDrag);
        console.log(widthColParent);
        $('.tbl-body-calendar-gantt').css('width', widthOverflow);





        var splitobj = Split(["#colLeftCard", "#colRightCard"], {
            elementStyle: function(dimension, size, gutterSize) {
                $(window).trigger('resize'); // Optional
                if (size <= 70) {
                    return {
                        'flex-basis': 'calc(' + size + '% - ' + gutterSize + 'px)'
                    }
                }

            },
            gutterStyle: function(dimension, gutterSize) {
                return {
                    'flex-basis': gutterSize + 'px'
                }
            },
            sizes: [50, 50],
            minSize: [40, 40],
            gutterSize: 5,
            cursor: 'col-resize',
            expandToMin: true,
        });*/

    </script>

    <script>
        $(document).ready(function() {
            $().ready(function() {
                $sidebar = $('.sidebar');
                $navbar = $('.navbar');
                $main_panel = $('.main-panel');

                $full_page = $('.full-page');

                $sidebar_responsive = $('body > .navbar-collapse');
                sidebar_mini_active = true;
                white_color = false;

                window_width = $(window).width();

                fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();



                $('.fixed-plugin a').click(function(event) {
                    if ($(this).hasClass('switch-trigger')) {
                        if (event.stopPropagation) {
                            event.stopPropagation();
                        } else if (window.event) {
                            window.event.cancelBubble = true;
                        }
                    }
                });

                $('.fixed-plugin .background-color span').click(function() {
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');

                    var new_color = $(this).data('color');

                    if ($sidebar.length != 0) {
                        $sidebar.attr('data', new_color);
                    }

                    if ($main_panel.length != 0) {
                        $main_panel.attr('data', new_color);
                    }

                    if ($full_page.length != 0) {
                        $full_page.attr('filter-color', new_color);
                    }

                    if ($sidebar_responsive.length != 0) {
                        $sidebar_responsive.attr('data', new_color);
                    }
                });

                $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function() {
                    var $btn = $(this);

                    if (sidebar_mini_active == true) {
                        $('body').removeClass('sidebar-mini');
                        sidebar_mini_active = false;
                        blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                    } else {
                        $('body').addClass('sidebar-mini');
                        sidebar_mini_active = true;
                        blackDashboard.showSidebarMessage('Sidebar mini activated...');
                    }

                    // we simulate the window Resize so the charts will get updated in realtime.
                    var simulateWindowResize = setInterval(function() {
                        window.dispatchEvent(new Event('resize'));
                    }, 180);

                    // we stop the simulation of Window Resize after the animations are completed
                    setTimeout(function() {
                        clearInterval(simulateWindowResize);
                    }, 1000);
                });

                $('.switch-change-color input').on("switchChange.bootstrapSwitch", function() {
                    var $btn = $(this);

                    if (white_color == true) {

                        $('body').addClass('change-background');
                        setTimeout(function() {
                            $('body').removeClass('change-background');
                            $('body').removeClass('white-content');
                        }, 900);
                        white_color = false;
                    } else {

                        $('body').addClass('change-background');
                        setTimeout(function() {
                            $('body').removeClass('change-background');
                            $('body').addClass('white-content');
                        }, 900);

                        white_color = true;
                    }


                });

                $('.light-badge').click(function() {
                    $('body').addClass('white-content');
                });

                $('.dark-badge').click(function() {
                    $('body').removeClass('white-content');
                });
            });
        });

    </script>
    <script>
        $(document).ready(function() {
            // Javascript method's body can be found in assets/js/demos.js
            //demo.initDashboardPageCharts();
        });

    </script>










</body>

</html>
