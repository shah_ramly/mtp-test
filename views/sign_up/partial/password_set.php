

<body class="white-content page-sign-up">
<div class="sidenav">
    <a href="<?php echo url_for('/'); ?>">
        <div class="sidenav-logo">
            <img src="<?php echo url_for("/assets/img/mtp_logo_dark.png") ?>" alt="" />
        </div>
    </a>
	<div class="sidenav-illus-container side-nav-sign-up">
		<div class="sidenav-illus-img">
			<img src="<?php echo url_for('/assets/img/MTP-Web-Illus-Sign-in.png') ?>" alt="">
		</div>
	</div>
</div>
      <div class="sign-in-main set-password">
         <div class="col-md-12 col-sm-12">
            <div class=" col-md-12 login-form">
              <form class="form-signin form-set-password" id="redirectForm">
                    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
                    <h1 class="main-title sign-in-title"><?php echo lang('set_password_title'); ?></h1>
                    <div class="set-password-group">
                        <input type="password" id="password" name="password" class="form-control form-control-input" placeholder="<?php echo lang('set_password_placeholder'); ?>" pattern="(.{8,}" title="<?php echo lang('set_password_must_contain'); ?>" required="" autofocus="">
                        <span class="bi-tooltip" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-original-title="<?php echo lang('sys_password_rules'); ?>">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                            </svg>
                        </span>
                    </div>
                    <div class="set-password-group">
                        <input type="password" id="confirm_password" class="form-control form-control-input" placeholder="<?php echo lang('set_password_confirm_password'); ?>" required="">
                        <span class="bi-tooltip" data-toggle="tooltip" data-html="true" data-placement="top" title="" data-original-title="<?php echo lang('sys_password_rules'); ?>">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                            </svg>
                        </span>
                    </div>
            <span id='message' style="display:block;margin-bottom:24px;"></span>
            <button class="btn-icon-custom btn-chevron-right gtm-pass-set-submit" type="submit" id="submitBtn">
			<span class="btn-label"><?php echo lang('set_password_title'); ?></span>
			<span class="btn-icon" id="icon"><svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                            </svg></span></button>
            </form>
            </div>
         </div>
      </div>
   

    <script>
        const $dropdown = $(".sidebar .dropdown");
        const $dropdownToggle = $(".sidebar .dropdown-toggle");
        const $dropdownMenu = $(".sidebar .dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function () {
          if (this.matchMedia("(min-width: 768px)").matches) {
            $dropdown.hover(
              function () {
                const $this = $(this);
                $this.addClass(showClass);
                $this.find($dropdownToggle).attr("aria-expanded", "true");
                $this.find($dropdownMenu).addClass(showClass);
              },
              function () {
                const $this = $(this);
                $this.removeClass(showClass);
                $this.find($dropdownToggle).attr("aria-expanded", "false");
                $this.find($dropdownMenu).removeClass(showClass);
              }
            );
          } else {
            $dropdown.off("mouseenter mouseleave");
          }
        });
        
        
        $('.card-header .dropdown-menu .nav-tabs .nav-link').on("click.bs.dropdown", function (e) { 
            $(this).tab('show'); 
            e.stopPropagation(); 
        });

    
    </script>
   
    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })

        var widthOverflow = $('.tbl-header-calendar-gantt-body-overflow')[0].scrollWidth;
        var widthColParent = $('.col-right-card-flex').width();
        var maxDrag = 0;
        maxDrag = widthOverflow - widthColParent;
        console.log(maxDrag);
        console.log(widthColParent);
        $('.tbl-body-calendar-gantt').css('width', widthOverflow);
        
        
        
        
        
        var splitobj = Split(["#colLeftCard","#colRightCard"], {
            elementStyle: function (dimension, size, gutterSize) { 
                $(window).trigger('resize'); // Optional
                if (size <= 70){
                    return {'flex-basis': 'calc(' + size + '% - ' + gutterSize + 'px)'}
                }
                    
            },
            gutterStyle: function (dimension, gutterSize) { return {'flex-basis':  gutterSize + 'px'} },
            sizes: [50,50],
            minSize: [40, 40],
            gutterSize: 5,
            cursor: 'col-resize',
            expandToMin: true,
        });
        
        
    </script>

    <script>
        $(document).ready(function() {
			//  Disabled the button
            $("#redirectForm button[type='submit']").attr("disabled", true);
            $("#redirectForm button[type='submit']").addClass("btn-icon-disabled").css('transition', 'none');

			$("input#password, input#confirm_password").keyup(function(){
			    if($('input#password').val().length && $('input#confirm_password').val().length && $('input#password').val() === $('input#confirm_password').val()) {
                    $("#redirectForm button[type='submit']").attr("disabled", false);
                    $("#redirectForm button[type='submit']").removeClass("btn-icon-disabled");
                }else{
                    $("#redirectForm button[type='submit']").addClass("btn-icon-disabled");
                    $("#redirectForm button[type='submit']").attr("disabled", true);
			    }
			});

            //const button = document.querySelector('button');

            $('#confirm_password').on('keyup', function () {
                if ($('#password').val() === $('#confirm_password').val()) {
                    //button.disabled = false;
                    $('#message').html('').css('color', 'green');
                } else {
                    //button.disabled = true;
                    $('#message').html('Password does not match').css('color', 'red');
                }
            });
			/*
            //  Check is the form all filled 
            $(document).on('change', '#redirectForm', function(e){
                var isValid = true;
                $("#redirectForm input").each(function() {
                    if ($(this).val() == ""){
                        isValid = false;
                    }
                });

                if(isValid == false){
                    $("#redirectForm button[type='submit']").attr("disabled", true);
                    $("#redirectForm button[type='submit']").addClass("btn-icon-disabled");
                }else{
                    $("#redirectForm button[type='submit']").attr("disabled", false);
                    $("#redirectForm button[type='submit']").removeClass("btn-icon-disabled");
                }
            });
			*/
			
            $().ready(function() {
                $sidebar = $('.sidebar');
                $navbar = $('.navbar');
                $main_panel = $('.main-panel');

                $full_page = $('.full-page');

                $sidebar_responsive = $('body > .navbar-collapse');
                sidebar_mini_active = true;
                white_color = false;

                window_width = $(window).width();

                fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();



                $('.fixed-plugin a').click(function(event) {
                    if ($(this).hasClass('switch-trigger')) {
                        if (event.stopPropagation) {
                            event.stopPropagation();
                        } else if (window.event) {
                            window.event.cancelBubble = true;
                        }
                    }
                });

                $('.fixed-plugin .background-color span').click(function() {
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');

                    var new_color = $(this).data('color');

                    if ($sidebar.length != 0) {
                        $sidebar.attr('data', new_color);
                    }

                    if ($main_panel.length != 0) {
                        $main_panel.attr('data', new_color);
                    }

                    if ($full_page.length != 0) {
                        $full_page.attr('filter-color', new_color);
                    }

                    if ($sidebar_responsive.length != 0) {
                        $sidebar_responsive.attr('data', new_color);
                    }
                });

                $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function() {
                    var $btn = $(this);

                    if (sidebar_mini_active == true) {
                        $('body').removeClass('sidebar-mini');
                        sidebar_mini_active = false;
                        blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                    } else {
                        $('body').addClass('sidebar-mini');
                        sidebar_mini_active = true;
                        blackDashboard.showSidebarMessage('Sidebar mini activated...');
                    }

                    // we simulate the window Resize so the charts will get updated in realtime.
                    var simulateWindowResize = setInterval(function() {
                        window.dispatchEvent(new Event('resize'));
                    }, 180);

                    // we stop the simulation of Window Resize after the animations are completed
                    setTimeout(function() {
                        clearInterval(simulateWindowResize);
                    }, 1000);
                });

                $('.switch-change-color input').on("switchChange.bootstrapSwitch", function() {
                    var $btn = $(this);

                    if (white_color == true) {

                        $('body').addClass('change-background');
                        setTimeout(function() {
                            $('body').removeClass('change-background');
                            $('body').removeClass('white-content');
                        }, 900);
                        white_color = false;
                    } else {

                        $('body').addClass('change-background');
                        setTimeout(function() {
                            $('body').removeClass('change-background');
                            $('body').addClass('white-content');
                        }, 900);

                        white_color = true;
                    }


                });

                $('.light-badge').click(function() {
                    $('body').addClass('white-content');
                });

                $('.dark-badge').click(function() {
                    $('body').removeClass('white-content');
                });
            });
        });

    </script>
    <script>
        $(document).ready(function() {
            // Javascript method's body can be found in assets/js/demos.js
            demo.initDashboardPageCharts();

        });
    </script>
    
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
</body>

</html>
