<!DOCTYPE html>
<html lang="en">


<body class="white-content page-sign-up">
<div class="sidenav">
    <a href="<?php echo url_for('/'); ?>">
        <div class="sidenav-logo">
            <img src="<?php echo url_for("/assets/img/mtp_logo_dark.png") ?>" alt="" />
        </div>
    </a>
	<div class="sidenav-illus-container side-nav-sign-up">
		<div class="sidenav-illus-img">
			<img src="<?php echo url_for('/assets/img/MTP-Web-Illus-Sign-in.png') ?>" alt="">
		</div>
	</div>
</div>
      <div class="sign-in-main">
        <div class="col-md-12 col-sm-12">
            <div class=" col-md-12 login-form">
                <form class="form-signin form-forgot-password" id="redirectForm">
                    <h1 class="main-title sign-in-title">WHOOPS!</h1>
                    <span class="title-desc"><span class="span-block"><?php echo lang('forgot_password_text_1'); ?></span> <?php echo lang('forgot_password_text_2'); ?></span>
                    <input type="email" id="inputEmail" name="email" class="form-control form-control-input" placeholder="<?php echo lang('forgot_password_placeholder_email'); ?>" required="" autofocus="">
                    <div class="form-group button-forgot-container">
                    <button type="button" class="btn-icon-full btn-step-prev od-back2 gtm-forgotpass-back" onClick="window.history.back();">
										<span class="btn-label">Back</span>
										<span class="btn-icon">
                                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"></path></svg>
                                            </span>
									</button>
                     <button class="btn-icon-custom btn-chevron-right gtm-forgotpass-submit" type="submit"><span class="btn-label"><?php echo lang('forgot_password_submit'); ?></span><span class="btn-icon"><svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                            </svg></span></button>
                            </div>
            </form>
            </div>
         </div>
      </div>
   
    <script>
		//  Disabled the button
		$("#redirectForm button[type='submit']").attr("disabled", true);
		$("#redirectForm button[type='submit']").addClass("btn-icon-disabled");
		
		// Change to onkeyup instead
		$("input").keyup(function(){
			if ($('#inputEmail').val() === "") {
				$("#redirectForm button[type='submit']").attr("disabled", true);
				$("#redirectForm button[type='submit']").addClass("btn-icon-disabled");
			} else {
				$("#redirectForm button[type='submit']").attr("disabled", false);
				$("#redirectForm button[type='submit']").removeClass("btn-icon-disabled");
			}
		});
		
		/*
		//  Check is the form all filled 
		$(document).on('change', '#redirectForm', function(e){
			var isValid = true;
			$("#redirectForm input").each(function() {
				if ($(this).val() == ""){
					isValid = false;
				}
			});

			if(isValid == false){
				$("#redirectForm button[type='submit']").attr("disabled", true);
				$("#redirectForm button[type='submit']").addClass("btn-icon-disabled");
			}else{
				$("#redirectForm button[type='submit']").attr("disabled", false);
				$("#redirectForm button[type='submit']").removeClass("btn-icon-disabled");
			}
		});
*/
        const $dropdown = $(".sidebar .dropdown");
        const $dropdownToggle = $(".sidebar .dropdown-toggle");
        const $dropdownMenu = $(".sidebar .dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function () {
          if (this.matchMedia("(min-width: 768px)").matches) {
            $dropdown.hover(
              function () {
                const $this = $(this);
                $this.addClass(showClass);
                $this.find($dropdownToggle).attr("aria-expanded", "true");
                $this.find($dropdownMenu).addClass(showClass);
              },
              function () {
                const $this = $(this);
                $this.removeClass(showClass);
                $this.find($dropdownToggle).attr("aria-expanded", "false");
                $this.find($dropdownMenu).removeClass(showClass);
              }
            );
          } else {
            $dropdown.off("mouseenter mouseleave");
          }
        });
        
        
        $('.card-header .dropdown-menu .nav-tabs .nav-link').on("click.bs.dropdown", function (e) { 
            $(this).tab('show'); 
            e.stopPropagation(); 
        });

    
    </script>
   
    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })

        var widthOverflow = $('.tbl-header-calendar-gantt-body-overflow')[0].scrollWidth;
        var widthColParent = $('.col-right-card-flex').width();
        var maxDrag = 0;
        maxDrag = widthOverflow - widthColParent;
        console.log(maxDrag);
        console.log(widthColParent);
        $('.tbl-body-calendar-gantt').css('width', widthOverflow);
        
        
        
        
        
        var splitobj = Split(["#colLeftCard","#colRightCard"], {
            elementStyle: function (dimension, size, gutterSize) { 
                $(window).trigger('resize'); // Optional
                if (size <= 70){
                    return {'flex-basis': 'calc(' + size + '% - ' + gutterSize + 'px)'}
                }
                    
            },
            gutterStyle: function (dimension, gutterSize) { return {'flex-basis':  gutterSize + 'px'} },
            sizes: [50,50],
            minSize: [40, 40],
            gutterSize: 5,
            cursor: 'col-resize',
            expandToMin: true,
        });
        
        
    </script>

    <script>
        $(document).ready(function() {
            $().ready(function() {
                $sidebar = $('.sidebar');
                $navbar = $('.navbar');
                $main_panel = $('.main-panel');

                $full_page = $('.full-page');

                $sidebar_responsive = $('body > .navbar-collapse');
                sidebar_mini_active = true;
                white_color = false;

                window_width = $(window).width();

                fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();



                $('.fixed-plugin a').click(function(event) {
                    if ($(this).hasClass('switch-trigger')) {
                        if (event.stopPropagation) {
                            event.stopPropagation();
                        } else if (window.event) {
                            window.event.cancelBubble = true;
                        }
                    }
                });

                $('.fixed-plugin .background-color span').click(function() {
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');

                    var new_color = $(this).data('color');

                    if ($sidebar.length != 0) {
                        $sidebar.attr('data', new_color);
                    }

                    if ($main_panel.length != 0) {
                        $main_panel.attr('data', new_color);
                    }

                    if ($full_page.length != 0) {
                        $full_page.attr('filter-color', new_color);
                    }

                    if ($sidebar_responsive.length != 0) {
                        $sidebar_responsive.attr('data', new_color);
                    }
                });

                $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function() {
                    var $btn = $(this);

                    if (sidebar_mini_active == true) {
                        $('body').removeClass('sidebar-mini');
                        sidebar_mini_active = false;
                        blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                    } else {
                        $('body').addClass('sidebar-mini');
                        sidebar_mini_active = true;
                        blackDashboard.showSidebarMessage('Sidebar mini activated...');
                    }

                    // we simulate the window Resize so the charts will get updated in realtime.
                    var simulateWindowResize = setInterval(function() {
                        window.dispatchEvent(new Event('resize'));
                    }, 180);

                    // we stop the simulation of Window Resize after the animations are completed
                    setTimeout(function() {
                        clearInterval(simulateWindowResize);
                    }, 1000);
                });

                $('.switch-change-color input').on("switchChange.bootstrapSwitch", function() {
                    var $btn = $(this);

                    if (white_color == true) {

                        $('body').addClass('change-background');
                        setTimeout(function() {
                            $('body').removeClass('change-background');
                            $('body').removeClass('white-content');
                        }, 900);
                        white_color = false;
                    } else {

                        $('body').addClass('change-background');
                        setTimeout(function() {
                            $('body').removeClass('change-background');
                            $('body').addClass('white-content');
                        }, 900);

                        white_color = true;
                    }


                });

                $('.light-badge').click(function() {
                    $('body').addClass('white-content');
                });

                $('.dark-badge').click(function() {
                    $('body').removeClass('white-content');
                });
            });
        });

    </script>
    <script>
        $(document).ready(function() {
            // Javascript method's body can be found in assets/js/demos.js
            demo.initDashboardPageCharts();

        });

    </script>
    
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
</body>

</html>
