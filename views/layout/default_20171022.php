<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, width=device-width" />
<meta name="description" content="<?php echo $settings['meta_description']; ?>">
<meta name="keywords" content="<?php echo $settings['meta_keyword']; ?>">

<title><?php echo option('title_prefix') . option('title'); ?></title>
<link rel="stylesheet" href="<?php echo url_for('/assets/css'); ?>/style.css" />
<script type="text/javascript" src="<?php echo url_for('/assets/libs/jquery/jquery-2.1.0.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/jquery/jquery-migrate-1.2.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/jquery/jquery-ui.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/bootstrap/timepicker/moment-with-locales.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/bootstrap/timepicker/bootstrap-datetimepicker.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/plupload/plupload.full.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/plupload/plupload.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/notifyjs/notify.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/notifyjs/metro/notify-metro.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/bxslider/jquery.bxslider.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/prettyPhoto/js/jquery.prettyPhoto.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/owl-carousel/owl.carousel.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/js'); ?>/bootbox.min.js"></script>
<script type="text/javascript" src="<?php echo url_for('/js-mtp'); ?>/script.js"></script>
<?php echo $settings['google_analytics']; ?>
</head>

<body>

<div id="hWrapper">
    <header>
    	<div class="container">
        	<div class="top-nav">
            	<div class="pull-left">
                	<ul>
                    	<li><a href="<?php echo url_for('/locations'); ?>">Locations</a></li>
                        <li><a href="<?php echo url_for('/faq'); ?>">Faq</a></li>
                        <li><a href="<?php echo url_for('/policies'); ?>">Policies</a></li>
                        <li><a href="<?php echo url_for('/contact-us'); ?>">Contact Us</a></li>
                        <li><a href="<?php echo url_for('/about-us'); ?>">About Us</a></li>
                    </ul>
                </div>
                <div class="pull-right">
                	<ul>
                    	<li><a href="<?php echo url_for('/cart'); ?>">Cart (<span class="cart-total"><?php echo $cms->cartTotal(); ?></span>)</a></li>
                    <?php if($user->logged){ ?>
                    	<li>Welcome, <a href="<?php echo url_for('/account'); ?>"><?php echo ucwords($user->info['firstname'] . ' ' . $user->info['lastname']); ?></a></li>
                        <li><a href="<?php echo url_for('/logout'); ?>">Logout</a></li>
                    <?php }else{ ?>
                    	<li><a href="<?php echo url_for('/login'); ?>">Login</a></li>
                        <li><a href="<?php echo url_for('/register'); ?>">Register</a></li>
                    <?php } ?>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        	<div class="logo"><a href="<?php echo url_for('/'); ?>" class="logo"><img src="<?php echo img($settings['logo']); ?>" width="150px" /></a></div>
            <div class="navbar-nicchris">
                <ul class="nav-categories nav navbar-nav">
                	<li class="nav-small"><a href="#">Categories</a></li>
                <?php
                    if($cms->menus()){
                        foreach($cms->menus() as $menu){
                            if(!empty($menu['sub'])){
                                echo '<li class="dropdown' . (!empty($menu['active']) ? ' active' : '') . '">';
                                echo '<a' . (!empty($menu['href']) ? ' href="' . url_for($menu['href']) . '"' : '') . ' class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $menu['title'] . ' <span class="caret"></span></a>';
                                echo '<ul class="dropdown-menu">';
                                    foreach($menu['sub'] as $sub){
                                        echo '<li><a' . (!empty($sub['href']) ? ' href="' . url_for($sub['href']) . '"' : '') . '>' . $sub['title'] . '</a></li>';
                                    }
                                echo '</ul>';
                                echo '</li>';
                            }else{
                                echo '<li' . ($menu['href'] == $path ? ' class="active"' : '') . '><a' . (!empty($menu['href']) ? ' href="' . url_for($menu['href'] ). '"' : '') . '>' . $menu['title'] . '</a></li>';
                            }
                        }
                    }
                ?>    
                </ul>
            </div>
        
        	<?php /*
            <nav class="navbar navbar-default navbar-nicchris">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="<?php echo url_for('/'); ?>" class="logo"><img src="<?php echo img($settings['logo']); ?>" /></a>
                    </div>
                
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">                        
                        <ul class="nav navbar-nav">
						<?php
                            if($cms->menus()){
                                foreach($cms->menus() as $menu){
                                    if(!empty($menu['sub'])){
                                        echo '<li class="dropdown' . (!empty($menu['active']) ? ' active' : '') . '">';
										echo '<a' . (!empty($menu['href']) ? ' href="' . url_for($menu['href']) . '"' : '') . ' class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $menu['title'] . ' <span class="caret"></span></a>';
                                        echo '<ul class="dropdown-menu">';
                                        foreach($menu['sub'] as $sub){
                                            echo '<li><a' . (!empty($sub['href']) ? ' href="' . url_for($sub['href']) . '"' : '') . '>' . $sub['title'] . '</a></li>';
                                        }
                                        echo '</ul>';
                                        echo '</li>';
                                    }else{
                                        echo '<li' . ($menu['href'] == $path ? ' class="active"' : '') . '><a' . (!empty($menu['href']) ? ' href="' . url_for($menu['href'] ). '"' : '') . '>' . $menu['title'] . '</a></li>';
                                    }
                                }
                            }
                        ?>    
                        </ul>
                    </div>
                </div>
            </nav>
            */ ?>
            
            
        </div>  
    </header>
</div>

<div id="sWrapper">
	<?php echo $content; ?>
</div>

<div id="fWrapper">
	<footer>
    	<div class="container">
        	<div class="row">
            	<div class="col-md-3">
                	<h4>Shop By Category</h4>
                    <ul>
                    <?php
						if($cms->menus()){
							foreach($cms->menus() as $menu){
					?>
                    	<li><a href="<?php echo $menu['href']; ?>"><?php echo $menu['title']; ?></a></li>
                    <?php
							}
						}
					?>
                    </ul>
                </div>
                <div class="col-md-3">
                	<h4>About Us</h4>
                    <ul>
                    	<li><a href="<?php echo url_for('/contact-us'); ?>">Contact Us</a></li>
                        <li><a href="<?php echo url_for('/careers'); ?>">Careers</a></li>
                        <li><a href="#">Find A Store (Coming Soon)</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                	<h4>Help</h4>
                    <ul>
                    	<li><a href="<?php echo url_for('/faq'); ?>">FAQ</a></li>
                        <li><a href="<?php echo url_for('/redeem-a-gift-card'); ?>">Redeem A Gift Card</a></li>
                        <li><a href="<?php echo url_for('/store-pick-up'); ?>">Store Pick-Up</a></li>
                        <li><a href="<?php echo url_for('/shipping-info'); ?>">Shipping Info</a></li>
                        <li><a href="<?php echo url_for('/returns'); ?>">Returns</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                	<h4>Enquiries</h4>
                    <!--ul>
                    	<li><a href="<?php echo url_for('/commercial-enquiries'); ?>">Commercial Enquiries</a></li>
                        <li><a href="<?php echo url_for('/marketing-and-media-enquiries'); ?>">Marketing & Media Enquiries</a></li>
                    </ul-->
                    <ul>
                    	<li><a href="<?php echo url_for('/contact-us'); ?>">Commercial Enquiries</a></li>
                        <li><a href="<?php echo url_for('/contact-us'); ?>">Marketing & Media Enquiries</a></li>
                    </ul>
                </div>	
            </div>
            <div class="copy">© Copyright <?php echo date('Y'); ?> <?php echo SITE_NAME; ?>. All Right Reserved.</div>
        </div>
	</footer>	
</div>

</body>
</html>
