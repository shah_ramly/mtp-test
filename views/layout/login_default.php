<!DOCTYPE html>
<html lang="<?php echo !isset($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) || ($_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] == 'EN') ? 'en':'ms' ?>">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P7GCVWH');</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, width=device-width" />
<meta name="description" content="<?php echo $settings['meta_description']; ?>">
<meta name="keywords" content="<?php echo $settings['meta_keyword']; ?>">

<title><?php echo isset($header_title) ? $header_title : '' ?> | MakeTimePay </title>

<script type="text/javascript" src="<?php echo url_for('/assets/libs/jquery/jquery-2.1.0.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/jquery/jquery-migrate-1.2.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/jquery/jquery-ui.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/bootstrap/timepicker/moment-with-locales.js'); ?>"></script>
<!--script type="text/javascript" src="<?php echo url_for('/assets/libs/bootstrap/timepicker/bootstrap-datetimepicker.js'); ?>"></script-->
<script type="text/javascript" src="<?php echo url_for('/assets/libs/bxslider/jquery.bxslider.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/prettyPhoto/js/jquery.prettyPhoto.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/owl-carousel/owl.carousel.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/js'); ?>/bootbox.min.js"></script>
<script type="text/javascript" src="<?php echo url_for('/js-mtp'); ?>/script.js"></script>
 
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo url_for('/assets'); ?>/img/apple-icon.png">
<link rel="icon" type="image/png" href="<?php echo url_for('/assets'); ?>/img/favicon.png">

<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet" />
<link href="<?php echo url_for('/assets/css/glyphicons.css'); ?>" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<!-- Nucleo Icons -->
<link href="<?php echo url_for('/assets/css'); ?>/nucleo-icons.css" rel="stylesheet" />
<!-- CSS Files -->
<link href="<?php echo url_for('/assets/css/bootstrap-tagsinput.css'); ?>" rel="stylesheet">
<link href="<?php echo url_for('/assets/css/dropzone.min.css') ?>" rel="stylesheet">
<link href="<?php echo url_for('/assets/libs/bootstrap/timepicker/bootstrap-datetimepicker.css'); ?>" rel="stylesheet" />
<!-- Swiper CSS -->
<link href="<?php echo url_for('/assets/css/'); ?>/swiper.min.css" rel="stylesheet">
<link href="<?php echo url_for('/assets/css'); ?>/global.css" rel="stylesheet" />
<link href="<?php echo url_for('/assets/css'); ?>/intlTelInput.css" rel="stylesheet" />
<link href="<?php echo url_for('/assets/css'); ?>/style-shah.css<?php echo  '?' . option('style_version') ?>" rel="stylesheet" />
<link href="<?php echo url_for('/assets/css'); ?>/style-syafiq.css<?php echo  '?' . option('style_version') ?>" rel="stylesheet" />
<link href="<?php echo url_for('/assets/css'); ?>/style-byte2c.css" rel="stylesheet" />
<!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"-->
<?php echo $settings['google_analytics']; ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-191800673-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-191800673-1');
    </script>
</head>

<body class="white-content">

        <?php echo partial('layout/sidebar.html.php') ?>
        <div class="mob-navbar-toggler">
			<button type="button" class="navbar-toggler">
				<span class="navbar-toggler-bar bar1"></span>
				<span class="navbar-toggler-bar bar2"></span>
				<span class="navbar-toggler-bar bar3"></span>
			</button>
		</div>
        <div class="main-panel">
            <!-- End Navbar -->
            <div class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-chart">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-12 col-header-filter">
										<?php if($header_title == "Settings"){ $header_title = "My Preferences"; } ?>
                                        <h2 class="card-title-new"><?php echo isset($header_title) ? $header_title : '' ?></h2>
                                        <?php echo partial('workspace/partial/header.html.php'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="modal modal-search fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        </div>
                                        <div class="modal-body">
                                            <form class="modal-private-search-form">
                                                <div class="modal-input-filter-flex">
                                                    <div class="form-group form-group-search has-icon">
                                                        <span class="search-modal-icon">
                                                            <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                                                <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                                            </svg>
                                                        </span>
                                                        <input type="text" class="form-control search-input" placeholder="Search">
                                                        <span class="typeahead-close" title='clear the search'><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                                                <line x1="6" y1="6" x2="18" y2="18"></line>
                                                            </svg></span>
                                                        <button type="button" class="btn-linktxt btn-adv-search" id="btn_adv_search">Advanced Search</button>
                                                    </div>
                                                    <button type="submit" class="btn-icon-full">
                                                        <span class="btn-label">Search</span>
                                                        <span class="btn-icon">
                                                            <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                                            </svg>
                                                        </span>
                                                    </button>
                                                </div>
                                            </form>
                                            <div class="modal-advanced-filter-wrapper multi-collapse collapse">
                                                <div class="modal-advanced-filter-container">
                                                    <div class="modal-search-filter-row row-filter-taskjob">
                                                        <label class="modal-search-filter-lbl">Filter by task or job</label>
                                                        <div class="form-block-filter form-block-filter-flex">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="filtertype_1">
                                                                <label class="custom-control-label" for="filtertype_1">Task</label>
                                                            </div>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="filtertype_2">
                                                                <label class="custom-control-label" for="filtertype_2">Job</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-search-filter-row row-filter-location">
                                                        <label class="modal-search-filter-lbl">Filter by location</label>
                                                        <div class="form-block-filter form-block-filter-flex space-between">
                                                            <div class="form-filter-state">
                                                                <select class="form-control form-control-input" placeholder="State">
                                                                    <option>State</option>
                                                                    <option value="">Kuala Lumpur</option>
                                                                    <option value="">Selangor</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-filter-city">
                                                                <select class="form-control form-control-input" placeholder="City">
                                                                    <option disabled="" selected="">Town/City</option>
                                                                    <option value="">Petaling Jaya</option>
                                                                    <option value="">Shah Alam</option>
                                                                    <option value="">Subang Jaya</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-search-filter-row row-filter-dateposted">
                                                        <label class="modal-search-filter-lbl">Filter by date posted</label>
                                                        <div class="form-block-filter form-block-filter-flex space-between">
                                                            <div class="form-filter-dateposted">
                                                                <select class="form-control form-control-input" placeholder="Posted Date">
                                                                    <option disabled="" selected="">Date Posted</option>
                                                                    <option value=""><?php echo lang('dashboard_filter_any_time'); ?></option>
                                                                    <option value=""><?php echo sprintf(lang('public_search_last_x_hours'), 24); ?></option>
                                                                    <option value=""><?php echo sprintf(lang('public_search_last_x_days'), 3); ?></option>
                                                                    <option value=""><?php echo sprintf(lang('public_search_last_x_days'), 7); ?></option>
                                                                    <option value=""><?php echo sprintf(lang('public_search_last_x_days'), 14); ?></option>
                                                                    <option value=""><?php echo sprintf(lang('public_search_last_x_days'), 30); ?></option>
                                                                    <option value=""><?php echo lang('public_search_custom_range'); ?></option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group form-block-filter-flex space-between form-group-customrange">
                                                                <div class="form-filter-customrange-from">
                                                                    <div class="input-group input-group-datetimepicker date" id="form_datefilterfrom" data-target-input="nearest">
                                                                        <input type="text" class="form-control datetimepicker-input" data-target="#form_datefilterfrom" placeholder="From Date" />
                                                                        <span class="input-group-addon" data-target="#form_datefilterfrom" data-toggle="datetimepicker">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-filter-customrange-to">
                                                                    <div class="input-group input-group-datetimepicker date" id="form_datefilterto" data-target-input="nearest">
                                                                        <input type="text" class="form-control datetimepicker-input" data-target="#form_datefilterto" placeholder="To Date" />
                                                                        <span class="input-group-addon" data-target="#form_datefilterto" data-toggle="datetimepicker">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-search-filter-row row-filter-category">
                                                        <label class="modal-search-filter-lbl">Filter by category/subcategory</label>
                                                        <div class="form-block-filter form-block-filter-flex space-between">
                                                            <div class="form-filter-category">
                                                                <select class="form-control form-control-input" placeholder="Category">
                                                                    <option disabled="" selected="">Category</option>
                                                                    <option value="">Option 1</option>
                                                                    <option value="">Option 2</option>
                                                                    <option value="">Option 3</option>
                                                                    <option value="">Option 4</option>
                                                                    <option value="">Option 5</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-filter-subcategory">
                                                                <select class="form-control form-control-input" placeholder="Sub Category">
                                                                    <option disabled="" selected="">Sub Category</option>
                                                                    <option value="">Option 1</option>
                                                                    <option value="">Option 2</option>
                                                                    <option value="">Option 3</option>
                                                                    <option value="">Option 4</option>
                                                                    <option value="">Option 5</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-advanced-filter-footer form-block-filter-flex space-between">
                                                    <div class="modal-filter-clear">
                                                        <a href="#" class="deflink clear-filter-link">Clear all filters</a>
                                                    </div>
                                                    <div class="modal-filter-action form-block-filter-flex">
                                                        <button type="button" class="btn-icon-full btn-cancel">
                                                            <span class="btn-label">Cancel</span>
                                                            <span class="btn-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                        <button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                                                            <span class="btn-label">Apply</span>
                                                            <span class="btn-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                    <polyline points="20 6 9 17 4 12"></polyline>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
    <!-- /.modal -->
<div id="sWrapper">
    <?php echo $content; ?>
</div>
                            <?php echo partial('workspace/partial/modals.html.php'); ?>
<?php /*
<div id="fWrapper">
    <footer>
        
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h4>Shop By Category</h4>
                    <ul>
                    <?php
                        if($cms->menus()){
                            foreach($cms->menus() as $menu){
                    ?>
                        <li>
                            <!-- <a href="<?php //echo $menu['href']; ?>"><?php //echo $menu['title']; ?></a> -->
                            <a href="<?php echo url_for($menu['href'] ); ?>"><?php echo $menu['title']; ?></a>
                        </li>
                    <?php
                            }
                        }
                    ?>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4>About Us</h4>
                    <ul>
                        <li><a href="<?php echo url_for('/contact-us'); ?>">Contact Us</a></li>
                        <li><a href="<?php echo url_for('/careers'); ?>">Careers</a></li>
                        <!-- <li><a href="<?php //echo url_for('/find-a-store'); ?>">Find A Store</a></li> -->
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4>Help</h4>
                    <ul>
                        <li><a href="<?php echo url_for('/faq'); ?>">FAQ</a></li>
                        <!--li><a href="<?php echo url_for('/redeem-a-gift-card'); ?>">Redeem A Gift Card</a></li-->
                        <!--li><a href="<?php echo url_for('/store-pick-up'); ?>">Store Pick-Up</a></li-->
                        <li><a href="<?php echo url_for('/shipping-info'); ?>">Shipping Info</a></li>
                        <!--li><a href="<?php echo url_for('/returns'); ?>">Returns</a></li-->
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4>Enquiries</h4>
                    <!--ul>
                        <li><a href="<?php echo url_for('/commercial-enquiries'); ?>">Commercial Enquiries</a></li>
                        <li><a href="<?php echo url_for('/marketing-and-media-enquiries'); ?>">Marketing & Media Enquiries</a></li>
                    </ul-->
                    <ul>
                        <li><a href="<?php echo url_for('/contact-us'); ?>">Commercial Enquiries</a></li>
                        <li><a href="<?php echo url_for('/contact-us'); ?>">Marketing & Media Enquiries</a></li>
                    </ul>
                </div>  
            </div>
            <div class="copy">© Copyright <?php echo date('Y'); ?> <?php echo SITE_NAME; ?>. All Right Reserved.</div>
        </div>
    </footer>   
</div>
<?php */ ?>
	</div>
</body>


  <!--   Core JS Files   -->
    <script src="<?php echo url_for('/assets/js'); ?>/core/jquery.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/core/jquery-ui.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/core/popper.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/core/bootstrap.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!--  Google Maps Plugin    -->
    <!-- Chart JS -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/chartjs.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/dragscrollable.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/dragscroll.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/split.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
    <!--<script src="../assets/js/black-dashboard.min.js?v=1.0.0"></script>-->
    <script src="<?php echo url_for('/assets'); ?>/demo/demo.js"></script>



        <!--   Extraaa   Plugins JS Files   -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/moment-with-locales.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/tempusdominus-bootstrap-4.min.js"></script>


	<?php if(stripos(request_uri(), '/my_cv') !== false): ?>
	<script src="<?php echo url_for('/assets/js/plugins/bootstrap-tagsinput-modified.min.js') ?>"></script>
	<?php else: ?>
	<script src="<?php echo url_for('/assets/js/plugins/bootstrap-tagsinput.min.js') ?>"></script>
	<?php endif ?>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/swiper.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/bootstrap-slider.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/jquery.validate.js"></script>
    
    <!--  Search Suggestion Plugin    -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/typeahead.bundle.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/bloodhound.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/handlebars.js"></script>
    
    <!-- Chart JS -->
    <script src='<?php echo url_for('/assets/js'); ?>/plugins/snap.svg-min.js'></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/chart.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/progressbar.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/d3.v3.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/bootstrap-notify.js"></script>

    <!-- add summernote -->
    <link href="<?php echo url_for('/assets/libs/summernote'); ?>/summernote.min.css" rel="stylesheet">
    <script src="<?php echo url_for('/assets/libs/summernote'); ?>/summernote.min.js"></script>
    
    <!--  Custom JS    -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/dropzone.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/chat-sidebar.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/main.js<?php echo  '?' . option('script_version') ?>"></script>



    <!-- Sidebar Hover Function -->
    <script>
        // Sidebar Hover Function
        const $dropdown = $(".sidebar .dropdown");
        const $dropdownToggle = $(".sidebar .dropdown-toggle");
        const $dropdownMenu = $(".sidebar .dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function() {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                    function() {
                        const $this = $(this);
                        $this.addClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "true");
                        $this.find($dropdownMenu).addClass(showClass);
                    },
                    function() {
                        const $this = $(this);
                        $this.removeClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "false");
                        $this.find($dropdownMenu).removeClass(showClass);
                    }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });


        $('.card-header .dropdown-menu .nav-tabs .nav-link').on("click.bs.dropdown", function(e) {
            $(this).tab('show');
            e.stopPropagation();
        });
        $('.col-account-header .dropdown.col-account .switch-toggle .slider-toggler').on("click", function(e) {
            e.stopPropagation();
        });

    </script>
    <!-- Sidebar Hover Function -->

       <!-- Dispute Percentage Slider -->
    <script>
        // Dispute Percentage Slider
        if($("#disputePercentage").length) {
            var disputePercentage = new Slider("#disputePercentage", {
                min: 0,
                max: 100,
                value: 30,
                formatter: function (value) {
                    return value + '%';
                }
            });
        }

    </script>

    
    <script>
        (function($) {
            "use strict";
            $(document).ready(function() {
                if (document.querySelector('.end-of-chat')) {
                    document.querySelector('.end-of-chat').scrollIntoView();
                }
            });
    
        })(jQuery);
        
        
    </script>
    

   <!-- Write Review Modal - Rating Star Function -->
    <script>
        $(document).ready(function(){
        
            $("#timeliness_rating_group .btn-rating").on('click',(function(e) {

                var previous_value = $("#timeliness_rating").val();

                var selected_value = $(this).attr("data-attr");
                $("#timeliness_rating").val(selected_value);
                
                $(".timeliness-rating").empty();
                $(".timeliness-rating").html(selected_value);


                for (i = 1; i <= selected_value; ++i) {
                    $("#timeliness_rating_group #rating-star-"+i).toggleClass('btn-staractive');
                    $("#timeliness_rating_group #rating-star-"+i).toggleClass('btn-stardefault');
                }

                for (ix = 1; ix <= previous_value; ++ix) {
                    $("#timeliness_rating_group #rating-star-"+ix).toggleClass('btn-staractive');
                    $("#timeliness_rating_group #rating-star-"+ix).toggleClass('btn-stardefault');
                }

            }));
            
            $("#quality_rating_group .btn-rating").on('click',(function(e) {

                var previous_value = $("#quality_rating").val();

                var selected_value = $(this).attr("data-attr");
                $("#quality_rating").val(selected_value);
                
                $(".quality-rating").empty();
                $(".quality-rating").html(selected_value);


                for (i = 1; i <= selected_value; ++i) {
                    $("#quality_rating_group #rating-star-"+i).toggleClass('btn-staractive');
                    $("#quality_rating_group #rating-star-"+i).toggleClass('btn-stardefault');
                }

                for (ix = 1; ix <= previous_value; ++ix) {
                    $("#quality_rating_group #rating-star-"+ix).toggleClass('btn-staractive');
                    $("#quality_rating_group #rating-star-"+ix).toggleClass('btn-stardefault');
                }

            }));
            
            $("#communication_rating_group .btn-rating").on('click',(function(e) {

                var previous_value = $("#communication_rating").val();

                var selected_value = $(this).attr("data-attr");
                $("#communication_rating").val(selected_value);
                
                $(".communication-rating").empty();
                $(".communication-rating").html(selected_value);


                for (i = 1; i <= selected_value; ++i) {
                    $("#communication_rating_group #rating-star-"+i).toggleClass('btn-staractive');
                    $("#communication_rating_group #rating-star-"+i).toggleClass('btn-stardefault');
                }

                for (ix = 1; ix <= previous_value; ++ix) {
                    $("#communication_rating_group #rating-star-"+ix).toggleClass('btn-staractive');
                    $("#communication_rating_group #rating-star-"+ix).toggleClass('btn-stardefault');
                }

            }));
            $("#responsiveness_rating_group .btn-rating").on('click',(function(e) {

                var previous_value = $("#responsiveness_rating").val();

                var selected_value = $(this).attr("data-attr");
                $("#responsiveness_rating").val(selected_value);
                
                $(".responsiveness-rating").empty();
                $(".responsiveness-rating").html(selected_value);


                for (i = 1; i <= selected_value; ++i) {
                    $("#responsiveness_rating_group #rating-star-"+i).toggleClass('btn-staractive');
                    $("#responsiveness_rating_group #rating-star-"+i).toggleClass('btn-stardefault');
                }

                for (ix = 1; ix <= previous_value; ++ix) {
                    $("#responsiveness_rating_group #rating-star-"+ix).toggleClass('btn-staractive');
                    $("#responsiveness_rating_group #rating-star-"+ix).toggleClass('btn-stardefault');
                }

            }));


        });

    </script>
    

    <!-- Bar Chart Function -->
    <script>
        // Bar Chart Rounded Corner custom
        Chart.elements.Rectangle.prototype.draw = function() {

            var ctx = this._chart.ctx;
            var vm = this._view;
            var left, right, top, bottom, signX, signY, borderSkipped, radius;
            var borderWidth = vm.borderWidth;
            // Set Radius Here
            // If radius is large enough to cause drawing errors a max radius is imposed
            var cornerRadius = 12;

            if (!vm.horizontal) {
                // bar
                left = vm.x - vm.width / 2;
                right = vm.x + vm.width / 2;
                top = vm.y;
                bottom = vm.base;
                signX = 1;
                signY = bottom > top ? 1 : -1;
                borderSkipped = vm.borderSkipped || 'bottom';
            } else {
                // horizontal bar
                left = vm.base;
                right = vm.x;
                top = vm.y - vm.height / 2;
                bottom = vm.y + vm.height / 2;
                signX = right > left ? 1 : -1;
                signY = 1;
                borderSkipped = vm.borderSkipped || 'left';
            }

            // Canvas doesn't allow us to stroke inside the width so we can
            // adjust the sizes to fit if we're setting a stroke on the line
            if (borderWidth) {
                // borderWidth shold be less than bar width and bar height.
                var barSize = Math.min(Math.abs(left - right), Math.abs(top - bottom));
                borderWidth = borderWidth > barSize ? barSize : borderWidth;
                var halfStroke = borderWidth / 2;
                // Adjust borderWidth when bar top position is near vm.base(zero).
                var borderLeft = left + (borderSkipped !== 'left' ? halfStroke * signX : 0);
                var borderRight = right + (borderSkipped !== 'right' ? -halfStroke * signX : 0);
                var borderTop = top + (borderSkipped !== 'top' ? halfStroke * signY : 0);
                var borderBottom = bottom + (borderSkipped !== 'bottom' ? -halfStroke * signY : 0);
                // not become a vertical line?
                if (borderLeft !== borderRight) {
                    top = borderTop;
                    bottom = borderBottom;
                }
                // not become a horizontal line?
                if (borderTop !== borderBottom) {
                    left = borderLeft;
                    right = borderRight;
                }
            }

            ctx.beginPath();
            ctx.fillStyle = vm.backgroundColor;
            ctx.strokeStyle = vm.borderColor;
            ctx.lineWidth = borderWidth;

            // Corner points, from bottom-left to bottom-right clockwise
            // | 1 2 |
            // | 0 3 |
            var corners = [
                [left, bottom],
                [left, top],
                [right, top],
                [right, bottom]
            ];

            // Find first (starting) corner with fallback to 'bottom'
            var borders = ['bottom', 'left', 'top', 'right'];
            var startCorner = borders.indexOf(borderSkipped, 0);
            if (startCorner === -1) {
                startCorner = 0;
            }

            function cornerAt(index) {
                return corners[(startCorner + index) % 4];
            }

            // Draw rectangle from 'startCorner'
            var corner = cornerAt(0);
            ctx.moveTo(corner[0], corner[1]);

            for (var i = 1; i < 4; i++) {
                corner = cornerAt(i);
                nextCornerId = i + 1;
                if (nextCornerId == 4) {
                    nextCornerId = 0
                }

                nextCorner = cornerAt(nextCornerId);

                width = corners[2][0] - corners[1][0];
                height = corners[0][1] - corners[1][1];
                x = corners[1][0];
                y = corners[1][1];

                var radius = cornerRadius;

                // Fix radius being too large
                if (radius > height / 2) {
                    radius = height / 2;
                }
                if (radius > width / 2) {
                    radius = width / 2;
                }

                ctx.moveTo(x + radius, y);
                ctx.lineTo(x + width - radius, y);
                ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
                ctx.lineTo(x + width, y + height - radius);
                ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
                ctx.lineTo(x + radius, y + height);
                ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
                ctx.lineTo(x, y + radius);
                ctx.quadraticCurveTo(x, y, x + radius, y);

            }

            ctx.fill();
            if (borderWidth) {
                ctx.stroke();
            }
        };
        // Bar Chart Rounded Corner custom

        window.onload = function() {
            var barchart_canvas = document.getElementById('barchart_canvas');
            if(barchart_canvas){
                var ctx = barchart_canvas.getContext('2d');
                var gradient = ctx.createLinearGradient(0, 0, 0, 400);
                gradient.addColorStop(0, 'rgba(255,133,99,1)');
                gradient.addColorStop(1, 'rgba(237,89,43,1)');
                window.myBar = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
                        datasets: [{
                            data: [7, 11, 20, 19, 14, 10, 8],
                            backgroundColor: [
                                gradient,
                                gradient,
                                gradient,
                                gradient,
                                gradient,
                                gradient,
                                gradient,
                            ],
                            barPercentage: 0.5,
                        }]
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                            display: false,
                        },
                        title: {
                            display: false,
                        },
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    display: false,
                                }
                            }],
                            yAxes: [{
                                gridLines: {
                                    borderDash: [8, 4],
                                    color: "#eeeeee",
                                    drawBorder: false,
                                    lineWidth: 1,
                                },
                                ticks: {
                                    display: false,
                                    drawBorder: false
                                },
                            }]
                        }
                    }
                });
            }

        };

    </script>
      <!-- Bar Chart Function -->



 <script type="text/javascript">
        $(function() {
            $('#datetimepicker2').datetimepicker({
                locale: 'ru'
            });
        });

    </script>

    
    <!-- Donut Chart Function -->
    <script>
        // write text plugin
        Chart.pluginService.register({
            afterUpdate: function(chart) {
                if (chart.config.options.elements.center) {
                    var helpers = Chart.helpers;
                    var centerConfig = chart.config.options.elements.center;
                    var globalConfig = Chart.defaults.global;
                    var ctx = chart.chart.ctx;

                    var fontStyle = helpers.getValueOrDefault(centerConfig.fontStyle, globalConfig.defaultFontStyle);
                    var fontFamily = helpers.getValueOrDefault(centerConfig.fontFamily, globalConfig.defaultFontFamily);

                    if (centerConfig.fontSize)
                        var fontSize = centerConfig.fontSize;
                    // figure out the best font size, if one is not specified
                    else {
                        ctx.save();
                        var fontSize = helpers.getValueOrDefault(centerConfig.minFontSize, 1);
                        var maxFontSize = helpers.getValueOrDefault(centerConfig.maxFontSize, 256);
                        var maxText = helpers.getValueOrDefault(centerConfig.maxText, centerConfig.text);

                        do {
                            ctx.font = helpers.fontString(fontSize, fontStyle, fontFamily);
                            var textWidth = ctx.measureText(maxText).width;

                            // check if it fits, is within configured limits and that we are not simply toggling back and forth
                            if (textWidth < chart.innerRadius * 2 && fontSize < maxFontSize)
                                fontSize += 1;
                            else {
                                // reverse last step
                                fontSize -= 1;
                                break;
                            }
                        } while (true)
                        ctx.restore();
                    }

                    // save properties
                    chart.center = {
                        font: helpers.fontString(fontSize, fontStyle, fontFamily),
                        fillStyle: helpers.getValueOrDefault(centerConfig.fontColor, globalConfig.defaultFontColor)
                    };
                }
            },
            afterDraw: function(chart) {
                if (chart.center) {
                    var centerConfig = chart.config.options.elements.center;
                    var ctx = chart.chart.ctx;

                    ctx.save();
                    ctx.font = chart.center.font;
                    ctx.fillStyle = chart.center.fillStyle;
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
                    var centerY = ((chart.chartArea.top + 6) + chart.chartArea.bottom) / 2;
                    ctx.fillText(centerConfig.text, centerX, centerY);
                    ctx.restore();
                }
            },
        })


        var config = {
            type: 'doughnut',
            data: {
                labels: [
                    "Red",
                    "Gray"
                ],
                datasets: [{
                    data: [<?php echo $user->profileCompleteness(); ?>, <?php echo 100 - $user->profileCompleteness(); ?>],
                    backgroundColor: [
                        "#3644ad",
                        "#ccc"
                    ],
                    hoverBackgroundColor: [
                        "#3644ad",
                        "#ccc"
                    ]
                }]
            },
            options: {
                cutoutPercentage: 70,
                legend: {
                    display: false
                },
                tooltips: {
                    enabled: false
                },
                maintainAspectRatio: false,
                elements: {
                    arc: {
                        roundedCornersFor: 0
                    },
                    center: {
                        // the longest text that could appear in the center
                        maxText: '100%',
                        text: '<?php echo $user->profileCompleteness(); ?>%',
                        fontColor: '#3644ad',
                        fontFamily: "'Poppins', 'Helvetica', 'Arial', sans-serif",
                        fontStyle: 'normal',
                        // fontSize: 12,
                        // if a fontSize is NOT specified, we will scale (within the below limits) maxText to take up the maximum space in the center
                        // if these are not specified either, we default to 1 and 256
                        minFontSize: 16,
                        maxFontSize: 22,
                    }
                }
            }
        };


        if( $('#doProfile').length ){
            var ctx = document.getElementById("doProfile").getContext("2d");
            var doProfile = new Chart(ctx, config);
        }

    </script>
    <!-- Donut Chart Function -->



    
    <!-- Donut Chart Function -->
    <script>
        var doughnut = document.getElementById("doChart");
        if(doughnut){
            var myDoughnutChart = new Chart(doughnut, {
                type: 'doughnut',
                data: {
                    labels: ["Credit Card", "Bank Transfer"],
                    datasets: [{
                        data: [4100, 2500],
                        backgroundColor: ['#3644ad', '#f0724c'],
                        borderColor: ['#3644ad', '#f0724c'],
                        hoverBorderWidth: 10,
                        hoverRadius: 1,
                        borderWidth: 0,
                    }]
                },
                options: {
                    defaultFontFamily: Chart.defaults.global.defaultFontFamily = "'Poppins'",
                    legend: {
                        display: false,
                        position: 'bottom'
                    },
                    cutoutPercentage: 70,
                    maintainAspectRatio: false,
                    responsive: true,
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];
                                var precentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                return precentage + "%";
                            }
                        }
                    }
                }
            });
        }

    </script>
    <!-- Donut Chart Function -->

    <!-- Progress Bar Function -->
    <script>
        // progressbar.js@1.0.0 version is used
        // Docs: https://progressbarjs.readthedocs.org/en/1.0.0/

        if($('#progress_1').length) {
            var bar1 = new ProgressBar.Line(progress_1, {
                strokeWidth: 1,
                easing: 'easeInOut',
                duration: 1400,
                color: '#655f93',
                trailColor: '#eee',
                trailWidth: 1,
                svgStyle: {
                    width: '100%',
                    height: '100%'
                },
                text: {
                    style: {
                        // Text color.
                        // Default: same as stroke color (options.color)
                    },
                    autoStyleContainer: false
                },
                step: (state, bar) => {
                    bar.setText(Math.round(bar.value() * 100) + ' %');
                }
            });


            bar1.animate(0.25); // Number from 0.0 to 1.0
        }
        if($('#progress_2').length) {
            var bar2 = new ProgressBar.Line(progress_2, {
                strokeWidth: 1,
                easing: 'easeInOut',
                duration: 1400,
                color: '#007cf0',
                trailColor: '#eee',
                trailWidth: 1,
                svgStyle: {
                    width: '100%',
                    height: '100%'
                },
                text: {
                    style: {
                        // Text color.
                        // Default: same as stroke color (options.color)
                    },
                    autoStyleContainer: false
                },
                step: (state, bar) => {
                    bar.setText(Math.round(bar.value() * 100) + ' %');
                }
            });


            bar2.animate(0.62); // Number from 0.0 to 1.0
        }
        if($('#progress_3').length) {
            var bar3 = new ProgressBar.Line(progress_3, {
                strokeWidth: 1,
                easing: 'easeInOut',
                duration: 1400,
                color: '#00c3bc',
                trailColor: '#eee',
                trailWidth: 1,
                svgStyle: {
                    width: '100%',
                    height: '100%'
                },
                text: {
                    style: {
                        // Text color.
                        // Default: same as stroke color (options.color)
                    },
                    autoStyleContainer: false
                },
                step: (state, bar) => {
                    bar.setText(Math.round(bar.value() * 100) + ' %');
                }
            });


            bar3.animate(0.34); // Number from 0.0 to 1.0
        }
    </script>
    <!-- Progress Bar Function -->


       <!-- Initialize Progress Bar -->
    <script>
        if($('#progress_ongoingtask_1').length) {
            var bar1 = new ProgressBar.Line(progress_ongoingtask_1, {
                strokeWidth: 1,
                easing: 'easeInOut',
                duration: 1400,
                color: '#655F93',
                trailColor: '#eee',
                trailWidth: 1,
                svgStyle: {width: '100%', height: '100%'},
                text: {
                    style: {
                        // Text color.
                        // Default: same as stroke color (options.color)
                    },
                    autoStyleContainer: false
                },
                step: (state, bar) => {
                    bar.setText(Math.round(bar.value() * 100) + ' %');
                }
            });

            bar1.animate(0.25);  // Number from 0.0 to 1.0
        }
    </script>
    <!-- Initialize Progress Bar -->

    <script type="text/javascript" src="<?php echo url_for('/assets/libs/blueimp/iframe-transport.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo url_for('/assets/libs/blueimp/fileupload.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo url_for('/js-mtp'); ?>/uploader.js"></script>
</html>


