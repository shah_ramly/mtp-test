
<div class="sidebar">
	<div class="sidebar-wrapper">
		<div class="logo">
			<img src="<?php echo url_for('/assets/img/logo.png'); ?>" alt="" /> </div>
		<ul class="nav top">
			<li class="nav-item <?php echo stripos(request_uri(), 'dashboard') || stripos(get_latest_url(), 'dashboard') !== false ? 'active' : '' ?>">
				<a href="<?php echo url_for('dashboard'); ?>">
					<p><?php echo lang('menu_dashboard'); ?></p>
				</a>
			</li>
			<li class="nav-item nav-taskcentre<?php echo stripos(request_uri(), 'task-centre') || stripos(get_latest_url(), 'task-centre') !== false ? ' active' : '' ?><?php echo isset($tasks_notification) && $tasks_notification ? ' new' : '' ?>">
				<a href="<?php echo $user->info['type'] !== '1' ? url_for('workspace/task-centre/applied/list') : url_for('workspace/task-centre/posted/list'); ?>">
					<p><?php echo lang('menu_task_centre'); ?></p>
				</a>
			</li>
			<?php if((int)$user->info['type'] === 0): ?>
			<li class="nav-item nav-jobcentre<?php echo stripos(request_uri(), 'job-centre') || stripos(get_latest_url(), 'job-centre') !== false ? ' active' : '' ?><?php echo isset($jobs_notification) && $jobs_notification ? ' new' : '' ?>">
				<a href="<?php echo url_for('/workspace/job-centre/') ?>">
					<p><?php echo lang('menu_job_centre'); ?></p>
				</a>
			</li>
			<?php endif ?>
			<?php if((int)$user->info['type'] === 0): ?>
			<li class="nav-item  <?php echo stripos(request_uri(), 'earning') || stripos(request_uri(), 'spent') || stripos(get_latest_url(), 'billings') !== false ? 'active' : '' ?>">
				<a href="<?php echo url_for('workspace/billings/earning'); ?>">
					<p><?php echo lang('menu_financial'); ?></p>
				</a>
			</li>
			<?php endif ?>
			<?php if((int)$user->info['type'] === 1): ?>
			<li class="nav-item  <?php echo stripos(request_uri(), 'spent') || stripos(get_latest_url(), 'spent') !== false ? 'active' : '' ?>">
				<a href="<?php echo url_for('workspace/billings/spent'); ?>">
					<p><?php echo lang('menu_financial'); ?></p>
				</a>
			</li>
			<?php endif ?>

			<li class="nav-item  <?php echo stripos(request_uri(), 'rating') || stripos(get_latest_url(), 'rating') !== false ? 'active' : '' ?>">
				<a href="<?php echo url_for('workspace/rating-centre'); ?>">
					<p><?php echo lang('menu_ratings'); ?></p>
				</a>
			</li>
			
			<li class="nav-item analytics  <?php echo stripos(request_uri(), 'analytics') || stripos(get_latest_url(), 'analytics') !== false ? 'active' : '' ?>">
				<a href="<?php echo url_for('/analytics'); ?>">
					<p><?php echo lang('menu_analytics'); ?></p>
				</a>
			</li>
		</ul>
	</div>
</div>
