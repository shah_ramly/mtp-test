<!DOCTYPE html>
<html lang="<?php echo !isset($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) || ($_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] == 'EN') ? 'en':'ms' ?>">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P7GCVWH');</script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="user-scalable=no, width=device-width" />
	<meta name="description" content="<?php echo $settings['meta_description']; ?>">
	<meta name="keywords" content="<?php echo $settings['meta_keyword']; ?>">

	<title><?php echo isset($header_title) ? $header_title : '' ?> | MakeTimePay </title>

	<!--<script type="text/javascript" src="<?php /*echo url_for('/assets/libs/jquery/jquery-2.1.0.min.js'); */?>"></script>
	<script type="text/javascript" src="<?php /*echo url_for('/assets/libs/jquery/jquery-migrate-1.2.1.min.js'); */?>"></script>
	<script type="text/javascript" src="<?php /*echo url_for('/assets/libs/jquery/jquery-ui.js'); */?>"></script>
	<script type="text/javascript" src="<?php /*echo url_for('/assets/libs/bootstrap/js/bootstrap.min.js'); */?>"></script>
	<script type="text/javascript" src="<?php /*echo url_for('/assets/libs/bootstrap/timepicker/moment-with-locales.js'); */?>"></script>
	<script type="text/javascript" src="<?php /*echo url_for('/assets/libs/bootstrap/timepicker/bootstrap-datetimepicker.js'); */?>"></script>
	<script type="text/javascript" src="<?php /*echo url_for('/assets/libs/plupload/plupload.full.min.js'); */?>"></script>
	<script type="text/javascript" src="<?php /*echo url_for('/assets/libs/plupload/plupload.min.js'); */?>"></script>
	<script type="text/javascript" src="<?php /*echo url_for('/assets/libs/notifyjs/notify.min.js'); */?>"></script>
	<script type="text/javascript" src="<?php /*echo url_for('/assets/libs/notifyjs/metro/notify-metro.js'); */?>"></script>
	<script type="text/javascript" src="<?php /*echo url_for('/assets/libs/bxslider/jquery.bxslider.js'); */?>"></script>
	<script type="text/javascript" src="<?php /*echo url_for('/assets/libs/prettyPhoto/js/jquery.prettyPhoto.js'); */?>"></script>
	<script type="text/javascript" src="<?php /*echo url_for('/assets/libs/owl-carousel/owl.carousel.min.js'); */?>"></script>
	<script type="text/javascript" src="<?php /*echo url_for('/assets/js'); */?>/bootbox.min.js"></script>
	<script type="text/javascript" src="<?php /*echo url_for('/assets/js'); */?>/script.js"></script>-->
	 <meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo url_for('/assets'); ?>/img/apple-icon.png">
	<link rel="icon" type="image/png" href="<?php echo url_for('/assets'); ?>/img/favicon.png">

	<!--     Fonts and icons     -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet" />
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<!-- Nucleo Icons -->
	<link href="<?php echo url_for('/assets/css'); ?>/nucleo-icons.css" rel="stylesheet" />

	<?php echo $settings['google_analytics']; ?>

	<link href="<?php echo url_for('/assets/css'); ?>/glyphicons.css" rel="stylesheet">

	<!-- CSS Files -->
	<link href="<?php echo url_for('/assets/css'); ?>/animate.min.css" rel="stylesheet">
	<link href="<?php echo url_for('/assets/css'); ?>/bootstrap-dropdownhover.min.css" rel="stylesheet">

	<link href="<?php echo url_for('/assets/css'); ?>/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />
	<link href="<?php echo url_for('/assets/css'); ?>/bootstrap-tagsinput.css" rel="stylesheet">
	<link href="<?php echo url_for('/assets/css'); ?>/dropzone.min.css" rel="stylesheet">
	<link href="<?php echo url_for('/assets/css'); ?>/typeahead.css?v=1.0.1" rel="stylesheet" />
	<link href="<?php echo url_for('/assets/css'); ?>/bootstrap-slider.min.css?v=1.0.1" rel="stylesheet" />
	<!-- CSS Files -->
	<link href="<?php echo url_for('/assets/css'); ?>/global.css" rel="stylesheet" />
	<link href="<?php echo url_for('/assets/css'); ?>/style-shah.css<?php echo  '?' . option('style_version') ?>" rel="stylesheet" />
	<link href="<?php echo url_for('/assets/css'); ?>/style-syafiq.css<?php echo  '?' . option('style_version') ?>" rel="stylesheet" />
	<link href="<?php echo url_for('/assets/css/style-byte2c.css') . '?' . option('style_version') ?>" rel="stylesheet" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-191800673-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-191800673-1');
    </script>
</head>

<body class="white-content page-pub">
	<div class="public-wrapper">
		<header>
			<div class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light" id="navbarMain">
                    <a class="navbar-brand gtm-search-logo" href="<?php echo url_for('/') ?>">
                        <div class="logo"><img src="<?php echo url_for('assets/img/mtp_logo.png') ?>" alt=""></div>
                    </a>
                    <div class="lang-toggle">
                        <div class="menu-lang menu-lang-mobile">
                            <ul class="navbar-nav">
                                <li>
                                    <a href="<?php echo url_for('/lang/EN'); ?>" class="gtm-search-menu-en">EN</a>
                                </li>
                                <li>
                                    <a href="<?php echo url_for('/lang/BM'); ?>" class="gtm-search-menu-bm">BM</a>
                                </li>
                            </ul>
                        </div>
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarMainList" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="navbarMainList">
                        <div class="navbar-collapse-container">
                            <div class="menu-main">
                                <ul class="navbar-nav">
                                    <li>
                                        <a href="<?php echo url_for('/'); ?>" class="gtm-menu-search-home"><?php echo lang('title_home'); ?></a>
                                    </li>
	                                <li>
		                                <a href="<?php echo url_for('https://maketimepay.com/blog') ?>" class='gtm-menu-blog' target="_blank"><?php echo lang('title_blog'); ?></a>
	                                </li>
                                    <li class="sub-menu-mobile">
                                        <a href="#introduction" class="gtm-menu-search-intro"><?php echo lang('title_introduction'); ?></a>
                                    </li>
                                    <li class="sub-menu-mobile">
                                        <a href="#howitworks" class="gtm-menu-search-hiw"><?php echo lang('title_how_it_works'); ?></a>
                                    </li>
                                    <li class="sub-menu-mobile">
                                        <a href="#ourpromise" class="gtm-menu-search-promise"><?php echo lang('title_our_promise'); ?></a>
                                    </li>
                                    <li class="sub-menu-mobile">
                                        <a href="#subscriptionplans" class="gtm-menu-search-plan"><?php echo lang('title_subscription_plan'); ?></a>
                                    </li>
                                    <li>
                                        <?php
                                        if(isset($_SESSION['WEB_USER_ID'])){ ?>
		                                    <a href="<?php echo url_for('/dashboard'); ?>" class="gtm-menu-search-dashboard"><?php echo lang('title_dashboard'); ?></a>
                                            <?php
                                        }else{
                                            ?>
		                                    <a href="<?php echo url_for('/login'); ?>" class="gtm-menu-search-login"><?php echo lang('title_log_in'); ?></a>
                                        <?php } ?>
                                    </li>
                                    <?php if(!$user->logged): ?>
                                    <li>
	                                    <a href="<?php echo url_for('/sign_up') ?>" class="btn-main btn-header btn-signup"><?php echo lang('title_sign_up'); ?></a>
                                    </li>
                                    <?php endif ?>
                                </ul>
                            </div>
                            <div class="menu-lang">
                                <ul class="navbar-nav">
                                    <li>
                                        <a href="<?php echo url_for('/lang/EN'); ?>" class="gtm-pub-search-menu-en">EN</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo url_for('/lang/BM'); ?>" class="gtm-pub-search-menu-bm">BM</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
		</header>
		<?php echo $content; ?>
		<footer>
			<div class="container">
				<div class="footer-container">
					<div class="footer-col-left">
						<div class="footer-logo"><img src="<?php echo url_for('/assets/img/logo.png') ?>" alt="" /></div>
						<div class="footer-copyright">
							<span class="copyright-year"><?php echo date("Y"); ?> MTP</span>
							<span class="copyright-desc">All Rights Reserved</span>
						</div>
					</div>
					<!--div class="footer-col-right">
						<div class="footer-col footer-col-discover">
							<label class="footer-lbl">Discover</label>
							<ul class="footer-list">
								<li><a href="#">How it works</a></li>
								<li><a href="#">Airtasker for business</a></li>
								<li><a href="#">Earn money</a></li>
								<li><a href="#">New users FAQ</a></li>
								<li><a href="#">How it works</a></li>
								<li><a href="#">Airtasker for business</a></li>
								<li><a href="#">Earn money</a></li>
								<li><a href="#">New users FAQ</a></li>
							</ul>
						</div>
						<div class="footer-col footer-col-aboutus">
							<label class="footer-lbl">About us</label>
							<ul class="footer-list">
								<li><a href="#">Careers</a></li>
								<li><a href="#">Media enquiries</a></li>
								<li><a href="#">Community guidelines</a></li>
								<li><a href="#">Tasker principles</a></li>
								<li><a href="#">Careers</a></li>
								<li><a href="#">Media enquiries</a></li>
								<li><a href="#">Community guidelines</a></li>
								<li><a href="#">Tasker principles</a></li>
							</ul>
						</div>
						<div class="footer-col footer-col-contactus">
							<label class="footer-lbl">Contact us</label>
							<ul class="footer-list">
								<li><a href="#">Privacy policy</a></li>
								<li><a href="#">Existing members</a></li>
								<li><a href="#">Post a task</a></li>
								<li><a href="#">Browse tasks</a></li>
								<li><a href="#">Privacy policy</a></li>
								<li><a href="#">Existing members</a></li>
								<li><a href="#">Post a task</a></li>
								<li><a href="#">Browse tasks</a></li>
							</ul>
						</div>
					</div-->
				</div>
				<div class="subfooter-container">
					<div class="subfooter-col-left">
                        <ul class="subfooter-list">
                            <li><span class="subfooter-lbl gtm-footer-tnc" data-toggle="modal" data-target="#modal_t_c"><?php echo lang('sign_up_tnc_1'); ?></span></li>
                            <li><span class="subfooter-lbl gtm-footer-pdpa" data-toggle="modal" data-target="#modal_pdpa"><?php echo lang('sign_up_tnc_2'); ?></span></li>
                            <li class="subfooter-email">
                                <a href="mailto:query@maketimepay.com" class="gtm-footer-query">
                                    <span class="subfooter-email-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                                        <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555zM0 4.697v7.104l5.803-3.558L0 4.697zM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757zm3.436-.586L16 11.801V4.697l-5.803 3.546z"/>
                                        </svg>
                                    </span>
                                    <span class="subfooter-email-val">query@maketimepay.com</span>
                                </a>
                            </li>
                        </ul>
					</div>
					 <div class="subfooter-col-right">
                        <a href="https://www.facebook.com/maketimepay" target="_blank" class="gtm-footer-fb">
                            <span class="subfooter-icon icon-fb">
                                <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" class="svg-inline--fa fa-facebook-f fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path></svg>
                            </span>
                        </a>
                        <a href="https://www.instagram.com/maketimepay" target="_blank" class="gtm-footer-ig">
                            <span class="subfooter-icon icon-ig">
                                <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" class="svg-inline--fa fa-instagram fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path>
                                </svg>
                            </span>
                        </a>
                        <a href="https://www.twitter.com/maketimepay" target="_blank" class="gtm-footer-tw">
                            <span class="subfooter-icon icon-tw">
                                <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter" class="svg-inline--fa fa-twitter fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></svg>
                            </span>
                        </a>
                        <a href="https://linkedin.com/company/maketimepay" target="_blank" class="gtm-footer-li">
                            <span class="subfooter-icon icon-in">
                                <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin-in" class="svg-inline--fa fa-linkedin-in fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"></path></svg>
                            </span>
                        </a>
                        <!-- <a href="#" target="_blank">
                            <span class="subfooter-icon icon-yt">
                                <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="youtube" class="svg-inline--fa fa-youtube fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"></path></svg>
                            </span>
                        </a> -->
                    </div>
				</div>
			</div>
		</footer>

    </div>
    

    <!-- Modal - T&C -->
    <div id="modal_t_c" class="modal modal-confirm fade" aria-labelledby="modal_t_c" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"><?php echo lang('sign_up_terms_of_use'); ?></div>
	            <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
		            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
			            <line x1="18" y1="6" x2="6" y2="18"></line>
			            <line x1="6" y1="6" x2="18" y2="18"></line>
		            </svg>
	            </button>
                <div class="modal-body">
                    <ul class="nav nav-pills nav-pills-help" id="pills-tabTnc" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="tncEnTab-tab" data-toggle="pill" href="#tncEnTab" role="tab" aria-controls="tncEnTab" aria-selected="true">English</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tncBmTab-tab" data-toggle="pill" href="#tncBmTab" role="tab" aria-controls="tncEnTab" aria-selected="false">BM</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContentTnc">
                        <div class="tab-pane fade show active" id="tncEnTab" role="tabpanel">
                            <div class="tnc-rows-container">
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">1.</span>
                                        <span class="tnc-lbl">General</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">These are the terms of use and services (“<b>Terms</b>”) of MakeTimePay Sdn Bhd (Company No: 1373558-V). The access to and use of this website and/or mobile application and/or the services on this website and/or mobile application (collectively, the “<b>Services</b>”) are subject to these Terms.</p>
                                        <p class="modal-para">The Services are an online ecosystem that facilitates the proliferation of opportunities primarily for on-demand work for the Users.</p>
                                        <p class="modal-para">By accessing and using the Services, you agree to comply with and be bound by these Terms which shall be applicable to all Users of the Services. If you are using and accessing the Services for and on behalf of a business entity, the term “you” in these Terms shall mean the business entity in which you are representing in using the Services, unless the context does not permit.</p>
                                        <p class="modal-para">You are advised to read these Terms carefully as these affect your rights and liabilities under applicable laws and regulations in Malaysia. If you do not agree to these Terms, you should not use the Services.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">2.</span>
                                        <span class="tnc-lbl">Key terms</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">The following terms shall have the following respective meanings:</p>
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">2.1</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Account</b>” means an account created with MTP via the website and/or mobile application for full use of and access to the Services;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.2</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Applicant</b>” means a Job Applicant or a Task Applicant;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.3</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Application</b>” has the definition prescribed to it under Clause 8.3(a);</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.4</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Engagement</b>” means Task Engagement and/or Job Engagement;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.5</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Intellectual Property Rights</b>” means any patent, copyright, registered design, trade mark, right in design, service mark, right under licence or other industrial or intellectual property right, whether or not any of them are registered and including applications for registration of any of the foregoing and all forms of protection of a similar nature or having similar effect which may subsist in Malaysia and/or anywhere in the world.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.6</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Job</b>” means a full-time employment position that is payable with the Job Fees;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.7</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Job Applicant</b>” means a Job Seeker who has successfully submitted an application for a Job Listing in accordance with Clause 8.3;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.8</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Job Engagement</b>” has the definition prescribed to it under Clause 10.2;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.9</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Job Fees</b>” means the wages payable by the Job by the Job Poster to the Job Seeker pursuant to the delivery of services on terms to be mutually agreed upon by the Job Seeker and the Job Poster;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.10</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Job Holder</b>” means a Job Applicant that has been successfully engaged by a Job Poster in accordance with Clause 10.2;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.11</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Job Poster</b>” means Users that are accessing and/or using the Services for the purposes of posting Task Listings and seeking candidates to carry out tasks for and on its behalf;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.12</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Job Seeker</b>” means Users that are accessing and/or using the Services in search of a Job(s) and/or other similar income opportunities;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.13</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Listings</b>” has the definition prescribed to it under Clause 7.1;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.14</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>MTP</b>” means MakeTimePay Sdn Bhd [Company No: 1373558-V], having its registered office at no.16-2, 2nd floor, Jalan 1/76C, Desa Pandan, 55100 Kuala Lumpur, Wilayah Persekutuan, Malaysia;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.15</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Posters</b>” mean Task Posters and Job Posters;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.16</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Seekers</b>” mean Task Seekers and Job Seekers;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.17</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Services</b>” means the various services and functions that are made available on this website and/or mobile application as provided by MTP;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.18</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Task</b>” means a project or short-term assignment for whatever duration necessary that is payable, upon completion, with the Task Fees;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.19</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Task Applicant</b>” means a Task Seeker who has successfully submitted an application for a Task Listing in accordance with Clause 8.3;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.20</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Task Engagement</b>” has the definition prescribed to it under Clause 10.1;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.21</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Task Fees</b>” means the fees payable by the Task Poster to the Task Seeker pursuant to the completion of a Task(s) as delivered by the Task Seeker in accordance with the Task Listings;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.22</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Task Holder</b>” means a Task Applicant that has been successfully engaged by a Task Poster in accordance with Clause 10.1;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.23</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Task Seeker</b>” means Users that are accessing and/or using the Services in search of a Task(s) and/or other similar income opportunities;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.24</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Third-Party Websites and/or Social Media Platforms</b>” has the definition prescribed to it under Clause 7.1;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.25</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>User</b>” means any person, individual or corporate entity using the Services as provided by MTP, including but not limited to Seekers and Posters.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">3.</span>
                                        <span class="tnc-lbl">Effect</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">3.1</span>
                                                <span class="tnc-desc"><p class="modal-para">We reserve our right to change, modify, add or remove these Terms or any part thereof, at any time. Changes will be effective when posted via the Services. You may determine when we last changed these Terms by referring to the ‘Last Updated’ statement above. We shall not be responsible for any damage suffered or sustained by you in connection with your failure to understand the amended Terms. Your continued access to and use of the Services following the posting of the amended Terms constitute your acceptance to abide and be bound by the amended Terms.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">3.2</span>
                                                <span class="tnc-desc"><p class="modal-para">You shall comply with these Terms and shall be solely responsible for all loses and damages arising out of or in connection with your breach or failure to comply with these Terms.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">4.</span>
                                        <span class="tnc-lbl">Right to use and restrictions</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">4.1</span>
                                                <span class="tnc-desc"><p class="modal-para">You are responsible for the access to and use of the Services, including obtaining the necessary data network access, even if the access is by another person.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.2</span>
                                                <span class="tnc-desc"><p class="modal-para">You are accessing and using the Services at your own risk. We shall not be liable for any damage to, or viruses or other codes that may affect any hardware or device, software, data or other property as a result of your access to and use of the Services.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.3</span>
                                                <span class="tnc-desc"><p class="modal-para">You may only access the Services using authorised means. It is your responsibility to check and ensure that you have downloaded and/or updated the correct software for the hardware and device for your access to and use of the Services. We do not guarantee that the Services, or any part thereof, will function as intended on any particular hardware or device. We are not liable if you do not have compatible hardware or a compatible device or if you have downloaded the wrong version of the software on your hardware or device.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.4</span>
                                                <span class="tnc-desc"><p class="modal-para">You shall access and use the Services in accordance with any instructions for access to and use of the Services which we may make from time to time, these Terms and regulations at the time being in force in Malaysia.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.5</span>
                                                <span class="tnc-desc"><p class="modal-para">You shall not:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">impersonate any person or party or falsely declare, distort or misrepresent your affiliation with any person or party in connection with the Services, or express or imply that we endorse any statement you make;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">in any way damage or disrupt the Services and/or the operation of the Services;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">use the Services for any fraudulent, unauthorised or unlawful purpose;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">deliberately and knowingly provide information or content that is false, misleading, inaccurate, illegal, threatening, obscene, hateful, libellous or defamatory;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(e)</span>
                                                            <span class="tnc-desc"><p class="modal-para">use the Services to defame, abuse, harass, stalk, threaten or otherwise violate the rights of others, including without limitation other’s privacy rights;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(f)</span>
                                                            <span class="tnc-desc"><p class="modal-para">use the Services in a manner that may be contrary to our interests and/or the interest of other Users;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(g)</span>
                                                            <span class="tnc-desc"><p class="modal-para">introduce or allow the introduction of computer viruses and/or other computer programming routines such as any viruses, malware, unsolicited e-mails, Trojan horses, trap doors, back doors, worms, time bombs or cancelbots that may damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or personal information, relating to or in connection with the Services;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(h)</span>
                                                            <span class="tnc-desc"><p class="modal-para">reproduce, duplicate, copy, sell, resell or otherwise exploit for any commercial purposes, any portion of, use of, or access to the Services;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(i)</span>
                                                            <span class="tnc-desc"><p class="modal-para">modify, adapt, translate, reverse engineer, decompile or disassemble any portion of the Services;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(j)</span>
                                                            <span class="tnc-desc"><p class="modal-para">infringe upon any other person’s proprietary rights, or remove any copyright, trade mark or other proprietary rights notice from the Services or materials origination from the Services; and/or</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(k)</span>
                                                            <span class="tnc-desc"><p class="modal-para">use the Services in a manner that is in breach of any laws and regulations in Malaysia.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.6</span>
                                                <span class="tnc-desc"><p class="modal-para">We reserve the right to:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">check, vet and/or control any activity, content or information occurring on or through the Services;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">investigate any infringement of these Terms and take any appropriate action thereafter;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">restrict your access to and use of the Services if you violate any of the items stated in Clause 4.5 above; and/or</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">report any suspicious activity concerning the possible transgression of any applicable law or regulation to the appropriate authorities and to cooperate with such authorities.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.7</span>
                                                <span class="tnc-desc"><p class="modal-para">Subject to your compliance with Clause 4.5, you are permitted to upload and share material, information and content in respect of your use of the Services.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.8</span>
                                                <span class="tnc-desc"><p class="modal-para">You agree and acknowledge that you are solely responsible and liable for any material, information and content that you upload and share in respect of your use of the Services and any loss or damage which you or any third party suffer as a result of such material, information and content is solely your responsibility.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.9</span>
                                                <span class="tnc-desc"><p class="modal-para">You agree and understand that we shall have the sole discretion to refuse, delete or remove any material, information and content that you have uploaded and shared via the Services, including but not limited to the following:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Where such material, information and/or content are in breach of these Terms or violate any law or regulation;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Where such material, information and/or content contain incomplete, false or inaccurate information about any other User or person;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Where we have received a complaint from other Users of the Services, which following an investigation by us, is found to be legitimate, well-founded and/or genuine; and/or</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Where we have received a notice or complaint of infringement, which following an investigation by us, is found to be legitimate, well-founded and/or genuine.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">5.</span>
                                        <span class="tnc-lbl">Availability</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">5.1</span>
                                                <span class="tnc-desc"><p class="modal-para">We endeavour to make the Services available twenty-four (24) hours a day, however we cannot be liable if for any reason the Services are unavailable for any time or for any period. We make no warranty or guarantee that your access to and use of the Services will be continuously uninterrupted or error-free.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">5.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Access to and use of the Services may be suspended or withdrawn temporarily or permanently at any time and without notice if such action is deemed necessary by us. We may temporarily suspend the provision of the Services due to repair, maintenance, checking, replacement, breakdown of communication facilities or introduction of new facilities and functions. We shall not be liable for any damages or losses that you may suffer or sustain as a result of temporary or permanent suspension of the Services.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">6.</span>
                                        <span class="tnc-lbl">Confidentiality</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">Through your access to and use of the Services, you may be allowed to access or view information which is confidential in nature, whether or not such information is expressly stated to be confidential. Such information may pertain to us or to other parties. Where you are able to access or view such information, you warrant and represent that unless such disclosure is expressly provided for under these Terms, you will not and will not allow other parties to disclose or reproduce such information without the prior approval of the owner of such information provided always that you may disclose such information if: (a) such information is already in the public domain otherwise than by disclosure by you; (b) the information was lawfully obtained or available from a third party who is lawfully in possession of the same and is free to disclose it; or (c) the disclosure of such information is required by law.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">7.</span>
                                        <span class="tnc-lbl">Account</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">7.1</span>
                                                <span class="tnc-desc"><p class="modal-para">We may make available advertisements, advertising of Task and Job opportunities and other income generating content as posted by Posters or via links of other websites and social media platforms (“<b>Third-Party Websites and/or Social Media Platforms</b>”) (“<b>Listings</b>”) through the provision of the Services.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Although browsing or searching for Listings via the Services is free, you would be required to register and create an Account in order to access and use some aspects of the Services (e.g. submitting Applications and posting Listings) prior to using the Services, pursuant to which you may be required to provide certain information to us, including but not limited to your name, address, contact details, banking details and any other information as may be deemed necessary. You are responsible to update the said information from time to time to ensure that it is accurate, complete, up to date and truthful.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.3</span>
                                                <span class="tnc-desc"><p class="modal-para">By creating an Account, you represent and warrant that:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">If you are under eighteen (18) years of age, you may only use the Services with the consent of and under the supervision of your parent or legal guardian who shall be responsible and liable for all your activities in respect of the use of the Services;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">If you are a corporate User, the individual who creates and registers the Account on your behalf is the authorised representative of your corporation and that your corporate entity shall remain solely responsible for all activities of the Account;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">You have accepted our MTP’s Personal Data Protection Notice (which will form part of these Terms) [insert hyperlink]; and</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">You will comply with all applicable laws with respect to your activities in using the Services.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.4</span>
                                                <span class="tnc-desc"><p class="modal-para">When you register for an Account, you will be required to create a username and password. You are solely responsible for keeping your login details secure and preventing any unauthorised person from using your Account. If your Account has been compromised, you must inform us immediately. If we have reason to believe that there is likely to be a breach of security or misuse of the Account, we may require you to change your username or password, or we may suspend your Account without prior notice. We will not be liable for any misuse or unauthorised use of your Account due to your failure in keeping your login details secure and preventing unauthorised use of your Account.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.5</span>
                                                <span class="tnc-desc"><p class="modal-para">You recognise that any access to and/or use of your Account, and any information, data or communications referable or traceable to your username and password shall be deemed to be (a) access to and/or use of your Account by you; or (b) information, data or communications posted, transmitted and validly issued by you. You agree to be bound by such access to and/or use of your Account and you agree that we are entitled to act upon and hold you responsible and liable for such action, as if carried out or transmitted by you. You also agree to indemnify us against any and all losses, attributable to any access to and/or use of your Account referable or traceable to your username and password.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.6</span>
                                                <span class="tnc-desc"><p class="modal-para">You may be required to provide us with additional information or documentation from time to time for the purpose of confirming or verifying, including but not limited to, your identity, age, your banking details or for such other purpose as may be deemed necessary.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.7</span>
                                                <span class="tnc-desc"><p class="modal-para">Unless expressly permitted by MTP and subject to these Terms and any other terms as MTP determines, you shall not create multiple Accounts. You are prohibited from lending, transferring or selling your Account and must not use another person’s Account without their permission. You also agree and understand that we may, at our sole discretion, refuse to register any personal as a User.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.8</span>
                                                <span class="tnc-desc"><p class="modal-para">You are allowed to delete your Account at any time. Deletion of your Account shall be subject to:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">There not being any outstanding fees or amounts owing on the Account; and</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">There not being any outstanding resolutions to any disputes which may have arisen, or been lodged by any other party in the course of recent engagements related to a Job(s) and/or Task(s).</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.9</span>
                                                <span class="tnc-desc"><p class="modal-para">You agree and understand that you will lose all information related to your Account and use of the Services upon the suspension or deletion of your Account, however, deleting your Account does not guarantee the deletion or removal of all of the information that we possess in respect of your Account, as we may retain some of your information in accordance with our Personal Data Protection Notice [insert hyperlink].</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.10</span>
                                                <span class="tnc-desc"><p class="modal-para">For avoidance of doubt, we reserve the right to:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Suspend or delete your Account and/or your access to or use of the Services at any time, for any breach of the Terms;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Suspend or delete your Account and/or your access to and/or use of the Services at any time, for any reason, and without any prior notice; and</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Alter, limit, modify, suspend or discontinue your access to and/or use of the Services, or any part thereof, at any time for any reason and without any prior notice.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">8.</span>
                                        <span class="tnc-lbl">Terms and conditions applicable to Seekers</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">8.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Listings are created and provided by Posters and other third parties over whom we do not exercise control over. The Listings, the content therein and/or any conditions imposed therein that are created, provided and linked from other third parties are not within our control. If you leave the Services and choose to enter into Third-Party Websites and/or Social Media Platforms you shall be governed by any terms and conditions imposed by that third party.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.2</span>
                                                <span class="tnc-desc"><p class="modal-para">For the avoidance of doubt, you understand and acknowledge that we do not have control over and shall not be responsible or liable for, any of the following:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">The quality, safety, legality or morality of any aspect of the Listings listed or posted on the Services; or</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">(b)The truth, completeness, reliability, legality or accuracy of the Listings, the authority of Posters or any other third party to provide or create the Listings, the ability of the Posters and/or any other third party to pay for your services provided pursuant your acceptance of the Task and/or Job, and any other information submitted by the Poster and/or third party.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.3</span>
                                                <span class="tnc-desc"><p class="modal-para">Application for Listings</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">You may submit your resume and/or application information in applying for a Task and/or Job as posted by the Posters via Listings (“<b>Application</b>”) through the Services. If you wish to submit your Application for Listings as created, provided or linked from any other third party, you will be redirected to the Third-Party Websites and/or Social Media Platforms and shall be governed by any terms and conditions imposed by that third party.</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Once you have submitted your Application to a Poster and/or third party, we do not have control over the Poster’s and/or third party’s use or disclosure of that information.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.4</span>
                                                <span class="tnc-desc"><p class="modal-para">When you submit an Application via the Services, we will attempt to send your Application to the Poster. You understand and acknowledge that we cannot guarantee that the Poster will receive, be notified of, have access to, read, acknowledge or respond to your Application or that there will be no error in the transmission or storage of your Application and any information related therein. However, we will endeavour to notify you if any of the aforementioned events occur.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.5</span>
                                                <span class="tnc-desc"><p class="modal-para">When you submit an Application via the Services, you agree to us conducting an automated processing in relation to your Application and any correspondences between you and the Poster through the Services will be processed and analysed by us according to these Terms.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.6</span>
                                                <span class="tnc-desc"><p class="modal-para">You understand and acknowledge that we will not be responsible for any of the content of the Listings, assessment criteria, hiring and interviewing process and/or the determination in respect of your Application by the Posters and/or third parties. Additionally, we do not guarantee that the Poster and/or third party will consider your Application or make a particular determination in respect of your Application.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.7</span>
                                                <span class="tnc-desc"><p class="modal-para">We may use your information to calibrate your interest in a Listing and may communicate with you in respect of other prospective Listings.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.8</span>
                                                <span class="tnc-desc"><p class="modal-para">The availability of Listings may lapse from the time you submit your Application and when such Application is received, and you acknowledge and understand that we do not have control or responsibility over expired Listings or for delivering such information prior to the lapse of the Listings.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.9</span>
                                                <span class="tnc-desc"><p class="modal-para">We may notify you if a Poster takes any action with regards to your Application in relation to a Listing.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.10</span>
                                                <span class="tnc-desc"><p class="modal-para">Upon the Poster awarding a Task or Job to you and your acceptance of the Task and/or Job, your relationship with the Poster will be governed by these Terms, specifically Clause 10.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">9.</span>
                                        <span class="tnc-lbl">Terms and conditions applicable to Posters</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">9.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Posters may create, post and make available Listings via the Services through your Account. You shall be solely responsible for the Listings posted by you, the content therein and/or any conditions imposed therein.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">9.2</span>
                                                <span class="tnc-desc"><p class="modal-para">For avoidance of doubt, you understand and acknowledge that we shall not be liable nor responsible for the truth, completeness, reliability, legality of the Listings, your authority in providing or creating the Listings, your ability and/or any other third party to pay for the services provided by a Seeker upon their acceptance of the Task and/or Job and any other information submitted by you.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">9.3</span>
                                                <span class="tnc-desc"><p class="modal-para">Posting of Listings</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">In posting a Listing, you will be required to provide information relevant to the Task and/or Job including but not limited to its nature and scope, duration, expected date of completion.</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">The Listings must not contain:</p>
                                                                 <ul class="tnc-list forth-level">
                                                                    <li>
                                                                        <span class="tnc-no">(i)</span>
                                                                        <span class="tnc-desc"><p class="modal-para">Misleading, misrepresenting, irrelevant, false and/or inaccurate information;</p></span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="tnc-no">(ii)</span>
                                                                        <span class="tnc-desc"><p class="modal-para">Names, logos, or copyrighted material of persons unaffiliated with you;</p></span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="tnc-no">(iii)</span>
                                                                        <span class="tnc-desc"><p class="modal-para">Material that is discriminatory, explicit, obscene, abusive or hateful;</p></span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="tnc-no">(iv)</span>
                                                                        <span class="tnc-desc"><p class="modal-para">Any sale, promotion or advertisement of products and services;</p></span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="tnc-no">(v)</span>
                                                                        <span class="tnc-desc"><p class="modal-para">Any franchise, pyramid scheme or multi-level marketing opportunity; and</p></span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="tnc-no">(vi)</span>
                                                                        <span class="tnc-desc"><p class="modal-para">Any attempt to seek sexual services or seek any services that are immoral or illegal.</p></span>
                                                                    </li>
                                                                </ul>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">We will publish the Listing upon your submission of the same. However, you understand and acknowledge that we cannot guarantee that all Seekers will receive, be notified of or have access to the Listing or that there will be no error in the transmission or storage of your Listing and any information related therein. Nonetheless, we will endeavour to notify you if any of the aforementioned events occur.</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">When you post a Listing via the Services, you agree to us conducting an automated processing in relation to the Listing and any correspondences between you and the Seeker through the Services will be processed and analysed by us according to these Terms.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">9.4</span>
                                                <span class="tnc-desc"><p class="modal-para">Seekers may submit Applications in response to the Listings, however you understand and acknowledge that we cannot guarantee that you will receive, be notified of and/or have access to the Applications or that there will be no error in the transmission or storage of the Listings.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">9.5</span>
                                                <span class="tnc-desc"><p class="modal-para">We may make available to you screening tools for you to use in order to process and examine the Applications.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">9.6</span>
                                                <span class="tnc-desc"><p class="modal-para">Upon the award of a Listing to the selected Applicant by you, we will notify the Applicant of your acceptance. If the Listing is accepted and confirmed by the Applicant, you will be redirected to the payment overview page to make the necessary payments as set out under Clause 11 and Clause 12. Failure to make the necessary payments in accordance with Clause 11 and Clause 12 will result in the cancellation and/or suspension of the Listing.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">9.7</span>
                                                <span class="tnc-desc"><p class="modal-para">Upon the Applicant’s acceptance of the Listing, your relationship with the Applicant will be governed by these Terms, specifically Clause 10.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">10.</span>
                                        <span class="tnc-lbl">Terms governing engagement of Applicants by Posters</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">10.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Terms governing engagement of Task Applicants by Task Posters</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">The provisions under this Clause 10.1 shall constitute a legal agreement between a Task Applicant and a Task Poster (individually referred to as a “<b>Party</b>” and collectively referred to as the “<b>Parties</b>”) upon the successful confirmation of the engagement of the Task Applicant by the Task Poster, and the acceptance of the engagement by the Task Applicant in relation to a Task Listing (“<b>Task Engagement</b>”), following which, the Task Applicant shall be referred to as a Task Holder.</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Upon the Task Engagement, the Task Poster hereby agrees to engage the Task Holder to complete the Task as set out in the Task Listing and undertakes to pay the Task Fees to the Task Holder upon completion, and the Task Holder hereby agrees to deliver such task as set out in the Task Listing in which he/she were engaged to complete, in consideration of the Task Fees.</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Each Party shall be solely responsible in ensuring that they comply with their obligations pursuant to the Task Engagement. Each Party shall be responsible in enforcing any rights that they may have. For the avoidance of doubt, MTP shall not be liable nor will MTP have the responsibility to enforce any rights that either Party may have under this Clause 10.1 or any applicable law for any damages to, or loss suffered by either Party in relation to the Task Engagement.</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Each Party acknowledges and agrees that the relationship with the other Party is that of an independent contractor and nothing in these Terms shall create any form of partnership, joint-venture, agency or employment relationship between the Parties and/or with MTP.</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(e)</span>
                                                            <span class="tnc-desc"><p class="modal-para">The Parties acknowledge and agree that they will at all times comply with these Terms, specifically Clause 11 and Clause 12.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">10.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Terms governing engagement of Job Applicants by Job Posters</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">The provisions under this Clause 10.2 shall constitute a legal agreement between a Job Applicant and a Job Poster (individually referred to as a “<b>Party</b>” and collectively referred to as the “<b>Parties</b>”) upon the successful confirmation of the engagement of the Job Applicant by the Job Poster, and the acceptance of the engagement by the Job Applicant in relation to a Job Listing (“<b>Job Engagement</b>”), following which, the Job Applicant shall be referred to as a Job Holder.</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Upon the Job Engagement, the Job Poster hereby agrees to engage the Job Applicant for the Job as set out in the Job Listing in consideration of the Job Fees in accordance to terms to be mutually agreed upon by the Parties.</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Each Party shall be solely responsible in ensuring that they comply with their obligations pursuant to the Job Engagement. Each Party shall be responsible in enforcing any rights that they may have. For the avoidance of doubt, MTP shall not be liable nor will MTP have the responsibility to enforce any rights that either Party may have under this Clause 10.2 or any applicable law for any damages to, or loss suffered by either Party in relation to the Job Engagement.</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Each Party acknowledges and agrees that nothing in these Terms shall create any form of partnership, joint-venture, agency or employment relationship with MTP.</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(e)</span>
                                                            <span class="tnc-desc"><p class="modal-para">The Parties acknowledge and agree that they will at all times comply with these Terms, specifically Clause 11 and Clause 12.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">11.</span>
                                        <span class="tnc-lbl">Fees and charges</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">11.1</span>
                                                <span class="tnc-desc"><p class="modal-para">You may be subject to a charge or a fee for the use of certain aspects of the Services, including but not limited to Task Spotlights, Job Spotlights, Candidate Spotlights, Premium Membership. You may also be subject to a fee for the processing of Applications in respect of an Engagement. When you are using aspects of the Services that are subject to a charge, you will be provided the opportunity to review and accept the terms of the fees that you will be charged based on this Clause 11.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">11.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Before you proceed to make payment via the Services, you will be presented with a webpage pursuant to which you will be informed of the exact amount due and payable to us.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">11.3</span>
                                                <span class="tnc-desc"><p class="modal-para">Payment via the Services may be made using only <i>[FPX, credit card, debit card or PayPal]</i>.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">11.4</span>
                                                <span class="tnc-desc"><p class="modal-para">Before completing a transaction, you will be presented with a confirmation screen pursuant to which you are required to verify the transaction details. It is your responsibility to confirm and verify that the details stated therein are accurate.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">11.5</span>
                                                <span class="tnc-desc"><p class="modal-para">If your transaction is successful, you will receive the receipt of your payment via email. You may also download the receipt under your Account dashboard on the website and/or mobile application.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">11.6</span>
                                                <span class="tnc-desc"><p class="modal-para">The information supplied by you in connection with your use of the Services is processed through a secure website and/or mobile application. However, you acknowledge and agree that internet transmissions cannot be guaranteed to be entirely secure or private and any information provided by you may be able to be read and/or intercepted by a third party. We shall not be liable for any interception and/or other unauthorised access to the information supplied by you in connection with your use of the Services.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">11.7</span>
                                                <span class="tnc-desc"><p class="modal-para">You shall be responsible for paying any taxes, including sales and services tax, which may be applicable in your use of the Services. These taxes will be added to the fees charged to you, if applicable.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">11.8</span>
                                                <span class="tnc-desc"><p class="modal-para">Unless otherwise stated, all fees and charges are quoted in Ringgit Malaysia (MYR).</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">12.</span>
                                        <span class="tnc-lbl">Escrow services</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">12.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Upon a Task Engagement, the Task Poster shall make an advance payment of the Task Fees in full and shall authorise us to collect and hold such funds on its behalf until the completion of the services set out in the Task Listing by the Task Holder.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">12.2</span>
                                                <span class="tnc-desc"><p class="modal-para">The advance payment of the Task Fees shall be held by us and will only be released upon the following events:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Where we have received instructions from the Task Poster to release the funds; and</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Where there is a pending or ongoing dispute, when the dispute has been resolved in the Task Holder’s favour.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">12.3</span>
                                                <span class="tnc-desc"><p class="modal-para">We reserve the right to hold, set off and/or deduct against the Task Fees any amounts and/or fees or charges payable and/or owing by the Task Poster and/or Task Holder, including but not limited to any fees or charges pursuant to Clause 11.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">12.4</span>
                                                <span class="tnc-desc"><p class="modal-para">Where we have not received any instructions or dispute from the Task Poster or Task Holder in respect of the Task Fees within one (1) month from the date of the advance payment of the Task Fees was made, the funds will be returned to the Task Poster, subject to applicable deductions for administrative fees incurred in the amount not exceeding 5% of the Task Fees.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">13.</span>
                                        <span class="tnc-lbl">Intellectual Property Rights</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">13.1</span>
                                                <span class="tnc-desc"><p class="modal-para">The Services contains copyright material, trade names and marks and other proprietary information, including, but not limited to, text, software, photos and graphics, and may in future include video, graphics, music and sound, which may be protected by Intellectual Property Rights.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">13.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Except as expressly provided in these Terms, nothing contained herein shall be construed as conferring you or any third party any license or right, by implication, estoppel or otherwise, under any law, rule or regulation, including Intellectual Property Rights. You agree that all intellectual property appearing on the Services are the property of their respective owners. You shall not use, copy, transmit, publish, distribute or broadcast any works including but not limited to text, pictures, video and other content found on the Services without prior permission of the respective owners.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">13.3</span>
                                                <span class="tnc-desc"><p class="modal-para">Notwithstanding and in addition to any applicable written law dealing with Intellectual Property Rights and infringements of such rights, any of the following shall be deemed unauthorised acts, if done, without the written consent and/or license of MTP or the respective owners:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Copying, republishing, distributing or transmitting any portion of the Services or its content;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Using the Services or any of its content as part of any information storage retrieval system or database that is offered for commercial distribution;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Creating compilations or derivative works of the Services or its content;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Using the Services or its content in any manner that infringes copyright, trade mark or other intellectual property or proprietary rights;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(e)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Removing or obscuring any copyright or other intellectual property or proprietary notice;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(f)</span>
                                                            <span class="tnc-desc"><p class="modal-para">(f)Making any portion of the Services or its content available in any other medium;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(g)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Removing or reverse-engineering any software in respect of the Services; or</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(h)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Using any automatic or other process to harvest data and information from the Services.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">14.</span>
                                        <span class="tnc-lbl">Links</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">14.1</span>
                                                <span class="tnc-desc"><p class="modal-para">The Services may contain links and/or references to Third-Party Websites and/or Social Media Platforms. This may include advertising or any other form of communications originating from Third-Party Websites and/or Social Media Platforms. In the event that the Services include any such advertising, we are not responsible for the legality of or any error or inaccuracy in advertisers’ materials or for the acts or omissions of the advertisers.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">14.2</span>
                                                <span class="tnc-desc"><p class="modal-para">While we have monitoring measures in place, we shall not be responsible for the accuracy or completeness of the content published, opinions expressed or information provided by Third-Party Websites and/or Social Media Platforms, and the inclusion of or reference to the Third-Party Websites and/or Social Media Platforms by our Services does not imply approval or endorsement of those sites and platforms by us. We will not be involved in, and shall not be responsible for any transactions between you and a company connected through such Third-Party Websites and/or Social Media Platforms.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">14.3</span>
                                                <span class="tnc-desc"><p class="modal-para">In the event you decide to leave the Services and access Third-Party Websites and/or Social Media Platforms, you shall do so at your own risk.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">15.</span>
                                        <span class="tnc-lbl">Personal data</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">Our Personal Data Protection Notice governs the use of any personal information that you provide to us via the Services. A copy of the Personal Data Protection Notice can be accessed here [insert hyperlink].</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">16.</span>
                                        <span class="tnc-lbl">Disclaimer</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">We shall not be liable for any damages whatsoever, including but without limitation to any direct, indirect, special, consequential, punitive or incidental damages, or damages for loss of use, profits, data or other intangibles, damage to goodwill or reputation, arising out of or related to the use, inability to use or failures of the Services. We expressly disclaim any express and implied warranties, including, without limitation, completeness or accuracy of any information provided or made available to you. We do not warrant that the Services and any and all of its related facilities or capabilities will be free of interruption or error, that defects will be corrected, or that the equipment and software that makes the Services available is free of viruses or other harmful components.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">17.</span>
                                        <span class="tnc-lbl">Indemnity</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">You agree to indemnify, defend and hold harmless MTP, our employees, directors, agents and/or vendors harmless against all claims, damages, costs and expenses of whatever nature (including costs on all indemnity basis) which we may incur or which may be awarded against us and which arise out of (a) the access to or use of the Services by you or any persons authorised by you; (b) any breach of the undertakings, warranties or representations provided by you in these Terms; and (c) your infringement of any third party right (including any intellectual property, property or privacy right). This indemnity shall not be subject to any limitation of liability and includes without limitation costs and expenses including professional fees incurred in responding to the dealing with claims made irrespective of whether proceedings have been commenced.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">18.</span>
                                        <span class="tnc-lbl">Miscellaneous</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">18.1</span>
                                                <span class="tnc-desc"><p class="modal-para">These Terms shall be governed by the laws of Malaysia and the parties submit to the exclusive jurisdiction of the courts of Malaysia.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">18.2</span>
                                                <span class="tnc-desc"><p class="modal-para">No failure or delay by us or you in exercising any right under these Terms shall operate as a waiver of such right or affect any other or subsequent event or impair any rights or remedies in respect of it or in any way modify or diminish our or your rights under these Terms.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">18.3</span>
                                                <span class="tnc-desc"><p class="modal-para">If any clause in these Terms shall become or shall be declared by any court of competent jurisdiction to be invalid or unenforceable, such invalidity or unenforceability shall in no way affect any other clause or part of any clause, all of which shall remain in full force and effect, so long as these Terms shall be capable of continuing in effect without the unenforceable term.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">18.4</span>
                                                <span class="tnc-desc"><p class="modal-para">These Terms shall bind and inure to benefit of the parties and their respective permitted assigns, representatives and successors in title.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">18.5</span>
                                                <span class="tnc-desc"><p class="modal-para">We may assign, delegate or transfer any rights or obligations under these Terms, in our sole discretion, to a third party. You shall not assign, delegate or transfer any rights or obligations under these Terms to a third party without our written approval.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">18.6</span>
                                                <span class="tnc-desc"><p class="modal-para">These Terms contains the entire understanding between the parties to these Terms with respect only to the subject matter thereof and supersedes all prior agreements or understandings, inducements or conditions, express or implied, oral or written.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">18.7</span>
                                                <span class="tnc-desc"><p class="modal-para">For any questions with regard to these Terms, please contact MTP at <a href="mailto:legal@maketimepay.com">legal@maketimepay.com</a>.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade show" id="tncBmTab" role="tabpanel">
                            <div class="tnc-rows-container">
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">1.</span>
                                        <span class="tnc-lbl">Terma am</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">ni adalah terma penggunaan and perkhidmatan (“<b>Terma</b>”) MakeTimePay Sdn Bhd (No. Syarikat: 1373558-V). Akses dan penggunaan laman web ini dan/atau aplikasi mudah alih dan/atau perkhidmatan di laman web ini dan/atau di aplikasi mudah alih (secara kolektif, “<b>Perkhidmatan</b>”) adalah tertakluk kepada Terma ini.</p>
                                        <p class="modal-para">Perkhidmatan ini merupakan ekosistem atas talian yang memudahkan percambahan peluang terutamanya untuk pekerjaan berdasarkan permintaan (<i>on-demand work</i>) untuk Pengguna.</p>
                                        <p class="modal-para">Dengan mengakses dan menggunakan Perkhidmatan ini, anda bersetuju untuk mematuhi dan terikat kepada Terma ini yang akan terpakai kepada semua Pengguna Perkhidmatan ini. Jika anda menggunakan dan mengakses Perkhidmatan bagi pihak dan atas nama entiti perniagaan, istilah “anda” dalam Terma ini akan bermaksud entiti perniagaan yang anda wakili dalam menggunakan Perkhidmatan ini, kecuali jika konteksnya tidak mengizinkan.</p>
                                        <p class="modal-para">Anda dinasihatkan untuk membaca Terma ini dengan teliti oleh kerana ia akan menjejaskan hak dan liabiliti anda dibawah undang-undang dan peraturan yang berkenaan di Malaysia. Sekiranya anda tidak bersetuju dengan Terma ini, anda tidak sepatutnya menggunakan Perkhidmatan ini.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">2.</span>
                                        <span class="tnc-lbl">Istilah-istilah utama</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">Istilah-istilah berikut mempunyai maksud seperti yang berikut:</p>
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">2.1</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Akaun</b>” bermaksud akaun yang dicipta dengan MTP melalui laman web dan/atau aplikasi mudah alih untuk penggunaan penuh dan akses ke Perkhidmatan ini;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.2</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Fi Kerja</b>” bermaksud gaji yang harus dibayar oleh Poster Kerja kepada Pemegang Kerja setelah penyampaian perkhidmatan atas terma yang akan dipersetujui secara bersama oleh Pemegang Kerja dan Poster Kerja;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.3</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Fi Tugas</b>” bermaksud fi yang harus dibayar oleh Poster Tugas kepada Pemegang Tugas setelah penyelesaian penyampaian Tugas oleh Pemegang Tugas mengikut Senarai Tugas;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.4</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Hak Harta Intelektual</b>” bermaksud sebarang hak paten, hak cipta, reka bentuk berdaftar, tanda niaga, hak dalam reka bentuk, tanda perkhidmatan, hak di bawah lesen atau hak harta industri atau intelektual lain, sama ada didaftarkan atau tidak dan termasuk permohonan pendaftaran mana-mana perkara di atas dan kesemua bentuk perlindungan yang serupa atau mempunyai kesan yang serupa yang boleh wujud di Malaysia dan/atau di mana sahaja di dunia;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.5</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Kerja</b>” bermaksud pekerjaan sepenuh masa yang dibayar dengan Fi Kerja;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.6</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Laman Web Pihak Ketiga dan/atau Platform Media Sosial</b>” mempunyai maksud seperti yang dinyatakan di bawah Klausa 7.1;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.7</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Lantikan</b>” bermaksud Lantikan Tugas dan/atau Lantikan Kerja;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.8</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Lantikan Kerja</b>” mempunyai maksud seperti yang dinyatakan di bawah Klausa 10.2;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.9</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Lantikan Tugas</b>” mempunyai maksud seperti yang dinyatakan di bawah Klausa 10.1;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.10</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>MTP</b>” bermaksud MakeTimePay Sdn Bhd [No. Syarikat: 1373558-V], yang mempunyai pejabat berdaftar di no.16-2, 2nd floor, Jalan 1/76C, Desa Pandan, 55100 Kuala Lumpur, Wilayah Persekutuan, Malaysia;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.11</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Pemohon</b>” bermaksud Pemohon Kerja atau Pemohon Tugas;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.12</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Pemohon Kerja</b>” bermaksud Pencari Kerja yang telah berjaya menghantar Permohonan bagi Senarai Kerja mengikut Klausa 8.3;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.13</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Pemohon Tugas</b>” bermaksud Pencari Tugas yang telah berjaya menghantar Permohonan bagi Senarai Tugas mengikut Klausa 8.3;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.14</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Pemegang Kerja</b>” bermaksud Pemohon Kerja yang telah berjaya dilantik oleh Poster Kerja mengikut Klausa 10.2;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.15</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Pemegang Tugas</b>” bbermaksud Pemohon Tugas yang telah berjaya dilantik oleh Poster Tugas mengikut Klausa 10.1;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.16</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Pencari</b>” bermaksud Pencari Kerja dan Pencari Tugas;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.17</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Pencari Kerja</b>” bermaksud Pengguna yang mengakses dan/atau menggunakan Perkhidmatan ini untuk mencari Kerja dan/atau peluang pendapatan yang serupa dengannya;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.18</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Pencari Tugas</b>” bermaksud Pengguna yang mengakses dan/atau menggunakan Perkhidmatan ini untuk mencari Tugas dan/atau peluang pendapatan yang serupa dengannya;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.19</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Pengguna</b>” bermaksud mana-mana orang, individu atau entiti korporat yang menggunakan Perkhidmatan seperti yang disediakan oleh MTP, termasuk tetapi tidak terhad kepada Pencari dan Poster.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.20</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Perkhidmatan</b>” bermaksud pelbagai perkhidmatan dan fungsi yang disediakan melalui laman web ini dan/atau aplikasi mudah alih yang disediakan oleh MTP;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.21</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Permohonan</b>” mempunyai maksud seperti yang dinyatakan di bawah Klausa 8.3(a);</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.22</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Poster</b>” bermaksud Poster Kerja dan Poster Tugas;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.23</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Poster Kerja</b>” bermaksud Pengguna yang mengakses dan/atau menggunakan Perkhidmatan ini bagi tujuan untuk memaparkan Senarai Kerja dan mencari calon untuk menjalankan Kerja;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.24</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Senarai</b>” mempunyai maksud seperti yang dinyatakan dibawah Klausa 7.1;</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">2.25</span>
                                                <span class="tnc-desc"><p class="modal-para">“<b>Tugas</b>” bermaksud sesuatu projek atau tugasan jangka pendek bagi apa pun tempoh yang diperlukan yang dibayar dengan Fi Tugas setelah penyelesaian;</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">3.</span>
                                        <span class="tnc-lbl">Kesan</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">3.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Kami berhak untuk menukar, mengubahsuai atau menghapuskan Terma ini atau mana-mana bahagian daripadanya pada bila-bila masa. Perubahan akan berkesan setelah perubahan dipaparkan melalui Perkhidmatan ini. Anda boleh menentukan pindaan terakhir kami kepada Terma ini dengan merujuk pada pernyataan ‘Kemaskini Terakhir’ di atas. Kami tidak akan bertanggung jawab ke atas apa-apa kerosakan/ganti rugi yang dialami atau ditanggung oleh anda berkenaan dengan kegagalan anda untuk memahami Terma yang dikemaskini. Akses dan penggunaan Perkhidmatan anda yang berterusan setelah pemaparan Terma yang dikemaskini merupakan persetujuan anda untuk mematuhi dan terikat dengan Terma yang dikemaskini.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">3.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda harus mematuhi Terma ini dan akan bertanggungjawab sepenuhnya ke atas segala kehilangan dan sebarang kerosakan/ganti rugi yang timbul daripada atau berkaitan dengan pelanggaran atau kegagalan anda untuk mematuhi Terma ini.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">4.</span>
                                        <span class="tnc-lbl">Hak untuk menggunakan dan sekatan</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">4.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda bertanggungjawab untuk mengakses dan menggunakan Perkhidmatan ini, termasuk mendapatkan akses kepada rangkaian data yang diperlukan, walaupun akses tersebut dilakukan oleh orang lain.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda mengakses dan menggunakan Perkhidmatan ini pada risiko anda sendiri. Kami tidak akan bertanggungjawab ke atas apa-apa kerosakan/ganti rugi, atau virus atau kod lain yang mungkin boleh mempengaruh perkakasan atau peranti, perisian, data atau harta benda lain akibat daripada akses dan penggunaan Perkhidmatan ini oleh anda.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.3</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda hanya boleh mengakses Perkhidmatan ini menggunakan cara yang dibenarkan. Ia adalah tanggungjawab anda untuk memeriksa dan memastikan bahawa anda telah memuat turun dan/atau mengemaskini perisian yang betul untuk perkakasan dan peranti bagi akses dan penggunaan Perkhidmatan oleh anda. Kami tidak menjamin bahawa Perkhidmatan ini, atau bahagiannya, akan berfungsi sebagaimana yang dimaksud pada kesemua perkakasan atau peranti. Kami tidak bertanggungjawab sekiranya anda tidak mempunyai perkakasan atau peranti yang serasi atau jika anda memuat turun versi perisian yang salah pada perkakasan atau peranti anda.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.4</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda akan mengakses dan menggunakan Perkhidmatan ini dengan menurut kepada arahan untuk mengakses dan menggunakan Perkhidmatan ini yang mungkin kami keluarkan dari semasa ke semasa, dimana Terma dan peraturan ini adalah berkuatkuasa di Malaysia pada masa ini.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.5</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda tidak boleh:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">menyamar sebagai mana-mana orang atau pihak atau aku palsu, memutarbelitkan atau salah nyata hubungan anda dengan mana-mana orang atau pihak yang berkaitan dengan Perkhidmatan ini, atau menyatakan atau menyiratkan bahawa kami menyokong pernyataan yang dibuat oleh anda;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">merosakkan atau menggangu Perkhidmatan ini dan/atau operasi Perkhidmatan ini dengan cara apa pun;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">menggunakan Perkhidmatan ini begi tujuan penipuan, tidak sah atau menyalahi undang-undang;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">memberikan maklumat atau kandungan yang salah, menyesatkan, tidak tepat, menyalahi undang-undang, mengancam, lucah, benci, libel atau memfitnah dengan sengaja;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(e)</span>
                                                            <span class="tnc-desc"><p class="modal-para">menggunakan Perkhidmatan ini untuk memfitnah, menyalahgunakan, mengganggu, mengintai, mengancam atau melanggar hak orang lain, termasuk tetapi tidak terhad kepada hak privasi pihak lain;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(f)</span>
                                                            <span class="tnc-desc"><p class="modal-para">menggunakan Perkhidmatan ini dengan cara yang mungkin bertentangan dengan kepentingan kami dan/atau kepentingan Pengguna lain;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(g)</span>
                                                            <span class="tnc-desc"><p class="modal-para">memperkenalkan atau membenarkan pengenalan virus komputer dan/atau rutin pengaturcaraan komputer lain (<i>other computer programming routines</i>) seperti virus, perisian hasad (<i>malware</i>), e-mel yang tidak diminta, <i>Trojan horses, trap doors, back doors, worms, time bombs</i> atau <i>cancelbots</i> yang boleh merosakkan, mengganggu sehingga merugikan, memintas secara diam-diam atau menggunakan mana-mana sistem, data atau maklumat peribadi, yang berkaitan dengan atau berkenaan dengan Perkhidmatan ini;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(h)</span>
                                                            <span class="tnc-desc"><p class="modal-para">menghasilkan semula, menggandakan, menyalin, menjual, menjual semula atau mengeksploitasi dengan cara lain untuk tujuan komersial, mana-mana bahagian, penggunaan, atau akses ke Perkhidmatan ini;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(i)</span>
                                                            <span class="tnc-desc"><p class="modal-para">mengubah suai, menyesuaikan, menterjemahkan, <i>reverse engineer</i>, menguraikan atau membongkar mana-mana bahagian Perkhidmatan ini;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(j)</span>
                                                            <span class="tnc-desc"><p class="modal-para">melanggar hak milik pihak lain, atau menghapuskan hak cipta, tanda niaga atau notis hak milik lain dari Perkhidmatan ini atau bahan-bahan yang berasal dari Perkhidmatan ini; dan/atau;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(k)</span>
                                                            <span class="tnc-desc"><p class="modal-para">menggunakan Perkhidmatan ini dengan cara yang menyalahi sebarang undang-undang dan peraturan di Malaysia.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.6</span>
                                                <span class="tnc-desc"><p class="modal-para">Kami merizabkan hak kami untuk:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">memeriksa dan/atau mengawal sebarang aktiviti, kandungan atau maklumat yang berlaku di atau melalui Perkhidmatan ini;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">menyiasat sebarang pelanggaran Terma ini dan mengambil Tindakan yang sewajarnya;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">mengehadkan akses dan penggunaan Perkhidmatan ini oleh anda sekiranya anda melanggar mana-mana perkara yang dinyatakan dalam Klausa 4.5 di atas; dan/atau</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">melaporkan sebarang kegiatan yang mencurigakan mengenai kemungkinan pelanggaran undang-undang atau peraturan yang berlaku kepada pihak berkuasa yang berkenaan dan untuk bekerjasama dengan pihak berkuasa tersebut.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.7</span>
                                                <span class="tnc-desc"><p class="modal-para">Tertakluk pada pematuhan anda terhadap Klausa 4.5, anda dibenarkan untuk memuat naik dan berkongsi bahan, maklumat dan kandungan berkenaan dengan penggunaan Perkhidmatan ini.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.8</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda bersetuju dan mengakui bahawa anda bertanggungjawab sepenuhnya dan bertanggungjawab terhadap sebarang bahan, maklumat dan kandungan yang anda muat naik dan kongsi berkenaan dengan penggunaan Perkhidmatan ini oleh anda dan sebarang kehilangan atau kerosakan/ganti rugi yang anda atau pihak ketiga menderita akibat dari bahan, maklumat dan kandungan tersebut yang di bawah tanggungjawab anda sendiri.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.9</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda bersetuju dan memahami bahawa kami mempunyai budi bicara sepenuhnya untuk menolak, menghapuskan atau memindahkan sebarang bahan, maklumat dan kandungan yang telah anda muat naik dan kongsi melalui Perkhidmatan ini, termasuk tetapi tidak terhad kepada yang berikut:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Jika bahan, informasi dan/atau kandungan tersebut melanggar Terma ini atau menyalahi sebarang undang-undang atau peraturan;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Sekiranya bahan, maklumat dan/atau kandungan tersebut mengandungi maklumat yang tidak lengkap, palsu atau tidak tepat mengenai Pengguna atau pihak lain;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Jika kami telah menerima aduan dari Pengguna Perkhidmatan yang lain yang didapati sah, berasas dan/atau tulen berikutan siasatan oleh kami; dan/atau</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Jika kami telah menerima notis atau aduan pelanggaran yang didapati sah, berasas dan/atau tulen berikutan siasatan oleh kami.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">5.</span>
                                        <span class="tnc-lbl">Ketersediaan</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">5.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Kami berusaha untuk menjadikan Perkhidmatan ini tersedia dua puluh empat (24) jam sehari, walaubagaimanapun kami tidak akan bertanggungjawab ke atas apa-apa sebab Perkhidmatan ini tidak tersedia pada bila-bila masa atau untuk jangka masa apa sekali pun. Kami tidak memberikan waranti atau jaminan bahawa akses dan penggunaan Perkhidmatan ini oleh anda akan berterusan tanpa gangguan atau bebas dari kesilapan.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">5.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Akses dan penggunaan Perkhidmatan ini boleh ditangguhkan atau ditarik balik buat sementara atau secara kekal pada bila-bila masa dan tanpa pemberitahuan sekiranya tindakan tersebut dianggap perlu oleh kami. Kami mungkin akan menggantung penyediaan Perkhidmatan ini buat sementara atas sebab pembaikan, penyelenggaraan, pemeriksaan, penggantian, kerosakan kemudahan komunikasi atau pengenalan kemudahan dan fungsi baru. Kami tidak akan bertanggungjawab atas kerosakan/ganti rugi atau kerugian yang mungkin anda alami akibat daripada penggantungan Perkhidmatan secara sementara atau kekal.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">6.</span>
                                        <span class="tnc-lbl">Kesulitan</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">Melalui akses dan penggunaan Perkhidmatan ini oleh anda, anda mungkin dibenarkan mengakses atau melihat maklumat yang bersifat sulit, sama ada maklumat tersebut dinyatakan sebagai sulit atau tidak. Maklumat tersebut mungkin berkaitan dengan kami atau pihak lain. Sekiranya anda dapat mengakses atau melihat maklumat tersebut, anda menjamin dan menyatakan bahawa melainkan jika pendedahan tersebut dinyatakan dengan jelas di bawah Terma ini, anda tidak akan dan tidak akan membenarkan pihak lain untuk mendedahkan atau menghasilkan semula maklumat tersebut tanpa persetujuan terlebih dahulu dari pemilik maklumat yang diberikan tertakluk pada syarat selalu bahawa anda boleh mendedahkan maklumat tersebut sekiranya:<br>(a) maklumat tersebut sudah terdapat di domain awam selain daripada pendedahan oleh anda; (b) maklumat itu diperoleh secara sah atau tersedia dari pihak ketiga yang secara sah memiliki maklumat yang sama dan bebas untuk mengungkapkannya; atau (c) pendedahan maklumat tersebut diperlukan dibawah undang-undang.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">7.</span>
                                        <span class="tnc-lbl">Akaun</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">7.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Kami boleh menyediakan iklan-iklan, pengiklanan peluang Tugas dan Kerja dan lain-lain yang berkaitan kandungan menjana pendapatan seperti yang dipaparkan oleh Poster atau melalui pautan laman web lain dan platform media sosial (“<b>Laman Web Pihak Ketiga dan / atau Platform Media Sosial</b>”) (“<b>Senarai</b>”) melalui penyediaan Perkhidmatan ini.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Walaupun melayari atau mencari Senarai  melalui Perkhidmatan ini adalah percuma, anda perlu mendaftar dan membuat Akaun untuk mengakses dan menggunakan beberapa aspek Perkhidmatan ini (contohnya, untuk mengemukakan Permohonan dan memaparkan Senarai) sebelum menggunakan Perkhidmatan ini, dimana anda mungkin diminta untuk memberikan maklumat tertentu kepada kami, termasuk tetapi tidak terhad kepada nama, alamat, maklumat hubungan, maklumat bank dan maklumat lain yang diperlukan. Anda bertanggungjawab untuk mengemaskini maklumat tersebut dari semasa ke semasa untuk memastikan maklumat tersebut tepat, lengkap, terkini dan benar.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.3</span>
                                                <span class="tnc-desc"><p class="modal-para">Dengan mencipta Akaun, anda menyatakan dan menjamin bahawa:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Sekiranya anda berumur di bawah lapan belas (18) tahun, anda hanya boleh menggunakan Perkhidmatan ini dengan persetujuan dan di bawah pengawasan ibu bapa atau penjaga sah anda yang akan bertanggungjawab ke atas semua aktiviti anda berkenaan dengan penggunaan Perkhidmatan ini;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Sekiranya anda adalah Pengguna korporat, individu yang membuat dan mendaftarkan Akaun bagi pihak anda adalah wakil sah syarikat anda dan entiti korporat anda akan tetap bertanggungjawab sepenuhnya bagi semua aktiviti Akaun tersebut;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Anda telah menerima Notis Perlindungan Data Peribadi MTP kami (yang akan menjadi sebahagian daripada Terma ini) [insert hyperlink]; dan</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Anda akan mematuhi semua undang-undang yang berkenaan dengan aktiviti anda dalam menggunakan Perkhidmatan ini.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.4</span>
                                                <span class="tnc-desc"><p class="modal-para">Semasa mendaftar untuk Akaun, anda akan diminta untuk mencipta nama pengguna dan kata laluan. Anda bertanggungjawab sepenuhnya untuk memastikan keselamatan maklumat log masuk anda dan mengelak penggunaan Akaun anda oleh orang yang tidak dibenarkan. Sekiranya Akaun anda telah dikompromi, anda mesti memberitahu kami dengan segera. Sekiranya kami mempunyai sebab untuk mempercayai bahawa terdapat kemungkinan pelanggaran keselamatan atau penyalahgunaan Akaun, kami mungkin meminta anda menukar nama pengguna atau kata laluan anda, atau kami mungkin akan menangguhkan Akaun anda tanpa pemberitahuan terlebih dahulu. Kami tidak akan bertanggungjawab atas penyalahgunaan atau penggunaan Akaun anda secara tidak sah kerana kegagalan anda menjaga keselamatan maklumat log masuk anda dan mengelak penggunaan Akaun anda secara tidak sah.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.5</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda memahami bahawa sebarang akses ke dan/atau penggunaan Akaun anda, dan sebarang maklumat, data atau komunikasi yang dapat dirujuk atau dikesan ke nama pengguna dan kata laluan anda akan dianggap sebagai <br>(a) akses dan/atau penggunaan Akaun anda oleh anda; atau (b) maklumat, data atau komunikasi yang dipaparkan, dihantar dan dikeluarkan secara sah oleh anda. Anda bersetuju untuk terikat dengan akses dan/atau penggunaan Akaun anda dan anda bersetuju bahawa kami berhak bertindak dan menanggung anda sebagai bertanggungjawab ke atas tindakan tersebut, seolah-olah dijalankan atau dihantar oleh anda. Anda juga bersetuju untuk menanggung ganti rugi kepada kami atas segala kerugian yang disebabkan oleh sebarang akses dan/atau penggunaan Akaun anda yang dapat dirujuk atau dapat dikesan ke nama pengguna dan kata laluan anda.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.6</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda mungkin diminta untuk memberikan kepada kami maklumat atau dokumentasi tambahan dari semasa ke semasa untuk tujuan mengesahkan, termasuk tetapi tidak terhad kepada, identiti, usia, maklumat perbankan anda atau untuk tujuan lain yang difikirkan perlu.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.7</span>
                                                <span class="tnc-desc"><p class="modal-para">Kecuali jika dibenarkan oleh MTP dan tertakluk kepada Terma ini dan terma-terma lain seperti yang ditentukan oleh MTP, anda tidak boleh mendaftarkan berbilang Akaun. Anda dilarang meminjamkan, memindahkan atau menjual Akaun anda dan tidak boleh menggunakan Akaun pihak lain tanpa kebenaran mereka. Anda juga bersetuju dan memahami bahawa kami mungkin, mengikut budi bicara kami sendiri, enggan untuk mendaftarkan mana-mana pihak sebagai Pengguna.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.8</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda dibenarkan untuk memadam Akaun anda pada bila-bila masa. Pemadaman Akaun anda tertakluk kepada:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Tiada sebarang bayaran atau jumlah tertunggak yang terhutang ke atas Akaun; dan</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Tiada ketetapan yang tertunggak bagi apa-apa pertikaian yang mungkin telah timbul, atau yang telah diajukan oleh pihak lain sewaktu atau selama tempoh pengunaan yang berkenaan dengan Kerja dan/atau Tugas.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.9</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda bersetuju dan memahami bahawa anda akan hilang segala maklumat yang berkaitan dengan Akaun anda dan penggunaan Perkhidmatan ini setelah penggantungan atau pemadaman Akaun anda, bagaimanapun, pemadaman Akaun anda tidak menjamin penghapusan atau penyingkiran semua maklumat yang kami miliki berkenaan dengan Akaun anda, oleh kerana kami mungkin menyimpan sebahagian maklumat anda menurut kepada dengan Notis Perlindungan Data Peribadi kami [insert hyperlink].</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">7.10</span>
                                                <span class="tnc-desc"><p class="modal-para">Bagi mengelak keraguan, kami berhak untuk:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Menggantung atau memadam Akaun anda dan/atau akses anda atau penggunaan Perkhidmatan ini pada bila-bila masa, bagi apa-apa pelanggaran Terma ini;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Menggantung atau memadam Akaun dan/atau akses anda dan/atau penggunaan Perkhidmatan ini pada bila-bila masa, atas sebab apa pun, dan tanpa pemberitahuan terlebih dahulu; dan</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Mengubah, mengehadkan, menukarkan, menggantung atau menghentikan akses anda dan/atau penggunaan Perkhidmatan ini, atau mana-mana bahagiannya, pada bila-bila masa atas sebarang sebab dan tanpa sebarang notis terlebih dahulu.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">8.</span>
                                        <span class="tnc-lbl">Terma dan syarat yang terpakai bagi Pencari</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">8.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Senarai adalah dicipta dan disediakan oleh Poster dan pihak ketiga lain yang berada di luar kawalan kami. Senarai, kandunganya dan/atau apa-apa syarat yang dikenakan di olehnya yang dibuat, disediakan dan dipaut dari pihak ketiga yang lain tidak berada dalam kawalan kami. Sekiranya anda meninggalkan Perkhidmatan dan memilih untuk memasuki Laman Web Pihak Ketiga dan/atau Platform Media Sosial, anda akan ditadbir oleh terma dan syarat yang dikenakan oleh pihak ketiga tersebut.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Untuk mengelak keraguan, anda memahami dan mengakui bahawa kami tidak mengawal dan tidak akan bertanggungjawab terhadap mana-mana perkara yang berikut:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Kualiti, keselamatan, kesahan atau moral dari setiap aspek Senarai yang disenaraikan atau dipaparkan pada Perkhidmatan ini; atau</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Kebenaran, kelengkapan, kebolehpercayaan, kesahan atau ketepatan Senarai, kuasa Poster atau mana-mana pihak ketiga yang lain untuk menyediakan atau membuat Senarai, kemampuan Poster dan/atau mana-mana pihak ketiga untuk membayar bagi perkhidmatan anda berdasarkan penerimaan Tugas dan/atau Kerja, dan maklumat lain yang dihantar oleh Poster dan/atau pihak ketiga.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.3</span>
                                                <span class="tnc-desc"><p class="modal-para">Permohonan bagi Senarai</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Anda boleh mengemukakan <i>resume</i> dan/atau informasi permohonan anda untuk memohon Tugas dan/atau Kerja seperti yang dipaparkan oleh Poster (“<b>Permohonan</b>”) melalui Perkhidmatan ini. Sekiranya anda ingin mengemukakan Permohonan anda untuk Senarai seperti yang telah dibuat, disediakan atau dipautkan dari mana-mana pihak ketiga yang lain, anda akan dibawa ke Laman Web Pihak Ketiga dan/atau Platform Media Sosial dan akan ditadbir oleh terma dan syarat yang dikenakan oleh pihak ketiga tersebut.</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Setelah anda mengemukakan Permohonan anda ke Poster dan/atau pihak ketiga, kami tidak mempunyai kawalan ke atas penggunaan atau penzahiran maklumat tersebut oleh Poster dan/atau pihak ketiga.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.4</span>
                                                <span class="tnc-desc"><p class="modal-para">Apabila anda mengemukakan Permohonan untuk Senarai melalui Perkhidmatan ini, kami akan berusaha untuk menghantar Permohonan anda ke Poster. Anda memahami dan mengakui bahawa kami tidak dapat menjamin bahawa Poster akan menerima, diberitahu, memiliki akses, membaca, mengakui atau menjawab Permohonan anda atau bahawa tidak akan ada kesalahan dalam penghantaran atau penyimpanan Permohonan anda dan maklumat yang berkaitan di dalamnya. Walaubagaimanapun, kami akan berusaha untuk memberitahu anda sekiranya perkara-perkara tersebut berlaku.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.5</span>
                                                <span class="tnc-desc"><p class="modal-para">Semasa anda mengemukakan Permohonan melalui Perkhidmatan ini, anda bersetuju untuk kami melakukan pemprosesan automatik berkaitan dengan Permohonan anda dan sebarang komunikasi antara anda dan Poster melalui Perkhidmatan akan diproses dan dianalisis oleh kami mengikut Terma ini.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.6</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda memahami dan mengakui bahawa kami tidak akan bertanggungjawab ke atas kandungan Senarai, kriteria penilaian, proses pengambilan dan temu duga dan/atau penentuan berkenaan dengan Permohonan anda oleh Poster dan/atau pihak ketiga. Selain itu, kami tidak menjamin bahawa Poster dan/atau pihak ketiga akan mempertimbangkan Permohonan anda atau membuat penentuan tertentu berkenaan dengan Permohonan anda.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.7</span>
                                                <span class="tnc-desc"><p class="modal-para">Kami mungkin akan menggunakan maklumat anda untuk menentukan minat anda berkenaan dengan apa-apa Senarai dan mungkin akan berkomunikasi dengan anda berkenaan dengan Senarai prospektif yang lain.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.8</span>
                                                <span class="tnc-desc"><p class="modal-para">Senarai yang tersedia ada mungkin akan luput dari masa anda menghantar  Permohonan anda dan ketika Permohonan tersebut diterima, dan anda mengakui dan memahami bahawa kami tidak mempunyai kawalan atau tanggungjawab terhadap Senarai yang telah luput atau untuk menyampaikan maklumat tersebut sebelum peluputan Senarai tersebut.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.9</span>
                                                <span class="tnc-desc"><p class="modal-para">Kami mungkin akan memberitahu anda jika Poster mengambil tindakan berkaitan dengan Permohonan anda berkenaan dengan Senarai.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">8.10</span>
                                                <span class="tnc-desc"><p class="modal-para">Setelah Poster memberikan Senarai kepada anda dan penerimaan anda terhadap Senarai, hubungan anda dengan Poster akan tertakluk kepada Terma ini, khususnya Klausa 10.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">9.</span>
                                        <span class="tnc-lbl">Terma dan syarat yang terpakai bagi Poster</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">9.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Poster boleh membuat, memaparkan dan menyediakan Senarai di Perkhidmatan ini melalui Akaun anda. Anda akan bertanggungjawab sepenuhnya untuk Senarai yang dipaparkan oleh anda, kandungannya dan/atau apa-apa syarat yang dikenakan olehnya.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">9.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Untuk mengelak keraguan, anda memahami dan mengakui bahawa kami tidak akan bertanggungjawab atau tidak bertanggungjawab terhadap kebenaran, kelengkapan, kebolehpercayaan, kesahan Senarai, kuasa anda dalam menyediakan atau membuat Senarai, kemampuan anda dan/atau mana-mana pihak ketiga untuk membayar perkhidmatan yang diberikan oleh Pencari setelah mereka menerima Tugas dan/atau Kerja dan maklumat lain yang dikemukakan oleh anda.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">9.3</span>
                                                <span class="tnc-desc"><p class="modal-para">Memaparkan Senarai</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Untuk memaparkan Senarai, anda akan diminta untuk memberikan maklumat yang berkenaan dengan Tugas dan/atau Kerja termasuk tetapi tidak terhad kepada sifat dan skop, jangka masa, tarikh penyelesaian yang dijangkakan.</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Senarai tidak boleh mengandungi:</p>
                                                                 <ul class="tnc-list forth-level">
                                                                    <li>
                                                                        <span class="tnc-no">(i)</span>
                                                                        <span class="tnc-desc"><p class="modal-para">Maklumat yang mengelirukan, salah nyata, tidak berkaitan, palsu dan/atau tidak tepat;</p></span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="tnc-no">(ii)</span>
                                                                        <span class="tnc-desc"><p class="modal-para">Nama, logo, atau bahan hak cipta orang yang tidak berkaitan dengan anda;</p></span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="tnc-no">(iii)</span>
                                                                        <span class="tnc-desc"><p class="modal-para">Bahan yang bersifat diskriminasi, eksplisit, lucah, kasar atau kebencian;</p></span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="tnc-no">(iv)</span>
                                                                        <span class="tnc-desc"><p class="modal-para">Sebarang penjualan, promosi atau iklan produk dan perkhidmatan;</p></span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="tnc-no">(v)</span>
                                                                        <span class="tnc-desc"><p class="modal-para">Sebarang francais, skim piramid atau peluang pemasaran pelbagai peringkat (<i>multi-level marketing opportunity</i>); dan</p></span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="tnc-no">(vi)</span>
                                                                        <span class="tnc-desc"><p class="modal-para">Sebarang percubaan untuk mendapatkan perkhidmatan seksual atau mendapatkan perkhidmatan yang tidak bermoral atau menyalahi undang-undang.</p></span>
                                                                    </li>
                                                                </ul>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Kami akan memaparkan Senarai setelah anda mengemukakannya. Walaubagaimanapun, anda memahami dan mengakui bahawa kami tidak dapat menjamin bahawa semua Pencari akan menerima, diberitahu atau memiliki akses ke Senarai atau bahawa tidak akan ada kesalahan dalam penghantaran atau penyimpanan Senarai anda dan maklumat yang berkaitan dengannya. Walaubagaimanapun, kami akan berusaha untuk memberitahu anda sekiranya berlakunya perkara-perkara yang disebut diatas.</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Apabila anda memaparkan Senarai melalui Perkhidmatan ini, anda bersetuju untuk kami melakukan pemprosesan automatik berkaitan dengan Senarai dan sebarang komunikasi di antara anda dan Pencari melalui Perkhidmatan akan diproses dan dianalisis oleh kami menurut Terma ini.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">9.4</span>
                                                <span class="tnc-desc"><p class="modal-para">Pencari dapat mengemukakan Permohonan sebagai respon terhadap Senarai, namun anda memahami dan mengakui bahawa kami tidak dapat menjamin bahawa anda akan menerima, diberitahu dan/atau mempunyai akses ke Permohonan atau bahawa tidak akan ada kesalahan dalam penghantaran atau penyimpanan Senarai.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">9.5</span>
                                                <span class="tnc-desc"><p class="modal-para">Kami mungkin menyediakan alat saringan untuk anda gunakan bagi tujuan memproses dan memeriksa Permohonan.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">9.6</span>
                                                <span class="tnc-desc"><p class="modal-para">Setelah Senarai diberikan kepada Pencari terpilih oleh anda, kami akan memberitahu Pemohon mengenai penerimaan anda. Jika Senarai diterima dan disahkan oleh Pemohon, anda akan dibawa ke halaman web pembayaran untuk melakukan pembayaran yang diperlukan seperti yang dinyatakan di bawah Klausa 11 dan Klausa 12. Kegagalan melakukan pembayaran yang diperlukan menurut Klausa 11 dan Klausa 12 akan mengakibatkan pembatalan dan/atau penangguhan Senarai.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">9.7</span>
                                                <span class="tnc-desc"><p class="modal-para">Setelah Pemohon menerima Senarai, hubungan anda dengan Pemohon akan tertakluk kepada Terma ini, khususnya Klausa 10.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">10.</span>
                                        <span class="tnc-lbl">Terma dan syarat yang mentadbir pelantikan Pemohon oleh Poster</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">10.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Terma berkenaan dengan pelantikan Pemohon Tugas oleh Poster Tugas</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Peruntukan-peruntukan di bawah Klausa 10.1 ini merupakan perjanjian antara Pemohon Tugas dan Poster Tugas (disebut secara individu sebagai “<b>Pihak</b>” dan secara kolektif disebut sebagai “<b>Pihak-pihak</b>”) setelah pengesahan yang berjaya mengenai pelantikan Pemohon Tugas oleh Poster Tugas, dan penerimaan pelantikan oleh Pemohon Tugas berkenaan dengan Senarai Tugas (“<b>Pelantikan Tugas</b>”), berikutan dimana Pemohon Tugas akan dirujuk sebagai Pemegang Tugas.</span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Setelah Pelantikan Tugas bermula, Poster Tugas dengan ini bersetuju untuk melantik Pemegang Tugas untuk menyelesaikan Tugas menurut Senarai Tugas dan berjanji untuk membayar Fi Tugas kepada Pemegang Tugas setelah selesainya Tugas tersebut, dan Pemegang Tugas dengan ini bersetuju untuk menyampaikan Tugas tersebut menurut Senarai Tugas dimana Pemegang Tugas telah dilantik untuk menyelesaikan, sebagai balasan Fi Tugas.</span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Setiap Pihak bertanggungjawab sepenuhnya dalam memastikan bahawa mereka mematuhi kewajipan mereka dibawah Pelantikan Tugas. Setiap Pihak bertanggungjawab dalam menguatkuasakan hak-hak yang dimiliki olehnya. Untuk mengelakkan keraguan, MTP tidak akan bertanggungjawab untuk menguatkuasakan hak yang dimiliki oleh mana-mana Pihak di bawah Klausa 10.1 ini atau undang-undang yang terpakai bagi sebarang kerosakan, atau kerugian yang dialami oleh mana-mana Pihak berkaitan dengan Pelantikan Tugas.</span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Setiap Pihak mengakui dan bersetuju bahawa hubungan dengan Pihak lain adalah hubungan kontraktor bebas (<i>independent contractor</i>) dan tiada apa-apa dalam Terma ini yang akan mewujudkan sebarang bentuk perkongsian, usahasama, agensi atau hubungan kerja diantara Pihak-pihak dan/atau dengan MTP.</span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(e)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Pihak-pihak mengakui dan bersetuju bahawa mereka akan sentiasa mematuhi Terma ini, khususnya Klausa 11 dan Klausa 12.</span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">10.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Terma berkenaan dengan pelantikan Pemohon Kerja oleh Poster Kerja</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Peruntukan-peruntukan di bawah Klausa 10.2 ini merupakan perjanjian antara Pemohon Kerja dan Poster Kerja (disebut secara individu sebagai “<b>Pihak</b>” dan secara kolektif disebut sebagai “<b>Pihak-pihak</b>”) setelah pengesahan yang berjaya mengenai pelantikan Pemohon Kerja oleh Poster Kerja, dan penerimaan pelantikan oleh Pemohon Kerja berkenaan dengan Senarai Kerja (“<b>Pelantikan Kerja</b>”), berikutan dimana Pemohon Kerja akan dirujuk sebagai Pemegang Kerja.</span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Setelah Pelantikan Kerja bermula, Poster Kerja dengan ini bersetuju untuk melantik Pemohon Kerja bagi Kerja tersebut menurut Senarai Kerja atas balasan Fi Kerja menurut terma yang akan atau telah dipersetujui oleh Pihak-pihak secara bersama.</span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Setiap Pihak bertanggungjawab sepenuhnya dalam memastikan bahawa mereka mematuhi kewajipan mereka dibawah Pelantikan Kerja. Setiap Pihak bertanggungjawab dalam menguatkuasakan hak-hak yang dimiliki olehnya. Untuk mengelakkan keraguan, MTP tidak akan bertanggungjawab untuk menguatkuasakan hak yang dimiliki oleh mana-mana Pihak di bawah Klausa 10.2 ini atau undang-undang yang terpakai bagi sebarang kerosakan, atau kerugian yang dialami oleh mana-mana Pihak berkaitan dengan Pelantikan Kerja.</span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Setiap Pihak mengakui dan bersetuju bahawa tiada apa-apa dalam Terma ini yang akan mewujudkan sebarang bentuk perkongsian, usahasama, agensi atau hubungan pekerjaan dengan MTP.</span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(e)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Pihak-pihak mengakui dan bersetuju bahawa mereka akan sentiasa mematuhi Terma ini, khususnya Klausa 11 dan Klausa 12</span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">11.</span>
                                        <span class="tnc-lbl">Fi dan caj</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">11.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda mungkin akan dikenakan caj atau fi untuk penggunaan aspek tertentu Perkhidmatan ini, termasuk tetapi tidak terhad kepada <i>Task Spotlights, Job Spotlights, Candidate Spotlights,</i> Keahlian Premium. Anda juga mungkin akan dikenakan fi bagi pemprosesan Permohonan berkenaan dengan Pelantikan. Sekiranya anda menggunakan aspek Perkhidmatan yang dikenakan caj, anda akan diberikan peluang untuk menyemak dan menerima terma fi yang akan dikenakan berdasarkan Klausa 11 ini.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">11.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Sebelum anda membuat sebarang pembayaran melalui Perkhidmatan ini, anda akan dibawa ke halaman web dimana anda akan dimaklumkan jumlah tepat yang perlu dibayar kepada kami.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">11.3</span>
                                                <span class="tnc-desc"><p class="modal-para">Pembayaran melalui Perkhidmatan ini boleh dibuat hanya dengan menggunakan <i>[FPX, kad kredit, kad debit atau PayPal]</i>.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">11.4</span>
                                                <span class="tnc-desc"><p class="modal-para">Sebelum menyelesaikan transaksi, anda akan dibawak ke skrin pengesahan dimana anda akan diminta untuk mengesahkan maklumat transaksi. Ia adalah tanggungjawab anda untuk mengesahkan bahawa maklumat yang dinyatakan pada skrin tersebut adalah tepat.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">11.5</span>
                                                <span class="tnc-desc"><p class="modal-para">Sekiranya transaksi anda berjaya, anda akan menerima resit pembayaran anda melalui emel. Anda juga boleh memuat turun resit di bawah papan pemuka (<i>dashboard</i>) Akaun di laman web dan/atau aplikasi mudah alih.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">11.6</span>
                                                <span class="tnc-desc"><p class="modal-para">Maklumat yang diberikan oleh anda berkaitan dengan penggunaan Perkhidmatan oleh anda diproses melalui laman web dan/atau aplikasi mudah alih yang terlindung. Walaubagaimanapun, anda mengakui dan bersetuju bahawa keselamatan dan kesulitan penghantaran internet tidak dapat dijamin sepenuhnya dan sebarang maklumat yang diberikan oleh anda mungkin dapat dibaca dan/atau dipintas oleh pihak ketiga. Kami tidak akan bertanggungjawab atas sebarang pintasan dan/atau akses lain yang tidak dibenarkan ke maklumat yang diberikan oleh anda berkaitan dengan penggunaan Perkhidmatan oleh anda.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">11.7</span>
                                                <span class="tnc-desc"><p class="modal-para">Anda akan bertanggungjawab untuk membayar segala cukai, termasuk cukai penjualan dan perkhidmatan, yang mungkin akan terpakai penggunaan Perkhidmatan oleh anda. Cukai ini, jika terpakai, akan ditambahkan ke fi yang dikenakan kepada anda.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">11.8</span>
                                                <span class="tnc-desc"><p class="modal-para">Kecuali dinyatakan sebaliknya, semua yuran dan caj dinyatakan dalam Ringgit Malaysia (MYR).</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">12.</span>
                                        <span class="tnc-lbl">Perkhidmatan <i>Escrow</i></span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">12.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Setelah Pelantikan Tugas, Poster Tugas harus membuat pembayaran pendahuluan Fi Tugas sepenuhnya dan akan memberi kuasa kepada kami untuk mengumpulkan dan menahan dana tersebut bagi pihaknya sehingga selesainya perkhidmatan yang dinyatakan dalam Senarai Tugas oleh Pemegang Tugas.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">12.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Bayaran pendahuluan Fi Tugas akan dipegang oleh kami dan hanya akan dilepaskan setelah berlakunya perkara-perkara yang berikut:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Di mana kami telah menerima arahan dari Poster Tugas untuk melepaskan dana; dan</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Jika ada perselisihan yang belum selesai atau sedang berlangsung, ketika perselisihan tersebut telah diselesaikan dan memihak kepada Pencari Tugas.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">12.3</span>
                                                <span class="tnc-desc"><p class="modal-para">Kami berhak untuk memegang, menolak dan/atau memotong ke atas Fi Tugas apa-apa jumlah dan/atau fi atau caj yang harus dibayar dan/atau dihutang oleh Poster Tugas dan/atau Pemegang Tugas, termasuk tetapi tidak terhad kepada apa-apa fi atau caj menurut Klausa 11.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">12.4</span>
                                                <span class="tnc-desc"><p class="modal-para">Sekiranya kami tidak menerima arahan atau perselisihan dari Poster Kerja atau Pencari Kerja berkenaan dengan Fi Tugas dalam jangka masa satu (1) bulan dari tarikh pembayaran pendahuluan Fi Tugas dibuat, dana akan dikembalikan ke Poster Tugas, tertakluk kepada pemotongan yang dikenakan bagi kos pentadbiran yang dikenakan pada jumlah yang tidak melebihi 5% dari Fi Tugas.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">13.</span>
                                        <span class="tnc-lbl">Hak Harta Intelektual</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">13.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Perkhidmatan ini mengandungi bahan hak cipta, nama dan tanda niaga dan maklumat hak milik lain, termasuk, tetapi tidak terhad kepada, teks, perisian, foto dan grafik, dan mungkin di masa depan termasuk video, grafik, muzik dan suara, yang mungkin dilindungi oleh Hak Harta Intelektual.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">13.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Kecuali sebagaimana dinyatakan secara jelas dalam Terma ini, tidak ada yang terkandung di dalam ini yang akan ditafsirkan sebagai memberi anda atau mana-mana pihak ketiga lesen atau hak, secara tersirat, <i>estoppel</i> atau sebaliknya, di bawah undang-undang, kaedah atau peraturan, termasuk Hak Harta Intelektual. Anda bersetuju bahawa semua harta intelektual yang terdapat pada Perkhidmatan ini adalah hak milik pemilik masing-masing. Anda tidak boleh menggunakan, menyalin, menghantar, menerbitkan, menyebarkan atau menyiarkan sebarang karya termasuk tetapi tidak terhad kepada teks, gambar, video dan kandungan lain yang terdapat di Perkhidmatan ini tanpa kebenaran terlebih dahulu dari pemilik masing-masing.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">13.3</span>
                                                <span class="tnc-desc"><p class="modal-para">Walauapapun dan sebagai tambahan kepada apa-apa undang-undang bertulis yang terpakai berkenaan dengan Hak Harta Intelektual dan pelanggaran hak tersebut, salah satu perkara berikut akan dianggap sebagai tindakan yang tidak dibenarkan jika dilakukan tanpa persetujuan bertulis dan/atau lesen dari MTP atau pemilik masing-masing:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Menyalin, menerbitkan semula, menyebarkan atau menghantar mana-mana bahagian Perkhidmatan atau kandungannya;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Menggunakan Perkhidmatan atau kandungannya sebagai sebahagian daripada sistem pengambilan atau pangkalan data penyimpanan maklumat (<i>information retrieval storage retrieval system or database</i>) yang ditawarkan untuk pengedaran komersial;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Membuat kompilasi atau karya terbitan Perkhidmatan atau kandungannya;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Menggunakan Perkhidmatan atau kandungannya dengan apa-apa cara yang melanggar hak cipta, cap niaga atau harta intelektual atau hak milik;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(e)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Membuang atau mengaburkan hak cipta atau harta intelektual lain atau notis hak milik;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(f)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Menyediakan mana-mana bahagian Perkhidmatan atau kandungannya dalam apa-apa media lain;</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(g)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Membuang atau <i>reverse engineering</i> mana pun perisian yang berkenaan dengan Perkhidmatan; atau</p></span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(h)</span>
                                                            <span class="tnc-desc"><p class="modal-para">Menggunakan proses automatik atau lain-lain untuk mengumpulkan data dan maklumat dari Perkhidmatan.</p></span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">14.</span>
                                        <span class="tnc-lbl">Pautan</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">14.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Perkhidmatan mungkin mengandungi pautan dan/atau rujukan ke Laman Web Pihak Ketiga dan/atau Platform Media Sosial. Ini mungkin termasuk pengiklanan atau bentuk komunikasi lain yang berasal dari Laman Web Pihak Ketiga dan/atau Platform Media Sosial. Sekiranya Perkhidmatan merangkumi pengiklanan seperti yang disebutkan, kami tidak akan bertanggungjawab ke atas kesahan atau apa-apa kesalahan atau ketidaktepatan dalam bahan pengiklan atau untuk tindakan atau peninggalan pengiklan.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">14.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Walaupun kami memiliki langkah-langkah pemantauan, kami tidak akan bertanggungjawab atas ketepatan atau kelengkapan kandungan yang diterbitkan, pendapat yang dinyatakan atau maklumat yang diberikan oleh Laman Web Pihak Ketiga dan/atau Platform Media Sosial, dan penyertaan atau rujukan kepada  Laman Web Pihak Ketiga dan/atau Platform Media Sosial oleh Perkhidmatan kami tidak menunjukkan persetujuan atau sokongan terhadap laman web dan platform tersebut oleh kami. Kami tidak akan terlibat dalam, dan tidak akan bertanggungjawab untuk sebarang transaksi antara anda dan syarikat yang dihubungkan melalui Laman Web Pihak Ketiga dan/atau Platform Media Sosial.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">14.3</span>
                                                <span class="tnc-desc"><p class="modal-para">Sekiranya anda memutuskan untuk meninggal Perkhidmatan ini bagi mengakses ke Laman Web Pihak Ketiga dan/atau Platform Media Sosial, anda berbuat demikian pada risiko anda sendiri.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">15.</span>
                                        <span class="tnc-lbl">Data peribadi</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">Notis Perlindungan Data Peribadi kami mentadbir penggunaan setiap maklumat peribadi yang anda berikan kepada kami melalui Perkhidmatan ini. Salinan Notis Perlindungan Data Peribadi boleh diakses di sini [insert hyperlink].</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">16.</span>
                                        <span class="tnc-lbl">Penolak tuntutan</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">Kami tidak akan bertanggungjawab atas apa-apa kerosakan/ganti rugi, termasuk tetapi tidak terhad kepada kerosakan/ganti rugi langsung, tidak langsung, khas, berbangkit, punitif atau kecil, atau kerosakan/ganti rugi kerana kehilangan penggunaan, keuntungan, data atau kerosakan/ganti rugi tidak ketara yang  lain, kerosakan/ganti rugi muhibah (<i>goodwill</i>) atau reputasi, yang timbul keluar atau berkaitan dengan penggunaan, ketidakupayaan untuk menggunakan atau kegagalan Perkhidmatan ini. Kami secara nyata menolak sebarang jaminan tersurat dan tersirat, termasuk tetapi tidak terhad kepada kelengkapan atau ketepatan setiap maklumat yang diberikan oleh atau disediakan untuk anda. Kami tidak menjamin bahawa Perkhidmatan dan mana-mana dan semua kemudahan atau kemampuan yang berkaitan dengannya akan bebas daripada gangguan atau kesalahan, bahawa kecacatan akan diperbaiki, atau bahawa peralatan dan perisian yang menjadikan Perkhidmatan tersedia bebas daripada virus atau komponen berbahaya yang lain .</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">17.</span>
                                        <span class="tnc-lbl">Tanggung rugi</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">Anda bersetuju untuk tanggung rugi, mempertahankan dan tidak memudaratkan MTP, pekerja, pengarah, ejen dan/atau vendor kami terhadap semua tuntutan, kerosakan/ganti rugi, kos dan perbelanjaan apa sahaja (termasuk kos atas dasar tanggung rugi) yang mungkin kami tanggung atau yang mungkin diberikan terhadap kami dan yang timbul daripada (a) akses atau penggunaan Perkhidmatan ini oleh anda atau orang yang diberi kuasa oleh anda; (b) sebarang pelanggaran terhadap janji, jaminan atau penyataan yang diberikan oleh anda dalam Terma ini; dan (c) pelanggaran anda terhadap hak pihak ketiga (termasuk harta intelektual, harta atau hak privasi). Ganti rugi ini tidak akan dikenakan sebarang had liabiliti dan merangkumi tanpa had kos dan perbelanjaan termasuk kos profesional yang ditanggung dalam menghadapi dan menangani urusan tuntutan yang dibuat tanpa mengira sama ada prosiding telah dimulakan.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">18.</span>
                                        <span class="tnc-lbl">Lain-lain</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">18.1</span>
                                                <span class="tnc-desc"><p class="modal-para">Terma ini akan ditadbir oleh undang-undang Malaysia dan kesemua pihak bersetujui dengan bidang kuasa eksklusif mahkamah-mahkamah Malaysia.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">18.2</span>
                                                <span class="tnc-desc"><p class="modal-para">Segala kegagalan atau kelewatan oleh kami atau anda dalam menguatkuasakan mana-mana hak di bawah Terma ini tidak akan diterima sebagai pengabaian hak tersebut atau menjejaskan kejadian lain atau kejadian seterusnya, atau merosotkan apa-apa hak atau remedi yang berkaitan dengannya atau dengan tidak sama sekali mengubah atau menyusutkan hak kami atau hak anda di bawah Terma ini.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">18.3</span>
                                                <span class="tnc-desc"><p class="modal-para">Sekiranya mana-mana klausa dalam Terma ini diisytiharkan oleh mahkamah di bawah mana-mana bidang kuasa sebagai tidak sah atau tidak dapat dikuatkuasakan, ketidaksahan atau ketidakkuatkuasaan tersebut tidak sama sekali akan mempengaruhi klausa lain atau bahagian mana-mana klausa yang lain, dimana kesemuanya akan terus berkuatkuasa dan berkesan selagi Terma ini berkemampuan untuk terus berkuatkuasa tanpa terma yang tidak dapat dikuatkuasakan tersebut.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">18.4</span>
                                                <span class="tnc-desc"><p class="modal-para">Terma ini akan mengikat dan memanfaatkan kesemua pihak dan penerima penyerahan hak masing-masing (<i>respective permitted assigns</i>), wakil (<i>representatives</i>) dan pengganti hakmilik (<i>succesors in title</i>) yang dibenarkan.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">18.5</span>
                                                <span class="tnc-desc"><p class="modal-para">Kami boleh menyerahkan, mewakilkan atau memindahkan hak atau kewajipan dibawah Terma ini mengikut budi bicara kami sendiri kepada pihak ketiga. Anda tidak boleh menyerahkan, mewakilkan atau memindahkan hak atau kewajipan di bawah Terma ini kepada pihak ketiga tanpa persetujuan bertulis dari kami.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">18.6</span>
                                                <span class="tnc-desc"><p class="modal-para">Terma ini mengandungi keseluruhan pemahaman antara Pihak-pihak pada Terma ini dan hanya berkenaan dengan subjek perkara tersebut dan menggantikan segala perjanjian atau pemahaman, pujukan atau syarat, tersurat atau tersirat, lisan atau bertulis yang sebelumnya.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">18.7</span>
                                                <span class="tnc-desc"><p class="modal-para">Sekiranya terdapat percanggahan antara versi Bahasa Inggeris dan versi Bahasa Malaysia Terma ini, versi Bahasa Inggeris akan digunapakai.</p></span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">18.8</span>
                                                <span class="tnc-desc"><p class="modal-para">Untuk sebarang pertanyaan mengenai Terma ini, sila hubungi MTP di <a href="mailto:legal@maketimepay.com">legal@maketimepay.com</a>.</p></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_task_od">
                        <span class="btn-label"><?php echo lang('sign_up_terms_of_use_close'); ?></span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <!--<button type="button" id="btnAgreeTNC" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                        <span class="btn-label"><?php /*echo lang('sign_up_terms_of_use_agree'); */?></span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>-->
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <!-- Modal - PDPA -->
    <div id="modal_pdpa" class="modal modal-pdpa fade" aria-labelledby="modal_pdpa" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"><?php echo lang('sign_up_personal_data_protection_notice'); ?></div>
	            <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
		            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
			            <line x1="18" y1="6" x2="6" y2="18"></line>
			            <line x1="6" y1="6" x2="18" y2="18"></line>
		            </svg>
	            </button>
                <div class="modal-body">
                    <ul class="nav nav-pills nav-pills-help" id="pills-tabPDPA" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pdpaEnTab-tab" data-toggle="pill" href="#pdpaEnTab" role="tab" aria-controls="pdpaEnTab" aria-selected="true">English</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pdpaBmTab-tab" data-toggle="pill" href="#pdpaBmTab" role="tab" aria-controls="pdpaBmTab" aria-selected="false">BM</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContentTnc">
                        <div class="tab-pane fade show active" id="pdpaEnTab" role="tabpanel">
                            <div class="tnc-rows-container">
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">1.</span>
                                        <span class="tnc-lbl">General</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">1.1</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">The Personal Data Protection Act 2010 <b>(“PDPA”)</b> requires an organisation that processes personal data in a commercial transaction to comply with data protection principles, including to inform data subjects such as yourselves on the manner in which we process your personal data.</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">1.2</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">This Personal Data Protection Notice <b>(“Notice”)</b> describes how we, MakeTimePay Sdn. Bhd. [1373558-V] <b>(“MTP”)</b> collect, use, process and disclose your personal data through the use of our services via our website and/or mobile application <b>(“Services”)</b>.</p>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">2.</span>
                                        <span class="tnc-lbl">Collection of Personal Data</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">By accessing, using and registering yourself with the Services, you consent to us processing the personal data which:</p>
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">(a)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">you provide when you create and/or log in to your account, including when you choose to connect your account with an external third-party service or application;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(b)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">you provide when you fill up any forms or documents on our site;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(c)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">you provide when you interact and communicate with us in connection with the Services, through various methods such as emails, social media and letters, telephone calls and conversation you have with our personnel and/or our authorised agents;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(d)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">you provide when you interact and communicate with us at or during any events or activities;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(e)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">you provide when you enter contests that we have organised;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(f)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">we collect from cookies, advertising identifiers and other similar technology;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(g)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">we collect from your mobile phone pursuant to your use of the Services;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(h)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">we obtain from any service provider we engage; and</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(i)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">we obtain from public domains and third parties such as statutory bodies or government agencies.</p>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">3.</span>
                                        <span class="tnc-lbl">Types of Personal Data</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">3.1</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">The personal data collected may include but is not limited to personal data that relates to:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">your identity (including your name, date of birth, age, National Registration Identification Card/passport details, gender, education level, employment history and photograph);</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">your contact information (including your telephone numbers, e-mail and mailing addresses);</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">your payment information (including your credit card information);</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">your demographic information (including your zip/postal code or region, hometown);</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(e)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">your qualifications, skills, interests and work history;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(f)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">your use of Services, including the date and time you use the Services, pages viewed, and other related details;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(g)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">device data such as information concerning your device IP address, app features, app crashes and other system activities;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(h)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">any other personal data that you provide to us for the purpose of the commercial transaction between us.</p>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">3.2</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">Where any third-party information is provided to us, including but not limited to information in relation to your present or past employer(s), present or past employee(s), present or past colleagues and/or family members, it is assumed that such information is correct, accurate, up to date and complete and that you have obtained the necessary consent to disclose the same.</p>
                                                </span>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">4.</span>
                                        <span class="tnc-lbl">Purposes for Processing Personal Data</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">4.1</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">Where applicable, we will process your personal data for the following purposes:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to complete registrations and/or creation of accounts in connection with the Services;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to facilitate your use of and access to the Services;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to process transactions;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to verify the information supplied by you, including but not limited to your academic and professional qualification;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(e)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to monitor and analyse your usage of the Services and other trends;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(f)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to personalise and improve the Services and your experiences with the Services (such as to provide content or features to further match user interest);</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(g)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to increase the Services’ functionality and user friendliness;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(h)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to request feedback and to otherwise contact you about your use of the Services;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(i)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to respond to your emails, questions, comments, requests and complaints, and to provide customer service;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(j)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to send you confirmations, updates, security alerts, additional information about the Services and support and administrative messages;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(k)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">for research, statistical studies and/or data analysis;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(l)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to provide to you marketing information relating to the Services and/or to facilitate the communication and provision of opportunities, offerings, products and services that we foresee would be valuable to you;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(m)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to pool risks profiles for potential insurers for the purpose of offering and/or providing and/or recommending to you any insurance products;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(n)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to facilitate our internal administration such as auditing, data analysis and maintaining database records;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(o)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to process transactions in respect of the use of the Services</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(p)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">to facilitate our business continuity management and procedures; and</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(q)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">for all other purposes that is incidental, ancillary and/or in furtherance to the above.</p>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.2</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">The information is necessary to us. If you do not provide and/or allow us to process all the information as requested, we may not be able to accomplish the above stated purposes.</p>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">5.</span>
                                        <span class="tnc-lbl">Sensitive Personal Data (if applicable)</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">By signifying your consent, you have agreed to grant your explicit consent for us to process your sensitive personal data such as your mental or physical health and condition, religious beliefs and the commission or alleged commission by you of any offence for the purposes described above or as required by law.</p>
                                        <p class="modal-para">We may also obtain your sensitive personal information from other parties when it is allowed by law.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">6.</span>
                                        <span class="tnc-lbl">Disclosure of Personal Data</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">We may, if necessary, disclose your personal data to the following parties (including those within and outside Malaysia):</p>
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">(a)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">any other persons, individuals or corporate entities using the Services as provided by MTP <b>(“Users”)</b>, including but not limited to Users that are accessing and/or using the Services for the purposes of posting a task listing and seeking candidates to carry out tasks for and on its behalf <b>(“Job Posters”)</b>;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(b)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">our agents, vendors, consultants, and other service providers (including but not limited to professional, financial and/or payment service providers) that provide services including data processing services relating to the purposes above;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(c)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">other third parties contracted by us and our strategic partners that assist and complement our provision of the Services to you including but not limited to marketing/analytics providers and insurance providers (of which you may or may not have active policy(ies);</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(d)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">advertisers (if any);</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(e)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">your agents and advisors (including but not limited to professional advisers) and any other person notified and authorised by you;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(f)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">government agencies/statutory bodies/authorities;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(g)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">our assignees or potential assignees and successors-in-title;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(h)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">any person, who is under a duty of confidentiality and who has undertaken the responsibility to keep such data confidential;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(i)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">the general public, through user generated content via the Services; and</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(j)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">any person during or in connection with any merger, sale of company assets, consolidation or restructuring, financing or acquisition of all or a portion of our business by or into another company (if any).</p>
                                                </span>
                                            </li>
                                        </ul>
                                        <p class="modal-para">Other than as set out above, we will not disclose your personal data without your consent. However, we may also disclose your personal data (within and outside Malaysia) in good faith, (i) to comply with requirements of the government, law enforcement agency, any authorities to whom we are subject to or any orders of court; (ii) as is necessary or relevant in relation to any legal process; (iii) if required or authorised by law; or (iv) as is reasonably necessary or appropriate to protect the rights, property, or safety of our related and associated companies and affiliates (whether locally or globally) (if any), our users, our employees, copyright owners, third-parties or the public.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">7.</span>
                                        <span class="tnc-lbl">Access, corrections and complaints</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">If you would like to make any inquiries or complaints or requests to access, correct or limit our processing of your personal data, you may contact our [Customer Experience Officer] at [insert phone number and email]. Any request to access or correct personal data, or limit the processing of personal data may be subject to a fee and will be subject to the prevailing data protection laws in Malaysia.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">8.</span>
                                        <span class="tnc-lbl">Variation to this Notice</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">We may review and update this Notice from time to time to reflect changes in the law, changes in our business practices, procedures and structure, and the community's changing privacy expectations.</p>
                                        <p class="modal-para">We will take all reasonable steps to notify you of any variations to this Notice. You may also obtain a copy of the Notice at [insert link to the Notice] to ensure that you are aware of the most recent version which will apply.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <p class="modal-para">In the event of any inconsistencies between the English version and the Bahasa Malaysia version of this notice, the English version shall prevail.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade show" id="pdpaBmTab" role="tabpanel">
                            <div class="tnc-rows-container">
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">1.</span>
                                        <span class="tnc-lbl">Am</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">1.1</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">Akta Perlindungan Data Peribadi 2010 <b>(“PDPA”)</b> memerlukan organisasi yang memproses data peribadi dalam transaksi komersial untuk mematuhi prinsip-prinsip perlindungan data, termasuk memberitahu subjek data seperti diri anda mengenai cara kami memproses data peribadi anda.</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">1.2</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">Notis Perlindungan Data Peribadi ini <b>(“Notis”)</b> menerangkan bagaimana kami, MakeTimePay Sdn. Bhd. [1373558-V] <b>(“MTP”)</b> mengumpulkan, menggunakan, memproses dan menzahirkan data peribadi anda melalui penggunaan perkhidmatan kami melalui laman web dan/atau aplikasi mudah alih kami <b>(“Perkhidmatan”)</b>.</p>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">2.</span>
                                        <span class="tnc-lbl">Pengumpulan Data Peribadi</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">Dengan mengakses, menggunakan dan mendaftarkan diri anda dengan Perkhidmatan ini, anda membenarkan kami untuk memproses data peribadi yang:</p>
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">(a)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">anda berikan semasa anda membuat dan/atau log masuk ke akaun anda, termasuk ketika anda memilih untuk menyambungkan akaun anda dengan perkhidmatan atau aplikasi pihak ketiga;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(b)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">anda berikan semasa anda mengisi borang atau dokumen di laman web kami;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(c)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">anda berikan semasa anda berinteraksi dan berkomunikasi dengan kami berkenaan dengan Perkhidmatan ini, melalui pelbagai kaedah seperti emel, media sosial dan surat, panggilan telefon dan perbualan diantara anda dan kakitangan kami dan/atau ejen kami yang sah;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(d)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">anda berikan semasa anda berinteraksi dan berkomunikasi dengan kami di atau semasa sebarang acara atau aktiviti;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(e)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">anda berikan semasa anda memasuki pertandingan yang telah kami anjurkan;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(f)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">kami mengumpulkan dari kuki <i>(cookies)</i>, pengecam iklan <i>(advertising identifiers)</i> dan teknologi lain yang serupa dengannya;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(g)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">kami mengumpulkan dari telefon bimbit anda berdasarkan penggunaan Perkhidmatan ini oleh anda;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(h)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">kami dapatkan dari mana-mana pembekal perkhidmatan yang kami lantik; dan</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(i)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">kami dapatkan dari domain awam dan pihak ketiga seperti badan berkanun atau agensi kerajaan.</p>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">3.</span>
                                        <span class="tnc-lbl">Jenis Data Peribadi</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">3.1</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">Data peribadi yang dikumpulkan merangkumi tetapi tidak terhad kepada data peribadi yang berkaitan dengan:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">identiti anda (termasuk nama, tarikh lahir, umur, butiran Kad Pengenalan/pasport, jantina, tahap pendidikan, sejarah pekerjaan dan gambar anda);</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">maklumat hubungan anda (termasuk nombor telefon, emel dan alamat surat-menyurat anda);</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">maklumat pembayaran anda (termasuk maklumat kad kredit anda);</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">maklumat demografi anda (termasuk poskod atau daerah, kampung halaman anda);</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(e)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">kelayakan, kemahiran, minat dan sejarah kerja anda;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(f)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">penggunaan Perkhidmatan ini oleh anda, termasuk tarikh dan masa anda menggunakan Perkhidmatan, halaman yang dilihat, dan maklumat lain yang berkaitan;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(g)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">data peranti seperti maklumat mengenai alamat IP peranti anda, ciri aplikasi, kerosakan aplikasi dan aktiviti sistem lain;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(h)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">sebarang data peribadi lain yang anda berikan kepada kami bagi tujuan transaksi komersial diantara kita.</p>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">3.2</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">Sekiranya terdapat maklumat pihak ketiga yang diberi kepada kami, termasuk tetapi tidak terhad kepada maklumat berkaitan dengan majikan anda yang terkini atau yang lalu, rakan kerja terkini atau yang lalu dan/atau ahli keluarga, ia diandaikan bahawa maklumat tersebut betul, tepat, terkini dan lengkap dan anda telah mendapat persetujuan yang deperlukan untuk menzahirkannya.</p>
                                                </span>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">4.</span>
                                        <span class="tnc-lbl">Tujuan Memproses Data Peribadi</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">4.1</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">Sekiranya berkenaan, kami akan memproses data peribadi anda untuk tujuan berikut:</p>
                                                    <ul class="tnc-list forth-level">
                                                        <li>
                                                            <span class="tnc-no">(a)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk menyelesaikan pendaftaran dan/atau penciptaan akaun yang berkaitan dengan Perkhidmatan ini;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(b)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk memudahkan penggunaan dan akses ke Perkhidmatan ini oleh anda;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(c)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk memproses transaksi;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(d)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk mengesahkan maklumat yang anda berikan, termasuk tetapi tidak terhad kepada kelayakan akademik dan profesional anda;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(e)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk memantau dan menganalisis penggunaan Perkhidmatan ini oleh anda dan trend lain;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(f)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk menyesuaikan dan meningkatkan Perkhidmatan dan pengalaman anda dengan Perkhidmatan ini (seperti menyediakan kandungan atau ciri untuk lebih sesuai dengan minat pengguna);</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(g)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk meningkatkan fungsi Perkhidmatan ini dan kemesraan pengguna;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(h)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk meminta maklumbalas dan untuk menghubungi anda mengenai penggunaan Perkhidmatan ini oleh anda;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(i)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk menjawab emel, pertanyaan, komen, permintaan dan aduan anda, dan untuk memberikan perkhidmatan pelanggan;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(j)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk menghantar pengesahan, kemaskini, amaran keselamatan, maklumat tambahan mengenai Perkhidmatan ini dan mesej sokongan dan pentadbiran kepada anda;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(k)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk penyelidikan, kajian statistik dan/atau analisis data;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(l)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk memberi anda maklumat pemasaran yang berkaitan dengan Perkhidmatan dan/atau untuk memudahkan komunikasi dan penyediaan peluang, tawaran, produk dan perkhidmatan yang kami jangkakan akan menjadi berharga bagi anda;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(m)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk mengumpulkan profil risiko untuk syarikat insurans yang berpotensi untuk tujuan menawarkan dan/atau menyediakan dan/atau mencadangkan apa-apa produk insurans kepada anda;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(n)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk memudahkan pentadbiran dalaman kami seperti pengauditan, analisis data dan penyelenggaraan rekod pangkalan data;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(o)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk memproses transaksi berkenaan dengan penggunaan Perkhidmatan;</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(p)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk memudahkan pengurusan dan prosedur pelansungan perniagaan kami; dan</p>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <span class="tnc-no">(q)</span>
                                                            <span class="tnc-desc">
                                                                <p class="modal-para">untuk semua tujuan lain yang bersifat sampingan dan / atau yang berkaitan dengan perkara di atas.</p>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">4.2</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">Maklumat tersebut diperlukan oleh kami. Sekiranya anda tidak memberikan dan/atau membenarkan kami untuk memproses semua maklumat seperti yang diminta, kami mungkin tidak dapat memenuhi tujuan yang dinyatakan di atas.</p>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">5.</span>
                                        <span class="tnc-lbl">Data Peribadi Sensitif (jika berkenaan)</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">Dengan menandakan persetujuan anda, anda telah bersetuju untuk memberikan persetujuan eksplisit anda untuk kami memproses data peribadi anda yang sensitif seperti kesihatan dan keadaan mental atau fizikal anda, kepercayaan agama dan perlakuan atau tuduhan perlakuan kesalahan oleh anda untuk tujuan yang dinyatakan di atas atau seperti yang dikehendaki di bawah undang-undang.</p>
                                        <p class="modal-para">Kami juga boleh mendapatkan maklumat peribadi anda yang sensitif dari pihak lain apabila ia dibenarkan di bawah undang-undang.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">6.</span>
                                        <span class="tnc-lbl">Penzahiran Data Peribadi</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">Jika perlu, kami mungkin menzahirkan data peribadi anda kepada pihak-pihak yang berikut (termasuk yang berada di dalam dan di luar Malaysia):</p>
                                        <ul class="tnc-list third-level">
                                            <li>
                                                <span class="tnc-no">(a)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">mana-mana orang, individu atau entiti korporat lain yang menggunakan Perkhidmatan seperti yang disediakan oleh MTP <b>("Pengguna")</b>, termasuk tetapi tidak terhad kepada Pengguna yang mengakses dan/atau menggunakan Perkhidmatan ini untuk tujuan memaparkan senarai tugas dan mencari calon untuk menjalankan tugas tersebut bagi pihaknya <b>("Poster Kerja")</b>;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(b)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">ejen, vendor, perunding dan pembekal perkhidmatan kami yang lain (termasuk tetapi tidak terhad kepada pembekal perkhidmatan profesional, kewangan dan/atau pembayaran) yang menyediakan perkhidmatan termasuk perkhidmatan pemprosesan data yang berkaitan dengan tujuan di atas;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(c)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">pihak ketiga lain yang dikontrak oleh kami dan rakan strategik kami yang membantu dan melengkapkan bekalan Perkhidmatan kami kepada anda termasuk tetapi tidak terhad kepada pembekal pemasaran/analitik dan syarikat insurans (di antaranya anda mungkin atau tidak mempunyai polisi aktif bersamanya);</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(d)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">pengiklan (jika ada);</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(e)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">ejen dan penasihat anda (termasuk tetapi tidak terhad kepada penasihat profesional) dan mana-mana orang lain yang diberitahu dan diberi kuasa oleh anda;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(f)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">agensi kerajaan/badan berkanun/pihak berkuasa;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(g)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">penerima penyerahan hak kami atau bakal penerima penyerahan hak dan pengganti hak milik;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(h)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">mana-mana orang, yang berada di bawah tanggungjawab kerahsiaan dan yang telah memikul tanggungjawab untuk merahsiakan data tersebut;</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(i)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">orang awam, melalui kandungan yang dihasilkan pengguna melalui Perkhidmatan ini; dan</p>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="tnc-no">(j)</span>
                                                <span class="tnc-desc">
                                                    <p class="modal-para">mana-mana pihak yang berkaitan dengan penggabungan, penjualan aset syarikat, penyatuan atau penyusunan semula, pembiayaan atau pemerolehan semua atau sebahagian perniagaan kami oleh atau ke dalam syarikat lain (jika ada).</p>
                                                </span>
                                            </li>
                                        </ul>
                                        <p class="modal-para">Selain dari yang dinyatakan di atas, kami tidak akan menzahirkan data peribadi anda tanpa persetujuan anda. Walau bagaimanapun, kami juga mungkin menzahirkan data peribadi anda (di dalam dan di luar Malaysia) dengan ikhlas, (i) untuk mematuhi arahan kerajaan, agensi penguatkuasaan undang-undang, mana-mana pihak berkuasa yang atau perintah mahkamah; (ii) sebagaimana yang perlu atau relevan berkaitan dengan proses undang-undang; (iii) jika dikehendaki atau dibenarkan dibawah undang-undang; atau (iv) sebagaimana yang wajar atau sesuai untuk melindungi hak, harta benda, atau keselamatan syarikat berkaitan dan perkongsian kami (sama ada tempatan atau luar negara) (jika ada), pengguna dan pekerja kami, pemilik hak cipta, pihak ketiga atau orang awam.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">7.</span>
                                        <span class="tnc-lbl">Akses, pembetulan dan aduan</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">Sekiranya anda ingin membuat pertanyaan atau aduan atau permintaan untuk mengakses, membetulkan atau mengehadkan pemprosesan data peribadi anda, anda boleh menghubungi kami [Pengawai Pengalaman Pelanggan] at [insert phone number and email].  Segala permintaan untuk mengakses atau membetulkan data peribadi, atau mengehadkan pemrosesan data peribadi mungkin dikenakan yuran dan akan tertakluk kepada oleh undang-undang perlindungan data yang berkenaan di Malaysia.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <span class="tnc-no">8.</span>
                                        <span class="tnc-lbl">Perubahan pada Notis ini</span>
                                    </div>
                                    <div class="tnc-desc second-level">
                                        <p class="modal-para">Kami mungkin akan menyemak dan mengemaskini Notis ini dari semasa ke semasa untuk mencerminkan perubahan undang-undang, perubahan dalam amalan, prosedur dan struktur perniagaan kami, dan jangkaan privasi masyarakat yang berubah.</p>
                                        <p class="modal-para">Kami akan mengambil semua langkah yang munasabah untuk memberitahu anda mengenai sebarang perubahan pada Notis ini. Anda juga boleh mendapatkan salinan Notis ini di [insert link to the Notice] untuk memastikan bahawa anda mengetahui versi terbaru yang akan digunapakaikan.</p>
                                    </div>
                                </div>
                                <div class="tnc-row">
                                    <div class="tnc-title first-level">
                                        <p class="modal-para">Sekiranya terdapat percanggahan antara versi Bahasa Inggeris dan versi Bahasa Malaysia Notis ini, versi Bahasa Inggeris akan digunapakai.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_task_od">
                        <span class="btn-label"><?php echo lang('sign_up_terms_of_use_close'); ?></span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <!--<button type="button" id="btnAgreePDPA"  class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                        <span class="btn-label"><?php /*echo lang('sign_up_terms_of_use_agree'); */?></span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>-->
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->



  <!--   Core JS Files   -->
    <script src="<?php echo url_for('/assets/js'); ?>/core/jquery.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/core/jquery-ui.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/core/popper.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/core/bootstrap.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!--  Google Maps Plugin    -->
    <!-- Chart JS -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/chartjs.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/dragscrollable.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/dragscroll.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/split.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
    <!--<script src="../assets/js/black-dashboard.min.js?v=1.0.0"></script>-->
    <script src="<?php echo url_for('/assets'); ?>/demo/demo.js"></script>

    <script src="<?php echo url_for('/assets/js'); ?>/bubble.js"></script>

        <!--   Extraaa   Plugins JS Files   -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/moment-with-locales.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/tempusdominus-bootstrap-4.min.js"></script>

    <script src="<?php echo url_for('/assets/js'); ?>/plugins/bootstrap-tagsinput.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/swiper.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/bootstrap-slider.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/jquery.validate.js"></script>
    
    <!--  Search Suggestion Plugin    -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/typeahead.bundle.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/bloodhound.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/handlebars.js"></script>
    
    <!-- Chart JS -->
    <script src='<?php echo url_for('/assets/js'); ?>/plugins/snap.svg-min.js'></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/chart.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/progressbar.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/d3.v3.min.js"></script>
     <!--  Notifications Plugin    -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/bootstrap-notify.js"></script>
    
    <!--  Custom JS    -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/dropzone.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/chat-sidebar.js"></script>
    <script src="<?php echo url_for('/js-mtp'); ?>/script.js<?php echo  '?' . option('script_version') ?>"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/main.js<?php echo  '?' . option('script_version') ?>"></script>




    <script>
        // Task Listing Filter - Range Budget Hour
        if($('#range_budget_hour_filter').length) {
            var range_budget_hour_filter = new Slider("#range_budget_hour_filter", {
                min: 10,
                max: 100,
                step: 1,
                value: [30, 65],
                range: true,
                tooltip_position: 'bottom',
                formatter: function (value) {
                    return 'RM' + value[0] + ' - RM' + value[1];
                }
            });
        }

        // Task Listing Filter - Range Budget Lumpsum
        if($('#range_budget_lumpsum_filter').length) {
            var range_budget_lumpsum_filter = new Slider("#range_budget_lumpsum_filter", {
                min: 10,
                max: 1000,
                step: 50,
                value: [300, 650],
                range: true,
                tooltip_position: 'bottom',
                formatter: function (value) {
                    return 'RM' + value[0] + ' - RM' + value[1];
                }
            });
        }

    </script>
    

    <!-- Sidebar Hover Function -->
    <script>
        // Sidebar Hover Function
        const $dropdown = $(".sidebar .dropdown");
        const $dropdownToggle = $(".sidebar .dropdown-toggle");
        const $dropdownMenu = $(".sidebar .dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function() {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                    function() {
                        const $this = $(this);
                        $this.addClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "true");
                        $this.find($dropdownMenu).addClass(showClass);
                    },
                    function() {
                        const $this = $(this);
                        $this.removeClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "false");
                        $this.find($dropdownMenu).removeClass(showClass);
                    }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });


        $('.card-header .dropdown-menu .nav-tabs .nav-link').on("click.bs.dropdown", function(e) {
            $(this).tab('show');
            e.stopPropagation();
        });
        $('.col-account-header .dropdown.col-account .switch-toggle .slider-toggler').on("click", function(e) {
            e.stopPropagation();
        });

    </script>
    <!-- Sidebar Hover Function -->

    
    <script>
        (function($) {
            "use strict";
            $(document).ready(function() {
                if (document.querySelector('.end-of-chat')) {
                    document.querySelector('.end-of-chat').scrollIntoView();
                }
            });
    
        })(jQuery);
        
        
    </script>
    
<?php if( isset($scripts) ) echo $scripts; ?>

</body>


</html>


