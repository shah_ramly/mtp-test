<!DOCTYPE html>
<html lang="<?php echo !isset($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) || ($_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] == 'EN') ? 'en':'ms' ?>">

<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P7GCVWH');</script>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo url_for('/assets/'); ?>/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?php echo url_for('/assets/'); ?>/img/favicon.png">
    <title>My Company Profile | MakeTimePay</title>

    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/'); ?>/css/glyphicons.css" rel="stylesheet">
    <link href="<?php echo url_for('/assets/'); ?>/css/font-awesome.min.css" rel="stylesheet">

    <!-- Nucleo Icons -->
    <link href="<?php echo url_for('/assets/'); ?>/css/nucleo-icons.css" rel="stylesheet" />

    <!-- CSS Files -->
    <link href="<?php echo url_for('/assets/'); ?>/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo url_for('/assets/'); ?>/css/bootstrap-dropdownhover.min.css" rel="stylesheet">

    <link href="<?php echo url_for('/assets/'); ?>/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/'); ?>/css/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="<?php echo url_for('/assets/'); ?>/css/dropzone.min.css" rel="stylesheet">
    <link href="<?php echo url_for('/assets/'); ?>/css/swiper.min.css" rel="stylesheet">
    <link href="<?php echo url_for('/assets/'); ?>/css/typeahead.css?v=1.0.1" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/'); ?>/css/bootstrap-slider.min.css?v=1.0.1" rel="stylesheet" />

    <link href="<?php echo url_for('/assets/'); ?>/css/global.css?v=1.0.1" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/'); ?>/css/intlTelInput.css" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/'); ?>/css/style-shah.css?v=1.0.1" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/'); ?>/css/style-syafiq.css?<?php echo  '?' . option('style_version') ?>" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/'); ?>/css/style-byte2c.css?<?php echo  '?' . option('style_version') ?>" rel="stylesheet" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-191800673-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-191800673-1');
    </script>
</head>

<body class="white-content od-mode">
    <header class="private-header">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light" id="navbarMain">
                <a class="navbar-brand" href="<?php echo url_for('/') ?>">
                    <div class="logo"><img src="<?php echo url_for('assets/img/mtp_logo.png') ?>" alt=""></div>
                </a>
            </nav>
        </div>
    </header>
    <section class="section section-bodyintro section-bubbleintro" id="bodyintro">
		<div class="container">
            <div class="personalize-header-flex">
                <div class="col-left">
                    <h2 class="bodyintro-title"><?php echo lang('my_company_profile_main_title'); ?></h2>
                </div>
                <div class="col-right">
                    <p class="body-para bodyintro-body"><?php echo lang('my_company_profile_signup_subtitle'); ?></p>
                </div>
            </div>
		</div>
	</section>
    <div class="companyprofile-signup-container">
        <div class="col-bubble-selection">
            <div class="form-company-profile-container">
                <form id="redirectForm" class="form-companyprofile">
                    <div class="form-collapse-row row-collapse-pic">
                        <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('person_in_charge_section_title') ?></span></div>
                        <div class="form-flex">
                            <div class="input-group-flex row-cpfirstname">
                                <input type="text" id="firstName" class="form-control form-control-input" placeholder="<?php echo lang('profile_first_name_placeholder') ?>" required="" autofocus="" name="firstname" value="<?php echo $user->info['firstname']; ?>">
                            </div>
                            <div class="input-group-flex row-cplastname">
                                <input type="text" id="lastName" class="form-control form-control-input" placeholder="<?php echo lang('profile_last_name_placeholder') ?>" required="" name="lastname" value="<?php echo $user->info['lastname']; ?>">
                            </div>
                            <div class="input-group-flex row-cpid">
                                <input type="text" id="identification" class="form-control form-control-input" placeholder="<?php echo lang('profile_mykad_placeholder') ?>" required="" name="nric" value="<?php echo $user->info['nric']; ?>">
                            </div>
                            <div class="input-group-flex row-cpmobile">
                                <span class="tel-prefix">
                                    <input type="tel" id="contactMobile" class="form-control form-control-input intl-tel-input" placeholder="<?php echo lang('company_contact_mobile_number') ?>" value="<?php echo $user->info['mobile_number']; ?>" required>
                                </span>
                            </div>
                            <div class="input-group-flex row-cpemail">
                                <input type="email" id="inputEmail" class="form-control form-control-input" placeholder="<?php echo lang('profile_email_placeholder') ?>" name="email" value="<?php echo $user->info['email']; ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-collapse-row row-collapse-compdetails">
                        <div class="collapse-title-lbl"><span class="title-collapse"><?php echo lang('my_company_details_section_title') ?></span></div>
                        <div class="form-flex">
                            <div class="input-group-flex row-cpcompname">
                                <input type="text" id="companyName" class="form-control form-control-input" placeholder="<?php echo lang('company_name') ?>" required="" name="company_name" value="<?php echo $user->info['company']['name']; ?>">
                            </div>
                            <div class="input-group-flex row-cpcompreg">
                                <input type="text" id="companyReg" class="form-control form-control-input" placeholder="<?php echo lang('company_registration') ?>" required="" name="reg_num" value="<?php echo $user->info['company']['reg_num']; ?>">
                            </div>
                            <div class="input-group-flex row-cpcompaddress">
                                <input type="text" id="companyAddress" class="form-control form-control-input" placeholder="<?php echo lang('address') ?>" required="" name="address1" value="<?php echo $user->info['address1']; ?>">
                            </div><div class="input-group-flex row-cpcompaddress">
                                <input type="text" id="companyAddress" class="form-control form-control-input" placeholder="<?php echo lang('address_2') ?>" required="" name="address2" value="<?php echo $user->info['address2']; ?>">
                            </div>

	                        <div class="input-group-flex row-cpcomppostal">
		                        <input type="tel" id="companyPostal" class="form-control form-control-input" placeholder="<?php echo lang('company_postcode') ?>" required="" name="zip" value="<?php echo $user->info['zip']; ?>">
	                        </div>

	                        <div class="input-group-flex row-cpcompcountry">
		                        <select class="form-control form-control-input country" placeholder="<?php echo lang('profile_country_placeholder') ?>" name="country" data-target="state">
			                        <option disabled selected><?php echo lang('profile_country_placeholder') ?></option>
                                    <?php foreach ($countries as $key) { ?>
				                        <option value="<?php echo $key['id']; ?>"<?php echo $user->info['country'] == $key['id'] ? " selected" : ""; ?>><?php echo $key['name']; ?></option>
                                    <?php } ?>
		                        </select>
	                        </div>

                            <div class="input-group-flex row-cpcompstate">
		                        <select class="form-control form-control-input state" placeholder="<?php echo lang('profile_state_placeholder') ?>"
		                                name="state" data-selected="<?php echo $user->info['state']; ?>" data-target="city" required>
			                        <option disabled selected><?php echo lang('profile_state_placeholder') ?></option>
                                    <?php foreach($cms->states($user->info['country']) as $state){ ?>
				                        <option value="<?php echo $state['id']; ?>" <?php echo $user->info['state'] == $state['id'] ? "selected" : ""; ?>><?php echo $state['name']; ?></option>
                                    <?php } ?>
		                        </select>
	                        </div>

                            <div class="input-group-flex row-cpcomppostal">
	                            <select class="form-control form-control-input city"  name="city"
	                                    placeholder="<?php echo lang('profile_town_placeholder') ?>" data-selected="<?php echo $user->info['city']; ?>">
		                            <option disabled selected><?php echo lang('profile_town_placeholder') ?></option>
                                    <?php foreach($cms->cities($user->info['state']) as $city){ ?>
			                            <option value="<?php echo $city['id']; ?>" <?php echo $user->info['city'] == $city['id'] ? "selected" : ""; ?>><?php echo $city['name']; ?></option>
                                    <?php } ?>
	                            </select>
                            </div>
	                        
                            <div class="input-group-flex row-cpcompemail">
                                <input type="email" id="companyEmail" class="form-control form-control-input" placeholder="<?php echo lang('company_email') ?>" required="" autofocus="" name="company_email" value="<?php echo $user->info['company']['email']; ?>">
                            </div>
                            <div class="input-group-flex row-cpcompphone">
                                <span class="tel-prefix">
                                    <input type="tel" id="companyPhone" class="form-control form-control-input intl-tel-input" placeholder="<?php echo lang('company_contact_fixed_line') ?>" value="<?php echo $user->info['company']['company_phone']; ?>" required>
                                </span>
                            </div>
                            <div class="input-group-flex row-cpcompabout">
                                <textarea class="form-control form-control-input" placeholder="<?php echo lang('about_the_company') ?>" required="" rows="3" name="about_us"><?php echo $user->info['company']['about_us']; ?></textarea>
                            </div>
                            <div class="input-group-flex row-cpcompindustry">
                                <select class="form-control form-control-input" name="industry" placeholder="<?php echo lang('sign_up_company_industry') ?>">
                                    <option disabled selected><?php echo lang('sign_up_company_industry') ?></option>
                                    <?php foreach($cms->getIndustries2Level() as $industry){ if($industry['subs']){ ?>
                                    <optgroup label="<?php echo $industry['title']; ?>">
                                    <?php foreach($industry['subs'] as $sub){ ?>
                                    <option value="<?php echo $sub['id']; ?>"<?php echo $sub['id'] == $user->info['company']['industry'] ? ' selected' : ''; ?>><?php echo $sub['title']; ?></option>
                                    <?php } ?>
                                    </optgroup>
                                    <?php }} ?>
                                </select>
                            </div>
                            <div class="input-group-block row-cpcompavatar">
                                <label class="input-lbl input-lbl-block">
                                    <span class="input-label-txt"><?php echo lang('company_profile_photo_section_title_not_required') ?></span>
                                </label>
                                <div class="profile-photo">
                                    <?php if($user->info['photo']){ ?><img src="<?php echo imgCrop($user->info['photo'], 100, 100); ?>" class="profile-img"><?php } ?>
                                    <button type="button" class="btn btn-default btn-sm mt-1" onClick="$(this).next().click();"><span class="fa fa-upload"></span> <?php echo lang('profile_photo_upload') ?></button>
                                    <input type="file" name="file" id="profileImage" class="d-none">
                                    <input type="hidden" name="photo" value="<?php echo $user->info['photo']; ?>">
                                </div>
	                            <p class="help-notes"><?php echo sprintf(lang('profile_photo_upload_limit'), $cms->settings()['avatar_max_size'] . 'MB', strtoupper(str_replace(',', ', ', $cms->settings()['avatar_file_extensions']))); ?></p>
                            </div>

                            <div class="input-group-block row-cpcompgallery">
                                <label class="input-lbl input-lbl-block">
                                    <span class="input-label-txt"><?php echo lang('company_image_gallery_section_title') ?></span>
                                </label>
                                <div class="uploads">
                                    <ul class="image-list company_gallery">
                                        <?php if($user->info['company']['photos']){ foreach($user->info['company']['photos'] as $photo){ ?>
                                        <li><input type="hidden" name="photos[]" value="<?php echo $photo; ?>"><img src="<?php echo imgCrop($photo, 125, 100); ?>"><a href="#" class="remove-image"><i class="fa fa-times"></i></a></li>
                                        <?php }} ?>
                                    </ul>
                                    <button type="button" class="btn btn-default btn-sm mt-1" onClick="$(this).next().click();"><span class="fa fa-upload"></span> <?php echo lang('profile_photo_upload') ?></button>
                                    <input type="file" name="file" id="profile-company_gallery" class="d-none" multiple>
                                </div>
	                            <p class="help-notes"><?php echo sprintf(lang('profile_photo_upload_limit'), $cms->settings()['avatar_max_size'] . 'MB', strtoupper(str_replace(',', ', ', $cms->settings()['avatar_file_extensions']))); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-collapse-row row-collapse-bank-info">
                        <div class="collapse-title-lbl">
	                        <span class="title-collapse"><?php echo lang('pref_bank'); ?></span>
	                        <span class="bi-tooltip" data-toggle="tooltip" data-placement="top" title=""
	                              data-original-title="<?php echo lang('pref_bank_tooltips'); ?>">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                                </svg>
                            </span>
                        </div>
                        <div class="form-flex">
                            <div class="input-group-block row-bankacc">
                                <select class="form-control form-control-input" name="preference[bank]">
                                    <option value=""><?php echo lang('pref_select_bank') ?></option>
	                                <option value="PHBMMYKL" <?php echo $user->info['preference']['bank'] == "PHBMMYKL" ? ' selected' : ''; ?>>Affin Bank</option>
	                                <option value="MFBBMYKL" <?php echo $user->info['preference']['bank'] == "MFBBMYKL" ? " selected" : ''; ?>>Alliance Bank</option>
	                                <option value="ARBKMYKL" <?php echo $user->info['preference']['bank'] == "ARBKMYKL" ? " selected" : ''; ?>>AmBank</option>
	                                <option value="BIMBMYKL" <?php echo $user->info['preference']['bank'] == "BIMBMYKL" ? " selected" : ''; ?>>Bank Islam</option>
	                                <option value="BKRMMYKL" <?php echo $user->info['preference']['bank'] == "BKRMMYKL" ? " selected" : ''; ?>>Bank Kerjasama Rakyat Malaysia</option>
	                                <option value="BMMBMYKL" <?php echo $user->info['preference']['bank'] == "BMMBMYKL" ? " selected" : ''; ?>>Bank Muamalat</option>
	                                <option value="BSNAMYK1" <?php echo $user->info['preference']['bank'] == "BSNAMYK1" ? " selected" : ''; ?>>Bank Simpanan Nasional</option>
	                                <option value="CIBBMYKL" <?php echo $user->info['preference']['bank'] == "CIBBMYKL" ? " selected" : ''; ?>>CIMB Bank</option>
	                                <option value="HLBBMYKL" <?php echo $user->info['preference']['bank'] == "HLBBMYKL" ? " selected" : ''; ?>>Hong Leong Bank</option>
	                                <option value="HBMBMYKL" <?php echo $user->info['preference']['bank'] == "HBMBMYKL" ? " selected" : ''; ?>>HSBC</option>
	                                <option value="KFHOMYKL" <?php echo $user->info['preference']['bank'] == "KFHOMYKL" ? " selected" : ''; ?>>Kuwait Finance House Bank</option>
	                                <option value="MBBEMYKL" <?php echo $user->info['preference']['bank'] == "MBBEMYKL" ? " selected" : ''; ?>>Maybank</option>
	                                <option value="OCBCMYKL" <?php echo $user->info['preference']['bank'] == "OCBCMYKL" ? " selected" : ''; ?>>OCBC Bank</option>
	                                <option value="PBBEMYKL" <?php echo $user->info['preference']['bank'] == "PBBEMYKL" ? " selected" : ''; ?>>Public Bank</option>
	                                <option value="RHBBMYKL" <?php echo $user->info['preference']['bank'] == "RHBBMYKL" ? " selected" : ''; ?>>RHB Bank</option>
	                                <option value="SCBLMYKX" <?php echo $user->info['preference']['bank'] == "SCBLMYKX" ? " selected" : ''; ?>>Standard Chartered</option>
	                                <option value="UOVBMYK" <?php echo $user->info['preference']['bank'] == "UOVBMYK" ? " selected" : ''; ?>>UOB</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-flex" id="work_location">
                            <div class="input-group-flex row-accholdername">
                                <input type="text" class="form-control form-control-input" placeholder="<?php echo lang('pref_account_name') ?>" name="preference[acc_name]" value="<?php echo  $user->info['preference']['acc_name']; ?>" >
                            </div>
                            <div class="input-group-flex row-accno">
                                <input type="text" class="form-control form-control-input" placeholder="<?php echo lang('pref_account_num') ?>" name="preference[acc_num]" value="<?php echo  $user->info['preference']['acc_num']; ?>" >
                            </div>
                        </div>
                    </div>
                    <div class="footer-form-action">
                        <button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                            <span class="btn-label"><?php echo lang('pref_save') ?></span>
                            <span class="btn-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                    <polyline points="20 6 9 17 4 12"></polyline>
                                </svg>
                            </span>
                        </button>
                        <?php /*
                        <button type="button" class="btn-icon-full btn-cancel" onClick="location.href='<?php echo url_for('/dashboard'); ?>'">
                            <span class="btn-label">Cancel</span>
                            <span class="btn-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </span>
                        </button>
                        */ ?>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--   Core JS Files   -->
    <script src="<?php echo url_for('/assets/'); ?>/js/core/jquery.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/core/jquery-ui.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/core/popper.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/core/bootstrap.min.js"></script>

    <!--   Plugins JS Files   -->
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/moment-with-locales.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/tempusdominus-bootstrap-4.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/dragscroll.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/bootstrap-tagsinput.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/swiper.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/bootstrap-slider.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/jquery.validate.js"></script>

    <!--  Search Suggestion Plugin    -->
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/typeahead.bundle.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/bloodhound.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/handlebars.js"></script>

    <!-- Chart JS -->
    <script src='<?php echo url_for('/assets/'); ?>/js/plugins/snap.svg-min.js'></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/chart.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/progressbar.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/d3.v3.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/bootstrap-notify.js"></script>

    <!--  Custom JS    -->
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/dropzone.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/main.js<?php echo  '?' . option('script_version') ?>"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/bubble.js"></script>
    <script type="text/javascript" src="<?php echo url_for('/assets/libs/blueimp/iframe-transport.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo url_for('/assets/libs/blueimp/fileupload.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo url_for('/js-mtp'); ?>/script.js"></script>
    <script type="text/javascript" src="<?php echo url_for('/js-mtp'); ?>/uploader.js"></script>

    <!-- Sidebar Hover Function -->
    <script>
        // Sidebar Hover Function
        const $dropdown = $(".sidebar .dropdown");
        const $dropdownToggle = $(".sidebar .dropdown-toggle");
        const $dropdownMenu = $(".sidebar .dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function() {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                    function() {
                        const $this = $(this);
                        $this.addClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "true");
                        $this.find($dropdownMenu).addClass(showClass);
                    },
                    function() {
                        const $this = $(this);
                        $this.removeClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "false");
                        $this.find($dropdownMenu).removeClass(showClass);
                    }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });
        
        $('.card-header .dropdown-menu .nav-tabs .nav-link').on("click.bs.dropdown", function(e) {
            $(this).tab('show');
            e.stopPropagation();
        });
        
        $('.col-account-header .dropdown.col-account .switch-toggle .slider-toggler').on("click", function(e) {
            e.stopPropagation();
        });

        $(document).on('click', '.remove-image', function(){
            el = $(this);
            el.parent().slideUp(function(){
                el.parent().remove();
            });
        });
    </script>
    <!-- Sidebar Hover Function -->

    <!-- Mobile Phone Number - IntlTelInput -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/intlTelInput.min.js"></script>
    <script>
        /* var inputHome = document.querySelector("#contactPhone");
        window.intlTelInput(inputHome, {
           autoHideDialCode: false,
           autoPlaceholder: "off",
           initialCountry: "my",
           preferredCountries: ['my'],
           separateDialCode: true,
           utilsScript: rootPath + "assets/js/plugins/utils.js",
           hiddenInput: "contact_number",
        }); */
        
        var inputMobile = document.querySelector("#contactMobile");
        window.intlTelInput(inputMobile, {
           autoHideDialCode: false,
           autoPlaceholder: "off",
           initialCountry: "my",
           preferredCountries: ['my'],
           separateDialCode: true,
           utilsScript: rootPath + "assets/js/plugins/utils.js",
           hiddenInput: "mobile_number",
        });

        var inputCompany = document.querySelector("#companyPhone");
        window.intlTelInput(inputCompany, {
           autoHideDialCode: false,
           autoPlaceholder: "off",
           initialCountry: "my",
           preferredCountries: ['my'],
           separateDialCode: true,
           utilsScript: rootPath + "assets/js/plugins/utils.js",
           hiddenInput: "company_phone",
        });
    </script>
    <!-- Mobile Phone Number - IntTelInput -->
</body>
</html>