<!DOCTYPE html>
<html lang="<?php echo !isset($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) || ($_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] == 'EN') ? 'en':'ms' ?>">

<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P7GCVWH');</script>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo url_for('/assets/img/apple-icon.png') ?>">
    <link rel="icon" type="image/png" href="<?php echo url_for('/assets/img/favicon.png') ?>">
    <title><?php echo $page_title ?? 'Task Centre' ?> | MakeTimePay</title>

    <!-- Fonts and icons -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/css/glyphicons.css') ?>" rel="stylesheet">
    <link href="<?php echo url_for('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">

    <!-- Nucleo Icons -->
    <link href="<?php echo url_for('/assets/css/nucleo-icons.css') ?>" rel="stylesheet" />

    <!-- CSS Files -->
    <link href="<?php echo url_for('/assets/css/animate.min.css') ?>" rel="stylesheet">
    <link href="<?php echo url_for('/assets/css/bootstrap-dropdownhover.min.css') ?>" rel="stylesheet">

    <link href="<?php echo url_for('/assets/css/tempusdominus-bootstrap-4.min.css') ?>" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/css/bootstrap-tagsinput.css') ?>" rel="stylesheet">
    <link href="<?php echo url_for('/assets/css/dropzone.min.css') ?>" rel="stylesheet">
    <link href="<?php echo url_for('/assets/css/swiper.min.css') ?>" rel="stylesheet">
    <link href="<?php echo url_for('/assets/css/typeahead.css') ?>" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/css/bootstrap-slider.min.css') ?>" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/libs/summernote/summernote.min.css') ?>" rel="stylesheet" />

    <link href="<?php echo url_for('/assets/css/global.css') ?>" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/css/intlTelInput.css'); ?>" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/css/style-shah.css') . '?' . option('style_version') ?>" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/css/style-syafiq.css') . '?' . option('style_version') ?>" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/css/style-byte2c.css') ?>" rel="stylesheet" />
	<script src="<?php echo url_for('/assets/js/core/jquery.min.js') ?>"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-191800673-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-191800673-1');
    </script>
</head>

<body class="white-content page-task-centre <?php echo isset($view) ? ($view === 'list' ? 'tc-list-mode' : 'tc-calendar-mode') : '' ?> <?php echo !isset($current_tab) || $current_tab === '#tcAll' ? 'tc-all-active':'tc-inprogress-active' ?> <?php echo (isset($mode) && $user->info['type'] === '1') ? $mode : 'od-mode' ?> <?php echo $user->info['type'] === '0' ? 'individual' : 'company' ?>">
	<div class="wrapper">
		<?php echo partial('layout/sidebar.html.php'); ?>
		<div class="mob-navbar-toggler">
			<button type="button" class="navbar-toggler">
				<span class="navbar-toggler-bar bar1"></span>
				<span class="navbar-toggler-bar bar2"></span>
				<span class="navbar-toggler-bar bar3"></span>
			</button>
		</div>
	    <div class="main-panel">
	        <?php echo $content; ?>
	    </div>
	</div>
	
	<?php echo partial('workspace/partial/modals.html.php'); ?>
	<!--   Core JS Files   -->

	<script src="<?php echo url_for('/assets/js/core/jquery-ui.min.js') ?>"></script>
	<script src="<?php echo url_for('/assets/js/core/popper.min.js') ?>"></script>
	<script src="<?php echo url_for('/assets/js/core/bootstrap.min.js') ?>"></script>

	<!--   Plugins JS Files   -->
	<script src="<?php echo url_for('/assets/js/plugins/moment-with-locales.min.js') ?>"></script>
	<script>moment.locale('<?php echo !isset($_SESSION[WEBSITE_PREFIX . "LANGUAGE"]) || $_SESSION[WEBSITE_PREFIX . "LANGUAGE"] === "EN" ? "en" : "ms" ?>');</script>
	<script src="<?php echo url_for('/assets/js/plugins/tempusdominus-bootstrap-4.min.js') ?>"></script>
	<script src="<?php echo url_for('/assets/js/plugins/perfect-scrollbar.jquery.min.js') ?>"></script>
	<script src="<?php echo url_for('/assets/js/plugins/dragscroll.js') ?>"></script>
    <?php if(stripos(request_uri(), '/search') === false): ?>
	<script src="<?php echo url_for('/assets/js/plugins/bootstrap-tagsinput-modified.min.js') ?>"></script>
	<?php else: ?>
	<script src="<?php echo url_for('/assets/js/plugins/bootstrap-tagsinput.min.js') ?>"></script>
	<?php endif ?>
	<script src="<?php echo url_for('/assets/js/plugins/swiper.min.js') ?>"></script>
	<script src="<?php echo url_for('/assets/js/plugins/bootstrap-slider.min.js') ?>"></script>
	<script src="<?php echo url_for('/assets/js/plugins/jquery.validate.js') ?>"></script>
	<?php if( isset($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) && $_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] === 'BM' ): ?>
	<script src="<?php echo url_for('/assets/js/plugins/messages_my.js') ?>"></script>
	<?php endif ?>
	<script src="<?php echo url_for('/assets/libs/summernote/summernote.min.js') ?>"></script>

	<!--  Search Suggestion Plugin    -->
	<script src="<?php echo url_for('/assets/js/plugins/typeahead.bundle.min.js') ?>"></script>
	<script src="<?php echo url_for('/assets/js/plugins/bloodhound.js') ?>"></script>
	<script src="<?php echo url_for('/assets/js/plugins/handlebars.js') ?>"></script>

	<!-- Chart JS -->
	<script src='<?php echo url_for('/assets/js/plugins/snap.svg-min.js') ?>'></script>
	<script src="<?php echo url_for('/assets/js/plugins/chart.min.js') ?>"></script>
	<script src="<?php echo url_for('/assets/js/plugins/progressbar.js') ?>"></script>
	<script src="<?php echo url_for('/assets/js/plugins/d3.v3.min.js') ?>"></script>

	<!--  Notifications Plugin    -->
	<script src="<?php echo url_for('/assets/js/plugins/bootstrap-notify.js') ?>"></script>
	<script> window.task_upload = "<?php echo option("site_uri") . url_for("/workspace/task/upload") ?>"; </script>
	<!--  Custom JS    -->
	<script src="<?php echo url_for('/assets/js/plugins/dropzone.js') ?>"></script>
	<script src="<?php echo url_for('/assets/js/main.js') . '?' . option('script_version') ?>"></script>
	<script src="<?php echo url_for('/assets/js/bubble.js') ?>"></script>
	<script src="<?php echo url_for('/js-mtp/script.js') ?>"></script>
    <?php if(stripos(request_uri(), '/workspace') !== FALSE && isset($page_title) && ($page_title === 'Task Centre' || $page_title === 'Job Centre')): ?>
		<script src="<?php echo url_for('/assets/js/chat-sidebar.js') ?>"></script>
		<script src="<?php echo url_for('/assets/js/tasks.js') . "?1.0.11" ?>"></script>
    <?php endif ?>
    <?php if(stripos(request_uri(), '/search')): ?>
		<script> window.currentURI = "<?php echo option("site_uri") . url_for('workspace/search') ?>"; </script>
		<script src="<?php echo url_for('/assets/js/search.js') ?>"></script>
    <?php endif ?>
    <?php if(stripos(request_uri(), '/dashboard') !== FALSE): ?>
		<!-- Donut Chart Function -->
		<script>
            // write text plugin
            Chart.pluginService.register({
                afterUpdate: function(chart) {
                    if (chart.config.options.elements.center) {
                        var helpers = Chart.helpers;
                        var centerConfig = chart.config.options.elements.center;
                        var globalConfig = Chart.defaults.global;
                        var ctx = chart.chart.ctx;

                        var fontStyle = helpers.getValueOrDefault(centerConfig.fontStyle, globalConfig.defaultFontStyle);
                        var fontFamily = helpers.getValueOrDefault(centerConfig.fontFamily, globalConfig.defaultFontFamily);

                        if (centerConfig.fontSize)
                            var fontSize = centerConfig.fontSize;
                        // figure out the best font size, if one is not specified
                        else {
                            ctx.save();
                            var fontSize = helpers.getValueOrDefault(centerConfig.minFontSize, 1);
                            var maxFontSize = helpers.getValueOrDefault(centerConfig.maxFontSize, 256);
                            var maxText = helpers.getValueOrDefault(centerConfig.maxText, centerConfig.text);

                            do {
                                ctx.font = helpers.fontString(fontSize, fontStyle, fontFamily);
                                var textWidth = ctx.measureText(maxText).width;

                                // check if it fits, is within configured limits and that we are not simply toggling back and forth
                                if (textWidth < chart.innerRadius * 2 && fontSize < maxFontSize)
                                    fontSize += 1;
                                else {
                                    // reverse last step
                                    fontSize -= 1;
                                    break;
                                }
                            } while (true)
                            ctx.restore();
                        }

                        // save properties
                        chart.center = {
                            font: helpers.fontString(fontSize, fontStyle, fontFamily),
                            fillStyle: helpers.getValueOrDefault(centerConfig.fontColor, globalConfig.defaultFontColor)
                        };
                    }
                },
                afterDraw: function(chart) {
                    if (chart.center) {
                        var centerConfig = chart.config.options.elements.center;
                        var ctx = chart.chart.ctx;

                        ctx.save();
                        ctx.font = chart.center.font;
                        ctx.fillStyle = chart.center.fillStyle;
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
                        var centerY = ((chart.chartArea.top + 6) + chart.chartArea.bottom) / 2;
                        ctx.fillText(centerConfig.text, centerX, centerY);
                        ctx.restore();
                    }
                },
            })


            var config = {
                type: 'doughnut',
                data: {
                    labels: [
                        "Red",
                        "Gray"
                    ],
                    datasets: [{
                        data: [<?php echo $user->profileCompleteness(); ?>, <?php echo 100 - $user->profileCompleteness(); ?>],
                        backgroundColor: [
                            "#3644ad",
                            "#ccc"
                        ],
                        hoverBackgroundColor: [
                            "#3644ad",
                            "#ccc"
                        ]
                    }]
                },
                options: {
                    cutoutPercentage: 70,
                    legend: {
                        display: false
                    },
                    tooltips: {
                        enabled: false
                    },
                    maintainAspectRatio: false,
                    elements: {
                        arc: {
                            roundedCornersFor: 0
                        },
                        center: {
                            // the longest text that could appear in the center
                            maxText: '100%',
                            text: '<?php echo $user->profileCompleteness(); ?>%',
                            fontColor: '#3644ad',
                            fontFamily: "'Poppins', 'Helvetica', 'Arial', sans-serif",
                            fontStyle: 'normal',
                            // fontSize: 12,
                            // if a fontSize is NOT specified, we will scale (within the below limits) maxText to take up the maximum space in the center
                            // if these are not specified either, we default to 1 and 256
                            minFontSize: 16,
                            maxFontSize: 22,
                        }
                    }
                }
            };


            if( $('#doProfile').length ){
                var ctx = document.getElementById("doProfile").getContext("2d");
                var doProfile = new Chart(ctx, config);
            }

		</script>
		<!-- Donut Chart Function -->

		<!-- Donut Chart Function -->
		<script>
            var doughnut = document.getElementById("doChart");
            if(doughnut){
                var myDoughnutChart = new Chart(doughnut, {
                    type: 'doughnut',
                    data: {
                        labels: ["Credit Card", "Bank Transfer"],
                        datasets: [{
                            data: [4100, 2500],
                            backgroundColor: ['#3644ad', '#f0724c'],
                            borderColor: ['#3644ad', '#f0724c'],
                            hoverBorderWidth: 10,
                            hoverRadius: 1,
                            borderWidth: 0,
                        }]
                    },
                    options: {
                        defaultFontFamily: Chart.defaults.global.defaultFontFamily = "'Poppins'",
                        legend: {
                            display: false,
                            position: 'bottom'
                        },
                        cutoutPercentage: 70,
                        maintainAspectRatio: false,
                        responsive: true,
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    var dataset = data.datasets[tooltipItem.datasetIndex];
                                    var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                        return previousValue + currentValue;
                                    });
                                    var currentValue = dataset.data[tooltipItem.index];
                                    var precentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                    return precentage + "%";
                                }
                            }
                        }
                    }
                });
            }

		</script>
		<!-- Donut Chart Function -->
	<?php endif ?>

	<!-- Task Centre - Synchronize Scroll -->
	<script>
        $(document).ready(function() {
            $("#colLeftCard .tbl-body-calendar").scroll(function() {
                $("#colRightCard .tbl-body-calendar-gantt").scrollTop($("#colLeftCard .tbl-body-calendar").scrollTop());
            });
            $("#colRightCard .tbl-body-calendar-gantt").scroll(function() {
                $("#colLeftCard .tbl-body-calendar").scrollTop($("#colRightCard .tbl-body-calendar-gantt").scrollTop());
            });
        });
	</script>
	<!-- Task Centre - Synchronize Scroll -->

	<!-- Page Function -->
	<script>
	    $(function() {
	        $('[data-toggle="tooltip"]').tooltip()
	    })

	    <?php if(stripos(request_uri(), '/workspace/task-centre') !== FALSE ): ?>
	    $(document).ready(function() {

	        $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
		        var target = $(e.target).attr('href');

	            var widthOverflow = $(target + ' .tbl-header-calendar-gantt-body-overflow').length ? $(target + ' .tbl-header-calendar-gantt-body-overflow')[0].scrollWidth : 0;
	            var widthColParent = $(target + ' .col-right-card-flex').width();
	            var maxDrag = 0;
	            maxDrag = widthOverflow - widthColParent;

	            $(target + ' .tbl-body-calendar-gantt').css('width', widthOverflow);

	        });


            var widthOverflow = $('.tbl-header-calendar-gantt-body-overflow').not(':hidden').length ? $('.tbl-header-calendar-gantt-body-overflow').not(':hidden')[0].scrollWidth : 0;
            var widthColParent = $('.col-right-card-flex').not(':hidden').width();
            var maxDrag = 0;
            maxDrag = widthOverflow - widthColParent;

            $('.tbl-body-calendar-gantt').not(':hidden').css('width', widthOverflow);

            /*var splitobj = Split(["#colLeftCard", "#colRightCard"], {
                elementStyle: function(dimension, size, gutterSize) {
                    $(window).trigger('resize'); // 
                    if (size <= 70) {
                        return {
                            'flex-basis': 'calc(' + size + '% - ' + gutterSize + 'px)'
                        }
                    }

                },
                gutterStyle: function(dimension, gutterSize) {
                    return {
                        'flex-basis': gutterSize + 'px'
                    }
                },
                sizes: [50, 50],
                minSize: [40, 40],
                gutterSize: 5,
                cursor: 'col-resize',
                expandToMin: true,
            });*/
	    });
	    <?php endif; ?>
	</script>

	<!-- Sidebar Hover Function -->
	<script>
	    // Sidebar Hover Function
	    const $dropdown = $(".sidebar .dropdown");
	    const $dropdownToggle = $(".sidebar .dropdown-toggle");
	    const $dropdownMenu = $(".sidebar .dropdown-menu");
	    const showClass = "show";

	    $(window).on("load resize", function() {
	        if (this.matchMedia("(min-width: 768px)").matches) {
	            $dropdown.hover(
	                function() {
	                    const $this = $(this);
	                    $this.addClass(showClass);
	                    $this.find($dropdownToggle).attr("aria-expanded", "true");
	                    $this.find($dropdownMenu).addClass(showClass);
	                },
	                function() {
	                    const $this = $(this);
	                    $this.removeClass(showClass);
	                    $this.find($dropdownToggle).attr("aria-expanded", "false");
	                    $this.find($dropdownMenu).removeClass(showClass);
	                }
	            );
	        } else {
	            $dropdown.off("mouseenter mouseleave");
	        }
	    });


	    $('.card-header .dropdown-menu .nav-tabs .nav-link').on("click.bs.dropdown", function(e) {
	        $(this).tab('show');
	        e.stopPropagation();
	    });
	    $('.col-account-header .dropdown.col-account .switch-toggle .slider-toggler').on("click", function(e) {
	        e.stopPropagation();
	    });

	</script>
	<!-- Sidebar Hover Function -->

	<script>
	    // Task Listing Filter - Range Budget Hour
	    if($("#range_budget_hour_filter").length) {
            var range_budget_hour_filter = new Slider("#range_budget_hour_filter", {
                min: 10,
                max: 100,
                step: 1,
                <?php if(isset($filters['hour_budget']) && !empty($filters['hour_budget'])): ?>
                value: [<?php echo implode(",", $filters['hour_budget']) ?>],
	            <?php else: ?>
                value: [30, 65],
	            <?php endif ?>
                range: true,
                tooltip: 'always',
                tooltip_position: 'bottom',
                formatter: function (value) {
                    return 'RM' + value[0] + ' - RM' + value[1];
                }
            });
        }

	    // Task Listing Filter - Range Budget Lumpsum
	    if($("#range_budget_lumpsum_filter").length) {
            var range_budget_lumpsum_filter = new Slider("#range_budget_lumpsum_filter", {
                min: 10,
                max: 1000,
                step: 50,
                <?php if(isset($filters['task_budget']) && !empty($filters['task_budget'])): ?>
                value: [<?php echo implode(",", $filters['task_budget']) ?>],
                <?php else: ?>
                value: [300, 650],
                <?php endif ?>
                range: true,
                tooltip: 'always',
                tooltip_position: 'bottom',
                formatter: function (value) {
                    return 'RM' + value[0] + ' - RM' + value[1];
                }
            });
        }

	</script>

<?php if(stripos(request_uri(), '/workspace/task-centre') !== false): ?>
	<!-- Gantt Chart Collapse Height Function -->
	<script>
		$(function(){
            var url = document.location.toString();
            if (url.match('#')) {
                var hash = url.split('#')[1];
                $('.nav-pills-taskcentre a[href="#' + hash + '"]').tab('show');

                if(hash.length === 36){
                    hash = hash.toLowerCase();
                    $('#'+hash).children().first().next().collapse('toggle');
                    document.getElementById(hash).scrollIntoView();
                }
                //$('.nav-pills-taskcentre a[href="#' + hash + '"]').trigger('click');
            }

            // Change hash for page-reload
            $('.nav-pills-taskcentre a').on('shown.bs.tab', function (e) {
                var targetHash = e.target.hash;
                //window.location.hash = targetHash;
                if (targetHash == '#tcAll') {
                    $('body').removeClass('tc-pending-active');
                    $('body').removeClass('tc-inprogress-active');
                    $('body').removeClass('tc-closed-active');
                    $('body').addClass('tc-all-active');
                }
                else if (targetHash == '#tcPending') {
                    $('body').removeClass('tc-all-active');
                    $('body').removeClass('tc-inprogress-active');
                    $('body').removeClass('tc-closed-active');
                    $('body').addClass('tc-pending-active');
                }
                else if (targetHash === "#tcInProgressMonth") {
                    $('body').removeClass('tc-all-active');
                    $('body').removeClass('tc-pending-active');
                    $('body').removeClass('tc-closed-active');
                    $('body').addClass('tc-inprogress-active');
                }
                else if (targetHash == '#tcInProgressWeek') {
                    $('body').removeClass('tc-all-active');
                    $('body').removeClass('tc-pending-active');
                    $('body').removeClass('tc-closed-active');
                    $('body').addClass('tc-inprogress-active');
                }
                else if (targetHash == '#tcInProgressDay') {
                    $('body').removeClass('tc-all-active');
                    $('body').removeClass('tc-pending-active');
                    $('body').removeClass('tc-closed-active');
                    $('body').addClass('tc-inprogress-active');
                }
                else if (targetHash == '#tcClosed') {
                    $('body').removeClass('tc-all-active');
                    $('body').removeClass('tc-pending-active');
                    $('body').removeClass('tc-inprogress-active');
                    $('body').addClass('tc-closed-active');
                }


                hash && $('ul.nav a[href="' + hash + '"]').tab('show');

                $('.nav-tabs a').click(function(e) {
                    $(this).tab('show');
                    var scrollmem = $('body').scrollTop() || $('html').scrollTop();
                    window.location.hash = this.hash;
                    $('html,body').scrollTop(scrollmem);
                });
            });
		});

        // Bar collapse function
        $('.tasks-contents').on('click', '.btn-arrow-bar.collapsed', function() {
            $(this).closest('.tbl-group-calendar-bar-flex').next().collapse('toggle');
        });


        $('.tbl-group-collapse-title').click(function(e) {
            $(this).parent().toggleClass('show');
            $(this).parent().next().collapse('toggle');
            e.stopPropagation();
        });

        // Tab active body function
        $('#tcAll-tab').click(function() {
            $('body').removeClass('tc-pending-active');
            $('body').removeClass('tc-inprogress-active');
            $('body').removeClass('tc-closed-active');
            $('body').addClass('tc-all-active');
        });
        $('#tcPending-tab').click(function() {
            $('body').removeClass('tc-all-active');
            $('body').removeClass('tc-inprogress-active');
            $('body').removeClass('tc-closed-active');
            $('body').addClass('tc-pending-active');
        });
        $('#tcInProgressWeek-tab').click(function() {
            $('body').removeClass('tc-all-active');
            $('body').removeClass('tc-pending-active');
            $('body').removeClass('tc-closed-active');
            $('body').addClass('tc-inprogress-active');
        });
        $('#tcClosed-tab').click(function() {
            $('body').removeClass('tc-all-active');
            $('body').removeClass('tc-pending-active');
            $('body').removeClass('tc-inprogress-active');
            $('body').addClass('tc-closed-active');
        });

        $('.active-posted-tasks-filter a, .inactive-posted-tasks-filter a, .active-posted-tasks-order a, .inactive-posted-tasks-order a').on('click', function(e){
            e.preventDefault();
            var $filter = $(e.currentTarget),
                _filter_text = $filter.text().toLowerCase().split(' ').join('-'),
                $posted_tasks_list = $filter.closest('.tbl-group-calendar-row').find('.posted-tasks-list'),
                request_type = 'filter',
                filterby = _filter_text, orderby = 'latest',
                section = $filter.parent().hasClass('active-posted-tasks-filter') || $filter.parent().hasClass('active-posted-tasks-order') ? 'active' : 'inactive';

            if($filter.parent().hasClass('active-posted-tasks-order') || $filter.parent().hasClass('inactive-posted-tasks-order')) {
                var state = $filter.parent().hasClass('active-posted-tasks-order') ? '.active' : '.inactive';
                orderby = $filter.data('sortby');
                request_type = 'order';
                filterby = $(state+'-posted-tasks-filter').find('.dropdown-item.active').text().toLowerCase().split(' ').join('-');
            }else{
                var state = $filter.parent().hasClass('active-posted-tasks-filter') ? '.active' : '.inactive';
                orderby = $(state+'-posted-tasks-order').find('.dropdown-item.active').data('sortby');
            }

            $filter.parent().find('a').removeClass('active');
            $filter.parent().prev().find('span.drop-val').text($filter.text());
            $filter.addClass('active');
            $posted_tasks_list.find('.loader').hide().removeClass('hide').fadeIn();

            $.ajax({
                method: 'POST',
                url: '<?php echo option('site_uri') . url_for('/workspace/task-centre/tasks/list') ?>',
                data: {
                    'filter': filterby,
                    'order': orderby,
                    'tab': 'posted',
	                'page': 1,
                    'section': section,
                    'tasks_filter_token': '<?php echo html_form_token_field(lemon_csrf_token('tasks_filter_token'), true) ?>'
                }
            }).done(function(response){
                if( response.html ) {
                    if ($filter.parent().hasClass('active-posted-tasks-filter') || $filter.parent().hasClass('active-posted-tasks-order')) {
                        $('.active-tc-row .posted-tasks-list .tasks-contents').html(response.html);
                    } else {
                        $('.inactive-tc-row .posted-tasks-list .tasks-contents').html(response.html);
                    }

                    if ( (section === 'active' && response.total_active > 10) || (section === 'inactive' && response.total_inactive > 10) ){
                        $filter.closest('.tbl-group-calendar-row').find('.more-posted-tasks')
	                        .data('nextPage', 2).attr('data-next-page', 2)
	                        .removeClass('hide').next().hide();
                    }else{
                        $filter.closest('.tbl-group-calendar-row').find('.more-posted-tasks')
                            .data('nextPage', 2).attr('data-next-page', 2)
	                        .addClass('hide').next().hide();
                    }
                }
                $posted_tasks_list.find('.loader').fadeOut(function(){ $(this).addClass('hide') });
            });

	        $filter.parent().prev().dropdown('toggle');
        });

        $('.more-posted-tasks').on('click', function(e){
            let btn = $(this);
            let filterby = 'all', orderby = 'latest';
            let page = btn.data('nextPage');
            let totalPages = btn.data('pages');
            let loader = btn.closest('.tbl-group-calendar-row').find('.posted-tasks-list .loader');

            loader.hide().removeClass('hide').fadeIn();

            btn.closest('.tbl-collapse-group:parent').prev().find('a.active').each(function(idx, ele){
                if (idx === 1) {
                    filterby = $(ele).text().toLowerCase().split(' ').join('-');
                } else if ( $(ele).data('sortby') ) {
                    orderby = $(ele).data('sortby');
                }
            });

            $.ajax({
                method: 'POST',
                url: '<?php echo option('site_uri') . url_for('/workspace/task-centre/tasks/list') ?>',
                data: {
                    'filter': filterby,
                    'order': orderby,
                    'tab': 'posted',
                    'page': page,
                    'section': btn.parent().parent().hasClass('active-tc-row') ? 'active' : 'inactive',
                    'tasks_filter_token': '<?php echo html_form_token_field(lemon_csrf_token('tasks_filter_token'), true) ?>'
                }
            }).done(function(response){
                loader.fadeOut(function(){ loader.addClass('hide') });
                if( response.html ){
                    let nextPage = parseInt(page, 10) + 1;
                    let pages = parseInt(totalPages, 10);
                    if( nextPage <= pages ) {
                        btn.data('nextPage', nextPage).attr('data-next-page', nextPage);
                    }else{
                        btn.addClass('hide').next().show();
                    }
                    btn.prev().append( response.html );
                }
            });

        });

        $('.more-applied-tasks').on('click', function(e){
            let btn = $(this);
            let filterby = 'all', orderby = 'latest';
            let page = btn.data('nextPage');
            let totalPages = btn.data('pages');
            let loader = btn.closest('.tbl-group-calendar-row').find('.applied-tasks-list .loader');

            loader.hide().removeClass('hide').fadeIn();

            btn.closest('.tbl-collapse-group:parent').prev().find('a.active').each(function(idx, ele){
                if (idx === 1) {
                    filterby = $(ele).text().toLowerCase().split(' ').join('-');
                } else if ( $(ele).data('sortby') ) {
                    orderby = $(ele).data('sortby');
                }
            });

            $.ajax({
                method: 'POST',
                url: '<?php echo option('site_uri') . url_for('/workspace/task-centre/tasks/list') ?>',
                data: {
                    'filter': filterby,
                    'order': orderby,
                    'tab': 'applied',
                    'page': page,
                    'section': btn.parent().parent().hasClass('active-tc-row') ? 'active' : 'inactive',
                    'tasks_filter_token': '<?php echo html_form_token_field(lemon_csrf_token('tasks_filter_token'), true) ?>'
                }
            }).done(function(response){
                loader.fadeOut(function(){ loader.addClass('hide') });
                if( response.html ){
                    let nextPage = parseInt(page, 10) + 1;
                    let pages = parseInt(totalPages, 10);
                    if( nextPage <= pages ) {
                        btn.data('nextPage', nextPage).attr('data-next-page', nextPage);
                    }else{
                        btn.addClass('hide').next().show();
                    }
                    btn.prev().append( response.html );
                }
            });

        });

        $('.tasks-back-to-top').on('click', function(e){
            let element = $(this).data('scrollTo');
            let scrollToTop = element ? $(element)[0].offsetTop - 150 : 0;
            $(this).closest('.tbl-body-calendar')[0].scrollTo({ top: scrollToTop, left: 0, behavior: 'smooth'});
        });

        $('.active-applied-tasks-filter a, .inactive-applied-tasks-filter a, .active-applied-tasks-order a, .inactive-applied-tasks-order a').on('click', function(e){
            e.preventDefault();
            var $filter = $(e.currentTarget),
                _filter_text = $filter.text().toLowerCase().split(' ').join('-'),
                $applied_tasks_list = $filter.closest('.tbl-group-calendar-row').find('.applied-tasks-list'),
                request_type = 'filter',
                filterby = _filter_text, orderby = 'latest',
                section = $filter.parent().hasClass('active-applied-tasks-filter') || $filter.parent().hasClass('active-applied-tasks-order') ? 'active' : 'inactive';

            if($filter.parent().hasClass('active-applied-tasks-order') || $filter.parent().hasClass('inactive-applied-tasks-order')) {
                var state = $filter.parent().hasClass('active-applied-tasks-order') ? '.active' : '.inactive';
                orderby = $filter.data('sortby');
                request_type = 'order';
                filterby = $(state+'-applied-tasks-filter').find('.dropdown-item.active').text().toLowerCase().split(' ').join('-');
            }else{
                var state = $filter.parent().hasClass('active-applied-tasks-filter') ? '.active' : '.inactive';
                orderby = $(state+'-applied-tasks-order').find('.dropdown-item.active').data('sortby');
            }

            $filter.parent().find('a').removeClass('active');
            $filter.parent().prev().find('span.drop-val').text($filter.text());
            $filter.addClass('active');
            $applied_tasks_list.find('.loader').hide().removeClass('hide').fadeIn();

            $.ajax({
                method: 'POST',
                url: '<?php echo option('site_uri') . url_for('/workspace/task-centre/tasks/list') ?>',
                data: {
                    'filter': filterby,
	                'order': orderby,
                    'tab': 'applied',
                    'section': section,
	                'tasks_filter_token': '<?php echo html_form_token_field(lemon_csrf_token('tasks_filter_token'), true) ?>'
                }
            }).done(function(response){
                if( response ){
                    if( $filter.parent().hasClass('active-applied-tasks-filter') || $filter.parent().hasClass('active-applied-tasks-order') ){
                        $('.active-tc-row .applied-tasks-list .tasks-contents').html( response.html );
                    }else{
                        $('.inactive-tc-row .applied-tasks-list .tasks-contents').html( response.html );
                    }

                    if ( (section === 'active' && response.total_active > 10) || (section === 'inactive' && response.total_inactive > 10) ){
                        $filter.closest('.tbl-group-calendar-row').find('.more-applied-tasks')
                            .data('nextPage', 2).attr('data-next-page', 2)
                            .removeClass('hide').next().hide();
                    }else{
                        $filter.closest('.tbl-group-calendar-row').find('.more-applied-tasks')
                            .data('nextPage', 2).attr('data-next-page', 2)
                            .addClass('hide').next().hide();
                    }
                }
                $applied_tasks_list.find('.loader').fadeOut(function(){ $(this).addClass('hide') });
            });

            $filter.parent().prev().dropdown('toggle');
        });

        $('.pagination a.task-applicant-pagination').on('click', function(e) {
            get_applicants(e)
        });

        function get_applicants(e) {
            e.preventDefault();
            var target = $(e.currentTarget),
                task = target.data('task'),
                url = target.attr('href'),
                container = $('.applicants-' + task);

	        target.attr('disabled', 'disabled');

            if (target[0].href.indexOf('#') <= -1) {
                $.ajax({url: url})
                    .done(function (response) {
                        container.html(response);
                        container.find('[data-toggle="tooltip"]').tooltip();
                        $('.pagination a.task-applicant-pagination').on('click', function(e) {
                            get_applicants(e)
                        });
                    });
            }
        }

        $('#modal_talent_details').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            if(button.attr('class') === 'applicant-hire-link'){
                window.hire_link = button;
            }
            var talent_id = button.data('id');
            <?php if(!stripos(request_uri(), 'job-centre')): ?>
            var task_id = button.data('taskId');
	        <?php else: ?>
            var job_id = button.data('jobId');
	        <?php endif ?>
            var task_assigned = button.data('taskStatus');
            var url = '<?php echo option('site_uri') . url_for("/workspace/talent/the_id/details") ?>';
            url = url.replace("the_id", talent_id);
            var modal = $(this);
            modal.find('.modal-body').css('opacity', '0');

            if(talent_id === undefined){
                modal.find('.modal-body').animate({opacity:1});
                return;
            }

            if(task_assigned != 0){
                modal.find('.col-talent-modal-button-wrapper').hide();
                modal.find('.col-talent-details-wrapper').css('marginTop', '24px');
            }else{
                modal.find('.col-talent-details-wrapper').removeAttr('style');
            }


	        $.ajax({
		        url: url,
                <?php if(!stripos(request_uri(), 'job-centre')): ?>
		        data: {'task_id' : task_id}
		        <?php else: ?>
		        data: {'job_id' : job_id}
		        <?php endif ?>
	        }).done(function(response){

                var talent = response.talent;
		        var photo = talent.photo;

                var starred = parseInt(Math.floor(talent.rating.overall)/2),
                    rest = 5 - starred,
                    stars = '',
	                counter = 1;
                if(starred > 0) {
                    while(counter <= starred)
                    {
                        stars+='<span class="rating-star-icon active"><i class="fa fa-star"></i></span>';
                        counter++;
                    }
                }
                if(rest > 0) {
                    counter = 1;
                    while(counter <= rest){
                        stars +='<span class="rating-star-icon"><i class="fa fa-star"></i></span>';
                        counter++;
                    }
                }
                var skills = [], languages = [], allSkills = '', allLanguages = '';
                if(talent.skills.length > 0){
                    skills = talent.skills.split(',');
                    skills.forEach(function(skill, index){
                        allSkills += '<span class="skills-label-link"><span class="tag label label-info bubble-lbl">'+skill+'</span></span> ';
                    });
                }

                if(talent.language.length > 0){
                    languages = talent.languages.split(',');
                    languages.forEach(function(lang, index){
                        allLanguages += '<span class="skills-label-link"><span class="tag label label-info bubble-lbl">'+lang+'</span></span> ';
                    });
                }

                var reviewHtml = modal.find('.col-rating-list li:first-child').clone();
                var empHtml = modal.find('.talent-details-workxp-row').clone();
                var eduHtml = modal.find('.talent-details-edu-row').clone();
                var education_container = modal.find('.talent-details-edu-rows');
                modal.find('.talent-details-workxp-row').hide();
                modal.find('.talent-details-edu-row').hide();

                var emptyHtml = '<div class="empty-state-container">';
                emptyHtml += '<div class="empty-img"><img src="<?php echo imgCrop('/assets/img/mtp-no-data-2.png') ?>" alt=""></div>';
                emptyHtml += '<span class="empty-lbl">No data to display</span>';
                emptyHtml += '</div>';

                if(talent.employments.length > 0){
                    talent.employments.forEach(function(emp, index){
                        var e = empHtml.clone();
	                    e.show();
                        e.find('.talent-details-workxp-compname').text(emp.company_name);
                        e.find('.talent-details-workxp-pos').text(emp.position);
                        e.find('.talent-details-workxp-location').text(emp.location);
                        e.find('.talent-details-workxp-duration').text(emp.duration);
                        e.find('.talent-details-workxp-resp .job-desc').html(emp.job_desc);
                        e.find('.talent-details-workxp-list').html(emp.job_responsibilities);

                        modal.find('.talent-details-workxp-rows').append(e[0]);
                    });

                }else{
                    $('.talent-details-workxp-rows').html(emptyHtml);
                }

                if(talent.educations.length > 0){
                    talent.educations.forEach(function(edu, index){
                        var ed = eduHtml.clone();
	                    ed.show();
                        ed.find('.talent-details-edu-insname').text(edu.edu_institution);
                        ed.find('.talent-details-edu-level').text(edu.highest_edu_level);
                        ed.find('.talent-details-edu-fieldstudy').text(edu.field + ', ' + edu.major);
                        ed.find('.talent-details-edu-location').text(edu.state + ', ' + edu.country);
                        ed.find('.talent-details-edu-grade').text(edu.grade);
                        ed.find('.talent-details-edu-year').text(edu.grad_year);

                        education_container.append(ed[0]);
                    });

                }else{
                    $('.talent-details-edu-rows').html(emptyHtml);
                }

                if(talent.reviews.length > 0){
                    modal.find('.col-rating-list li:first-child').hide();
                    modal.find('.col-rating-list li:not(:first-child)').remove();
                    talent.reviews.forEach(function(review, index){
                        var r = reviewHtml.clone(),
                            photo = review.photo;

	                    //var overall = (review.timeliness + review.quality + review.communication + review.responsiveness)/4;

                        var starred = parseInt(Math.floor(review.total_rating)/2),
                            rest = 5 - starred,
                            stars = '',
                            counter = 1;
                        if(starred > 0) {
                            while(counter <= starred)
                            {
                                stars+='<span class="rating-star-icon active"><i class="fa fa-star"></i></span>';
                                counter++;
                            }
                        }
                        if(rest > 0) {
                            counter = 1;
                            while(counter <= rest){
                                stars +='<span class="rating-star-icon"><i class="fa fa-star"></i></span>';
                                counter++;
                            }
                        }

	                    var reviewer = review.company_name || (review.firstname+ ' '+review.lastname);
                        r.find('.col-rating-photo img').attr('src', photo);
                        r.find('.rating-name-link').text(reviewer);
                        r.find('.bottom-rating-description').text(review.review);
                        r.find('.task-details-comment-date').text(moment(review.created_at).fromNow());
                        r.find('.task-row-rating-star').html(stars);
                        r.find('.task-row-average-rating-val').text(review.total_rating);
                        modal.find('.col-rating-list').append(r[0]);
                        modal.find('.col-rating-list li:not(:first-child)').show();
                    });

                }else{

                    modal.find('.col-rating-list li:not(:first-child)').remove();
                    modal.find('.col-rating-list li:first-child').hide();
                    $('.col-rating-list').html(emptyHtml);
                }

                modal.find('.talent-details-name').text(talent.name);
                modal.find('.avatar-img').attr('src', photo);
                modal.find('.talent-details-profile-location-val').text(talent.state + ", " + talent.country);
                modal.find('.talent-rating-overall').text(parseFloat(talent.rating.overall).toFixed(1));
                modal.find('.talent-rating-reviews-count').text(talent.rating.count + ' ' + (talent.rating.count == 1 ? '<?php echo lang('task_details_review') ?>' : '<?php echo lang('task_details_reviews') ?>'));
                modal.find('.tasks-completed').text(talent.info.completed);
                modal.find('.tasks-on-going').text(talent.info.on_going);
                modal.find('.total-hours').text( parseInt(talent.info.hours_completed) );
                modal.find('.last-login').text(talent.info.last_login);
                modal.find('.member-since').text(talent.member_since);
                <?php if(!stripos(request_uri(), 'job-centre')): ?>
                modal.find('#task_id').val(task_id);
                <?php else: ?>
                modal.find('#job_id').val(job_id);
                <?php endif ?>
                modal.find('#candidate_id').val(talent_id);
                modal.find('.talent-rating-stars').html(stars);
                modal.find('.talent-details-about').html(talent.about ? talent.about : emptyHtml);
                modal.find('.talent-skills').html(allSkills);
                modal.find('.talent-languages').html(allLanguages ? allLanguages : emptyHtml);
                modal.find('.modal-body').animate({opacity:1});
                button.parent().parent().find('td:first-child').removeClass('new-active');

                if(talent.docs_html){
                    modal.find('.talent-details-documents').html(talent.docs_html);
                    modal.find('.talent-details-documents').parent().show();
                }else{
                    modal.find('.talent-details-documents').html(talent.docs_html);
                    modal.find('.talent-details-documents').parent().hide();
                }

                /*if(button.closest('.posted-task-row').find('.col-chat.active, .info.new-active').length === 0){
                    button.closest('.posted-task-row').removeClass('active-row');
                }*/
		        button.closest('.info').removeClass('new-applicant');

                modal.find('[title]').tooltip();
            });
        });

        $('#modal_confirm_talent').on('show.bs.modal', function(event){
           var link = $(event.relatedTarget);

           if( link.data('hireLink') === true ){
               var talent = link.data('id');
               var task = link.data('taskId');
               $('#assign-task').find('#task_id').val(task);
               $('#assign-task').find('#candidate_id').val(talent);
               link.closest('tr').children('td').first().removeClass('new-applicant');
           }
        });

        $('#post_task_od').on('show.bs.modal', function (event) {
            var link = $(event.relatedTarget);
	        var task_id = link.data('taskId');
            var modal = $(this);
            modal.find('.summernote').summernote({
                tooltip: false,
                placeholder: 'Task Description',
                toolbar: [
                    ['style', ['bold', 'italic', 'underline']],
                    ['para', ['ul', 'ol']],
                ],
                height: 100,
                width: '100%',
            });
	        if(task_id){
	            var url = '<?php echo option('site_uri') . url_for("/workspace/task/task_id/details") ?>';
	            url = url.replace("task_id", task_id);
                $.ajax({
                    url: url
                }).done(function(response){

                    var task = response.task;
                    var tags = task.tags[Object.keys(task.tags)[0]] || [];
                    var questions = task.questions[Object.keys(task.questions)[0]] || [];
                    var attachments = task.attachments || [];
                    var skills = task.skills;

                    modal.find('#form_post_task_od').prepend('<input type="hidden" name="task_id" value="' + task.task_id + '" />');
                    modal.find('#od_category').val(task.category).trigger('change');
                    modal.find('#od_subcategory').on("categoryUpdated", function(e){
                        $(this).val(task.sub_category);
                    })

                    modal.find('#od_tasktitle').val(task.title);
                    modal.find('#od_taskdesc').val(task.description);
                    modal.find('.summernote').summernote('code', task.description);
                    if(task.location === 'remotely'){
                        modal.find('#tl_remote').prop('checked', true);
                        modal.find('#tl_remote').trigger('change');
                    }else{
                        modal.find('#loc').val(task.location);
                        modal.find('#tl_travel').prop('checked', true);
                        modal.find('#tl_travel').trigger('change');
                    }
                    if(task.task_type === 'by-hour'){
                        modal.find('#not_bythehour').prop('checked', true);
                        modal.find('#not_bythehour').trigger('change');
                        modal.find('input[name*="od_hours"]').val(task.hours);
                        modal.find('input[name*="od_budget[by-hour]"]').val(task.budget);
                        modal.find('#bthHourlyRate').val(parseInt(task.budget/task.hours));

	                    var start_date = moment(task.start_by);

	                    if(start_date.isBefore(moment()) || start_date.isSame(moment()))
	                        start_date = false;
	                    else
	                        start_date = start_date.format("DD-MMM-YYYY");

                        modal.find('#form_datetime_hour').datetimepicker('clear');
                        modal.find('input[name*="od_start_by[by-hour]"]').val(start_date || '');
                        modal.find('#form_datetime_hour').datetimepicker({
                            defaultDate: start_date
                        });
                        modal.find('input[name*="od_start_by[by-hour]"]').trigger('change');

                        var end_date = moment(task.complete_by);
                        if(end_date.isBefore(moment()) || end_date.isSame(moment()))
                            end_date = false;
                        else
                            end_date = end_date.format("DD-MMM-YYYY");

                        modal.find('#form_datetime_hour_completeby').datetimepicker('clear');
                        modal.find('input[name*="od_complete_by[by-hour]"]').val(end_date || '');
                        modal.find('#form_datetime_hour_completeby').datetimepicker({
	                        defaultDate: end_date
                        });
                        modal.find('input[name*="od_complete_by[by-hour]"]').trigger('change');

                        if(task.urgent == 1) {
                            modal.find('input[name*="od_urgent[by-hour]"]').prop('checked', true);
                        }else{
                            modal.find('input[name*="od_urgent[by-hour]"]').prop('checked', false);
                        }

                        if (start_date && moment(start_date).isBefore(moment().add(5, 'days'))) {
                            modal.find('#form_byhour .form-mark-urgent').removeClass('hide');
                        }

                        modal.find('[name*="rejection_rate[by-hour]"]').val(task.rejection_rate);
                        modal.find('[name*="rejection_rate[by-hour]"]').trigger('change');
                    }else{
                        modal.find('#not_lumpsum').prop('checked', true);
                        modal.find('input[name*="od_budget[lump-sum]"]').val(task.budget);
                        modal.find('#not_lumpsum').trigger('change');

                        var start_date = moment(task.start_by);

                        if(start_date.isBefore(moment()) || start_date.isSame(moment()))
                            start_date = '';
                        else
                            start_date = start_date.format("DD-MMM-YYYY");

                        modal.find('#form_datetime_lumpsum').datetimepicker('clear');
                        modal.find('input[name*="od_start_by[lump-sum]"]').val(start_date || '');
                        modal.find('form_datetime_lumpsum').datetimepicker({
                            defaultDate: start_date
                        });
                        modal.find('input[name*="od_start_by[lump-sum]"]').trigger('change');

                        var end_date = moment(task.complete_by);
                        if(end_date.isBefore(moment()) || end_date.isSame(moment()))
                            end_date = '';
                        else
                            end_date = end_date.format("DD-MMM-YYYY");

                        modal.find('#form_datetime_lumpsum_completeby').datetimepicker('clear');
                        modal.find('input[name*="od_complete_by[lump-sum]"]').val(end_date || '');
                        modal.find('#form_datetime_lumpsum_completeby').datetimepicker({
                            defaultDate: end_date
                        });
                        modal.find('input[name*="od_complete_by[lump-sum]"]').trigger('change');

                        if(task.urgent == 1) {
                            modal.find('input[name*="od_urgent[lump-sum]"]').prop('checked', true);
                        }else {
                            modal.find('input[name*="od_urgent[lump-sum]"]').prop('checked', false);
                        }

                        if (start_date && moment(start_date).isBefore(moment().add(5, 'days'))) {
                            modal.find('#form_bylumpsum .form-mark-urgent').removeClass('hide');
                        }

                        modal.find('[name*="rejection_rate[lump-sum]"]').val(task.rejection_rate);
                        modal.find('[name*="rejection_rate[lump-sum]"]').trigger('change');
                    }

                    /*tags.forEach(function(i, index) {
                        tag = i.name;
                        $('#tag1').tagsinput('add', tag);
                    });*/
                    skills.forEach(function(skill, idx){
                        $('#tag1').tagsinput('add', skill);
                    });

                    questions.forEach(function(i, index){
	                    modal.find('input[name*="od_qna[]"]').last().val(i.name);
	                    if(index != questions.length - 1){
	                        modal.find('.od-add-button').trigger('click');
	                    }
                    });

	                $('.attachments-list').remove();
                    attachments_list = '<div class="form-group attachments-list"><ul class="draft-attachments">';
                    attachments.forEach(function(a, idx){
                        var splits = a.name.split('/');
                       attachments_list += '<li><span title="' + (splits[splits.length-1]) + '">' + (idx+1) + '</span>' +
	                                            '<a href="#" class="remove-attachment text-danger" data-id="'+a.id+'" ' +
	                                            'data-task="'+task.task_id+'"' +
	                                            '><span class="fa fa-trash"></span></a>' +
	                                        '</li>';
                    });
	                attachments_list += '</ul></div>';
                    modal.find('#sf2 .modal-col-right fieldset').append(attachments_list);

                    modal.find('.remove-attachment').on('click', function(e){
                        e.preventDefault();
                        var url = '<?php echo option('site_uri') . url_for("/workspace/the_task/attachment/the_file/delete") ?>',
                            link = $(e.currentTarget),
                            id = link.data('id'),
                            task = link.data('task'),
                            message = {};
                        link.parent().addClass('text-danger');
                        link.addClass('disabled-link').removeClass('text-danger').attr('disabled', 'disabled');
                        url = url.replace(/the_file/g, id);
                        url = url.replace(/the_task/g, task);


                        $.ajax({
	                        url: url
                        }).done(function(response){
                            message = {};
                            if(response.status === 'success'){
                                link.parent().slideUp(function(){ $(this).remove() })
                                message.title = '<b><?php echo lang('success') ?>!</b>';
                                message.status = 'success';
                                message.text = response.message;
                                message.icon = "fa fa-check";
                            }else{
                                message.title = '<b><?php echo lang('error') ?>!</b>';
                                message.status = 'danger';
                                message.text = response.message;
                                message.icon = "fa fa-exclamation";
                                link.removeClass('disabled-link').addClass('text-danger').removeAttr('disabled');
                                link.parent().removeClass('text-danger');
                            }
                            $.notify({
                                title: message.title,
                                message: message.text,
                                icon: message.icon
                            }, {
                                style: 'metro',
                                type: message.status,
                                globalPosition: 'top right',
                                showAnimation: 'slideDown',
                                hideAnimation: 'slideUp',
                                showDuration: 200,
                                hideDuration: 200,
                                autoHide: true,
                                clickToHide: true
                            });
                        });

                    });

                });
	        }
        });

        $('#modal_view_answer').on('hidden.bs.modal', function(e){
            $(this).find('.qna-list li:not(:first-child)').remove();
            $(this).find('.spinner-border').removeClass('hide');
            $(this).find('.qna-list').addClass('hide');
        });

        $('#modal_view_answer').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget)
            var talent_id = button.data('id')
            var task_id = button.data('taskId')
            var job_id = button.data('jobId')
	        var url = '';
	        if(task_id === undefined) {
	            task_id = job_id;
                url = '<?php echo option('site_uri') . url_for("/workspace/job/the_task/answers/the_talent") ?>';
            }else{
                url = '<?php echo option('site_uri') . url_for("/workspace/task/the_task/answers/the_talent") ?>';
	        }

            url = url.replace("the_task", task_id);
            url = url.replace("the_talent", talent_id);

            var modal = $(this);

            button.closest('tr').children('td').first().removeClass('new-applicant');

            $.ajax({
                url: url
            }).done(function(response){
                var qna = response.answers;
                var answers_container = modal.find('.qna-list');
                var qna_item = modal.find('.qna-list li:first-child').clone();
                //modal.find('.qna-list li:first-child').hide();
                modal.find('.qna-list li:not(:first-child)').remove();
                modal.find('.spinner-border').addClass('hide');
	            qna.forEach(function(q, index){
	                var item = qna_item.clone();
	                item.find('.qna-question').text(q.question);
	                item.find('.qna-answer').text(q.answer);
	                item.removeClass('hide').show();
	                answers_container.append(item[0]);
	            });
	            answers_container.removeClass('hide');
            });
        });

        $('#modal_confirm_delete_od').on('show.bs.modal', function (event) {
            var link = $(event.relatedTarget);
            var task_id = link.data('taskId');
            var modal = $(this);
            modal.find('input[name="task_id"]').val(task_id);
            modal.find('.btn-confirm').on('click', function(e){
                modal.find('form')[0].submit();
            });
        });

        $('#modal_confirm_cancel_od').on('show.bs.modal', function (event) {
            var link = $(event.relatedTarget);
            var task_id = link.data('taskId');
            var modal = $(this);
            modal.find('input[name="task_id"]').val(task_id);
            modal.find('.btn-confirm').on('click', function(e){
                modal.find('form')[0].submit();
            });
        });

        $('form.questions-form').on('submit', function(e){
            e.preventDefault();
            $(this).find('button').prop('disabled', true);
            this.submit();
        });
	</script>
<?php endif; ?>

	<script>
		$(document).ready(function(){
			<?php if(flash_now('message') && flash_now('status')): ?>
            $.notify({
                title: "<b><?php echo flash_now('status') === "success" ? lang("success") : lang("error") ?>!</b>",
                message: "<?php echo flash_now('message') ?>",
                icon: "<?php echo flash_now('status') === 'success' ? "fa fa-check" : "fa fa-exclamation" ?>"
            }, {
                style: 'metro',
                type: "<?php echo flash_now('status') ?>",
                globalPosition: 'top right',
                showAnimation: 'slideDown',
                hideAnimation: 'slideUp',
                showDuration: 200,
                hideDuration: 200,
                autoHide: true,
                clickToHide: true
            });
            <?php endif ?>
		});
	</script>
	<?php if(isset($preferred_times)): ?>
	<script>
		var task_planning = <?php echo json_encode($preferred_times) ?>;
	</script>
    <?php endif ?>

<script>

	var chatTimeOut = null;
    $('#modal_chat').on('show.bs.modal', function(e){
        var chatbtn = $(e.relatedTarget);
        var receiverID = chatbtn.data('userId');
        var taskId = chatbtn.data('taskId');
        var taskType = chatbtn.data('taskType') || 'task';
        var taskSlug = chatbtn.data('taskSlug');
        var chatEnabled = chatbtn.data('chatEnabled');
        var chatClosed = chatbtn.data('taskClosed');
        var chatClosedDate = chatbtn.data('taskClosedDate');
        var url = window.location.pathname;
        var modal = $(this);
        var receiverImg = '';
        var receiverName = '';
        var scrollDown = false;

        var applicantRow = chatbtn.closest('tr').children('td').first();
        if( applicantRow.hasClass('new-applicant') ) {
            $.ajax({ url: '<?php echo option("site_uri") . url_for("workspace/task/viewed") ?>/'+taskId+'/'+receiverID });
            applicantRow.removeClass('new-applicant');
        }
        
	    if(chatbtn.parent().prop('nodeName') === 'TD'){
	        chatbtn.parent().removeClass('active');
            if(chatbtn.closest('.posted-task-row').find('.col-chat.active, .info.new-active').length === 0){
                chatbtn.closest('.posted-task-row').removeClass('active-row');
            }
	    }else{
	        chatbtn.removeClass('active');
	        if(chatbtn.closest('.applied-task-row').find('.btn-chat.active').length === 0){
                chatbtn.closest('.applied-task-row').removeClass('active-row');
	        }
	    }

        // removing any messages in the chat body
        modal.find('.chat-content').html('');

	    if(chatEnabled) {
            modal.find('.chat-footer').find('input, button, textarea').prop('disabled', false);
            modal.find('.chat-footer').find('input, button, textarea').removeAttr('disabled');
            var xhttp = new XMLHttpRequest();

            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    if (this.responseText !== '') {
                        modal.find('.chat-content').html(this.responseText);
                       
                       
                        setTimeout(function () {
                                var element = modal.find('.chat-content')[0];
                                element.scrollTo({top: element.scrollHeight, left: 0, behavior: 'smooth'});

                                if(chatClosed){
                                    var closing = modal.find('#closing-mark').clone();
                                    closing.html(function(idx, html){ return html.replace(/%ClosedDate%/g, chatClosedDate) });
                                    closing.css('display', 'initial');
                                    $(element).find('.container-xxl:first-child').append(closing);
                                    closing.show();
                                    modal.find('.chat-footer').find('input, button, textarea').attr('disabled', true);
                                    modal.find('.btn-send-msg').off('click');
                                    modal.find('#chat-id-input').off('keydown');
                                    clearInterval(chatTimeOut);
                                    chatTimeOut = null;
                                }

                            }
                            , 100);
                    }
                }

            };

            xhttp.open("GET", rootPath + "ajax/chat/content/" + receiverID + "/" + taskId + "/" + taskType, true);
            xhttp.send();

            if(!chatClosed) {
                chatTimeOut = setInterval(function () {
                    var url = window.location.pathname;
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function () {
                        if (this.readyState === 4 && this.status === 200) {
                            if (this.responseText !== '') {
                                modal.find('.chat-content').html(this.responseText);
                                var element = modal.find('.chat-content')[0];
                                if(scrollDown) {
                                    element.scrollTo({top: element.scrollHeight, left: 0, behavior: 'smooth'});
                                    scrollDown = !scrollDown;
                                }
                            }
                        }
                    };
                    xhttp.open("GET", rootPath + "ajax/chat/count/" + receiverID + "/" + taskId + "/" + taskType, true);
                    xhttp.send();


                }, 2000);

                // when Send button is clicked, fire send() function
                modal.find('.btn-send-msg').on('click', function (e) {
                    send(receiverID, taskId, taskType);
                    scrollDown = true;
                });

                
                //when enter is pressed in the textarea, fire send() function
                modal.find("#chat-id-input").keydown(function (e) {
                    if (e.keyCode === 13) {
                        modal.find('.btn-send-msg').trigger('click');
                    }
                });
            }
        }else{
            if(chatClosed){
                var closing = modal.find('#closing-mark').clone();
                var element = modal.find('.chat-content')[0];
                console.log(closing, element)
                element.scrollTo({top: element.scrollHeight, left: 0, behavior: 'smooth'});
                closing.html(function(idx, html){ return html.replace(/%ClosedDate%/g, chatClosedDate) });
                closing.css('display', 'initial');
                if($(element).find('.container-xxl:first-child'))
                    $(element).find('.container-xxl:first-child').append(closing);
                else
                    $(element).find('.chat-content').append(closing);
                closing.show();
                clearInterval(chatTimeOut);
                chatTimeOut = null;
            }
            modal.find('.btn-send-msg').off('click');
            modal.find('#chat-id-input').off('keydown');
            modal.find('.chat-footer').find('input, button, textarea').attr('disabled', true);
            modal.find('.chat-footer').find('input, button, textarea').prop('disabled', true);
	    }

    });

    // when chat modal is closed, stop and clear the Interval, and remove any events from Send and Textarea elements
    $('#modal_chat').on('hidden.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.btn-send-msg').off('click');
        modal.find('#chat-id-input').off('keydown');
        clearInterval(chatTimeOut);
        chatTimeOut = null;
    });

    function pinMessage(id,type){
        $.ajax({
            url: rootPath + 'ajax/chat/pin/?id=' + id+'&type='+type,
        }).done(function (response) {
                 
        });
    }

    function unpinMessage(id, el){
        $.ajax({
            url: rootPath + 'ajax/chat/unpin/' + id,
        }).done(function (response) {
            li = $(el).closest('li');
            li.slideUp(function(){
                li.remove();
            });
        });
    }

    function send(receiverID, taskId, taskType){

        var url = window.location.pathname;
        var message = $("#chat-id-input").val();
        var replyID= $("#replyID").val();

        var fd = new FormData();
        var files = $('#hiddenUploadButton')[0].files;

        if (tmp_files.length > 0) { //got file but no message

            for (var i = 0; i < tmp_files.length; i++) {
                fd.append('file' + i, tmp_files[i]);
            }

            if (message != "") {
                fd.append('content', message);

            }
            if (replyID != "") {
                fd.append('replyID', replyID);
            }

            $.ajax({
                url: rootPath + 'ajax/chat/uploadAttachment/' + receiverID + '/' + taskId + '/' + taskType,
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,

                success: function (response) {
                    $('#hiddenUploadButton').val('');
                    $("#chat-id-input").removeAttr('disabled');
                    $(".disableProp").css("background-color", "#FFF");
                    $("#chat-id-input").val('');
                    $("#replyID").val('');
                    $('#closeReply').click();
                },
            });

        } else if (document.getElementById("hiddenUploadButton").files.length == 0 && message != "") { //no file but got message
            $.ajax({
                url: rootPath + 'ajax/chat/send/' + receiverID + '/' + taskId + '/' + taskType + '?content=' + message + "&replyID=" + replyID,

            }).done(function (response) {
                $("#chat-id-input").val('');
                $("#replyID").val('');
                $('#closeReply').click();
            });

        } else if (document.getElementById("hiddenUploadButton").files.length > 0 && message != "") { // got file and message


        }
    }
</script>

<script type="text/javascript" src="<?php echo url_for('/assets/libs/blueimp/iframe-transport.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/blueimp/fileupload.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/js-mtp'); ?>/uploader.js"></script>

<?php if( isset($scripts) ): ?>
<?php echo $scripts ?>
<?php endif ?>

<?php if( isset($extra_scripts) ): ?>
<?php echo $extra_scripts ?>
<?php endif ?>
</body>

</html>
