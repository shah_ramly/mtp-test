<!DOCTYPE html>
<html lang="<?php echo !isset($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) || ($_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] == 'EN') ? 'en':'ms' ?>">

<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P7GCVWH');</script>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo url_for('/assets/'); ?>/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?php echo url_for('/assets/'); ?>/img/favicon.png">
    <title>Personalise Your Profile | MakeTimePay</title>
    
    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/'); ?>/css/glyphicons.css" rel="stylesheet">
    <link href="<?php echo url_for('/assets/'); ?>/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- Nucleo Icons -->
    <link href="<?php echo url_for('/assets/'); ?>/css/nucleo-icons.css" rel="stylesheet" />
    
    <!-- CSS Files -->
    <link href="<?php echo url_for('/assets/'); ?>/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo url_for('/assets/'); ?>/css/bootstrap-dropdownhover.min.css" rel="stylesheet">
    
    <link href="<?php echo url_for('/assets/'); ?>/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/'); ?>/css/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="<?php echo url_for('/assets/'); ?>/css/dropzone.min.css" rel="stylesheet">
    <link href="<?php echo url_for('/assets/'); ?>/css/swiper.min.css" rel="stylesheet">
    <link href="<?php echo url_for('/assets/'); ?>/css/bootstrap-slider.min.css?v=1.0.1" rel="stylesheet" />

    <link href="<?php echo url_for('/assets/'); ?>/css/global.css" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/'); ?>/css/style-shah.css<?php echo  '?' . option('style_version') ?>" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/'); ?>/css/style-syafiq.css<?php echo  '?' . option('style_version') ?>" rel="stylesheet" />
    <link href="<?php echo url_for('/assets/'); ?>/css/style-byte2c.css" rel="stylesheet" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-191800673-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-191800673-1');
    </script>
</head>

<body class="white-content od-mode">
    <header class="private-header">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light" id="navbarMain">
                <a class="navbar-brand" href="<?php echo url_for('/') ?>">
                    <div class="logo"><img src="<?php echo url_for('assets/img/mtp_logo.png') ?>" alt=""></div>
                </a>
            </nav>
        </div>
    </header>
    <section class="section section-bodyintro section-bubbleintro" id="bodyintro">
		<div class="container">
            <div class="personalize-header-flex">
                <div class="col-left">
                    <h2 class="bodyintro-title"><?php echo lang('bubble_intro_main_title'); ?></h2>
                </div>
                <div class="col-right">
                    <p class="body-para bodyintro-body"><?php echo lang('bubble_description'); ?></p>
                </div>
            </div>
		</div>
	</section>

    <form id="personalizeForm">
    <div class="bubble-selection-container skills-container">
        <div class="bubble-group-row active">
            <div class="bubble-section-header">
                <div class="tbl-main-title tbl-task-name"><span class="title-counter">1 of 3 </span> <?php echo lang('bubble_title_your_skills'); ?></div>
            </div>
            <div class="col-bubble-selection">
                <div class="skills-section">
                    <div id="skills" class="button-group-pills button-subgroup-pills skills" data-toggle="buttons">
	                    <?php foreach ($skills as $key => $skill): ?>
	                    <label class="btn btn-bubble">
		                    <input type="checkbox" class="skill-tier" data-steps="<?php echo $key ?>" name="skillstier[]" value="<?php echo $skill['id'] ?>" /><div><?php echo $skill['name'] ?></div>
	                    </label>
	                    <?php endforeach ?>
                    </div>
                    <div class="form-group form-addmore-skills">
                        <label class="input-lbl">
                            <span class="input-label-txt"><?php echo lang('bubble_sub_title'); ?></span>
                        </label>
                        <span class="para-lbl-txt"><?php echo lang('bubble_sub_description'); ?></span>
                        <div class="bs-example">
                            <input type="text" name="skills" class="bootstrap-tagsinput skills" placeholder="<?php echo lang('bubble_add_skills'); ?>" value="<?php //echo $user->info['skills']; ?>" />
                        </div>
                    </div>
                    <div class="bubble-selection-actions">
                        <button type="button" class="btn-icon-full btn-confirm btn-icon-disabled next-section" data-dismiss="modal" data-orientation="next" disabled>
                            <span class="btn-label"><?php echo lang('post_next'); ?></span>
                            <span class="btn-icon">
                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                </svg>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="bubble-group-row inactive">
            <div class="bubble-section-header">
                <div class="tbl-main-title tbl-task-name"><span class="title-counter">2 of 3 </span> <?php echo lang('bubble_title_your_interests'); ?></div>
            </div>
            <div class="col-bubble-selection">
                <div class="interests-section hide">
                    <div id="interests" class="button-group-pills button-subgroup-pills interests" data-toggle="buttons">
                        <?php foreach ($interests as $key => $interest): ?>
		                    <label class="btn btn-bubble">
			                    <input type="checkbox" class="interest-tier" name="intereststier[]"  data-steps="<?php echo $key ?>" value="<?php echo $interest['id'] ?>" /><div><?php echo $interest['name'] ?></div>
		                    </label>
                        <?php endforeach ?>
                    </div>
                    <div class="form-group form-addmore-interests">
                        <label class="input-lbl">
                            <span class="input-label-txt"><?php echo lang('bubble_sub_title_interest'); ?></span>
                        </label>
                        <span class="para-lbl-txt" style="display:block"><?php echo lang('bubble_sub_description_interest'); ?></span>
                        <div class="bs-example">
                            <input type="text" name="interests" class="bootstrap-tagsinput interests" placeholder="<?php echo lang('bubble_add_interests'); ?>" />
                        </div>
                    </div>
                    <div class="bubble-selection-actions">
                        <button type="button" class="btn-icon-full btn-prev btn-prev-icon previous-section">
                            <span class="btn-icon">
                                <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"></path></svg>
                            </span>
                        </button>
                        <button type="button" class="btn-icon-full btn-confirm btn-icon-disabled next-section" data-dismiss="modal" data-orientation="next" disabled>
                            <span class="btn-label"><?php echo lang('post_next'); ?></span>
                            <span class="btn-icon">
                                <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                </svg>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="bubble-group-row inactive">
            <div class="bubble-section-header">
                <div class="tbl-main-title tbl-task-name"><span class="title-counter">3 of 3 </span> <?php echo lang('bubble_title_lets_get_started'); ?></div>
            </div>
            <div class="col-bubble-selection">
                <div class="letsgetstarted-section hide">
                    <div class="form-group form-addmore-almostthere">
                        <span class="para-lbl-txt"><?php echo lang('bubble_desc_thats_a_great_start'); ?></span>
                        <span class="para-lbl-txt"><?php echo lang('bubble_desc_if_you_want_to_add_more'); ?></span>
                        <span class="para-lbl-txt para-lbl-bold"><?php echo lang('bubble_desc_quick_tip'); ?></span>
                        <span class="para-lbl-txt para-lbl-bold"><?php echo lang('bubble_desc_now_lets_go_make_time_pay'); ?></span>
                    
                        <div class="bubble-selection-actions">
                            <button type="button" class="btn-icon-full btn-prev btn-prev-icon previous-section">
                                <span class="btn-icon">
                                    <svg class="bi bi-chevron-left" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"></path></svg>
                                </span>
                            </button>
                            <button type="submit" class="btn-icon-full btn-confirm btn-icon-disabled" data-dismiss="modal" data-orientation="next" disabled>
                                <span class="btn-label"><?php echo lang('bubble_btn_go_to_dashboard'); ?></span>
                                <span class="btn-icon">
                                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
                                    </svg>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>

    <!--   Core JS Files   -->
    <script src="<?php echo url_for('/assets/'); ?>/js/core/jquery.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/core/jquery-ui.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/core/popper.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/core/bootstrap.min.js"></script>
    
    <!--   Plugins JS Files   -->
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/moment-with-locales.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/tempusdominus-bootstrap-4.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/dragscroll.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/bootstrap-tagsinput-modified.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/swiper.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/bootstrap-slider.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/jquery.validate.js"></script>
    
    <!--  Search Suggestion Plugin    -->
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/typeahead.bundle.min.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/bloodhound.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/handlebars.js"></script>
    
    <!-- Chart JS -->
    <!--<script src='<?php /*echo url_for('/assets/'); */?>/js/plugins/snap.svg-min.js'></script>
    <script src="<?php /*echo url_for('/assets/'); */?>/js/plugins/chart.min.js"></script>
    <script src="<?php /*echo url_for('/assets/'); */?>/js/plugins/progressbar.js"></script>
    <script src="<?php /*echo url_for('/assets/'); */?>/js/plugins/d3.v3.min.js"></script>-->
    
    <!--  Notifications Plugin    -->
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/bootstrap-notify.js"></script>
    
    <!--  Custom JS    -->
    <script src="<?php echo url_for('/assets/'); ?>/js/plugins/dropzone.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/main.js<?php echo  '?' . option('script_version') ?>"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/bubble.js"></script>
    <script src="<?php echo url_for('/assets/'); ?>/js/script.js"></script>

    
    <!-- Sidebar Hover Function -->
    <script>
        var skillsInput = $('input.skills');
        var interestsInput = $('input.interests');

        var skillsList = <?php echo json_encode($skills) ?>;
        var interests = <?php echo json_encode($interests) ?>;

        skillsInput.on('change', function(e){
            if(skillsInput.val() !== ''){
                $('[type="submit"]').prop('disabled', false).removeClass('btn-icon-disabled');
                $('.skills-section button.next-section').prop('disabled', false).removeClass('btn-icon-disabled');
            }else{
                $('[type="submit"]').prop('disabled', true).addClass('btn-icon-disabled');
                $('.skills-section button.next-section').prop('disabled', true).addClass('btn-icon-disabled');
            }
        });

        interestsInput.on('change', function(e){
            if(interestsInput.val() !== ''){
                $('.interests-section button.next-section').prop('disabled', false).removeClass('btn-icon-disabled');
            }else{
                $('.interests-section button.next-section').prop('disabled', true).addClass('btn-icon-disabled');
                $('[type="submit"]').prop('disabled', true).addClass('btn-icon-disabled');
            }
        });

        $('button.next-section').on('click', function(e){
            var target = $(e.currentTarget);
            var parent = target.closest('.col-bubble-selection').parent();
            var container = target.closest('.col-bubble-selection').children().first();
            parent.removeClass('active').addClass('completed');
            container.fadeOut(function(){
                parent.next().removeClass('inactive').addClass('active');
                parent.next().find('.col-bubble-selection').children().first().hide().removeClass('hide').fadeIn();
                container.addClass('hide');
            });
        });

        $('button.previous-section').on('click', function(e){
            var target = $(e.currentTarget);
            var parent = target.closest('.col-bubble-selection').parent();
            var container = target.closest('.col-bubble-selection').children().first();
            parent.removeClass('active').addClass('inactive');
            container.fadeOut(function(){
                parent.prev().removeClass('completed').addClass('active');
                parent.prev().find('.col-bubble-selection').children().first().hide().removeClass('hide').fadeIn();
                container.addClass('hide');
            });
        });

        // Sidebar Hover Function
        const $dropdown = $(".sidebar .dropdown");
        const $dropdownToggle = $(".sidebar .dropdown-toggle");
        const $dropdownMenu = $(".sidebar .dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function () {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                    function () {
                        const $this = $(this);
                        $this.addClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "true");
                        $this.find($dropdownMenu).addClass(showClass);
                    },
                    function () {
                        const $this = $(this);
                        $this.removeClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "false");
                        $this.find($dropdownMenu).removeClass(showClass);
                    }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });

        $('.card-header .dropdown-menu .nav-tabs .nav-link').on("click.bs.dropdown", function (e) {
            $(this).tab('show');
            e.stopPropagation();
        });

        $('.col-account-header .dropdown.col-account .switch-toggle .slider-toggler').on("click", function (e) {
            e.stopPropagation();
        });

        $('#personalizeForm').on('submit', function(){
            $.ajax({
                type:'POST',
                dataType : 'json',
                data : $('#personalizeForm').serialize(),
                success: function(results){
                    if(results.error == true){
                        t('e', results.msg);
                    }else{
                        //t('s', results.msg);
                        //setTimeout(function(){
                            location.href = results.url;
                        //}, 1000);
                    }
                },
                error: function(error){
                    ajax_error(error);
                },
                complete: function(){}
            });
            return false;
        });

        function skillsCallback(ele) {
            if (ele.checked) {
                var $this = $(ele);
                var id = $this[0].value;
                var name = $this.next()[0].innerText;
                var steps = $this.data('steps');
                var stepsLevel = steps.toString().split(',');
                var tempSkills = [];
                var parentStep = 0;
                stepsLevel.forEach(function(step, idx){
                    if(idx === 0) {
                        parentStep = step;
                        tempSkills = skillsList[parentStep].regimes;
                    }else {
                        tempSkills = skillsList[parentStep].regimes[step].regimes;
                    }
                });

                $this.prop('disabled', true).parent().addClass('disabled');
                insertTag(skillsInput, id, name);

                var children = tempSkills || [];

                if(children.length){
                    generateMoreSkillBubbles(children, $this, steps);
                }else {
                    getMoreSkills(id, $this, skillsList);
                }

                $this.parent().fadeOut(300);
            }
        }

        function interestCallback(ele) {
            if (ele.checked) {
                var $this = $(ele);
                var id = $this[0].value;
                var name = $this.next()[0].innerText;
                var steps = $this.data('steps');
                var stepsLevel = steps.toString().split(',');
                var tempInterests = [];
                var parentStep = 0;
                stepsLevel.forEach(function(step, idx){
                    if(idx === 0) {
                        parentStep = step;
                        tempInterests = interests[parentStep].regimes;
                    }else {
                        tempInterests = interests[parentStep].regimes[step].regimes;
                    }
                });

                $this.prop('disabled', true).parent().addClass('disabled');
                insertTag(interestsInput, id, name);

                var children = tempInterests || [];

                if(children.length){
                    generateMoreBubbles(children, $this, steps);
                }else {
	                getMoreInterests(id, $this, interests);
                }

                $this.parent().fadeOut(300);
            }
        }

        function insertTag(parent, id, name){
            parent.tagsinput('add', { 'id': id, 'name': name });
        }

        function generateMoreSkillBubbles(children, ele, steps){
            $this = ele;
            var bubbles = '';
            children.forEach(function (value, idx) {
                var nextSteps = steps + ',' + idx;
                bubbles += '<label class="btn btn-bubble"><input type="checkbox" data-steps="'+nextSteps+'" class="skill-tier" name="skillstier[]" value="' + value.id + '"><div>' + value.name + '</div></label>';
            });
            $(bubbles).insertAfter($this.parent());
        }

        function generateMoreBubbles(children, ele, steps){
            $this = ele;
            var bubbles = '';
            children.forEach(function (value, idx) {
                var nextSteps = steps + ',' + idx;
                bubbles += '<label class="btn btn-bubble"><input type="checkbox" data-steps="'+nextSteps+'" class="interest-tier" name="intereststier[]" value="' + value.id + '"><div>' + value.name + '</div></label>';
            });
	        $(bubbles).insertAfter($this.parent());
        }

        function getMoreSkills(id, ele, skillsList){
            $this = ele;
            $.ajax({
                method: 'GET',
                url: '<?php echo option("site_uri") . url_for("/ajax/skills/") ?>/' + id,
            }).done(function (response) {
                if(response.length) {
                    var bubbles = '';
                    response.forEach(function (value, idx) {
                        skillsList.push(value);
                        var nextSteps = (skillsList.length - 1);
                        bubbles += '<label class="btn btn-bubble"><input type="checkbox" data-steps="' + nextSteps + '" class="interest-tier" name="intereststier[]" value="' + value.id + '"><div>' + value.name + '</div></label>';
                    });
                    $(bubbles).insertAfter($this.parent());
                    $this.parent().remove();
                }else{
                    $this.parent().remove();
                }
            });
        }

        function getMoreInterests(id, ele, interests){
            $this = ele;
            $.ajax({
                method: 'GET',
                url: '<?php echo option("site_uri") . url_for("/ajax/interests") ?>/' + id,
            }).done(function (response) {
                if(response.length) {
                    var bubbles = '';
                    response.forEach(function (value, idx) {
                        interests.push(value);
                        var nextSteps = (interests.length - 1);
                        bubbles += '<label class="btn btn-bubble"><input type="checkbox" data-steps="' + nextSteps + '" class="interest-tier" name="intereststier[]" value="' + value.id + '"><div>' + value.name + '</div></label>';
                    });
                    $(bubbles).insertAfter($this.parent());
                    $this.parent().remove();
                }else{
                    $this.parent().remove();
                }
            });
        }

        $('#skills').on('change', 'input.skill-tier', function(e){
            skillsCallback(e.currentTarget);
        });

        $('#interests').on('change', 'input.interest-tier', function(e){
            interestCallback(e.currentTarget);
        });
        
        var skills = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: rootPath + 'ajax/skills.json?all=1'
        });
        skills.clearPrefetchCache();
        skills.initialize();
        
        skillsInput.tagsinput({
            itemValue: function(item){ return item.id !== undefined ? item.id : 'new:'+item },
            itemText: function(item){ return item.name || item },
            confirmKeys: [9, 13, 188],
            typeaheadjs: {
                name: 'skills',
                displayKey: 'name',
                source: skills.ttAdapter(),
                templates: {
                    empty: Handlebars.compile("<div class='tt-empty'>No skills found</div><span class='tt-suggestion-footer tt-selectable add-new-tag'>Add new '{{query}}'</span>")
                }
            },
	        freeInput: true
        });

        skillsInput.on('beforeItemAdd', function(event) {
            if( typeof(event.item) === 'string' ){
                event.cancel = true;
                var target = $(this);
                target.tagsinput('add', {id:'new:'+event.item, name: event.item});
                //target.tagsinput('refresh');
            }
        });

        var all_interests = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            //prefetch: '<?php echo option("site_uri") . url_for("/ajax/interests.json") ?>?all=1'
            prefetch: {
                url: '<?php echo option("site_uri") . url_for("/ajax/interests.json") ?>?all=1',
                filter: function(list){
                    return $.map(list, function(interest){
                        return interest
                    })
                }
            }
        });
        all_interests.clearPrefetchCache();
        all_interests.initialize();

        interestsInput.tagsinput({
            itemValue: function(item){ return item.id !== undefined ? item.id : 'new:'+item },
            itemText: function(item){ return item.name || item },
            confirmKeys: [9, 13, 188],
            typeaheadjs: {
                name: 'all_interests',
                displayKey: 'name',
                source: all_interests.ttAdapter(),
                templates: {
                    empty: Handlebars.compile("<div class='tt-empty'>No interests found</div><span class='tt-suggestion-footer tt-selectable add-new-interest'>Add new '{{query}}'</span>")
                }
            },
	        freeInput: true
        });

        interestsInput.on('beforeItemAdd', function(event) {
            if( typeof(event.item) === 'string' ){
                event.cancel = true;
                var target = $(this);
                target.tagsinput('add', {id:'new:'+event.item, name: event.item});
            }
        });

        $('.bs-example').bind('typeahead:render', function(e) {
            var input = e.currentTarget;
            var target = interestsInput;
            if( target.length === 0 )
                target = interestsInput;

            var tag_btn = $('.add-new-interest');
            if( tag_btn.length ){
                tag_btn.off('click');
                tag_btn.on('click', function(e){
                    const tag_text = $(e.currentTarget).text();
                    const regex = /\'([\w\d\s\&\.\|]+)\'$/mg;
                    let m;

                    m = regex.exec(tag_text);
                    if( m !== null && m[1] !== undefined && m[1].length >= 2 ) {
                        var tag = m[1].trim();
                        target.tagsinput('add', {id:'new:'+tag, name: tag});
                    }
                })
            }
        });

        <?php if($selected_skills){ foreach($selected_skills as $skill){ ?>
        skillsInput.tagsinput('add', { 'id': '<?php echo $skill['id']; ?>', 'name': '<?php echo $skill['name']; ?>' });
        <?php }} ?>

        <?php if(isset($user->info['interests_list']) && !empty($user->info['interests_list'])){ foreach($user->info['interests_list'] as $interest){ ?>
        interestsInput.tagsinput('add', { 'id': '<?php echo $interest['id']; ?>', 'name': '<?php echo $interest['name']; ?>' });
        <?php }} ?>
    </script>
    <!-- Sidebar Hover Function -->


</body>

</html>
