<!DOCTYPE html>
<html lang="<?php echo !isset($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) || ($_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] == 'EN') ? 'en':'ms' ?>">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P7GCVWH');</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, width=device-width" />
<meta name="description" content="<?php echo $settings['meta_description'] ?? ''; ?>">
<meta name="keywords" content="<?php echo $settings['meta_keyword'] ?? ''; ?>">

<title><?php echo isset($title) ? $title : '' ?> | MakeTimePay </title>
<?php if( ! isset($not_found) ): ?>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/jquery/jquery-2.1.0.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/jquery/jquery-migrate-1.2.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/jquery/jquery-ui.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/bootstrap/timepicker/moment-with-locales.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/bootstrap/timepicker/bootstrap-datetimepicker.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/plupload/plupload.full.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/plupload/plupload.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/bxslider/jquery.bxslider.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/prettyPhoto/js/jquery.prettyPhoto.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/libs/owl-carousel/owl.carousel.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo url_for('/assets/js'); ?>/bootbox.min.js"></script>
<script type="text/javascript" src="<?php echo url_for('/js-mtp'); ?>/script.js"></script>
<?php endif ?>
 <meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo url_for('/assets'); ?>/img/apple-icon.png">
<link rel="icon" type="image/png" href="<?php echo url_for('/assets'); ?>/img/favicon.png">
<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet" />
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<!-- Nucleo Icons -->
<link href="<?php echo url_for('/assets/css'); ?>/nucleo-icons.css" rel="stylesheet" />
<!-- CSS Files -->
<link href="<?php echo url_for('/assets/css'); ?>/global.css" rel="stylesheet" />
<link href="<?php echo url_for('/assets/css'); ?>/back-dashboard.css" rel="stylesheet" />
<link href="<?php echo url_for('/assets/css'); ?>/style-shah.css<?php echo  '?' . option('style_version') ?>" rel="stylesheet" />
<link href="<?php echo url_for('/assets/css'); ?>/style-syafiq.css<?php echo  '?' . option('style_version') ?>" rel="stylesheet" />
<link href="<?php echo url_for('/assets/css'); ?>/style-byte2c.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<?php echo $settings['google_analytics']; ?>

<script src="<?php echo url_for('/assets/js'); ?>/core/jquery.min.js"></script>
<?php if( ! isset($not_found) ): ?>
  <!--   Core JS Files   -->
    <script src="<?php echo url_for('/assets/js'); ?>/core/jquery-ui.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/core/popper.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/core/bootstrap.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!--  Google Maps Plugin    -->
    <!-- Chart JS -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/chartjs.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/dragscrollable.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/dragscroll.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/split.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
    <!--<script src="../assets/js/black-dashboard.min.js?v=1.0.0"></script>-->
    <script src="<?php echo url_for('/assets'); ?>/demo/demo.js"></script>



        <!--   Extraaa   Plugins JS Files   -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/moment-with-locales.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/tempusdominus-bootstrap-4.min.js"></script>
   
  
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/bootstrap-tagsinput.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/swiper.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/bootstrap-slider.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/jquery.validate.js"></script>
    
    <!--  Search Suggestion Plugin    -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/typeahead.bundle.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/bloodhound.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/handlebars.js"></script>
    
    <!-- Chart JS -->
    <script src='<?php echo url_for('/assets/js'); ?>/plugins/snap.svg-min.js'></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/chart.min.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/progressbar.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/d3.v3.min.js"></script>
    
    <!--  Custom JS    -->
    <script src="<?php echo url_for('/assets/js'); ?>/plugins/dropzone.js"></script>
    <script src="<?php echo url_for('/assets/js'); ?>/main.js<?php echo  '?' . option('script_version') ?>"></script>
<?php endif ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-191800673-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-191800673-1');
    </script>

</head>

<body <?php echo isset($style) ? "class='{$style}'" : '' ?>>
<?php /*
<div id="hWrapper">
    <header>
    	<div class="container">
        	<div class="top-nav">
            	<div class="pull-left">
                	<ul>
                    	<!-- <li><a href="<?php //echo url_for('/locations'); ?>">Locations</a></li> -->
                        <li><a href="<?php echo url_for('/faq'); ?>">Faq</a></li>
                        <li><a href="<?php echo url_for('/policies'); ?>">Policies</a></li>
                        <li><a href="<?php echo url_for('/contact-us'); ?>">Contact Us</a></li>
                        <li><a href="<?php echo url_for('/about-us'); ?>">About Us</a></li>
                    </ul>
                </div>
                <div class="pull-right">
                    
                	<ul>
	        
            <li><a href="https://www.wasap.my/+60123965608/nicchris" target="_blank"  style="z-index: 10000;"><i class="fa fa-whatsapp fa-1x"></i></a></li>
            <li><a href="https://www.facebook.com/nicchris.official/" target="_blank"  style="z-index: 10000;"><i class="fa fa-facebook fa-1x"></i></a></li>
           <li><a href="https://www.instagram.com/nicchris.furniture/" target="_blank"  style="z-index: 10000;"><i class="fa fa-instagram fa-1x"></i></a></li>
                    	<!-- <li><a href="<?php //echo url_for('/cart'); ?>">Cart (<span class="cart-total"><?php //echo $cms->cartTotal(); ?></span>)</a></li> -->
                    <?php if($user->logged){ ?>
                    	<li>Welcome, <a href="<?php echo url_for('/account'); ?>"><?php echo ucwords($user->info['firstname'] . ' ' . $user->info['lastname']); ?></a></li>
                        <li><a href="<?php echo url_for('/logout'); ?>">Logout</a></li>
                    <?php }else{ ?>
                    	<li><a href="<?php echo url_for('/login'); ?>">Login</a></li>
                        <li><a href="<?php echo url_for('/register'); ?>">Register</a></li>
                    <?php } ?>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        	<div class="logo"><a href="<?php echo url_for('/'); ?>" class="logo"><img src="<?php echo img($settings['logo']); ?>"  /></a></div>
            <div class="navbar-nicchris">
                <ul class="nav-categories nav navbar-nav">
                	<li class="nav-small"><a href="#">Categories</a></li>
                <?php
                    if($cms->menus()){
                        foreach($cms->menus() as $menu){
                            if(!empty($menu['sub'])){
                                echo '<li class="dropdown' . (!empty($menu['active']) ? ' active' : '') . '">';
                                echo '<a' . (!empty($menu['href']) ? ' href="' . url_for($menu['href']) . '"' : '') . ' class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $menu['title'] . ' <span class="caret"></span></a>';
                                echo '<ul class="dropdown-menu">';
                                    foreach($menu['sub'] as $sub){
                                        echo '<li><a' . (!empty($sub['href']) ? ' href="' . url_for($sub['href']) . '"' : '') . '>' . $sub['title'] . '</a></li>';
                                    }
                                echo '</ul>';
                                echo '</li>';
                            }else{
                                echo '<li' . ($menu['href'] == $path ? ' class="active"' : '') . '><a' . (!empty($menu['href']) ? ' href="' . url_for($menu['href'] ). '"' : '') . '>' . $menu['title'] . '</a></li>';
                            }
                        }
                    }
                ?>    
                </ul>
            </div>
        
        	<?php /*
            <nav class="navbar navbar-default navbar-nicchris">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="<?php echo url_for('/'); ?>" class="logo"><img src="<?php echo img($settings['logo']); ?>" /></a>
                    </div>
                
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">                        
                        <ul class="nav navbar-nav">
						<?php
                            if($cms->menus()){
                                foreach($cms->menus() as $menu){
                                    if(!empty($menu['sub'])){
                                        echo '<li class="dropdown' . (!empty($menu['active']) ? ' active' : '') . '">';
										echo '<a' . (!empty($menu['href']) ? ' href="' . url_for($menu['href']) . '"' : '') . ' class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $menu['title'] . ' <span class="caret"></span></a>';
                                        echo '<ul class="dropdown-menu">';
                                        foreach($menu['sub'] as $sub){
                                            echo '<li><a' . (!empty($sub['href']) ? ' href="' . url_for($sub['href']) . '"' : '') . '>' . $sub['title'] . '</a></li>';
                                        }
                                        echo '</ul>';
                                        echo '</li>';
                                    }else{
                                        echo '<li' . ($menu['href'] == $path ? ' class="active"' : '') . '><a' . (!empty($menu['href']) ? ' href="' . url_for($menu['href'] ). '"' : '') . '>' . $menu['title'] . '</a></li>';
                                    }
                                }
                            }
                        ?>    
                        </ul>
                    </div>
                </div>
            </nav>
            */ /* ?>
            
            
        </div>  
    </header>
</div>
<?php */ ?>
<!--<div id="sWrapper">-->
	<?php echo $content; ?>
<!--</div>-->
<?php /*
<div id="fWrapper">
	<footer>
	    
    	<div class="container">
        	<div class="row">
            	<div class="col-md-3">
                	<h4>Shop By Category</h4>
                    <ul>
                    <?php
						if($cms->menus()){
							foreach($cms->menus() as $menu){
					?>
                    	<li>
                    	    <!-- <a href="<?php //echo $menu['href']; ?>"><?php //echo $menu['title']; ?></a> -->
                    	    <a href="<?php echo url_for($menu['href'] ); ?>"><?php echo $menu['title']; ?></a>
                    	</li>
                    <?php
							}
						}
					?>
                    </ul>
                </div>
                <div class="col-md-3">
                	<h4>About Us</h4>
                    <ul>
                    	<li><a href="<?php echo url_for('/contact-us'); ?>">Contact Us</a></li>
                        <li><a href="<?php echo url_for('/careers'); ?>">Careers</a></li>
                        <!-- <li><a href="<?php //echo url_for('/find-a-store'); ?>">Find A Store</a></li> -->
                    </ul>
                </div>
                <div class="col-md-3">
                	<h4>Help</h4>
                    <ul>
                    	<li><a href="<?php echo url_for('/faq'); ?>">FAQ</a></li>
                        <!--li><a href="<?php echo url_for('/redeem-a-gift-card'); ?>">Redeem A Gift Card</a></li-->
                        <!--li><a href="<?php echo url_for('/store-pick-up'); ?>">Store Pick-Up</a></li-->
                        <li><a href="<?php echo url_for('/shipping-info'); ?>">Shipping Info</a></li>
                        <!--li><a href="<?php echo url_for('/returns'); ?>">Returns</a></li-->
                    </ul>
                </div>
                <div class="col-md-3">
                	<h4>Enquiries</h4>
                    <!--ul>
                    	<li><a href="<?php echo url_for('/commercial-enquiries'); ?>">Commercial Enquiries</a></li>
                        <li><a href="<?php echo url_for('/marketing-and-media-enquiries'); ?>">Marketing & Media Enquiries</a></li>
                    </ul-->
                    <ul>
                    	<li><a href="<?php echo url_for('/contact-us'); ?>">Commercial Enquiries</a></li>
                        <li><a href="<?php echo url_for('/contact-us'); ?>">Marketing & Media Enquiries</a></li>
                    </ul>
                </div>	
            </div>
            <div class="copy">© Copyright <?php echo date('Y'); ?> <?php echo SITE_NAME; ?>. All Right Reserved.</div>
        </div>
	</footer>	
</div>
<?php */ ?>
</body>

<?php if(isset($not_found) && $not_found): ?>
<script>
	$('#feedback-404').on('submit', function(e){
	   e.preventDefault();
	   let form = $(this);
	   let data = form.serialize();
	   form.find('button, textarea').prop('disabled', true);
	   $.ajax({
		  method: 'POST',
		  url: form.attr('action'),
		  data: data
	   }).done(function(response){
	       if( response.status === 's' ) {
               form.addClass('d-none').next().removeClass('d-none');
           }else{
	           alert(response.message);
	       }
           form.find('button, textarea').prop('disabled', false);
	   });
	});
</script>
<?php endif ?>
</html>
