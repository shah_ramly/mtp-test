<?php
/*require __DIR__ . '/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;*/

set_include_path( get_include_path().PATH_SEPARATOR."..");

// create directory if it doesn't exist
if( !is_dir(__DIR__ . '/mass_pymt/') ){
    mkdir(__DIR__ . '/mass_pymt/');
}

if( !is_dir(__DIR__ . '/mass_pymt_mtp/') ){
    mkdir(__DIR__ . '/mass_pymt_mtp/');
}

include_once __DIR__ . "/lib/class/SimpleXLSXGen.php";
include_once __DIR__ . "/lib/config.php";

/*const DB_HOSTNAME = "localhost";
const DB_USERNAME = "root";
const DB_PASSWORD = "";
const DB_DATABASE = "mtp_milestone_staging";

$db = new db(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);*/

$db->query("SELECT p.`id`,
            (SELECT `title` FROM `tasks` WHERE `id` = p.`task_id` LIMIT 1) AS `task_title`,
            p.`task_id`,
            pr.`acc_name`, p.`email`, pr.`bank`, pr.`bank` AS `bank_code`, pr.`acc_num`,  SUM(p.`amount`) AS `amount`,
            p.`type`,
            (SELECT `transaction_reference` FROM `task_payment` WHERE `id` = p.`order_id` LIMIT 1) AS `transaction_reference`,
            (SELECT `transaction_date` FROM `task_payment` WHERE `id` = p.`order_id` LIMIT 1) AS `transaction_date`
            FROM `payment_release` p 
            LEFT JOIN `preference` pr ON (SELECT `id` FROM `members` WHERE `email` = p.`email`) = pr.`member_id`
            WHERE p.`amount` > 0 AND DATE(p.`created_at`) = DATE(NOW())
            GROUP BY p.`order_id`, p.`email`");
$data = $db->getRowList();

$mtp_data = [];
$senangPay_data = [];

foreach($data as $row) {
    $mtp_data[] = $row;
    unset($row['id'], $row['task_title'], $row['task_id'], $row['type'], $row['transaction_reference'], $row['transaction_date']);
    $senangPay_data[] = $row;
}

//echo '<pre>';var_dump($data);die;
if( !empty($data) ) {
    //$writer   = new XLSXWriter();
    $filename = __DIR__ . '/mass_pymt/' . date('Ymd') . ".xlsx";
    $mtp_filename = __DIR__ . '/mass_pymt_mtp/' . date('Ymd') . ".xlsx";
    $keywords = ['Mass', 'Payment', 'MakeTimePay'];
    $title = 'Mass Payment for MakeTimePay';
    $subject = 'Mass Payment on [' . date('Y-m-d') . ']';
    $author = 'MakeTimePay';
    $description = 'This file contains the Mass Payment for MakeTimePay on [' . date('Y-m-d') . ']';

    $banks = [
        'PHBMMYKL' => 'Affin Bank',
        'MFBBMYKL' => 'Alliance Bank',
        'ARBKMYKL' => 'AmBank',
        'BIMBMYKL' => 'Bank Islam',
        'BKRMMYKL' => 'Bank Kerjasama Rakyat Malaysia',
        'BMMBMYKL' => 'Bank Muamalat',
        'BSNAMYK1' => 'Bank Simpanan Nasional',
        'CIBBMYKL' => 'CIMB Bank',
        'HLBBMYKL' => 'Hong Leong Bank',
        'HBMBMYKL' => 'HSBC',
        'KFHOMYKL' => 'Kuwait Finance House Bank',
        'MBBEMYKL' => 'Maybank',
        'OCBCMYKL' => 'OCBC Bank',
        'PBBEMYKL' => 'Public Bank',
        'RHBBMYKL' => 'RHB Bank',
        'SCBLMYKX' => 'Standard Chartered',
        'UOVBMYK' => 'UOB',
    ];

    /*$writer->setTitle($title);
    $writer->setSubject($subject);
    $writer->setAuthor($author);
    $writer->setCompany($author);
    $writer->setKeywords($keywords);
    $writer->setDescription($description);*/

    $header = [
        [
        'Name'               ,// => 'string',
        'Email'              ,// => 'string',
        'Bank Name'          ,// => 'string',
        'E-Banker Code'      ,// => 'string',
        'Bank Account Number',// => 'integer',
        'Amount'             ,// => '#,##0.00',
        ]
    ];

    $mtp_header = [
        [
            'ID',
            'Task Title',
            'Task ID',
            'Name',
            'Email',
            'Bank Name',
            'E-Banker Code',
            'Bank Account Number',
            'Amount',
            'Transaction Type',
            'Transaction Reference',
            'Transaction Date',
        ]
    ];
    //, ['halign'=>'center'], ['halign'=>'center'], ['halign'=>'center'], ['halign'=>'center'], ['halign'=>'center'], ['halign'=>'center']
    /*$writer->writeSheetHeader('Mass Payment', $header, $col_options = ['widths'=>[25,30,22,20,22,15]]);

    foreach($data as $row)
        $writer->writeSheetRow('Mass Payment', $row);

    $writer->writeToFile($filename);*/

    /*$spreadsheet = new Spreadsheet();

    $spreadsheet->getProperties()
        ->setCreator($author)
        ->setLastModifiedBy($author)
        ->setTitle($title)
        ->setSubject($subject)
        ->setDescription($description)
        ->setKeywords(implode(' ', $keywords))
        ->setCategory($title);

    $sheet = $spreadsheet->getActiveSheet();

    $sheet->getColumnDimension('A')->setWidth(25);
    $sheet->getColumnDimension('B')->setWidth(30);
    $sheet->getColumnDimension('C')->setWidth(22);
    $sheet->getColumnDimension('D')->setWidth(20);
    $sheet->getColumnDimension('E')->setWidth(22);
    $sheet->getColumnDimension('F')->setWidth(15);
    $sheet->getStyle('F2:F'.count($data))->getNumberFormat()->setFormatCode('#,##0.00');

    $j=1;
    foreach($header as $x_value) {
        $sheet->setCellValueByColumnAndRow($j,1,$x_value);
        $j=$j+1;
    }

    for($i=0;$i<count($data);$i++){
        $row=$data[$i];
        $j=1;
        foreach($row as $x => $x_value) {
            if( $j === 5 ){
                $sheet->getCellByColumnAndRow($j, $i+2)->setValueExplicit($x_value, 's');
            }else {
                $sheet->setCellValueByColumnAndRow($j, $i + 2, $x_value);
            }
            $j=$j+1;
        }
    }

    $writer = new Xlsx($spreadsheet);
    $writer->save($filename);*/
    //$values = [];
    foreach ($mtp_data as &$row){
        $row['bank'] = $banks[$row['bank']] ?? $row['bank'];
    }

    foreach ($senangPay_data as &$row){
        $row['bank'] = $banks[$row['bank']] ?? $row['bank'];
    }

    $senangPay_book = array_merge($header, $senangPay_data);
    (new SimpleXLSXGen)->addSheet( $senangPay_book, 'Mass Payment')->saveAs($filename);

    $mtp_book = array_merge($mtp_header, $mtp_data);
    (new SimpleXLSXGen)->addSheet( $mtp_book, 'Mass Payment')->saveAs($mtp_filename);

}

exit(0);