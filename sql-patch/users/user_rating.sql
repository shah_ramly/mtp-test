DROP TABLE IF EXISTS `user_rating`;
CREATE TABLE IF NOT EXISTS `user_rating` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `user_id` CHAR(36) NOT NULL COLLATE 'utf8_general_ci',
    `project_id` CHAR(36) NOT NULL COLLATE 'utf8_general_ci',
    `project_type` ENUM('task','job') NOT NULL DEFAULT 'task' COLLATE 'utf8_general_ci',
    `timeliness` DECIMAL(10,1) NOT NULL DEFAULT '0.0',
    `quality` DECIMAL(10,1) NOT NULL DEFAULT '0.0',
    `communication` DECIMAL(10,1) NOT NULL DEFAULT '0.0',
    `responsiveness` DECIMAL(10,1) NOT NULL DEFAULT '0.0',
    `review` MEDIUMTEXT NOT NULL COLLATE 'utf8_unicode_ci',
    `author_id` CHAR(36) NOT NULL COMMENT 'This would contain the user_id whom wrote the review' COLLATE 'utf8_unicode_ci',
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET='utf8' COLLATE='utf8_unicode_ci';
COMMIT;
