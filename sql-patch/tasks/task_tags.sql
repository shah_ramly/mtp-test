CREATE TABLE IF NOT EXISTS `task_tags` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_tags_task_id_tag_unique` (`task_id`,`tag`),
  KEY `task_tags_task_id_index` (`task_id`),
  KEY `task_tags_tag_index` (`tag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
