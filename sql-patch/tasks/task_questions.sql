CREATE TABLE IF NOT EXISTS `task_questions` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_questions_id_task_id_unique` (`id`,`task_id`),
  KEY `task_questions_task_id_index` (`task_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;