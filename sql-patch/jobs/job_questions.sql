CREATE TABLE IF NOT EXISTS `job_questions` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `job_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `job_questions_id_job_id_unique` (`id`,`job_id`),
  KEY `job_questions_job_id_index` (`job_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
