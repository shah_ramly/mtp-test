CREATE TABLE IF NOT EXISTS `answers` (
	`task_id` CHAR(36) NOT NULL COLLATE 'utf8_unicode_ci',
    `question_id` CHAR(36) NOT NULL COLLATE 'utf8_unicode_ci',
    `user_id` INT(11) NOT NULL,
    `answer` MEDIUMTEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`type` CHAR(10) NOT NULL DEFAULT 'task' COLLATE 'utf8_unicode_ci',
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`task_id`, `question_id`) USING BTREE,
    INDEX `FK_question` (`question_id`) USING BTREE,
    INDEX `FK_user` (`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
