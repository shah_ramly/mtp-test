CREATE TABLE IF NOT EXISTS `favourites` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` CHAR(36) NOT NULL COLLATE 'utf8_unicode_ci',
	`task_id` CHAR(36) NOT NULL COLLATE 'utf8_unicode_ci',
	`type` CHAR(10) NOT NULL DEFAULT 'task' COLLATE 'utf8_unicode_ci',
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`) USING BTREE
)ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;