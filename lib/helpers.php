<?php
require_once 'limonade_security.php';
function request($field = ''){

    if($_REQUEST){
        return $_REQUEST[$field] ?? null;
    }else{
        $params = json_decode(file_get_contents("php://input"), true);
        return $params[$field] ?? null;
    }
    return null;

}

function session($key, $value = null, $unset = false){

    if( $unset ){
        if( isset(env()['SESSION'][$key]) ) unset( env()['SESSION'][$key] );
    }elseif( is_null($value) ){
        return isset( env()['SESSION'][$key] ) ? env()['SESSION'][$key] : null;
    }else{
        env()['SESSION'][$key] = $value;
    }
}

function set_latest_url($url = null){
    if( ! isset($url) ) return;

    env()['SESSION']['latest_url'] = $url;
}

function get_latest_url(){
    return env()['SESSION']['latest_url'];
}

function validate($fields, $rules = [], $messages = []){
    $all_fields = [];
    if(is_array($fields)){
        foreach ($fields as $name => $value){
            if($name === 'form_token') continue;
            if(is_array($value)){
                foreach ($value as $k => $v){
                    $all_fields[$name.'.'.$k] = $v;
                }
            }else {
                $all_fields[$name] = $value;
            }
        }
    }

    $result_messages = [];

    foreach ($rules as $field => $rule){
        if($rule === 'required' && (!isset($all_fields[$field]) || empty($all_fields[$field]) ) ){
            $name = explode('.', $field);
            $name = end($name);
            $result_messages[$name] = $messages[$field.'.required'];
        }
    }

    return ['errors' => $result_messages];
}

function hash_name($name = null){
    if(null === $name) return '';

    $keys   = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $values = '12345678998765432100123456';
    $array  = array_combine(str_split($keys), str_split($values));
    $digits = '';
    $name   = str_replace(' ', '', $name);
    if(strlen($name) < 8)
        $name .= $name;

    $name = strtoupper(substr($name, 0, 8));
    for($i=0; $i<strlen($name); $i++){
        $digits .= ! is_numeric($name[$i]) ? $array[$name[$i]] : $name[$i];
    }

    return "#{$digits}";
}

function plural( $amount, $singular = '', $plural = 's' ) {
    return ( (int)$amount === 1 ) ? $singular : $plural;
}

if( ! function_exists('dd') ) {
    function dd(...$vars)
    {
        foreach ($vars as $v) {
            var_dump($v);
        }

        exit(1);
    }
}

function rip_tags($string) {

    // ----- remove HTML TAGs -----
    $string = preg_replace ('/<[^>]*>/', ' ', $string);

    // ----- remove control characters -----
    $string = str_replace("\r", '', $string);    // --- replace with empty space
    $string = str_replace("\n", ' ', $string);   // --- replace with space
    $string = str_replace("\t", ' ', $string);   // --- replace with space

    // ----- remove multiple spaces -----
    $string = trim(preg_replace('/ {2,}/', ' ', $string));
    $string = trim(preg_replace('/&nbsp;/', ' ', $string));

    return $string;

}

function dateRangeToDays($date_start = '', $date_end = '', $showHours = false){
    if(!$date_end || $date_end == NULL || $date_end == '0000-00-00') $date_end = date('Y-m-d');
    if($date_start && $date_end){
        $diff = abs(strtotime($date_end) - strtotime($date_start));
        
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

        $text = '';

        if( $showHours ) {
            $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));

            if ($hours) {
                $text = $hours . ' ' . ($hours > 1 ? lang('hours') : lang('hour')) . ' ' . $text;
            }
        }

        if($days){
            $text = $days .' '. ($days > 1 ? lang('days') : lang('day')) .' '. $text;
        }

        if($months){
            $text = $months .' '. ($months > 1 ? lang('months') : lang('month')) .' '. $text;
        }

        if($years){
            $text = $years .' '. ($years > 1 ? lang('years') : lang('year')) .' '. $text;
        }
        
        return $text;
    }
}

function format_percentage($val){
    return fmod($val, 1) !== 0.0 ? number_format($val, 1) : number_format($val);
}

function log_debug($message, array $data, $logFile = "debug.log"){

    foreach ($data as $key => $val) {
        if( is_array($val) )
            $val = '(' . implode(',', $val) . ')';
        $message = str_replace("%{$key}%", $val, $message);
    }

    $message .= PHP_EOL;

    return file_put_contents($logFile, $message, FILE_APPEND);
}