<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require_once __DIR__ . '/../../vendor/autoload.php';

class Mail{

    public $from = 'MakeTimePay';
    private $API_KEY = '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';
    private $email;
    private $language;
    private $sendGrid;
    private $db;

    /**
     * Mail constructor.
     * @var $db db
     */
    public function __construct($db){
        $this->db       = $db;
        $this->sendGrid = new SendGrid($this->API_KEY);
        $this->email    = new SendGrid\Mail\Mail();
        $lang_session   = WEBSITE_PREFIX . 'LANGUAGE';
        $this->language = isset($_SESSION[$lang_session]) ? strtolower($_SESSION[$lang_session]) : 'en';
    }

    public function sendGrid($to, $subject, $template = '', $data = []){

        $body = partial("emails/{$this->language}/{$template}.php", $data);
        $this->email->setFrom('noreply@email.maketimepay.com', $this->from);
        $this->email->setSubject($subject);
        $this->email->addTo($to['email'], $to['name']);
        $this->email->addContent('text/html', $body);

        try {
            $response = $this->sendGrid->send($this->email);
            return $response;
        } catch (Exception $e) {}

    }

    /**
     * @param array $to ['email' => some@email.com, 'name' => John]
     * @param string $action please refer to getTemplate() function
     * @param array $data array of data that's needed inside $template file
     * @param string $language i.e. 'en'
     */
    public function sendmail($to = [], $action = '', $data = [], $language = '')
    {
        $allowed_emails = [
            'activate-your-account',
            'password-changed',
            'account-successfully-created',
            'reset-password',
            'you-received-question',
            'you-received-answer',
            'you-have-applied-for-task',
            'new-task-application',
            'you-have-been-appointed',
            'seeker-has-accepted-task',
            'you-received-chat-message',
            'seeker-escrow-notification',
            'poster-accepted-task',
            'poster-marked-task-completed',
            'poster-marking-task-completed',
            'seeker-marking-task-completed',
            'error-404',
            'task-publish',
        ];

        if( !in_array($action, $allowed_emails) )
            return true;

        $template = $this->getTemplate($action);

        if ($template) {
            $subject = $data['subject'] ?? $template['subject'];
            $template_file = $template['template'];
            $language = !empty($language) ? $language : $this->language;
            $body = partial("emails/{$language}/{$template_file}.php", $data);

            try {
                $mail = new PHPMailer(true);
                $mail->isSMTP();
                $mail->Host       = 'email.maketimepay.com';
                $mail->SMTPAuth   = true;
                $mail->Username   = 'noreply@email.maketimepay.com';
                $mail->Password   = '#GE*H%NDy^V[;?Vq*v';
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
                $mail->Port       = 465;

                $mail->setFrom('noreply@maketimepay.com', 'MakeTimePay');
                $mail->addAddress($to['email'], $to['name']);     //Add a recipient

                $mail->isHTML(true);
                $mail->Subject = $subject;
                $mail->Body    = $body;

                $mail->send();
            } catch (Exception $e) {}
        }
    }

    public function schedule($data = [], $action = '', $type = 'notification'){

        $allowed_emails = [
            'activate-your-account',
            'password-changed',
            'account-successfully-created',
            'reset-password',
            'you-received-question',
            'you-received-answer',
            'new-task-application',
            'you-have-been-appointed',
            'seeker-has-accepted-task',
            'you-received-chat-message',
            'you-have-applied-for-task',
            'seeker-escrow-notification',
            'poster-accepted-task',
            'poster-marked-task-completed',
            'poster-marking-task-completed',
            'seeker-marking-task-completed',
        ];

        if( empty($data) || empty($action) || !in_array($action, $allowed_emails) ) return;

        $schedule = isset($data['data']['schedule']) ? $data['data']['schedule'] : \Carbon\Carbon::now()->toDateTimeString();
        if( isset($data['data']['schedule']) ) unset($data['data']['schedule']);

        $this->db->table('schedule');
        $this->db->insertArray([
            'type' => $type,
            'action' => $action,
            'data' => json_encode($data),
            'scheduled_at' => $schedule,
        ]);
        $this->db->insert();

        return $this->db->insertid();
    }

    /**
     * @param array $data ['email' => some@email.com, 'slug' => task-slug, ...]
     * @param string $action please refer to getTemplate() function
     * @param string $type 'email' or 'notification', default 'notification'
     * @return bool TRUE or FALSE
     */
    public function isScheduled($data = [], $action = '', $type = 'notification'){

        if( empty($data) || empty($action) ) return false;

        $keys = array_keys($data);
        $query = "SELECT COUNT(`id`) AS `count` FROM `schedule`
                  WHERE `type` = '{$type}' AND `action` = '{$action}'";

        foreach ($keys as $key){
            $source = $key === 'email' ? '$.to' : '$.data';

            if( $key === 'slug' )
                $query .= " AND JSON_CONTAINS(`data`, '{\"task\":{\"{$key}\": \"{$data[$key]}\"}}', '{$source}')";
            else
                $query .= " AND JSON_CONTAINS(`data`, '{\"{$key}\": \"{$data[$key]}\"}', '{$source}')";
        }

        return $this->db->getValue($query) > 0;
    }

    protected function getTemplate($trigger){
        return [
               'activate-your-account'         => ['template' => 'activate-your-account', 'subject' => 'Welcome to MakeTimePay!'],
               'account-successfully-created'  => ['template' => 'account-successfully-created', 'subject' => 'MakeTimePay Account Successfully Created'],
               'reset-password'                => ['template' => 'reset-password', 'subject' => 'MakeTimePay Reset Password'],
               'password-changed'              => ['template' => 'successful-password-change', 'subject' => 'Successfully Change Password'],
               'complete-profile-reminder-1'   => ['template' => 'complete-profile-reminder-1', 'subject' => 'Complete Your Profile Reminder'],
               'complete-profile-reminder-2'   => ['template' => 'complete-profile-reminder-2', 'subject' => 'Complete Your Profile Reminder'],
               'task-skill-matching'           => ['template' => 'task-skill-matching', 'subject' => 'Opportunities that match your skills'],
               'expand-your-capabilities'      => ['template' => 'expand-your-capabilities', 'subject' => 'Expand your capabilities'],
               'urgent-task-notification'      => ['template' => 'urgent-task-notification', 'subject' => 'New Urgent Tasks'],
               'seeker-posted-question'        => ['template' => 'seeker-posted-question', 'subject' => 'You have posted a question'],
               'seeker-has-accepted-task'      => ['template' => 'seeker-has-accepted-task', 'subject' => 'Seeker has accepted your task'],
               'you-received-question'         => ['template' => 'you-received-a-question', 'subject' => 'You have received a question'],
               'you-received-answer'           => ['template' => 'you-received-an-answer', 'subject' => 'You received a response to your question'],
               'you-received-chat-message'     => ['template' => 'you-received-chat-message', 'subject' => 'You received new chat message'],
               'new-task-application'          => ['template' => 'new-task-application', 'subject' => 'New Task Application'],
               'you-have-applied-for-task'     => ['template' => 'you-have-applied-for-task', 'subject' => 'You have applied for task'],
               'new-job-application'           => ['template' => 'new-job-application', 'subject' => 'New Job Application'],
               'you-have-been-appointed'       => ['template' => 'you-have-been-appointed', 'subject' => 'You have been appointed'],
               'poster-escrow-notification'    => ['template' => 'poster-escrow-notification', 'subject' => 'Escrow Notification'],
               'poster-payment-notification'   => ['template' => 'poster-payment-notification', 'subject' => 'Payment Notification'],
               'seeker-escrow-notification'    => ['template' => 'seeker-escrow-notification', 'subject' => 'Escrow Notification'],
               'seeker-payment-notification'   => ['template' => 'seeker-payment-notification', 'subject' => 'Payment Notification'],
               'poster-accepted-task'          => ['template' => 'poster-accepted-task', 'subject' => 'Congratulations! The task you marked as completed, has been accepted by the Poster'],
               'poster-marked-task-completed'  => ['template' => 'poster-marked-task-completed', 'subject' => 'Congratulations! Poster has marked the task as Completed'],
               'poster-marking-task-completed' => ['template' => 'poster-marking-task-completed', 'subject' => 'You are marking task as COMPLETED'],
               'seeker-marking-task-completed' => ['template' => 'seeker-marking-task-completed', 'subject' => 'You are marking task as COMPLETED'],
               'feedback-submission'           => ['template' => 'feedback-submission-inside-system', 'subject' => 'Feedback Submission'],
               'error-404'                     => ['template' => 'error-404', 'subject' => 'MakeTimePay: Feedback from 404 Page'],
               'task-publish'                  => ['template' => 'task-publish', 'subject' => 'Your task has been successfully published'],
           ][$trigger] ?? null;
    }

}