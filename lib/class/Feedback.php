<?php

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../helpers.php';

use Carbon\Carbon;

class Feedback
{
    public $db;
    public $user;
    public $date;

    /**
     * @var $db db
     * @var $user user
     * @return Feedback
     */
    public static function init($db, $user){
        return new static($db, $user);
    }
    /**
     * @var $db db
     * @var $user user
     */
    public function __construct($db, $user){

        if( isset($_SESSION['member_id']) && !empty($_SESSION['member_id']) ){
            $_SESSION[WEBSITE_PREFIX.'USER_ID'] = $_SESSION['member_id'];
        }

        $this->db = $db;
        $this->user = $user;
        date_default_timezone_set('Asia/Kuala_Lumpur');

        if( !isset($_SESSION[WEBSITE_PREFIX.'LANGUAGE']) || $_SESSION[WEBSITE_PREFIX.'LANGUAGE'] == 'EN' ){
            $locale = ['English', 'en_GB'];
        }else{
            $locale = ['Malay', 'ms_MY'];
        }

        setLocale(LC_TIME, $locale[1]);
        Carbon::setLocale($locale[1]);

        $this->date = Carbon::today();
    }

    private function is_logged_in(){
        if((!isset($this->user->info['email']) || empty($this->user->info['email'])) &&
            (!isset($this->user->info['status']) || $this->user->info['status'] !== '1')) {
            //halt(HTTP_UNAUTHORIZED, "This area is for logged in users!");
        }
        return true;
    }

    public function feedbacks($update = false){
        $this->is_logged_in();
        $user_id = $this->user->info['id'];
        $per_page = 4;
        $page = 1;

        if( isset( $_SESSION['feedbacks'] ) && (unserialize($_SESSION['feedbacks']) instanceof pagination) && ! $update ) return unserialize($_SESSION['feedbacks']);

        $this->db->query("SELECT * FROM `feedback` WHERE `user_id` = '{$user_id}' ORDER BY `created_at` DESC");
        $all_feedbacks = $this->db->getRowList();

        $feedbacks = array_map(function ($feedback) {
            return [
                'id' => $feedback['id'],
                'area' => $feedback['area'],
                'feedback' => $feedback['feedback'],
                'data' => $feedback['data'],
                'date' => Carbon::parse($feedback['created_at'])->format('d-M-Y'),
                'time' => Carbon::parse($feedback['created_at'])->format('H:i:s'),
                'attachments' => array_map(function($attachment){
                                    $attachment = (array) $attachment;
                                    return ['name' => $attachment['name'], 'file' => $attachment['file']];
                                }, json_decode($feedback['attachments']))
            ];
        }, $all_feedbacks);

        $pagination = new pagination($feedbacks, $per_page, $page, option('site_uri') . url_for('/feedback/'), '/');
        $_SESSION['feedbacks'] = serialize($pagination);
        return $pagination;
    }

    public function paginate($page){
        $feedback = unserialize($_SESSION['feedbacks']) ?? $this->feedbacks();
        $feedback->setStartData($page);
        echo partial('partial/feedback-table.html.php', ['feedbacks' => $feedback]);
    }

    public function save(){
        if(lemon_csrf_require_valid_token()){
            $area = sanitize( request('feedback_area') );
            $feedback = sanitize( request('feedback') );
            $report = sanitize( request('report') );

            if ( isset($area, $feedback) && !empty(trim($feedback)) ){

                if( $report ){
                    $report = $area === 'Report User' ? ['member_id' => $report] : ['post_id' => $report];
                }elseif( request('data') ){
                    $report = json_decode(request('data'), true);
                }

                $record = [
                    'user_id' => $this->user->info['id'] ?? 0,
                    'area' => mysqli_escape_string($this->db->connection, $area),
                    'feedback' => mysqli_escape_string($this->db->connection, $feedback),
                    'data' => !empty($report) ? json_encode($report) : null
                ];

                $record = array_filter($record);

                $attachments = request('attachments');

                if( !is_null($attachments) && !empty($attachments) ){
                    $new_attachments = array_map(function ($attach){
                        $parts = explode('|', $attach);
                        return ['name' => $parts[0], 'file' => $parts[1]];
                    }, $attachments);

                    if( isset($new_attachments) && !empty($new_attachments) ) {
                        $new_attachments       = json_encode(array_values($new_attachments));
                        $record['attachments'] = $new_attachments;
                    }
                }

                $this->db->table('feedback');
                $this->db->insertArray($record);
                $this->db->insert();

                $feedback = $this->feedbacks(true);
                $feedback->setStartData(1);
                $the_feedback = partial('partial/feedback-table.html.php', ['feedbacks' => $feedback]);

                echo json(['status' => 's', 'message' => lang('thank_you_for_your_feedback'), 'feedback' => $the_feedback]);

                if( $this->user->info['id'] ) {
                    $name = $this->user->info['type'] === '0' ? $this->user->info['firstname'] : $this->user->info['company']['name'];

                    (new Mail($this->db))->sendmail(
                        [
                            'email' => 'query@maketimepay.com',
                            'name'  => $name
                        ],
                        /*'feedback-submission',
                        [
                            'firstName' => $name,
                        ],*/
                        'error-404',
                        [
                            'record'    => $record,
                        ]
                    );
                }

            }else{
                echo json(['status' => 'e', 'message' => lang('area_and_feedback_fields_are_required')]);
            }
        }else{
            echo json(['status' => 'e', 'message' => lang('invalid_action')]);
        }
    }

    public function upload_attachment(){
        $this->is_logged_in();
        $allowed_files = ['jpg','jpeg','png','doc','docx','pdf', 'ppt','pptx', 'csv', 'xls', 'xlsx'];
        $user_id = $this->user->info['id'];
        if(lemon_csrf_require_valid_token()) {
            if( empty($_FILES) || !isset($_FILES['file']) )
            {
                echo lang('no_file_uploaded');
                exit;
            }

            $name     = $_FILES['file']['name'];
            $tmpName  = $_FILES['file']['tmp_name'];
            $type     = $_FILES['file']['type'];
            $error    = $_FILES['file']['error'];
            $size     = $_FILES['file']['size'];
            $ext      = strtolower(pathinfo($name, PATHINFO_EXTENSION));
            $fileName = preg_replace('#[\s]#i', '_', preg_replace('#[^a-z.0-9\-\s]#i', '_', $name));
            $status   = 'error';

            switch ($error) {
                case UPLOAD_ERR_OK:
                    $valid = true;
                    //validate file extensions
                    if ( !in_array($ext, $allowed_files) ) {
                        $valid = false;
                        $message = lang('invalid_file_extension');
                    }
                    //validate file size
                    if ( $size/1024/1024 > 5 ) {
                        $valid = false;
                        $message = lang('file_size_is_exceeding_maximum_allowed_size');
                    }
                    //upload file
                    if ($valid) {
                        //create a user specific foler with user ID
                        if(!is_dir(UPLOAD_DIR . 'feedback' . DIRECTORY_SEPARATOR . $user_id)){
                            mkdir(UPLOAD_DIR . 'feedback' . DIRECTORY_SEPARATOR . $user_id, 0755, true);
                        }
                        $name = $fileName;
                        $new_name = md5($fileName . microtime()) . '.' . $ext;
                        $targetPath =  UPLOAD_DIR . 'feedback' . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR . $new_name;
                        @move_uploaded_file($tmpName, $targetPath);
                        $message = "{$name}|{$new_name}";
                        $status = 'success';
                    }
                    break;
                case UPLOAD_ERR_INI_SIZE:
                    $message = lang('file_size_is_exceeding_maximum_allowed_size');
                    break;
                case UPLOAD_ERR_PARTIAL:
                    $message = lang('uploaded_file_was_only_partially_uploaded');
                    break;
                case UPLOAD_ERR_NO_FILE:
                    $message = lang('no_file_uploaded');
                    break;
                case UPLOAD_ERR_NO_TMP_DIR:
                    $message = lang('failed_to_upload_file_please_try_again');
                    break;
                case UPLOAD_ERR_CANT_WRITE:
                    $message = lang('failed_to_upload_file_please_try_again');
                    break;
                default:
                    $message = lang('failed_to_upload_file_please_try_again');
                    break;
            }

            echo json_encode(['status' => $status, 'message' => $message]);

        }
    }

    public function download_attachment(){
        $this->is_logged_in();

        if( is_null( params('feedback') ) && is_null(params('file')) ) {
            echo json(['status' => 'e', 'message' => lang('file_does_not_exist')]);
            exit;
        }

        $feedback = sanitizeItem(params('feedback'));
        $file     = sanitizeItem(params('file'));
        $user_id  = $this->user->info['id'];
        $the_file = UPLOAD_DIR . 'feedback' . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR . $file;

        if(file_exists($the_file)) {

            $file_name = $this->get_file_name($file, $feedback) ?? $the_file;

            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=\"".$file_name."\"");
            header("Content-Transfer-Encoding: chunked");
            header("Expires: 0");
            header("Cache-Control: private, must-revalidate, post-check=0, pre-check=0");
            header("Pragma: public");

            readfile("{$the_file}");
        }

        exit;
    }

    private function get_file_name($file = null, $feedback = null){
        if( !is_null($file) && !is_null($feedback) ){
            $query = "SELECT 
                        @path_to_name := JSON_UNQUOTE(JSON_SEARCH(`attachments`, 'one', '{$file}', NULL, '$[*].file')) AS `path_to_name`,
                        @path_to_parent := TRIM(TRAILING '.file' from @path_to_name) AS `path_to_parent`,
                        @file_object := JSON_EXTRACT(`attachments`, @path_to_parent) AS `file_object`,
                        JSON_UNQUOTE(JSON_EXTRACT(@file_object, '$.name')) AS `file_name`
                      FROM `feedback` 
                      WHERE `id` = '{$feedback}' LIMIT 1";
            $this->db->query($query);
            $result = $this->db->getSingleRow();
            return !empty($result) && isset($result['file_name']) ? $result['file_name'] : null;
        }
    }

    public function delete_attachment(){
        $this->is_logged_in();

        if( is_null( params('feedback') ) && is_null(params('file')) && !request('file_name') ) {
            echo json(['status' => 'failed', 'message' => lang('file_does_not_exist')]);
        }

        $feedback = sanitizeItem(params('feedback'));
        $file_id = sanitizeItem(params('file'));
        $file_name = sanitizeItem(request('file_name'));
        $user_id = $this->user->info['id'];

        if( strpos($file_name, '|') !== false ) $file_name = explode('|', $file_name)[1];

        if( isset($file_name) ){
            $file = UPLOAD_DIR . 'feedback' . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR . $file_name;
            if( file_exists($file) ) {
                @unlink($file);
                echo json(['status' => 's', 'message' => lang('file_was_deleted_successfully')]);
            }else{
                echo json(['status' => 'e', 'message' => lang('failed_to_delete_file_please_try_again')]);
            }
            exit;
        }
    }
}