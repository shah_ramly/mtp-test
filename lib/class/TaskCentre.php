<?php

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../helpers.php';
require_once __DIR__ . '/Milestone.php';

use Carbon\Carbon;
use Spipu\Html2Pdf\Html2Pdf;

class TaskCentre
{
    protected $db;
    protected $user;
    protected $date;

    /**
     * @var $db db
     * @var $user user
     */
    public function __construct($db, $user){
        $this->db = $db;
        $this->site_host = (!empty($_SERVER['HTTPS']) ? 'https://' : 'http://') . ($_SERVER['HTTP_HOST'] ?? '');

        if( isset($_SESSION['member_id']) && !empty($_SESSION['member_id']) ){
            $_SESSION[WEBSITE_PREFIX.'USER_ID'] = $_SESSION['member_id'];
        }

        $this->user = $user;
        date_default_timezone_set('Asia/Kuala_Lumpur');

        if( !isset($_SESSION[WEBSITE_PREFIX.'LANGUAGE']) || $_SESSION[WEBSITE_PREFIX.'LANGUAGE'] == 'EN' ){
            $locale = ['English', 'en_GB'];
        }else{
            $locale = ['Malay', 'ms_MY'];
        }

        setLocale(LC_TIME, $locale[1]);
        Carbon::setLocale($locale[1]);
        $this->date = Carbon::today();
    }

    private function is_logged_in(){
        if((!isset($this->user->info['email']) || empty($this->user->info['email'])) &&
            (!isset($this->user->info['status']) || $this->user->info['status'] !== '1')) {
            halt(HTTP_UNAUTHORIZED, "This area is for logged in users!");
        }
        return true;
    }

    private function is_ajax(){
        return 'xmlhttprequest' === strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '' );
    }

    public function set_countries(){
        return $this->getCountries();
    }

    protected function set_date(){

        $year  = params('year') ? (int)sanitize(params('year'), 'int') : Carbon::now()->year;
        $month = params('month') ? (int)sanitize(params('month'), 'int') : Carbon::now()->month;
        $day   = params('day') ? (int)sanitize(params('day'), 'int') : Carbon::now()->day;
        $timestamp = strtotime("{$year}-{$month}-{$day}");
        $this->date->timestamp( $timestamp );
        if( $this->date->month !== $month ){
            $this->date->month($month)->endOfMonth();
        }
    }

    public function get_date(){
        $this->set_date();
        return $this->date;
    }

    public function task_centre(){
        set_latest_url( 'task-centre' );
        $this->is_logged_in();

        [$type, $total_active, $total_inactive] = $this->getTasksList();
        $this->overall_notifications();

        set('current_tab', $type === 'day' ? '#tcInProgressDay' : ($type === 'week' ? '#tcInProgressWeek' : '#tcInProgressMonth'));

        if( $type === 'index' ){
            set('current_tab', '#tcAll');
        }

        return render('workspace/index.html.php', 'layout/app.html.php');
    }

    public function ajaxGetTasksList(){
        $this->is_logged_in();
        if( ! $this->is_ajax() || request_method() !== 'POST' || !lemon_csrf_require_valid_token('invalid token', lemon_csrf_token('tasks_filter_token')) ) return;

        $tab     = mysqli_real_escape_string($this->db->connection, request('tab'));
        $filter  = mysqli_real_escape_string($this->db->connection, request('filter'));
        $tasksStatus = mysqli_real_escape_string($this->db->connection, request('section'));
        $order = mysqli_real_escape_string($this->db->connection, request('order'));
        $page = (int) mysqli_real_escape_string($this->db->connection, request('page')) ?? 1;

        if( !in_array($order, ['latest', 'value_high_to_low', 'value_low_to_high', 'applicants_high_to_low', 'applicants_low_to_high']) ){
            $order = 'latest';
        }

        session("{$tab}_{$tasksStatus}_filtered", true);
        session("{$tab}_{$tasksStatus}_filter", $filter);
        session("{$tab}_{$tasksStatus}_order", $order);
        session("current_request_filter", "{$tab}_{$tasksStatus}");

        set('shownTab', $tab);
        set('selectedSection', $tasksStatus);

        [$type, $total_posted_active, $total_posted_inactive, $total_applied_active, $total_applied_inactive] = $this->getTasksList($page);
        return json([
            'html' => partial('workspace/partial/task-centre/list-view-ajax.html.php'),
            'total_active' => $tab === 'posted' ? $total_posted_active : $total_applied_active,
            'total_inactive' => $tab === 'posted' ? $total_posted_inactive : $total_applied_inactive,
        ]);
    }

    private function getQuarterMonths( $date )
    {
        $quarter = empty($date) ? $this->date : Carbon::parse($date);

        $start  = $quarter->copy()->startOfQuarter();
        $middle = $start->copy()->addMonth();
        $end    = $quarter->copy()->endOfQuarter();

        $firstMonth = [
            'month' => $start->month,
            'name'  => $start->shortLocaleMonth,
            'days'  => $start->daysInMonth
        ];

        $secondMonth = [
            'month' => $middle->month,
            'name'  => $middle->shortLocaleMonth,
            'days'  => $middle->daysInMonth
        ];

        $thirdMonth = [
            'month' => $end->month,
            'name'  => $end->shortLocaleMonth,
            'days'  => $end->daysInMonth
        ];

        return [$firstMonth, $secondMonth, $thirdMonth];
    }

    private function prepareDate(string $type = 'index')
    {
        $this->set_date();

        $date            = $this->date->copy();
        $prev            = $date->copy()->subMonth();
        $next            = $date->copy()->addMonth();
        $monthsInQuarter = $this->getQuarterMonths($date);

        $sub_url = $type !== 'index' ? "/{$type}" : '';
        $url_hash = ($sub_url === '') ? '#tcInProgressMonth' :
            ($sub_url === '/week' ? '#tcInProgressWeek' : '#tcInProgressDay');
        $data = [
            'date'               => $date,
            'current_day'        => $date->day,
            'current_month'      => $date->month,
            'current_year'       => $date->year,
            'current_quarter'    => $date->quarter,
            'current_month_name' => $date->localeMonth,
            'next_month_name'    => $date->copy()->addMonth()->localeMonth,
            'prev_month_name'    => $date->copy()->subMonth()->localeMonth,
            'current_date'       => $date->format('d M Y'),
            'quarter'            => $monthsInQuarter,
            'curr_day'           => url_for("workspace/day", $date->year, $date->month, $date->day),
            'curr_month'         => url_for("workspace/week", $date->year, $date->month),
            'curr_year'          => url_for("workspace/month", $date->year, $date->month),
            'next_day'           => url_for("workspace{$sub_url}", $date->copy()->addDay()->year, $date->copy()->addDay()->month, $date->copy()->addDay()->day . $url_hash),
            'prev_day'           => url_for("workspace{$sub_url}", $date->copy()->subDay()->year, $date->copy()->subDay()->month, $date->copy()->subDay()->day . $url_hash),
            'next_month'         => $date->copy()->addMonth()->month,//url_for("workspace{$sub_url}", $next->year, $next->startOfMonth()->month . $url_hash),
            'prev_month'         => $date->copy()->subMonth()->month,//url_for("workspace{$sub_url}", $prev->year, $prev->startOfMonth()->month . $url_hash),
            'next_year'          => url_for("workspace{$sub_url}", $date->copy()->startOfYear()->addYear()->year, $date->month . $url_hash),
            'prev_year'          => url_for("workspace{$sub_url}", $date->copy()->startOfYear()->subYear()->year, $date->month . $url_hash),
            'next_quarter'       => url_for("workspace{$sub_url}", $date->copy()->startOfQuarter()->addQuarter()->year, $date->copy()->startOfQuarter()->addQuarter()->month . $url_hash),
            'prev_quarter'       => url_for("workspace{$sub_url}", $date->copy()->startOfQuarter()->subQuarter()->year, $date->copy()->startOfQuarter()->subQuarter()->month . $url_hash),
        ];

        return $data;
    }

    public function getCountries(){
        if( ! isset($_SESSION['countries']) ) {
            $this->db->query("SELECT `id`, `name` FROM `countries`  ORDER BY `name`");
            $countries = $this->db->getRowList();
            $_SESSION['countries'] = array_column($countries, null, 'id');
        }

        return $_SESSION['countries'];
    }

    public function getCountryName($id){
        if( ! isset($_SESSION['countries']) ){
            $this->getCountries();
        }

       return is_numeric($id) ? $_SESSION['countries'][$id]['name'] : $id;
    }

    public function getStates($country_id = null, $return = false){
        $country_id = $country_id ?? params('country_id');
        if(null === $country_id) {
            $country_id = 132; // Malaysia //return $this->is_ajax() ? json_encode([]) : [];
        }

        if( ! isset($_SESSION['states_' . $country_id]) ) {
            $this->db->query("SELECT `id`, `name` FROM `states` WHERE `country_id` = {$this->db->escape($country_id)}  ORDER BY `name`");
            $_SESSION['states_' . $country_id] = $this->db->getRowList();
        }

        $states = $_SESSION['states_' . $country_id];

        if($this->is_ajax() && !$return) echo json_encode($states);
        elseif($this->is_ajax() && $return) return json($states);
        else return $states;
    }

    public function getCities($state_id = null, $return = false){
        $state_id = $state_id ?? params('state_id');
        if(null === $state_id) {
            return $this->is_ajax() ? json_encode([]) : [];
        }

        if( ! isset($_SESSION['cities_' . $state_id])) {
            $this->db->query("SELECT `id`, `name`, `state_id` FROM `cities` WHERE `state_id` = {$this->db->escape($state_id)}  ORDER BY `name`");
            $_SESSION['cities_' . $state_id] = $this->db->getRowList();
        }

        $cities = $_SESSION['cities_' . $state_id];

        if($this->is_ajax() && !$return) echo json_encode($cities);
        elseif($this->is_ajax() && $return) return json_encode($cities);
        else return $cities;
    }

    public function getCategories($categories = [], $table = 'category', $return = false){
        $categories = !empty($categories) ? $categories : params('categories');
        if(!is_array($categories)) $categories = explode(',', $categories);
        if(!is_null($categories)) $categories = implode("','", $categories);

        $query = "SELECT `id`, `title` AS `name`, `parent` FROM `{$table}` WHERE ";

        if(!is_null($categories) && $categories !== '')
            $query .= " `id` IN('{$categories}')  ";
        else
            $query .= " `parent` = '0' ";

        $query .= " ORDER BY `id`";
        $session_query = base64_encode($query);
        if( ! isset($_SESSION['categories_' . $session_query]) ) {
            $this->db->query($query);
            $_SESSION['categories_' . $session_query] = $this->db->getRowList();
        }
        //if($this->is_ajax() && !$return) echo json_encode($this->db->getRowList());
        //elseif($this->is_ajax() && $return) return json_encode($this->db->getRowList());
        return $_SESSION['categories_' . $session_query];
    }

    public function getStateName($id){
        $this->db->table('states');
        $this->db->query("SELECT `name` FROM `states` WHERE `id` = " . $this->db->escape($id));
        return $this->db->getValue();
    }

    public function getCityName($id){
        $this->db->table('cities');
        $this->db->query("SELECT `name` FROM `cities` WHERE `id` = " . $this->db->escape($id));
        return $this->db->getValue();
    }

    //Tasks Search
    public function tasks_listing($return = false){
        if( stripos(request_uri(), 'dashboard') ) set_latest_url( request_uri() );

        $this->was_viewing( request('task') ?? request('job'), request('task') ? 'task' : 'job' );

        $taskObject = Task::init($this->db, $this->user);
        $jobObject = Job::init($this->db, $this->user);
        $this->is_logged_in();
        $search_mode = $this->user->info['preference']['search_mode'] ?? 'latest';
        $per_page = 20;
        $talent_per_page = 16;
        $filters = [];
        $published_jobs_count  = $published_tasks_count = 0;
        $there_is_more = false;
        $scope = request('scope') ? sanitize(request('scope')) : 'task';
        $search_term = request('q') ? sanitize(request('q')) : null;

        if(!is_null($search_term))
            $search_term = array_values(array_filter(explode(' ', preg_replace('#[^a-zA-Z0-9\s]#i', '', $search_term))));

        $filters = [
            //'type' => request('type') ? explode(',', sanitize(request('type'))) : null,
            'category' => request('category') ? sanitize(request('category')): null,
            'state' => request('state') ? count(explode(',', sanitize(request('state')))) > 1 ? explode(',', sanitize(request('state'))) : sanitize(request('state')) : null,
            'city' => request('city') ? count(explode(',', sanitize(request('city')))) > 1 ? explode(',', sanitize(request('city'))) : sanitize(request('city')) : null,
            'task_type' => request('task_type') ? count(explode(',', sanitize(request('task_type')))) > 1 ? explode(',', sanitize(request('task_type'))) : sanitize(request('task_type')) : null,
            'task_budget' => request('task_budget') ? count(explode('|', sanitize(request('task_budget')))) > 1 ? explode('|', sanitize(request('task_budget'))) : sanitize(request('task_budget')) : null,
            'date_type' => request('date_type') ? count(explode(',', sanitize(request('date_type')))) > 1 ? explode(',', sanitize(request('date_type'))) : sanitize(request('date_type')) : null,
            'date_period' => request('date_period') ? count(explode(',', sanitize(request('date_period')))) > 1 ? explode(',', sanitize(request('date_period'))) : sanitize(request('date_period')) : null,
            'from' => request('from') ? count(explode(',', sanitize(request('from')))) > 1 ? explode(',', sanitize(request('from'))) : sanitize(request('from')) : null,
            'to' => request('to') ? count(explode(',', sanitize(request('to')))) > 1 ? explode(',', sanitize(request('to'))) : sanitize(request('to')) : null,
            'skills' => request('skills') ? sanitize(request('skills')) : null,
            'type' => request('type') ? sanitize(request('type')) : null,
            'location_by' => request('location_by') ? sanitize(request('location_by')) : null,
            'within' => request('within') ? sanitize(request('within')) : null,
        ];

        $filters = array_filter($filters);
        $params = $filters;
        $params['scope'] = $scope;
        $url = url_for('workspace/search') .'?'. http_build_query($params);
        // checking if task budget was passed as ',' separated
        if( isset($filters['task_budget']) && is_string($filters['task_budget']) && strpos($filters['task_budget'], ',') ){
            $filters['task_budget'] = explode(',', $filters['task_budget']);
        }

        if( isset($filters['date_period']) && $filters['date_period'] !== 'customrange' ){ $filters['from'] = null; $filters['to'] = null; }

        /*if( $this->user->info['type'] === '0' ) {
            $lat_lng       = isset($_SESSION['current_user_location']) ? $_SESSION['current_user_location'] : null;
            $nearby_places = (new Search($this->db, $this->user))->get_nearby_names(null, $lat_lng);

            if (isset($nearby_places['physical']) || isset($nearby_places['remote'])) {

                if (isset($nearby_places['physical']) && $nearby_places['physical']['location'] === 'same_country') {
                    if (is_null($filters['state']) && $nearby_places['physical']['state'] !== '0') {
                        $physical_location_set = true;
                        $filters['state']      = $nearby_places['physical']['state'];
                    }
                }
                if (isset($nearby_places['remote']) && $nearby_places['remote']['location'] === 'same_country') {
                    if ($nearby_places['remote']['state'] !== '0') {
                        $filters['remote_state'] = $this->getStateName($nearby_places['remote']['state']);
                    }
                }

            } else {
                if (!empty($nearby_places)) {
                    $filters['nearby_places'] = $nearby_places;
                    // for debugging purposes
                    log_debug("[%date%] [:info] [] [client %ip%] (%latlng%), %user_id%, %name%, %p_radius%, %places%",
                        [
                            'ip' => $_SERVER['REMOTE_ADDR'].':'.$_SERVER['REMOTE_PORT'],
                            'date' => date('D M d H:i:s.u Y'),
                            'latlng' => $lat_lng,
                            'user_id' => $this->user->info['id'],
                            'name' => $this->user->info['firstname'],
                            'p_radius' => $this->user->info['preference']['p_radius'],
                            'places' => $nearby_places
                        ]);
                }
            }
        }*/
        if( isset($filters['location_by']) && $filters['location_by'] === 'locbystate' ) {
            if (isset($filters['state'])) $filters['state_name'] = $this->getStateName($filters['state']);
            if (isset($filters['city'])) $filters['city_name'] = $this->getCityName($filters['city']);
        }elseif ( isset($filters['location_by']) && $filters['location_by'] === 'locbynearest' ){
            if( isset($filters['within']) && $filters['within'] !== '' ){
                $within        = preg_replace("/[^0-9]/", "", $filters['within']); // strip all non-numeric from the passed value
                $lat_lng       = isset($_SESSION['current_user_location']) ? $_SESSION['current_user_location'] : null;

                @list($lat, $lng) = !is_array($lat_lng) ? explode(',', $lat_lng, 2) : $lat_lng;

                // Use cities table to get nearby places
                $nearby_places = (new Search($this->db, $this->user))->get_nearby($lat, $lng, $within);
                $filters['nearby_places'] = array_column($nearby_places, 'city');
                if(!empty( $nearby_places )) {
                    $filters['city'] = array_column($nearby_places, 'city_id');
                    //$filters['state_name'] = array_column($nearby_places, 'state');
                }else{
                    // if cities table has no results, then use google maps api to get nearby places
                    $nearby_places = (new Search($this->db, $this->user))->get_nearby_names(null, $lat_lng, $within);
                    if( !empty($nearby_places) )
                        $filters['nearby_places'] = $nearby_places;
                }
            }
        }

        if( isset($filters['category']) && $filters['category'] != 0 ){
            $table = $scope === 'task' ? 'task_category' : 'category';
            $this->db->query("SELECT `id` FROM `{$table}` WHERE `parent` IN({$filters['category']})");
            $category_subcategories_list = $this->db->getRowList();
            if( !empty($category_subcategories_list) ){
                $category_subcategories_list = array_column($category_subcategories_list, 'id');
                $filter_category = $filters['category']; /// we need to save original selected categories in filter
                $filters['category'] = implode(',', $category_subcategories_list);
            }
        }else{
            $filters['category'] = null;
        }

        $page = params('page') ? (int)params('page') : 1;
        $offset = $scope !== 'talent' ? ($page * $per_page) - $per_page : ($page * $talent_per_page) - $talent_per_page;;
        if( $offset < 0 ) $offset = 0;

        if($scope === 'task') {
            $published_tasks_count = (int) $taskObject->get_all_published_tasks_count($search_term, $filters);
            $total_count = $published_tasks_count;
        }elseif($scope === 'job'){
            $published_jobs_count = (int) $jobObject->get_all_published_jobs_count($search_term, $filters);
            $total_count = $published_jobs_count;
        }elseif($scope === 'talent'){
            $talents_total_count = (int) $this->get_total_talents_count($search_term, $filters);
            $total_count = $talents_total_count;
        }

        $total_pages = ceil($total_count/$per_page);

        $temp_per_page = $per_page;
        if(in_array($scope, ['task', 'job'])){
            /*if( $this->is_ajax() && $page > 1 ){
                $per_page /= 2;
                $offset = $page * $per_page;
            }*/
            if( $page < $total_pages ){
                $there_is_more = true;
            }else{
                $there_is_more = false;
            }
        }else {
            if( $this->is_ajax() && $page > 1 ){
                /*$talent_per_page = $talent_per_page === 16 ? $talent_per_page/2 : $talent_per_page;
                $offset = $page * $talent_per_page;
                $total_pages = ceil($talents_total_count/$talent_per_page);*/
            }

            if( $page < $total_pages ){
                $there_is_more = true;
            }else{
                $there_is_more = false;
            }
        }

        if ($scope === 'task') {
            $tasks = $taskObject->get_published_tasks($search_term, $filters, $offset, $per_page);
            $internal_tasks = array_column(array_filter($tasks, function ($task) { return $task['the_type'] === 'internal'; }), 'task_id');
            $external_tasks = array_column(array_filter($tasks, function ($task) { return $task['the_type'] === 'external'; }), 'task_id');
            $int_tasks = $taskObject->get_tasks_by_id($internal_tasks);
            $ext_tasks = $taskObject->get_external_tasks_by_id($external_tasks);
            $tasks = [];
            $all = array_merge($int_tasks, $ext_tasks);

            /*usort($all, function($first, $second){
                return $first['priority'] == $second['priority'] ? 0 :
                    ($first['priority'] < $second['priority'] ? -1 : 1);
            });*/

            /*if( $search_mode === 'match' ) {
                usort($all, function ($first, $second) {
                    return $first['matched'] == $second['matched'] ? 0 :
                        ($first['matched'] > $second['matched'] ? -1 : 1);
                });
            }*/

            $counter = 0;
            foreach ($all as $key => $task) {
                if( !isset($task['source']) ) {
                    $task = $taskObject->get_task_by_id($task['task_id']);
                    unset($task['password']);
                    $tasks[$counter] = $task;
                    $tasks[$counter]['type']            = 'internal_task';
                    $tasks[$counter]['country']         = $this->getCountryName($task['country']);
                    $tasks[$counter]['state']           = $this->getStateName($task['state']);
                    //$tasks[$counter]['published_count'] = $taskObject->get_published_tasks_count($task['user_id']);
                    //$tasks[$counter]['rating']          = $this->get_ratings($task['user_id']);
                    //$tasks[$counter]['questions']       = $taskObject->questions($task['task_id']);
                    $counter++;
                }else{
                    $task = $taskObject->get_external_task_by_id($task['task_id']);
                    @list($state, $country) = explode(', ', $task['state_country'], 2);
                    $tasks[$counter] = $task;
                    $tasks[$counter]['type']    = 'external_task';
                    $tasks[$counter]['country'] = $country;
                    $tasks[$counter]['state']   = $state;
                    $counter++;
                }
            }

            $favourites['tasks'] = $taskObject->get_favourites($this->user->info['id']);
            $favourites['ids']   = array_map(function ($task) {
                return $task['task_id'];
            }, $favourites['tasks']);

            $applied['tasks'] = $taskObject->get_applied_tasks($this->user->info['id'], 0, 100);
            $applied['external_tasks'] = $taskObject->get_applied_external_tasks($this->user->info['id'], 0, 100);

            $applied_tasks_ids = array_map(function ($task) {
                return $task['id'];
            }, $applied['tasks']);

            $applied_ext_tasks_ids = array_map(function ($task) {
                return $task['id'];
            }, $applied['external_tasks']);

            $applied['ids'] = array_merge($applied_tasks_ids, $applied_ext_tasks_ids);

        } else {
            $tasks = $favourites['ids'] = $favourites['tasks'] = $applied['tasks'] = $applied['ids'] = [];
        }

        if ($scope === 'job') {

            $jobs = $jobObject->get_published_jobs($search_term, $filters, $offset, $per_page);
            $internal_jobs = array_column(array_filter($jobs, function ($job) { return $job['the_type'] === 'internal'; }), 'job_id');
            $external_jobs = array_column(array_filter($jobs, function ($job) { return $job['the_type'] === 'external'; }), 'job_id');
            $int_jobs = $jobObject->get_jobs_by_id($internal_jobs);
            $ext_jobs = $jobObject->get_external_jobs_by_id($external_jobs);
            $jobs = [];
            $all = array_merge($ext_jobs, $int_jobs);
            /*usort($all, function($first, $second){
                return Carbon::parse($first['created_at'])->equalTo(Carbon::parse($second['created_at'])) ? 0 :
                    (Carbon::parse($first['created_at'])->greaterThan(Carbon::parse($second['created_at'])) ? -1 : 1);
            });*/
            usort($all, function($first, $second){
                return $first['priority'] == $second['priority'] ? 0 :
                    ($first['priority'] < $second['priority'] ? -1 : 1);
            });
            $jobs = $all;
            $counter = 0;
            /*foreach ($all as $key => $job) {
                if( !isset($job['source']) ){
                    $job = $jobObject->get_job_by_id($job['job_id']);
                    unset($job['password']);
                    $jobs[$counter]                    = $job;
                    $jobs[$counter]['type']            = 'internal_job';
                    $jobs[$counter]['country']         = $this->getCountryName($job['country']);
                    $jobs[$counter]['state']           = $this->getStateName($job['state']);
                    $jobs[$counter]['job_country']         = $this->getCountryName($job['job_country']);
                    $jobs[$counter]['job_state']           = $this->getStateName($job['job_state']);
                    //$jobs[$counter]['published_count'] = $jobObject->get_published_jobs_count($job['user_id']);
                    //$jobs[$counter]['rating']          = $this->get_ratings($job['user_id']);
                    //$jobs[$counter]['questions']       = $jobObject->questions($job['job_id']);
                    $jobs[$counter]['salary_range_max']   = number_format($job['salary_range_max'], 2);
                    $jobs[$counter]['salary_range_min']   = number_format($job['salary_range_min'], 2);
                    $counter++;
                }else{
                    $job = $jobObject->get_external_job_by_id($job['job_id']);
                    $jobs[$counter]            = $job;
                    $jobs[$counter]['type']    = 'external_job';
                    $jobs[$counter]['job_country'] = $this->getCountryName($job['country']);
                    $jobs[$counter]['job_state']   = $job['state'];
                    $jobs[$counter]['salary_range_max']   = number_format($job['salary_range_max'], 2);
                    $jobs[$counter]['salary_range_min']   = number_format($job['salary_range_min'], 2);
                    $counter++;
                }
            }*/

            //$favourites['jobs']  = $jobObject->get_favourites($this->user->info['id']);
            $favourites_jobs_ids = array_map(function ($job) {
                if($job['favourited'] > 0) return $job['job_id'];
            }, $jobs);

            $applied['jobs']  = [];//$jobObject->get_applied_jobs($this->user->info['id']);
            $applied['external_jobs'] = [];// $jobObject->get_applied_external_jobs($this->user->info['id']);

            $applied_jobs_ids = array_map(function ($job) {
                return $job['id'];
            }, $applied['jobs']);

            $applied_ext_jobs_ids = array_map(function ($job) {
                return $job['id'];
            }, $applied['external_jobs']);

            $applied_jobs_ids = array_merge($applied_jobs_ids, $applied_ext_jobs_ids);

        } else {
            $jobs = $favourites['jobs'] = $favourites_jobs_ids = $applied['jobs'] = $applied_jobs_ids = [];
        }

        $per_page = $temp_per_page;

        $favourites['ids'] = array_merge($favourites['ids'], $favourites_jobs_ids);

        $applied['ids'] = array_merge($applied['ids'], $applied_jobs_ids);
        $all_list       = array_merge($tasks, $jobs);

        /*usort($all_list, function($first, $second){
            return Carbon::parse($first['created_at'])->equalTo(Carbon::parse($second['created_at'])) ? 0 :
                   (Carbon::parse($first['created_at'])->greaterThan(Carbon::parse($second['created_at'])) ? -1 : 1);
        });*/

        if( (!empty(array_filter($filters)) || request('completed') || !empty($search_term)) && sanitize(request('scope')) === 'talent' ){

            /*if( !$this->is_ajax() ){
                $offset = 0;
                $talent_per_page = $page > 1 ? $page * $talent_per_page/2 : $talent_per_page;
            }else {
                if( $page > 1 && $talent_per_page === 16)
                    $talent_per_page /= 2;

                $offset = $page * $talent_per_page;
            }*/

            if( request('completed') ) $filters['completed'] = sanitize(request('completed'));
            $talents = $this->talents_listing($search_term, $filters, $offset, $talent_per_page);
            $talents_total_count = (int)$this->get_total_talents_count($search_term, $filters);
            $talents_total_pages = ceil($talents_total_count/($talent_per_page));

            if( !$this->is_ajax() ){
                if( $page < $talents_total_pages ){
                    $there_is_more_talents = true;
                }else{
                    $there_is_more_talents = false;
                }
            }else {
                if( $page < $talents_total_pages ){
                    $there_is_more_talents = true;
                }else{
                    $there_is_more_talents = false;
                }
            }
        }elseif( $scope === 'talent' ) {
            /*if( !$this->is_ajax() ){
                $offset = 0;
                $talent_per_page = $page > 1 ? $page * $talent_per_page : $talent_per_page;
            }else {
                /*if($page > 1 && $talent_per_page === 16)
                    $talent_per_page /= 2;
                $offset = ($page - $talent_per_page) - $talent_per_page;*/
                //$talent_per_page = $talent_per_page === 16 ? $talent_per_page/2 : $talent_per_page;
                //$offset = $page * $talent_per_page;

                //ray()->table(['offset' => $offset, 'per page' => $talent_per_page, 'total' => $total_pages])->blue();
            //}

            $talents = $this->talents_listing([], [], $offset, $talent_per_page);
            $talents_total_count = (int)$this->get_total_talents_count($search_term, []);
            $talents_total_pages = ceil($talents_total_count/$talent_per_page);

            if( !$this->is_ajax() ){
                if( $page < $talents_total_pages ){
                    $there_is_more_talents = true;
                }else{
                    $there_is_more_talents = false;
                }
            }else {
                if( $page < $talents_total_pages ){
                    $there_is_more_talents = true;
                }else{
                    $there_is_more_talents = false;
                }
            }
        }

        if( isset($physical_location_set) && $physical_location_set ){
            $filters['state'] = null;
            $filters['state_name'] = null;
            $filters['remote_state'] = null;
        }

        if( isset($within) ){
            $filters['state'] = null;
            $filters['state_name'] = null;
            $filters['city'] = null;
        }

        $states = $this->getStates(null, true);
        $cities = isset($filters['state']) ? $this->getCities($filters['state'], true) : [];
        if( isset($filter_category) ) $filters['category'] = $filter_category;
        if (request('category') !== null) {
            $category_names = in_array('0', explode(',', request('category'))) ? lang('public_search_all_category') . ', ' : '';
            if (isset($filters['category'])) {
                $filters['category_names'] = !isset($filters['type']) || is_null($filters['type']) || in_array($filters['type'], ['task']) ? $this->getCategories(explode(',', $filters['category']), 'task_category') : $this->getCategories(explode(',', $filters['category']));
                $category_names            .= implode(', ', array_column($filters['category_names'], 'name'));
            }
            $filters['category'] .= in_array('0', explode(',', request('category'))) && is_null($filters['category']) ? '0,' : '';
            $filters['category_names'] = trim($category_names, ', ');
        }

        if( !empty($filters['skills']) ) {
            $this->db->query("SELECT `id`, `name` FROM `skills` WHERE `status` = '1' AND `id` IN({$filters['skills']})");
            $filters['skills_list'] = $this->db->getRowList();
        }

        $keywords = !empty($search_term) ? implode(' ', $search_term) : '';
        array_filter($filters, function ($filter, $key) use(&$keywords, $filters){
            if(in_array($key, ['category', 'state', 'city', 'skills_list', 'nearby_places', 'states', 'remote_state', 'type', 'location_by'])) return false;
            if('task_budget' === $key && isset($filters['task_budget'])){
                if((int)$filter[0] === 1000){
                    $keywords .= ', '. lang('search_budget') .': RM' . $filter[0] . ' ' . lang('dashboard_pay_range_select_amount_and_above');
                }else {
                    $keywords .= ', '. lang('search_budget') .': RM' . implode(' - RM', $filter);
                }
                return false;
            }
            if('date_type' === $key && isset($filters['date_type'])){
                $type = [
                            'postedDate' => lang('public_search_posted_date') . ': ',
                            'startDate'  => lang('public_search_start_date') . ': ',
                            'closeDate'  => lang('public_search_closing_date') . ': '
                        ][$filter];
                $keywords .=  ', ' . $type;
                return false;
            }
            if('date_period' === $key && isset($filters['date_type'], $filters['date_period'])){
                if('customrange' !== $filter) {
                    if($filters['date_type'] === 'postedDate' && $filter !== 'any')
                        $period = [
                                      '24h' => sprintf(lang('public_search_last_x_hours'), 24),
                                      '3d'  => sprintf(lang('public_search_last_x_days'), 3),
                                      '7d'  => sprintf(lang('public_search_last_x_days'), 7),
                                      '14d' => sprintf(lang('public_search_last_x_days'), 14),
                                      '30d' => sprintf(lang('public_search_last_x_days'), 30)
                                  ][$filter];
                    elseif (in_array($filters['date_type'], ['startDate', 'closeDate']) && $filter !== 'any')
                        $period = [
                                      '24h' => sprintf(lang('public_search_next_x_hours'), 24),
                                      '3d'  => sprintf(lang('public_search_next_x_days'), 3),
                                      '7d'  => sprintf(lang('public_search_next_x_days'), 7),
                                      '14d' => sprintf(lang('public_search_next_x_days'), 14),
                                      '30d' => sprintf(lang('public_search_next_x_days'), 30)
                                  ][$filter];
                    else
                        $period = lang('search_any');
                }else{
                    $period = lang('search_between') . ' (';
                }
                $keywords .=  $period;
                return false;
            }
            if('from' === $key && isset($filters['date_type'], $filters['date_period']) && $filters['date_period'] === 'customrange'){
                $keywords .=  str_replace('-', '/', $filter);
                return false;
            }
            if('to' === $key && isset($filters['date_type'], $filters['date_period']) && $filters['date_period'] === 'customrange'){
                $keywords .=  ' - ' . str_replace('-', '/', $filter) . ')';
                return false;
            }

            if('completed' === $key && $filter !== ''){
                if($keywords !== '') $keywords .= ', ';
                $keywords .= lang('public_search_talent_completed_task') . ': ' . ($filter === 'all' ? lang('public_search_filter_all') : $filter);
                return false;
            }

            if('skills' === $key && !empty($filter)){
                if($keywords !== '') $keywords .= ', ';
                $filter = implode(', ', array_column($filters['skills_list'], 'name'));
                $keywords .= $filter;
                return false;
            }

            if('within' === $key && !empty($filter)){
                if($keywords !== '') $keywords .= ', ';
                $filter = lang('search_within') . " {$filter}";
                $keywords .= $filter;
                return false;
            }

            if(is_array($filter)) $keywords .=  ', ' . ucwords(implode(', ', $filter));
            elseif(!is_null($filter)) $keywords .=  ', ' . ucwords($filter);
        }, ARRAY_FILTER_USE_BOTH);

        $keywords = str_replace(['By-hour', 'Lump-sum'], [lang('dashboard_task_type_by_hour'), lang('dashboard_task_type_lump_sum')], trim($keywords, ', '));
        $saved_search = !$this->is_ajax() ? (new Search($this->db, $this->user))->get_search() : [];
        $filters = array_filter($filters);

        if($return === true){
            return [
                'saved_search'    => $saved_search,
                'scope'           => $scope,
                'talents'         => $talents ?? [],
                'listing'         => $all_list,
                'current_user'    => $this->user->info,
                'favourites'      => $favourites,
                'applied'         => $applied,
                'tasks_applied'    => $applied,
                'jobs_applied'    => $applied,
                'more'            => $there_is_more,
                'more_talents'    => $there_is_more_talents ?? false,
                'current_page'    => $page,
                'search_term'     => implode(' ', $search_term),
                'filters'         => $filters,
                'keywords'        => $keywords,
                'states'          => $states,
                'cities'          => $cities,
                'task_categories' => $this->getCategories([], 'task_category'),
                'job_categories'  => $this->getCategories(),
                'url'             => $url,
            ];
            die;
        }else {

            set('saved_search', $saved_search);
            set('scope', $scope);
            set('talents', $talents ?? []);
            set('listing', $all_list);
            set('current_user', $this->user->info);
            set('favourites', $favourites);
            set('applied', $applied);
            set('tasks_applied', $applied);
            set('jobs_applied', $applied);
            set('more', $there_is_more);
            set('more_talents', $there_is_more_talents ?? false);
            set('current_page', $page);
            set('search_term', implode(' ', $search_term));
            set('filters', $filters);
            set('keywords', $keywords);
            set('states', $states);
            set('cities', $cities);
            set('task_categories', $this->getCategories([], 'task_category'));
            set('job_categories', $this->getCategories());
            set('page_title', 'Search');
            set('url', $url);

            if ($this->is_ajax()) {
                if (request('scope') === 'task')
                    return json(['more' => $there_is_more, 'html' => partial('workspace/partial/task-listing/task-search-result.html.php'), 'keywords' => $keywords]);
                elseif (request('scope') === 'job')
                    return json(['more' => $there_is_more, 'html' => partial('workspace/partial/task-listing/job-search-result.html.php'), 'keywords' => $keywords]);
                else
                    return json(['more' => $there_is_more_talents, 'html' => partial('workspace/partial/task-listing/talent-search-result.html.php'), 'keywords' => $keywords]);
            }

            return render('workspace/task-listing.html.php', 'layout/app.html.php');
        }
    }

    //Talents Search
    public function talents_listing( $search = [], $filters = [], $offset = 0, $limit = 25 ){
        $limit = ceil($limit);
        $filters = array_filter($filters);

        $query = "SELECT `id`, `firstname`, `lastname`, `date_created` FROM `members` m WHERE m.`type` = '0' AND m.`id` != " . $this->db->escape($this->user->info['id']);

        if(!empty($search)) {
            $query .= " AND (";
            $length = count($search);

            $skills_query = "SELECT `id` FROM `skills` WHERE ";
            foreach ($search as $key => $str){
                $skills_query .= " LOWER(CONVERT(`name` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length){
                    $skills_query .= " OR ";
                }
            }

            $this->db->query($skills_query);
            $skills_result = $this->db->getRowList();

            foreach ($search as $key => $str){
                $query .= " ( LOWER(CONVERT(`firstname` USING utf8mb4)) LIKE '%{$str}%' ";
                $query .= " OR LOWER(CONVERT(`lastname` USING utf8mb4)) LIKE '%{$str}%' ";
                $query .= " OR LOWER(CONVERT(`email` USING utf8mb4)) LIKE '%{$str}%' ";
                $query .= " OR LOWER(CONVERT(`about` USING utf8mb4)) LIKE '%{$str}%' ";
                $query .= " OR LOWER(CONVERT(`language` USING utf8mb4)) LIKE '%{$str}%' )";
                if($key+1 !== $length){
                    $query .= " OR ";
                }
            }
            if( !empty($skills_result) ) {
                foreach(array_column($skills_result, 'id') as $skill) {
                    $query .= " OR FIND_IN_SET({$skill}, `skills`) ";
                }
            }

            $query .= " ) ";
        }

        if(!empty($filters)){
            extract($filters);

            if(isset($state) && $state !== 'any'){
                $query .= " AND (`state` IN({$state}))";
            }

            if(isset($states['ids']) && !empty($states['ids']) ){
                $state = implode(',', $states['ids']);
                $query .= " AND (`state` IN({$state}))";
            }

            if(isset($nearby_places)){
                foreach ($nearby_places as $places){
                    foreach($places as $place){
                        $place = trim(strtolower($place));
                        $query .= " OR (LOWER(CONVERT(`address1` USING utf8mb4)) LIKE '%{$place}%' ";
                        $query .= " OR LOWER(CONVERT(`address2` USING utf8mb4)) LIKE '%{$place}%')";
                    }
                }
            }

            if(isset($city)){
                if( is_array($city) ) $city = implode(',', $city);
                $query .= " AND (`city` IN({$city}))";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);
                $query .= " AND ( ";
                $length = count($skills);

                foreach ($skills as $key => $str){
                    $str = strtolower($str);
                    $query .= " FIND_IN_SET($str,`skills`)";
                    if($key+1 !== $length){
                        $query .= " OR ";
                    }
                }

                $query .= " ) ";
            }

            if( isset($completed) ){
                if($completed !== 'all') {
                    @list($min, $max) = explode('-', $completed);
                    $min = trim($min);
                    $max = trim($max);

                    $query .= "AND ((
                                SELECT COUNT(t.`id`)
                                FROM `tasks` t
                                WHERE
                                    t.`assigned_to` = m.`id`
                                    AND t.`status` IN ('completed', 'closed')
                                    AND t.`assigned` IS NOT NULL
                            ) ";
                    if( $max !== '') {
                        $query .= " BETWEEN {$min} AND {$max}) ";
                    }else{
                        $min = trim($min, '+');
                        $query .= " > {$min}) ";
                    }
                }
            }
        }

        $query .= " ORDER BY `date_created` DESC LIMIT {$offset}, {$limit}";

        $this->db->query($query);
        $talents_id = array_column($this->db->getRowList(), 'id');

        $talents = [];
        foreach ($talents_id as $id){
            $talent = $this->get_talent($id, false);
            $talent['skills'] = isset($talent['skills']) ? array_filter(explode(',', $talent['skills'])) : [];
            $talent['language'] = isset($talent['language']) ? array_filter(explode(',', $talent['language'])) : [];
            array_push($talents, $talent);
        }

        return $talents;
    }

    //Talents Search Result Count
    public function get_total_talents_count( $search = [], $filters = [] ){
        $filters = array_filter($filters);

        $query = "SELECT COUNT(`id`) AS `count` FROM `members` m WHERE m.`type` = '0' AND m.`id` != " . $this->db->escape($this->user->info['id']);

        if(!empty($search)) {
            $query .= " AND (";
            $length = count($search);

            $skills_query = "SELECT `id` FROM `skills` WHERE ";
            foreach ($search as $key => $str){
                $skills_query .= " LOWER(CONVERT(`name` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length){
                    $skills_query .= " OR ";
                }
            }

            $this->db->query($skills_query);
            $skills_result = $this->db->getRowList();

            foreach ($search as $key => $str){
                $query .= " ( LOWER(CONVERT(`firstname` USING utf8mb4)) LIKE '%{$str}%' ";
                $query .= " OR LOWER(CONVERT(`lastname` USING utf8mb4)) LIKE '%{$str}%' ";
                $query .= " OR LOWER(CONVERT(`email` USING utf8mb4)) LIKE '%{$str}%' ";
                $query .= " OR LOWER(CONVERT(`about` USING utf8mb4)) LIKE '%{$str}%' ";
                $query .= " OR LOWER(CONVERT(`language` USING utf8mb4)) LIKE '%{$str}%' )";
                if($key+1 !== $length){
                    $query .= " OR ";
                }
            }
            if( !empty($skills_result) ) {
                foreach(array_column($skills_result, 'id') as $skill) {
                    $query .= " OR FIND_IN_SET({$skill}, `skills`) ";
                }
            }

            $query .= " ) ";
        }

        if(!empty($filters)){
            extract($filters);

            if(isset($state) && $state !== 'any'){
                $query .= " AND (`state` IN({$state}))";
            }

            if(isset($states['ids']) && !empty($states['ids'])){
                $state = implode(',', $states['ids']);
                $query .= " AND (`state` IN({$state}))";
            }

            if(isset($nearby_places)){
                foreach ($nearby_places as $places){
                    foreach($places as $place){
                        $place = trim(strtolower($place));
                        $query .= " OR (LOWER(CONVERT(`address1` USING utf8mb4)) LIKE '%{$place}%' ";
                        $query .= " OR LOWER(CONVERT(`address2` USING utf8mb4)) LIKE '%{$place}%')";
                    }
                }
            }

            if(isset($city)){
                if( is_array($city) ) $city = implode(',', $city);
                $query .= " AND (`city` IN({$city}))";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = [$skills];
                $query .= " AND ( ";
                $length = count($skills);

                foreach ($skills as $key => $str){
                    $str = strtolower($str);
                    $query .= " FIND_IN_SET($str,`skills`)";
                    if($key+1 !== $length){
                        $query .= " OR ";
                    }
                }

                $query .= " ) ";
            }

            if( isset($completed) ){
                if($completed !== 'all') {
                    @list($min, $max) = explode('-', $completed);
                    $min = trim($min);
                    $max = trim($max);

                    $query .= "AND ((
                                SELECT COUNT(t.`id`)
                                FROM `tasks` t
                                WHERE
                                    t.`assigned_to` = m.`id`
                                    AND t.`status` IN ('completed', 'closed')
                                    AND t.`assigned` IS NOT NULL
                            ) ";
                    if( $max !== '') {
                        $query .= " BETWEEN {$min} AND {$max}) ";
                    }else{
                        $min = trim($min, '+');
                        $query .= " > {$min}) ";
                    }
                }
            }
        }

        $this->db->query($query);
        return $this->db->getValue();
    }

    public function get_task(){
        if(is_null(params('task'))) {
            redirect( option('site_uri') . url_for('/workspace/search') );
        }
        $task  = sanitizeItem(params('task'));
        $taskClass = Task::init($this->db, $this->user);
        if( request('s') && request('s') === 'external' ) {
            $task               = $taskClass->get_external_task($task);
            @list($state, $country) = explode(', ', $task['state_country']);
            $task['country']    = $country;
            $task['state']    = $state;
            $task['external']   = true;
            $task['comments']   = [];

        }else {

            $task      = $taskClass->get_task($task);
            if (isset($task['password'])) unset($task['password']);
            $task['state']             = $this->getStateName($task['state']);
            $task['country']           = $this->getCountryName($task['country']);
            $task['published_count']   = $taskClass->get_published_tasks_count($task['user_id']);
            $task['disputed_count']    = $taskClass->get_status_count('disputed', $task['user_id']);
            $task['rating']            = $this->get_ratings($task['user_id']);
            $task['tags']              = $taskClass->tags($task['task_id']);
            $task['questions']         = $taskClass->questions($task['task_id']);
            $task['comments']          = $this->get_comments($task['task_id'], $task['user_id']);
            $task['skills']            = $this->get_skills($task['task_skills']);
            $task['attachments']       = $taskClass->attachments($task['task_id']);
            $task['poster']            = $taskClass->get_task_user($task['user_id']);
            $task['poster']['state']   = $this->getStateName($task['poster']['state']);
            $task['poster']['country'] = $this->getCountryName($task['poster']['country']);
            unset($task['poster']['password']);

            @list($category, $subcategory) = $this->getCategories( [ $task['category'], $task['sub_category'] ], 'task_category' );
            $task['category_name'] = $category['name'];
            $task['subcategory_name'] = $subcategory['name'];
        }

        $favourites['tasks'] = $taskClass->get_favourites($this->user->info['id']);
        $favourites['ids'] = array_map(function ($task){
            return $task['task_id'];
        }, $favourites['tasks']);

        $applied['tasks'] = $taskClass->get_applied_tasks($this->user->info['id']);
        $applied['external_tasks'] = $taskClass->get_applied_external_tasks($this->user->info['id']);
        $applied_at = array_column($applied['tasks'], 'applied_at', 'id');
        $applied['ids'] = array_map(function ($task){
            return $task['id'];
        }, $applied['tasks']);

        $applied_ext_ids = array_map(function ($task){
            return $task['id'];
        }, $applied['external_tasks']);

        $applied['ids'] = array_merge($applied['ids'], $applied_ext_ids);

        $comment_answers = [];
        $task['answers'] = [];
        array_map(function ($comment) use (&$comment_answers){
            $comment_answers[$comment['comment_id']][] = $this->get_comment_answers($comment['post_id'], $comment['comment_id']);
        }, $task['comments']);

        foreach ($comment_answers as $key => $comment){
            $task['answers'][$key] = $comment[0];
        }

        if((int)$this->user->info['id'] === (int)$task['user_id']) {
            $this->mark_comments_as_read($task['task_id']);
        }

        unset($comment_answers);

        if($this->is_ajax()) {
            $task['skills'] = $this->get_all_related_skills($task['task_skills']);
            echo json(['task' => $task]);
            die();
        }

        set('task', $task);
        set('current_user', $this->user->info);
        set('task_favourites', $favourites);
        set('task_applied', $applied);
        set('applied_at', $applied_at);
        set('page_title', 'Task Details');

        return render('workspace/task-details.html.php', 'layout/app.html.php');
    }

    public function get_job(){
        if(is_null(params('job'))) {
            redirect( option('site_uri') . url_for('/workspace/search') );
        }
        $job = sanitizeItem(params('job'));
        if( request('s') && request('s') === 'external' ) {
            $job               = Job::init($this->db, $this->user)->get_external_job($job);
            $job['country_id'] = $job['country'];
            $job['country']    = $this->getCountryName($job['country']);
            $job['external']   = true;
            $job['comments']   = [];
        }else{
            $job                      = (new Job($this->db, $this->user))->get_job($job);
            $job['state_id']          = $job['state'];
            $job['country_id']        = $job['country'];
            $job['state']             = $this->getStateName($job['state']);
            $job['country']           = $this->getCountryName($job['country']);
            $job['published_count']   = (new Job($this->db, $this->user))->get_published_jobs_count($job['user_id']);
            $job['disputed_count']    = (new Job($this->db, $this->user))->get_status_count('disputed', $job['user_id']);
            $job['rating']            = $this->get_ratings($job['user_id']);
            $job['benefits']          = json_decode($job['benefits']);
            $job['skills']            = $this->get_skills($job['job_skills']);
            $job['comments']          = $this->get_comments($job['job_id']);
            $job['questions']         = (new Job($this->db, $this->user))->questions($job['job_id']);
            $job['poster']            = (new Job($this->db, $this->user))->get_job_user($job['user_id']);
            $job['poster']['state']   = $this->getStateName($job['poster']['state']);
            $job['poster']['country'] = $this->getCountryName($job['poster']['country']);
            unset($job['poster']['password']);

            @list($category, $subcategory) = $this->getCategories([$job['category'], $job['sub_category']]);
            $job['category_name']    = $category['name'];
            $job['subcategory_name'] = $subcategory['name'];
        }

        $favourites['jobs'] = (new Job($this->db, $this->user))->get_favourites($this->user->info['id']);
        $favourites['ids'] = array_map(function ($job){
            return $job['task_id'];
        }, $favourites['jobs']);

        $applied['jobs'] = (new Job($this->db, $this->user))->get_applied_jobs($this->user->info['id']);
        $applied['external_jobs'] = (new Job($this->db, $this->user))->get_applied_external_jobs($this->user->info['id']);

        $applied['ids'] = array_map(function ($job){
            return $job['id'];
        }, $applied['jobs']);

        $applied_ext_ids = array_map(function ($job){
            return $job['id'];
        }, $applied['external_jobs']);

        $applied['ids'] = array_merge($applied['ids'], $applied_ext_ids);
        $comment_answers = [];
        $job['answers'] = [];
        array_map(function ($comment) use (&$comment_answers){
            $comment_answers[$comment['comment_id']][] = $this->get_comment_answers($comment['post_id'], $comment['comment_id']);
        }, $job['comments']);

        foreach ($comment_answers as $key => $comment){
            $job['answers'][$key] = $comment[0];
        }

        if((int)$this->user->info['id'] === (int)$job['user_id']) {
            $this->mark_comments_as_read($job['job_id']);
        }

        unset($comment_answers);

        if($this->is_ajax()) {
            $job['skills'] = $this->get_all_related_skills($job['job_skills']);
            echo json(['job' => $job]);
            die();
        }

        set('job', $job);
        set('current_user', $this->user->info);
        set('job_favourites', $favourites);
        set('job_applied', $applied);
        set('page_title', 'Job Details');

        return render('workspace/job-details.html.php', 'layout/app.html.php');
    }

    public function getIndustryName($id = 0){
		return $this->db->getValue("SELECT title FROM industry WHERE status = '1' AND id = " .  $this->db->escape($id));	
	}

    public function get_talent($id = null, $return = true){
        if(isset($_SESSION['receiver'])){
            unset($_SESSION['receiver']);
        }
        if(is_null(params('id')) && is_null($id)) {
            return [];
        }

        $user_id = $id ? (int)sanitize($id) : (int)sanitizeItem(params('id'));
        $talent = $this->db->query("SELECT *, m.`id` AS `m_id`, m.language AS languages FROM `members` m
                                    LEFT JOIN `preference` p ON p.`member_id` = m.`id` 
                                    WHERE m.`id` = '{$user_id}' AND m.`type` = '0' LIMIT 1");
        $talent = $this->db->getSingleRow();
        if(!empty($talent)) {
            $talent['number'] = (string)('TL' . (20100 + $talent['m_id']));
            $employments = $this->db->query("SELECT * FROM `employment_details` e 
                                    WHERE e.`member_id` = '{$user_id}'");
            $employments = $this->db->getRowList();

            $talent['employments'] = $employments;

            foreach ($talent['employments'] as $key => $emp) {
                $talent['employments'][$key]['position'] = $emp['position'] . ', ' . $this->getIndustryName($emp['industry']);
                $talent['employments'][$key]['location'] = $this->getStateName($emp['state']) . ', ' . $this->getCountryName($emp['country']);
                $talent['employments'][$key]['duration'] = dateRangeToDays($emp['date_start'], $emp['date_end']);
            }

            $educations = $this->db->query("SELECT e.*,
                                    (SELECT `name` FROM `states` WHERE `id` = e.`state` LIMIT 1) AS `state`, 
                                    (SELECT `name` FROM `countries` WHERE `id` = e.`country` LIMIT 1) AS `country` 
                                    FROM `education` e 
                                    WHERE e.`member_id` = '{$user_id}'");
            $educations = $this->db->getRowList();

            $talent['educations'] = $educations;

            if (!empty($talent)) unset($talent['password']);
            $talent['reviews'] = $this->get_reviews($user_id);
            foreach ($talent['reviews'] as $key => $review) {
                unset($talent['reviews'][$key]['password']);
                $talent['reviews'][$key]['photo'] = imgCrop($talent['reviews'][$key]['photo'], 35, 35, 'assets/img/default-avatar.png');
                /*$talent['reviews'][$key]['photo'] ?
                    option('site_uri') . url_for($talent['reviews'][$key]['photo']) :
                    option('site_uri') . url_for('/assets/img/default-avatar.png');*/
            }
            $talent['rating']  = $this->get_ratings($user_id);
            $talent['country'] = !empty($talent['country']) ? $this->getCountryName($talent['country']) : lang('profile_nationality_malaysian_others');
            $talent['state']   = !empty($talent['state']) ? $this->getStateName($talent['state']) : lang('profile_nationality_malaysian_others');

            if (is_numeric($talent['city'])) {
                $talent['city'] = !empty($talent['city']) ? $this->getCityName($talent['city']) : lang('profile_nationality_malaysian_others');
            }

            if( !empty($talent['skills']) ){
                $this->db->query("SELECT `id`, `name` FROM `skills` WHERE `status` = '1' AND `id` IN({$talent['skills']})");
                $skills = $this->db->getRowList();
                $talent['skills'] = implode(',', array_column($skills, 'name'));
                $talent['skills_list'] = $skills;
            }

            $talent['member_since']       = $this->date->copy()->parse($talent['date_created'])->formatLocalized('%B %Y');
            $info                         = $this->db->query("SELECT
                                                                (
                                                                    SELECT
                                                                        COUNT(t.`id`)
                                                                    FROM
                                                                        `tasks` t
                                                                    WHERE
                                                                        t.`id` IN( SELECT `task_id` FROM `poster_seeker` WHERE `seeker_id` = m.`id` AND `status` IN ('completed', 'closed', 'disputed') )
                                                                ) AS `completed`,
                                                                (
                                                                    SELECT
                                                                        COUNT(t.`id`)
                                                                    FROM
                                                                        `tasks` t
                                                                    WHERE
                                                                        t.`id` IN( SELECT `task_id` FROM `poster_seeker` WHERE `seeker_id` = m.`id` AND `status` IN ('in-progress', 'completed') )
                                                                ) AS `on_going`,
                                                                COALESCE(
                                                                    (
                                                                        SELECT
                                                                            SUM(t.`hours`)
                                                                        FROM
                                                                            `tasks` t
                                                                        WHERE
                                                                            t.`id` IN( SELECT `task_id` FROM `poster_seeker` WHERE `seeker_id` = m.`id` AND `status` IN ('completed', 'closed', 'disputed') )
                                                                            AND t.`type` = 'by-hour'
                                                                    ),
                                                                    0
                                                                ) AS `hours_completed`,
                                                                COALESCE(
                                                                    (
                                                                        SELECT
                                                                            SUM(
                                                                                TIMESTAMPDIFF(HOUR, t.`start_by`, t.`complete_by`)
                                                                            )
                                                                        FROM
                                                                            `tasks` t
                                                                        WHERE
                                                                            t.`id` IN( SELECT `task_id` FROM `poster_seeker` WHERE `seeker_id` = m.`id` AND `status` IN ('completed', 'closed', 'in-progress') )
                                                                            AND t.`type` = 'lump-sum'
                                                                    ),
                                                                    0
                                                                ) AS `lumpsum_hours`,
                                                                (
                                                                    SELECT MAX(`date`) AS `date`
                                                                    FROM `member_logs`
                                                                    WHERE `member_id` IN ('{$user_id}')
                                                                    GROUP BY `member_id`
                                                                    ORDER BY `date` DESC
                                                                    LIMIT 1
                                                                ) AS `last_login`
                                                            FROM `members` m
                                                            WHERE m.`id` IN ('{$user_id}')
                                                            GROUP BY m.`id`");
            $info                         = $this->db->getSingleRow();
            $talent['info']               = $info;
            $talent['info']['last_login'] = isset($talent['info']['last_login']) ? Carbon::parse($talent['info']['last_login'])->diffForHumans() : lang('new_user');
            $talent['name']               = $talent['firstname'] . ' ' . $talent['lastname'];
            // GUYS, STOP CHANGING PHOTO SIZE FROM HERE ... RESIZE IT IN YOUR PART
            $talent['photo']              = !is_null($talent['photo']) ? imgCrop($talent['photo'], '', '', 'assets/img/default-avatar.png')
                                                                       : imgCrop('assets/img/default-avatar.png');

            $this->db->query("SELECT * FROM member_docs WHERE member_id = " . $this->db->escape($user_id));
            $_docs = $this->db->getRowList();

            $docs = array();
            foreach($_docs as $doc){
                $docs[$doc['type']][] = $doc['src'];
            }

            $docs_type['resume'] = 'Resume';
            $docs_type['cover_letter'] = 'Cover Letter';
            $docs_type['cert'] = 'Award & Certification Files';
            $docs_html = '';

            foreach($docs_type as $type => $title){
                if(!empty($docs[$type])){
                    $docs_html .= partial('talent/talent-documents.html.php', ['docs' => $docs[$type], 'title' => $title]);
                }
            }

            $talent['docs_html'] = $docs_html;

            if ($this->is_ajax() && $return) {
                //$talent['photo'] = imgCrop($talent['photo'], 180, 180);
                echo json(['talent' => $talent]);
                $task_id = request('task_id') ? sanitize(request('task_id')) : sanitize(request('job_id'));
                if($task_id) {
                    Task::init($this->db, $this->user)->set_profile_viewed($user_id, $task_id);
                }
                die;
            }
        }

        $_SESSION['receiver'] = $talent;
        return $talent;
    }

    public function get_answers(){
        if(is_null(params('task')) || is_null(params('user'))) {
            echo json(['answers' => []]); //redirect( option('site_uri') . url_for('/workspace', $this->date->year, $this->date->month) );
        }

        $user_id = (int)sanitizeItem(params('user'));
        $task_id = sanitizeItem(params('task'));

        $task = Task::init($this->db, $this->user)->get_task_by_id($task_id);

        if( $task['user_id'] === $this->user->info['id'] ){
            Task::init($this->db, $this->user)->set_profile_viewed($user_id, $task['task_id']);
        }

        $questions = $this->db->query("SELECT q.`id`, q.`question` FROM `task_questions` q WHERE q.`task_id` = '{$task_id}'");
        $questions = $this->db->getRowList();

        $answers = $this->db->query("SELECT a.`question_id` AS `id`, a.`answer` FROM `answers` a WHERE a.`user_id` = '{$user_id}' AND a.`task_id` = '{$task_id}'");
        $answers = $this->db->getRowList();
        $qna     = [];

        array_map(function ($question) use($answers, &$qna){
            $answer = array_filter($answers, function($answer)use($question){ return $answer['id'] === $question['id'] ? $answer['answer'] : false; });
            $qna[] = [
                'question' => $question['question'],
                'answer' => !empty(end($answer)['answer']) ? end($answer)['answer'] : 'N/A',
            ];
        }, $questions);

        echo json(['answers' => $qna]);
    }

    public function profile_viewed(){
        $task_id = params('task');
        $user_id = params('user');

        if( isset($task_id, $user_id)) {
            $task = Task::init($this->db, $this->user)->get_task_by_id($task_id);
            if( $task && $task['user_id'] === $this->user->info['id'] ) {
                Task::init($this->db, $this->user)->set_profile_viewed($user_id, $task_id);
            }
        }
    }

    protected function seeker_has_task_review($seeker, $task, $reviews){
        $has_review = false;

        foreach ($reviews as $review){
            if( $review['task'] === $task && $review['seeker'] === $seeker ) {
                $has_review = true;
                break;
            }
        }

        return $has_review;
    }

    public function rating_centre(){
        set_latest_url( request_uri() );
        $this->is_logged_in();
        $taskObject = Task::init($this->db, $this->user);
        $posted = $taskObject->get_my_posted_tasks_ids();
        $performed = $taskObject->get_my_performed_tasks_ids();

        $posted_tasks = [];

        //$projects = (new Task($this->db, $this->user))->get_my_posted_and_performed_tasks();
        $my_reviews = (new RatingCentre($this->db, $this->user))->my_reviews();
        $reviews_by_me = (new RatingCentre($this->db, $this->user))->my_written_reviews();
        $reviewed_tasks = array_column(array_merge($reviews_by_me['as_poster'], $reviews_by_me['as_talent']), 'task_id');

        $reviews_by_me_ids = array_map(function($task){
            return ['task' => $task['task_id'], 'seeker' => $task['reviewer_id']];
        }, $reviews_by_me['as_talent']);

        $performed = array_filter($performed, function($task) use($reviews_by_me){
            return !in_array($task['id'], array_column($reviews_by_me['as_poster'], 'task_id'));
        });

        foreach (array_unique(array_column($posted, 'id')) as $key => $task){
            $posted_tasks[$key] = ['id' => $task, 'title' => $posted[$key]['title']];
            $posted_tasks[$key]['seekers'] = [];
            foreach ($posted as $seeker) {
                if( $seeker['id'] === $task && !$this->seeker_has_task_review($seeker['assigned_to'], $task, $reviews_by_me_ids) )
                    $posted_tasks[$key]['seekers'][] = ['id' => $seeker['assigned_to'], 'name' => $seeker['seeker_name']];
            }
        }

        $posted_tasks = array_filter($posted_tasks, function($task){ return !empty($task['seekers']); });

        $posted = $posted_tasks;
        unset($posted_tasks);

        //$performed = array_filter($projects, function($project) use($reviewed_tasks){ return (int)$project['assigned_to'] === (int) $this->user->info['id'] && !in_array($project['id'], $reviewed_tasks); });
        //unset($projects);
        $projects = ['posted' => $posted, 'performed' => $performed];

        $written_reviews = array_merge($reviews_by_me['as_poster'], $reviews_by_me['as_talent']);

        usort($written_reviews, function($first, $second){
            return Carbon::parse($first['created_at'])->equalTo(Carbon::parse($second['created_at'])) ? 0 :
                (Carbon::parse($first['created_at'])->greaterThan(Carbon::parse($second['created_at'])) ? -1 : 1);
        });

        $reviews_as_talent_dates = array_column($my_reviews['as_talent'], 'created_at');
        $reviews_as_poster_dates = array_column($my_reviews['as_poster'], 'created_at');
        sort($reviews_as_poster_dates);
        sort($reviews_as_talent_dates);
        $my_reviews['t_overall'] = 0; $my_reviews['p_overall'] = 0;
        $my_reviews['as_talent_since'] = array_shift($reviews_as_talent_dates);
        $my_reviews['as_poster_since'] = array_shift($reviews_as_poster_dates);
        $my_reviews['as_poster_top_rating'] = isset($my_reviews['as_poster'][0]) ? $my_reviews['as_poster'][0]['top_rating'] : 0;
        $my_reviews['as_talent_top_rating'] = isset($my_reviews['as_talent'][0]) ? $my_reviews['as_talent'][0]['top_rating'] : 0;
        $my_reviews['timeliness'] = $my_reviews['expertise'] = $my_reviews['satisfactory'] = $my_reviews['p_easy'] =  0;
        $my_reviews['clear'] = $my_reviews['flexibility'] = $my_reviews['trustworthiness'] = $my_reviews['t_easy'] =  0;

        foreach( $my_reviews['as_talent'] as $review ){
            $my_reviews['timeliness'] += $review['timeliness_avg'];
            $my_reviews['expertise'] += $review['expertise_avg'];
            $my_reviews['satisfactory'] += $review['satisfactory_avg'];
            $my_reviews['t_easy'] += $review['easy_avg'];
            $my_reviews['t_overall'] += $review['total_rating'];
        }

        foreach( $my_reviews['as_poster'] as $review ){
            $my_reviews['clear'] += $review['clear_avg'];
            $my_reviews['flexibility'] += $review['flexibility_avg'];
            $my_reviews['trustworthiness'] += $review['trustworthiness_avg'];
            $my_reviews['p_easy'] += $review['easy_avg'];
            $my_reviews['p_overall'] += $review['total_rating'];
        }

        $my_reviews['t_count'] = count( $my_reviews['as_talent'] );
        $my_reviews['p_count'] = count( $my_reviews['as_poster'] );

        if($my_reviews['t_count'] > 0){
            $my_reviews['t_overall'] /= $my_reviews['t_count'];
            $my_reviews['timeliness'] /= $my_reviews['t_count'];
            $my_reviews['expertise'] /= $my_reviews['t_count'];
            $my_reviews['satisfactory'] /= $my_reviews['t_count'];
            $my_reviews['t_easy'] /= $my_reviews['t_count'];
        }

        if($my_reviews['p_count'] > 0){
            $my_reviews['p_overall'] /= $my_reviews['p_count'];
            $my_reviews['clear'] /= $my_reviews['p_count'];
            $my_reviews['flexibility'] /= $my_reviews['p_count'];
            $my_reviews['trustworthiness'] /= $my_reviews['p_count'];
            $my_reviews['p_easy'] /= $my_reviews['p_count'];
        }

        if (request('review') && !isset($_SESSION['review_id']) ){
            $_SESSION['review_id'] = strtoupper(request('review'));
        }

        if (request('seeker') && !isset($_SESSION['review_seeker_id']) ){
            $_SESSION['review_seeker_id'] = strtoupper(request('seeker'));
        }

        unset($reviews_by_me);

        set('projects', $projects);
        set('ratings', $this->get_ratings($this->user->info['id']));
        set('highest_rating', $this->get_highest_rating());
        set('reviews', $my_reviews);
        set('written_reviews', $written_reviews);
        set('current_user', $this->user->info);
        set('page_title', 'My Rating Centre');

        return render('workspace/my-rating-centre.html.php', 'layout/app.html.php');
    }

    public function my_favourite(){
        $this->is_logged_in();
        $taskObject = Task::init($this->db, $this->user);
        $jobObject = Job::init($this->db, $this->user);

        $user_id = $this->user->info['id'];

        if( (isset($_SESSION[WEBSITE_PREFIX . 'FAV']) && !empty($_SESSION[WEBSITE_PREFIX.'FAV'])) || ( request('id') && request('type')) ){
            if( request('id') && request('type') ){
                $_SESSION[WEBSITE_PREFIX . 'FAV'] = [
                    'id'   => sanitize(request('id')),
                    'type' => sanitize(request('type'))
                ];
                redirect_to(option('site_uri') . url_for('/workspace/my-favourite'));
            }elseif( isset($_SESSION[WEBSITE_PREFIX . 'FAV']) ){
                $data = $_SESSION[WEBSITE_PREFIX . 'FAV'];
            }
            $favouriteObject = $data['type'] === 'task' ? $taskObject : $jobObject;
            if( ! $favouriteObject->is_favourited($data['id']) && $data['id'] !== '' && $data['type'] !== '' ) {
                $this->db->table("favourites");
                $this->db->insertArray([
                    'user_id'    => $this->user->info['id'],
                    'task_id'    => sanitize($data['id']),
                    'type'       => sanitize($data['type']),
                    'created_at' => Carbon::now()->toDateTimeString()
                ]);
                $this->db->insert();

                $task = $favouriteObject->get_by_id(sanitize($data['id']));

                $task['task_id'] = isset($task, $task['task_id']) ? $task['task_id'] : $task['job_id'];
                (new TaskActivity($this->db, $this->user))->add(['task_id' => $task['task_id'], 'task_number' => $task['number'], 'event' => 'favourite']);
            }

            unset($_SESSION[WEBSITE_PREFIX . 'FAV']);
        }

        $section = request('section') ?? 'all';

        $current_page = [
            'active_tasks' => 1,
            'inactive_tasks' => 1,
            'active_jobs' => 1,
            'inactive_jobs' => 1
        ];

        $page = request('page') ? (int)sanitize(request('page'), 'int') : 1;

        $current_page[$section] = $page;

        $per_page = 20;
        $offset = ($page * $per_page) - $per_page;

        if( $this->is_ajax() ){
            $per_page = $per_page/2;
            if( $page > 1 ){
                $offset = ($page * $per_page);
            }
        }

        $active_tasks_favourites = $inactive_tasks_favourites = $active_jobs_favourites = $inactive_jobs_favourites = [];
        $active_tasks_total = $inactive_tasks_total =$active_jobs_total = $inactive_jobs_total = 0;

        if( $section === 'all' || $section === 'active_tasks' ) {
            $active_tasks_total = $this->db->getValue("SELECT COUNT(*) FROM `favourites` 
                                            WHERE `user_id` = '{$user_id}' AND `type` = 'task'
                                            AND (
                                                    `task_id` IN( SELECT `id` FROM `tasks` WHERE `status` IN('published', 'in-progress') )
                                                    OR `task_id` IN( SELECT `id` FROM `external_tasks`  WHERE `status` IN('published', 'in-progress') )
                                              )
                                            ");
            $this->db->query("SELECT DISTINCT `task_id`, `type`, `created_at`,
                          CASE WHEN `task_id` IN( SELECT `task_id` FROM `user_task` WHERE `user_id` = '{$user_id}' AND `type` = 'task' ) THEN 0 ELSE 1 END AS `priority`   
                          FROM `favourites` 
                          WHERE `user_id` = '{$user_id}' AND `type` = 'task' 
                          AND (
                                `task_id` IN( SELECT `id` FROM `tasks` WHERE `status` IN('published', 'in-progress') )
                                OR `task_id` IN( SELECT `id` FROM `external_tasks`  WHERE `status` IN('published', 'in-progress') )
                          ) 
                          ORDER BY `priority` DESC, `created_at` DESC LIMIT {$offset}, {$per_page}");

            $active_tasks_favourites = $this->db->getRowList();
            $active_tasks_favourites = array_unique($active_tasks_favourites, SORT_REGULAR);
        }

        if( $section === 'all' || $section === 'inactive_tasks' ) {
            $inactive_tasks_total = $this->db->getValue("SELECT COUNT(*) FROM `favourites` 
                                            WHERE `user_id` = '{$user_id}' AND `type` = 'task'
                                            AND (
                                                    `task_id` IN( SELECT `id` FROM `tasks` WHERE `status` NOT IN('published', 'in-progress') )
                                                    OR `task_id` IN( SELECT `id` FROM `external_tasks` WHERE `status` NOT IN('published', 'in-progress') )
                                              )
                                            ");
            $this->db->query("SELECT DISTINCT `task_id`, `type`, `created_at`,
                          CASE WHEN `task_id` IN( SELECT `task_id` FROM `user_task` WHERE `user_id` = '{$user_id}' AND `type` = 'task' ) THEN 0 ELSE 1 END AS `priority`   
                          FROM `favourites` 
                          WHERE `user_id` = '{$user_id}' AND `type` = 'task' 
                          AND (
                                `task_id` IN( SELECT `id` FROM `tasks` WHERE `status` NOT IN('published', 'in-progress') )
                                OR `task_id` IN( SELECT `id` FROM `external_tasks`  WHERE `status` NOT IN('published', 'in-progress') )
                          ) 
                          ORDER BY `priority` DESC, `created_at` DESC LIMIT {$offset}, {$per_page}");

            $inactive_tasks_favourites = $this->db->getRowList();
            $inactive_tasks_favourites = array_unique($inactive_tasks_favourites, SORT_REGULAR);
        }

        if( $section === 'all' || $section === 'active_jobs' ) {
            $active_jobs_total = $this->db->getValue("SELECT COUNT(*) FROM `favourites` 
                                           WHERE `user_id` = '{$user_id}' AND `type` = 'job'
                                           AND (
                                                    `task_id` IN( SELECT `id` FROM `jobs` WHERE `status` IN('published') )
                                                    OR `task_id` IN( SELECT `id` FROM `external_jobs` WHERE `status` IN('published') )
                                              )
                                           ");
            $this->db->query("SELECT DISTINCT `task_id`, `type`, `created_at`,
                          CASE WHEN `task_id` IN( SELECT `task_id` FROM `user_task` WHERE `user_id` = '{$user_id}' AND `type` = 'job' ) THEN 0 ELSE 1 END AS `priority`   
                          FROM `favourites` 
                          WHERE `user_id` = '{$user_id}' AND `type` = 'job' 
                          AND (
                                `task_id` IN( SELECT `id` FROM `jobs` WHERE `status` IN('published') )
                                OR `task_id` IN( SELECT `id` FROM `external_jobs`  WHERE `status` IN('published') )
                          ) 
                          ORDER BY `priority` DESC, `created_at` DESC LIMIT {$offset}, {$per_page}");
            $active_jobs_favourites = $this->db->getRowList();
            $active_jobs_favourites = array_unique($active_jobs_favourites, SORT_REGULAR);
        }

        if( $section === 'all' || $section === 'inactive_jobs' ) {
            $inactive_jobs_total = $this->db->getValue("SELECT COUNT(*) FROM `favourites` 
                                           WHERE `user_id` = '{$user_id}' AND `type` = 'job'
                                           AND (
                                                    `task_id` IN( SELECT `id` FROM `jobs` WHERE `status` NOT IN('published') )
                                                    OR `task_id` IN( SELECT `id` FROM `external_jobs` WHERE `status` NOT IN('published') )
                                              )
                                           ");


            $this->db->query("SELECT DISTINCT `task_id`, `type`, `created_at`,
                          CASE WHEN `task_id` IN( SELECT `task_id` FROM `user_task` WHERE `user_id` = '{$user_id}' AND `type` = 'job' ) THEN 0 ELSE 1 END AS `priority`   
                          FROM `favourites` 
                          WHERE `user_id` = '{$user_id}' AND `type` = 'job' 
                          AND (
                                `task_id` IN( SELECT `id` FROM `jobs` WHERE `status` NOT IN('published') )
                                OR `task_id` IN( SELECT `id` FROM `external_jobs`  WHERE `status` NOT IN('published') )
                          ) 
                          ORDER BY `priority` DESC, `created_at` DESC LIMIT {$offset}, {$per_page}");
            $inactive_jobs_favourites = $this->db->getRowList();
            $inactive_jobs_favourites = array_unique($inactive_jobs_favourites, SORT_REGULAR);
        }

        $favourites = array_merge($active_tasks_favourites, $inactive_tasks_favourites, $active_jobs_favourites, $inactive_jobs_favourites);


        $applied['tasks'] = $taskObject->get_applied_tasks($this->user->info['id']);
        $applied['ext_tasks'] = $taskObject->get_applied_external_tasks($this->user->info['id']);

        $applied_tasks_ids = array_map(function ($task){
            return $task['id'];
        }, $applied['tasks']);

        $applied_ext_tasks_ids = array_map(function ($task){
            return $task['id'];
        }, $applied['ext_tasks']);

        $applied['ids'] = array_merge($applied_tasks_ids, $applied_ext_tasks_ids);

        $applied['jobs'] = $jobObject->get_applied_jobs($this->user->info['id']);
        $applied['ext_jobs'] = $jobObject->get_applied_external_jobs($this->user->info['id']);

        $applied_jobs_ids = array_map(function ($job){
            return $job['id'];
        }, $applied['jobs']);

        $applied_ext_jobs_ids = array_map(function ($job){
            return $job['id'];
        }, $applied['ext_jobs']);

        $applied_jobs_ids = array_merge($applied_jobs_ids, $applied_ext_jobs_ids);

        $applied['ids'] = array_merge($applied['ids'], $applied_jobs_ids);

        $all = [];
        $ids = [];
        foreach ($favourites as $favourite){
            array_push($ids, $favourite['task_id']);

            if($favourite['type'] === 'task'){
                $fav = $taskObject->get_task_by_id($favourite['task_id']);
                if( empty($fav) )
                    $fav = $taskObject->get_external_task_by_id($favourite['task_id']);
            }else{
                $fav = $jobObject->get_job_by_id($favourite['task_id']);
                if( empty($fav) )
                    $fav = $jobObject->get_external_job_by_id($favourite['task_id']);
            }

            if(isset($fav['password'])) unset($fav['password']);
            $all[$fav['task_id'] ?? $fav['job_id']] = $fav;
        }

        foreach ($all as $key => $task){
            if(isset($task['task_id'])){
                if( !isset($task['source']) ) {
                    $all[$key]['type']            = 'internal_task';
                    $all[$key]['country']         = $task['country'] ? $this->getCountryName($task['country']) : '';
                    $all[$key]['state']           = $task['state'] ? $this->getStateName($task['state']) : '';
                    //$all[$key]['published_count'] = (new Task($this->db, $this->user))->get_published_tasks_count($task['user_id']);
                    //$all[$key]['rating']          = $this->get_ratings($task['user_id']);
                    //$all[$key]['questions']       = (new Task($this->db, $this->user))->questions($task['task_id']);
                }else{
                    @list($state, $country) = explode(', ', $task['state_country'], 2);
                    $all[$key]['type']            = 'external_task';
                    $all[$key]['country']         = $country;
                    $all[$key]['state']           = $state;
                }
            }else{
                if( !isset($task['source']) ) {
                    $all[$key]['type']            = 'internal_job';
                    $all[$key]['country']         = $this->getCountryName($task['country']);
                    $all[$key]['state']           = $this->getStateName($task['state']);
                    $all[$key]['job_country']         = $this->getCountryName($task['job_country']);
                    $all[$key]['job_state']           = $this->getStateName($task['job_state']);
                    //$all[$key]['published_count'] = (new Job($this->db, $this->user))->get_published_jobs_count($task['user_id']);
                    //$all[$key]['rating']          = $this->get_ratings($task['user_id']);
                    //$all[$key]['questions']       = (new Job($this->db, $this->user))->questions($task['job_id']);
                }else{
                    $all[$key]['type']            = 'external_job';
                    $all[$key]['country']         = $task['country'];
                    $all[$key]['state']           = $task['state'];
                    $all[$key]['job_country'] = $this->getCountryName($task['country']);
                    $all[$key]['job_state']   = $task['state'];
                }
            }
        }

        /*$active_tasks = array_filter($all, function($task){ return in_array($task['status'], ['published', 'in-progress']); });
        $inactive_tasks = array_filter($all, function($task){ return !in_array($task['status'], ['published', 'in-progress']); });
        $active_jobs = array_filter($all, function($task){ return in_array($task['status'], ['published']); });
        $inactive_jobs = array_filter($all, function($task){ return !in_array($task['status'], ['published']); });*/
        //dd( $all, array_column($active_tasks_favourites, 'task_id') );
        $all_favourites = [
            'active_tasks' => $active_tasks_favourites,
            'inactive_tasks' => $inactive_tasks_favourites,
            'active_jobs' => $active_jobs_favourites,
            'inactive_jobs' => $inactive_jobs_favourites
        ];

        $total = [
            'active_tasks' => $active_tasks_total,
            'inactive_tasks' => $inactive_tasks_total,
            'active_jobs' => $active_jobs_total,
            'inactive_jobs' => $inactive_jobs_total
        ];

        $total['active_tasks_more'] = ceil($total['active_tasks']/$per_page) > $page;
        $total['inactive_tasks_more'] = ceil($total['inactive_tasks']/$per_page) > $page;
        $total['active_jobs_more'] = ceil($total['active_jobs']/$per_page) > $page;
        $total['inactive_jobs_more'] = ceil($total['inactive_jobs']/$per_page) > $page;

        /*foreach ($all as $favourite):
            if( !in_array($favourite['job_id'], array_column($all_favourites['active_jobs'], 'task_id')) ) continue;
            dump($favourite['job_id']);
        endforeach;
        dd(array_column($all_favourites['active_jobs'], 'task_id'));*/
        //$pagination = new pagination($all_favourites, $per_page, $page, option('site_uri') . url_for('workspace/my-favourite'));

        set('current_page', $current_page);
        set('all', $all_favourites);
        set('favourites', $ids);
        set('favourites_list', $all);
        set('applied', $applied);
        set('current_user', $this->user->info);
        set('page_title', 'My Favourite');
        set('total', $total);
        //set('pagination', $pagination);

        if( $this->is_ajax() ){
            $total_pages = ceil($total[$section]/$per_page);
            if( $section === 'active_tasks' ) {
                return json(['more' => ($page < $total_pages), 'html' => partial('workspace/partial/my-favourite-active-tasks-more.html.php')]);
            }elseif( $section === 'inactive_tasks' ) {
                return json(['more' => ($page < $total_pages), 'html' => partial('workspace/partial/my-favourite-inactive-tasks-more.html.php')]);
            }elseif( $section === 'active_jobs' ) {
                return json(['more' => ($page < $total_pages), 'html' => partial('workspace/partial/my-favourite-active-jobs-more.html.php')]);
            }elseif( $section === 'inactive_jobs' ) {
                return json(['more' => ($page < $total_pages), 'html' => partial('workspace/partial/my-favourite-inactive-jobs-more.html.php')]);
            }
        }else {
            return render('workspace/my-favourite.html.php', 'layout/app.html.php');
        }
    }

    public function create_review(){
        if(lemon_csrf_require_valid_token()) {
            if( request('task') && ((request('talent') && request('seeker')) || request('poster')) && request('review') ){
                $task = Task::init($this->db, $this->user)->get_task_by_id( sanitize(request('task')) );
                if($task && in_array($task['status'], ['in-progress', 'completed', 'closed', 'disputed'])){
                    $table = request('talent') ? 'poster_reviews' : 'talent_reviews';
                    $data  = request('talent') ? sanitize(request('talent')) : sanitize(request('poster'));

                    $data['reviewer_id'] = $this->user->info['id'];

                    if ( request('talent') ) {
                        $data['talent_id'] = sanitizeItem(request('seeker'));
                    } elseif (request('poster')) {
                        $data['poster_id'] = $task['user_id'];
                    }

                    $this->db->table($table);
                    $data = array_merge($data, [
                        'task_id' => $task['task_id'],
                        'review' => mysqli_real_escape_string($this->db->connection, sanitize(request('review'))),
                    ]);

                    $this->db->insertArray( $data );
                    $this->db->insert();

                    $this->db->query("SELECT CONCAT(m.`firstname`, ' ', m.`lastname`) AS `name`, d.`name` AS `company_name`
                                      FROM `members` m
                                      LEFT JOIN `company_details` d ON m.`type` = '1' AND d.`member_id` = m.`id`
                                      WHERE m.`id` = '{$data['reviewer_id']}'
                                      LIMIT 1");
                    $task['reviewer'] = $this->db->getSingleRow();

                    $task['from'] = $data['reviewer_id'];
                    if( $table === 'poster_reviews' ){
                        $task['to'] = $data['talent_id'];

                        $poster_status = Task::init($this->db, $this->user)->get_poster_task_status($task['task_id'], $data['talent_id'], $data['reviewer_id']);
                        $seeker_status = Task::init($this->db, $this->user)->get_seeker_task_status($task['task_id'], $data['talent_id']);

                        if($seeker_status !== 'disputed') {
                            // set poster task status to be 'closed'
                            $this->db->table('poster_seeker');
                            $this->db->updateArray(['status' => 'closed']);
                            $this->db->whereArray(['task_id' => $task['task_id'], 'seeker_id' => $data['talent_id'], 'poster_id' => $data['reviewer_id']]);
                            $this->db->update();
                        }else{
                            // set poster task status to be 'disputed' after rating ( as final status )
                            $this->db->table('poster_seeker');
                            $this->db->updateArray(['status' => 'disputed']);
                            $this->db->whereArray(['task_id' => $task['task_id'], 'seeker_id' => $data['talent_id'], 'poster_id' => $data['reviewer_id']]);
                            $this->db->update();
                        }

                        Milestone::update($this->db,
                            [ 'performed' => '1'],
                            [
                                'task_id' => $task['task_id'],
                                'by_id'   => $data['talent_id'],
                                'for_id'  => $data['reviewer_id']
                            ]
                        );

                    }else{
                        $task['to'] = $data['poster_id'];

                        $seeker_status = Task::init($this->db, $this->user)->get_seeker_task_status($task['task_id'], $data['reviewer_id']);

                        if($seeker_status !== 'disputed') {
                            // set seeker task status to be 'closed'
                            $this->db->table('user_task');
                            $this->db->updateArray(['status' => 'closed']);
                            $this->db->whereArray(['task_id' => $task['task_id'], 'user_id' => $data['reviewer_id'], 'type' => 'task']);
                            $this->db->update();
                        }

                        Milestone::update($this->db,
                            [ 'performed' => '1'],
                            [
                                'task_id' => $task['task_id'],
                                'by_id'   => $task['user_id'],
                                'for_id'  => $data['reviewer_id']
                            ]
                        );
                    }
                    Event::trigger('task.review', [$task]);

                    flash('message', lang('review_was_posted_successfully'));
                    flash('status', 'success');
                }else {
                    flash('message', lang('sorry_you_cannot_review_this_task_until_it_is_closed'));
                    flash('status', 'danger');
                }
            }else{
                flash('message', lang('failed_to_save_your_review_please_try_again'));
                flash('status', 'danger');
            }
        }

        $url = option('site_uri') . url_for('/workspace/rating-centre') . '#pills-rc-writereview';/*isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] . '#pills-rc-writereview' :*/
        redirect($url);
    }

    public function get_reviews($user_id = null){
        if(null === $user_id) return [];

        $reviews = (new RatingCentre($this->db, $this->user))->user_reviews($user_id);
        $reviews = array_merge( $reviews['as_poster'], $reviews['as_talent'] );

        return $reviews;
    }

    public function get_ratings($user_id = null){
        if(null === $user_id) return [];
        $rating_centre = new RatingCentre($this->db, $this->user);
        $user_reviews = $rating_centre->user_reviews($user_id);
        $reviews = ['overall' => 0];
        $reviews['as_poster_top_rating'] = isset($user_reviews['as_poster'][0]) ? $user_reviews['as_poster'][0]['top_rating'] : 0;
        $reviews['as_talent_top_rating'] = isset($user_reviews['as_talent'][0]) ? $user_reviews['as_talent'][0]['top_rating'] : 0;
        $reviews['timeliness'] = $reviews['expertise'] = $reviews['satisfactory'] = $reviews['p_easy'] =  0;
        $reviews['clear'] = $reviews['flexibility'] = $reviews['trustworthiness'] = $reviews['t_easy'] =  0;

        $reviews_as_talent_dates = array_column($user_reviews['as_talent'], 'created_at');
        $reviews_as_poster_dates = array_column($user_reviews['as_poster'], 'created_at');
        $reviews_dates = array_merge( $reviews_as_poster_dates, $reviews_as_talent_dates );
        sort($reviews_dates);

        $reviews['since'] = array_shift($reviews_dates);

        foreach( $user_reviews['as_talent'] as $review ){
            $reviews['timeliness'] += $review['timeliness_avg'];
            $reviews['expertise'] += $review['expertise_avg'];
            $reviews['satisfactory'] += $review['satisfactory_avg'];
            $reviews['t_easy'] += $review['easy_avg'];
            $reviews['overall'] += $review['total_rating'];
        }

        foreach( $user_reviews['as_poster'] as $review ){
            $reviews['clear'] += $review['clear_avg'];
            $reviews['flexibility'] += $review['flexibility_avg'];
            $reviews['trustworthiness'] += $review['trustworthiness_avg'];
            $reviews['p_easy'] += $review['easy_avg'];
            $reviews['overall'] += $review['total_rating'];
        }
        $reviews['count'] = count( array_merge($user_reviews['as_talent'], $user_reviews['as_poster']) );
        if($reviews['count'] > 0){
            $reviews['overall'] /= $reviews['count'];
            $reviews['timeliness'] /= $reviews['count'];
            $reviews['expertise'] /= $reviews['count'];
            $reviews['satisfactory'] /= $reviews['count'];
            $reviews['t_easy'] /= $reviews['count'];
            $reviews['clear'] /= $reviews['count'];
            $reviews['flexibility'] /= $reviews['count'];
            $reviews['trustworthiness'] /= $reviews['count'];
            $reviews['p_easy'] /= $reviews['count'];
        }

        return $reviews;
    }

    public function get_chats($user_id = null, $task_id = null){
        if(null === $user_id && null === $task_id) return [];
        $user_id = $this->db->escape($user_id);
        $this->db->query("
        SELECT * 
        FROM `chats`
        WHERE `receiver_id` = '{$user_id}' AND `task_id` = '{$task_id}'");
        $chats = [];

        foreach($this->db->getRowList() as $chat){
            $chats[$chat['task_id']][] = $chat;
        }

        return $chats;
    }

    public function has_new_chats($user_id = null, $task_id = null){
        if(null === $user_id && null === $task_id) return [];
        $user_id = $this->db->escape($user_id);
        $this->db->query("
        SELECT * 
        FROM `chats`
        WHERE `receiver_id` = '{$user_id}' AND `task_id` = '{$task_id}' AND `viewed` = 0");
        $chats = [];

        foreach($this->db->getRowList() as $chat){
            $chats[] = $chat['task_id'];
        }

        return !empty($chats);
    }

    public function sent_new_chats($user_id = null, $task_id = null){
        if( ! isset($user_id, $task_id) ) return [];

        $user_id = $this->db->escape($user_id);
        $task_id = $this->db->escape($task_id);

        $this->db->query("
        SELECT * 
        FROM `chats`
        WHERE `sender_id` = {$user_id} AND `task_id` = {$task_id} AND `viewed` = 0");
        $chats = [];

        foreach($this->db->getRowList() as $chat){
            $chats[] = $chat['task_id'];
        }

        return !empty($chats);
    }

    public function get_highest_rating($user_id = null){
        $user_id = $user_id ?? $this->user->info['id'];
        $this->db->query("SELECT 
                        `user_id`,
                        COALESCE(CONVERT((`timeliness`+`quality`+`communication`+`responsiveness`)/4, DECIMAL(10,1)), 0) AS `user_rating`
                        FROM `user_rating`
                        WHERE `user_id` = '{$user_id}'
                        GROUP BY `user_id`
                        ORDER BY `user_rating` DESC LIMIT 1");
        return !empty($this->db->getSingleRow()) ? $this->db->getSingleRow() : ['user_rating' => 0];
    }

    public function create_comment(){
        if(lemon_csrf_require_valid_token()) {
            if( request('post_id') && request('user_id') && request('comment') ){
                $this->db->table("comments");
                $this->db->insertArray([
                    'user_id'   => sanitize(request('user_id')),
                    'post_id'   => sanitize(request('post_id')),
                    'comment'   => sanitize(request('comment')),
                    'parent_id' => sanitize(request('parent_id')),
                ]);
                $this->db->insert();
                $comment = $this->get_comment( $this->db->insertid() );

                if( request('post_type') === 'task' ) {
                    $post = (new Task($this->db, $this->user))->get_task_by_id(sanitize(request('post_id')));
                    $comment['task_id'] = $post['task_id'];
                }else{
                    $post = (new Job($this->db, $this->user))->get_job_by_id(sanitize(request('post_id')));
                    $comment['job_id'] = $post['job_id'];
                }
                $mail = new Mail($this->db);
                $comment['title'] = $post['title'];
                $comment['slug'] = $post['slug'];
                $comment['user_type'] = $post['user_type'];
                $comment['user_id'] = $post['user_id'];
                $comment['company_name'] = $post['company_name'];

                if( $post['user_id'] === $this->user->info['id'] ) {
                    $comment_id = sanitize(request('parent_id'));
                    $this->db->query("SELECT * FROM `comments` WHERE `id` = '{$comment_id}' LIMIT 1");
                    $to = $this->db->getSingleRow();
                    $comment['to'] = $to['user_id'];

                    $this->db->query("SELECT `firstname`, `email` FROM `members` WHERE `id` = '{$to['user_id']}' LIMIT 1");
                    $seeker = $this->db->getSingleRow();

                    if( request('post_type') === 'task' ) {
                        Event::trigger('task.new.answer', [$comment]);
                        $mail->sendmail(
                            [
                                'email' => $seeker['email'],
                                'name'  => $seeker['firstname']
                            ],
                            'you-received-answer',
                            [
                                'first_name' => $seeker['firstname'],
                                'question'   => $to['comment'],
                                'answer'     => sanitize(request('comment')),
                                'task'       => $post,
                                'type'       => 'task'
                            ]
                        );
                    }else {
                        Event::trigger('job.new.answer', [$comment]);
                        $mail->sendmail(
                            [
                                'email' => $seeker['email'],
                                'name'  => $seeker['firstname']
                            ],
                            'you-received-answer',
                            [
                                'first_name' => $seeker['firstname'],
                                'question'   => $to['comment'],
                                'answer'     => sanitize(request('comment')),
                                'task'       => $post,
                                'type'       => 'job'
                            ]
                        );
                    }
                }else {
                    $this->db->query("SELECT `firstname`, `email` FROM `members` WHERE `id` = '{$post['user_id']}' LIMIT 1");
                    $poster = $this->db->getSingleRow();

                    if( request('post_type') === 'task' ) {
                        Event::trigger('task.new.question', [$comment]);
                        $mail->sendmail(
                            [
                                'email' => $poster['email'],
                                'name'  => $poster['firstname']
                            ],
                            'you-received-question',
                            [
                                'first_name' => $poster['firstname'],
                                'question'   => sanitize(request('comment')),
                                'task'       => $post,
                                'type'       => 'task'
                            ]
                        );
                    }else {
                        $mail->sendmail(
                            [
                                'email' => $poster['email'],
                                'name'  => $poster['firstname']
                            ],
                            'seeker-posted-question',
                            [
                                'first_name' => $poster['firstname'],
                                'question'   => sanitize(request('comment')),
                                'task'       => $post,
                                'type'       => 'job'
                            ]
                        );
                        Event::trigger('job.new.question', [$comment]);
                    }
                }
            }
        }

        $url = isset($_SERVER['HTTP_REFERER']) ? url_for($_SERVER['HTTP_REFERER']) : option('site_uri') . url_for('/workspace/search');
        redirect( $url );
    }

    public function get_comment($comment_id = null){
        if(null === $comment_id) return [];

        $this->db->query("
                        SELECT *, c.`id` AS `comment_id`, c.`viewed` FROM `comments` c
                        INNER JOIN `members` m ON m.`id` = c.`user_id`
                        WHERE c.`id` = '{$comment_id}'
                        LIMIT 1");
        return $this->db->getSingleRow();
    }

    public function get_comments($post_id = null, $user_id = null){
        if(null === $post_id) return [];

        $query = "SELECT *, c.`id` AS `comment_id`, c.`viewed` FROM `comments` c
                  INNER JOIN `members` m ON m.`id` = c.`user_id`
                  WHERE c.`post_id` = '{$post_id}' AND c.`parent_id` = '0'";

        /*if( $user_id != $this->user->info['id']  ){
            $query .= " AND c.`hidden` = '0' ";
        }*/

        $query .= " ORDER BY c.`created_at` DESC";

        $this->db->query($query);
        return $this->db->getRowList();
    }

    public function get_comment_answers($post_id = null, $parent_id = null){
        if(null === $post_id || null === $parent_id) return [];

        $this->db->query("
                        SELECT *, c.`id` AS `comment_id`, c.`viewed`, d.`name` AS `company_name` 
                        FROM `comments` c
                        INNER JOIN `members` m ON m.`id` = c.`user_id`
                        LEFT JOIN `company_details` d ON m.`type` = '1' AND d.`member_id` = m.`id`
                        WHERE c.`parent_id` = '{$parent_id}' AND c.`post_id` = '{$post_id}'
                        ORDER BY c.`created_at` ASC");
        return $this->db->getRowList();
    }

    public function mark_comments_as_read($task_id = null){
        if( is_null($task_id) ) return;
        $this->db->table('comments');
        $this->db->whereArray([
            'post_id' => $task_id
        ]);
        $this->db->updateArray([
            'viewed' => 'Y'
        ]);
        $this->db->update();
    }

    public function hide_comment($comment_id = null, $post_id= null, $type = 'task', $state = 0){
        $comment_id = ! is_null($comment_id) ? $comment_id : request('comment_id');
        $post_id = ! is_null($post_id) ? $post_id : request('post_id');
        $state = request('state') ?? $state;
        $type = request('type') ?? $type;

        if( is_null($comment_id) || is_null($post_id) ) return;

        $post = ($type === 'task') ? Task::init($this->db, $this->user) : Job::init($this->db, $this->user);

        if( ! $post->owner($this->user->info['id'], $post_id) ) return;

        $this->db->table('comments');
        $this->db->whereArray([
            'id' => $comment_id,
            'post_id' => $post_id
        ]);
        $this->db->updateArray([
            'hidden' => $state
        ]);
        $this->db->update();

        echo json(['status' =>'success', 'title' => lang('success'), 'message' => $state === '0' ? lang('question_is_visible') : lang('question_is_hidden')]);
    }

    public function applicants(){
        /*if($this->is_logged_in() && $this->is_ajax() === false) {
            redirect_to($_SERVER['HTTP_REFERER'] ?? '/workspace');
            halt(HTTP_FORBIDDEN);
        }*/
        $limit = 10;
        $task_id = params('task');
        $page = (int)params('page');
        $task = new Task($this->db, $this->user);
        $applicants = $task->get_applicants([$task_id], ($page - 1) * $limit, $limit);
        $total = $task->get_total_applicants($task_id)['count'];
        $applicants = array_map(function(&$applicant){
           unset($applicant['password']);
            $applicant['rating'] = $this->get_ratings($applicant['user_id']);
            $applicant['reviews'] = $this->get_reviews($applicant['user_id']);
            $applicant['has_new_updates'] = $this->get_applicants_new_updates($applicant['user_id'], $applicant['task_id'], $applicant);
           return $applicant;
        }, $applicants);

        $current_task = $task->get_task_by_id($task_id);
        if(!empty($current_task)) unset($current_task['password']);
        $current_task['id'] = $current_task['task_id'];

        /*if( $this->is_ajax() ){*/
            $picked_applicants = $task->get_task_picked_applicants(null, $current_task['task_id']);
            $selected_applicants = $all_applicants = [];

            foreach ($picked_applicants as $applicant){
                $applicant['poster_status'] = $task->get_poster_task_status($applicant['task_id'], $applicant['seeker_id'], $applicant['poster_id']);
                $applicant['seeker_status'] = $task->get_seeker_task_status($applicant['task_id'], $applicant['seeker_id']);

                $all_applicants[$applicant['seeker_id']] = $applicant;
                $selected_applicants[$applicant['task_id']][$applicant['seeker_id']] = $applicant;
            }

            $tasks['applicants'] = $applicants;

            $actions = $task->get_tasks_milestone_action($current_task['task_id']);
            $payments = $task->get_tasks_payment($current_task['task_id']);

            if(!empty($current_task)) unset($current_task['password']);
            set('selected_applicants', $selected_applicants);
            set('tasks', $tasks);
            //echo json(['page' => $page, 'count' => $total, 'task' => $task->get_task_by_id($task_id), 'applicants' => $applicants]);
            echo partial('/workspace/partial/task-centre/applicants.html.php',
                [
                    'applicants'          => $applicants,
                    'task'                => $current_task,
                    'count'               => $total,
                    'page'                => $page,
                    'selected_applicants' => $selected_applicants,
                    'actions'             => $actions,
                    'payments'            => $payments,
                    'calendar'            => isset( $_SESSION['current_task_centre_view'] ) && $_SESSION['current_task_centre_view'] === 'calendar' ? true : null
                ]
            );
            /*die;
        }else {

            //echo json(['page' => $page, 'count' => $total, 'task' => $task->get_task_by_id($task_id), 'applicants' => $applicants]);
            echo partial('/workspace/partial/task-centre/applicants.html.php',
                [
                    'applicants' => $applicants,
                    'task'       => $current_task,
                    'count'      => $total,
                    'page'       => $page
                ]
            );
        }*/
    }

    public function get_applicants_new_updates($user_id = null, $task_id = null, $applicant = []){
        if( !isset($user_id, $task_id) ) return false;

        $new_chats = $this->sent_new_chats($user_id, $task_id);
        $new_applicant = !empty($applicant) && isset($applicant['viewed']) && $applicant['viewed'] === 'N';

        return $new_chats || $new_applicant;

    }

    public function assign(){
        if(lemon_csrf_require_valid_token() && request('task_id') && request('user_id')) {
            if (request('task_id') && request('user_id')) {
                $user_id  = request('user_id');
                $task_id  = request('task_id');
                $job_id   = request('job_id');
                $owner_id = $this->user->info['id'];

                if ($task_id) {
                    $task = new Task($this->db, $this->user);
                } elseif ($job_id) {
                    $task = new Job($this->db, $this->user);
                }

                if ($task && $task->owner($owner_id, $task_id)) {
                    if ($task->has_applicant($user_id, $task_id)) {
                        //if(!$task->is_published($task_id, $owner_id)) {
                        $talent             = $this->get_talent($user_id, false);
                        $photo              = $this->db->getValue("SELECT `photo` FROM `members` WHERE `id` = '{$this->user->info['id']}' LIMIT 1");
                        $talent['photo']    = $photo;
                        $the_task           = $task->get_task_by_id($task_id);
                        $the_task           = $task->set_service_charge($the_task);
                        $tags               = $task->tags($task_id);
                        $the_task['tags']   = $tags;
                        $the_task['skills'] = $this->get_skills($the_task['skills']);
                        if (isset($the_task['password'])) unset($the_task['password']);

                        $this->db->query("SELECT * FROM task_payment WHERE user_id = " . $this->db->escape($this->user->id) . " AND task_id = " . $this->db->escape($task_id) . " AND assigned_to = " . $this->db->escape($user_id) . " AND status = '1'");
                        $task_is_paid = $this->db->getSingleRow();

                        $payment_id = flash_now('payment_id') ? flash_now('payment_id') : (!empty($task_is_paid['id']) ? $task_is_paid['id'] : '');
                        $this->db->query("SELECT * FROM task_payment WHERE id = " . $this->db->escape($payment_id));
                        $task_payment = $this->db->getSingleRow();

                        set('task_payment', $task_payment);
                        set('msg', str_replace('_', ' ', flash_now('msg')));

                        if (!$task_is_paid) {
                            $this->db->table('task_payment');
                            $this->db->updateArray(['status' => '999']);
                            $this->db->whereArray([
                                'user_id' => $this->user->id,
                                'task_id' => $task_id,
                                'assigned_to' => $user_id,
                                'status' => '0'
                            ]);
                            $this->db->update();

                            $this->db->table('task_payment');
                            $this->db->insertArray([
                                'user_id' => $this->user->id,
                                'task_id' => $task_id,
                                'assigned_to' => $user_id,
                                'total' => $the_task['budget'],
                                'service_charge' => $the_task['service_charge'],
                                'name' => $this->user->info['name'],
                                'email' => $this->user->info['email'],
                                'phone' => $this->user->info['mobile_number'],
                                'date' => 'NOW()'
                            ]);
                            $this->db->insert();
                            $payment_id = $this->db->insertid();

                            $senangpay['detail']   = str_replace(['"', "'"], '', $the_task['title']);
                            $senangpay['amount']   = number_format($the_task['budget'] + $the_task['service_charge'], 2, '.', '');
                            $senangpay['order_id'] = $payment_id;
                            $senangpay['name']     = $this->user->info['name'];
                            $senangpay['email']    = $this->user->info['email'];
                            $senangpay['phone']    = $this->user->info['mobile_number'];
                            $senangpay['hash']     = $hashed_string = hash_hmac('sha256', SENANGPAY['secret_key'] . urldecode($senangpay['detail']) . urldecode($senangpay['amount']) . urldecode($senangpay['order_id']), SENANGPAY['secret_key']);

                            set('senangpay', $senangpay);
                        }else{
                            Milestone::update($this->db, [
                               'performed' => '1'
                            ],[
                                'task_id' => $task_id,
                                'by_id' => $user_id,
                                'for_id' => $this->user->id
                            ]);

                            if( empty(Milestone::get($this->db, ['task_id' => $task_id, 'by_id' => $the_task['user_id'], 'for_id' => $user_id, 'required_action' => 'confirmed'])) ){
                                Milestone::save($this->db, [
                                    'task_id' => $task_id,
                                    'by_id' => $the_task['user_id'],
                                    'for_id' => $user_id,
                                    'required_action' => 'confirmed'
                                ]);
                            }


                            $status = $this->date::parse($the_task['start_by'])->isFuture() && !$this->date::parse($the_task['start_by'])->isToday() ? 'waiting' : 'in-progress';

                            $this->db->table('poster_seeker');
                            $this->db->updateArray(['status' => $status]);
                            $this->db->whereArray([
                                'task_id' => $task_id,
                                'seeker_id' => $user_id,
                                'poster_id' => $this->user->id
                            ]);
                            $this->db->update();

                            $this->db->table('user_task');
                            $this->db->updateArray(['status' => $status]);
                            $this->db->whereArray([
                                'task_id' => $task_id,
                                'user_id' => $user_id
                            ]);
                            $this->db->update();
                        }

                        //$the_task['title']

                        set('assigned', $task_is_paid);
                        set('task', $the_task);
                        set('talent', $talent);
                        set('current_user', $this->user->info);
                        set('page_title', 'Payment Details');
                        set('date', $this->get_date());
                        set('redirect', url_for('workspace/task-centre/posted/list'));
                        return render("/workspace/payment-details.html.php", "layout/app.html.php");
                        //}
                    }
                }
            } else {
                flash('message', lang('failed_to_assign_task_please_try_again'));
                flash('status', 'danger');
            }
        }

        redirect_to($this->site_host . ROOTPATH . 'workspace/' . $this->date->year . '/' . $this->date->month);

    }

    public function hire(){
        $redirect_to = $_SERVER['HTTP_REFERER'];
        if(lemon_csrf_require_valid_token() && request('task_id') && request('user_id')) {
            $user_id  = request('user_id');
            $task_id  = request('task_id');
            $owner_id = $this->user->info['id'];
            $task     = new Task($this->db, $this->user);

            if ($task && $task->owner($owner_id, $task_id)) {
                if ($task->has_applicant($user_id, $task_id) && !$task->is_completed($task_id, $owner_id)) {
                    if($task->assign($user_id, $task_id)) {
                        if( $this->is_ajax() ) {
                            echo json(['message' => lang('Talent will be notified'), 'status' => 'success']);
                        }else{
                            flash('message', lang('Talent will be notified'));
                            flash('status', 'success');
                            redirect_to($redirect_to . "#" . strtolower($task_id));
                        }
                    }else{
                        if( $this->is_ajax() ) {
                            echo json(['message' => lang('Failed to hire candidate, please try again'), 'status' => 'danger']);
                        }else{
                            flash('message', lang('Failed to hire candidate, please try again'));
                            flash('status', 'danger');
                            redirect_to($redirect_to . "#" . strtolower($task_id));
                        }
                    }
                }
            }
        }else{
            flash('message', lang('Failed to hire candidate, please try again'));
            flash('status', 'danger');
        }
        redirect_to($redirect_to . "#" . strtolower($task_id));
    }

    public function offer(){

        $redirect_to = $_SERVER['HTTP_REFERER'];
        if(lemon_csrf_require_valid_token() && request('task_id') && request('user_id')) {
            $user_id  = sanitize(request('user_id'), 'int');
            $task_id  = sanitize(request('task_id'));
            $task     = new Task($this->db, $this->user);
            $the_task = $task->get_task_by_id($task_id);
            $owner_id = $the_task['user_id'];

            if ($the_task && $task->has_applicant($user_id, $task_id) ) {
                if ( !is_null(request('accept')) ) {
                    $result = $task->accept_offer($task_id, $user_id);

                    if($result['status'] === 'success') {
                        Milestone::update($this->db, [
                            'performed' => '1'
                        ], [
                            'task_id' => $task_id,
                            'by_id' => $owner_id,
                            'for_id' => $user_id,
                            'required_action' => 'appointed',
                        ]);
                        if( $this->is_ajax() ) {
                            echo json($result);
                        }else{
                            flash('message', $result['message']);
                            flash('status', $result['status']);
                            redirect_to($redirect_to . "#" . strtolower($task_id));
                        }
                    }else{
                        if( $this->is_ajax() ) {
                            echo json($result);
                        }else{
                            flash('message', $result['message']);
                            flash('status', $result['status']);
                            redirect_to($redirect_to . "#" . strtolower($task_id));
                        }
                    }
                }elseif ( !is_null(request('decline')) ) {
                    $result = $task->reject_offer($task_id, $user_id);
                    if($result['status'] === 'success') {
                        Milestone::update($this->db, [
                            'performed' => '1'
                        ], [
                            'task_id' => $task_id,
                            'by_id' => $owner_id,
                            'for_id' => $user_id,
                            'required_action' => 'appointed',
                        ]);
                        if( $this->is_ajax() ) {
                            echo json($result);
                        }else{
                            flash('message', $result['message']);
                            flash('status', $result['status']);
                            redirect_to($redirect_to . "#" . strtolower($task_id));
                        }
                    }else{
                        if( $this->is_ajax() ) {
                            echo json($result);
                        }else{
                            flash('message', $result['message']);
                            flash('status', $result['status']);
                            redirect_to($redirect_to . "#" . strtolower($task_id));
                        }
                    }
                }
            }

        }else{
            flash('message', lang('Something wrong happened, please try again'));
            flash('status', 'danger');
        }
        redirect_to($redirect_to . "#" . strtolower($task_id));
    }

    public function retract_offer(){

        $redirect_to = $_SERVER['HTTP_REFERER'];
        if(lemon_csrf_require_valid_token() && request('task_id') && request('user_id')) {
            $user_id  = sanitize(request('user_id'), 'int');
            $task_id  = sanitize(request('task_id'));
            $task     = new Task($this->db, $this->user);
            $the_task = $task->get_task_by_id($task_id);
            $owner_id = $the_task['user_id'];

            if ($the_task && $task->has_applicant($user_id, $task_id) ) {

                $result = $task->retract_offer($task_id, $user_id);

                if($result['status'] === 'success') {

                    if( $this->is_ajax() ) {
                        echo json($result);
                    }else{
                        flash('message', $result['message']);
                        flash('status', $result['status']);
                        redirect_to($redirect_to . "#" . strtolower($task_id));
                    }
                }else{
                    if( $this->is_ajax() ) {
                        echo json($result);
                    }else{
                        flash('message', $result['message']);
                        flash('status', $result['status']);
                        redirect_to($redirect_to . "#" . strtolower($task_id));
                    }
                }

            }

        }else{
            flash('message', lang('Failed to retract offer, please try again'));
            flash('status', 'danger');
        }
        redirect_to($redirect_to . "#" . strtolower($task_id));
    }

    public function retract_application(){

        $redirect_to = $_SERVER['HTTP_REFERER'];
        if(lemon_csrf_require_valid_token() && request('task_id') && request('user_id')) {
            $user_id  = sanitize(request('user_id'), 'int');
            $task_id  = sanitize(request('task_id'));
            $task     = new Task($this->db, $this->user);
            $the_task = $task->get_task_by_id($task_id);
            $owner_id = $the_task['user_id'];

            if ($the_task && $task->has_applicant($user_id, $task_id) ) {

                $result = $task->retract_application($task_id, $user_id);

                if($result['status'] === 'success') {

                    if( $this->is_ajax() ) {
                        echo json($result);
                    }else{
                        flash('message', $result['message']);
                        flash('status', $result['status']);
                        redirect_to($redirect_to);
                    }
                }else{
                    if( $this->is_ajax() ) {
                        echo json($result);
                    }else{
                        flash('message', $result['message']);
                        flash('status', $result['status']);
                        redirect_to($redirect_to);
                    }
                }

            }

        }else{
            flash('message', lang('Failed to retract application, please try again'));
            flash('status', 'danger');
        }
        redirect_to($redirect_to);
    }

    public function user_completeness(){
        return isset($this->user->info['completion']) ? $this->user->info['completion'] : 0;
    }

    //Billing Details page
    public function billing(){
        $redirect_to = option('site_uri') . url_for('/workspace', $this->date->year, $this->date->month);
        if(params('task')) {
            $task         = new Task($this->db, $this->user);
            $taskActivity = new TaskActivity($this->db, $this->user);
            $user_id      = $this->user->info['id'];
            $the_task     = $task->get_task(sanitize(params('task')));
            $task_id      = $the_task['task_id'];
            $user_type    = null;

            if($task->is_published($task_id, $the_task['user_id']) || $task->is_draft($task_id, $the_task['user_id'])){
               redirect_to($redirect_to);
            }


            if( $the_task && (int)$the_task['user_id'] === (int)$user_id ) {
                $user_type    = 'poster';
                $task_owner   = $user_id;
                $user_details = $this->get_talent($the_task['assigned_to'], false);
                $taskActivity->set_marked_completed_as_seen($task_id);
                $the_task['rejected_twice']    = $taskActivity->has_been_rejected_twice($task_id);
                $page         = '/workspace/partial/billing/billing-details-poster.html.php';
            }elseif( $the_task && (int)$the_task['assigned_to'] === (int)$user_id ){
                $user_type    = 'seeker';
                $task_owner   = $the_task['user_id'];
                $user_details = $task->get_task_user($the_task['user_id']);
                $user_details['country'] = $this->getCountryName($user_details['country']);
                $user_details['state'] = $this->getStateName($user_details['state']);
                $user_details['rating'] = $this->get_ratings($user_details['id']);
                $taskActivity->set_accepted_as_seen($task_id);
                $taskActivity->set_rejected_as_seen($task_id);
                $taskActivity->set_disputed_as_seen($task_id);
                $taskActivity->set_completed_as_seen($task_id);
                if($user_details['type'] === '1') {
                    $user_details['name'] = $user_details['company']['name'];
                }else{
                    $user_details['name'] = "{$user_details['firstname']} {$user_details['lastname']}";
                }
                $page         = '/workspace/partial/billing/billing-details-seeker.html.php';
            }

            $the_task['in-progress']      = $task->is_inProgress($task_id, $task_owner);
            $the_task['completed']        = $task->is_completed($task_id, $task_owner);
            $the_task['marked-completed'] = $task->is_status($task_id, $task_owner, 'marked-completed');
            $the_task['has_payment']      = (new PaymentRequest($this->db, $this->user))->has($task_id);

            if( $user_type && $task_id /*&& ( $the_task['in-progress'] || $the_task['completed'] )*/){
                if( ($user_type === 'poster' && $task->owner($user_id, $task_id)) || $user_type === 'seeker' ){

                    @list($category, $sub_category) = $this->getCategories([$the_task['category'], $the_task['sub_category']], 'task_category');
                    $the_task['category_name']     = $category['name'];
                    $the_task['subcategory_name']  = $sub_category['name'];
                    $the_task['user']              = $user_details;
                    $the_task['activities']        = $taskActivity->formatted_activities($the_task);

                    unset($the_task['password']);

                    set('task', $the_task);
                    set('page_title', 'Billing Details');

                    return render($page, 'layout/app.html.php');
                }else{
                    flash('message', lang('task_is_unavailable'));
                    flash('status', 'danger');
                    redirect_to($redirect_to);
                }
            }else{
                flash('message', lang('task_is_unavailable'));
                flash('status', 'danger');
                redirect_to($redirect_to);
            }

        }else{
            flash('message', lang('task_is_unavailable'));
            flash('status', 'danger');
            redirect_to($redirect_to);
        }
    }

    //Billings (Earning / Spent) pages
    public function billings(){
        set_latest_url( request_uri() );
        $redirect_to = option('site_uri') . url_for('/workspace/task-centre');
        if( is_null(params('page')) ) redirect_to($redirect_to);

        $query_string =  isset($env['SERVER']['QUERY_STRING']) ? $env['SERVER']['QUERY_STRING'] : @getenv('QUERY_STRING');
        $parsed_url = parse_url($query_string);
        $parsed = [];

        if( !empty($parsed_url) && isset($parsed_url['query']) ) {
            parse_str($parsed_url['query'], $parsed);
        }

        if( isset($parsed_url) && isset($parsed_url['path']) && !empty($parsed) && isset($parsed['review']) && !isset($_SESSION['review_id']) ){
            $_SESSION['review_id'] = strtoupper($parsed['review']);
            $url = option('site_uri') . url_for(ltrim(ltrim($parsed_url['path'], 'uri='), option('base_uri')));
            redirect_to($url);
        }elseif (request('review') && !isset($_SESSION['review_id']) ){
            $_SESSION['review_id'] = strtoupper(request('review'));
            $url = option('site_uri') . url_for("/workspace/billings/".params('page'));
            redirect_to($url);
        }

        $current_tab = strtolower(params('page')) === 'earning' ? '/workspace/partial/billing/billing-earning.html.php'
                                             : '/workspace/partial/billing/billing-spent.html.php';

        $projects = (new Task($this->db, $this->user))->get_my_posted_and_performed_tasks();
        $my_reviews = (new RatingCentre($this->db, $this->user))->my_written_reviews();
        $reviewed_tasks = array_column(array_merge($my_reviews['as_poster'], $my_reviews['as_talent']), 'task_id');
        $posted = array_filter($projects, function($project) use($reviewed_tasks){ return (int)$project['user_id'] === (int) $this->user->info['id'] && !in_array($project['id'], $reviewed_tasks); });
        $performed = array_filter($projects, function($project) use($reviewed_tasks){ return (int)$project['assigned_to'] === (int) $this->user->info['id'] && !in_array($project['id'], $reviewed_tasks); });
        unset($projects);
        $projects = ['posted' => $posted, 'performed' => $performed, 'ids' => array_column(array_merge($posted, $performed), 'id')];
        $filter = [];
        $tab = 'all';
        //Check if we have any date period specified
        if( params('filter') ){
            @list($filter_type, $tab) = explode('-', params('filter'));
            if( 'month' === strtolower($filter_type) ) {
                $filter = [Carbon::now()->startOfMonth()->startOfDay()->toDateString(), Carbon::now()->endOfMonth()->endOfDay()->toDateString()];
            }elseif( 'year' === strtolower($filter_type) ) {
                $filter = [Carbon::now()->startOfYear()->startOfDay()->toDateString(), Carbon::now()->endOfYear()->endOfDay()->toDateString()];
            }elseif( 'quarter' === strtolower($filter_type) ) {
                $filter = [Carbon::now()->startOfQuarter()->startOfDay()->toDateString(), Carbon::now()->endOfQuarter()->endOfDay()->todateString()];
            }elseif( 'custom' === strtolower($filter_type) ){
                @list($from, $to, $tab) = explode('-', base64_decode($tab));
                if( isset($from, $to) && !is_null($from) && !is_null($to) ){
                    if( strtotime($from) && strtotime($to) ) {
                        $filter = [Carbon::parse($from)->startOfDay()->toDateString(), Carbon::parse($to)->endOfDay()->toDateString()];
                    }
                    set('from', Carbon::parse($from)->format("d-M-Y"));
                    set('to', Carbon::parse($to)->format("d-M-Y"));
                }
            }
        }

        if( !is_null( params('page') ) && strtolower(params('page')) === 'spent' ){
            $task        = new Task($this->db, $this->user);
            $tasks       = $task->get_all_tasks(['in-progress', 'marked-completed', 'completed', 'disputed', 'closed'], $filter, 'owned');
            $in_progress = array_filter($tasks, function ($task) { return $task['status'] === 'in-progress' || $task['status'] === 'marked-completed'; });
            $completed   = array_filter($tasks, function ($task) { return $task['status'] === 'completed'; });
            $closed      = array_filter($tasks, function ($task) { return $task['status'] === 'closed'; });
            $disputed    = array_filter($tasks, function ($task) { return $task['status'] === 'disputed'; });
            $all         = [
                'all'         => $tasks,
                'in-progress' => $in_progress,
                'completed'   => $completed,
                'closed'      => $closed,
                'disputed'    => $disputed,
                'active'      => array_merge($in_progress, $completed),
                'inactive'    => array_merge($disputed, $closed),
            ];

            set('tab', is_null($tab) ? 'all' : $tab);
            set('payment_request', new PaymentRequest($this->db, $this->user));
            set('page_title', 'My Spent');
            set('tasks', $all);
        }else{
            $task        = new Task($this->db, $this->user);
            $tasks       = $task->get_all_tasks(['in-progress', 'marked-completed', 'completed', 'disputed', 'closed'], $filter, 'assigned');
            $in_progress = array_filter($tasks, function ($task) { return $task['status'] === 'in-progress' || $task['status'] === 'marked-completed'; });
            $completed   = array_filter($tasks, function ($task) { return $task['status'] === 'completed'; });
            $closed      = array_filter($tasks, function ($task) { return $task['status'] === 'closed'; });
            $disputed    = array_filter($tasks, function ($task) { return $task['status'] === 'disputed'; });
            $all         = [
                'all'         => $tasks,
                'in-progress' => $in_progress,
                'completed'   => $completed,
                'closed'      => $closed,
                'disputed'    => $disputed,
                'active'      => array_merge($in_progress, $completed),
                'inactive'    => array_merge($disputed, $closed),
            ];

            set('tab', is_null($tab) ? 'all' : $tab);
            set('payment_request', new PaymentRequest($this->db, $this->user));
            set('page_title', 'My Earning');
            set('tasks', $all);
        }

        if( request('export') ){
            $tab = ($tab === 'inprogress') ? 'in-progress' : $tab;
            $this->export($all[$tab], request('export'), sanitize(params('page')) . '-' . $tab);
            exit;
        }

        set('current_page', params('page'));
        set('current_tab', $current_tab);
        set('projects', $projects);
        return render('/workspace/financial.html.php', 'layout/app.html.php');
    }

    public function talent_details(){
        $talent_id = params('id');
        $talent_id = (int)str_replace(['TL', 'tl'], '', $talent_id) - 20100;

        if($talent_id && $talent_id > 0) {
            $talent = $this->get_talent($talent_id, false);
            $skills = explode(',', $talent['skills']);
            $languages = explode(',', $talent['languages']);
            $talent['skills'] = array_filter($skills);
            $talent['languages'] = array_filter($languages);

            set('talent', $talent);
            set('page_title', 'Talent Details');
            return render('talent/talent-details.html.php', 'layout/app.html.php');
        }else{
            flash('message', lang('talent_is_unavailable'));
            flash('status', 'danger');
            redirect_to('/workspace', $this->date->year, $this->date->month);
        }
    }

    private function export(...$args){
        $data   = isset($args[0]) ? $args[0] : [];
        $format = isset($args[1]) ? $args[1] : 'excel';
        $name   = isset($args[2]) ? $args[2] . '_' . Carbon::now()->format('Y-m-d-h-i-s') : Carbon::now()->format('Y-m-d-h-i-s');

        if( !empty($data) ){
            $data = array_values($data);
            if( $format === 'excel' ) {
                $filename = $name . '.xls';
                header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
                header("Content-Disposition: attachment; filename={$filename}");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private",false);
                header("Cache-Control: max-age=0");

                $separator = "\t";
                $headers = array_keys($data[0]);

                $headers = array_filter($headers, function($header){
                    return in_array($header, ['number', 'title', 'description', 'complete_by', 'budget', 'status']);
                });

                $headers = array_map(function ($header) {
                    return ucwords(str_replace('_', ' ', $header));
                }, $headers);

                echo implode($separator, $headers) . "\n";

                foreach ($data as $task) {
                    $row = [];
                    foreach($task as $k => $v){
                        if( !in_array($k, ['number', 'title', 'description', 'complete_by', 'budget', 'status']) ) continue;
                        $row[$k] = str_replace($separator . "$", "", $v);
                        $row[$k] = str_replace("-", " ", $row[$k]);
                        $row[$k] = preg_replace("/\r\n|\n\r|\n|\r/", " ", $row[$k]);
                        if( $k === 'description' ) $row[$k] = strip_tags(str_replace('<', ' <', $row[$k]));
                        $row[$k] = trim(in_array($row[$k], ['marked-completed']) ? 'in-progress' : $row[$k]);
                    }
                    echo implode($separator, $row) . "\n";
                }
            }else{
                //echo partial('workspace/partial/billing/pdf-export.html.php', ['data' => $data, 'title' => $args[2]]);
                $pdf = new Html2Pdf('P', 'A4', 'en');
                $pdf->setDefaultFont('arial');
                $pdf->writeHTML( partial('workspace/partial/billing/pdf-export.html.php', ['data' => $data, 'title' => $args[2]]) );
                $pdf->output( $name.'.pdf' );
            }
        }
    }

    /**
     * @param string $ids (comma separated ids)
     * @return array array of id, name pairs
     */
    public function get_skills($ids = null){
        if( is_null($ids) || empty($ids) ) return [];
        $skills = explode(',', $ids);
        $skills = array_filter($skills, function ($skill){ return (int)$skill; });
        if( empty($skills) ) return [];

        $this->db->query("SELECT `id`, `name` FROM `skills` WHERE `id` IN({$ids})"); // AND `status` = '1'
        return $this->db->getRowList();
    }

    /**
     * @param string $ids (comma separated ids)
     * @return array array of all skills (even not approved ones) id, name pairs
     */
    public function get_all_related_skills($ids = null){
        if( is_null($ids) || empty($ids) ) return [];
        $skills = explode(',', $ids);
        $skills = array_filter($skills, function ($skill){ return (int)$skill; });
        if( empty($skills) ) return [];

        $this->db->query("SELECT `id`, `name` FROM `skills` WHERE `id` IN({$ids})");
        return $this->db->getRowList();
    }

    public function milestone_progress(){
        if( ! request('task') ) return json(['status' => 'failed']);

        $taskObject = Task::init($this->db, $this->user);
        $task       = $taskObject->get_task_by_id(request('task'));
        $current_user_id = $this->user->info['id'];
        if(! $task) return json(['status' => 'failed']);

        $task       = $taskObject->set_service_charge($task);
        $task['start_date'] = $this->date::parse($task['start_by'])->formatLocalized("%d %B %Y");
        $task['end_date'] = $this->date::parse($task['complete_by'])->formatLocalized("%d %B %Y");
        $dispute = [];

        if( request('seeker') ){
            $seeker = $this->get_talent(request('seeker'), false);
        }else{
            $seeker = $this->get_talent($current_user_id, false);
        }

        $poster = $taskObject->get_task_user($task['user_id']);
        $poster['name'] = isset($poster['company']) ?
                                                    $poster['company']['name'] :
                                                    "{$poster['firstname']} {$poster['lastname']}";

        if( $task['user_id'] === $current_user_id ){
            Task::init($this->db, $this->user)->set_profile_viewed($seeker['m_id'], $task['task_id']);
        }

        if( request('task_status') ){
            $task_status_request = sanitizeItem(request('task_status'));
            $method = "get_{$task_status_request}_milestone_action";

            if( $current_user_id === $task['user_id'] )
                $task['status'] = $taskObject->get_poster_task_status($task['task_id'], $seeker['m_id'], $task['user_id']);
            else
                $task['status'] = $taskObject->get_seeker_task_status($task['task_id'], $seeker['m_id']);

            if(!$task['status']){
                $task['status'] = $task_status_request;
            }

            $by = $taskObject->$method($task['task_id'], $seeker['m_id']);

            if( empty($by) )
                $by = $taskObject->$method($task['task_id'], $task['user_id']);

            if( empty($by) )
                $by['by_id'] = $task['user_id'];

            $task['other_person'] = ! ($by['by_id'] == $this->user->info['id']);
        }

        if( $progress_status = request('progress_status') ){
            //'seeker-progress-3b'
            if( !in_array($progress_status, ['seeker-progress-7', 'seeker-progress-5a', 'seeker-progress-5b']) && $current_user_id === $seeker['m_id'] ){
                Milestone::update($this->db, ['performed' => '1'],[
                   'task_id' => $task['task_id'],
                   'by_id'  => $task['user_id'],
                   'for_id' => $seeker['m_id']
                ]);
                $seeker['hide-status'] = true;
            }elseif( $progress_status === 'seeker-progress-7' && $current_user_id === $seeker['m_id'] ){
                $dispute = $taskObject->get_latest_release_payment($task['task_id'], $task['user_id'], $seeker['m_id']);
            }
        }

        $payment = $taskObject->get_latest_payment($task['task_id'], $task['user_id'], $seeker['m_id']);
        if( $payment ){
            $task['payment'] = $payment;
            $task['payment_status'] = ! (bool)$payment['status'] ? false : true;
        }

        return $task['user_id'] === $current_user_id ?
            partial('workspace/partial/task-centre/milestone-poster-modal.html.php', compact('task', 'seeker')) :
            partial('workspace/partial/task-centre/milestone-seeker-modal.html.php', compact('task', 'poster', 'seeker', 'dispute'));
    }

    public function pin_applicant(){
        $request = [request('seeker'), request('task'), request('hired')];

        if( ! isset($request) ) return json(['status' => 'failed']);

        $taskObj = Task::init($this->db, $this->user);
        $task = $taskObj->get_task_by_id(sanitizeItem(request('task')));

        if( !$task || ! $taskObj->owner($this->user->info['id'], $task['task_id']) ) return json(['status' => 'failed']);

        $seeker_id = mysqli_real_escape_string($this->db->connection, request('seeker'));

        if( $task['user_id'] === $this->user->info['id'] ){
            Task::init($this->db, $this->user)->set_profile_viewed($seeker_id, $task['task_id']);
        }

        $isPinned = $this->db->getValue("SELECT `pinned` FROM `user_task` WHERE `user_id` = '{$seeker_id}' AND `task_id` = '{$task['task_id']}' AND `type` = 'task' LIMIT 1");
        $status = (bool)$isPinned['pinned'] ? '0' : '1';

        if( (bool) request('hired') ){
            $this->db->table('poster_seeker');
            $this->db->updateArray(['pinned' => $status]);
            $this->db->whereArray([
                'poster_id' => $this->user->info['id'],
                'seeker_id' => sanitizeItem(request('seeker')),
                'task_id' => $task['task_id']
            ]);
            $this->db->update();

            $this->db->table('user_task');
            $this->db->updateArray(['pinned' => $status]);
            $this->db->whereArray([
                'user_id' => sanitizeItem(request('seeker')),
                'task_id' => $task['task_id'],
                'type' => 'task',
            ]);
            $this->db->update();
        }else{
            $this->db->table('user_task');
            $this->db->updateArray(['pinned' => $status]);
            $this->db->whereArray([
                'user_id' => $seeker_id,
                'task_id' => $task['task_id'],
                'type' => 'task',
            ]);
            $this->db->update();
        }

        return json(['status' => 'success']);
    }

    public function overall_notifications(){
        $this->is_logged_in();
        $user_id = $this->user->info['id'];

        $query = "SELECT t.`id`,
                    (SELECT COUNT(`id`) FROM `chats` WHERE `receiver_id` = t.`user_id` AND `viewed` = '0' AND `task_id` = t.`id` ) AS `chats`,
                    (SELECT COUNT(`id`) FROM `comments` WHERE `post_id` = t.`id` AND `user_id` != t.`user_id` AND `viewed` = 'N' AND `hidden` = '0') AS `comments`,
                    (SELECT COUNT(`id`) FROM `milestone_action` WHERE `task_id` = t.`id` AND `for_id` = t.`user_id` AND `performed` = '0' ) AS `actions`,
                    (SELECT COUNT(`id`) FROM `user_task` WHERE `task_id` = t.`id` AND `type` = 'task' AND `viewed` = 'N') AS `applicants`
                    FROM `tasks` t WHERE t.`status` IN ('published','in-progress') AND t.`user_id` = '{$user_id}'
                    
                    UNION 
                    
                    SELECT ut.`task_id`, 
                    (SELECT COUNT(`id`) FROM `chats` WHERE `receiver_id` = ut.`user_id` AND `viewed` = '0' AND `task_id` = ut.`task_id` ) AS `chats`,
                    (SELECT COUNT(`id`) FROM `comments` WHERE `post_id` = ut.`task_id` AND `user_id` != ut.`user_id` AND `viewed` = 'N' AND `hidden` = '0') AS `comments`,
                    (SELECT COUNT(`id`) FROM `milestone_action` WHERE `task_id` = ut.`task_id` AND `for_id` = ut.`user_id` AND `performed` = '0' ) AS `actions`,
                    0 AS `applicants`
                    
                    FROM `user_task` ut 
                    INNER JOIN `tasks` t ON t.`id` = ut.`task_id` AND t.`status` IN ('published','in-progress')
                    WHERE ut.`user_id` = '{$user_id}' AND ut.`type` = 'task' AND ut.`status` NOT IN ('closed', 'cancelled', 'disputed')";
        $this->db->query($query);
        $notifications = $this->db->getRowList();
        $notifications = array_map(function ($notification){ unset($notification['id']); return array_filter($notification); }, $notifications);

        set('tasks_notification', (bool) count(array_filter($notifications)));
    }

    /**
     * @return void
     */
    public function was_viewing( $task_slug, $type ){
        if ($this->user->info['type'] === '0' && $task_slug) {
            $slug        = mysqli_real_escape_string($this->db->connection, $task_slug);
            if( $type === 'task' ) {
                $task = Task::init($this->db, $this->user)->get_task($slug);

                if (!$task) {
                    $task = Task::init($this->db, $this->user)->get_external_task($slug);
                }

                $task['external'] = isset($task['source']) ? '?s=external' : '';
                $task['p_type'] = 'task';
            }else{
                $task = Job::init($this->db, $this->user)->get_job($slug);

                if (!$task)
                    $task = Job::init($this->db, $this->user)->get_external_job($slug);

                $task['external'] = isset($task['source']) ? '?s=external' : '';
                $task['p_type'] = 'job';
            }

            $was_viewing = $task;
            set('was_viewing', $was_viewing);
        }
    }

    /**
     * @return string
     */
    public function getTasksList($page = 1){
        $offset = ($page - 1) * 10;
        $section = params('section') && in_array(sanitizeItem(params('section')), ['applied', 'posted']) ? sanitizeItem(params('section')) : 'applied';
        $view    = params('view') && in_array(sanitizeItem(params('view')), ['list', 'calendar']) ? sanitizeItem(params('view')) : 'list';
        if ($this->user->info['type'] === '1') $section = 'posted';
        $_SESSION['current_task_centre_view'] = $view;
        $request                              = route_find('get', request_uri());

        if (!empty($request) && $request['path'] === "/workspace/:year/:month/:day") {
            redirect_to(option('site_uri') . url_for("/workspace/task-centre/{$section}/{$view}"));
        }

        //dd($section, $view);
        $uri_segments = explode('/', trim(request_uri(), '/'));
        $type         = 'month';

        if (request('show') && in_array(request('show'), ['month', 'week', 'day', 'hour'])) {
            $type = request('show');
        }

        $this->set_date();

        $date       = $this->prepareDate($type);
        $user_id    = $this->user->info['id']; //'6FCCDF26-BF9A-475C-8C1F-D022E5322CF7';
        $taskObject = Task::init($this->db, $this->user);

        $tasks               = $taskObject->tasks($user_id, $offset);
        $picked_applicants   = $taskObject->get_tasks_picked_applicants($user_id);
        $selected_applicants = [];

        foreach ($picked_applicants as &$applicant) {
            $applicant['poster_status'] = $taskObject->get_poster_task_status($applicant['task_id'], $applicant['seeker_id'], $applicant['poster_id']);
            $applicant['seeker_status'] = $taskObject->get_seeker_task_status($applicant['task_id'], $applicant['seeker_id']);

            $selected_applicants[$applicant['task_id']][$applicant['seeker_id']] = $applicant;
        }

        $tags        = $tasks['tags'];
        $questions   = $tasks['questions'];
        $attachments = $tasks['attachments'];

        $jobs          = (new Job($this->db, $this->user))->jobs($user_id);
        $job_questions = $jobs['questions'];

        foreach ($tasks['applicants'] as $key => $applicant) {
            $tasks['applicants'][$key]['rating']          = $this->get_ratings($applicant['user_id']);
            $tasks['applicants'][$key]['reviews']         = $this->get_reviews($applicant['user_id']);
            $tasks['applicants'][$key]['has_new_updates'] = $this->get_applicants_new_updates($applicant['user_id'], $applicant['task_id'], $applicant);
        }

        unset($tasks['tags'], $tasks['questions'], $tasks['attachments'], $jobs['questions']);

        $sub_url = ($type === 'index') ? 'month' : $type;

        $posted_date_set        = $applied_date_set = false;
        $applied_start_dates    = array_column(array_filter($tasks['all_applied']['active'], function ($task) { return in_array($task['apply_status'], ['in-progress', 'marked-completed', 'completed']); }), 'start_by');
        $applied_complete_dates = array_column(array_filter($tasks['all_applied']['active'], function ($task) { return in_array($task['apply_status'], ['in-progress', 'marked-completed', 'completed']); }), 'complete_by');
        $posted_start_dates     = array_column(array_filter($tasks['all_posted']['active'], function ($task) { return in_array($task['status'], ['in-progress', 'marked-completed', 'completed']); }), 'start_by');
        $posted_complete_dates  = array_column(array_filter($tasks['all_posted']['active'], function ($task) { return in_array($task['status'], ['in-progress', 'marked-completed', 'completed']); }), 'complete_by');
        $applied_start_date     = array_merge($applied_start_dates, $applied_complete_dates);
        $posted_start_date      = array_merge($posted_start_dates, $posted_complete_dates);
        sort($applied_start_date);
        sort($posted_start_date);

        if ($type === 'hour') {

            $task_planner = $taskObject->get_planning($this->date);

            $time_plans = [];
            foreach ($task_planner as $plan) {
                $time_plans[$plan['task_id']][] = [
                    'date'  => Carbon::parse($plan['date']),
                    'start' => Carbon::parse($plan['date'] . ' ' . $plan['start']),
                    'end'   => Carbon::parse($plan['date'] . ' ' . $plan['end']),
                    'hours' => $plan['total_hours'],
                ];
            }

            if (!empty($time_plans)) {
                set('time_plans', $time_plans);
            }

            if (!empty($applied_start_date)) {
                foreach ($applied_start_date as $applied_date) {
                    if ($this->date->copy()->parse($applied_date)->between($this->date->copy(), $this->date->copy())) {
                        $applied_date_set = true;
                        set('applied_start_day', $this->date->copy()->subDays(2)->diffInHours($this->date->copy()->parse($applied_date)) - 2);
                        break;
                    }
                }
            }

            if (!$applied_date_set) {
                set('applied_start_day', $this->date->copy()->subDays(2)->diffInHours($this->date->copy()->setTime(7, 0)));
            }

            if (!empty($posted_start_date)) {
                foreach ($posted_start_date as $posted_date) {
                    if ($this->date->copy()->parse($posted_date)->between($this->date->copy(), $this->date->copy())) {
                        $posted_date_set = true;
                        set('posted_start_day', $this->date->copy()->subDays(2)->diffInHours($this->date->copy()->parse($posted_date)) - 2);
                        break;
                    }
                }
            }

            if (!$posted_date_set) {
                set('posted_start_day', $this->date->copy()->subDays(2)->diffInHours($this->date->copy()->setTime(7, 0)));
            }
        } elseif ($type === 'day') {

            /*if( !empty($applied_start_date) ){
                foreach($applied_start_date as $applied_date) {
                    if ($this->date->copy()->parse($applied_date)->between($this->date->copy()->subWeeks(2), $this->date->copy()->addWeeks(2))) {
                        $applied_date_set = true;
                        set('applied_start_day', $this->date->copy()->subWeeks(2)->diffInDays($this->date->copy()->parse($applied_date)) - 1);
                        break;
                    }
                }
            }*/

            if (!$applied_date_set) {
                set('applied_start_day', $this->date->copy()->subWeeks(2)->diffInDays($this->date->copy()) - 1);
            }

            /*if( !empty($posted_start_date) ){
                foreach($posted_start_date as $posted_date) {
                    if ($this->date->copy()->parse($posted_date)->between($this->date->copy()->subWeeks(2), $this->date->copy()->addWeeks(2))) {
                        $posted_date_set = true;
                        set('posted_start_day', $this->date->copy()->subWeeks(2)->diffInDays($this->date->copy()->parse($posted_date)) - 1);
                        break;
                    }
                }
            }*/

            if (!$posted_date_set) {
                set('posted_start_day', $this->date->copy()->subWeeks(2)->diffInDays($this->date->copy()) - 1);
            }
        } elseif ($type === 'week') {

            if (!$applied_date_set) {
                set('applied_start_day', $this->date->copy()->subMonth()->diffInWeeks($this->date->copy()));
            }

            if (!$posted_date_set) {
                set('posted_start_day', $this->date->copy()->subMonth()->diffInWeeks($this->date->copy()));
            }
        } elseif ($type === 'month') {

            if (!$applied_date_set) {
                set('applied_start_day', $this->date->copy()->startOfYear()->diffInMonths($this->date->copy()));
            }

            if (!$posted_date_set) {
                set('posted_start_day', $this->date->copy()->startOfYear()->diffInMonths($this->date->copy()));
            }
        }

        /*if($sub_url === 'week' || $sub_url === 'month') {
            $applied_week_start_dates = array_column(array_filter($tasks['applied'], function ($task) { return (int)$this->user->info['id'] === (int)$task['assigned_to'] && in_array($task['status'], ['in-progress', 'marked-completed']); }), 'start_by');
            $applied_week_complete_dates = array_column(array_filter($tasks['applied'], function ($task) { return (int)$this->user->info['id'] === (int)$task['assigned_to'] && in_array($task['status'], ['in-progress', 'marked-completed']); }), 'complete_by');
            $assigned_week_start_dates = array_column(array_filter($tasks['assigned'], function($task){ return in_array($task['status'], ['in-progress', 'marked-completed']); }), 'start_by');
            $assigned_week_complete_dates = array_column(array_filter($tasks['assigned'], function($task){ return in_array($task['status'], ['in-progress', 'marked-completed']); }), 'complete_by');
            $week_start_dates = array_merge($applied_week_start_dates, $assigned_week_start_dates, $applied_week_complete_dates, $assigned_week_complete_dates);
            sort($week_start_dates);
            if(!empty($week_start_dates)) {
                if($sub_url === 'week') {
                    $current_month = array_filter($week_start_dates, function ($current) { return Carbon::parse($current)->month === $this->date->month && Carbon::parse($current)->year === $this->date->year; });
                }else{
                    $current_month = array_filter($week_start_dates, function ($current) { return Carbon::parse($current)->isSameYear($this->date) && Carbon::parse($current)->isSameQuarter($this->date); });
                }
                if($current_month) {
                    $week_start_date = array_shift($current_month);
                    $week_start_date = Carbon::parse($week_start_date);
                    set('week_start_date', $week_start_date);
                }
            }
        }*/

        /*if($sub_url === 'day') {
            $task_planner = $taskObject->get_planning($this->date);
            $time_plans = [];
            foreach ($task_planner as $plan){
                $time_plans[$plan['task_id']][] = [
                    'date'  => Carbon::parse($plan['date']),
                    'start' => Carbon::parse($plan['date'] . ' ' . $plan['start']),
                    'end'   => Carbon::parse($plan['date'] . ' ' . $plan['end']),
                    'hours' => $plan['total_hours'],
                ];
            }

            if(!empty($time_plans)){
                set('time_plans', $time_plans);
            }
        }*/

        $preferred_times = $times = [];
        $all_plans       = $taskObject->get_all_planning();
        foreach ($all_plans as $plan) {
            $preferred_times[$plan['task_id']][] = [
                'date'  => Carbon::parse($plan['date']),
                'start' => Carbon::parse($plan['date'] . ' ' . $plan['start']),
                'end'   => Carbon::parse($plan['date'] . ' ' . $plan['end']),
                'hours' => $plan['total_hours'],
            ];

            array_push($times, Carbon::parse($plan['date']));
        }

        $day_sub_url  = '/day';
        $day_url_hash = '#tcInProgressDay';
        $next_date    = array_filter($times, function ($time) { return $time->greaterThan($this->date); });
        $prev_date    = array_filter($times, function ($time) { return $time->lessThan($this->date); });
        $next_date    = array_shift($next_date);
        $prev_date    = end($prev_date);
        $next_day     = ($next_date) ? url_for("workspace{$day_sub_url}", $next_date->year, $next_date->month, $next_date->copy()->day . $day_url_hash) : '#';
        $prev_day     = ($prev_date) ? url_for("workspace{$day_sub_url}", $prev_date->year, $prev_date->month, $prev_date->copy()->day . $day_url_hash) : '#';
        $day_times    = [
            'next_day' => $next_day,
            'prev_day' => $prev_day
        ];

        set('data', $date);
        set('tasks', $tasks);
        set('milestone_actions', $tasks['actions']);
        set('payments', $tasks['payments']);
        set('jobs', $jobs);
        set('tags', $tags);
        set('questions', $questions);
        set('job_questions', $job_questions);
        set('attachments', $attachments);
        set('selected_applicants', $selected_applicants);
        set('current_user', $this->user->info);
        set('preferred_times', $preferred_times);
        set('day_times', $day_times);
        set("{$sub_url}", 'active');
        set('page_title', 'Task Centre');
        set('section', $section);
        set('view', $view);
        set('calendar_view', $type);
        return [$type, $tasks['all_posted']['active_total'], $tasks['all_posted']['inactive_total'], $tasks['all_applied']['active_total'], $tasks['all_applied']['inactive_total']];
    }
}