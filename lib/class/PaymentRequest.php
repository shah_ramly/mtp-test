<?php


class PaymentRequest{

    protected $db;
    protected $user;
    public function __construct($db, $user){
        $this->db = $db;

        if( isset($_SESSION['member_id']) && !empty($_SESSION['member_id']) ){
            $_SESSION[WEBSITE_PREFIX.'USER_ID'] = $_SESSION['member_id'];
        }

        $this->user = $user;
        date_default_timezone_set('Asia/Kuala_Lumpur');

        if( !isset($_SESSION[WEBSITE_PREFIX.'LANGUAGE']) || $_SESSION[WEBSITE_PREFIX.'LANGUAGE'] == 'EN' ){
            $locale = ['English', 'en_GB'];
        }else{
            $locale = ['Malay', 'ms_MY'];
        }

        setLocale(LC_TIME, $locale[1]);
        \Carbon\Carbon::setLocale($locale[1]);
    }

    private function checkIfTableExists(){
        $this->db->query("SHOW TABLES LIKE 'payment_requests'");
        $table_exists = $this->db->getSingleRow();
        if(empty($table_exists)){
            halt('There is some technical issue, please contact us'); // "payment_requests" table does not exist
        }
    }

    public function make( $task_id = null ){
        if( is_null($task_id) ) return false;
        $task_id = sanitize($task_id);

        $this->checkIfTableExists();

        $this->db->query(" SELECT `id` FROM `payment_requests` WHERE `task_id` = '{$task_id}' LIMIT 1 ");
        $payment = $this->db->getSingleRow();

        if( !empty($payment) ){
            $this->db->queryOrDie(" UPDATE `payment_requests` SET `times` = (`times` + 1) WHERE `task_id` = '{$task_id}' AND `id` = '{$payment['id']}' ");
        }else{
            $this->db->table('payment_requests');
            $this->db->insertArray([
                'task_id' => $task_id,
                'times'   => 1,
            ]);

            $this->db->insert();
        }

        return true;

    }

    public function reject( $task_id = null ){
        if( is_null($task_id) ) return false;
        $task_id = sanitize($task_id);

        $this->checkIfTableExists();

        $this->db->query(" SELECT `id` FROM `payment_requests` WHERE `task_id` = '{$task_id}' AND `times` > 0 LIMIT 1 ");
        $payment = $this->db->getSingleRow();

        if( !empty($payment) ){
            $this->db->queryOrDie(" UPDATE `payment_requests` SET `times` = `times` - 1 WHERE `task_id` = '{$task_id}' AND `id` = {$payment['id']} ");
        }

        return true;

    }

    public function has($task_id = null){
        if( is_null($task_id) ) return false;
        $task_id = sanitize($task_id);

        $this->checkIfTableExists();

        $this->db->query(" SELECT `id` FROM `payment_requests` WHERE `task_id` = '{$task_id}' AND `times` != 0 LIMIT 1 ");
        $payment = $this->db->getSingleRow();

        return count($payment) > 0 ? true : false;
    }

}