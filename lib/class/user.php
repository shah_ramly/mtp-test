<?php
class user{
	public $db;
    public $id;
    public $guest_id;
    public $admin;
    public $logged;
    public $info;
	
	public function __construct($db){
		$this->db = $db;
		$this->id = !empty($_SESSION[WEBSITE_PREFIX.'USER_ID']) ? $_SESSION[WEBSITE_PREFIX.'USER_ID'] : 0;
		$this->guest_id = $this->id > 0 ? 0 : (!empty($_SESSION[WEBSITE_PREFIX.'GUEST']['id']) ? $_SESSION[WEBSITE_PREFIX.'GUEST']['id'] : 0);
		$this->admin = !empty($_SESSION[BACKEND_PREFIX.'ADMIN_ID']) ? 1 : 0;
		$this->logged = !empty($this->id) ? 1 : 0;
		$this->info = $this->info($this->id);
	}	
	
	public function info($id = 0, $return = false){
		if($id){
		    if( isset($_SESSION['info']) && $id == $this->id ) return $_SESSION['info'];

			$this->db->query("SELECT * FROM members WHERE id = " . $this->db->escape($id));
			$info = $this->db->getSingleRow();

			if($info){
				unset($info['password']);
				if(!$info['country']) $info['country'] = 132;
				//if(!$info['state']) $info['state'] = 1944;
				$info['contact_method'] = explode(',', $info['contact_method']);

				$info['name'] = $info['firstname'] . ($info['lastname'] ? ' ' . $info['lastname'] : '');
				$info['url'] = '/talent/TL' . (20100 + $info['id']);

				$this->db->query("SELECT * FROM preference WHERE member_id = " . $this->db->escape($info['id']));
				$preference = $this->db->getSingleRow();

				if(!$preference){
					$this->db->table('preference');
					$this->db->insertArray(array(
						"member_id" => $info['id'],
					));
					$this->db->insert();
				}

				$info['preference'] = $preference;
				if(empty($info['preference']['rm_country'])) $info['preference']['rm_country'] = $info['country'];
				//if(!$info['preference']['rm_state']) $info['preference']['rm_state'] = $info['state'];
				if(empty($info['preference']['p_country'])) $info['preference']['p_country'] = $info['country'];
				//if(!$info['preference']['p_state']) $info['preference']['p_state'] = $info['state'];
				if(empty($info['preference']['p_work_location'])) $info['preference']['p_work_location'] = 'same_country';

				if($info['type'] == '1'){
					$this->db->query("SELECT * FROM company_details WHERE member_id = " . $this->db->escape($info['id']));
					$company = $this->db->getSingleRow();

					if($company){
						if($company['photos']) $company['photos'] = unserialize($company['photos']);
						$company['phone'] = $company['company_phone'];
						$info['company'] = $company;
					}
				}

				if(empty($preference['language'])) $preference['language'] = '1';
                $language_code = $this->db->getValue("SELECT `code` FROM `language` WHERE `status` = '1' AND `id` = " . $this->db->escape($preference['language']) . " LIMIT 1");
				$_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] = $language_code;
				
				if($info['plan']) $this->db->query("SELECT * FROM subscription WHERE id = " .$this->db->escape($info['plan']));
				else $this->db->query("SELECT * FROM subscription WHERE id = '1'");
				$subscription = $this->db->getSingleRow();
				$info['plan'] = $subscription;

				$info['interests'] = $this->db->getValue("SELECT `interests` FROM `user_interests` WHERE `user_id` = '{$this->id}' LIMIT 1");
				if( !empty($info['interests']) ){
				    $ids = implode("','", explode(',', $info['interests']));
				    $this->db->query("SELECT `id`, `title` AS `name` FROM `interest` WHERE `id` IN('{$ids}')");
				    $info['interests_list'] = $this->db->getRowList();
                }

				if($id == $this->id && !$return){
					//$site_url = (!empty($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . ROOTPATH;
					$this->db->query("SELECT * FROM member_logs WHERE member_id = " .$this->db->escape($id) . " ORDER BY date DESC LIMIT 1");
					$last_login = $this->db->getSingleRow();

					if($info['type'] == '0' && empty($last_login) && empty($info['skills']) && !preg_match('/\/personalize/', $_SERVER['REQUEST_URI'])){
						//Event::trigger('user.first.login', [$info]);
                        redirect_to(option('site_uri') . url_for('personalize'));
						//header('Location: ' . $site_url . 'personalize');
						//exit();
					}

					if($info['type'] == '1' && empty($last_login) && !preg_match('/\/my-company-profile-signup/', $_SERVER['REQUEST_URI'])){
					    redirect_to(option('site_uri') . url_for('my-company-profile-signup'));
						/*header('Location: ' . $site_url . 'my-company-profile-signup');
						exit();*/
					}
				}else{
				    return $info;
                }
				$_SESSION['info'] = $info;
			}
			
		}else{
			$info['id'] = $this->guest_id;
			$info['email'] = !empty($_SESSION[WEBSITE_PREFIX.'GUEST']['email']) ? $_SESSION[WEBSITE_PREFIX.'GUEST']['email'] : '';
			$info['password'] = !empty($_SESSION[WEBSITE_PREFIX.'GUEST']['password']) ? $_SESSION[WEBSITE_PREFIX.'GUEST']['password'] : '';
			$info['salutation'] = '';
			$info['firstname'] = '';
			$info['lastname'] = '';
			$info['contact'] = '';
			$info['address1'] = '';
			$info['address2'] = '';
			$info['city'] = '';
			$info['zip'] = '';
			$info['country'] = '129';
			$info['state'] = '1983';
		}
		
		return $info;
	}

	public function profileCompleteness(){
		$info = $this->info($this->id);
		$score = 0;

		if($this->id){
			if($info['firstname']) $score += 5;
			if($info['email']) $score += 5;
			if($info['country']) $score += 5;
			if(!is_null($info['state']) && $info['state'] != '') $score += 5;
			if(!is_null($info['city']) && $info['city'] != '') $score += 5;
			if($info['nric']) $score += 5;			
			if($info['dob']) $score += 5;			
			if($info['skills']) $score += 10;
			if($info['language']) $score += 10;
			
			$this->db->query("SELECT * FROM preference WHERE  member_id = " . $this->db->escape($this->id) );
			$preference = $this->db->getSingleRow();

			if($preference['bank'] && $preference['acc_name'] && $preference['acc_num']) $score += 10;

			// Remove completeness checking for work location
			/*
			if($preference['rm_work_location'] == 'near' && $preference['rm_radius']) $score += 5;
			if($preference['rm_work_location'] == 'anywhere') $score += 5;
			if($preference['rm_work_location'] == 'same_country' && $preference['p_state']) $score += 5;

			if($preference['p_work_location'] == 'near' && $preference['p_radius']) $score += 5;
			if($preference['p_work_location'] == 'anywhere') $score += 5;
			if($preference['p_work_location'] == 'same_country' && $preference['p_state']) $score += 5;
			*/
			$score += 15;
						
			//if($preference['work_day'] && $preference['working_hours']) $score += 10;
			if($preference['work_day']) $score += 10;
			//if($preference['work_load_hours'] && $preference['expected_earning']) $score += 10;
			if($preference['work_load_hours']) $score += 10;

			if($info['completion'] != $score){
				$this->db->queryOrDie("UPDATE members SET completion = " . $this->db->escape($score) . " WHERE id = " . $this->db->escape($this->id));
			}
			
		}

		return $score;
	}
}
?>