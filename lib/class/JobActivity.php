<?php

class JobActivity
{
    protected $db;
    protected $user;
    public function __construct($db, $user){
        $this->db = $db;

        if( isset($_SESSION['member_id']) && !empty($_SESSION['member_id']) ){
            $_SESSION[WEBSITE_PREFIX.'USER_ID'] = $_SESSION['member_id'];
        }

        $this->user = $user;
        date_default_timezone_set('Asia/Kuala_Lumpur');

        if( !isset($_SESSION[WEBSITE_PREFIX.'LANGUAGE']) || $_SESSION[WEBSITE_PREFIX.'LANGUAGE'] == 'EN' ){
            $locale = ['English', 'en'];
        }else{
            $locale = ['Malay', 'ms'];
        }

        setLocale(LC_TIME, $locale[0]);
        \Carbon\Carbon::setLocale($locale[1]);
    }

    public function add($args = []){
        extract($args, EXTR_OVERWRITE);
        $user_id = isset($user_id) ? $user_id : $this->user->info['id'];
        $created_at = isset($created_at) ? $created_at : \Carbon\Carbon::now();
        $this->db->table('task_activity');
        $this->db->insertArray([
            'user_id'     => $user_id,
            'task_id'     => $task_id,
            'task_number' => $task_number,
            'event'       => $event,
            'message'     => isset($message) ? $message : null,
            'created_at'  => $created_at->toDateTimeString()
        ]);
        $this->db->insert();
    }

    public function get_activities( $task_id = null ){
        if( is_null($task_id) ) return [];

        $query = "SELECT ta.*,
                  (
                    SELECT CONCAT(m.`firstname`, ' ', m.`lastname`) AS `name` 
                    FROM `members` m
                    WHERE m.`id` = ta.`user_id`
                  ) AS `user_name`
                  
                  FROM `task_activity` ta
                  WHERE ta.`task_id` = " . $this->db->escape($task_id);
        $query .= " ORDER BY ta.`created_at` DESC";

        $this->db->query($query);
        return $this->db->getRowList();
    }

    public function formatted_activities( $the_task = null ){
        $activities   = $this->get_activities($the_task['task_id']);
        $current_user = $this->user->info['id'];
        $task         = new Task($this->db, $this->user);
        $talent       = (new TaskCentre($this->db, $this->user))->get_talent($the_task['assigned_to'], false);
        $logs         = [];

        foreach ($activities as $activity):
            $log  = [];
            $user = ($current_user === $activity['user_id']) ? 'You' : $activity['user_name'];
            $date = \Carbon\Carbon::parse($activity['created_at'])->format("d/m/Y");
            switch($activity['event']):
                case 'created':
                    $log['date'] = $date;
                    $log['message'] = "Task #{$activity['task_number']} has been created";
                    break;

                case 'published':
                    $log['date'] = $date;
                    $log['message'] = "The Task id #{$activity['task_number']} has been published";
                    break;

                case 'applied':
                    if( !$task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = "You applied for Task id #{$activity['task_number']}";
                    endif;
                    break;

                case 'assigned':
                    $log['date'] = $date;
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['message'] = "{$talent['name']} has been selected to complete the Task id #{$activity['task_number']}";
                    elseif( $the_task['assigned_to'] === $current_user ):
                        $log['message'] = "{$the_task['user']['name']} assigned you for Task id #{$activity['task_number']}";
                    else:
                        $log['message'] = "You have lost for Task id #{$activity['task_number']}";
                    endif;
                    break;

                case 'payment request':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = "{$talent['name']} has requested payment for Task id #{$activity['task_number']}";
                    elseif( $the_task['assigned_to'] === $current_user ):
                        $log['date'] = $date;
                        $log['message'] = "You have requested payment for Task id #{$activity['task_number']}";
                    endif;
                    break;

                case 'payment made':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = "Payment has been made to Escrow for Task id #{$activity['task_number']}";
                    endif;
                    break;

                case 'marked completed':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = "{$talent['name']} has mark Task id #{$activity['task_number']} as completed";
                    else:
                        $log['date'] = $date;
                        $log['message'] = "You have mark Task id #{$activity['task_number']} as completed";
                    endif;
                    break;

                case 'rejected':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = "You have rejected for Task id #{$activity['task_number']}";
                    else:
                        $log['date'] = $date;
                        $log['message'] = "{$the_task['user']['name']} has rejected Task id #{$activity['task_number']}";
                    endif;
                    break;

                case 'accepted':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = "You have accepted for Task id #{$activity['task_number']}";
                    else:
                        $log['date'] = $date;
                        $log['message'] = "{$the_task['user']['name']} has accepted Task id #{$activity['task_number']}";
                    endif;
                    break;

                case 'completed':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = "You have set the Task id #{$activity['task_number']} as completed";
                    elseif( $the_task['assigned_to'] === $current_user ):
                        $log['date'] = $date;
                        $log['message'] = "{$the_task['user']['name']} has set the Task id #{$activity['task_number']} as completed";
                    endif;
                    break;

                case 'disputed':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = "You have disputed for The Task id #{$activity['task_number']}";
                    elseif( $the_task['assigned_to'] === $current_user ):
                        $log['date'] = $date;
                        $log['message'] = "{$the_task['user']['name']} has disputed for The Task id #{$activity['task_number']}";
                    endif;
                    break;

                case 'closed':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = "The payment has been released to {$talent['name']} for Task id #{$activity['task_number']} and the status now is closed";
                    elseif($the_task['assigned_to'] === $current_user):
                        $log['date'] = $date;
                        $log['message'] = "The payment has been released to you for Task id #{$activity['task_number']} and the status now is closed";
                    endif;
                    break;

                case 'cancelled':
                    $log['date'] = $date;
                    if(is_null($activity['user_name'])) $user = "The System";
                    $log['message'] = "The Task id #{$activity['task_number']} has been cancelled by {$user}";
                    break;
            endswitch;

            if(!empty($log)) array_push($logs, $log);
        endforeach;

        return $logs;
    }

    public function has_marked_completed_activity( $task_id = null ){
        if( is_null($task_id) ) return false;

        $query = "SELECT `id` FROM `task_activity` WHERE `event` = 'marked completed' AND `updated_at` IS NULL 
                  AND `task_id` = " . $this->db->escape($task_id);
        $query .= " ORDER BY `created_at` DESC LIMIT 1";

        $this->db->query($query);
        return !empty( $this->db->getRowList() );
    }

    public function set_marked_completed_as_seen( $task_id = null ){
        if( is_null($task_id) ) return false;

        $now = \Carbon\Carbon::now()->toDateTimeString();
        $query = "UPDATE `task_activity` SET `updated_at` = '{$now}' 
                  WHERE `event` = 'marked completed' AND `task_id` = " . $this->db->escape($task_id);

        $this->db->queryOrDie($query);
    }
}