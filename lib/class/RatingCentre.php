<?php


class RatingCentre extends TaskCentre
{

    public function my_written_reviews(){
        $user_id = $this->user->info['id'];

        $query = "SELECT m.`firstname`, m.`lastname`, m.`photo`, pr.`talent_id` AS `reviewer_id`, d.`name` AS `company_name`,
                    pr.`id` AS `review_id`, 'talent' AS `review_for`, pr.`task_id`, pr.`review`, pr.`created_at`,
                    ( SELECT `name` FROM `states` s WHERE s.`id` = m.`state` limit 1 ) AS `state`,
                    ( SELECT `name` FROM `countries` c WHERE c.`id` = m.`country` limit 1 ) AS `country`,
                    ( SELECT `slug` FROM `tasks` WHERE `id` = pr.`task_id` limit 1 ) AS `slug`,
                    ( SELECT `number` FROM `tasks` WHERE `id` = pr.`task_id` limit 1 ) AS `task_number`,
                    COALESCE(CONVERT((pr.`timeliness`+pr.`expertise`+pr.`satisfactory`+pr.`easy`)/4, DECIMAL(10,1)), 0) AS `total_rating`
                    FROM `poster_reviews` pr
                    INNER JOIN `members` m ON m.`id` = pr.`talent_id`
                    LEFT JOIN `company_details` d ON d.`member_id` = m.`id` AND m.`type` = 1
                    WHERE pr.`reviewer_id` = '{$user_id}'
                    ORDER BY pr.`created_at` DESC";

        $this->db->query($query);
        $as_talent = $this->db->getRowList();

        $query = "SELECT m.`firstname`, m.`lastname`, m.`photo`, tr.`task_id`, d.`name` AS `company_name`,
                    tr.`id` AS `review_id`, 'poster' AS `review_for`, tr.`poster_id` AS `reviewer_id`, tr.`review`, tr.`created_at`, 
                    ( SELECT `name` FROM `states` s WHERE s.`id` = m.`state` limit 1 ) AS `state`,
                    ( SELECT `name` FROM `countries` c WHERE c.`id` = m.`country` limit 1 ) AS `country`,
                    ( SELECT `slug` FROM `tasks` WHERE `id` = tr.`task_id` limit 1 ) AS `slug`,
                    ( SELECT `number` FROM `tasks` WHERE `id` = tr.`task_id` limit 1 ) AS `task_number`,
                    COALESCE(CONVERT((tr.`easy`+tr.`clear`+tr.`flexibility`+tr.`trustworthiness`)/4, DECIMAL(10,1)), 0) AS `total_rating`
                    FROM `talent_reviews` tr 
                    INNER JOIN `members` m ON m.`id` = tr.`poster_id`
                    LEFT JOIN `company_details` d ON d.`member_id` = m.`id` AND m.`type` = 1
                    WHERE tr.`reviewer_id` = '{$user_id}'
                    ORDER BY tr.`created_at` DESC";

        $this->db->query($query);
        $as_poster = $this->db->getRowList();

        return [ 'as_poster' => $as_poster, 'as_talent' => $as_talent ];
    }

    public function my_reviews(){
        $user_id = $this->user->info['id'];

        $query = "SELECT m.`firstname`, m.`lastname`, m.`photo`, pr.`talent_id` AS `review_for_id`, d.`name` AS `company_name`,
                    pr.`id` AS `review_id`, 'talent' AS `review_for`, pr.`task_id`, pr.`reviewer_id`, pr.`review`, pr.`created_at`,
                    ( SELECT `name` FROM `states` s WHERE s.`id` = m.`state` limit 1 ) AS `state`,
                    ( SELECT `name` FROM `countries` c WHERE c.`id` = m.`country` limit 1 ) AS `country`,
                    ( SELECT `slug` FROM `tasks` WHERE `id` = pr.`task_id` limit 1 ) AS `slug`,
                    ( SELECT `number` FROM `tasks` WHERE `id` = pr.`task_id` limit 1 ) AS `task_number`,
                    COALESCE(CONVERT((pr.`timeliness`+pr.`expertise`+pr.`satisfactory`+pr.`easy`)/4, DECIMAL(10,1)), 0) AS `total_rating`,
                    (
                        SELECT 
                        MAX(COALESCE(CONVERT((`timeliness`+`expertise`+`satisfactory`+`easy`)/4, DECIMAL(10,1)), 0)) 
                        FROM `poster_reviews` 
                        WHERE `talent_id` = '{$user_id}' AND `task_id` = pr.`task_id`
                    ) AS `top_rating`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`timeliness`), DECIMAL(10,1)), 0) 
                        FROM `poster_reviews` 
                        WHERE `talent_id` = '{$user_id}' AND `task_id` = pr.`task_id`
                    ) AS `timeliness_avg`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`expertise`), DECIMAL(10,1)), 0) 
                        FROM `poster_reviews` 
                        WHERE `talent_id` = '{$user_id}' AND `task_id` = pr.`task_id`
                    ) AS `expertise_avg`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`satisfactory`), DECIMAL(10,1)), 0) 
                        FROM `poster_reviews` 
                        WHERE `talent_id` = '{$user_id}' AND `task_id` = pr.`task_id`
                    ) AS `satisfactory_avg`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`easy`), DECIMAL(10,1)), 0) 
                        FROM `poster_reviews` 
                        WHERE `talent_id` = '{$user_id}' AND `task_id` = pr.`task_id`
                    ) AS `easy_avg`
                    
                    FROM `poster_reviews` pr
                    LEFT JOIN `members` m ON m.`id` = pr.`reviewer_id`
                    LEFT JOIN `talent_reviews` tr ON tr.`poster_id` = m.`id` AND tr.`task_id` = pr.`task_id`
                    LEFT JOIN `company_details` d ON d.`member_id` = m.`id` AND m.`type` = 1
                    WHERE pr.`talent_id` = '{$user_id}'
                    GROUP BY pr.`id`
                    ORDER BY pr.`created_at` DESC";

        $this->db->query($query);
        $as_talent = $this->db->getRowList();

        $query = "SELECT m.`firstname`, m.`lastname`, m.`photo`, tr.`task_id`, d.`name` AS `company_name`,
                    tr.`id` AS `review_id`, 'poster' AS `review_for`, tr.`poster_id` AS `review_for_id`, tr.`reviewer_id`, tr.`review`, tr.`created_at`, 
                    ( SELECT `name` FROM `states` s WHERE s.`id` = m.`state` limit 1 ) AS `state`,
                    ( SELECT `name` FROM `countries` c WHERE c.`id` = m.`country` limit 1 ) AS `country`,
                    ( SELECT `slug` FROM `tasks` WHERE `id` = tr.`task_id` limit 1 ) AS `slug`,
                    ( SELECT `number` FROM `tasks` WHERE `id` = tr.`task_id` limit 1 ) AS `task_number`,
                    COALESCE(CONVERT((tr.`easy`+tr.`clear`+tr.`flexibility`+tr.`trustworthiness`)/4, DECIMAL(10,1)), 0) AS `total_rating`,
                    (
                        SELECT 
                        MAX(CONVERT((`easy`+`clear`+`flexibility`+`trustworthiness`)/4, DECIMAL(10,1))) 
                        FROM `talent_reviews` 
                        WHERE `poster_id` = '{$user_id}' AND `task_id` = tr.`task_id`
                    ) AS `top_rating`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`easy`), DECIMAL(10,1)), 0)
                        FROM `talent_reviews` 
                        WHERE `poster_id` = '{$user_id}' AND `task_id` = tr.`task_id`
                    ) AS `easy_avg`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`clear`), DECIMAL(10,1)), 0)
                        FROM `talent_reviews` 
                        WHERE `poster_id` = '{$user_id}' AND `task_id` = tr.`task_id`
                    ) AS `clear_avg`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`flexibility`), DECIMAL(10,1)), 0)
                        FROM `talent_reviews` 
                        WHERE `poster_id` = '{$user_id}' AND `task_id` = tr.`task_id`
                    ) AS `flexibility_avg`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`trustworthiness`), DECIMAL(10,1)), 0)
                        FROM `talent_reviews` 
                        WHERE `poster_id` = '{$user_id}' AND `task_id` = tr.`task_id`
                    ) AS `trustworthiness_avg`
                    
                    FROM `talent_reviews` tr 
                    LEFT JOIN `members` m ON m.`id` = tr.`reviewer_id`
                    LEFT JOIN `poster_reviews` pr ON pr.`talent_id` = m.`id` AND pr.`task_id` = tr.`task_id`
                    LEFT JOIN `company_details` d ON d.`member_id` = m.`id` AND m.`type` = 1
                    WHERE tr.`poster_id` = '{$user_id}'
                    GROUP BY tr.`id`
                    ORDER BY tr.`created_at` DESC";

        $this->db->query($query);
        $as_poster = $this->db->getRowList();

        return [ 'as_poster' => $as_poster, 'as_talent' => $as_talent ];
    }

    public function user_reviews( $user_id = null, $task_id = null){
        if( is_null($user_id) ) return [];

        $query = "SELECT pr.*, m.`id` AS `talent_id`, m.`firstname`, m.`lastname`, m.`photo`, d.`name` AS `company_name`,
                    pr.`id` AS `review_id`, 'talent' AS `review_for`, pr.`task_id`, pr.`talent_id` AS `review_for_id`,
                    ( SELECT `name` FROM `states` s WHERE s.`id` = m.`state` limit 1 ) AS `state`,
                    ( SELECT `name` FROM `countries` c WHERE c.`id` = m.`country` limit 1 ) AS `country`,
                    ( SELECT `slug` FROM `tasks` WHERE `id` = pr.`task_id` limit 1 ) AS `slug`,
                    ( SELECT `number` FROM `tasks` WHERE `id` = pr.`task_id` limit 1 ) AS `task_number`,
                    COALESCE(CONVERT((pr.`timeliness`+pr.`expertise`+pr.`satisfactory`+pr.`easy`)/4, DECIMAL(10,1)), 0) AS `total_rating`,
                    (
                        SELECT 
                        MAX(COALESCE(CONVERT((`timeliness`+`expertise`+`satisfactory`+`easy`)/4, DECIMAL(10,1)), 0)) 
                        FROM `poster_reviews` 
                        WHERE `talent_id` = '{$user_id}' AND `task_id` = pr.`task_id`
                    ) AS `top_rating`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`timeliness`), DECIMAL(10,1)), 0) 
                        FROM `poster_reviews` 
                        WHERE `talent_id` = '{$user_id}' AND `task_id` = pr.`task_id`
                    ) AS `timeliness_avg`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`expertise`), DECIMAL(10,1)), 0) 
                        FROM `poster_reviews` 
                        WHERE `talent_id` = '{$user_id}' AND `task_id` = pr.`task_id`
                    ) AS `expertise_avg`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`satisfactory`), DECIMAL(10,1)), 0) 
                        FROM `poster_reviews` 
                        WHERE `talent_id` = '{$user_id}' AND `task_id` = pr.`task_id`
                    ) AS `satisfactory_avg`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`easy`), DECIMAL(10,1)), 0) 
                        FROM `poster_reviews` 
                        WHERE `talent_id` = '{$user_id}' AND `task_id` = pr.`task_id`
                    ) AS `easy_avg`
                    
                    FROM `poster_reviews` pr
                    INNER JOIN `members` m ON m.`id` = pr.`reviewer_id`
                    INNER JOIN `talent_reviews` tr ON tr.`poster_id` = m.`id` AND tr.`task_id` = pr.`task_id`
                    LEFT JOIN `company_details` d ON d.`member_id` = m.`id` AND m.`type` = 1
                    WHERE pr.`talent_id` = '{$user_id}'
                    ";
        if( !is_null($task_id) ){
            $query .= " AND pr.`task_id` = '{$task_id}'";
        }
        $query .= " GROUP BY pr.`id`
                    ORDER BY pr.`created_at` DESC";

        $this->db->query($query);
        $as_talent = $this->db->getRowList();

        $query = "SELECT m.`id` AS `talent_id`, m.`firstname`, m.`lastname`, m.`photo`, tr.`task_id`, d.`name` AS `company_name`,
                    tr.`id` AS `review_id`, 'poster' AS `review_for`, tr.`poster_id` AS `review_for_id`, tr.`reviewer_id`, tr.`review`, tr.`created_at`, 
                    ( SELECT `name` FROM `states` s WHERE s.`id` = m.`state` limit 1 ) AS `state`,
                    ( SELECT `name` FROM `countries` c WHERE c.`id` = m.`country` limit 1 ) AS `country`,
                    ( SELECT `slug` FROM `tasks` WHERE `id` = tr.`task_id` limit 1 ) AS `slug`,
                    ( SELECT `number` FROM `tasks` WHERE `id` = tr.`task_id` limit 1 ) AS `task_number`,
                    COALESCE(CONVERT((tr.`easy`+tr.`clear`+tr.`flexibility`+tr.`trustworthiness`)/4, DECIMAL(10,1)), 0) AS `total_rating`,
                    (
                        SELECT 
                        MAX(CONVERT((`easy`+`clear`+`flexibility`+`trustworthiness`)/4, DECIMAL(10,1))) 
                        FROM `talent_reviews` 
                        WHERE `poster_id` = '{$user_id}' AND `task_id` = tr.`task_id`
                    ) AS `top_rating`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`easy`), DECIMAL(10,1)), 0)
                        FROM `talent_reviews` 
                        WHERE `poster_id` = '{$user_id}' AND `task_id` = tr.`task_id`
                    ) AS `easy_avg`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`clear`), DECIMAL(10,1)), 0)
                        FROM `talent_reviews` 
                        WHERE `poster_id` = '{$user_id}' AND `task_id` = tr.`task_id`
                    ) AS `clear_avg`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`flexibility`), DECIMAL(10,1)), 0)
                        FROM `talent_reviews` 
                        WHERE `poster_id` = '{$user_id}' AND `task_id` = tr.`task_id`
                    ) AS `flexibility_avg`,
                    (
                        SELECT 
                        COALESCE(CONVERT(AVG(`trustworthiness`), DECIMAL(10,1)), 0)
                        FROM `talent_reviews` 
                        WHERE `poster_id` = '{$user_id}' AND `task_id` = tr.`task_id`
                    ) AS `trustworthiness_avg`
                    
                    FROM `talent_reviews` tr 
                    INNER JOIN `members` m ON m.`id` = tr.`reviewer_id`
                    INNER JOIN `poster_reviews` pr ON pr.`talent_id` = m.`id` AND pr.`task_id` = tr.`task_id`
                    LEFT JOIN `company_details` d ON d.`member_id` = m.`id` AND m.`type` = 1
                    WHERE tr.`poster_id` = '{$user_id}'";
        if (!is_null($task_id)) {
            $query .= " AND tr.`task_id` = '{$task_id}'";
        }
        $query .= " GROUP BY tr.`id`
                    ORDER BY tr.`created_at` DESC";

        $this->db->query($query);
        $as_poster = $this->db->getRowList();

        return [ 'as_poster' => $as_poster, 'as_talent' => $as_talent ];
    }

    public function has_rated_task($task_id = null, $user_id = null){
        if( ! isset($task_id) ) return false;

        $task = Task::init($this->db, $this->user)->get_task_by_id($task_id);
        $current_user_id = $this->user->info['id'];
        $current_user = $task['user_id'] === $current_user_id ? 'poster' : 'seeker';
        $table = $current_user === 'poster' ? 'poster_reviews' : 'talent_reviews';

        if($current_user === 'poster' && !isset($user_id)) return false;

        $query = "SELECT COUNT(`id`) FROM `{$table}` WHERE `task_id` = '{$task['task_id']}' AND `reviewer_id` = '{$current_user_id}' ";

        if( $current_user === 'poster' && isset($user_id) )
            $query .= "AND `talent_id` = '{$user_id}'";
        else
            $query .= "AND `poster_id` = '{$task['user_id']}'";

        return (int) $this->db->getValue($query) > 0;
    }

}