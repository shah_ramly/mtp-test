<?php

use Carbon\Carbon;

class cms{
	public $db;
	public $user;
	
	public function __construct($db, $user){
		$this->db = $db;
		$this->user = $user;
		$this->lang = !empty($_SESSION[WEBSITE_PREFIX.'LANGUAGE']) ? $_SESSION[WEBSITE_PREFIX.'LANGUAGE'] : 'en';
		$this->defaultLimit = 20;
		$this->deliveryMinDate = date('Y-m-d', strtotime('today + 1 days + 48 hours'));
		$this->sandbox = false;
		$this->shipping_time = $this->shipping_time();
		$this->date = Carbon::today();
		$this->fbAppId = '201588961772295'; /* '252922739292236'; */
		$this->fbAppSecret = '56b9f4187fbf4fafd1581ff5dbac68b0'; /* '1bcf46851ad2f17e533e8f8c71caba7e'; */
		$this->googleClientID = '315705742999-ebt3dv07drso8s9olif15ftn04gk30ec.apps.googleusercontent.com';
		$this->googleClientSecret = 'ms2i8Dts7AEl8dRuWtU7U2pd';
		$this->site_host = (!empty($_SERVER['HTTPS']) ? 'https://' : 'http://') . ($_SERVER['HTTP_HOST'] ?? '');
	}

    private function is_ajax(){
        return 'xmlhttprequest' === strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '' );
    }
	
	public function menus($type = ''){
		$menus = '';
		$i = 0;
		
		$categories = $this->categories();
		if($categories){
			foreach($categories as $cat){
				$menus[$i]['title'] = $cat['title'];
				$menus[$i]['href'] = 'cat/' . $cat['id'] . '-' . slug($cat['title']);

				$subs = $this->categories($cat['id']);
				if($subs){
					$j = 0;
					foreach($subs as $sub){
						$menus[$i]['sub'][$j]['title'] = $sub['title'];
						$menus[$i]['sub'][$j]['href'] = 'cat/' . $sub['id'] . '-' . slug($sub['title']);
						$j++;
					}
				}				
				$i++;
			}
		}
		
		if(!empty($menus)){	
			foreach($menus as $i => $menu){
				if(!empty($menu['sub'])){
					foreach($menu['sub'] as $j => $sub){
						if('/' . $type == $sub['href']){
							$menus[$i]['active'] = true;
							$menus[$i]['sub'][$j]['active'] = true;
						}
					}
				}
			}
		}
				
		return $menus;
	}
	
	public function request($field = ''){
		if(!empty($field)){
			$val = !empty($_REQUEST[$field])? $_REQUEST[$field] : (!empty($_REQUEST[$field]) && $_REQUEST[$field] == '0' ? '0' : false);
		}else{
			$val = $_REQUEST;
		}
		return $val;
	}
	
	public function visitor(){
		$this->db->query('SELECT * FROM visitors WHERE ip_address = ' . $this->db->escape($_SERVER['REMOTE_ADDR']));
		$visited = $this->db->getSingleRow();
		
		if(!$visited){
			$this->db->table("visitors");
			$this->db->insertArray(array(
				"ip_address" => $_SERVER['REMOTE_ADDR'],
				"visit_count" => "1",
				"date" => 'NOW()',
			));
			$this->db->insert();
		}else{
			$this->db->table("visitors");
			$this->db->updateArray(array(
				"visit_count" => $visited['visit_count'] + 1,
				"date" => 'NOW()',
			));
			$this->db->whereArray(array(
				"ip_address" => $_SERVER['REMOTE_ADDR'],
			));
			$this->db->update();
		}
	}
	
	public function price($price, $operator = false){
		$currency = 'RM';
		if($operator) $return = ($price > 0 ? '+' : '-') . $currency . number_format(str_replace('-', '', $price), 2);
		else $return = $currency . number_format($price, 2);
		return $return;
	}
	
	public function settings(){
        if( ! isset($_SESSION['settings']) ) {
            $setting  = [];
            $settings = [];

            $this->db->query("SELECT `name`, `value` FROM settings");
            $settings = $this->db->getRowList();

            foreach ($settings as $keys) {
                $setting[$keys['name']] = $keys['value'];
            }

            if ($setting['date_format'] == 'custom') $setting['date_format'] = $setting['date_format_custom'];
            if ($setting['time_format'] == 'custom') $setting['time_format'] = $setting['time_format_custom'];
            if ($setting['session_timeout'] == 'custom') $setting['session_timeout'] = $setting['session_timeout_custom'];
            $_SESSION['settings'] = $setting;
        }else{
            $setting = $_SESSION['settings'];
        }
        return $setting;
	}
			
	public function countries(){
		$this->db->query("SELECT `id`, `name` FROM `countries` WHERE `status` = '1' ORDER BY `name`");
		return $this->db->getRowList();
	}
	
	public function country($id){
		$this->db->query("SELECT `id`, `name`, `iso2` FROM `countries` WHERE `id` = " .  $this->db->escape($id));
		return $this->db->getSingleRow();
	}
	
	public function states($country = 132){
		$this->db->query("SELECT `id`, `name` FROM `states` WHERE `status` = '1' AND `country_id` = " . $this->db->escape($country));
		$states = $this->db->getRowList();
		if( !$this->is_ajax() )
			$states[] = ['id' => '0', 'name' => lang('profile_nationality_malaysian_others')];

		return $states;
	}
	
	public function cities($state){
		$this->db->query("SELECT `id`, `name` FROM `cities` WHERE `status` = '1' AND `state_id` = " . $this->db->escape($state));
        $cities = $this->db->getRowList();
        if( !$this->is_ajax() )
            $cities[] = ['id' => '0', 'name' => lang('profile_nationality_malaysian_others')];

		return $cities;
	}
	
	public function getTopJobs(){
		$this->db->query("SELECT a.title ,COUNT(b.task_id) AS count, a.slug FROM tasks a, user_task b WHERE a.id = b.task_id AND a.status = 'published' GROUP BY b.task_id ORDER BY b.task_id DESC LIMIT 10");
		//die($this->db->query);
		return $this->db->getRowList();
	}
	
	public function getTaskPublic(){

		$this->db->query("SELECT COUNT(*) FROM tasks a, external_tasks b WHERE a.status IN ('published') AND b.status in ('published')");
		$count = $this->db->getValue();

		//$this->db->query("SELECT DISTINCT a.*,b.title AS category_name FROM tasks a, task_category b WHERE a.status IN ('published') AND a.category = b.id  ORDER BY RAND () LIMIT 8");
		$this->db->query("SELECT DISTINCT *
							FROM 
							(
								SELECT t.`title`, t.`number`, t.`description`, t.`slug`, t.`budget`, 'int' AS `type`, 1 AS `priority`, c.`title` AS `category_name`, t.`state_country` 
								FROM `tasks` t 
								LEFT JOIN `task_category` c ON c.`id` = t.`category` 
								WHERE t.`status` = 'published'
								union
								SELECT `title`, `number`, `description`, `slug`, `budget`, 'ext' AS `type`, 2 AS `priority`, 'Others' AS `category_name`, `state_country` 
								FROM `external_tasks` WHERE `status` = 'published'
							) AS `tasks`
							
							ORDER BY `priority`, RAND()
							LIMIT 8
		");
		$list = $this->db->getRowList();
		
		return ['list' => $list, 'count' => $count];
	}

	public function getMoreTaskPublic(){

        if( 'xmlhttprequest' === strtolower( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] : '' ) ) {
            $page   = request('page') ? (int)request('page') : 1;
            $offset = ($page * 8) - 8;

            //$this->db->query("SELECT DISTINCT a.*,b.title AS category_name FROM tasks a, category b WHERE a.status IN ('published') AND a.category = b.id  ORDER BY RAND () LIMIT {$offset},8");
            $this->db->query("SELECT DISTINCT *
							FROM 
							(
								SELECT t.`title`, t.`number`, t.`description`, t.`slug`, t.`budget`, 'int' AS `type`, 1 AS `priority`, c.`title` AS `category_name`, t.`state_country`
								FROM `tasks` t 
								LEFT JOIN `task_category` c ON c.`id` = t.`category` 
								WHERE t.`status` = 'published'
								union
								SELECT `title`, `number`, `description`, `slug`, `budget`, 'ext' AS `type`, 2 AS `priority`, 'Others' AS `category_name`, `state_country` 
								FROM `external_tasks` WHERE `status` = 'published'
							) AS `tasks`
							
							ORDER BY `priority`, RAND() 
							LIMIT {$offset},8");
            $list = $this->db->getRowList();

            return partial('partial/home/featured-list.html.php', ['list' => $this->db->getRowList()]);
        }
	}

	public function getHomeTaskPublic(){

		$this->db->query("SELECT COUNT(*) FROM tasks a, category b WHERE a.status IN ('published') AND a.category = b.id AND a.location = 'remotely'");
		$count = $this->db->getValue();

		$this->db->query("SELECT a.*,b.title AS category_name FROM tasks a, category b WHERE a.status IN ('published') AND a.category = b.id AND a.location = 'remotely'  ORDER BY created_at DESC LIMIT 8");
		$list = $this->db->getRowList();
		
		return ['list' => $list, 'count' => $count];
	}

	public function getMoreHomeTaskPublic(){

        if( 'xmlhttprequest' === strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '' ) ) {
            $page   = request('page') ? (int)request('page') : 1;
            $offset = ($page * 8) - 8;

            $this->db->query("SELECT a.*,b.title AS category_name FROM tasks a, category b WHERE a.status IN ('published') AND a.category = b.id AND a.location = 'remotely'  ORDER BY created_at DESC LIMIT {$offset},8");
            return partial('partial/home/from-home.html.php', ['list' => $this->db->getRowList()]);
        }
	}

	public function getOnSiteTaskPublic(){

		$this->db->query("SELECT COUNT(*) FROM tasks a, external_tasks b WHERE a.status IN ('published') AND b.status IN('published') AND a.location != 'remotely' AND b.location != 'remotely'");
		$count = $this->db->getValue();

		$this->db->query("SELECT DISTINCT *
							FROM 
							(
								SELECT t.`title`, t.`number`, t.`description`, t.`slug`, t.`budget`, 'int' AS `type`, 1 AS `priority`, c.`title` AS `category_name`, t.`created_at`, t.`state_country`
								FROM `tasks` t 
								LEFT JOIN `task_category` c ON c.`id` = t.`category` 
								WHERE t.`status` = 'published' AND t.`location` != 'remotely'
								union
								SELECT `title`, `number`, `description`, `slug`, `budget`, 'ext' AS `type`, 2 AS `priority`, 'Others' AS `category_name`, `created_at`, `state_country` 
								FROM `external_tasks` WHERE `status` = 'published' AND `location` != 'remotely'
							) AS `tasks`
							
							ORDER BY `priority`, `created_at`
							LIMIT 8");
		$list = $this->db->getRowList();
		
		return ['list' => $list, 'count' => $count];
	}
	public function getMoreOnSiteTaskPublic(){
        if( 'xmlhttprequest' === strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '' ) ) {
            $page   = request('page') ? (int)request('page') : 1;
            $offset = ($page * 8) - 8;

            $this->db->query("SELECT DISTINCT *
							FROM 
							(
								SELECT t.`title`, t.`number`, t.`description`, t.`slug`, t.`budget`, 'int' AS `type`, 1 AS `priority`, c.`title` AS `category_name`, t.`created_at`, t.`state_country`
								FROM `tasks` t 
								LEFT JOIN `task_category` c ON c.`id` = t.`category` 
								WHERE t.`status` = 'published' AND t.`location` != 'remotely'
								union
								SELECT `title`, `number`, `description`, `slug`, `budget`, 'ext' AS `type`, 2 AS `priority`, 'Others' AS `category_name`, `created_at`, `state_country` 
								FROM `external_tasks` WHERE `status` = 'published' AND `location` != 'remotely'
							) AS `tasks`
							
							ORDER BY `priority`, `created_at`
							LIMIT {$offset},8");

            return partial("/partial/home/on-site-list.html.php", ['list' => $this->db->getRowList()]);;
        }
	}
	public function getRecentTaskPublic(){

		$this->db->query("SELECT COUNT(*) FROM tasks a, external_tasks b WHERE a.status IN ('published') AND b.status IN ('published')");
		$count = $this->db->getValue();

		$this->db->query("SELECT DISTINCT *
							FROM 
							(
								SELECT t.`title`, t.`number`, t.`description`, t.`slug`, t.`budget`, 'int' AS `type`, 1 AS `priority`, c.`title` AS `category_name`, t.`created_at`, t.`state_country`
								FROM `tasks` t 
								LEFT JOIN `task_category` c ON c.`id` = t.`category` 
								WHERE t.`status` = 'published'
								union
								SELECT `title`, `number`, `description`, `slug`, `budget`, 'ext' AS `type`, 2 AS `priority`, 'Others' AS `category_name`, `created_at`, `state_country` 
								FROM `external_tasks` WHERE `status` = 'published'
							) AS `tasks`
							
							ORDER BY `priority`, `created_at`
							LIMIT 8");
		$list = $this->db->getRowList();
		
		return ['list' => $list, 'count' => $count];

	}

	public function getMoreRecentTaskPublic(){

        if( 'xmlhttprequest' === strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '' ) ) {
            $page   = request('page') ? (int)request('page') : 1;
            $offset = ($page * 8) - 8;

            $this->db->query("SELECT DISTINCT *
							FROM 
							(
								SELECT t.`title`, t.`number`, t.`description`, t.`slug`, t.`budget`, 'int' AS `type`, 1 AS `priority`, c.`title` AS `category_name`, t.`created_at`, t.`state_country`  
								FROM `tasks` t 
								LEFT JOIN `task_category` c ON c.`id` = t.`category` 
								WHERE t.`status` = 'published'
								union
								SELECT `title`, `number`, `description`, `slug`, `budget`, 'ext' AS `type`, 2 AS `priority`, 'Others' AS `category_name`, `created_at`, `state_country` 
								FROM `external_tasks` WHERE `status` = 'published'
							) AS `tasks`
							
							ORDER BY `priority`, `created_at`
							LIMIT {$offset},8");
            return partial("/partial/home/recent-list.html.php", ['list' => $this->db->getRowList()]);
        }

	}
	public function getNearestTaskPublic(){
		//match state and country from google IP
		if( !isset($_SESSION['state_country']) ) {
            $country = "";
			$region  = "";
			
            $ip            = $_SERVER['REMOTE_ADDR'];
            $details       = json_decode(file_get_contents("http://ip-api.com/json/{$ip}"), true);
            $country       = !empty($details['country']) ? $details['country'] : 'Malaysia';
            $region        = !empty($details['regionName']) ? $details['regionName'] : 'Kuala Lumpur';
            $state_country = $region . ', ' . $country;
			$_SESSION['state_country'] = ['region' => $region, 'country' => $country, 'state_country' => $state_country];
			$lat = !empty($details['lat']) ? $details['lat'] : '';
			$long = !empty($details['lon']) ? $details['lon'] : '';
            $_SESSION['current_user_location'] = "{$lat},{$long}";
        }

		$region = $_SESSION['state_country']['region'];

        $this->db->query("SELECT COUNT(*) FROM tasks a, external_tasks b WHERE a.status IN ('published') AND b.`status` IN('published') AND a.`state_country` LIKE '%{$region}%' AND b.`state_country` LIKE '%{$region}%'");
        $count = $this->db->getValue();
		
		$this->db->query("SELECT DISTINCT *
							FROM 
							(
								SELECT t.`title`, t.`number`, t.`description`, t.`slug`, t.`budget`, 'int' AS `type`, 1 AS `priority`, c.`title` AS `category_name`, t.`created_at`, t.`state_country`  
								FROM `tasks` t 
								LEFT JOIN `task_category` c ON c.`id` = t.`category` 
								WHERE t.`status` = 'published' AND t.`state_country` LIKE '%{$region}%'
								union
								SELECT `title`, `number`, `description`, `slug`, `budget`, 'ext' AS `type`, 2 AS `priority`, 'Others' AS `category_name`, `created_at`, `state_country` 
								FROM `external_tasks` WHERE `status` = 'published' AND `state_country` LIKE '%{$region}%'
							) AS `tasks`
							
							ORDER BY `priority`, `created_at`
							LIMIT 8");
        $list = $this->db->getRowList();

		return ['list' => $list, 'count' => $count];
	}

	public function getMoreNearestTaskPublic(){
		if( 'xmlhttprequest' === strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '' ) ) {
            $page   = request('page') ? (int)request('page') : 1;
            $offset = ($page * 8) - 8;
            $region = $_SESSION['state_country']['region'];

            $this->db->query("SELECT DISTINCT *
							FROM 
							(
								SELECT t.`title`, t.`number`, t.`description`, t.`slug`, t.`budget`, 'int' AS `type`, 1 AS `priority`, c.`title` AS `category_name`, t.`created_at`, t.`state_country`  
								FROM `tasks` t 
								LEFT JOIN `task_category` c ON c.`id` = t.`category` 
								WHERE t.`status` = 'published' AND t.`state_country` LIKE '%{$region}%'
								union
								SELECT `title`, `number`, `description`, `slug`, `budget`, 'ext' AS `type`, 2 AS `priority`, 'Others' AS `category_name`, `created_at`, `state_country` 
								FROM `external_tasks` WHERE `status` = 'published' AND `state_country` LIKE '%{$region}%'
							) AS `tasks`
							
							ORDER BY `priority`, `created_at`
                            LIMIT {$offset},8");
            return partial("/partial/home/nearest-list.html.php", ['list' => $this->db->getRowList()]);
        }
	}
	
	public function getTopParents(){
		$this->db->query("SELECT COUNT(*) AS numbers,category FROM tasks GROUP BY category ORDER BY numbers DESC LIMIT 3");	
		$parents = $this->db->getRowList();
		
		foreach($parents as $i =>$parent)
			{
				$parents[$i]['category_name'] = $this->getCategoryName($parents[$i]['category']);	
				// Get sub category
				$children = $this->getTopChildren($parents[$i]['category']);
				//var_dump($children);
				foreach($children as $x=>$child){
					//Get sub category name
					$parents[$i][$x]['child_name'] = $this->getCategoryName($child['sub_category']);
					$parents[$i][$x]['total'] = $child['total'];
				}
				
			}
		
		return $parents;
	}
	
	public function getTopChildren($parent_id){
		$this->db->query("SELECT COUNT(*) as total, sub_category FROM tasks WHERE category = '".$parent_id."' GROUP BY sub_category ORDER BY total DESC LIMIT 7");	
		$childs = $this->db->getRowList();
		//var_dump($childs);
		
		
		return $childs;
	}
	
	public function getCategoryName($id = 0){
		$this->db->query("SELECT title FROM task_category WHERE id = " .  $this->db->escape($id));	
		$key = $this->db->getSingleRow();

		if(!empty($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) && $_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] != 'EN'){
			$language_id = $this->db->getValue("SELECT id FROM `language` WHERE status = '1' AND code = " . $this->db->escape($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']));
			if($language_id){
				$text = $this->db->getValue("SELECT title FROM `task_category_language_text` WHERE language_id = " . $this->db->escape($language_id) . " AND category_id = " . $this->db->escape($id));
				if($text) $key['title'] = $text;
			}
		}

		return $key;
	}
	
	public function getCategories($parent_id = 0, $table = 'task_category'){
		$this->db->query("SELECT * FROM `{$table}` WHERE status = '1' AND parent = " . $this->db->escape($parent_id));
		$keys = $this->db->getRowList();
		
		if(!empty($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) && $_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] != 'EN'){
			$language_id = $this->db->getValue("SELECT id FROM `language` WHERE status = '1' AND code = " . $this->db->escape($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']));
			if($language_id){
				foreach($keys as $i => $key){
					$text = $this->db->getValue("SELECT title FROM `{$table}_language_text` WHERE language_id = " . $this->db->escape($language_id) . " AND category_id = " . $this->db->escape($key['id']));
					if($text) $keys[$i]['title'] = $text;
				}
			}
		}

		return $keys;
	}

	public function getIndustryName($id = 0){
		return $this->db->getValue("SELECT title FROM industry WHERE status = '1' AND id = " .  $this->db->escape($id));	
	}
	
	public function getIndustries2Level(){
		if(!empty($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) && $_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] != 'EN'){
			$language_id = $this->db->getValue("SELECT id FROM `language` WHERE status = '1' AND code = " . $this->db->escape($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']));
		}

		$this->db->query("SELECT * FROM industry WHERE status = '1' AND parent = '0'");	
		$keys = $this->db->getRowList();

		if($keys){
			foreach($keys as $i => $key){
				if(!empty($language_id)){
					$text = $this->db->getValue("SELECT title FROM `industry_language_text` WHERE language_id = " . $this->db->escape($language_id) . " AND industry_id = " . $this->db->escape($key['id']));
					if($text) $keys[$i]['title'] = $text;
				}

				$this->db->query("SELECT * FROM industry WHERE status = '1' AND parent = " . $this->db->escape($key['id']));	
				$keys[$i]['subs'] = $this->db->getRowList();

				if(!empty($language_id)){
					foreach($keys[$i]['subs'] as $j => $subs){
						$text = $this->db->getValue("SELECT title FROM `industry_language_text` WHERE language_id = " . $this->db->escape($language_id) . " AND industry_id = " . $this->db->escape($key['id']));
						if($text) $keys[$i]['title'] = $text;
					}
				}
			}
		}

		return $keys;
	}

	public function getIndustriesByParent($parent_id = 0){
		$this->db->query("SELECT * FROM industry WHERE status = '1' AND parent = " . $this->db->escape($parent_id));	
		return $this->db->getRowList();
	}

	public function getCategoriesAjax($parent_id = 0, $type = null){
		if( !is_null($type) && $type === 'task' ) $table = 'task_category'; else $table = 'category';
		$categories = $this->getCategories($parent_id, $table);
		$categoryList = '<option disabled selected>'. lang('post_step_1_sub_category') .'</option>';
		
		if($categories){
			foreach($categories as $key){
				$categoryList .= '<option value="' . $key['id'] . '">' . $key['title'] . '</option>';	
			}
		}

		//$categoryList .= '<option value="' . $key['id'] . '">' . $key['title'] . '</option>';	
		
		$return['html'] = $categoryList;
		$return['error'] = false;
		$return['msg'] = "Request success."; 
		
		return json($return);
	}

	public function getStates($country){
		$states = $this->states($country);
		$stateList = '<option value="" '. (!empty($states) ? 'selected' : '') .'>'. lang('profile_state_placeholder') .'</option>';
		//$stateList = '';
		
		if($states){
			foreach($states as $keys){
				$stateList .= '<option value="' . $keys['id'] . '">' . $keys['name'] . '</option>';	
			}
		}//else{

		$stateList .= '<option value="0" '. (empty($states) ? 'selected' : '') .'>' . lang('profile_nationality_malaysian_others') . '</option>';
        //}
		
		$return['states'] = $stateList;
		$return['error'] = false;
		$return['msg'] = "Request success."; 
		
		return json($return);
	}

	public function getCities($state_id = 1944 ){
		$states = $this->cities($state_id);
		$stateList = '<option value="" '. (!empty($states) ? 'selected' : '') .'>'. lang('profile_town_placeholder') .'</option>';
		//$stateList = '';
		
		if($states){
			foreach($states as $keys){
				$stateList .= '<option value="' . $keys['id'] . '">' . $keys['name'] . '</option>';	
			}
		}//else{

        $stateList .= '<option value="0" '. (empty($states) ? 'selected' : '') .'>' . lang('profile_nationality_malaysian_others') . '</option>';
		//}
		
		$return['states'] = $stateList;
		$return['error'] = false;
		$return['msg'] = "Request success."; 
		
		return json($return);
	}
	
	public function getCountryName($id){
		return $this->db->getValue("SELECT `name` FROM `countries` WHERE id = " . $this->db->escape($id));
	}
	
	public function getStateName($id){
		$this->db->query("SELECT `name` FROM `states` WHERE id = " . $this->db->escape($id));
		$state = $this->db->getSingleRow();
		return !empty($state['name']) ? $state['name'] : '';
	}
	
	public function logout(){
		session_destroy();
		redirect_to( option('site_uri') . url_for('/') );
	}
	
	public function products($catID = 0){
		if(!empty($catID)) $this->db->query("SELECT * FROM products WHERE status IN (1,8) AND category = " . $this->db->escape($catID) . " ORDER BY title");	
		else $this->db->query("SELECT * FROM products WHERE status IN (1,8) ORDER BY title");	
		return $this->db->getRowList();
	}
	
	public function categories($parent = 0){
		$this->db->query("SELECT * FROM category WHERE status = '1' AND parent = " . $this->db->escape($parent) . " ORDER BY sortby");	
		$categories = $this->db->getRowList();
		
		return $categories;
	}
	
	public function holidays(){
		$this->db->query("SELECT * FROM holidays WHERE status = '1'");	
		return $this->db->getRowList();
	}
	
	public function not_found(){
        route_missing( request_method(), request_uri() );
		//return render('404.php', 'layout/default.php');
	}

    public function getPublicCompanyNameList($except = ''){
        $where = '';

        if( !empty($except) ){
            $except = $this->db->escape($except);
            $where = " WHERE `url` != {$except} ";
        }

        $this->db->query("SELECT `name`, `url`, `member_id` FROM `company_details` {$where} ");
        return $this->db->getRowList();
    }

	public function checkCompanyUrlAvailability($company_name = ''){
        $companies_name = $this->getPublicCompanyNameList($this->user->info['company']['url']);
        // check for any reserved/defined routes in the system
        $routes = array_unique(array_column(route(), 'path'));
        $routes = array_merge(
            $routes,
            array_column($companies_name, 'url'),
            [
                '/personalize',
                '/my_profile',
                '/my_cv',
                '/personalize',
                '/my_profile',
                '/my_cv',
                '/profile',
            ]
        );
        $routes = array_map(function($route){ return ltrim($route, '/'); }, $routes);

        return !in_array($company_name, $routes);
	}

	public function checkPublicCompanyNameAvailability(){
		if( empty(request('company_name')) ) return false;

		$company_name = sanitize(request('company_name'));
		$company_name = preg_replace("/[^a-zA-Z\-]/", '', strtolower($company_name));
		$available = $this->checkCompanyUrlAvailability($company_name);

		return json(['available' => $available]);
	}
	
	public function page($type){
		$this->db->query("SELECT * FROM `company_details` WHERE `url` = " . $this->db->escape($type));
		$page = $this->db->getSingleRow();

		if($page){
            $this->db->query("SELECT * FROM `members` WHERE `id` = '{$page['member_id']}' LIMIT 1");
            $company = $this->db->getSingleRow();
			$user_id = $page['member_id'];

            $user = new stdClass;
            $user->info = $this->user->info($user_id);
            set('company', $user);
            $current_user = $this->user;
            if(! $current_user->logged) $current_user->info['id'] = 0;

            $taskObject = new Task($this->db, $this->user);
            $jobObject  = new Job($this->db, $this->user);
            $taskCentre = new TaskCentre($this->db, $this->user);
            //$user_id    = params('id') ? (int)str_replace('TL', '', sanitize(params('id'))) - 20100 : null;
            $all        = $taskObject->get_my_published_tasks_jobs($user_id);
            $tasks      = [];
            $jobs       = [];

            foreach ($all as $key => $task) {
                if($task['the_type'] === 'task') {
                    $task = $taskObject->get_task_by_id($task['task_id']);
                    unset($task['password']);
                    $tasks[$key] = $task;
                    $tasks[$key]['type'] = 'task';
                    $tasks[$key]['country']         = $this->getCountryName($task['country']);
                    $tasks[$key]['state']           = $this->getStateName($task['state']);
                    $tasks[$key]['published_count'] = $taskObject->get_published_tasks_count($task['user_id']);
                    $tasks[$key]['rating']          = $taskCentre->get_ratings($user_id);
                    $tasks[$key]['questions']       = $taskObject->questions($task['task_id']);
                    $tasks[$key]['disputed_count']  = $taskObject->get_status_count('disputed', $task['user_id']);
                }else{
                    $job  = $jobObject->get_job_by_id($task['task_id']);
                    unset($job['password']);
                    $jobs[$key] = $job;
                    $jobs[$key]['type'] = 'job';
                    $jobs[$key]['country']         = $this->getCountryName($job['job_country']);
                    $jobs[$key]['state']           = $this->getStateName($job['job_state']);
                    $jobs[$key]['published_count'] = $jobObject->get_published_jobs_count($job['user_id']);
                    $jobs[$key]['rating']          = $taskCentre->get_ratings($user_id);
                    $jobs[$key]['questions']       = $jobObject->questions($job['job_id']);
                    $jobs[$key]['disputed_count']  = $jobObject->get_status_count('disputed', $job['user_id']);
                }
            }

            if(isset($current_user)){
                $applied_jobs    = $jobObject->get_applied_jobs($this->user->info['id']);
                $applied_tasks   = $taskObject->get_applied_tasks($this->user->info['id']);
                $favourite_jobs  = $jobObject->get_favourites($this->user->info['id']);
                $favourite_tasks = $taskObject->get_favourites($this->user->info['id']);

                $this->user->applied_ids = array_merge(array_column($applied_jobs, 'id'), array_column($applied_tasks, 'id'));
                $this->user->favourite_ids = array_merge(array_column($favourite_jobs, 'task_id'), array_column($favourite_tasks, 'task_id'));
            }

            set('current_user', $current_user);
            set('user', $user);
            set('posts', array_merge($tasks, $jobs));
            set('tasks', $tasks);
            set('jobs', $jobs);
            set('ratings', $taskCentre->get_ratings($user_id));
            set('highest_rating', $taskCentre->get_highest_rating($user_id));
			set('title_prefix', $page['name'] . ' | ');
			return render('company-public-microsite.html.php', '');
		}else{
			return $this->not_found();
		}
	}
		
	public function sort_array_of_array(&$array, $subfield, $sort = 'asc'){
		$sortarray = array();
		foreach ($array as $key => $row)
		{
			$sortarray[$key] = $row[$subfield];
		}
		if($sort == 'asc') array_multisort($sortarray, SORT_ASC, $array);
		else array_multisort($sortarray, SORT_DESC, $array);
	}
	
	public function productsByCat($id, $page = 1){
		$id = explode('-', $id);
		$id = current($id);
		
		$sliders = '';
		
		$keyword = $this->request('keyword') ? $this->request('keyword') : strtolower($this->request('keyword'));
		$sort = $this->request('sort') ? $this->request('sort') : '';
		
		$qryOrder = '';
		if($sort == 'new') $qryOrder = ' ORDER BY is_new DESC';
		if($sort == 'sale') $qryOrder = ' ORDER BY is_sale DESC';
		if($sort == 'availability') $qryOrder = ' ORDER BY status';
		if($sort == 'price-low') $qryOrder = ' ORDER BY price ASC';
		if($sort == 'price-high') $qryOrder = ' ORDER BY price DESC';
		
		$qryKeyword = '';
		if(!empty($keyword)) $qryKeyword = " AND LOWER(title) LIKE " . $this->db->escape('%' . $keyword . '%');
		
				
		$this->db->query("SELECT * FROM category WHERE status = '1' AND id = " . $this->db->escape($id));	
		$cat = $this->db->getSingleRow();
		
		$this->db->query("SELECT * FROM products WHERE status IN (1,8) AND (categories = '" . $id . "' OR categories LIKE '%," . $id . "' OR categories LIKE '" . $id . ",%' OR categories LIKE '%," . $id . ",%')" . $qryKeyword . $qryOrder);	
		$pagination = new pagination($this->db->getRowList(), $this->defaultLimit, $page, url_for('/cat/' . $cat['id']. '-' . slug($cat['title'])));
		$products = $pagination->result();
		$totalPage = $pagination->totalPage;
		
		if($products){
			foreach($products as $i => $product){
				$options = unserialize($product['options']);
				if($options){
					$stock = 0;
					foreach($options as $j => $option){
						foreach($option['values'] as $value){
							$value['stock'] = !empty($value['stock']) ? $value['stock'] : 0;
							$stock += $value['stock'];
						}
					}
					if($stock == '0') $products[$i]['status'] = '8';
				}
			}
		}
		
		$breadcrumbs[] = array($cat['id'] . '-' . slug($cat['title']), $cat['title']);
		
		if($cat['parent']){
			$this->db->query("SELECT * FROM category WHERE status = '1' AND id = " . $this->db->escape($cat['parent']));	
			$parent = $this->db->getSingleRow();
			
			while(!empty($parent)){
				$breadcrumbs[] = array($parent['id'] . '-' . slug($parent['title']), $parent['title']);
				$this->db->query("SELECT * FROM category WHERE status = '1' AND id = " . $this->db->escape($parent['parent']));	
				$parent = $this->db->getSingleRow();
			}
		}
		
		$this->db->query("SELECT * FROM category WHERE status = '1' AND parent = " . $this->db->escape($id));	
		$listCategories = $this->db->getRowList();
		
		if(!$listCategories){
			//$sliders = unserialize($cat['images']);
		}
				
		$breadcrumbs = array_reverse($breadcrumbs);
		
		set('sliders', $sliders);
		set('currentCatID', $id);
		set('keyword', $keyword);
		set('sort', $sort);
		set('products', $products);
		set('cat', $cat);
		set('listCategories', $listCategories);
		set('breadcrumbs', $breadcrumbs);
		set('pagination', $pagination);
		option('title_prefix', $cat['title'] . ' | ');
		return render('categories.php', 'layout/default.php');
	}
	
	public function productById($id = 0){
		$id = explode('-', $id);
		$id = current($id);
		$suggestions = array();
				
		$this->db->query("SELECT * FROM products WHERE status IN (1,8) AND id = " . $this->db->escape($id));	
		$product = $this->db->getSingleRow();
		
		if($product){
			$stock = $product['quantity'];
			$categories = explode(',', $product['categories']);
			$product_suggestion = explode(',', $product['product_suggestion']);
			if($product_suggestion){
				foreach($product_suggestion as $key){
					$this->db->query("SELECT * FROM products WHERE status IN (1,8) AND id = " . $this->db->escape($key));	
					$suggestions[] = $this->db->getSingleRow();
				}
			}
			
			$discount = $product['price'] - $product['sale_price'];
			$discount = $discount / $product['price'] * 100;
			$discount = number_format($discount);
			$product['discount'] = $discount;
			
			$options = unserialize($product['options']);
			if($options){
				$stock = 0;
				foreach($options as $j => $option){
					foreach($option['values'] as $value){
						$value['stock'] = !empty($value['stock']) ? $value['stock'] : 0;
						$stock += $value['stock'];
					}
				}
			}
			
			if($stock == '0') $product['status'] = '8';
			
			if($categories){
				foreach($categories as $catID){
					$this->db->query("SELECT * FROM category WHERE status = '1' AND id = " . $this->db->escape($catID));	
					$cat = $this->db->getSingleRow();
					if($cat){
						$breadcrumbs[] = array($cat['id'] . '-' . slug($cat['title']), $cat['title']);
					}
				}
			}else $breadcrumbs = array();
			
			set('breadcrumbs', $breadcrumbs);
			set('product', $product);
			set('images', unserialize($product['images']));
			set('options', unserialize($product['options']));
			set('colors', unserialize($product['colors']));
			set('suggestions', array_filter($suggestions));
			return render('product.php', 'layout/default.php');
		}else{
			return $this->not_found();
		}
	}
	
	public function productBox($product, $col = 'col-md-3 col-sm-4 col-xs-6'){
		$images = unserialize($product['images']);
		$image = current($images);
		?>
			<div class="box <?php echo $col; ?>">
            	<div class="thumb<?php /*echo $product['status'] == '8' ? ' sold-out' : '';*/ //byte2c ?>"><a href="<?php echo url_for('/product/' . $product['id'] . '-' . slug($product['title'])); ?>"><img src="<?php echo imgCrop($image['src'], 200, 200); ?>" alt="<?php echo $image['alt']; ?>"></a></div>
                <div class="title"><a href="<?php echo url_for('/product/' . $product['id'] . '-' . slug($product['title'])); ?>" title="<?php echo $product['title']; ?>" data-pos="top"><?php echo strip($product['title'], 25); ?></a></div>
                <!--div class="price"><?php echo $product['sale_price'] > 0 ? '<span>' . $this->price($product['price']) . '</span>' . $this->price($product['sale_price']) : $this->price($product['price']); ?></div-->
                <div>
                	<!--<a href="<?php echo url_for('/product/' . $product['id'] . '-' . slug($product['title'])); ?>"><button class="btn btn-sm btn-mearisse<?php /*echo $product['status'] == '8' ? ' sold-out' : '';*/ //byte2c ?>"><?php echo $product['status'] == '1' ? 'Add To Cart' : 'Sold Out!'; ?></button></a>-->
                	<a href="<?php echo url_for('/product/' . $product['id'] . '-' . slug($product['title'])); ?>"></a>
                </div>
            </div>
        <?php
	}
	

	
	public function register(){	
		option('title_prefix', 'Register | ');
		set('countries', $this->countries());
		set('states', $this->state(129));
		return render('register.php', 'layout/default.php');
	}
	
	public function forgot($token = ''){
		set('token', $token);
		option('title_prefix', 'Forgot Password | ');
		return render('forgot.php', 'layout/default.php');
	}
	
	public function activate($token = ''){
		if(!empty($token)){
			$data = tokenDecode($token);
			$data = @unserialize($data);

			if($data !== false){
				$this->db->query("SELECT * FROM members WHERE email = " . $this->db->escape($data['email'])." AND status = 0");
				$member = $this->db->getSingleRow();
				
				if(!empty($member)){
					$this->db->table('members');
					$this->db->updateArray(array(
						"status" => '1',
					));
					$this->db->whereArray(array(
						"email" => $data['email'],
						"status" => "0",
					));
					$this->db->update();	
		
					$ref = 'http://' . $_SERVER['HTTP_HOST'] . url_for('/');
					$link = $ref;
				
					$from = $this->settings()['email'];
					$to = $member['email'];
					$subject = SITE_NAME . ": Thank you for your registration!";
					$body = 
					'<html><body style="font-family:Segoe UI,Verdana,sans-serif; font-size:12px">
					<p><strong>Dear ' . $member['firstname'] . ' ' . $member['lastname'] . ',</strong></p>
					<p>You have successfully registered an account with ' . SITE_NAME .  '!</p>
					<p>Click <a href="' . $link . '">here</a> to start shopping with us! Delay no more!</p>
					<p>This is an auto-generated email. Please do not reply to this email./p>
					<p>Sincerely,<br>' . SITE_NAME . ' Team</p>
					</body></html>';
	
					$mime = new sendMail($from, $to, "", "", $subject);
					$mime->set('html', true);
					$mime->parseBody($body);
					$mime->setHeaders(); 
					$mime->send();
				}
				
				set('token', $token);
				set('member', $member);
				return render('activate.php', 'layout/default.php');
				
			}else{
				return $this->not_found();
			}
		}else{
			return $this->not_found();
		}
	}
	
	public function accountPage(){
		set('countries', $this->countries());
		set('states', $this->state($this->user->info['country']));
		return render('account.php', 'layout/default.php');
	}
	
	public function accountHistoryPage(){
		$this->db->query("SELECT * FROM shopping_checkout WHERE status IN (1,2) AND member_id = " . $this->db->escape($this->user->id) . " ORDER BY payment_date DESC");
		$checkout = $this->db->getRowList();
		
		if($checkout){
			foreach($checkout as $i => $keys){
				$this->db->query("SELECT * FROM shopping_cart WHERE id IN (" . $keys['carts'] . ") AND member_id = " . $this->user->id);
				$carts = $this->db->getRowList();
				if($carts){
					foreach($carts as $j => $cart){
						$this->db->query("SELECT * FROM products WHERE id = " . $this->db->escape($cart['item_id']));	
						$carts[$j]['product'] = $this->db->getSingleRow();
						$carts[$j]['product']['options'] = unserialize($carts[$j]['product']['options']);
					}
				}
				
				$checkout[$i]['carts'] = $carts;
				$checkout[$i]['tracking'] = $this->lastTracking($keys['id']);
			}
		}
		
		set('checkout', $checkout);
		return render('account-history.php', 'layout/default.php');
	}
	
	public function lastTracking($id){
		$this->db->query("SELECT * FROM tracking WHERE checkout_id = " . $this->db->escape($id) . " ORDER BY date DESC LIMIT 1");
		return $this->db->getSingleRow();
	}
	
	public function cart(){
		$holidayCharged = false;
		$hasPromo = false;
		$delivery_charges = array();
		$shipping_cost = 0;
		$total = 0;
		$checkout = '';
		
		if($this->user->logged) $this->db->query("SELECT * FROM shopping_cart WHERE status = '1' AND member_id = " . $this->db->escape($this->user->id) . " ORDER BY date");
		else $this->db->query("SELECT * FROM shopping_cart WHERE status = '1' AND guest_id = " . $this->db->escape($this->user->guest_id) . " ORDER BY date");
		$carts = $this->db->getRowList();
		
		if($carts){
			$cartIDs = array();
			foreach($carts as $i => $cart){
				array_push($cartIDs, $cart['id']);
				$this->db->query("SELECT * FROM products WHERE status = '1' AND id = " . $this->db->escape($cart['item_id']));	
				$carts[$i]['product'] = $this->db->getSingleRow();
				$carts[$i]['product']['options'] = unserialize($carts[$i]['product']['options']);
				$carts[$i]['product']['colors'] = unserialize($carts[$i]['product']['colors']);
				$total += $cart['price'] * $cart['quantity'];
				
				if(!empty($cart['promo_code'])){
					$this->db->query("SELECT * FROM promo_code WHERE code = " . $this->db->escape($cart['promo_code']));
					$promo = $this->db->getSingleRow();
					$hasPromo = $cart['promo_code'] . ' (' . ($promo['type'] == 'percent' ? $promo['value'] . '%' : '-' . $this->price($promo['value'])) . ' product discount)';
				}
			}
			
			$address = !empty($_SESSION[WEBSITE_PREFIX.'ADDRESS']) ? $_SESSION[WEBSITE_PREFIX.'ADDRESS'] : array();
			$shipping = !empty($_SESSION[WEBSITE_PREFIX.'SHIPPING']) ? $_SESSION[WEBSITE_PREFIX.'SHIPPING'] : array();
		}
		
		$address = !empty($_SESSION[WEBSITE_PREFIX.'ADDRESS']) ? $_SESSION[WEBSITE_PREFIX.'ADDRESS'] : array();
		$address['DELIVERY_METHOD'] = isset($address['DELIVERY_METHOD']) ? $address['DELIVERY_METHOD'] : 1;
		$address['DELIVERY']['salutation'] = !empty($address['DELIVERY']['salutation']) ? $address['DELIVERY']['salutation'] : '';
		$address['DELIVERY']['firstname'] = !empty($address['DELIVERY']['firstname']) ? $address['DELIVERY']['firstname'] : '';
		$address['DELIVERY']['lastname'] = !empty($address['DELIVERY']['lastname']) ? $address['DELIVERY']['lastname'] : '';
		$address['DELIVERY']['contact'] = !empty($address['DELIVERY']['contact']) ? $address['DELIVERY']['contact'] : '';
		$address['DELIVERY']['address1'] = !empty($address['DELIVERY']['address1']) ? $address['DELIVERY']['address1'] : '';
		$address['DELIVERY']['address2'] = !empty($address['DELIVERY']['address2']) ? $address['DELIVERY']['address2'] : '';
		$address['DELIVERY']['city'] = !empty($address['DELIVERY']['city']) ? $address['DELIVERY']['city'] : '';
		$address['DELIVERY']['zip'] = !empty($address['DELIVERY']['zip']) ? $address['DELIVERY']['zip'] : '';
		$address['DELIVERY']['country'] = !empty($address['DELIVERY']['country']) ? $address['DELIVERY']['country'] : '';
		$address['DELIVERY']['state'] = !empty($address['DELIVERY']['state']) ? $address['DELIVERY']['state'] : '';
		$address['BILLING']['salutation'] = !empty($address['BILLING']['salutation']) ? $address['BILLING']['salutation'] : '';
		$address['BILLING']['firstname'] = !empty($address['BILLING']['firstname']) ? $address['BILLING']['firstname'] : '';
		$address['BILLING']['lastname'] = !empty($address['BILLING']['lastname']) ? $address['BILLING']['lastname'] : '';
		$address['BILLING']['contact'] = !empty($address['BILLING']['contact']) ? $address['BILLING']['contact'] : '';
		$address['BILLING']['address1'] = !empty($address['BILLING']['address1']) ? $address['BILLING']['address1'] : '';
		$address['BILLING']['address2'] = !empty($address['BILLING']['address2']) ? $address['BILLING']['address2'] : '';
		$address['BILLING']['city'] = !empty($address['BILLING']['city']) ? $address['BILLING']['city'] : '';
		$address['BILLING']['zip'] = !empty($address['BILLING']['zip']) ? $address['BILLING']['zip'] : '';
		$address['BILLING']['country'] = !empty($address['BILLING']['country']) ? $address['BILLING']['country'] : '';
		$address['BILLING']['state'] = !empty($address['BILLING']['state']) ? $address['BILLING']['state'] : '';
		
		$shipping = !empty($_SESSION[WEBSITE_PREFIX.'SHIPPING']) ? $_SESSION[WEBSITE_PREFIX.'SHIPPING'] : array();
		$shipping['zone'] = !empty($shipping['zone']) ? $shipping['zone'] : '0';
		$shipping['date'] = !empty($shipping['date']) ? $shipping['date'] : '';
		$shipping['time'] = !empty($shipping['time']) ? $shipping['time'] : '';
		$shipping['email'] = !empty($shipping['email']) ? $shipping['email'] : $this->user->info['email'];
		$shipping['comments'] = !empty($shipping['comments']) ? $shipping['comments'] : '';
				
		$delivery_method = isset($_SESSION[WEBSITE_PREFIX.'DELIVERY_METHOD']) ? $_SESSION[WEBSITE_PREFIX.'DELIVERY_METHOD'] : 1;
		$shipping_methods = $this->shipping_method();
				
		if(!empty($address['DELIVERY_METHOD']) && $address['DELIVERY_METHOD'] == '1'){
			
			if(in_array($address['DELIVERY']['state'], array('1981','1982'))) $delivery_charges[] = array('Sabah & Sarawak', '3');
			else $delivery_charges[] = array('Free Shipping', '0');
			
			/*
			$delivery_charges[] = array($shipping_methods[0] . ' (' . $this->price(50) . ')', '50');
			if(!empty($shipping['zone'])) $delivery_charges[] = array($shipping_methods[$shipping['zone']] . ' (+' . $this->price(20) . ')', '20');
			
			if(date('N', strtotime($shipping['date'])) == 7){
				$delivery_charges[] = array('Weekend' . ' (+' . $this->price(15) . ')', '15');
				$holidayCharged = true;
			}
			
			$holidays = $this->holidays();
			if($holidays){
				foreach($holidays as $holiday){								
					$date_start = strtotime($holiday['date_start']);
					$date_end = strtotime($holiday['date_end']);
					$date = strtotime($shipping['date']);
					if($date_start <= $date && $date_end >= $date) {
						if($holidayCharged == false){ 
							$delivery_charges[] = array($holiday['title'] . ' (+' . $this->price(15) . ')', '15');
							$holidayCharged = true;
						}else{
							$delivery_charges[] = array($holiday['title'], '0');
						}
					}
				}
			}
			*/
			
		}else{
			$delivery_charges[] = array('Self-Collection', '0');
		}
		
		if($delivery_charges){
			foreach($delivery_charges as $delivery_charge){
				$shipping_cost += $delivery_charge[1];
			}
		}
		
		if($total < 0) $total = 0;
		$subtotal = $total + $shipping_cost;
		
		if($carts){
			if($this->user->logged){
				$this->db->query("SELECT * FROM shopping_checkout WHERE status = '0' AND member_id = " . $this->db->escape($this->user->id));
				$checkout = $this->db->getSingleRow();
				
				if(!$checkout){
					$tracking_no = 'NIC' . strtoupper(substr(md5(uniqid(rand(1,6))), 0, 9));
					$cartIDs = array();
					
					$this->db->table('shopping_checkout');
					$this->db->insertArray(array(
						"carts" => implode(',', $cartIDs),
						"member_id" => $this->user->id,
						"total" => $total,
						"comment" => $shipping['comments'],
						"shipping" => $shipping_cost,
						"shipping_method" => $address['DELIVERY_METHOD'],
						//"shipping_date" => $carts['SHIPPING']['date'],
						//"shipping_time" => $carts['SHIPPING']['time'],
						"delivery_salutation" => $address['DELIVERY']['salutation'],
						"delivery_firstname" => $address['DELIVERY']['firstname'],
						"delivery_lastname" => $address['DELIVERY']['lastname'],
						"delivery_address1" => $address['DELIVERY']['address1'],
						"delivery_address2" => $address['DELIVERY']['address2'],
						"delivery_city" => $address['DELIVERY']['city'],
						"delivery_zip" => $address['DELIVERY']['zip'],
						"delivery_country" => $address['DELIVERY']['country'],
						"delivery_state" => $address['DELIVERY']['state'],
						"delivery_contact" => $address['DELIVERY']['contact'],
						"billing_salutation" =>$address['BILLING']['salutation'],
						"billing_firstname" =>$address['BILLING']['firstname'],
						"billing_lastname" => $address['BILLING']['lastname'],
						"billing_address1" => $address['BILLING']['address1'],
						"billing_address2" => $address['BILLING']['address2'],
						"billing_city" => $address['BILLING']['city'],
						"billing_zip" => $address['BILLING']['zip'],
						"billing_country" => $address['BILLING']['country'],
						"billing_state" => $address['BILLING']['state'],
						"billing_contact" => $address['BILLING']['contact'],
						"status" => '0',
						"tracking_no" => $tracking_no,
					));
					$this->db->insert();
					
					$this->db->query("SELECT * FROM shopping_checkout WHERE status = '0' AND member_id = " . $this->db->escape($this->user->id));
					$checkout = $this->db->getSingleRow();
				
				}
				
			}else{
				
				$this->db->query("SELECT * FROM shopping_checkout WHERE status = '0' AND guest_id = " . $this->db->escape($this->user->guest_id));
				$checkout = $this->db->getSingleRow();
				
				if(!$checkout){
					$tracking_no = 'MIA' . strtoupper(substr(md5(uniqid(rand(1,6))), 0, 9));
					$cartIDs = array();
					
					$this->db->table('shopping_checkout');
					$this->db->insertArray(array(
						"carts" => implode(',', $cartIDs),
						"guest_id" => $this->user->guest_id,
						"total" => $total,
						"comment" => $shipping['comments'],
						"shipping" => $shipping_cost,
						"shipping_method" => $address['DELIVERY_METHOD'],
						//"shipping_date" => $carts['SHIPPING']['date'],
						//"shipping_time" => $carts['SHIPPING']['time'],
						"delivery_salutation" => $address['DELIVERY']['salutation'],
						"delivery_firstname" => $address['DELIVERY']['firstname'],
						"delivery_lastname" => $address['DELIVERY']['lastname'],
						"delivery_address1" => $address['DELIVERY']['address1'],
						"delivery_address2" => $address['DELIVERY']['address2'],
						"delivery_city" => $address['DELIVERY']['city'],
						"delivery_zip" => $address['DELIVERY']['zip'],
						"delivery_country" => $address['DELIVERY']['country'],
						"delivery_state" => $address['DELIVERY']['state'],
						"delivery_contact" => $address['DELIVERY']['contact'],
						"billing_salutation" =>$address['BILLING']['salutation'],
						"billing_firstname" =>$address['BILLING']['firstname'],
						"billing_lastname" => $address['BILLING']['lastname'],
						"billing_address1" => $address['BILLING']['address1'],
						"billing_address2" => $address['BILLING']['address2'],
						"billing_city" => $address['BILLING']['city'],
						"billing_zip" => $address['BILLING']['zip'],
						"billing_country" => $address['BILLING']['country'],
						"billing_state" => $address['BILLING']['state'],
						"billing_contact" => $address['BILLING']['contact'],
						"status" => '0',
						"tracking_no" => $tracking_no,
					));
					$this->db->insert();
					
					$this->db->query("SELECT * FROM shopping_checkout WHERE status = '0' AND guest_id = " . $this->db->escape($this->user->guest_id));
					$checkout = $this->db->getSingleRow();
				
				}
			}
		}
		
		$return['items'] = $carts;
		$return['delivery_charges'] = $delivery_charges;
		$return['shipping_cost'] = $shipping_cost;
		$return['total'] = $total;
		$return['subtotal'] = $subtotal;
		$return['ADDRESS'] = $address;
		$return['SHIPPING'] = $shipping;
		$return['DELIVERY_METHOD'] = $delivery_method;
		$return['hasPromo'] = $hasPromo;
		$return['checkout'] = $checkout;
								
		return $return;
	}
	
	public function cartTotal($id){
		if($this->user->logged) $total = $this->db->getValue("SELECT SUM(quantity) FROM shopping_cart WHERE status = '1' AND member_id = " . $this->db->escape($this->user->id));
		else $total = $this->db->getValue("SELECT SUM(quantity) FROM shopping_cart WHERE status = '1' AND guest_id = " . $this->db->escape($this->user->guest_id));
		return $total ? $total : 0;
	}
	
	public function cartPage(){
		set('carts', $this->cart());
		return render('cart.php', 'layout/default.php');
	}
	
	public function cartPageAddress(){
		$myaddress['salutation'] = $this->user->info['salutation'];
		$myaddress['firstname'] = $this->user->info['firstname'];
		$myaddress['lastname'] = $this->user->info['lastname'];
		$myaddress['contact'] = $this->user->info['contact'];
		$myaddress['address1'] = $this->user->info['address1'];
		$myaddress['address2'] = $this->user->info['address2'];
		$myaddress['city'] = $this->user->info['city'];
		$myaddress['zip'] = $this->user->info['zip'];
		$myaddress['country'] = $this->user->info['country'];
		$myaddress['state'] = $this->user->info['state'];
				
		$address = !empty($_SESSION[WEBSITE_PREFIX.'ADDRESS']) ? $_SESSION[WEBSITE_PREFIX.'ADDRESS'] : '';
		$address['DELIVERY_METHOD'] = isset($address['DELIVERY_METHOD']) ? $address['DELIVERY_METHOD'] : 1;
		$address['DELIVERY']['salutation'] = !empty($address['DELIVERY']['salutation']) ? $address['DELIVERY']['salutation'] : '';
		$address['DELIVERY']['firstname'] = !empty($address['DELIVERY']['firstname']) ? $address['DELIVERY']['firstname'] : '';
		$address['DELIVERY']['lastname'] = !empty($address['DELIVERY']['lastname']) ? $address['DELIVERY']['lastname'] : '';
		$address['DELIVERY']['contact'] = !empty($address['DELIVERY']['contact']) ? $address['DELIVERY']['contact'] : '';
		$address['DELIVERY']['address1'] = !empty($address['DELIVERY']['address1']) ? $address['DELIVERY']['address1'] : '';
		$address['DELIVERY']['address2'] = !empty($address['DELIVERY']['address2']) ? $address['DELIVERY']['address2'] : '';
		$address['DELIVERY']['city'] = !empty($address['DELIVERY']['city']) ? $address['DELIVERY']['city'] : '';
		$address['DELIVERY']['zip'] = !empty($address['DELIVERY']['zip']) ? $address['DELIVERY']['zip'] : '';
		$address['DELIVERY']['country'] = !empty($address['DELIVERY']['country']) ? $address['DELIVERY']['country'] : '';
		$address['DELIVERY']['state'] = !empty($address['DELIVERY']['state']) ? $address['DELIVERY']['state'] : '';
		$address['BILLING']['salutation'] = !empty($address['BILLING']['salutation']) ? $address['BILLING']['salutation'] : '';
		$address['BILLING']['firstname'] = !empty($address['BILLING']['firstname']) ? $address['BILLING']['firstname'] : '';
		$address['BILLING']['lastname'] = !empty($address['BILLING']['lastname']) ? $address['BILLING']['lastname'] : '';
		$address['BILLING']['contact'] = !empty($address['BILLING']['contact']) ? $address['BILLING']['contact'] : '';
		$address['BILLING']['address1'] = !empty($address['BILLING']['address1']) ? $address['BILLING']['address1'] : '';
		$address['BILLING']['address2'] = !empty($address['BILLING']['address2']) ? $address['BILLING']['address2'] : '';
		$address['BILLING']['city'] = !empty($address['BILLING']['city']) ? $address['BILLING']['city'] : '';
		$address['BILLING']['zip'] = !empty($address['BILLING']['zip']) ? $address['BILLING']['zip'] : '';
		$address['BILLING']['country'] = !empty($address['BILLING']['country']) ? $address['BILLING']['country'] : '';
		$address['BILLING']['state'] = !empty($address['BILLING']['state']) ? $address['BILLING']['state'] : '';
		
		$_SESSION[WEBSITE_PREFIX.'ADDRESS'] = $address;
		
		$states = array();
		if(!empty($address['DELIVERY']['country'])) $states['DELIVERY'] = $this->state($address['DELIVERY']['country']);
		else $states['DELIVERY'] = $this->state(129);
		if(!empty($address['BILLING']['country'])) $states['BILLING'] = $this->state($address['BILLING']['country']);
		else $states['BILLING'] = $this->state(129);
						
		set('myaddress', json_encode($myaddress));
		set('address', $address);
		set('countries', $this->countries());
		set('states', $states);
		return render('cart-address.php', 'layout/default.php');
	}
	
	public function cartPageDelivery(){
		$shipping = !empty($_SESSION[WEBSITE_PREFIX.'SHIPPING']) ? $_SESSION[WEBSITE_PREFIX.'SHIPPING'] : '';
		$shipping['zone'] = !empty($shipping['zone']) ? $shipping['zone'] : '0';
		$shipping['date'] = !empty($shipping['date']) ? $shipping['date'] : '';
		$shipping['time'] = !empty($shipping['time']) ? $shipping['time'] : '';
		$shipping['email'] = !empty($shipping['email']) ? $shipping['email'] : $this->user->info['email'];
		$shipping['comments'] = !empty($shipping['comments']) ? $shipping['comments'] : '';
		$_SESSION[WEBSITE_PREFIX.'SHIPPING'] = $shipping;
		
		set('carts', $this->cart());
		set('shipping', $shipping);
		return render('cart-delivery.php', 'layout/default.php');
	}
	
	public function cartPagePayment(){
		$carts = $this->cart();
		
		if($carts){
			$cartIDs = array();
		
			foreach($carts['items'] as $i => $keys){
				array_push($cartIDs, $keys['id']);
			}
			
			if($this->user->logged){
				$this->db->table('shopping_checkout');
				$this->db->updateArray(array(
					"carts" => implode(',', $cartIDs),
					"total" => $carts['total'],
					"email" => $carts['SHIPPING']['email'],
					"comment" => $carts['SHIPPING']['comments'],
					"shipping" => $carts['shipping_cost'],
					"shipping_method" => $carts['ADDRESS']['DELIVERY_METHOD'],
					//"shipping_date" => $carts['SHIPPING']['date'],
					//"shipping_time" => $carts['SHIPPING']['time'],
					"delivery_salutation" => $carts['ADDRESS']['DELIVERY']['salutation'],
					"delivery_firstname" => $carts['ADDRESS']['DELIVERY']['firstname'],
					"delivery_lastname" => $carts['ADDRESS']['DELIVERY']['lastname'],
					"delivery_address1" => $carts['ADDRESS']['DELIVERY']['address1'],
					"delivery_address2" => $carts['ADDRESS']['DELIVERY']['address2'],
					"delivery_city" => $carts['ADDRESS']['DELIVERY']['city'],
					"delivery_zip" => $carts['ADDRESS']['DELIVERY']['zip'],
					"delivery_country" => $carts['ADDRESS']['DELIVERY']['country'],
					"delivery_state" => $carts['ADDRESS']['DELIVERY']['state'],
					"delivery_contact" => $carts['ADDRESS']['DELIVERY']['contact'],
					"billing_salutation" =>$carts['ADDRESS']['BILLING']['salutation'],
					"billing_firstname" =>$carts['ADDRESS']['BILLING']['firstname'],
					"billing_lastname" => $carts['ADDRESS']['BILLING']['lastname'],
					"billing_address1" => $carts['ADDRESS']['BILLING']['address1'],
					"billing_address2" => $carts['ADDRESS']['BILLING']['address2'],
					"billing_city" => $carts['ADDRESS']['BILLING']['city'],
					"billing_zip" => $carts['ADDRESS']['BILLING']['zip'],
					"billing_country" => $carts['ADDRESS']['BILLING']['country'],
					"billing_state" => $carts['ADDRESS']['BILLING']['state'],
					"billing_contact" => $carts['ADDRESS']['BILLING']['contact'],
				));
				$this->db->whereArray(array(
					"member_id" => $this->user->id,
					"status" => '0',
				));
				$this->db->update();
				
			}else{
				
			}$this->db->table('shopping_checkout');
				$this->db->updateArray(array(
					"carts" => implode(',', $cartIDs),
					"total" => $carts['total'],
					"email" => $carts['SHIPPING']['email'],
					"comment" => $carts['SHIPPING']['comments'],
					"shipping" => $carts['shipping_cost'],
					"shipping_method" => $carts['ADDRESS']['DELIVERY_METHOD'],
					//"shipping_date" => $carts['SHIPPING']['date'],
					//"shipping_time" => $carts['SHIPPING']['time'],
					"delivery_salutation" => $carts['ADDRESS']['DELIVERY']['salutation'],
					"delivery_firstname" => $carts['ADDRESS']['DELIVERY']['firstname'],
					"delivery_lastname" => $carts['ADDRESS']['DELIVERY']['lastname'],
					"delivery_address1" => $carts['ADDRESS']['DELIVERY']['address1'],
					"delivery_address2" => $carts['ADDRESS']['DELIVERY']['address2'],
					"delivery_city" => $carts['ADDRESS']['DELIVERY']['city'],
					"delivery_zip" => $carts['ADDRESS']['DELIVERY']['zip'],
					"delivery_country" => $carts['ADDRESS']['DELIVERY']['country'],
					"delivery_state" => $carts['ADDRESS']['DELIVERY']['state'],
					"delivery_contact" => $carts['ADDRESS']['DELIVERY']['contact'],
					"billing_salutation" =>$carts['ADDRESS']['BILLING']['salutation'],
					"billing_firstname" =>$carts['ADDRESS']['BILLING']['firstname'],
					"billing_lastname" => $carts['ADDRESS']['BILLING']['lastname'],
					"billing_address1" => $carts['ADDRESS']['BILLING']['address1'],
					"billing_address2" => $carts['ADDRESS']['BILLING']['address2'],
					"billing_city" => $carts['ADDRESS']['BILLING']['city'],
					"billing_zip" => $carts['ADDRESS']['BILLING']['zip'],
					"billing_country" => $carts['ADDRESS']['BILLING']['country'],
					"billing_state" => $carts['ADDRESS']['BILLING']['state'],
					"billing_contact" => $carts['ADDRESS']['BILLING']['contact'],
				));
				$this->db->whereArray(array(
					"guest_id" => $this->user->guest_id,
					"status" => '0',
				));
				$this->db->update();
		}
		
		set('carts', $carts);
		return render('cart-payment.php', 'layout/default.php');
	}
	
	public function cartPageReturn(){
		$carts = $this->cart();
		$token = !empty($_REQUEST['token']) ? $_REQUEST['token'] : '';
		$payerID = !empty($_REQUEST['PayerID']) ? $_REQUEST['PayerID'] : '';
		$carts = $this->cart();
		$success = false;
		$pp_error = '';
		$redirect = '<meta http-equiv="refresh" content="0; URL=' . url_for('/cart/return/failed') . '">';
		
		if($carts){		
			if(!empty($token) && !empty($payerID) && !empty($_SESSION[WEBSITE_PREFIX.'PAYPALNVP'])){
				$PAYPALNVP['user'] = $_SESSION[WEBSITE_PREFIX.'PAYPALNVP']['user'];
				$PAYPALNVP['pwd'] = $_SESSION[WEBSITE_PREFIX.'PAYPALNVP']['pwd'];
				$PAYPALNVP['signature'] = $_SESSION[WEBSITE_PREFIX.'PAYPALNVP']['signature'];
				$PAYPALNVP['version'] = $_SESSION[WEBSITE_PREFIX.'PAYPALNVP']['version'];
				$PAYPALNVP['method'] = "GetExpressCheckoutDetails";
				$PAYPALNVP['TOKEN'] = $token;	
				$nvp2 = array();
				$nvp3 = array();
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $_SESSION[WEBSITE_PREFIX.'PAYPALNVP']['NVP_URL']);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($PAYPALNVP, '', '&'));
				$chresult = curl_exec($ch);
				curl_close($ch);
				parse_str($chresult, $nvp2);
				
				$_SESSION[WEBSITE_PREFIX.'PAYPALNVP2'] = $nvp2;
				if(!empty($nvp2['L_LONGMESSAGE0'])) $pp_error = $nvp2['L_LONGMESSAGE0'];
				
				//$nvp2['ACK'] = 'success';
				
				if($this->user->logged){
					$this->db->table('shopping_checkout');
					$this->db->updateArray(array(
						"payment_method" => $_SESSION[WEBSITE_PREFIX.'PAYMENT']['method'],
						"pp_token" => $token,
						"pp_payer_id" => $payerID,
						"pp_GetExpressCheckoutDetails" => serialize($nvp2),
						"payment_date" => 'NOW()',
					));
					$this->db->whereArray(array(
						"member_id" => $this->user->id,
						"status" => '0',
					));
					$this->db->update();
									
				}else{
					$this->db->table('shopping_checkout');
					$this->db->updateArray(array(
						"payment_method" => $_SESSION[WEBSITE_PREFIX.'PAYMENT']['method'],
						"pp_token" => $token,
						"pp_payer_id" => $payerID,
						"pp_GetExpressCheckoutDetails" => serialize($nvp2),
						"payment_date" => 'NOW()',
					));
					$this->db->whereArray(array(
						"guest_id" => $this->user->guest_id,
						"status" => '0',
					));
					$this->db->update();
				}
				
				if(strtolower($nvp2['ACK']) == 'success' || strtolower($nvp2['ACK']) == 'successwithwarning'){
					$PAYPALCONFIRM['user'] = $_SESSION[WEBSITE_PREFIX.'PAYPALNVP']['user'];
					$PAYPALCONFIRM['pwd'] = $_SESSION[WEBSITE_PREFIX.'PAYPALNVP']['pwd'];
					$PAYPALCONFIRM['signature'] = $_SESSION[WEBSITE_PREFIX.'PAYPALNVP']['signature'];
					$PAYPALCONFIRM['method'] = "DoExpressCheckoutPayment";
					$PAYPALCONFIRM['version'] = $_SESSION[WEBSITE_PREFIX.'PAYPALNVP']['version'];
					$PAYPALCONFIRM['TOKEN'] = $token;
					$PAYPALCONFIRM['PAYERID'] = $payerID;
					$PAYPALCONFIRM['PAYMENTREQUEST_0_PAYMENTACTION'] = $_SESSION[WEBSITE_PREFIX.'PAYPALNVP']['PAYMENTREQUEST_0_PAYMENTACTION'];
					$PAYPALCONFIRM['PAYMENTREQUEST_0_AMT'] = $_SESSION[WEBSITE_PREFIX.'PAYPALNVP']['PAYMENTREQUEST_0_AMT'];
					$PAYPALCONFIRM['PAYMENTREQUEST_0_CURRENCYCODE'] = $_SESSION[WEBSITE_PREFIX.'PAYPALNVP']['PAYMENTREQUEST_0_CURRENCYCODE'];
					
					$PAYPALCONFIRMIRL = $_SESSION[WEBSITE_PREFIX.'PAYPALNVP']['NVP_URL'] . '?' . http_build_query($PAYPALCONFIRM, '', '&');
					
					$ch2 = curl_init();
					curl_setopt($ch2, CURLOPT_URL, $PAYPALCONFIRMIRL);
					curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, false);
					$ch2result = curl_exec($ch2);
					curl_close($ch2);
					parse_str($ch2result, $nvp3);
					
					if(!empty($nvp3['L_LONGMESSAGE0'])) $pp_error = $nvp3['L_LONGMESSAGE0'];
										
					$_SESSION[WEBSITE_PREFIX.'PAYPALNVP3'] = $nvp3;
					
					//$nvp3['ACK'] = 'success';
					
					if($this->user->logged){
						$this->db->table('shopping_checkout');
						$this->db->updateArray(array(
							"pp_DoExpressCheckoutPayment" => serialize($nvp3),
							"payment_date" => 'NOW()',
							"status" => '2',
						));
						$this->db->whereArray(array(
							"member_id" => $this->user->id,
							"status" => '0',
						));
						$this->db->update();
					
					}else{
						$this->db->table('shopping_checkout');
						$this->db->updateArray(array(
							"pp_DoExpressCheckoutPayment" => serialize($nvp3),
							"payment_date" => 'NOW()',
							"status" => '2',
						));
						$this->db->whereArray(array(
							"guest_id" => $this->user->guest_id,
							"status" => '0',
						));
						$this->db->update();
					}
										
					if(strtolower($nvp3['ACK']) == 'success' || strtolower($nvp3['ACK']) == 'successwithwarning'){
						$success = true;
						$redirect = '<meta http-equiv="refresh" content="0; URL=' . url_for('/cart/return/success?token=' . $token . '&PayerID=' . $payerID . '') . '">';
					}
				}
			}
		}
		
		if($success){
			echo '<meta http-equiv="refresh" content="0; URL=' . url_for('/cart/return/success&token=' . $token . '&PayerID=' . $payerID . '') . '">';
		}else{
			set('pp_error', $pp_error);
			return render('cart-return-failed.php', 'layout/default.php');
		}
	}
	
	public function cartPageReturnSuccess(){
		set('db', $this->db);
		set('carts', $this->cart());
		return render('cart-return-success.php', 'layout/default.php');
	}
	
	public function cartPageReturnFailed(){
		set('carts', $this->cart());
		return render('cart-return-failed.php', 'layout/default.php');
	}
	
	public function cartReminderEmail(){
		$this->db->query("SELECT * FROM shopping_cart WHERE status = '1' AND notified = '0' AND date < DATE_SUB(NOW(), INTERVAL 2 DAY) GROUP BY member_id");	
		$carts = $this->db->getRowList();
		
		if($carts){
			foreach($carts as $cart){
				$this->db->query("SELECT * FROM members WHERE id = " . $this->db->escape($cart['member_id']));
				$member = $this->db->getSingleRow();
				
				$ref = 'http://' . $_SERVER['HTTP_HOST'] . url_for('/cart');
				$link = $ref;
						
				$from = $this->settings()['email'];
				$to = $member['email'];
				$subject = SITE_NAME . " Shopping Cart Reminder";
				$body = 
				'<html><body style="font-family:Segoe UI,Verdana,sans-serif; font-size:12px">
				<p><strong>Dear ' . $member['firstname'] . ' ' . $member['lastname'] . ',</strong></p>
				<p>We noticed that you still have items in your shopping cart. Click <a href="' . $link . '">here</a> to complete your order.</p>
				<p>This is an auto-generated email. Please do not reply to this email./p>
				<p>Sincerely,<br>' . SITE_NAME . ' Team</p>
				</body></html>';
			
				$mime = new sendMail($from, $to, "", "", $subject);
				$mime->set('html', true);
				$mime->parseBody($body);
				$mime->setHeaders(); 
				$mime->send();
				
				$this->db->table("shopping_cart");
				$this->db->updateArray(array(
					"notified" => '1',
				));
				$this->db->whereArray(array(
					"member_id" => $cart['member_id'],
				));
				$this->db->update();
			}
		}
	}
	
	public function contactForm(){
		$settings = $this->settings();
		?>
        	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="user-scalable=no, width=device-width" />
            <meta name="description" content="<?php echo $settings['meta_description']; ?>">
            <meta name="keywords" content="<?php echo $settings['meta_keyword']; ?>">
            <title><?php echo option('title_prefix') . SITE_NAME;?></title>
			<link rel="stylesheet" href="<?php echo url_for('/assets/css'); ?>/style.css" />
            <script type="text/javascript" src="<?php echo url_for('/assets/libs/jquery/jquery-2.1.0.min.js'); ?>"></script>
			<script type="text/javascript" src="<?php echo url_for('/assets/libs/jquery/jquery-migrate-1.2.1.min.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo url_for('/assets/libs/jquery/jquery-ui.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo url_for('/assets/libs/bootstrap/js/bootstrap.min.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo url_for('/assets/libs/notifyjs/notify.min.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo url_for('/assets/libs/notifyjs/metro/notify-metro.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo url_for('/assets/js'); ?>/script.js"></script>
            <script>var rootPath = '<?php echo option('base_uri'); ?>';</script>
            </head>
            
            <body>
            <h2>Enquiry / Feedback</h2>
            <div class="row">
            	<div class="col-md-7">
                	<form id="contactForm" class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-3 control-label">First Name*</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="firstname" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Last Name*</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="lastname" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Email*</label>
                        <div class="col-sm-9">
                          <input type="email" class="form-control" name="email" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Contact Number*</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="contact" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Enquiry*</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="enquiry" value="General Enquiry" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Message*</label>
                        <div class="col-sm-9">
                          <textarea class="form-control" name="message" required></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9 r">
                          <input type="submit" class="btn btn-primary" value="Submit">
                        </div>
                      </div>
                    </form>
                </div>
            </div>
            </body>
			</html>
        <?php
	}
		
	public function checkProductExistUnderCat($catID){
		$hasProduct = 0;
		$ids = array($catID);
		$this->db->query("SELECT * FROM category WHERE status = '1' AND id = " . $this->db->escape($catID));	
		$cat = $this->db->getSingleRow();
		
		if($cat){
			$this->db->query("SELECT * FROM category WHERE status = '1' AND parent = " . $this->db->escape($cat['id']));	
			$cat2 = $this->db->getRowList();
			
			if($cat2){
				foreach($cat2 as $cat2){
					array_push($ids, $cat2['id']);
				}
				
				while($cat2){
					$this->db->query("SELECT * FROM category WHERE status = '1' AND parent = " . $this->db->escape($cat2['id']));	
					$cat2 = $this->db->getRowList();
					
					if($cat2){
						foreach($cat2 as $cat2){
							array_push($ids, $cat2['id']);
						}
					}
				}
			}
		}
		
		if($ids){
			foreach($ids as $id){
				$this->db->query("SELECT * FROM products WHERE status IN (1,8) AND (categories = '" . $id . "' OR categories LIKE '%," . $id . "' OR categories LIKE '" . $id . ",%' OR categories LIKE '%," . $id . ",%')");	
				$products = $this->db->getRowList();
				
				if($products){
					foreach($products as $product){
						$hasProduct++;
					}
				}
			}
		}
								
		return $hasProduct;
	}
	
	//  Get IP Address
    public function get_country_id() {
        $ipaddress = '';
        $countryCode = "";
        $country = 132;
		
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
		
		//$ipaddress = $_SERVER['REMOTE_ADDR'];

        $ipDetails = json_decode(file_get_contents("http://ipinfo.io/{$ipaddress}/json"));
        $countryCode = empty($ipDetails->country) ? "" : $ipDetails->country;
      
        if($countryCode){
			$this->db->query("SELECT `id` FROM `countries` WHERE `iso2` = " .  $this->db->escape($countryCode) . " LIMIT 1");
            $country_id = $this->db->getValue();    
            if($country_id) $country = $country_id;
        }

        return $country;
    }

	public function sign_up(){
		if($this->request('redirect')){
			if( base64_decode($this->request('redirect'), true) )
				$redirect = base64_decode($this->request('redirect'));
			else
				$redirect = urldecode($this->request('redirect'));
		}elseif( isset($_SESSION[WEBSITE_PREFIX . 'REDIRECT']) ){
			if(empty($_SESSION[WEBSITE_PREFIX.'REDIRECT'])){
				if(!empty($_SERVER['REDIRECT_URL'])){
					if(!preg_match('/\/login/', $_SERVER['REDIRECT_URL'])){
						$redirect = preg_match('/http/', $_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : $this->site_host . $_SERVER['REDIRECT_URL'];
					}else{
						$redirect = $this->site_host . ROOTPATH . 'dashboard';
					}
				}
			}else{
                if( base64_decode($_SESSION[WEBSITE_PREFIX . 'REDIRECT'], true) ) {
                    $redirect = base64_decode($_SESSION[WEBSITE_PREFIX . 'REDIRECT']);
                }else{
                    $redirect = $_SESSION[WEBSITE_PREFIX.'REDIRECT'];
                }
			}
		}

        if( isset($redirect) && base64_decode($redirect, true) ) {
            $_SESSION[WEBSITE_PREFIX . 'REDIRECT'] = base64_decode($redirect);
        }elseif( isset($redirect) ){
            $_SESSION[WEBSITE_PREFIX . 'REDIRECT'] = $redirect;
        }else{
            $redirect = $this->site_host . ROOTPATH . 'dashboard';
        }

        if( isset($_SESSION[WEBSITE_PREFIX.'REDIRECT']) ) {
            $parsed_url = parse_url($_SESSION[WEBSITE_PREFIX . 'REDIRECT']);
            if (isset($parsed_url['query']) && !empty($parsed_url['query'])) {
                $query = $parsed_url['query'];
                parse_str($query, $query_vars);
                if (array_key_exists('search_name', $query_vars)) {
                    $_SESSION[WEBSITE_PREFIX . 'SAVE_SEARCH'] = $query_vars;
                    $redirect                                 = option('site_uri') . url_for('/dashboard');
                } elseif (array_key_exists('id', $query_vars)) {
                    $_SESSION[WEBSITE_PREFIX . 'FAV'] = $query_vars;
                    $redirect                         = $parsed_url['path'];
                }
            }
        }

		$_SESSION[WEBSITE_PREFIX . 'REDIRECT'] = $redirect;
		
		if($this->user->logged){
			header('Location: ' . $redirect);
			exit();
		}
		$country_id = $this->get_country_id();
		set('redirect', $redirect);
		set("countries", $this->countries());
		set("countryId", $country_id);
		set("states", $this->states($country_id));
		set('title', 'Sign Up');
		return render('sign_up/sign_up.php', 'layout/default.php');
	}

	public function sign_up_success(){

        if ( request_method() !== 'POST' || !lemon_csrf_require_valid_token('invalid token', lemon_csrf_token('signup_token')) ) {
        	// redirect to sigup page
            redirect_to(option('site_uri') . url_for('/sign_up'));
        }

        set('style', 'white-content page-sign-up');
        return render('sign_up/sign_up_success.php', 'layout/default.php');

    }

	public function home(){
        $this->parentCategories = $this->gettopParents();
        $this->topJob = $this->gettopJobs();
        $this->getTask = $this->getTaskPublic();
        $this->getHomeTask = $this->getHomeTaskPublic();
        $this->getOnSiteTask = $this->getOnSiteTaskPublic();
        $this->getRecentTask = $this->getRecentTaskPublic();
        $this->getNearestTask = $this->getNearestTaskPublic();

		$this->db->query("SELECT * FROM `subscription` WHERE status = '1'");
		$subscriptions = $this->db->getRowList();

		if(!empty($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) && $_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] != 'EN'){
			$language_id = $this->db->getValue("SELECT id FROM `language` WHERE status = '1' AND code = " . $this->db->escape($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']));
		}

		if(!empty($language_id)){
			if($subscriptions){
				foreach($subscriptions as $i => $subscription){
					$title = $this->db->getValue("SELECT title FROM `subscription_language_text` WHERE language_id = " . $this->db->escape($language_id) . " AND subscription_id = " . $this->db->escape($subscription['id']));
					$description = $this->db->getValue("SELECT description FROM `subscription_language_text` WHERE language_id = " . $this->db->escape($language_id) . " AND subscription_id = " . $this->db->escape($subscription['id']));
					$title_2 = $this->db->getValue("SELECT title_2 FROM `subscription_language_text` WHERE language_id = " . $this->db->escape($language_id) . " AND subscription_id = " . $this->db->escape($subscription['id']));
					$description_2 = $this->db->getValue("SELECT description_2 FROM `subscription_language_text` WHERE language_id = " . $this->db->escape($language_id) . " AND subscription_id = " . $this->db->escape($subscription['id']));
					$features_2 = $this->db->getValue("SELECT features_2 FROM `subscription_language_text` WHERE language_id = " . $this->db->escape($language_id) . " AND subscription_id = " . $this->db->escape($subscription['id']));

					if($title) $subscriptions[$i]['title'] = $title;
					if($description) $subscriptions[$i]['description'] = $description;
					if($title_2) $subscriptions[$i]['title_2'] = $title_2;
					if($description_2) $subscriptions[$i]['description_2'] = $description_2;
					if($features_2) $subscriptions[$i]['features_2'] = $features_2;
				}
			}
		}

		set('subscriptions', $subscriptions);
		return render('home.php', NULL);
	}
	public function about(){
		return render('about-us.php', NULL);
	}
	public function faq(){
		return render('faq.php', NULL);
	}

	// editable for live
	public function total_external_jobs()
    {
        $query = "SELECT
					( SELECT COUNT(`id`) FROM `tasks` ) +
					( SELECT COUNT(`id`) FROM `external_tasks` ) +
					( SELECT COUNT(`id`) FROM `jobs` ) +
					( SELECT COUNT(`id`) FROM `external_jobs` ) + 100000
					AS `total_today`,
					( SELECT COUNT(`id`) FROM `tasks` WHERE DATE(`created_at`) < DATE(DATE_SUB(NOW(), interval 1 DAY)) ) +
					( SELECT COUNT(`id`) FROM `external_tasks` WHERE DATE(`created_at`) < DATE(DATE_SUB(NOW(), interval 1 DAY)) ) +
					( SELECT COUNT(`id`) FROM `jobs` WHERE DATE(`created_at`) < DATE(DATE_SUB(NOW(), interval 1 DAY)) ) +
					( SELECT COUNT(`id`) FROM `external_jobs` WHERE DATE(`created_at`) < DATE(DATE_SUB(NOW(), interval 1 DAY)) ) + 100000
					AS `total_yesterday`";

        $this->db->query($query);
        $total      = $this->db->getSingleRow();

        $difference = $total['total_today'] - $total['total_yesterday'];

        $seconds          = \Carbon\Carbon::now()->diffInSeconds(\Carbon\Carbon::now()->startOfDay());
        $minutes          = \Carbon\Carbon::now()->diffInMinutes(\Carbon\Carbon::now()->startOfDay());
        $full_day_seconds = 86400;
        $full_day_minutes = 1440;

        if( $difference > $full_day_seconds ) {

            $full_number = $difference > $full_day_seconds ?
                floor($difference / $full_day_seconds) : floor($full_day_seconds / $difference);

            $number_to_add = $difference > $full_day_seconds ? floor($seconds * $full_number) : floor($seconds/$full_number);

        }else{

            $full_number = $difference > $full_day_minutes ?
                ceil($difference / $full_day_minutes) : ceil($full_day_minutes / $difference);

            $number_to_add = $difference > $full_day_minutes ? ceil($minutes * $full_number) : ceil($minutes/$full_number);

        }

        $total['total_yesterday'] += $number_to_add;

		return $total;
	}

	public function ajaxTotalOpportunities(){
		return sprintf(lang('search_over_opportunities'), number_format($this->total_external_jobs()['total_yesterday']));
	}

	public function sendgrid(){
		return render('../sendgrid.php', 'layout/default.php');
	}
	public function login(){
		if($this->request('redirect')){
			if( base64_decode($this->request('redirect'), true) )
				$redirect = base64_decode($this->request('redirect'));
			else
				$redirect = urldecode($this->request('redirect'));
		}elseif( isset($_SESSION[WEBSITE_PREFIX . 'REDIRECT']) ){
			if(empty($_SESSION[WEBSITE_PREFIX.'REDIRECT'])){
				if(!empty($_SERVER['REDIRECT_URL'])){
					if(!preg_match('/\/login/', $_SERVER['REDIRECT_URL'])){
						$redirect = preg_match('/http/', $_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : $this->site_host . $_SERVER['REDIRECT_URL'];
					}else{
						$redirect = $this->site_host . ROOTPATH . 'dashboard';
					}
				}
			}else{
                if( base64_decode($_SESSION[WEBSITE_PREFIX . 'REDIRECT'], true) ) {
                    $redirect = base64_decode($_SESSION[WEBSITE_PREFIX . 'REDIRECT']);
                }else{
                    $redirect = $_SESSION[WEBSITE_PREFIX.'REDIRECT'];
                }
			}
		}

        if( isset($redirect) && base64_decode($redirect, true) ) {
            $_SESSION[WEBSITE_PREFIX . 'REDIRECT'] = base64_decode($redirect);
        }elseif( isset($redirect) ){
            $_SESSION[WEBSITE_PREFIX . 'REDIRECT'] = $redirect;
        }else{
            $redirect = $this->site_host . ROOTPATH . 'dashboard';
        }

        if( isset($_SESSION[WEBSITE_PREFIX.'REDIRECT']) ) {
            $parsed_url = parse_url($_SESSION[WEBSITE_PREFIX . 'REDIRECT']);
            if (isset($parsed_url['query']) && !empty($parsed_url['query'])) {
                $query = $parsed_url['query'];
                parse_str($query, $query_vars);
                if (array_key_exists('search_name', $query_vars)) {
                    $_SESSION[WEBSITE_PREFIX . 'SAVE_SEARCH'] = $query_vars;
                    $redirect                                 = option('site_uri') . url_for('/dashboard');
                } elseif (array_key_exists('id', $query_vars)) {
                    $_SESSION[WEBSITE_PREFIX . 'FAV'] = $query_vars;
                    $redirect                         = $parsed_url['path'];
                }
            }
        }

		$_SESSION[WEBSITE_PREFIX . 'REDIRECT'] = $redirect;
		
		if($this->user->logged){
			header('Location: ' . $redirect);
			exit();
		}

		set('redirect', $redirect);
		set('title', 'Login');
		return render('sign_up/partial/login.php', 'layout/default.php');
	}

	public function selectLanguage($code = 'EN'){
		$code = strtoupper($code);

		$this->db->query("SELECT * FROM `language` WHERE code = " . $this->db->escape($code));
		$codeExist = $this->db->getSingleRow();

		if($codeExist){
			$_SESSION[WEBSITE_PREFIX.'LANGUAGE'] = $code;
		}else{
			$_SESSION[WEBSITE_PREFIX.'LANGUAGE'] = 'EN';
		}

		$redirect = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : url_for('/');

		header('Location: ' . $redirect);
		exit();
	}

	public function password_reset(){
		$this->db->query('SELECT * FROM temp_token WHERE token = "'.$_GET['token'].'" and token_status != 99 ');
		$exist = $this->db->getSingleRow();
		date_default_timezone_set("Asia/Kuala_Lumpur");
		if($exist && (strtotime($exist['expiry_date']) > strtotime(date("Y-m-d h:i:s"))) ){
			set('header_title', 'Reset Password');
			option('title_prefix', 'Reset Password | ');
			return render('sign_up/partial/password_reset.php', 'layout/default.php');
		}else {
			return render('404.php', 'layout/default.php');
		}
	}

	public function set_password(){
		$this->db->query('SELECT * FROM temp_token WHERE token = "'.$_GET['token'].'" and token_status != 99 ');
		$exist = $this->db->getSingleRow();
		date_default_timezone_set("Asia/Kuala_Lumpur");
		if($exist && (strtotime($exist['expiry_date']) > strtotime(date("Y-m-d H:i:s"))) ){
            if( $this->request('redirect') ) $_SESSION[WEBSITE_PREFIX . 'REDIRECT'] = $this->request('redirect');
			set('redirect', $this->request('redirect'));
			set('header_title', 'Set Password');
			option('title_prefix', 'Set Password | ');
			return render('sign_up/partial/password_set.php', 'layout/default.php');
		}else {
			return render('404.php', 'layout/default.php');
		}
	}

	public function forgot_password(){
		option('title_prefix', 'Forgot Password | ');
		return render('sign_up/partial/forgot_password.php', 'layout/default.php');
	}

	public function search(){
		return render('search.php', 'layout/login_default.php');
	}

	public function billing_earning(){
		set('page_title', 'Billings - Earning');
		return render('reports/individual/billing_earning.php', 'layout/app.html.php');
	}

	public function talent_details_public(){
		return render('talent_details_public.php', 'layout/public_default.php');
	}

	public function talent_listing_public(){
		return render('talent_listing_public.php', 'layout/public_default.php');
	}

	public function task_details_public(){
		return render('task_details_public.php', 'layout/public_default.php');
	}

	public function task_details(){
		return render('task_details.php', 'layout/login_default.php');
	}	

	public function task_listing_public(){
		return render('task_listing_public.php', 'layout/public_default.php');
	}	
	
	public function billing_spent(){
		set('page_title', 'My Spent');
		return render('reports/company/billing_spent.php', 'layout/app.html.php');
	}

		public function report(){
		return render('reports/partial/report.php', 'layout/login_default.php');
	}


	public function bubble_category(){
		return render('bubble_category.php', 'layout/default.php');
	}

	public function findUserById($id){
		$this->db->query('SELECT * FROM members WHERE id = "'.$id.'"');
		$userDetails = $this->db->getSingleRow();
		
		return $userDetails;
	}

	public function chats(){
		$this->db->query('SELECT distinct sender_id, receiver_id FROM chats WHERE sender_id = "'.$_SESSION['member_id'].'" or receiver_id = '.$_SESSION['member_id'].' order by created_by asc');
		$chats = $this->db->getRowList();
		
		$exist= array();
		$memberDetails = array();
		foreach ($chats  as $eachChat => $value) {

			foreach ($value as $keys => $values) {
			
				if ($values == $_SESSION['member_id']) {

					unset($chats[$eachChat][$keys]);

				}else{
					if(in_array($values, $exist)){
						unset($chats[$eachChat][$keys]);

					}else{
						array_push($exist, $values);
						array_push($memberDetails,$this->findUserById($values));

					}
				}
			}
		}
		
		
		set('memberDetails',$memberDetails);
		set('chats',$chats);
		//var_dump($memberDetails);
		return render('profile/partial/chats.php', 'layout/login_default.php');
		
		
	}

	public function settingsPage(){
		set("states", $this->get_country_id());
        set('header_title', lang('pref_title'));
		return render('settings.php', 'layout/login_default.php');
	}

	public function components(){
		return render('components.php', 'layout/default.php');
	}

	public function personalize(){
		$_SESSION[WEBSITE_PREFIX.'FIRST_LOGIN'] = 1;
		
		$selected_skills = array();

		if($this->user->info['skills']){
			$this->db->query("SELECT `id`, `name` FROM skills WHERE status = '1' AND id IN (" . $this->user->info['skills'] . ")");
			$rows = $this->db->getRowList();

			if($rows){
				foreach($rows as $row){
					$selected_skills[] = array(
						"id" => $row['id'],
						"name" => $row['name']
					);
				}
			}
		}
		
		set('selected_skills', $selected_skills);
		set('skills', $this->getSkillsList());
		set('interests', $this->getInterestsList());
		return render('', 'layout/personalize.php');
	}

	public function getSkillList($all = false){
		if(!empty($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) && $_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] != 'EN'){
			$language_id = $this->db->getValue("SELECT id FROM `language` WHERE status = '1' AND code = " . $this->db->escape($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']));
		}

		if(!$all){
			$this->db->query("SELECT * FROM skills WHERE status = '1' AND parent = '0'");
			$rows = $this->db->getRowList();

			if($rows){
				foreach($rows as $i => $row){
					if(!empty($language_id)){
						$text = $this->db->getValue("SELECT name FROM `skill_language_text` WHERE language_id = " . $this->db->escape($language_id) . " AND skill_id = " . $this->db->escape($row['id']));
						if($text) $rows[$i]['name'] = $text;
					}
					
					$this->db->query("SELECT * FROM skills WHERE status = '1' AND parent = " . $this->db->escape($row['id']));
					$subs = $this->db->getRowList();	
					
					if(!empty($language_id)){
						foreach($subs as $j => $sub){
							$text = $this->db->getValue("SELECT name FROM `skill_language_text` WHERE language_id = " . $this->db->escape($language_id) . " AND skill_id = " . $this->db->escape($sub['id']));
							if($text) $subs[$i]['name'] = $text;
						}
					}
					
					if($subs){
						$rows[$i]['regimes'] = $subs;
					}
				}
			}

		}else{
			
			$this->db->query("SELECT * FROM skills WHERE status = '1'");
			$rows = $this->db->getRowList();
		}

		return $rows;
	}

	public function getInterestsList($all = false, $id = 0){
		if(!empty($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) && $_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] != 'EN'){
			$language_id = $this->db->getValue("SELECT `id` FROM `language` WHERE status = '1' AND code = " . $this->db->escape($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']));
		}

		if(!$all){
			$this->db->query("SELECT `id`, `title` AS `name`, `parent` FROM `interest` WHERE `status` = '1' AND `parent` = '{$id}'");
			$rows = $this->db->getRowList();

			if( empty($rows) ) return [];

            $interest_ids = array_column($rows, 'id');

            $ids = implode("','", $interest_ids);
            $this->db->query("SELECT `id`, `title` AS `name`, `parent` FROM `interest` WHERE `status` = '1' AND `parent` IN ('{$ids}')");
            $children = $this->db->getRowList();

            $rows = array_merge($rows, $children);
            $interest_ids = array_merge($interest_ids, array_column($children, 'id'));

			$interests = [];
			$parents = array_reverse(array_unique(array_column($rows, 'parent')));

			$keys = array_intersect($parents, $interest_ids);

			foreach(array_reverse($rows) as $row) {
                if(!empty($language_id)){
                    $text = $this->db->getValue("SELECT `title` AS `name` FROM `interest_language_text` WHERE `language_id` = " . $this->db->escape($language_id) . " AND `interest_id` = " . $this->db->escape($row['id']));
                    if($text) $row['name'] = $text;
                }
                $interests[$row['id']] = $row;
            }

			$to_remove = [];
			foreach($keys as $parent) {
				$regimes = [];
                foreach (array_reverse($rows) as $interest) {
                    if($interest['parent'] == $parent) {
                        $regimes = isset($interests[$parent]['regimes']) ? $interests[$parent]['regimes'] : [];
                        $to_remove[] = $interest['id'];
                    }
                }

                $interests[$parent]['regimes'] = $regimes;
            }

			foreach ($interests as $interest) {
                if (in_array($interest['id'], $to_remove)) {
                    $interests[$interest['parent']]['regimes'][] = $interests[$interest['id']];
                    unset($interests[$interest['id']]);
                }
            }

			$rows = array_values(array_reverse($interests));

		}else{

			$this->db->query("SELECT `id`, `title` AS `name` FROM `interest` WHERE `status` = '1'");
			$rows = $this->db->getRowList();
		}

		return $rows;
	}

	public function getSkillsList($all = false, $id = 0){
		if(!empty($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) && $_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] != 'EN'){
			$language_id = $this->db->getValue("SELECT `id` FROM `language` WHERE status = '1' AND code = " . $this->db->escape($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']));
		}

		if(!$all){
			$this->db->query("SELECT `id`, `name`, `parent` FROM `skills` WHERE `status` = '1' AND `parent` = '{$id}'");
			$rows = $this->db->getRowList();

			if( empty($rows) ) return [];

            $skills_ids = array_column($rows, 'id');

            $ids = implode("','", $skills_ids);
            $this->db->query("SELECT `id`, `name`, `parent` FROM `skills` WHERE `status` = '1' AND `parent` IN ('{$ids}')");
            $children = $this->db->getRowList();

            $rows = array_merge($rows, $children);
            $skills_ids = array_merge($skills_ids, array_column($children, 'id'));

			$skills = [];
			$parents = array_reverse(array_unique(array_column($rows, 'parent')));

			$keys = array_intersect($parents, $skills_ids);

			foreach(array_reverse($rows) as $row) {
                if(!empty($language_id)){
                    $text = $this->db->getValue("SELECT `name` FROM `skill_language_text` WHERE `language_id` = " . $this->db->escape($language_id) . " AND `skill_id` = " . $this->db->escape($row['id']));
                    if($text) $row['name'] = $text;
                }
                $skills[$row['id']] = $row;
            }

			$to_remove = [];
			foreach($keys as $parent) {
				$regimes = [];
                foreach (array_reverse($rows) as $skill) {
                    if($skill['parent'] == $parent) {
                        $regimes = isset($skills[$parent]['regimes']) ? $skills[$parent]['regimes'] : [];
                        $to_remove[] = $skill['id'];
                    }
                }

                $skills[$parent]['regimes'] = $regimes;
            }

			foreach ($skills as $skill) {
                if (in_array($skill['id'], $to_remove)) {
                    $skills[$skill['parent']]['regimes'][] = $skills[$skill['id']];
                    unset($skills[$skill['id']]);
                }
            }

			$rows = array_values(array_reverse($skills));

		}else{

			$this->db->query("SELECT `id`, `name` FROM `skills` WHERE `status` = '1'");
			$rows = $this->db->getRowList();
            $skills = [];
            foreach($rows as $row) {
                if(!empty($language_id)){
                    $text = $this->db->getValue("SELECT `name` FROM `skill_language_text` WHERE `language_id` = " . $this->db->escape($language_id) . " AND `skill_id` = " . $this->db->escape($row['id']));
                    if($text) $row['name'] = $text;
                }
                $skills[] = $row;
            }
            $rows = $skills;
		}

		return $rows;
	}

    public function getSubInterests(){

        $id = (int)params('id') ?? (int)request('id') ?? 0;

        $rows = $this->getInterestsList(false, $id);

        return json($rows);


	    /*$parent_id = params('id') ?? request('id') ?? 0;

        if(!empty($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']) && $_SESSION[WEBSITE_PREFIX . 'LANGUAGE'] != 'EN'){
            $language_id = $this->db->getValue("SELECT `id` FROM `language` WHERE status = '1' AND code = " . $this->db->escape($_SESSION[WEBSITE_PREFIX . 'LANGUAGE']));
        }

        $this->db->query("SELECT `id`, `title` AS `name`, `parent` FROM `interest` WHERE `status` = '1' AND `parent` = '{$parent_id}'");
        $rows = $this->db->getRowList();
        $interests = [];

	    foreach ($rows as $row){
	    	$interests[$row['id']] = $row;
        }

        if( !empty($rows) && isset($language_id) ){
        	$ids = array_column($rows, 'id');
        	$ids = implode("','", $ids);
            $this->db->query("SELECT `interest_id` AS `id`, `title` AS `name` FROM `interest_language_text` WHERE `language_id` = " . $this->db->escape($language_id) . " AND `interest_id` IN ('{$ids}')");
            $translations = $this->db->getRowList();

            if( !empty($translations) ){
            	foreach($translations as $translation){
            		if( in_array($translation['id'], array_keys($interests)) ){
            			$interests[$translation['id']]['name'] = $translation['name'];
                    }
                }
            }
        }

	    return json(array_values($interests));*/
	}

    public function getSubSkills(){

        $id = (int)params('id') ?? (int)request('id') ?? 0;

        $rows = $this->getSkillsList(false, $id);

        return json($rows);
	}

	public function ajaxSkillList(){
		$all = $this->request('all') ? true : false;
        $id = (int)request('id') ?? 0;
		$rows = $this->getSkillsList($all, $id);

        $skills = [];

        if($rows){
            foreach($rows as $row){
                $skills[] = [
                    "id" => $row['id'],
                    "name" => $row['name'],
                ];
            }
        }

        return json($skills);
	}

	public function ajaxInterestsList(){
		$all = $this->request('all') ? true : false;
		$id = (int)$this->request('id') ?? 0;
		$rows = $this->getInterestsList($all, $id);
		$interests = [];
		/*$bm_interests = [];
		$new_interests = [];*/

		if($rows){
			foreach($rows as $row){
				$interests[] = [
					"id" => $row['id'],
					"name" => $row['name'],
				];
			}
		}

		/*if( strtolower($_SESSION[WEBSITE_PREFIX.'LANGUAGE']) !== 'en' ){
			$this->db->query("SELECT `interest_id` AS `id`, `name` FROM `skill_language_text` WHERE `language_id` = 2");
			$bm_interests = $this->db->getRowList();
        }

		foreach ($interests as $key => $value){
			$new_interests[$value['id']] = $value;
        }

		foreach ($bm_interests as $key => $value){
			$new_interests[$value['id']] = $value;
        }*/

		return json($interests);
	}

	public function dashboard(){
		set_latest_url( request_uri() );
		if( $this->user->info['type'] === '1' ){
			if( !isset($this->user->info['company']['phone'], $this->user->info['company']['email']) ){
				redirect_to( option('site_uri') . url_for('/my-company-profile-signup') );
            }
        }

		(new TaskCentre($this->db, $this->user))->was_viewing(request('task') ?? request('job'), request('task') ? 'task' : 'job');

		$this->db->query("SELECT * FROM preference WHERE member_id = " .$this->db->escape($this->user->id)) ;
		$preference = $this->db->getSingleRow();

		$this->db->table("member_logs");
		$this->db->insertArray(array(
			"member_id" => $this->user->id,
			"date" => "NOW()",
			"ip_address" => $_SERVER['REMOTE_ADDR'],
			"user_agent" => $_SERVER['HTTP_USER_AGENT'],
		));
		$this->db->insert();

		$monday = date('Y-m-d', strtotime('monday this week'));
		$sunday = date('Y-m-d', strtotime('sunday this week'));
		$workload_this_week = $this->db->getValue("SELECT SUM(hours) FROM tasks WHERE status = 'in-progress' AND assigned_to = " . $this->db->escape($this->user->id) . " AND DATE(start_by) BETWEEN '" . $monday . "' AND '" . $sunday . "'");
		$workload_this_week = $workload_this_week ? ($workload_this_week > 12 ? 12 : $workload_this_week) : 0;

        if( isset($_SESSION[WEBSITE_PREFIX.'SAVE_SEARCH']) && !empty($_SESSION[WEBSITE_PREFIX.'SAVE_SEARCH']) ){
            $params = $_SESSION[WEBSITE_PREFIX.'SAVE_SEARCH'];
            $search_name = $params['search_name'] !== '' ? $params['search_name'] : $params['q'];
            unset($params['search_name']);
            $search_params = $params;
            $this->db->table('search');
            $this->db->insertArray([
                'user_id' => $this->user->info['id'],
                'name'    => $search_name,
                'params'  => json_encode($search_params)
            ]);
            $this->db->insert();
            unset($_SESSION[WEBSITE_PREFIX.'SAVE_SEARCH']);
        }

		$saved_search = (new Search($this->db, $this->user))->get_search();
		if($preference['dashboard_mode'] === 'work') {
            list($today_task, $earning_this_month, $earning_last_month, $spending_this_month, $spending_last_month,
                $urgent_task, $ongoing_task, $created_task, $prev_post_task, $pending_task, $new_job_recommendations,
                $job_created, $just_posted, $matched_skills) = $this->get_widgets_data();
        }else{
			set('tasks_listing', (new TaskCentre($this->db, $this->user))->tasks_listing(true) );
        }

		set('workload_this_week', $workload_this_week);
		set('preference', $preference);
		set('saved_search', $saved_search);
		set('page_title', 'Dashboard');
		set('today_task', $today_task);
		set('earning_this_month', $earning_this_month);
		set('earning_last_month', $earning_last_month);
		set('spending_this_month', $spending_this_month);
		set('spending_last_month', $spending_last_month);
		set('urgent_task', $urgent_task);
		set('ongoing_task', $ongoing_task);
		set('created_task', $created_task);
		set('prev_post_task', $prev_post_task);
		set('pending_task', $pending_task);
		set('new_job_recommendations', $new_job_recommendations);
		set('job_created', $job_created);
		set('just_posted', $just_posted);
		set('matched_skills', $matched_skills);
        set('states', $this->states());
        set('cities', []);
        set('task_categories', $this->getCategories(0, 'task_category'));
        set('job_categories', $this->getCategories());
		return render('dashboard.php', 'layout/app.html.php');
	}

	public function myCompanyPage(){
        $taskObject = new Task($this->db, $this->user);
        $jobObject  = new Job($this->db, $this->user);
        $taskCentre = new TaskCentre($this->db, $this->user);
        $user_id    = params('id') ? (int)str_replace('TL', '', sanitize(params('id'))) - 20100 : null;
        $all        = $taskObject->get_my_published_tasks_jobs($user_id);
        $tasks      = [];
        $jobs       = [];

        foreach ($all as $key => $task) {
            if($task['the_type'] === 'task') {
                $task = $taskObject->get_task_by_id($task['task_id']);
                unset($task['password']);
                $tasks[$key] = $task;
                $tasks[$key]['type'] = 'task';
                $tasks[$key]['country']         = $this->getCountryName($task['country']);
                $tasks[$key]['state']           = $this->getStateName($task['state']);
                $tasks[$key]['published_count'] = $taskObject->get_published_tasks_count($task['user_id']);
                $tasks[$key]['rating']          = $taskCentre->get_ratings($task['user_id']);
                $tasks[$key]['questions']       = $taskObject->questions($task['task_id']);
                $tasks[$key]['disputed_count']  = $taskObject->get_status_count('disputed', $task['user_id']);
            }else{
                $job  = $jobObject->get_job_by_id($task['task_id']);
                unset($job['password']);
                $jobs[$key] = $job;
                $jobs[$key]['type'] = 'job';
                $jobs[$key]['country']         = $this->getCountryName($job['job_country']);
                $jobs[$key]['state']           = $this->getStateName($job['job_state']);
                $jobs[$key]['published_count'] = $jobObject->get_published_jobs_count($job['user_id']);
                $jobs[$key]['rating']          = $taskCentre->get_ratings($job['user_id']);
                $jobs[$key]['questions']       = $jobObject->questions($job['job_id']);
                $jobs[$key]['disputed_count']  = $jobObject->get_status_count('disputed', $job['user_id']);
            }
        }
		if( !is_null($user_id) ){
			$user = new stdClass;
			$user->info = $this->user->info($user_id);
			set('company', $user);
        }
		if(isset($user)){
            $applied_jobs    = $jobObject->get_applied_jobs($this->user->info['id']);
            $applied_tasks   = $taskObject->get_applied_tasks($this->user->info['id']);
            $favourite_jobs  = $jobObject->get_favourites($this->user->info['id']);
            $favourite_tasks = $taskObject->get_favourites($this->user->info['id']);

            $this->user->applied_ids = array_merge(array_column($applied_jobs, 'id'), array_column($applied_tasks, 'id'));
            $this->user->favourite_ids = array_merge(array_column($favourite_jobs, 'task_id'), array_column($favourite_tasks, 'task_id'));
        }

		set('current_user', $this->user);
        set('posts', array_merge($tasks, $jobs));
		set('tasks', $tasks);
		set('jobs', $jobs);
        set('ratings', $taskCentre->get_ratings($this->user->info['id']));
        set('highest_rating', $taskCentre->get_highest_rating());
		set('page_title', 'Company Profile');
		return render('my-company-page.php', 'layout/app.html.php');
	}

	public function dashboardWidget(){
        list($today_task, $earning_this_month, $earning_last_month, $spending_this_month, $spending_last_month, $urgent_task, $ongoing_task, $created_task, $prev_post_task, $pending_task, $new_job_recommendations, $job_created) = $this->get_widgets_data();
        set('today_task', $today_task);
        set('earning_this_month', $earning_this_month);
        set('earning_last_month', $earning_last_month);
        set('spending_this_month', $spending_this_month);
        set('spending_last_month', $spending_last_month);
        set('urgent_task', $urgent_task);
        set('ongoing_task', $ongoing_task);
        set('created_task', $created_task);
        set('prev_post_task', $prev_post_task);
        set('pending_task', $pending_task);
		set('new_job_recommendations', $new_job_recommendations);
		set('job_created', $job_created);
		return render('', 'dashboard-widget.php');
	}

	public function my_company_profile(){
		$this->db->query("SELECT * FROM members WHERE  id = " .$this->db->escape($_SESSION['member_id']) );
		$member = $this->db->getSingleRow();

		$this->db->query("SELECT * FROM company_details WHERE  member_id = " . $this->db->escape($_SESSION['member_id']) );
		$company_details = $this->db->getSingleRow();

		set("member",$member);
		set("company_details",$company_details);
		return render('profile/company/my_company_profile.php', 'layout/app.html.php');
	}

	public function myCV(){
		$this->db->query("SELECT * FROM members WHERE id = " . $this->db->escape($this->user->id) );
		$member = $this->db->getSingleRow();
		
		$this->db->query("SELECT * FROM employment_details WHERE  member_id = " . $this->db->escape($this->user->id) );
		$employment_details = $this->db->getRowList();

		$this->db->query("SELECT * FROM education WHERE  member_id = " . $this->db->escape($this->user->id) );
		$educations = $this->db->getRowList();

		$this->db->query("SELECT * FROM preference WHERE  member_id = " . $this->db->escape($this->user->id) );
		$preference = $this->db->getSingleRow();

		$this->db->query("SELECT * FROM member_docs WHERE member_id = " . $this->db->escape($this->user->id) );
		$docs = $this->db->getRowList();

		$_docs = array();
		foreach($docs as $doc){
			$_docs[$doc['type']][] = $doc['src'];
		}

		$selected_skills = array();

		if($this->user->info['skills']){
			$this->db->query("SELECT * FROM skills WHERE id IN (" . $this->user->info['skills'] . ")"); //status = '1' AND
			$rows = $this->db->getRowList();

			if($rows){
				foreach($rows as $row){
					$selected_skills[] = array(
						"id" => $row['id'],
						"name" => $row['name']
					);
				}
			}
		}
		
		set("member",$member);
		set("employment_details",$employment_details);
		set("educations",$educations);
		set("preference",$preference);
		set("countries",$this->countries());
		set("states_employment",$this->states(132));
		set("docs", $_docs);
		set('selected_skills', $selected_skills);
		set('header_title', lang('my_cv_main_title'));
		return render('profile/individual/my_cv.php', 'layout/login_default.php');
	}

	public function myProfile(){
		$this->db->query("SELECT * FROM members WHERE id = " . $this->db->escape($this->user->id) );
		$member = $this->db->getSingleRow();
		
		$this->db->query("SELECT * FROM employment_details WHERE  member_id = " . $this->db->escape($this->user->id) );
		$employment_details = $this->db->getRowList();

		$this->db->query("SELECT * FROM education WHERE  member_id = " . $this->db->escape($this->user->id) );
		$educations = $this->db->getRowList();

		$this->db->query("SELECT * FROM preference WHERE  member_id = " . $this->db->escape($this->user->id) );
		$preference = $this->db->getSingleRow();

		$this->db->query("SELECT * FROM member_docs WHERE member_id = " . $this->db->escape($this->user->id) );
		$docs = $this->db->getRowList();

		$_docs = array();
		foreach($docs as $doc){
			$_docs[$doc['type']][] = $doc['src'];
		}

		$selected_skills = array();

		if($this->user->info['skills']){
			$this->db->query("SELECT * FROM skills WHERE id IN (" . $this->user->info['skills'] . ")"); //status = '1' AND
			$rows = $this->db->getRowList();

			if($rows){
				foreach($rows as $row){
					$selected_skills[] = array(
						"id" => $row['id'],
						"name" => $row['name']
					);
				}
			}
		}
		
		set("member",$member);
		set("employment_details",$employment_details);
		set("educations",$educations);
		set("preference",$preference);
		set("countries",$this->countries());
		set("states_employment",$this->states(132));
		set("docs", $_docs);
		set('selected_skills', $selected_skills);
		set('header_title', lang('my_profile_main_title'));
		return render('profile/individual/my_profile.php', 'layout/login_default.php');
	}

	public function myCompanyProfileSignup(){
		$this->db->table("member_logs");
		$this->db->insertArray(array(
			"member_id" => $this->user->id,
			"date" => "NOW()",
			"ip_address" => $_SERVER['REMOTE_ADDR'],
			"user_agent" => $_SERVER['HTTP_USER_AGENT'],
		));
		$this->db->insert();

		return render('', 'layout/my_company_profile_signup.php');
	}

	public function myCompanyProfile(){
        set('page_title', 'My Company Profile');
		return render('profile/company/my_company_profile.php', 'layout/app.html.php');
	}

	public function my_rating_centre(){
		return render('profile/individual/my_rating_centre.php', 'layout/login_default.php');
	}
	//End of Eric's Code

	public function fbLogin(){
		$fb = new Facebook\Facebook([
			'app_id' => $this->fbAppId,
			'app_secret' => $this->fbAppSecret,
			'default_graph_version' => 'v3.2',
		]);

		$helper = $fb->getRedirectLoginHelper();
				
		$permissions = ['email'];		
		$loginUrl = $helper->getLoginUrl('https://' . $_SERVER['HTTP_HOST'] . option('base_uri') . 'login/fb/callback', $permissions);
		
		header('Location: ' . $loginUrl);
		exit();
	}
	
	public function fbLoginCallback(){
		$fb = new Facebook\Facebook([
			'app_id' => $this->fbAppId,
			'app_secret' => $this->fbAppSecret,
			'default_graph_version' => 'v3.2',
		]);

		$helper = $fb->getRedirectLoginHelper();
		
		try {
			$accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		
		if (!isset($accessToken)) {
			$error = array();
			if ($helper->getError()) {
				header('HTTP/1.0 401 Unauthorized');
				$error['error'] = $helper->getError();
				$error['code'] = $helper->getErrorCode();
				$error['reason'] = $helper->getErrorReason();
				$error['description'] = $helper->getErrorDescription();
			} else {
				header('HTTP/1.0 400 Bad Request');
				$error['error'] = 'Bad request';
				$error['description'] = 'Bad request';
			}

			set('error', $error);
			option('title_prefix', 'Login | ');
			return render('sign_up/partial/login.php', 'layout/default.php');
		}
		
		$oAuth2Client = $fb->getOAuth2Client();
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);
		$tokenMetadata->validateAppId($this->fbAppId);
		$tokenMetadata->validateExpiration();
		
		if (!$accessToken->isLongLived()) {
			try {
				$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
			} catch (Facebook\Exceptions\FacebookSDKException $e) {
				echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
				exit;
			}		  
		}
		
		$_SESSION['fb_access_token'] = (string)$accessToken;
		
		try {
			$response = $fb->get('/me?fields=id,name,email,first_name,last_name', $accessToken);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		
		$user = $response->getGraphUser();
		
		if($user){											
			$_SESSION[WEBSITE_PREFIX.'FB']['UID'] = $user['id'];
			$_SESSION[WEBSITE_PREFIX.'FB']['FULLNAME'] = $user['name'];
			$_SESSION[WEBSITE_PREFIX.'FB']['FIRSTNAME'] = $user['first_name'];
			$_SESSION[WEBSITE_PREFIX.'FB']['LASTNAME'] = $user['last_name'];
			$_SESSION[WEBSITE_PREFIX.'FB']['EMAIL'] = $user['email'];
			$_SESSION[WEBSITE_PREFIX.'FB']['GENDER'] = $user['gender'];
			$_SESSION[WEBSITE_PREFIX.'FB']['BIRTHDAY'] = $user['birthday'];
			$_SESSION[WEBSITE_PREFIX.'FB']['LINK'] = $user['link'];
			
			header("Location: " . url_for('/login/fbCheck'));
			exit();
			
		}else{
			header("Location: " . url_for('/login/fb'));
			exit();
		}
	}
	
	public function fbCheck(){
		if($_SESSION[WEBSITE_PREFIX.'FB']['UID'] > 0){
				
			$this->db->query("SELECT * FROM members WHERE (fb_uid = " . $this->db->escape($_SESSION[WEBSITE_PREFIX.'FB']['UID']) . " OR email = " . $this->db->escape($_SESSION[WEBSITE_PREFIX.'FB']['EMAIL']) . ") AND status < 999");
			$member = $this->db->getSingleRow();
						
			if($member){
				if($member['status'] == '9'){
					$return['error'] = true;
					$return['msg'] = "Your account is suspended.<br>Please contact our website administrator."; 
				
				}else{
					$_SESSION[WEBSITE_PREFIX.'USER_ID'] = $member['id'];
					$_SESSION[WEBSITE_PREFIX.'LAST_LOGIN'] = date('Y-m-d H:i:s');
										
					if(empty($member['photo'])){
						$targetDir = 'files/' . $member['id'] . '/profile/';
						if (!file_exists($targetDir)) createPath($targetDir);
						array_map('unlink', glob($targetDir . "*"));
						$photo = $targetDir . 'fbProfilePhoto.jpg';
						$cover = $targetDir . 'fbCoverPhoto.jpg';

						copy('https://graph.facebook.com/' . $_SESSION[WEBSITE_PREFIX.'FB']['UID'] . '/picture?type=large', $photo);
						$this->db->table("members");
						$this->db->updateArray(array(
							"photo" => $photo,
						));
						$this->db->whereArray(array(
							"id" => $member['id'],
						));
						$this->db->update();
					}
					
					$this->db->table("members");
					$this->db->updateArray(array(
						"fb_uid" => $_SESSION[WEBSITE_PREFIX.'FB']['UID'],
						"firstname" => !empty($_SESSION[WEBSITE_PREFIX.'FB']['FIRSTNAME']) ? $_SESSION[WEBSITE_PREFIX.'FB']['FIRSTNAME'] : $_SESSION[WEBSITE_PREFIX.'FB']['FULLNAME'],
						"lastname" => $_SESSION[WEBSITE_PREFIX.'FB']['LASTNAME'],
						"status" => '1',
					));
					$this->db->whereArray(array(
						"id" => $member['id'],
					));
					$this->db->update();
				
					$this->db->table("member_logs");
					$this->db->insertArray(array(
						"member_id" => $member['id'],
						"date" => "NOW()",
						"ip_address" => $_SERVER['REMOTE_ADDR'],
						"user_agent" => $_SERVER['HTTP_USER_AGENT'],
					));
					$this->db->insert();
															
					$return['error'] = false;
					$return['msg'] = "You have been logged in successfully."; 
					
					$redirect = !empty($_SESSION[WEBSITE_PREFIX.'REDIRECT']) ? $_SESSION[WEBSITE_PREFIX.'REDIRECT'] : $this->site_host . ROOTPATH . 'dashboard';
					header("Location: " . $redirect);
					exit();
				}

				if($return['error']){
					$error['description'] = $return['msg'];
					set('error', $error);
					option('title_prefix', 'Login | ');
					return render('sign_up/partial/login.php', 'layout/default.php');
				}
				
			}else{
					
				$this->db->table("members");
				$this->db->insertArray(array(
					"fb_uid" => $_SESSION[WEBSITE_PREFIX.'FB']['UID'],
					"email" => $_SESSION[WEBSITE_PREFIX.'FB']['EMAIL'],
					"firstname" => !empty($_SESSION[WEBSITE_PREFIX.'FB']['FIRSTNAME']) ? $_SESSION[WEBSITE_PREFIX.'FB']['FIRSTNAME'] : $_SESSION[WEBSITE_PREFIX.'FB']['FULLNAME'],
					"lastname" => $_SESSION[WEBSITE_PREFIX.'FB']['LASTNAME'],
					"date_created" => "NOW()",
					"status" => "1"
				));
				$this->db->insert();
				
				$member_id = $this->db->insertid();
				$_SESSION[WEBSITE_PREFIX.'USER_ID'] = $member_id;
				$_SESSION[WEBSITE_PREFIX.'LAST_LOGIN'] = date('Y-m-d H:i:s');
				
				$targetDir = 'files/' . $member_id . '/profile/';
				if (!file_exists($targetDir)) createPath($targetDir);
				$photo = $targetDir . 'fbProfilePhoto.jpg';
				
				if(empty($member['photo'])){
					copy('https://graph.facebook.com/' . $_SESSION[WEBSITE_PREFIX.'FB']['UID'] . '/picture?type=large', $photo);
					$this->db->table("members");
					$this->db->updateArray(array(
						"photo" => $photo,
					));
					$this->db->whereArray(array(
						"id" => $member_id,
					));
					$this->db->update();
				}

				$redirect = !empty($_SESSION[WEBSITE_PREFIX.'FB']['redirect']) ? $_SESSION[WEBSITE_PREFIX.'FB']['redirect'] : $this->site_host . ROOTPATH . 'dashboard';
				header("Location: " . $redirect);
				exit();
			}
		}else{
        
            $return['error'] = true;
            $return['msg'] = "Facebook Login failed, please try again later.<br />Please contact our website administrator if you still see this error.";
            header('Location: ' . url_for('/'));
			exit();
		}
	}

	public function googleLogin(){
		$client_id = $this->googleClientID; 
		$client_secret = $this->googleClientSecret;
		$redirect_uri = 'https://' . $_SERVER['HTTP_HOST'] . option('base_uri') . 'login/google';
		
		$client = new Google_Client();
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);
		$client->setRedirectUri($redirect_uri);
		$client->addScope("email");
		$client->addScope("profile");
		
		//if($client->isAccessTokenExpired()) {
		//	unset($_SESSION['access_token']);
		//}
		
		$service = new Google_Service_Oauth2($client);
				
		if (isset($_GET['code'])){
			$token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
			$_SESSION['access_token'] = $token['access_token'];
			header('Location: ' . $redirect_uri);
			//echo '<meta http-equiv="refresh" content="0; URL=' . $redirect_uri . '">';
		}
						
		if (isset($_SESSION['access_token'])) {
			$client->setAccessToken($_SESSION['access_token']);
		} else {
			$authUrl = $client->createAuthUrl();
		}
						
		if (!empty($authUrl)){ 
			header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));
			
		}else{
			if(empty($_SESSION[WEBSITE_PREFIX.'GOOGLE'])){
				$user = $service->userinfo->get();
				$_SESSION[WEBSITE_PREFIX.'GOOGLE'] = $user;
			}
			
			header("Location: " . url_for('/login/googleCheck'));
			exit();
		}
	}
	
	public function googleCheck(){
		$google = !empty($_SESSION[WEBSITE_PREFIX.'GOOGLE']) ? $_SESSION[WEBSITE_PREFIX.'GOOGLE'] : '';
		$google = json_decode(json_encode($google), true);
		
		if(!empty($google['id'])){
			
			$this->db->query("SELECT * FROM members WHERE (google_id = " . $this->db->escape($google['id']) . " OR email = " . $this->db->escape($google['email']) . ") AND status < 999");
			$member = $this->db->getSingleRow();
						
			if($member){
				if($member['status'] == '9'){
					$return['error'] = true;
					$return['msg'] = "Your account is suspended.<br>Please contact our website administrator."; 
				
				}else{
					
					$_SESSION[WEBSITE_PREFIX.'USER_ID'] = $member['id'];
					$_SESSION[WEBSITE_PREFIX.'LAST_LOGIN'] = date('Y-m-d H:i:s');
										
					if(empty($member['photo'])){
						$targetDir = 'files/' . $member['id'] . '/profile/';
						if (!file_exists($targetDir)) createPath($targetDir);
						array_map('unlink', glob($targetDir . "*"));
						$photo = $targetDir . 'googleProfilePhoto.jpg';

						copy($google['picture'], $photo);
						$this->db->table("members");
						$this->db->updateArray(array(
							"photo" => $photo,
						));
						$this->db->whereArray(array(
							"id" => $member['id'],
						));
						$this->db->update();
					}
					
					$this->db->table("members");
					$this->db->updateArray(array(
						"google_id" => $google['id'],
						"firstname" => $google['givenName'],
						"lastname" => $google['familyName'],
						"status" => '1',
					));
					$this->db->whereArray(array(
						"id" => $member['id'],
					));
					$this->db->update();
				
					$this->db->table("member_logs");
					$this->db->insertArray(array(
						"member_id" => $member['id'],
						"date" => "NOW()",
						"ip_address" => $_SERVER['REMOTE_ADDR'],
						"user_agent" => $_SERVER['HTTP_USER_AGENT'],
					));
					$this->db->insert();
															
					$return['error'] = false;
					$return['msg'] = "You have been logged in successfully."; 
					
					$redirect = !empty($_SESSION[WEBSITE_PREFIX.'REDIRECT']) ? $_SESSION[WEBSITE_PREFIX.'REDIRECT'] : $this->site_host . ROOTPATH . 'dashboard';
					header("Location: " . $redirect);
					exit();
				}

				if($return['error']){
					$error['description'] = $return['msg'];
					set('error', $error);
					option('title_prefix', 'Login | ');
					return render('sign_up/partial/login.php', 'layout/default.php');
				}
				
			}else{
				
				$this->db->table("members");
				$this->db->insertArray(array(
					"google_id" => $google['id'],
					"firstname" => $google['givenName'],
					"lastname" => $google['familyName'],
					"email" => $google['email'],
					"date_created" => "NOW()",
					"status" => "1"
				));
				$this->db->insert();
				
				$member_id = $this->db->insertid();
				$_SESSION[WEBSITE_PREFIX.'USER_ID'] = $member_id;
				$_SESSION[WEBSITE_PREFIX.'LAST_LOGIN'] = date('Y-m-d H:i:s');
				
				$targetDir = 'files/' . $member_id . '/profile/';
				if (!file_exists($targetDir)) createPath($targetDir);
				$photo = $targetDir . 'googleProfilePhoto.jpg';
				
				if(empty($member['photo'])){
					copy($google['picture'], $photo);
					$this->db->table("members");
					$this->db->updateArray(array(
						"photo" => $photo,
					));
					$this->db->whereArray(array(
						"id" => $member_id,
					));
					$this->db->update();
				}
				
				$redirect = !empty($_SESSION[WEBSITE_PREFIX.'REDIRECT']) ? $_SESSION[WEBSITE_PREFIX.'REDIRECT'] : $this->site_host . ROOTPATH . 'dashboard';
				header("Location: " . $redirect);
				exit();
			}
			
		}else{
			header('Location: ' . url_for('/login/google'));
			exit();
		}
	}

	public function ajaxPinMessage(){
		$this->db->query("SELECT * FROM pinned_messages WHERE message_id = ".$this->db->escape($this->request('id'))." and pinned_by = ".$_SESSION['WEB_USER_ID']);
		$exist = $this->db->getSingleRow();	

		if(!$exist){
			$this->db->table("pinned_messages");
			$this->db->insertArray(array(
				"message_id" => $this->request('id'),
				"message_type" => $this->request('type'),
				"task_id" => $_SESSION['task_id'],
				"pinned_by" => $_SESSION['WEB_USER_ID'],
			));
			$this->db->insert();
		}
	}

	public function ajaxUnpinMessage($id = 0){
		$this->db->table("pinned_messages");
		$this->db->whereArray(array(
			"id" => $id,
			"pinned_by" => $this->user->id,
		));
		$this->db->delete();
	}

	public function ajaxChatContent($member_id = 0, $task_id = '', $type = 'task'){
		if(isset($_SESSION['chatsCount'])){
			unset($_SESSION['chatsCount']);
		}
		if(isset($_SESSION['read_messages_count'])){
			unset($_SESSION['read_messages_count']);
		}
		
		if(isset($_SESSION['pinnedMessage'])){
			unset($_SESSION['pinnedMessage']);
		}

		if($member_id && $this->user->id){
			$this->db->queryOrDie("UPDATE chats SET viewed = '1', viewed_date = NOW() WHERE receiver_id = " . $this->db->escape($this->user->id) . " AND sender_id = " . $this->db->escape($member_id));

			$this->db->query("SELECT *  FROM chats WHERE ((sender_id = " . $this->db->escape($this->user->id) . " AND receiver_id = " . $this->db->escape($member_id) . ") OR (sender_id = " . $this->db->escape($member_id) . " AND receiver_id = " . $this->db->escape($this->user->id) . ")) AND task_id = ".$this->db->escape($task_id)." AND deleted = '0' ORDER BY created_by ASC");
			$chats = $this->db->getRowList();
			
            $this->db->query("SELECT count(`id`) AS `count` FROM chats WHERE sender_id = " . $this->db->escape($this->user->id) . " AND receiver_id = " . $this->db->escape($member_id) . " and task_id =".$this->db->escape($task_id)." AND viewed = '1' and deleted = '0' ");
            $readMessageCount = $this->db->getRowList();

            foreach ($readMessageCount as $key) {
            	$_SESSION['read_messages_count'] = $key['count'];
            	
            }

            $this->db->query("SELECT count(`id`) AS `count`  FROM chats WHERE ((sender_id = " . $this->db->escape($this->user->id) . " AND receiver_id = " . $this->db->escape($member_id) . ") OR (sender_id = " . $this->db->escape($member_id) . " AND receiver_id = " . $this->db->escape($this->user->id) . ")) and task_id = ".$this->db->escape($task_id)." and deleted = 0 ORDER BY created_by ASC");
			$chatsCount = $this->db->getRowList();

            foreach ($chatsCount as $key) {
            	$_SESSION['chatsCount'] = $key['count'];
            }

            $this->db->query("SELECT count(`id`) AS `count` FROM pinned_messages WHERE pinned_by = " . $this->db->escape($this->user->id) ." and task_id =".$this->db->escape($task_id)." AND pin_status = '0'  ");
            $pinMessage = $this->db->getSingleRow();

            foreach ($pinMessage as $key) {
            	$_SESSION['pinnedMessage'] = $key['count'];
            }

            if(isset($_SESSION['task_id'])){
            	unset($_SESSION['task_id']);
            }
            if( count($chats) > 0 )
                $_SESSION['task_id'] = $chats[0]['task_id'];
			
			$last_sent_msg = $this->db->getValue("SELECT created_by FROM chats WHERE sender_id = " . $this->db->escape($member_id) . " AND deleted = '0' ORDER BY created_by DESC LIMIT 1");
			
			$last_sent_msg = $last_sent_msg ? timeAgo($last_sent_msg) : lang('new_user');
			if( $type === 'task' )
				$this->db->query("SELECT * FROM tasks WHERE id = " . $this->db->escape($task_id));
			else
                $this->db->query("SELECT * FROM `jobs` WHERE id = " . $this->db->escape($task_id));
        	$task = $this->db->getSingleRow();

			set('taskType', $type);
			set('task', $task);
			set('chats', $chats);
			set('member', $this->user->info($member_id));
			set('last_sent_msg', $last_sent_msg);
			return render('', 'chat/content.php');
		}
	}

	public function getImageBatch($batch_id){
		$this->db->query("SELECT * FROM chats WHERE batch_id = ".$this->db->escape($batch_id)." and type like 'image%'  ORDER BY created_by ASC");
		$imagePath = $this->db->getRowList();

		return $imagePath;	

	}

	public function getFileBatch($batch_id){
		$this->db->query("SELECT * FROM chats WHERE batch_id = ".$this->db->escape($batch_id)." and type like 'file%' and deleted = 0 ORDER BY created_by ASC");
		$filePath = $this->db->getRowList();

		return $filePath;	

	}

	public function ajaxChatCount($id = 0, $task_id = null, $type = 'task'){
		if($id && $this->user->id){
			$this->db->query("SELECT count(*) AS row FROM chats WHERE ((sender_id = " . $this->db->escape($this->user->id) . " AND receiver_id = " . $this->db->escape($id) . ") OR (sender_id = " . $this->db->escape($id) . " AND receiver_id = " . $this->db->escape($this->user->id) . ")) and task_id =".$this->db->escape($task_id)." and deleted = 0  ORDER BY created_by ASC");
			$chats = $this->db->getRowList();


            $this->db->query("SELECT count(`id`) AS `count` FROM chats WHERE sender_id = " . $this->db->escape($this->user->id) . " AND receiver_id = " . $this->db->escape($id) . " and task_id =".$this->db->escape($task_id)." AND viewed = '1' and deleted = '0' ");
            $readMessageCount = $this->db->getRowList();

            foreach ($readMessageCount as $key) {
            	$seenCount = $key['count'];
            	
            }

            $this->db->query("SELECT count(`id`) AS `count` FROM pinned_messages WHERE pinned_by = " . $this->db->escape($this->user->id) ." and task_id =".$this->db->escape($task_id)." AND pin_status = '0'  ");
            $pinMessage = $this->db->getSingleRow();
           
           foreach ($pinMessage as $key) {
            	$pinMessageCount = $key['count'];
            	
            }
            
			foreach($chats as $row){
				
				if(isset($_SESSION['chatsCount'] ) && isset($_SESSION['receiver_id'])){
					if($_SESSION['receiver_id'] == $id){

						if($row['row'] != $_SESSION['chatsCount'] || $seenCount > $_SESSION['read_messages_count'] || $pinMessageCount !=  $_SESSION['pinnedMessage']   ){
							echo $this->ajaxChatContent($id, $task_id, $type);

							$_SESSION['chatsCount'] = $row['row'];
						}
					}else{
						$_SESSION['chatsCount'] = $row['row'];
						$_SESSION['receiver_id'] = $id;
					}
				}else{

					$_SESSION['chatsCount'] = $row['row'];
					$_SESSION['receiver_id'] = $id;
				}
			}
		}
	}

	public function ajaxChatSidebar($member_id = 0, $task_id = '', $type = ''){
		$imageByTask = $this->returnAllImagesByTaskID($this->user->id, $member_id, $task_id);
		$fileByTask = $this->returnAllFilesByTaskID($this->user->id, $member_id,  $task_id);
		$pinnedItems = $this->returnAllPinnedItemsByTaskID($this->user->id, $member_id, $task_id);
					
		set('member', $this->user->info($member_id));
		set('imageByTask', $imageByTask);
		set('fileByTask', $fileByTask);
		set('pinnedItems', $pinnedItems);
		return render('', 'chat/sidebar.php');
	}

	public function ajaxChatSend($id = 0, $task_id = null ){
		if($id && $this->user->id && $this->request('content') ){
			$this->db->table("chats");
			$this->db->insertArray(array(
				"sender_id" => $this->user->id,
				"receiver_id" => $id,
				"contents" => $this->request('content'),
				"task_id" => $task_id,
				"reply_id" => $this->request('replyID'),
				"created_by" => "NOW()",
			));
			$this->db->insert();
		}
	}
	
	public function returnMessageType($id){
		if(is_numeric($id)){
			$identifier = "id";
		}else{
			$identifier = "batch_id";
		}
		$this->db->query("SELECT * FROM chats WHERE ".$identifier." = ".$this->db->escape($id)." and deleted = 0 ORDER BY created_by ASC");
		$type = $this->db->getSingleRow();

		return $type;
	}

	public function returnMemberDetails($id){
		$this->db->query("SELECT * FROM members WHERE id = ".$id);
		$keys = $this->db->getSingleRow();

		return $keys;
	}

	public function returnAllImagesByTaskID($sender,$receiver,$task_id){
		$this->db->query(" SELECT * FROM chats WHERE ((sender_id = " . $this->db->escape($sender) . " AND receiver_id = " . $this->db->escape($receiver) . ") OR (sender_id = " . $this->db->escape($receiver) . " AND receiver_id = " . $this->db->escape($sender) . ")) AND task_id = " . $this->db->escape($task_id) . " AND deleted = 0 AND type LIKE '%image%' ORDER BY created_by DESC");
		$keys = $this->db->getRowList();

		return $keys;
	}

	public function returnAllFilesByTaskID($sender,$receiver,$task_id){
		$this->db->query(" SELECT * FROM chats WHERE ((sender_id = " . $this->db->escape($sender) . " AND receiver_id = " . $this->db->escape($receiver) . ") OR (sender_id = " . $this->db->escape($receiver) . " AND receiver_id = " . $this->db->escape($sender) . ")) AND task_id = " . $this->db->escape($task_id) . " AND deleted = 0 AND type LIKE '%file%' ORDER BY created_by DESC");
		$keys = $this->db->getRowList();

		foreach($keys as $i => $key){
			$this->db->query("SELECT id FROM chats WHERE batch_id = " . $this->db->escape($key['batch_id']));
			$go_to_id = $this->db->getRowList();

			$keys[$i]['go_to_id'] = $go_to_id[0]['id'];
 		}

		return $keys;
	}

	public function returnAllPinnedItemsByTaskID($sender,$receiver,$task_id){
		$this->db->query("SELECT pm.* FROM pinned_messages pm RIGHT JOIN chats c ON pm.message_id = c.id WHERE pinned_by = " . $this->db->escape($sender) . " AND pm.task_id = " . $this->db->escape($task_id) . " AND pin_status = '0' AND c.deleted = '0' AND ((c.sender_id = " . $this->db->escape($sender) . " AND c.receiver_id = " . $this->db->escape($receiver) . ") OR (c.sender_id = " . $this->db->escape($receiver) . " AND c.receiver_id = " . $this->db->escape($sender) . ")) ORDER BY pinned_time DESC");
		$keys = $this->db->getRowList();

		return $keys;
	}

	public function getPinnedMessageContent($id){
		$this->db->query("SELECT * FROM chats WHERE id = " . $this->db->escape($id) . " AND deleted = '0'");
		$keys = $this->db->getSingleRow();


		return $keys;
	}

	public function fileSize($bytes){

        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;

	}



	public function shipping_method(){
		return array(
			'0' => 'Standard Delivery Charges',
			'1' => 'Airport Cargo Complex',
			'2' => 'Airline Road',
			'3' => 'Army Camps',
			'4' => 'Jurong Island',
			'5' => 'All PSA Ports',
			'6' => 'Sembawang Shipyard',
		);
	}
	
	public function shipping_time(){
		return array(
			'0' => '9:30 am - 10:30 am',
			'1' => '10:30 am - 12:30 pm',
			'2' => '12:30 am - 3:30 pm',
			'3' => '3:30 pm - 6:30 pm',
		);
	}

    /**
     * @return array
     */
    public function get_widgets_data(){
		// Today's Task
        $this->db->query("SELECT * FROM tasks WHERE status IN ('in-progress') AND assigned_to = " . $this->db->escape($this->user->id) . " ORDER BY created_at DESC");
        $today_task = $this->db->getRowList();

        // Earning This Month
        $this->db->query("SELECT * FROM tasks WHERE assigned_to = " . $this->db->escape($this->user->id) . " AND status IN ('closed','disputed') AND MONTH(updated_at) = MONTH(CURDATE())");
        $earning_this_month = $this->db->getRowList();

        // Earning Last Month
        $this->db->query("SELECT * FROM tasks WHERE assigned_to = " . $this->db->escape($this->user->id) . " AND status IN ('closed','disputed') AND MONTH(updated_at) < MONTH(CURDATE())");
        $earning_last_month = $this->db->getRowList();

        // Spending This Month
        $this->db->query("SELECT * FROM tasks WHERE user_id = " . $this->db->escape($this->user->id) . " AND status IN ('closed','disputed') AND MONTH(updated_at) = MONTH(CURDATE())");
        $spending_this_month = $this->db->getRowList();

        // Spending Last Month
        $this->db->query("SELECT * FROM tasks WHERE user_id = " . $this->db->escape($this->user->id) . " AND status IN ('closed','disputed') AND MONTH(updated_at) < MONTH(CURDATE())");
        $spending_last_month = $this->db->getRowList();

        // Urgent Tasks
        $this->db->query("SELECT * FROM tasks WHERE status IN ('in-progress') AND urgent = 1 AND (user_id = " . $this->db->escape($this->user->id) . " OR assigned_to = " . $this->db->escape($this->user->id) . ") ORDER BY complete_by DESC");
        $urgent_task = $this->db->getRowList();

        // On-going Tasks
        $this->db->query("SELECT * FROM tasks WHERE status IN ('in-progress') AND (user_id = " . $this->db->escape($this->user->id) . " OR assigned_to = " . $this->db->escape($this->user->id) . ") ORDER BY complete_by DESC");
        $ongoing_task = $this->db->getRowList();

        // Task Created
        $this->db->query("SELECT * FROM tasks WHERE status IN ('publised','in-progress') AND user_id = " . $this->db->escape($this->user->id) . " ORDER BY created_at DESC");
        $created_task = $this->db->getRowList();

        // Previously Posted Task
        $this->db->query("SELECT * FROM tasks WHERE status IN ('completed','closed','disputed','cancelled') AND user_id = " . $this->db->escape($this->user->id) . " ORDER BY created_at DESC");
        $prev_post_task = $this->db->getRowList();

        // Task Pending Decision
        $this->db->query("SELECT a.*,b.created_at AS applied_date FROM tasks a, user_task b WHERE a.status IN ('published') AND b.user_id = " .$this->db->escape($this->user->id). " AND b.task_id = a.id ORDER BY b.created_at DESC");
		$pending_task = $this->db->getRowList();
		
		// New Job Recommendations
		if($this->user->info['skills']){
			$skills = explode(',', $this->user->info['skills']);
			$skills_qry = array();
			foreach($skills as $skill){
				$skills_qry[] = "(skills = '" . $skill . "' OR skills LIKE '" . $skill . ",%' OR skills LIKE '%," . $skill . ",%' OR skills LIKE '%," . $skill . "')";
			}
			$skills_qry = 'AND (' . implode(' OR ', $skills_qry) . ')';

			$this->db->query("SELECT * FROM jobs WHERE status IN ('published') " . $skills_qry . " ORDER BY RAND()");
			$new_job_recommendations = $this->db->getRowList();

		}else{
			$this->db->query("SELECT * FROM jobs WHERE status IN ('published') ORDER BY RAND()");
			$new_job_recommendations = $this->db->getRowList();
		}
		
		// Jobs Created
		$this->db->query("SELECT a.*, b.name AS state_name FROM jobs a, states b WHERE a.status IN ('published','completed') AND a.user_id = " . $this->db->escape($this->user->id) . " AND a.state = b.id ORDER BY a.created_at DESC");
        $job_created = $this->db->getRowList();

		// Just Posted
		$this->db->query("SELECT a.* FROM tasks a WHERE a.status IN ('published') AND a.user_id != " . $this->db->escape($this->user->id) . " ORDER BY a.created_at DESC LIMIT 12");
        $just_posted = $this->db->getRowList();

		// Tasks Match Skills
		$query  = "SELECT t.* FROM `tasks` t WHERE t.`status` IN ('published') AND t.`user_id` != " . $this->db->escape($this->user->id);
        if($this->user->info['skills']) {
            $skills = explode(',', $this->user->info['skills']);
            $query .= " AND (t.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

            $length = count($skills);
            foreach ($skills as $key => $skill) {
                $query .= " FIND_IN_SET({$skill}, `skills`) ";
                if ($key + 1 !== $length) $query .= " OR ";
            }
            $query .= " ))";
        }
        $query .= " ORDER BY t.`created_at` DESC LIMIT 12";

        $this->db->query($query);
        $matched_skills = $this->db->getRowList();

        return [$today_task, $earning_this_month, $earning_last_month, $spending_this_month, $spending_last_month,
                $urgent_task, $ongoing_task, $created_task, $prev_post_task, $pending_task, $new_job_recommendations,
                $job_created, $just_posted, $matched_skills];
	}
	
	public function analytics(){
        set_latest_url( request_uri() );
		$base_date = 'updated_at';
		$interval = $this->request('interval') ? $this->request('interval') : 'month';
		if(!in_array($interval, array('month', 'day'))) $interval = 'month';
		$day = $this->request('day') ? $this->request('day') : date('d');
		$month = $this->request('month') ? $this->request('month') : date('n');
		$year = $this->request('year') ? $this->request('year') : date('Y');
		$type = $this->request('type') ? $this->request('type') : 'seeker';
		if(!in_array($type, array('seeker', 'poster'))) $type = 'seeker';
		if($this->user->info['type'] == '1') $type = 'poster';

		$param = array();
		if($this->request('interval')) $param['interval'] = $interval;
		if($this->request('day')) $param['day'] = $day;
		if($this->request('month')) $param['month'] = $month;
		if($this->request('year')) $param['year'] = $year;
		if($this->request('type')) $param['type'] = $type;
		$paramUri = http_build_query($param);

		$labels = array();
		$monthList = array();
		$monthList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01 -3 month'));
		$monthList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01 -2 month'));
		$monthList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01 -1 month'));
		$monthList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01'));
		$monthList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01 +1 month'));
		$monthList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01 +2 month'));
		$monthList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01 +3 month'));

		$dayList = array();
		$dayList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($day, 2, '0', STR_PAD_LEFT) . ' -3 day'));
		$dayList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($day, 2, '0', STR_PAD_LEFT) . ' -2 day'));
		$dayList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($day, 2, '0', STR_PAD_LEFT) . ' -1 day'));
		$dayList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($day, 2, '0', STR_PAD_LEFT) . ''));
		$dayList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($day, 2, '0', STR_PAD_LEFT) . ' +1 day'));
		$dayList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($day, 2, '0', STR_PAD_LEFT) . ' +2 day'));
		$dayList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($day, 2, '0', STR_PAD_LEFT) . ' +3 day'));

		if($interval == 'month'){
			$hours = array();
			$hours_highest['value'] = 0;
			$hours_highest['date'] = 0;

			$earnings = array();
			$earnings_highest['value'] = 0;
			$earnings_highest['date'] = 0;

			foreach(range(1, 12) as $m){
				$labels[] = date('M', mktime(0, 0, 0, $m, 10));

				if($type == 'seeker'){
					$this->db->query("SELECT SUM(hours) AS total_hours, SUM(budget) AS total_earnings, SUM(CASE WHEN status = 'disputed' THEN (budget*(100-rejection_rate)/100) ELSE budget END) AS total_final FROM `tasks` WHERE assigned_to = " . $this->db->escape($this->user->id) . " AND status IN ('closed','disputed') AND MONTH(" . $base_date . ") = " . $this->db->escape($m) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " GROUP BY MONTH(" . $base_date . ")");
				}else if($type == 'poster'){
					$this->db->query("SELECT SUM(hours) AS total_hours, SUM(budget) AS total_earnings, SUM(CASE WHEN status = 'disputed' THEN (budget*(100-rejection_rate)/100) ELSE budget END) AS total_final FROM `tasks` WHERE user_id = " . $this->db->escape($this->user->id) . " AND status IN ('closed','disputed') AND MONTH(" . $base_date . ") = " . $this->db->escape($m) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " GROUP BY MONTH(" . $base_date . ")");
				}
				$row = $this->db->getSingleRow();

				$hours[$m] = !empty($row['total_hours']) ? $row['total_hours'] : 0;
				$earnings[$m] = !empty($row['total_final']) ? $row['total_final'] : 0;
				if($hours[$m] > $hours_highest['value']){
					$hours_highest['value'] = $hours[$m];
					$hours_highest['date'] = $year . '-' . $m . '-01';
				}

				if($earnings[$m] > $earnings_highest['value']){
					$earnings_highest['value'] = $earnings[$m];
					$earnings_highest['date'] = $year . '-' . $m . '-01';
				}
			}

			set('param', $param);
			set('hours', $hours);
			set('hours_highest', $hours_highest);
			set('earnings', $earnings);
			set('earnings_highest', $earnings_highest);
		}

		if($interval == 'day'){
			$hours = array();
			$hours_highest['value'] = 0;
			$hours_highest['date'] = 0;

			$earnings = array();
			$earnings_highest['value'] = 0;
			$earnings_highest['date'] = 0;

			foreach(range(1, date('t', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01'))) as $d){
				$labels[] = $d;

				if($type == 'seeker'){
					$this->db->query("SELECT SUM(hours) AS total_hours, SUM(budget) AS total_earnings, SUM(CASE WHEN status = 'disputed' THEN (budget*(100-rejection_rate)/100) ELSE budget END) AS total_final FROM `tasks` WHERE assigned_to = " . $this->db->escape($this->user->id) . " AND status IN ('closed','disputed') AND DAY(" . $base_date . ") = " . $this->db->escape($d) . " AND DAY(" . $base_date . ") = " . $this->db->escape($d) . " AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " GROUP BY DAY(" . $base_date . ")");
				}else if($type == 'poster'){
					
					$this->db->query("SELECT SUM(hours) AS total_hours, SUM(budget) AS total_earnings, SUM(CASE WHEN status = 'disputed' THEN (budget*(100-rejection_rate)/100) ELSE budget END) AS total_final FROM `tasks` WHERE user_id = " . $this->db->escape($this->user->id) . " AND status IN ('closed','disputed') AND DAY(" . $base_date . ") = " . $this->db->escape($d) . " AND DAY(" . $base_date . ") = " . $this->db->escape($d) . " AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " GROUP BY DAY(" . $base_date . ")");
				}

				$row = $this->db->getSingleRow();

				$hours[$d] = !empty($row['total_hours']) ? $row['total_hours'] : 0;
				$earnings[$d] = !empty($row['total_final']) ? $row['total_final'] : 0;

				if($hours[$d] > $hours_highest['value']){
					$hours_highest['value'] = $hours[$d];
					$hours_highest['date'] = $year . '-' . $month . '-' . str_pad($d, 2, '0', STR_PAD_LEFT);
				}

				if($earnings[$d] > $earnings_highest['value']){
					$earnings_highest['value'] = $earnings[$d];
					$earnings_highest['date'] = $year . '-' . $month . '-' . str_pad($d, 2, '0', STR_PAD_LEFT);
				}
			}

			set('hours', $hours);
			set('hours_highest', $hours_highest);
			set('earnings', $earnings);
			set('earnings_highest', $earnings_highest);
		}

		if($interval == 'month'){
			$aggregated_data['hours'] = $hours[$month];
			$aggregated_data['earnings'] = $earnings[$month];

			if($type == 'seeker'){
				$this->db->query("SELECT * FROM `tasks` WHERE assigned_to = " . $this->db->escape($this->user->id) . " AND status IN ('completed','published','in-progress','closed','disputed') AND MONTH(assigned) = " . $this->db->escape($month) . " AND YEAR(assigned) = " . $this->db->escape($year) . " ORDER BY assigned");
			}else if($type == 'poster'){
				$this->db->query("SELECT * FROM `tasks` WHERE user_id = " . $this->db->escape($this->user->id) . " AND status IN ('completed','published','in-progress','closed','disputed') AND MONTH(created_at) = " . $this->db->escape($month) . " AND YEAR(created_at) = " . $this->db->escape($year) . " ORDER BY created_at");
			}
			
			$tasks = $this->db->getRowList();

			if($type == 'seeker'){
				$this->db->query("SELECT * FROM `tasks` WHERE assigned_to = " . $this->db->escape($this->user->id) . " AND status IN ('closed','disputed') AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " ORDER BY " . $base_date . "");
			}else if($type == 'poster'){
				$this->db->query("SELECT * FROM `tasks` WHERE user_id = " . $this->db->escape($this->user->id) . " AND status IN ('closed','disputed') AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " ORDER BY " . $base_date . "");
			}
			
			$tasks_completed = $this->db->getRowList();

		}else{

			if($type == 'seeker'){
				$this->db->query("SELECT SUM(hours) AS total_hours, SUM(budget) AS total_earnings, SUM(CASE WHEN status = 'disputed' THEN (budget*(100-rejection_rate)/100) ELSE budget END) AS total_final FROM `tasks` WHERE assigned_to = " . $this->db->escape($this->user->id) . " AND status IN ('closed','disputed') AND DAY(" . $base_date . ") = " . $this->db->escape($day) . " AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " GROUP BY MONTH(" . $base_date . ")");
			}else if($type == 'poster'){
				$this->db->query("SELECT SUM(hours) AS total_hours, SUM(budget) AS total_earnings, SUM(CASE WHEN status = 'disputed' THEN (budget*(100-rejection_rate)/100) ELSE budget END) AS total_final FROM `tasks` WHERE user_id = " . $this->db->escape($this->user->id) . " AND status IN ('closed','disputed') AND DAY(" . $base_date . ") = " . $this->db->escape($day) . " AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " GROUP BY MONTH(" . $base_date . ")");
			}

			$aggregated = $this->db->getSingleRow();

			$aggregated_data['hours'] = !empty($aggregated['total_hours']) ? $aggregated['total_hours'] : 0;
			$aggregated_data['earnings'] = !empty($aggregated['total_final']) ? $aggregated['total_final'] : 0;

			if($type == 'seeker'){
				$this->db->query("SELECT * FROM `tasks` WHERE assigned_to = " . $this->db->escape($this->user->id) . " AND status IN ('completed','published','in-progress','closed','disputed') AND DAY(assigned) = " . $this->db->escape($day) . " AND MONTH(assigned) = " . $this->db->escape($month) . " AND YEAR(assigned) = " . $this->db->escape($year) . " ORDER BY assigned");
			}else if($type == 'poster'){
				$this->db->query("SELECT * FROM `tasks` WHERE user_id = " . $this->db->escape($this->user->id) . " AND status IN ('completed','published','in-progress','closed','disputed') AND DAY(created_at) = " . $this->db->escape($day) . " AND MONTH(created_at) = " . $this->db->escape($month) . " AND YEAR(created_at) = " . $this->db->escape($year) . " ORDER BY created_at");
			}
			
			$tasks = $this->db->getRowList();

			if($type == 'seeker'){
				$this->db->query("SELECT * FROM `tasks` WHERE assigned_to = " . $this->db->escape($this->user->id) . " AND status IN ('closed','disputed') AND DAY(" . $base_date . ") = " . $this->db->escape($day) . " AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " ORDER BY " . $base_date . "");
			}else if($type == 'poster'){
				$this->db->query("SELECT * FROM `tasks` WHERE user_id = " . $this->db->escape($this->user->id) . " AND status IN ('closed','disputed') AND DAY(" . $base_date . ") = " . $this->db->escape($day) . " AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " ORDER BY " . $base_date . "");
			}
			
			$tasks_completed = $this->db->getRowList();
		}
		
		set('page_title', 'Analytics');
		set('param', $param);
		set('paramUri', $paramUri);
		set('interval', $interval);
		set('year', $year);
		set('month', $month);
		set('labels', $labels);
		set('monthList', $monthList);
		set('dayList', $dayList);
		set('aggregated', $aggregated_data);
		set('tasks', $tasks);
		set('tasks_completed', $tasks_completed);
		set('type', $type);
		return render('analytics.php', 'layout/app.html.php');
	}

	public function javascript($file = ''){
		if($file){
			if($file == 'jquery.validate.js'){
				$content = file_get_contents('assets/js/plugins/jquery.validate.js');
				$content = str_replace('This field is required.', lang('js_field_is_required'), $content);
				$content = str_replace('Please fix this field.', lang('js_please_fix_this_field'), $content);
				$content = str_replace('Please enter a valid email address.', lang('js_please_enter_a_valid_email'), $content);
				$content = str_replace('Please enter a valid URL.', lang('js_please_enter_a_valid_url'), $content);
				$content = str_replace('Please enter a valid date.', lang('js_please_enter_a_valid_date'), $content);
				$content = str_replace('Please enter a valid number.', lang('js_please_enter_a_valid_number'), $content);
				$content = str_replace('Please enter only digits.', lang('js_please_only_digits'), $content);
				
				echo $content;
			}

			if($file == 'script.js'){
				$content = file_get_contents('assets/js/script.js');
				$content = str_replace('rootPath = rootPath.join(\'assets\');', 'rootPath = \'' . url_for('/') . '\'', $content);
				
				$content = str_replace('Town/City', lang('sign_up_select_city'), $content);
				
				echo $content;
			}

			if($file == 'uploader.js'){
				$content = file_get_contents('assets/js/uploader.js');		

				$docs_max_size = $this->settings()['docs_max_size'] ? $this->settings()['docs_max_size'] * 1048576 : 0;
				$avatar_max_size = $this->settings()['avatar_max_size'] ? $this->settings()['avatar_max_size'] * 1048576 : 0;
				$resume_max_file = $this->settings()['resume_max_file'] ? $this->settings()['resume_max_file'] : 0;
				$cover_letter_max_file = $this->settings()['cover_letter_max_file'] ? $this->settings()['cover_letter_max_file'] : 0;
				$cert_max_file = $this->settings()['cert_max_file'] ? $this->settings()['cert_max_file'] : 0;

				$content = str_replace('docs_max_size = 0', 'docs_max_size = ' . $docs_max_size, $content);
				$content = str_replace('File too large. File must be less than docs_max_size.', sprintf(lang('sys_upload_max_size_error'), $this->settings()['docs_max_size'] . 'MB'), $content);
				
				$content = str_replace('avatar_max_size = 0', 'avatar_max_size = ' . $avatar_max_size, $content);
				$content = str_replace('File too large. File must be less than avatar_max_size.', sprintf(lang('sys_upload_max_size_error'), $this->settings()['avatar_max_size'] . 'MB'), $content);

				$content = str_replace('resume_max_file = 0', 'resume_max_file = ' . $resume_max_file, $content);
				$content = str_replace('cover_letter_max_file = 0', 'cover_letter_max_file = ' . $cover_letter_max_file, $content);
				$content = str_replace('cert_max_file = 0', 'cert_max_file = ' . $cert_max_file, $content);

				$content = str_replace('Maximum files allowed for resume has reach limit.', sprintf(lang('sys_upload_max_file_error'), $resume_max_file), $content);
				$content = str_replace('Maximum files allowed for cover_letter has reach limit.', sprintf(lang('sys_upload_max_file_error'), $cover_letter_max_file), $content);
				$content = str_replace('Maximum files allowed for cert has reach limit.', sprintf(lang('sys_upload_max_file_error'), $cert_max_file), $content);
				
				echo $content;
			}
		}
	}	
}
?>