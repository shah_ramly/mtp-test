<?php

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../helpers.php';

use Carbon\Carbon;

class Search
{
    protected $db;
    protected $user;
    protected $date;
    protected $google_api_key;

    /**
     * @var $db db
     * @var $user user
     */
    public function __construct($db, $user){

        if( isset($_SESSION['member_id']) && !empty($_SESSION['member_id']) ){
            $_SESSION[WEBSITE_PREFIX.'USER_ID'] = $_SESSION['member_id'];
        }

        $this->db = $db;
        $this->user = $user;
        date_default_timezone_set('Asia/Kuala_Lumpur');

        if( !isset($_SESSION[WEBSITE_PREFIX.'LANGUAGE']) || $_SESSION[WEBSITE_PREFIX.'LANGUAGE'] == 'EN' ){
            $locale = ['English', 'en_GB'];
        }else{
            $locale = ['Malay', 'ms_MY'];
        }

        setLocale(LC_TIME, $locale[1]);
        Carbon::setLocale($locale[1]);

        $this->date = Carbon::today();
        $this->google_api_key = "AIzaSyCKkk8Hj4PMcGn7bheNToZyDVJixuLsVL8";
    }

    private function is_ajax(){
        return 'xmlhttprequest' === strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '' );
    }

    public function save_location(){
        $lat_lng = sanitize( request('location') );
        $_SESSION['current_user_location'] = implode(',', $lat_lng);
    }

    private function get_user_lat_lng($user_id = null){
        if( !isset($_SESSION['current_user_location_by_address']) || empty($_SESSION['current_user_location_by_address']) ) {
            if( isset($_SESSION['WEB_USER_ID']) ) {
                $user_id = !is_null($user_id) ? sanitize($user_id, 'int') : $this->user->info['id'];

                $this->db->query("SELECT `address1`, `address2`, `zip`, 
                        (SELECT `name` FROM `cities` WHERE `id` = m.`city` LIMIT 1) AS `city`, 
                        (SELECT `name` FROM `states` WHERE `id` = m.`state` LIMIT 1) AS `state`, 
                        (SELECT `name` FROM `countries` WHERE `id` = m.`country` LIMIT 1) AS `country` 
                        FROM `members` m WHERE `id` = '{$user_id}' LIMIT 1");
                $address      = $this->db->getSingleRow();
                $full_address = '';

                foreach ($address as $line) {
                    if (!is_null($line) || $line !== '') {
                        $full_address .= $line . ',';
                    }
                }

                $full_address = urlencode(trim($full_address, ','));

                // USE GOOGLE MAP API TO GET LAT/LNG
                $google_api_url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input={$full_address}&inputtype=textquery&fields=formatted_address,name,geometry&key={$this->google_api_key}";

                $location = json_decode(file_get_contents($google_api_url), true);
                if( isset($location['candidates']) ) {
                    $location = $location['candidates'];
                    if( isset($location[0]) )
                        $location = $location[0]['geometry']['location'];
                }else{
                    $location = [];
                }
                $_SESSION['current_user_location_by_address'] = $location;
            }else{
                return null;
            }
        }

        return isset($_SESSION['current_user_location_by_address']) ? $_SESSION['current_user_location_by_address'] : null;
    }

    private function get_preference_locations(){
        if( isset($_SESSION['WEB_USER_ID']) ) {
            $physical_location = $this->user->info['preference']['p_work_location'];
            $remote_location   = $this->user->info['preference']['rm_work_location'];
            $physical          = [];
            $remote            = [];
            if ($physical_location === 'same_country') {
                $physical['location'] = $physical_location;
                $physical['country']  = $this->user->info['preference']['p_country'];
                $physical['state']    = $this->user->info['preference']['p_state'];
            } elseif ($physical_location === 'near') {
                $physical['location'] = $physical_location;
                $physical['radius']   = (int)str_replace(['within', 'km', ' '], '', strtolower($this->user->info['preference']['p_radius']));
            } else {
                $physical['location'] = $physical_location;
            }

            if ($remote_location === 'same_country') {
                $remote['location'] = $remote_location;
                $remote['country']  = $this->user->info['preference']['rm_country'];
                $remote['state']    = $this->user->info['preference']['rm_state'];
            } else {
                $remote['location'] = $remote_location;
            }
        }else{
            $physical['location'] = 'near';
            $physical['radius'] = 20;
            $remote['location'] = 'anywhere';
        }

        return ['physical' => $physical, 'remote' => $remote];
    }

    public function get_nearby_names($user_id = null, $lat_lng = null, $radius = null){
        $location_pref = $this->get_preference_locations();
        $signature     = base64_encode(implode($location_pref['physical']));

        if( !isset($_SESSION['search_physical']) || empty($_SESSION['search_physical']) || ($_SESSION['search_physical']['signature'] !== base64_decode($signature)) ) {

            if( !is_null($radius) || (isset($location_pref['physical']['location']) && isset($location_pref['physical']['radius']) && $location_pref['physical']['location'] === 'near') ) {
                $radius   = !is_null($radius) ? $radius : (isset($location_pref['physical']['radius']) ? $location_pref['physical']['radius'] : 5);
                $radius   *= 1000;

                if( is_null($lat_lng) ) {
                    $location = $this->get_user_lat_lng($user_id);
                }else{
                    $location = $lat_lng;
                }

                if( !is_null($location) ) {
                    if (is_array($location))
                        $location = implode(',', array_values($location));

                    // location = 3.1558712,101.7585521, $radius = 5000
                    $google_api_url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={$location}&radius={$radius}&key={$this->google_api_key}";
                    $places         = json_decode(file_get_contents($google_api_url), true);
                    $places         = $places['results'];
                    $places         = array_column($places, 'vicinity');
                    $all_places     = [];

                    foreach ($places as $place) {
                        $temp = explode(',', $place);
                        //cleaning up parts of the address that we don't need
                        if (count($temp) >= 3 && count($temp) < 5) {
                            $temp = array_slice($temp, 1);
                        } elseif (count($temp) >= 5) {
                            $temp = array_slice($temp, 2);
                        }

                        array_push($all_places, array_map('trim', $temp));
                    }

                    //Removing and cleaning up
                    $all_places = call_user_func_array('array_merge', $all_places);
                    $all_places = array_filter($all_places, function ($item) { return !is_numeric(str_replace('-', '', $item)) && strlen($item) > 5; });
                    $all_places = array_values(array_unique($all_places));
                    $final_places = [];
                    array_map(function($place)use(&$final_places){
                        $place = str_replace('Johor Bahru', 'Johor', $place);
                        $place = str_replace(['Wilayah Persekutuan Kuala Lumpur', 'Federal Territory of Kuala Lumpur'], 'Kuala Lumpur', $place);
                        $place = str_replace(['Pulau Penang', 'Pulau Pinang', 'Pinang'], 'Penang', $place);
                        array_push($final_places, $place);
                    }, $all_places);
                    $all_places = $final_places;
                    $_SESSION['search_physical']['signature'] = $signature;
                    $_SESSION['search_physical']['places']    = $all_places;
                }else{
                    return $location_pref;
                }
            }else{
                return $location_pref;
            }
        }

        return $_SESSION['search_physical']['places'];
    }

    public function get_nearby($lat = null, $lng = null, $radius = 5){
        if( empty($lat) && empty($lng) && $this->user->logged ){
            $city  = $this->user->info['city'];
            $state = $this->user->info['state'];
            if( !empty($city) ) {
                $this->db->query("SELECT `longitude`, `latitude` FROM `cities` WHERE `id` = '{$city}' LIMIT 1");
                if($city_lat_lng = $this->db->getSingleRow()){
                    @list($lng, $lat) = $city_lat_lng;
                    $location = "POINT({$lng}, {$lat})";
                }
            }elseif ( !empty($state) ){
                $points = [];
                $state = $this->user->info['state'];
                $this->db->query("SELECT `longitude`, `latitude` FROM `cities` WHERE `state_id` = '{$state}'");
                if($cities = $this->db->getRowList()){
                    foreach ($cities as $city){
                        array_push($points, "POINT({$city['longitude']},{$city['latitude']})");
                    }
                    $points = implode(',', $points);
                    $location = "MULTIPOINT({$points})";
                }else{
                    return [];
                }
            }
        }elseif( !empty($lat) && !empty($lng) ){
            $location = "POINT({$lng}, {$lat})";
        }else{
            return [];
        }

        $query = "
                    SELECT c.`name` AS `city`, c.`id` AS `city_id`, s.`name` AS `state`, s.`id` AS `state_id`,
                    ST_Distance_Sphere( {$location}, POINT(c.`longitude`, c.`latitude`) )/1000 AS `distance`
                    FROM `cities` c
                    INNER JOIN `states` s ON c.`state_id` = s.`id`
                    WHERE c.`country_id` = 132 HAVING distance <= {$radius}
                    ORDER BY `distance` ASC
                    ";

        $this->db->query($query);
        return $this->db->getRowList();
    }

    public function save(){
        $params = sanitize($_POST);
        $search_name = $params['search_name'];
        unset($params['search_name']);
        $search_params = $params;
        if( $search_name === '' ) $search_name = preg_replace("/[^0-9a-zA-Z]/", '-', implode('_', $search_params));
        $this->db->table('search');
        $this->db->insertArray([
            'user_id' => $this->user->info['id'],
            'name'    => $search_name,
            'params'  => json_encode($search_params)
        ]);
        $this->db->insert();

        $this->db->query("SELECT * FROM `search` WHERE `id` = ".$this->db->insertid()." LIMIT 1");
        $saved_search = $this->db->getSingleRow();
        $setup_search = $this->setup_result([$saved_search])[$search_name];

        return json([
            'message' => sprintf( lang('your_search_has_been_saved_successfully'), "(<b>{$search_name}</b>)" ), //Your search (<b>{$search_name}</b>) has been saved successfully
            'search'  => json_encode($setup_search),
        ]);
    }

    public function delete(){
        $params = sanitize($_POST);
        $search_id = $params['search_id'];
        $search_name = $params['search_name'];

        $this->db->table('search');
        $this->db->whereArray([
            'id'    => $search_id,
            'user_id' => $this->user->info['id'],
        ]);
        $this->db->delete();

        return json([
            'status'  => 'success',
            'message' => sprintf( lang('your_search_has_been_deleted_successfully'), "(<b>{$search_name}</b>)" ), //Your search (<b>{$search_name}</b>) has been saved successfully
        ]);
    }

    public function get_search(){
        $user_id = $this->user->info['id'];
        $this->db->query("SELECT * FROM `search` WHERE `user_id` = " . $this->db->escape($user_id));
        return $this->setup_result($this->db->getRowList());
    }

    private function setup_result(array $result = []){
        if( empty($result) ) return [];
        $search_result = [];
        foreach ($result as $search){
            $scope = json_decode($search['params'], true)['scope'];
            $query_str = http_build_query(array_filter(json_decode($search['params'], true)));
            $url = option('site_uri') . url_for('/workspace/search') . '?' . $query_str;
            $search_result[$search['name']] = ['id' => $search['id'], 'name' => $search['name'], 'scope' => $scope, 'url' => $url];
        }

        return $search_result;
    }

    public function public_search(){
        if( is_null(request('scope')) || request('scope') === 'task' || request('scope') === 'job' ) {
            if( request('scope') === 'job' ) $_REQUEST['scope'] = 'task';
            $result = $this->get_jobs_tasks_list();

            set('categories', is_null($result['filters']['type']) || in_array('task', $result['filters']['type']) ? (new TaskCentre($this->db, $this->user))->getCategories([], 'task_category') : (new TaskCentre($this->db, $this->user))->getCategories());
            set('states', $result['states']);
            set('cities', $result['cities']);
            set('listing', $result['listing']);
            set('current_page', $result['current_page']);
            set('search_term', $result['search_term']);
            set('filters', $result['filters']);
            set('keywords', $result['keywords']);
            set('header_title', 'Search');
            set('scope', request('scope') ?? 'task');
            set('more_tasks', $result['there_is_more']);
            set('task_categories', (new TaskCentre($this->db, $this->user))->getCategories([], 'task_category'));
            set('job_categories', (new TaskCentre($this->db, $this->user))->getCategories());

            $_SESSION[WEBSITE_PREFIX . 'SEARCH_REDIRECT'] = base64_encode($_SERVER['REQUEST_URI']);

            if ($this->is_ajax()) {
                return json(['more' => $result['there_is_more'], 'html' => partial('partial/public-search/listing-results.html.php', ['listing' => $result['listing']]), 'keywords' => $result['keywords']]);
            }

            return render('task_listing_public.php', 'layout/public_default.php');
        }elseif ( request('scope') === 'talent' ){
            $result = $this->get_talents_list();

            set('categories', (new TaskCentre($this->db, $this->user))->getCategories());
            set('states', $result['states']);
            set('cities', $result['cities']);
            set('listing', $result['listing']);
            set('current_page', $result['current_page']);
            set('search_term', $result['search_term']);
            set('filters', $result['filters']);
            set('keywords', $result['keywords']);
            set('header_title', 'Search');
            set('scope', 'talent');
            set('more_talents', $result['there_is_more']);

            $_SESSION[WEBSITE_PREFIX . 'SEARCH_REDIRECT'] = base64_encode($_SERVER['REQUEST_URI']);

            if($this->is_ajax()){
                return json(['more' => $result['there_is_more'], 'html' => partial('partial/public-search/talent-listing-results.html.php', ['listing' => $result['listing']]), 'keywords' => $result['keywords']]);
            }

            return render('task_listing_public.php', 'layout/public_default.php');
        }
    }

    public function task_details_public(){
        return render('task_details_public.php', 'layout/public_default.php');
    }

    private function get_jobs_tasks_list(){
        $per_page = 16;
        $filters = [];
        $published_jobs_count  = $published_tasks_count = 0;
        $there_is_more = false;

        $search_term = request('q') ? sanitize(request('q')) : null;

        if(!is_null($search_term))
            $search_term = array_values(array_filter(explode(' ', preg_replace('#[^a-zA-Z0-9\s]#i', '', $search_term))));

        $filters = [
            'type' => request('type') ? explode(',', sanitize(request('type'))) : ['all'],
            'category' => request('category') && request('category') != 0 ? sanitize(request('category')): null,
            'state' => request('state') ? count(explode(',', sanitize(request('state')))) > 1 ? explode(',', sanitize(request('state'))) : sanitize(request('state')) : null,
            'city' => request('city') ? count(explode(',', sanitize(request('city')))) > 1 ? explode(',', sanitize(request('city'))) : sanitize(request('city')) : null,
            'task_type' => request('task_type') ? count(explode(',', sanitize(request('task_type')))) > 1 ? explode(',', sanitize(request('task_type'))) : sanitize(request('task_type')) : null,
            'task_budget' => request('task_budget') ? count(explode(',', sanitize(request('task_budget')))) > 1 ? explode(',', sanitize(request('task_budget'))) : sanitize(request('task_budget')) : null,
            'date_type' => request('date_type') ? count(explode(',', sanitize(request('date_type')))) > 1 ? explode(',', sanitize(request('date_type'))) : sanitize(request('date_type')) : null,
            'date_period' => request('date_period') ? count(explode(',', sanitize(request('date_period')))) > 1 ? explode(',', sanitize(request('date_period'))) : sanitize(request('date_period')) : null,
            'from' => request('from') ? count(explode(',', sanitize(request('from')))) > 1 ? explode(',', sanitize(request('from'))) : sanitize(request('from')) : null,
            'to' => request('to') ? count(explode(',', sanitize(request('to')))) > 1 ? explode(',', sanitize(request('to'))) : sanitize(request('to')) : null,
            'skills' => request('skills') ? count(explode(',', sanitize(request('skills')))) > 1 ? explode(',', sanitize(request('skills'))) : sanitize(request('skills')) : null,
            'location_by' => request('location_by') ? sanitize(request('location_by')) : null,
            'within' => request('within') ? sanitize(request('within')) : null,
        ];

        if( isset($filters['location_by']) && $filters['location_by'] === 'locbystate' ) {
            if (isset($filters['state'])) $filters['state_name'] = (new TaskCentre($this->db, $this->user))->getStateName($filters['state']);
            if (isset($filters['city'])) $filters['city_name'] = (new TaskCentre($this->db, $this->user))->getCityName($filters['city']);
        }elseif ( isset($filters['location_by']) && $filters['location_by'] === 'locbynearest' ){
            if( isset($filters['within']) && $filters['within'] !== '' ){
                $within        = preg_replace("/[^0-9]/", "", $filters['within']); // strip all non-numeric from the passed value
                $lat_lng       = isset($_SESSION['current_user_location']) ? $_SESSION['current_user_location'] : null;

                @list($lat, $lng) = !is_array($lat_lng) ? explode(',', $lat_lng, 2) : $lat_lng;

                // Use cities table to get nearby places
                $nearby_places = (new Search($this->db, $this->user))->get_nearby($lat, $lng, $within);
                $filters['nearby_places'] = !empty($nearby_places) ? array_column($nearby_places, 'city') : null;
                if(!empty( $nearby_places )) {
                    $filters['city']       = array_column($nearby_places, 'city_id');
                }else{
                    // if cities table has no results, then use google maps api to get nearby places
                    $nearby_places = (new Search($this->db, $this->user))->get_nearby_names(null, $lat_lng, $within);
                    if( !empty($nearby_places) )
                        $filters['nearby_places'] = $nearby_places;
                }
            }
        }

        if( isset($filters['state']) ) $filters['state_name'] = (new TaskCentre($this->db, $this->user))->getStateName($filters['state']);
        if( isset($filters['city']) ) $filters['city_name'] = (new TaskCentre($this->db, $this->user))->getCityName($filters['city']);
        $filters = array_filter($filters);

        $page = params('page') ? (int)params('page') : 1;
        $offset = ($page - 1) * $per_page;

        if(is_null($filters['type']) || in_array('all', $filters['type'])) {
            $total_count = (int)(new Task($this->db, $this->user))->get_all_published_tasks_jobs_count($search_term, $filters);
        }else{
            if( in_array('job', $filters['type']) ){
                if( isset($filters['date_type']) && $filters['date_type'] === 'startDate' ){
                    $filters['date_type'] = 'closeDate';
                }
                $total_count = (int) Job::init($this->db, $this->user)->get_all_published_jobs_count($search_term, $filters);
            }
            if( in_array('task', $filters['type']) ){
                if( isset($filters['date_type']) && $filters['date_type'] === 'closeDate' ){
                    $filters['date_type'] = 'startDate';
                }
                $total_count = (int)(new Task($this->db, $this->user))->get_all_published_tasks_count($search_term, $filters);
            }
            //$total_count = $published_tasks_count + $published_jobs_count;
        }

        $total_pages = ceil($total_count/$per_page);

        if( ($per_page < $total_count) && ($offset+$per_page < $total_count) ){
            $there_is_more = true;
        }else{
            $there_is_more = false;
        }

        $jobs = [];
        $tasks = [];

        if(is_null($filters['type']) || in_array('all', $filters['type'])) {
            $all = (new Task($this->db, $this->user))->get_published_tasks_jobs($search_term, $filters, $offset, $per_page);
            $counter = 0;
            foreach ($all as $key => $task) {
                if(in_array($task['the_type'], ['internal_task', 'external_task'])) {
                    if( $task['the_type'] === 'internal_task' ) {
                        $task                           = Task::init($this->db, $this->user)->get_task_by_id($task['task_id']);
                        $tasks[$counter]                    = $task;
                        $tasks[$counter]['type']            = 'internal_task';
                        $tasks[$counter]['country']         = (new TaskCentre($this->db, $this->user))->getCountryName($task['country']);
                        $tasks[$counter]['state']           = (new TaskCentre($this->db, $this->user))->getStateName($task['state']);
                        $tasks[$counter]['published_count'] = (new Task($this->db, $this->user))->get_published_tasks_count($task['user_id']);
                        $tasks[$counter]['rating']          = (new TaskCentre($this->db, $this->user))->get_ratings($task['user_id']);
                        $tasks[$counter]['questions']       = (new Task($this->db, $this->user))->questions($task['task_id']);
                        $counter++;
                    }else{
                        $task                           = Task::init($this->db, $this->user)->get_external_task_by_id($task['task_id']);
                        @list($state, $country)         = explode(', ', $task['state_country'], 2);
                        $tasks[$counter]                    = $task;
                        $tasks[$counter]['type']            = 'external_task';
                        $tasks[$counter]['country']         = $country;
                        $tasks[$counter]['state']           = $state;
                        $counter++;
                    }
                }else{
                    if( $task['the_type'] === 'internal_job' ){
                        $job = (new Job($this->db, $this->user))->get_job_by_id($task['task_id']);

                        unset($job['password']);
                        $jobs[$counter]                    = $job;
                        $jobs[$counter]['type']            = 'internal_job';
                        $jobs[$counter]['country']         = (new TaskCentre($this->db, $this->user))->getCountryName($job['country']);
                        $jobs[$counter]['state']           = (new TaskCentre($this->db, $this->user))->getStateName($job['state']);
                        $jobs[$counter]['job_country']         = (new TaskCentre($this->db, $this->user))->getCountryName($job['job_country']);
                        $jobs[$counter]['job_state']           = (new TaskCentre($this->db, $this->user))->getStateName($job['job_state']);
                        $jobs[$counter]['published_count'] = (new Job($this->db, $this->user))->get_published_jobs_count($job['user_id']);
                        $jobs[$counter]['rating']          = (new TaskCentre($this->db, $this->user))->get_ratings($job['user_id']);
                        $jobs[$counter]['questions']       = (new Job($this->db, $this->user))->questions($job['job_id']);
                        $jobs[$counter]['salary_range_max']   = number_format($job['salary_range_max'], 2);
                        $jobs[$counter]['salary_range_min']   = number_format($job['salary_range_min'], 2);
                        $counter++;
                    }else{
                        $job = (new Job($this->db, $this->user))->get_external_job_by_id($task['task_id']);

                        $jobs[$counter]            = $job;
                        $jobs[$counter]['type']    = 'external_job';
                        $jobs[$counter]['job_country'] = (new TaskCentre($this->db, $this->user))->getCountryName($job['job_country']);
                        $jobs[$counter]['job_state']   = $job['state'];
                        $jobs[$counter]['salary_range_max']   = number_format($job['salary_range_max'], 2);
                        $jobs[$counter]['salary_range_min']   = number_format($job['salary_range_min'], 2);
                        $counter++;
                    }

                }
            }

        }else {

            if (in_array('task', $filters['type'])) {
                //$tasks = (new Task($this->db, $this->user))->get_published_tasks($search_term, $filters, $offset, $per_page);
                $tasks = (new Task($this->db, $this->user))->get_published_tasks($search_term, $filters, $offset, $per_page);
                $internal_tasks = array_column(array_filter($tasks, function ($task) { return $task['the_type'] === 'internal'; }), 'task_id');
                $external_tasks = array_column(array_filter($tasks, function ($task) { return $task['the_type'] === 'external'; }), 'task_id');
                $int_tasks = Task::init($this->db, $this->user)->get_tasks_by_id($internal_tasks);
                $ext_tasks = Task::init($this->db, $this->user)->get_external_tasks_by_id($external_tasks);
                $tasks = [];
                $all = array_merge($ext_tasks, $int_tasks);
                /*usort($all, function($first, $second){
                    return Carbon::parse($first['created_at'])->equalTo(Carbon::parse($second['created_at'])) ? 0 :
                        (Carbon::parse($first['created_at'])->greaterThan(Carbon::parse($second['created_at'])) ? -1 : 1);
                });*/
                usort($all, function($first, $second){
                    return $first['priority'] == $second['priority'] ? 0 :
                        ($first['priority'] < $second['priority'] ? -1 : 1);
                });
                $counter = 0;
                foreach ($all as $key => $task) {
                    if( !isset($task['source']) ) {
                        $task = Task::init($this->db, $this->user)->get_task_by_id($task['task_id']);
                        unset($task['password']);
                        $tasks[$counter] = $task;
                        $tasks[$counter]['type']            = 'internal_task';
                        $tasks[$counter]['country']         = (new TaskCentre($this->db, $this->user))->getCountryName($task['country']);
                        $tasks[$counter]['state']           = (new TaskCentre($this->db, $this->user))->getStateName($task['state']);
                        $tasks[$counter]['published_count'] = (new Task($this->db, $this->user))->get_published_tasks_count($task['user_id']);
                        $tasks[$counter]['rating']          = (new TaskCentre($this->db, $this->user))->get_ratings($task['user_id']);
                        $tasks[$counter]['questions']       = (new Task($this->db, $this->user))->questions($task['task_id']);
                        $counter++;
                    }else{
                        $task = Task::init($this->db, $this->user)->get_external_task_by_id($task['task_id']);
                        @list($state, $country) = explode(', ', $task['state_country'], 2);
                        $tasks[$counter] = $task;
                        $tasks[$counter]['type']    = 'external_task';
                        $tasks[$counter]['country'] = $country;
                        $tasks[$counter]['state']   = $state;
                        $counter++;
                    }
                }

            }

            if (in_array('job', $filters['type'])) {

                $jobs = (new Job($this->db, $this->user))->get_published_jobs($search_term, $filters, $offset, $per_page);
                $internal_jobs = array_column(array_filter($jobs, function ($job) { return $job['the_type'] === 'internal'; }), 'job_id');
                $external_jobs = array_column(array_filter($jobs, function ($job) { return $job['the_type'] === 'external'; }), 'job_id');
                $int_jobs = Job::init($this->db, $this->user)->get_jobs_by_id($internal_jobs);
                $ext_jobs = Job::init($this->db, $this->user)->get_external_jobs_by_id($external_jobs);
                $counter = 0;
                $jobs = [];
                foreach ($int_jobs as $key => $job) {
                    $job = (new Job($this->db, $this->user))->get_job_by_id($job['job_id']);
                    unset($job['password']);
                    $jobs[$counter]                    = $job;
                    $jobs[$counter]['type']            = 'internal_job';
                    $jobs[$counter]['country']         = (new TaskCentre($this->db, $this->user))->getCountryName($job['country']);
                    $jobs[$counter]['state']           = (new TaskCentre($this->db, $this->user))->getStateName($job['state']);
                    $jobs[$counter]['job_country']         = (new TaskCentre($this->db, $this->user))->getCountryName($job['job_country']);
                    $jobs[$counter]['job_state']           = (new TaskCentre($this->db, $this->user))->getStateName($job['job_state']);
                    $jobs[$counter]['published_count'] = (new Job($this->db, $this->user))->get_published_jobs_count($job['user_id']);
                    $jobs[$counter]['rating']          = (new TaskCentre($this->db, $this->user))->get_ratings($job['user_id']);
                    $jobs[$counter]['questions']       = (new Job($this->db, $this->user))->questions($job['job_id']);
                    $jobs[$counter]['salary_range_max']   = number_format($job['salary_range_max'], 2);
                    $jobs[$counter]['salary_range_min']   = number_format($job['salary_range_min'], 2);
                    $counter++;
                }

                foreach ($ext_jobs as $key => $job) {
                    $job = (new Job($this->db, $this->user))->get_external_job_by_id($job['job_id']);
                    unset($job['password']);
                    $jobs[$counter]            = $job;
                    $jobs[$counter]['type']    = 'external_job';
                    $jobs[$counter]['job_country'] = (new TaskCentre($this->db, $this->user))->getCountryName($job['job_country']);
                    $jobs[$counter]['job_state']   = $job['state'];
                    $jobs[$counter]['salary_range_max']   = number_format($job['salary_range_max'], 2);
                    $jobs[$counter]['salary_range_min']   = number_format($job['salary_range_min'], 2);
                    $counter++;
                }

                /*foreach ($jobs as $key => $job) {
                    $jobs[$key]['type'] = 'job';
                    unset($jobs[$key]['password']);
                    $jobs[$key]['country']         = (new TaskCentre($this->db, $this->user))->getCountryName($job['country']);
                    $jobs[$key]['state']           = (new TaskCentre($this->db, $this->user))->getStateName($job['state']);
                    $jobs[$key]['published_count'] = (new Job($this->db, $this->user))->get_published_jobs_count($job['user_id']);
                    $jobs[$key]['rating']          = (new TaskCentre($this->db, $this->user))->get_ratings($job['user_id']);
                    $jobs[$key]['questions']       = (new Job($this->db, $this->user))->questions($job['job_id']);
                }*/

            }
        }

        $all_list       = array_merge($tasks, $jobs);

        /*usort($all_list, function($first, $second){
            return Carbon::parse($first['created_at'])->equalTo(Carbon::parse($second['created_at'])) ? 0 :
                (Carbon::parse($first['created_at'])->greaterThan(Carbon::parse($second['created_at'])) ? -1 : 1);
        });*/

        if( isset($within) ){
            $filters['state'] = null;
            $filters['state_name'] = null;
            $filters['city'] = null;
        }

        $states = (new TaskCentre($this->db, $this->user))->getStates(null, true);
        $cities = isset($filters['state']) ? (new TaskCentre($this->db, $this->user))->getCities($filters['state'], true) : [];

        if (request('category') !== null) {
            $category_names = in_array('0', explode(',', request('category'))) ? lang('public_search_all_category') . ', ' : '';
            if (isset($filters['category']) && $filters['category'] !== '0') {
                $filters['category_names'] = is_null($filters['type']) || in_array( 'task', $filters['type'] ) ? (new TaskCentre($this->db, $this->user))->getCategories($filters['category'], 'task_category') : (new TaskCentre($this->db, $this->user))->getCategories($filters['category']);
                $category_names            .= implode(', ', array_column($filters['category_names'], 'name'));
            }

            if( ! isset($filters['category']) && request('category') )
                $filters['category'] = in_array('0', explode(',', request('category'))) ? '0' : '';

            $filters['category_names'] = trim($category_names, ', ');
        }

        if( !empty($filters['skills']) ) {
            $this->db->query("SELECT `id`, `name` FROM `skills` WHERE `status` = '1' AND `id` IN({$filters['skills']})");
            $filters['skills_list'] = $this->db->getRowList();
        }

        $keywords = !empty($search_term) ? implode(' ', $search_term) : '';
        array_filter($filters, function ($filter, $key) use(&$keywords, $filters){
            if(in_array($key, ['category', 'state', 'city', 'skills_list', 'nearby_places', 'states', 'remote_state', 'location_by'])) return false;
            if('type' === $key && isset($filters['type'])){
                if($keywords !== '') $keywords .= ', ';
                if( is_array($filter) ) $filter = end($filter);
                if('task' === $filter){
                    $keywords .= lang('task_details_on_demand');
                }else if( 'job' === $filter) {
                    $keywords .= lang('job_full_time_work');
                }
                return false;
            }
            if('task_budget' === $key && isset($filters['task_budget'])){
                if((int)$filter[0] === 1000){
                    $keywords .= ', ' . lang('search_range') . ': RM' . $filter[0] . ' ' . lang('dashboard_pay_range_select_amount_and_above');
                }else {
                    $keywords .= ', ' . lang('search_range') . ': RM' . implode(' - RM', $filter);
                }
                return false;
            }
            if('date_type' === $key && isset($filters['date_type'])){
                $type = [
                            'postedDate' => lang('public_search_posted_date') . ': ',
                            'startDate'  => lang('public_search_start_date') . ': ',
                            'closeDate'  => lang('public_search_closing_date') . ': '
                        ][$filter];
                $keywords .=  ', ' . $type;
                return false;
            }
            if('date_period' === $key && isset($filters['date_type'], $filters['date_period'])){
                if('customrange' !== $filter) {
                    if($filters['date_type'] === 'postedDate' && $filter !== 'any')
                        $period = [
                                      '24h' => sprintf(lang('public_search_last_x_hours'), 24),
                                      '3d'  => sprintf(lang('public_search_last_x_days'), 3),
                                      '7d'  => sprintf(lang('public_search_last_x_days'), 7),
                                      '14d' => sprintf(lang('public_search_last_x_days'), 14),
                                      '30d' => sprintf(lang('public_search_last_x_days'), 30)
                                  ][$filter];
                    elseif (in_array($filters['date_type'], ['startDate', 'closeDate']) && $filter !== 'any')
                        $period = [
                                      '24h' => sprintf(lang('public_search_next_x_hours'), 24),
                                      '3d'  => sprintf(lang('public_search_next_x_days'), 3),
                                      '7d'  => sprintf(lang('public_search_next_x_days'), 7),
                                      '14d' => sprintf(lang('public_search_next_x_days'), 14),
                                      '30d' => sprintf(lang('public_search_next_x_days'), 30)
                                  ][$filter];
                    else
                        $period = lang('search_any');
                }else{
                    $period = lang('search_between') . ' (';
                }
                $keywords .=  $period;
                return false;
            }
            if('from' === $key && isset($filters['date_type'], $filters['date_period']) && $filters['date_period'] === 'customrange'){
                $keywords .=  str_replace('-', '/', $filter);
                return false;
            }
            if('to' === $key && isset($filters['date_type'], $filters['date_period']) && $filters['date_period'] === 'customrange'){
                $keywords .=  ' - ' . str_replace('-', '/', $filter) . ')';
                return false;
            }

            if('completed' === $key && $filter !== ''){
                if($keywords !== '') $keywords .= ', ';
                $keywords .= lang('search_completed_task') . ': ' . $filter;
                return false;
            }

            if('skills' === $key && !empty($filter)){
                if($keywords !== '') $keywords .= ', ';
                $filter = implode(', ', array_column($filters['skills_list'], 'name'));
                $keywords .= $filter;
                return false;
            }

            if('within' === $key && !empty($filter)){
                if($keywords !== '') $keywords .= ', ';
                $filter = lang('search_within') . " {$filter}";
                $keywords .= $filter;
                return false;
            }

            if(is_array($filter)) $keywords .=  ', ' . ucwords(implode(', ', $filter));
            elseif(!is_null($filter)) $keywords .=  ', ' . ucwords($filter);
        }, ARRAY_FILTER_USE_BOTH);

        $keywords = str_replace(['By-hour', 'Lump-sum'], [lang('task_by_the_hour'), lang('task_lump_sum')], trim($keywords, ', '));

        return [
            'listing'       => $all_list,
            'current_page'  => $page,
            'search_term'   => implode(' ', $search_term),
            'filters'       => $filters,
            'keywords'      => $keywords,
            'there_is_more' => $there_is_more,
            'cities'        => $cities,
            'states'        => $states
        ];
    }

    private function get_talents_list(){
        $per_page = 10;
        $filters = [];

        $search_term = request('q') ? sanitize(request('q')) : null;

        if(!is_null($search_term))
            $search_term = array_values(array_filter(explode(' ', preg_replace('#[^a-zA-Z0-9\s]#i', '', $search_term))));

        $filters = [
            'type' => request('type') ? explode(',', sanitize(request('type'))) : null,
            'category' => request('category') ? sanitize(request('category')): null,
            'state' => request('state') ? count(explode(',', sanitize(request('state')))) > 1 ? explode(',', sanitize(request('state'))) : sanitize(request('state')) : null,
            'city' => request('city') ? count(explode(',', sanitize(request('city')))) > 1 ? explode(',', sanitize(request('city'))) : sanitize(request('city')) : null,
            'task_type' => request('task_type') ? count(explode(',', sanitize(request('task_type')))) > 1 ? explode(',', sanitize(request('task_type'))) : sanitize(request('task_type')) : null,
            'task_budget' => request('task_budget') ? count(explode(',', sanitize(request('task_budget')))) > 1 ? explode(',', sanitize(request('task_budget'))) : sanitize(request('task_budget')) : null,
            'date_type' => request('date_type') ? count(explode(',', sanitize(request('date_type')))) > 1 ? explode(',', sanitize(request('date_type'))) : sanitize(request('date_type')) : null,
            'date_period' => request('date_period') ? count(explode(',', sanitize(request('date_period')))) > 1 ? explode(',', sanitize(request('date_period'))) : sanitize(request('date_period')) : null,
            'from' => request('from') ? count(explode(',', sanitize(request('from')))) > 1 ? explode(',', sanitize(request('from'))) : sanitize(request('from')) : null,
            'to' => request('to') ? count(explode(',', sanitize(request('to')))) > 1 ? explode(',', sanitize(request('to'))) : sanitize(request('to')) : null,
            'skills' => request('skills') ? count(explode(',', sanitize(request('skills')))) > 1 ? explode(',', sanitize(request('skills'))) : sanitize(request('skills')) : null,
            'location_by' => request('location_by') ? sanitize(request('location_by')) : null,
            'within' => request('within') ? sanitize(request('within')) : null,
        ];


        if( isset($filters['location_by']) && $filters['location_by'] === 'locbystate' ) {
            if (isset($filters['state'])) $filters['state_name'] = (new TaskCentre($this->db, $this->user))->getStateName($filters['state']);
            if (isset($filters['city'])) $filters['city_name'] = (new TaskCentre($this->db, $this->user))->getCityName($filters['city']);
        }elseif ( isset($filters['location_by']) && $filters['location_by'] === 'locbynearest' ){
            if( isset($filters['within']) && $filters['within'] !== '' ){
                $within        = preg_replace("/[^0-9]/", "", $filters['within']); // strip all non-numeric from the passed value
                $lat_lng       = isset($_SESSION['current_user_location']) ? $_SESSION['current_user_location'] : null;
                //$nearby_places = (new Search($this->db, $this->user))->get_nearby_names(null, $lat_lng, $within);
                @list($lat, $lng) = !is_array($lat_lng) ? explode(',', $lat_lng, 2) : $lat_lng;
                $nearby_places = (new Search($this->db, $this->user))->get_nearby($lat, $lng, $within);
                $filters['nearby_places'] = array_column($nearby_places, 'city');
                $filters['city'] = array_column($nearby_places, 'city_id');
                $filters['state_name'] = array_column($nearby_places, 'state');
            }
        }

        if( isset($filters['state']) ) $filters['state_name'] = (new TaskCentre($this->db, $this->user))->getStateName($filters['state']);
        if( isset($filters['city']) ) $filters['city_name'] = (new TaskCentre($this->db, $this->user))->getCityName($filters['city']);

        $page = params('page') ? (int)params('page') : 1;
        $offset = ($page - 1) * $per_page;

        if( (!empty(array_filter($filters)) || request('completed') || !empty($search_term)) && sanitize(request('scope')) === 'talent' ){

            $offset = ($page - 1) * ($per_page/2);

            if( request('completed') ) $filters['completed'] = sanitize(request('completed'));
            $talents = (new TaskCentre($this->db, $this->user))->talents_listing($search_term, $filters, $offset, $per_page/2);
            $talents_total_count = (int)(new TaskCentre($this->db, $this->user))->get_total_talents_count($search_term, $filters);
            $talents_total_pages = ceil($talents_total_count/($per_page/2));
            if( !empty($filters['skills']) ) {
                $this->db->query("SELECT `id`, `name` FROM `skills` WHERE `id` IN({$filters['skills']})");
                $filters['skills_list'] = $this->db->getRowList();
            }
            if( $page < $talents_total_pages ){
                $there_is_more_talents = true;
            }else{
                $there_is_more_talents = false;
            }
        }else {
            $offset = ($page - 1) * ($per_page/2);
            $talents = (new TaskCentre($this->db, $this->user))->talents_listing([], [], $offset, $per_page/2);
            $talents_total_count = (int)(new TaskCentre($this->db, $this->user))->get_total_talents_count($search_term, $filters);
            $talents_total_pages = ceil($talents_total_count/($per_page/2));

            if( $page < $talents_total_pages ){
                $there_is_more_talents = true;
            }else{
                $there_is_more_talents = false;
            }
        }

        if( isset($within) ){
            $filters['state'] = null;
            $filters['state_name'] = null;
            $filters['city'] = null;
            $filters['city_name'] = null;
            $filters['nearby_places'] = null;
        }

        $states = (new TaskCentre($this->db, $this->user))->getStates(null, true);
        $cities = isset($filters['state']) ? (new TaskCentre($this->db, $this->user))->getCities($filters['state'], true) : [];

        if (request('category') !== null) {
            $category_names = in_array('0', explode(',', request('category'))) ? lang('public_search_all_category') . ', ' : '';
            if (isset($filters['category'])) {
                $filters['category_names'] = is_null($filters['type']) || in_array('task', $filters['type']) ? (new TaskCentre($this->db, $this->user))->getCategories($filters['category'], 'task_category') : (new TaskCentre($this->db, $this->user))->getCategories($filters['category']);
                $category_names            .= implode(', ', array_column($filters['category_names'], 'name'));
            }
            $filters['category'] .= in_array('0', explode(',', request('category'))) && is_null($filters['category']) ? '0,' : '';
            $filters['category_names'] = trim($category_names, ', ');
        }

        $keywords = !empty($search_term) ? implode(' ', $search_term) : '';
        array_filter($filters, function ($filter, $key) use(&$keywords, $filters){
            if(in_array($key, ['category', 'state', 'city', 'skills_list', 'nearby_places', 'states', 'remote_state', 'type', 'location_by'])) return false;
            if('task_budget' === $key && isset($filters['task_budget'])){
                $keywords .=  ', ' . lang('search_budget') . ': RM' . implode(' - RM', $filter);
                return false;
            }
            if('date_type' === $key && isset($filters['date_type'])){
                $type = [
                            'postedDate' => lang('public_search_posted_date') . ': ',
                            'startDate'  => lang('public_search_start_date') . ': ',
                            'closeDate'  => lang('public_search_closing_date') . ': '
                        ][$filter];
                $keywords .=  ', ' . $type;
                return false;
            }
            if('date_period' === $key && isset($filters['date_type'], $filters['date_period'])){
                if('customrange' !== $filter) {
                    if($filters['date_type'] === 'postedDate' && $filter !== 'any')
                        $period = [
                                      '24h' => sprintf(lang('public_search_last_x_hours'), 24),
                                      '3d'  => sprintf(lang('public_search_last_x_days'), 3),
                                      '7d'  => sprintf(lang('public_search_last_x_days'), 7),
                                      '14d' => sprintf(lang('public_search_last_x_days'), 14),
                                      '30d' => sprintf(lang('public_search_last_x_days'), 30)
                                  ][$filter];
                    elseif (in_array($filters['date_type'], ['startDate', 'closeDate']) && $filter !== 'any')
                        $period = [
                                      '24h' => sprintf(lang('public_search_next_x_hours'), 24),
                                      '3d'  => sprintf(lang('public_search_next_x_days'), 3),
                                      '7d'  => sprintf(lang('public_search_next_x_days'), 7),
                                      '14d' => sprintf(lang('public_search_next_x_days'), 14),
                                      '30d' => sprintf(lang('public_search_next_x_days'), 30)
                                  ][$filter];
                    else
                        $period = lang('search_any');
                }else{
                    $period = lang('search_between') . ' (';
                }
                $keywords .=  $period;
                return false;
            }
            if('from' === $key && isset($filters['date_type'], $filters['date_period']) && $filters['date_period'] === 'customrange'){
                $keywords .=  str_replace('-', '/', $filter);
                return false;
            }
            if('to' === $key && isset($filters['date_type'], $filters['date_period']) && $filters['date_period'] === 'customrange'){
                $keywords .=  ' - ' . str_replace('-', '/', $filter) . ')';
                return false;
            }

            if('completed' === $key && $filter !== ''){
                if($keywords !== '') $keywords .= ', ';
                $keywords .= lang('search_completed_task') . ': ' . ($filter === 'all' ? lang('public_search_filter_all') : $filter);
                return false;
            }

            if('skills' === $key && !empty($filter)){
                if($keywords !== '') $keywords .= ', ';
                $filter = implode(', ', array_column($filters['skills_list'], 'name'));
                $keywords .= $filter;
                return false;
            }

            if('within' === $key && !empty($filter)){
                if($keywords !== '') $keywords .= ', ';
                $filter = lang('search_within') . " {$filter}";
                $keywords .= $filter;
                return false;
            }

            if(is_array($filter) && !empty($filter)) $keywords .=  ', ' . ucwords(implode(', ', $filter));
            elseif(!is_null($filter)) $keywords .=  ', ' . ucwords($filter);
        }, ARRAY_FILTER_USE_BOTH);

        $keywords = trim($keywords, ', ');

        return [
            'listing'       => $talents,
            'current_page'  => $page,
            'search_term'   => implode(' ', $search_term),
            'filters'       => $filters,
            'keywords'      => $keywords,
            'there_is_more' => $there_is_more_talents,
            'cities'        => $cities,
            'states'        => $states
        ];
    }
}