<?php
class post{
	var $db;
	var $user;
	var $cms;
	
	public function __construct($db, $user, $cms){
		$this->db = $db;
		$this->user = $user;
		$this->cms = $cms;
	}
	
	public function request($field = ''){
		if(!empty($field)){
			$val = !empty($_REQUEST[$field]) ? $_REQUEST[$field] : (!empty($_REQUEST[$field]) && $_REQUEST[$field] == '0' ? '0' : false);
		}else{
			$val = $_REQUEST;
		}
		return $val;
	}
	
	public function settings(){
        if( ! isset($_SESSION['settings']) ) {
            $setting  = array();
            $settings = array();

            $this->db->query("SELECT `name`, `value` FROM settings");
            $settings = $this->db->getRowList();

            foreach ($settings as $keys) {
                $setting[$keys['name']] = $keys['value'];
            }

            if ($setting['date_format'] == 'custom') $setting['date_format'] = $setting['date_format_custom'];
            if ($setting['time_format'] == 'custom') $setting['time_format'] = $setting['time_format_custom'];
            if ($setting['session_timeout'] == 'custom') $setting['session_timeout'] = $setting['session_timeout_custom'];
            $_SESSION['settings'] = $setting;
        }else{
            $setting = $_SESSION['settings'];
        }
        return $setting;
	}
		
	public function loginRequired(){
		$return['error'] = true;
		$return['msg'] = lang('sys_login_required');
		$return['title'] = lang('error');
		$return['url'] = "#login";
		return json($return);
	}
	
	public function forgotSave(){
		
		$info = tokenDecode($this->request('token'));
		
		$data = @unserialize($info);
		if($data !== false) $data = $data;
		else $data = false;
		
		if(!empty($data)){
			if(!$this->request('password')){
				$return['error'] = true;
				$return['msg'] = lang('sys_password_required'); 
				$return['title'] = lang('error');

			}else if($this->request('password') != $this->request('password2')){
				$return['error'] = true;
				$return['msg'] = lang('sys_confirm_password_not_match'); 
				$return['title'] = lang('error');

			}else if(!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}$/", $this->request('password'))){
				$return['error'] = true;
				$return['msg'] = lang('sys_password_rules');
                $return['title'] = lang('error');

			}else{
				
				$this->db->table("members");
				$this->db->updateArray(array(
					"password" => encryptPassword($this->request('password')),
				));
				$this->db->whereArray(array(
					"email" => $data['email'],
				));
				$this->db->update();
								
				$return['error'] = false;
				$return['msg'] = lang('sys_password_saved');
                $return['title'] = lang('success');
			}
		}else{
			$return['error'] = true;
			$return['msg'] = lang('sys_invalid_token');
            $return['title'] = lang('error');
		}
			
		return json($return);
	}
	
	public function register(){	
		$validEmail = filter_var(strtolower($this->request('email')), FILTER_VALIDATE_EMAIL);
		
		if(!$this->request('salutation') || !$this->request('firstname') || !$this->request('contact') || !$this->request('email') || !$this->request('password') || !$this->request('password2')){
			$return['error'] = true;
			$return['msg'] = lang('sys_password_rules');
            $return['title'] = lang('error');
					
		}else if(!$validEmail){
			$return['error'] = true;
			$return['msg'] = lang('sys_invalid_email');
            $return['title'] = lang('error');
			
		}else if($this->request('password') != $this->request('password2')){
			$return['error'] = true;
			$return['msg'] =lang('sys_confirm_password_not_match');
            $return['title'] = lang('error');
					
		}else{
	
			$this->db->query("SELECT * FROM members WHERE email = " . $this->db->escape($this->request('email')) . " AND status = '1'");
			$member = $this->db->getSingleRow();
			
			if($member){
				$return['error'] = true;
				$return['msg'] = sprintf(lang('sys_email_exists'), $this->request('email'));
                $return['title'] = lang('error');
				
			}else{			
				$this->db->table("members");
				$this->db->insertArray(array(
					"email" => strtolower($this->request('email')),
					"password" => encryptPassword($this->request('password')),
					"salutation" => $this->request('salutation'),
					"firstname" => $this->request('firstname'),
					"lastname" => $this->request('lastname'),
					"contact" => $this->request('contact'),
					"address1" => $this->request('address1'),
					"address2" => $this->request('address2'),
					"zip" => $this->request('zip'),
					"city" => $this->request('city'),
					"country" => $this->request('country'),
					"state" => $this->request('state'),
					"date_created" => "NOW()",
					"status" => "0"
				));
				$this->db->insert();
				
				$infoData = array(
					"email" => strtolower($this->request('email')),
					"time" => time(),
				);
				$info = serialize($infoData);
				
				$ref = 'http://' . $_SERVER['HTTP_HOST'] . url_for('/');
				$link = $ref . 'activate/' . tokenEncode($info);
				
				$from = $this->cms->settings()['email'];
				$to = strtolower($this->request('email'));
				$subject = SITE_NAME . ": Thank you for your registration!";
				$body = 
				'<html><body style="font-family:Segoe UI,Verdana,sans-serif; font-size:12px">
				<p><strong>Dear ' . $this->request('firstname') . ' ' . $this->request('lastname') . ',</strong></p>
				<p>Thank you for registering with ' . SITE_NAME . '! You are just one step away to start shopping at our online store!</p>
				<p>To verify your registration, please click on this:<br><a href="' . $link . '">' . $link . '</a></p>
				<p>This is an auto-generated email. Please do not reply to this email.</p>
				<p>Sincerely,<br>' . SITE_NAME . ' Team</p>
				</body></html>';

				$mime = new sendMail($from, $to, "", "", $subject);
				$mime->set('html', true);
				$mime->parseBody($body);
				$mime->setHeaders(); 
				$mime->send();
									
				$return['error'] = false;
				$return['msg'] = lang('sys_activation_link_sent_to_email');
                $return['title'] = lang('success');
			}
		}
				
		return json_encode($return);
	}
	
	public function accountUpdate(){
		if(!$this->request('salutation') || !$this->request('firstname') || !$this->request('contact')){
			$return['error'] = true;
			$return['msg'] = lang('sys_password_rules');
            $return['title'] = lang('error');
					
		}else if($this->request('password') != $this->request('password2')){
			$return['error'] = true;
			$return['msg'] = lang('sys_confirm_password_not_match');
            $return['title'] = lang('error');
					
		}else{
			
			$member = $this->user->info;
			
			if($this->request('password')){
				$this->db->table("members");
				$this->db->updateArray(array(
					"password" => encryptPassword($this->request('password')),
				));
				$this->db->whereArray(array(
					"id" => $this->user->id,
				));
				$this->db->update();
			}
			
			$this->db->table("members");
			$this->db->updateArray(array(
				"salutation" => $this->request('salutation'),
				"firstname" => $this->request('firstname'),
				"lastname" => $this->request('lastname'),
				"contact" => $this->request('contact'),
				"address1" => $this->request('address1'),
				"address2" => $this->request('address2'),
				"zip" => $this->request('zip'),
				"city" => $this->request('city'),
				"country" => $this->request('country'),
				"state" => $this->request('state'),
			));
			$this->db->whereArray(array(
				"id" => $this->user->id,
			));
			$this->db->update();
						
			$return['error'] = false;
			$return['msg'] = lang('sys_account_info_update');
            $return['title'] = lang('success');
		}
		
		return json_encode($return);
	}
		
	public function updateAccount(){
		$validEmail = filter_var(strtolower($this->request('email')), FILTER_VALIDATE_EMAIL);

		if(!$this->request('email')){
			$return['msg'] = lang('sys_email_required');
            $return['title'] = lang('error');
			
		}else if(!$this->request('currentPassword')){
			$return['error'] = true;
			$return['msg'] = lang('sys_current_password_required');
            $return['title'] = lang('error');
					
		}else if(!$validEmail){
			$return['error'] = true;
			$return['msg'] = lang('sys_please_enter_a_valid_email');
            $return['title'] = lang('error');
			
		}else if($this->request('password') != $this->request('password2')){
			$return['error'] = true;
			$return['msg'] = lang('sys_confirm_password_not_match');
            $return['title'] = lang('error');
					
		}else{
			
			$this->db->query("SELECT * FROM members WHERE email = " . $this->db->escape($this->request('email')) . " AND email != " . $this->db->escape($this->user->info['email']) . " AND status = '1'");
			$emailExists = $this->db->getSingleRow();
			
			if($emailExists){
				$return['error'] = true;
				$return['msg'] = sprintf(lang('sys_email_exists'), $this->request('email'));
                $return['title'] = lang('error');
				
			}else if(encryptPassword($this->request('currentPassword')) != $this->user->info['password']){
				$return['error'] = true;
				$return['msg'] = lang('sys_current_password_wrong');
                $return['title'] = lang('error');
			
			}else{
				
				if($this->request('password')){
					$this->db->table("members");
					$this->db->updateArray(array(
						"password" => encryptPassword($this->request('password')),
					));
					$this->db->whereArray(array(
						"id" => $this->user->id,
					));
					$this->db->update();
				}
				
				$this->db->table("members");
				$this->db->updateArray(array(
					"email" => $this->request('email'),
				));
				$this->db->whereArray(array(
					"id" => $this->user->id,
				));
				$this->db->update();
						
				$return['error'] = false;
				$return['msg'] = lang('sys_account_details_updated');
                $return['title'] = lang('success');
			}
		}
			
		return json($return);
	}

	//Eric's Code
	public function sign_up_individual(){
		$validEmail = filter_var(strtolower($this->request('inputEmail')), FILTER_VALIDATE_EMAIL);
		$redirect = $this->request('redirect') ? '&redirect=' . urlencode($this->request('redirect')) : option('site_uri') . url_for('/dashboard');

		if(!$validEmail){
			$return['error'] = true;
			$return['msg'] = lang('sys_please_enter_a_valid_email');
            $return['title'] = lang('error');

		}else{

			$this->db->query("SELECT id FROM members WHERE email = '" . $this->request('inputEmail') . "'");	
			$member = $this->db->getSingleRow();

			if(!$member){
				if(!$this->request('firstName')){
					$return['error'] = true;
					$return['msg'] = lang('sys_firstname_required');
                    $return['title'] = lang('error');

				}else if(!$this->request('lastName')){
					$return['error'] = true;
					$return['msg'] = lang('sys_lastname_required');
                    $return['title'] = lang('error');

				}else if(!$this->request('country')){
					$return['error'] = true;
					$return['msg'] = lang('sys_select_a_country');
                    $return['title'] = lang('error');

				}else if(is_null(request('state')) && !$this->request('state_other')){
					$return['error'] = true;
					$return['msg'] = lang('sys_select_a_state');
                    $return['title'] = lang('error');

				}else if($this->request('tnc_ind_check') != '1'){
					$return['error'] = true;
					$return['msg'] = lang('sys_you_must_agree_to_our_tnc');
                    $return['title'] = lang('error');

				}else{

                    if( $this->request('state_other') ){
                        $country = $this->cms->country($this->request('country'));
                        $this->db->table('states');
                        $this->db->insertArray([
                            'name' => mysqli_real_escape_string($this->db->connection, $this->request('state_other')),
                            'country_id' => $country['id'],
                            'country_code' => $country['iso2'],
                            'status' => '0'
                        ]);
                        $this->db->insert();
                        $_REQUEST['state'] = $this->db->insertid();
                    }

                    if( $this->request('city_other') ){
                        $country = $country ?? $this->cms->country($this->request('country'));
                        $this->db->table('cities');
                        $this->db->insertArray([
                            'name' => mysqli_real_escape_string($this->db->connection, $this->request('city_other')),
                            'state_id' => $this->request('state'),
                            'state_code' => '',
                            'country_id' => $country['id'],
                            'country_code' => $country['iso2'],
                            'status' => '0'
                        ]);
                        $this->db->insert();
                        $_REQUEST['city'] = $this->db->insertid();
                    }

					$this->db->table("members");
					$this->db->insertArray(array(
						"firstname" => mysqli_real_escape_string($this->db->connection, request('firstName')),
						"lastname" => mysqli_real_escape_string($this->db->connection, request('lastName')),
						"email" => mysqli_real_escape_string($this->db->connection, request('inputEmail')),
						"country" => request('country'),
						"state" => request('state'),
						"city" => request('city'),
						"type" => "0",
					));
					$this->db->insert();

					$member_id = $this->db->insertid();

					date_default_timezone_set("Asia/Kuala_Lumpur");

					$token = bin2hex(random_bytes(8));

					$this->db->table("temp_token");
					$this->db->insertArray(array(
						"token" => $token,
						"expiry_date" => date("Y-m-d H:i:s",strtotime ("+12 hour")),
						"email" => $this->request('inputEmail'),

					));
					$this->db->insert();

                    $mail = new Mail($this->db);
                    $to   = [
                        'email' => $this->request('inputEmail'),
                        'name'  => $this->request('firstName')
                    ];
                    $link = option('site_uri') . url_for('/set_password') . "?token={$token}&redirect={$redirect}";
                    $mail->sendmail($to, 'activate-your-account', ['link' => $link, 'first_name' => $this->request('firstName') ]);

					$return['error'] = false;
					$return['msg'] = lang('sys_please_check_your_email');
                    $return['title'] = lang('success');
					//$return['url'] = "sendgrid?email=". $this->request('inputEmail');
				}
				
			}else{

				$return['error'] = true;
				$return['msg'] = sprintf(lang('sys_email_exists'), $this->request('inputEmail'));
                $return['title'] = lang('error');
			}
		}
		
		return json($return);
	}

	public function password_reset(){
		$this->db->query("SELECT email FROM temp_token WHERE token = '".$_GET['token']."'");	
		$tokenDetails = $this->db->getSingleRow();

		$this->db->query("SELECT `id`, `firstname`, `type` FROM `members` WHERE `email` = '".$tokenDetails['email']."'");
		$member = $this->db->getSingleRow();

		if(!preg_match("/^(?=.*\d)(?=.*[a-zA-Z]).{8,128}$/", $this->request('password'))){
			$return['error'] = true;
			$return['msg'] = lang('sys_password_rules');
            $return['title'] = lang('error');

		}else{

			$this->db->table('members');
			$this->db->updateArray(array(
				"password" => encryptPassword($this->request('password')),
			));
			$this->db->whereArray(array(
				"email" => $tokenDetails['email'],
			));
			$this->db->update();


			$this->db->table('temp_token');

			$this->db->updateArray(array(
				"token_status" => "99",
			));
			$this->db->whereArray(array(
				"token" => $_GET['token'],
			));
			$this->db->update();
			
			$return['error'] = false;
			$return['msg'] = lang('sys_password_changed_success');
            $return['title'] = lang('success');
			$return['url'] = "login";

			$mail = new Mail($this->db);
			$to = ['email' => $tokenDetails['email'], 'name' => $member['firstname']];
			$link = ['link' => option('site_uri') . url_for('login'), 'first_name' => $member['firstname'] ];
			$mail->sendmail($to, 'password-changed', $link);
		}
		
		return json_encode($return);
	}
		
	public function password_set(){
	    $redirect = isset($_SESSION[WEBSITE_PREFIX . 'REDIRECT']) ? $_SESSION[WEBSITE_PREFIX . 'REDIRECT'] :
		            ($this->request('redirect') ? url_for('/login') . '?redirect=' . $this->request('redirect') : url_for('/dashboard'));

		$this->db->query("SELECT email FROM temp_token WHERE token = '".$_GET['token']."'");	
		$tokenDetails = $this->db->getSingleRow();

		$this->db->query("SELECT `id`, `firstname`, `type` FROM `members` WHERE `email` = '".$tokenDetails['email']."'");
		$member = $this->db->getSingleRow();

		if(!preg_match("/^(?=.*\d)(?=.*[a-zA-Z]).{8,128}$/", $this->request('password'))){
			$return['error'] = true;
			$return['msg'] = lang('sys_password_rules');
            $return['title'] = lang('error');

		}else{

			$_SESSION[WEBSITE_PREFIX.'USER_ID'] = $member['id'];
			$_SESSION[WEBSITE_PREFIX.'LAST_LOGIN'] = date('Y-m-d H:i:s');

			$this->db->table('members');
			$this->db->updateArray(array(
				"password" => encryptPassword($this->request('password')),
			));
			$this->db->whereArray(array(
				"email" => $tokenDetails['email'],
			));
			$this->db->update();


			$this->db->table('temp_token');
			$this->db->updateArray(array(
				"token_status" => "99",
			));
			$this->db->whereArray(array(
				"token" => $_GET['token'],
			));
			$this->db->update();
			
			$return['error'] = false;
			$return['msg'] = lang('success_msg');
            $return['title'] = lang('success');
			$return['url'] = urldecode($redirect);

            $mail = new Mail($this->db);
            $to = ['email' => $tokenDetails['email'], 'name' => $member['firstname']];
            $link = ['link' => option('site_uri') . url_for('login'), 'first_name' => $member['firstname'] ];
            $mail->sendmail($to, 'account-successfully-created', $link);
		}

		return json_encode($return);
	}

	public function forgot_password(){
		$this->db->query("SELECT m.`id`, m.`firstname`, m.`email`, LOWER(l.`code`) AS `language` FROM `members` m
                          LEFT JOIN `preference` p ON p.`member_id` = m.`id`
                          LEFT JOIN `language` l ON l.`id` = p.`language`
                          WHERE email = ". $this->db->escape($this->request('email')));
		$member = $this->db->getSingleRow();

		if($member){
            date_default_timezone_set("Asia/Kuala_Lumpur");
            $token = bin2hex(random_bytes(8));
            $this->db->table("temp_token");
            $this->db->insertArray(array(
                "token"       => $token,
                "expiry_date" => date("Y-m-d H:i:s", strtotime("+1 hour")),
                "email"       => $this->request('email'),
            ));
            $this->db->insert();

            $return['error'] = false;
            $return['msg']   = lang('sys_an_email_has_been_sent_to_you');
            $return['title'] = lang('success');
            $return['url']   = url_for('/home');//"sendgrid?email=".$this->request('email');

            $mail = new Mail($this->db);
            $to   = [
                'email' => $member['email'],
                'name'  => $member['firstname']
            ];
            $link = option('site_uri') . url_for('/password_reset') . "?token={$token}";
            $mail->sendmail($to, 'reset-password', ['link' => $link, 'first_name' => $member['firstname'] ], $member['language']);

		}else{
			$return['error'] = true;
			$return['msg'] = lang('sys_email_not_found');
            $return['title'] = lang('error');
			$return['url'] = "";
		}
		return json_encode($return);
	}	


	public function login(){
		$this->db->query("SELECT id, status FROM members WHERE status < 999 AND email = ". $this->db->escape($this->request('email')) . " AND password = " . $this->db->escape(encryptPassword($this->request('password'))));
		$member = $this->db->getSingleRow();	

		if($member){
			if($member['status'] == '99'){
				$return['error'] = true;
				$return['msg'] = lang('sign_in_suspend');
                $return['title'] = lang('error');
				
			}else{
				$return['error'] = false;
				$return['msg'] = lang('sign_in_success');
                $return['title'] = lang('success');
				$return['url'] = !empty($_SESSION[WEBSITE_PREFIX.'REDIRECT']) ? $_SESSION[WEBSITE_PREFIX.'REDIRECT'] : url_for('/dashboard');
				$_SESSION[WEBSITE_PREFIX.'USER_ID'] = $member['id'];
				$_SESSION[WEBSITE_PREFIX.'LAST_LOGIN'] = date('Y-m-d H:i:s');

				$this->db->table("member_logs");
				$this->db->insertArray(array(
					"member_id" => $member['id'],
					"date" => "NOW()",
					"ip_address" => $_SERVER['REMOTE_ADDR'],
					"user_agent" => $_SERVER['HTTP_USER_AGENT'],
				));
				$this->db->insert();
			}

		}else{
			$return['error'] = true;
			$return['msg'] = lang('sign_in_failed');
            $return['title'] = lang('error');
		}
				
		return json_encode($return);

	}

	public function sign_up_company(){
		$validEmail = filter_var(strtolower($this->request('email')), FILTER_VALIDATE_EMAIL);
		$redirect = $this->request('redirect') ? '&redirect=' . urlencode($this->request('redirect')) : '';

		if(!$validEmail){
			$return['error'] = true;
			$return['msg'] = lang('sys_please_enter_a_valid_email');
            $return['title'] = lang('error');

		}else{

			$this->db->query("SELECT id FROM members where email = ". $this->db->escape($this->request('email')));	
			$members = $this->db->getSingleRow();

			if(!$members){
				if(!$this->request('firstName')){
					$return['error'] = true;
					$return['msg'] = lang('sys_firstname_required');
                    $return['title'] = lang('error');

				}else if(!$this->request('lastName')){
					$return['error'] = true;
					$return['msg'] = lang('sys_lastname_required');
                    $return['title'] = lang('error');

				}else if(!$this->request('name')){
					$return['error'] = true;
					$return['msg'] = lang('sys_company_name_required');
                    $return['title'] = lang('error');

				}else if(!$this->request('reg_num')){
					$return['error'] = true;
					$return['msg'] = lang('sys_company_registration_required');
                    $return['title'] = lang('error');

				}else if($this->request('tnc_comp_check') != '1'){
					$return['error'] = true;
					$return['msg'] = lang('sys_you_must_agree_to_our_tnc');
                    $return['title'] = lang('error');

				}else{
					$this->db->table("members");
					$this->db->insertArray(array(
						"firstname" => $this->request('firstName'),
						"lastname" => $this->request('lastName'),
						"email" => $this->request('email'),
						"type" => "1",
						"dashboard_widget" => "5,8,9,11",
						"status" => "1"
					));
					$this->db->insert();

					$member_id = $this->db->insertid();

					$this->db->table("company_details");
					$this->db->insertArray(array(
						"name" => $this->request('name'),
						"reg_num" => $this->request('reg_num'),
						"industry" => $this->request('industry'),
						"member_id" => $member_id
					));
					$this->db->insert();

					date_default_timezone_set("Asia/Kuala_Lumpur");

					$token = bin2hex(random_bytes(8));

					$this->db->table("temp_token");
					$this->db->insertArray(array(
						"token" => $token,
						"expiry_date" => date("Y-m-d H:i:s",strtotime ("+1 hour")),
						"email" => $this->request('email'),
					));
					$this->db->insert();

                    $mail = new Mail($this->db);
                    $to   = [
                        'email' => $this->request('email'),
                        'name'  => $this->request('name')
                    ];
                    $link = option('site_uri') . url_for('/set_password') . "?token={$token}{$redirect}";
                    $mail->sendmail($to, 'activate-your-account', ['link' => $link, 'first_name' => $this->request('name') ]);
					
					/*$subject = "MakeTimePay: Sign-up Succesful";
					$txt = "Greetings from MakeTimePay,<br><br>
					
					We're sending you this email because you recently signed-up with us. <br><br>
					
					Click <strong><a href='https://maketimepay.com/staging/set_password?token=" . $token . $redirect . "'>here</a></strong> to create a new password.<br><br>
					
					If you did not perform this, you may ignore this email.<br><br>		

					<b>Best Regards,</b><br>
					MakeTimePay";
					$headers = "";
					$headers .= "From: MTP <noreply@maketimepay.com> \r\n";
					$headers .= "Reply-To: noreply@maketimepay.com\r\n" ."X-Mailer: PHP/" . phpversion();
					$headers .= 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 
					mail($this->request('email'),$subject,$txt,$headers);*/

					$return['error'] = false;
					$return['msg'] = lang('sys_please_check_your_email');
                    $return['title'] = lang('success');
					//$return['url'] = "sendgrid?email=" . $this->request('email');
				}

			}else{
				$return['error'] = true;
				$return['msg'] = sprintf(lang('sys_email_exists'), $this->request('email'));
                $return['title'] = lang('error');
			}
		}
		
		return json($return);
	}

	public function my_company_profile(){
		$inclusion = [
		    "members" => "members",
		    "company_details" => "company_details"
		];

		$return['msg'] = "";
		foreach ($inclusion as $key => $value) {			
			if($key == "members"){
				$id_name = "id";
			}else if ($key == "company_details"){
				$id_name = "member_id";
			}

			foreach ($_POST[$key] as $keys => $values) {
				$this->db->table($key);
				$this->db->updateArray(array(
					$keys => $values,
				));
				$this->db->whereArray(array(
					$id_name => $_SESSION["member_id"], 
				));
				$this->db->update();
			}
		}

		$return['msg'] = lang('sys_company_profile_update_success');
        $return['title'] = lang('success');
		$return['error'] = false;
		$return['url'] = url_for('/dashboard');
		return json_encode($return);
	}

	public function personalize(){
        $skills = sanitize($this->request('skills'));
        $skill_ids = [];
        if( $skills ) {
            $parts     = explode(',', $skills);
            $skill_ids = array_filter($parts, function ($skill) {
                return stripos($skill, ':') === false;
            });
            $new_parts = array_diff($parts, $skill_ids);
            $new       = array_values(array_map(function ($skill) {
                list($key, $value) = explode(':', $skill, 2);
                return sanitize($value);
            }, $new_parts));

            if (!empty($new)) {
                $new_ids = [];
                foreach ($new as $skill) {
                    $this->db->query("SELECT * FROM `skills` WHERE `name` LIKE '{$skill}' LIMIT 1");
                    $row = $this->db->getSingleRow();
                    if (empty($row)) {
                        $this->db->table('skills');
                        $this->db->insertArray([
                            'name'   => $skill,
                            'new'    => '1',
                            'status' => '0'
                        ]);
                        $this->db->insert();
                        array_push($new_ids, $this->db->insertid());
                    } else {
                        array_push($new_ids, $row['id']);
                    }
                }
                $skill_ids = array_merge($skill_ids, $new_ids);
            }
        }
        $skills_list = !empty($skill_ids) ? implode(',', $skill_ids) : '';

        $interests = sanitize($this->request('interests'));
        $interest_ids = [];
        if( $interests ) {
            $parts     = explode(',', $interests);
            $interest_ids = array_filter($parts, function ($interest) {
                return stripos($interest, ':') === false;
            });
            $new_parts = array_diff($parts, $interest_ids);
            $new       = array_values(array_map(function ($interest) {
                list($key, $value) = explode(':', $interest, 2);
                return sanitize($value);
            }, $new_parts));

            if (!empty($new)) {
                $new_ids = [];
                foreach ($new as $interest) {
                    $this->db->query("SELECT * FROM `interest` WHERE `title` LIKE '{$interest}' LIMIT 1");
                    $row = $this->db->getSingleRow();
                    if (empty($row)) {
                        $this->db->table('interest');
                        $this->db->insertArray([
                            'title'   => $interest,
                            'new'    => '1',
                            'status' => '0'
                        ]);
                        $this->db->insert();
                        array_push($new_ids, $this->db->insertid());
                    } else {
                        array_push($new_ids, $row['id']);
                    }
                }
                $interest_ids = array_merge($interest_ids, $new_ids);
            }
        }
        $interests_list = !empty($interest_ids) ? implode(',', $interest_ids) : '';

        $exists = $this->db->getValue("SELECT COUNT(`id`) FROM `user_interests` WHERE `user_id` = '{$this->user->id}'");

        $this->db->table('user_interests');
        if( $exists ){
            $this->db->updateArray([
                'interests' => $interests_list
            ]);
            $this->db->whereArray([
                'user_id' => $this->user->id
            ]);
            $this->db->update();
        }else{
            $this->db->insertArray([
                'user_id' => $this->user->id,
                'interests' => $interests_list
            ]);
            $this->db->insert();
        }

		$this->db->table('members');
		$this->db->updateArray(array(
			"skills" => $skills_list,
		));
		$this->db->whereArray(array(
			"id" => $this->user->id, 
		));
		$this->db->update();
	
		$return['msg'] = lang('success_msg');
        $return['title'] = lang('success');
		$return['error'] = false;

		$redirect = !empty($_SESSION[WEBSITE_PREFIX.'REDIRECT']) ? $_SESSION[WEBSITE_PREFIX.'REDIRECT'] : $this->cms->site_host . ROOTPATH . 'dashboard';
		$return['url'] = $redirect;

		return json_encode($return);
	}

	public function dashboard(){
        $dashboard_mode = request('dashboard_mode') ? 'work' : 'search';
        if( $dashboard_mode === 'search' ){
            $search_mode = mysqli_real_escape_string($this->db->connection, request('search_mode')) ?? 'latest';
            $this->db->table('preference');
            $this->db->updateArray(['dashboard_mode' => 'search', 'search_mode' => $search_mode]);
            $this->db->whereArray(['member_id' => $this->user->id]);
            $this->db->update();
        }else{
            $this->db->queryOrDie("UPDATE `preference` SET `dashboard_mode` = 'work' WHERE `member_id` = '{$this->user->id}' LIMIT 1");
            $this->db->table('members');
            $this->db->updateArray(array(
                "dashboard_widget" => implode(',', $this->request('dashboard_widget')),
            ));
            $this->db->whereArray(array(
                "id" => $this->user->id,
            ));
            $this->db->update();
        }

        unset($_SESSION['info']);

		$return['msg'] = lang('success');
        $return['title'] = lang('success');
		$return['error'] = false;

		return json_encode($return);
	}

	public function myProfile(){
 		$return['error'] = false;
 		$return['msg'] = lang('myprofile_saved');
        $return['title'] = lang('success');
		$member = $_POST['member'];
        //$skills = $member['skills'];
        //$skill_ids = [];
        /*if( $skills ) {
            $parts     = explode(',', $skills);
            $skill_ids = array_filter($parts, function ($skill) {
                return stripos($skill, ':') === false;
            });
            $new_parts = array_diff($parts, $skill_ids);
            $new       = array_values(array_map(function ($skill) {
                list($key, $value) = explode(':', $skill, 2);
                return sanitize($value);
            }, $new_parts));

            if (!empty($new)) {
                $new_ids = [];
                foreach ($new as $skill) {
                    $check_for_an_existing_skills = $this->db->query("SELECT * FROM `skills` WHERE `name` LIKE '{$skill}' LIMIT 1");
                    $row                          = $this->db->getSingleRow();
                    if (empty($row)) {
                        $this->db->table('skills');
                        $this->db->insertArray([
                            'name'   => $skill,
                            'new'    => '1',
                            'status' => '0'
                        ]);
                        $this->db->insert();
                        array_push($new_ids, $this->db->insertid());
                    } else {
                        array_push($new_ids, $row['id']);
                    }
                }
                $skill_ids = array_merge($skill_ids, $new_ids);
            }
        }*/
		//$member['skills'] = !empty($skill_ids) ? implode(',', $skill_ids) : '';
		$keys = array();
		foreach ($member as $key => $value) {
			if($key == 'dob') $value = $value ? date('Y-m-d', strtotime(str_replace('/', '-', $value))) : '';
			if($key == 'contact_method') $value = implode(',', $value);
			$keys[$key] = trim($value);
		}

		$this->db->table('members');
		$this->db->updateArray($keys);
		$this->db->whereArray(array(
			"id" => $this->user->id, 
		));
		$this->db->update();

		$this->db->queryOrDie("DELETE FROM member_docs WHERE member_id = " . $this->db->escape($this->user->id) );
		$docs = $this->request('docs') ? array_filter($this->request('docs')) : '';

		if($docs){
			foreach($docs as $type => $srcs){
				foreach($srcs as $src){
					$this->db->table('member_docs');
					$this->db->insertArray(array(
						"member_id" => $this->user->id,
						"type" => $type,
						"src" => $src
					));
					$this->db->insert();
				}
			}
		}

		$preference = $this->request('preference');
		
		if($preference){
			$this->db->table('preference');
			$this->db->updateArray($preference);
			$this->db->whereArray(array(
				"member_id" => $this->user->id, 
			));
			$this->db->update();
		}

		unset($_SESSION['info']);//$return['url'] = url_for('/dashboard');

        if( $this->user->profileCompleteness() >= 100 ){
            Event::trigger('user.profile.complete', [$this->user->info]);
        }
		$return['url'] = url_for('/dashboard');
        unset($_SESSION['info']);
		return json_encode($return);
	}

	public function myCV(){
 		$return['error'] = false;
 		$return['msg'] = lang('myprofile_saved');
        $return['title'] = lang('success');
		$member = $_POST['member'];
        $skills = $member['skills'];
        $skill_ids = [];
        if( $skills ) {
            $parts     = explode(',', $skills);
            $skill_ids = array_filter($parts, function ($skill) {
                return stripos($skill, ':') === false;
            });
            $new_parts = array_diff($parts, $skill_ids);
            $new       = array_values(array_map(function ($skill) {
                list($key, $value) = explode(':', $skill, 2);
                return sanitize($value);
            }, $new_parts));

            if (!empty($new)) {
                $new_ids = [];
                foreach ($new as $skill) {
                    $check_for_an_existing_skills = $this->db->query("SELECT * FROM `skills` WHERE `name` LIKE '{$skill}' LIMIT 1");
                    $row                          = $this->db->getSingleRow();
                    if (empty($row)) {
                        $this->db->table('skills');
                        $this->db->insertArray([
                            'name'   => $skill,
                            'new'    => '1',
                            'status' => '0'
                        ]);
                        $this->db->insert();
                        array_push($new_ids, $this->db->insertid());
                    } else {
                        array_push($new_ids, $row['id']);
                    }
                }
                $skill_ids = array_merge($skill_ids, $new_ids);
            }
        }
		$member['skills'] = !empty($skill_ids) ? implode(',', $skill_ids) : '';

        $interests = $member['interests'];
        $interest_ids = [];
        if( $interests ) {
            $parts     = explode(',', $interests);
            $interest_ids = array_filter($parts, function ($interest) {
                return stripos($interest, ':') === false;
            });
            $new_parts = array_diff($parts, $interest_ids);
            $new       = array_values(array_map(function ($interest) {
                list($key, $value) = explode(':', $interest, 2);
                return sanitize($value);
            }, $new_parts));

            if (!empty($new)) {
                $new_ids = [];
                foreach ($new as $interest) {
                    $this->db->query("SELECT * FROM `interest` WHERE `title` LIKE '{$interest}' LIMIT 1");
                    $row = $this->db->getSingleRow();
                    if (empty($row)) {
                        $this->db->table('interest');
                        $this->db->insertArray([
                            'title'   => $interest,
                            'new'    => '1',
                            'status' => '0'
                        ]);
                        $this->db->insert();
                        array_push($new_ids, $this->db->insertid());
                    } else {
                        array_push($new_ids, $row['id']);
                    }
                }
                $interest_ids = array_merge($interest_ids, $new_ids);
            }
        }
        $interests_list = !empty($interest_ids) ? implode(',', $interest_ids) : '';

        $exists = $this->db->getValue("SELECT COUNT(`id`) FROM `user_interests` WHERE `user_id` = '{$this->user->id}'");

        $this->db->table('user_interests');
        if( $exists ){
            $this->db->updateArray([
                'interests' => $interests_list
            ]);
            $this->db->whereArray([
                'user_id' => $this->user->id
            ]);
            $this->db->update();
        }else{
            $this->db->insertArray([
                'user_id' => $this->user->id,
                'interests' => $interests_list
            ]);
            $this->db->insert();
        }
        unset($member['interests']);

		$keys = array();
		foreach ($member as $key => $value) {
			if($key == 'dob') $value = $value ? date('Y-m-d', strtotime(str_replace('/', '-', $value))) : '';
			if($key == 'contact_method') $value = implode(',', $value);
			$keys[$key] = trim($value);
		}

		$this->db->table('members');
		$this->db->updateArray($keys);
		$this->db->whereArray(array(
			"id" => $this->user->id, 
		));
		$this->db->update();

		$this->db->queryOrDie("DELETE FROM member_docs WHERE member_id = " . $this->db->escape($this->user->id) );
		$docs = $this->request('docs') ? array_filter($this->request('docs')) : '';

		if($docs){
			foreach($docs as $type => $srcs){
				foreach($srcs as $src){
					$this->db->table('member_docs');
					$this->db->insertArray(array(
						"member_id" => $this->user->id,
						"type" => $type,
						"src" => $src
					));
					$this->db->insert();
				}
			}
		}

		$preference = $this->request('preference');
		
		if($preference){
			$this->db->table('preference');
			$this->db->updateArray($preference);
			$this->db->whereArray(array(
				"member_id" => $this->user->id, 
			));
			$this->db->update();
		}

        unset($_SESSION['info']);
		//$return['url'] = url_for('/dashboard');

        if( $this->user->profileCompleteness() >= 100 ){
            Event::trigger('user.profile.complete', [$this->user->info]);
        }
		$return['url'] = url_for('/dashboard');
		return json_encode($return);
	}

	public function myCompanyProfileSignup(){
		$photos = $this->request('photos') ? array_filter($this->request('photos')) : array();
		$validEmail = filter_var(strtolower($this->request('company_email')), FILTER_VALIDATE_EMAIL);

		/*if(!$this->request('photo')){
			$return['error'] = true;
			$return['msg'] = lang('sys_profile_avatar_required');
            $return['title'] = lang('error');

		}*/if(!$this->request('mobile_number')){
			$return['error'] = true;
			$return['msg'] = lang('sys_contact_mobile_required');
            $return['title'] = lang('error');

		}/* else if(!$this->request('contact_number')){
			$return['error'] = true;
			$return['msg'] = lang('sys_contact_phone_required');
            $return['title'] = lang('error');

		} */else if(!$validEmail){
			$return['error'] = true;
			$return['msg'] = lang('sys_invalid_company_email');
            $return['title'] = lang('error');
			
		}else if(!$this->request('company_phone')){
			$return['error'] = true;
			$return['msg'] = lang('sys_company_phone_required');
            $return['title'] = lang('error');

		}else{

			$this->db->table('members');
			$this->db->updateArray(array(
				"firstname" => $this->request('firstname'),
				"lastname" => $this->request('lastname'),
				"nric" => $this->request('nric'),
				"contact_number" => $this->request('contact_number'),
				"mobile_number" => $this->request('mobile_number'),
				"address1" => $this->request('address1'),
				"address2" => $this->request('address2'),
				"zip" => $this->request('zip'),
				"city" => $this->request('city'),
				"country" => $this->request('country'),
				"state" => $this->request('state'),
				"nationality" => $this->request('nationality'),
				"photo" => $this->request('photo'),
			));
			$this->db->whereArray(array(
				"id" => $this->user->id, 
			));
			$this->db->update();

			$url = preg_replace("/[^a-zA-Z\-]/", '', strtolower($this->request('url')));

			if( ! $this->cms->checkCompanyUrlAvailability($url) ){
			    $url = preg_replace("/[^a-zA-Z\-]/", '', strtolower($this->request('company_name')));
			    $url = $this->cms->checkCompanyUrlAvailability($url) ? $url : null;
            }

			$this->db->table('company_details');
			$this->db->updateArray(array(
				"name" => $this->request('company_name'),
				"reg_num" => $this->request('reg_num'),
				"email" => $this->request('company_email'),
				"company_phone" => $this->request('company_phone'),
				"about_us" => $this->request('about_us'),
				"industry" => $this->request('industry'),
				"url" => $url,
				"photos" => serialize($photos),
			));
			$this->db->whereArray(array(
				"member_id" => $this->user->id, 
			));
			$this->db->update();

			$preference = $this->request('preference');

            $preference['survey'] = isset($preference['survey']) ? 1 : 0;
            $preference['dashboard_mode'] = 'work';

            if(isset($preference['acc_num']) && !empty($preference['acc_num']) && strlen($preference['acc_num']) < 6){
                $return['error'] = true;
                $return['msg'] = lang('sys_invalid_bank_account_no');
                $return['title'] = lang('error');

            }else {

                if($preference){
                    $this->db->table('preference');
                    $this->db->updateArray($preference);
                    $this->db->whereArray(array(
                        "member_id" => $this->user->id,
                    ));
                    $this->db->update();
                }

                $return['error'] = false;
                $return['msg']   = lang('myprofile_saved');
                $return['title'] = lang('success');
                $return['url']   = url_for('/dashboard');
            }
		}

		return json_encode($return);
	}

	public function ajaxUploadAttachment($id = 0, $task_id ){
		$batch_id = uniqid();
		$max_filesize = $this->settings()['chat_max_size'] ? $this->settings()['chat_max_size'] * 1048576 : 0;

		if(isset($_POST['content'])){
			$this->db->table("chats");
			$this->db->insertArray(array(
				"sender_id" => $this->user->id,
				"receiver_id" => $id,
				"contents" => $_POST['content'],
				"task_id" => $task_id,
				"type" => "textMix",
				"size" => $eachFile['size'],
				"batch_id" => $batch_id,
				"reply_id" => $_POST['replyID'],
				"file_extension" => $file_extension,
				"created_by" => "NOW()",
			));
			$this->db->insert();

			foreach($_FILES as $eachFile){
				if($eachFile['size'] < $max_filesize){
					$filename = $eachFile['name'];

					if (!file_exists("files/chats/".$task_id)) {
						mkdir("files/chats/".$task_id, 0777, true);
					}

					$location = "files/chats/".$task_id."/".$filename;
					$file_extension = pathinfo($location,PATHINFO_EXTENSION);
					$file_extension = strtolower($file_extension);

					$valid_img_extensions = array('jpg','jpeg','png','gif','bmp');
					$valid_file_extensions = explode(',', $this->settings()['chat_file_extensions']);
					$valid_extensions = array_merge($valid_img_extensions,$valid_file_extensions);


					if(in_array(strtolower($file_extension), $valid_extensions)) {
						/* Upload file */
						if(move_uploaded_file($eachFile['tmp_name'],$location)){
							$response = $location;
						}
					}

					if(in_array($file_extension, $valid_img_extensions)){
						$this->db->table("chats");
						$this->db->insertArray(array(
							"sender_id" => $this->user->id,
							"receiver_id" => $id,
							"contents" => $location,
							"task_id" => $task_id,
							"type" => "imageMix",
							"size" => $eachFile['size'],
							"batch_id" => $batch_id,
							"reply_id" => $_POST['replyID'],
							"file_extension" => $file_extension,
							"created_by" => "NOW()",
						));
						$this->db->insert();


					}else if (in_array($file_extension, $valid_file_extensions)){
						$this->db->table("chats");
						$this->db->insertArray(array(
							"sender_id" => $this->user->id,
							"receiver_id" => $id,
							"contents" => $location,
							"task_id" => $task_id,
							"type" => "fileMix",
							"size" => $eachFile['size'],
							"batch_id" => $batch_id,
							"reply_id" => $_POST['replyID'],
							"file_extension" => $file_extension,
							"created_by" => "NOW()",
						));
						$this->db->insert();
					}
				}
			}

		}else{

			foreach($_FILES as $eachFile){
				if($eachFile['size'] < $max_filesize){
					$filename = $eachFile['name'];

					if (!file_exists("files/chats/".$task_id)) {
						mkdir("files/chats/".$task_id, 0777, true);
					}
			
					$location = "files/chats/".$task_id."/".$filename;
					$file_extension = pathinfo($location, PATHINFO_EXTENSION);
					$file_extension = strtolower($file_extension);

					$valid_img_extensions = array('jpg','jpeg','png','gif','bmp');
					$valid_file_extensions = explode(',', $this->settings()['chat_file_extensions']);
					$valid_extensions = array_merge($valid_img_extensions,$valid_file_extensions);

					if(in_array(strtolower($file_extension), $valid_extensions)) {
						/* Upload file */
						if(move_uploaded_file($eachFile['tmp_name'],$location)){
							$response = $location;
						}
					}

					if(in_array($file_extension, $valid_img_extensions)){
						$this->db->table("chats");
						$this->db->insertArray(array(
							"sender_id" => $this->user->id,
							"receiver_id" => $id,
							"contents" => $location,
							"task_id" => $task_id,
							"type" => "image",
							"size" => $eachFile['size'],
							"batch_id" => $batch_id,
							"reply_id" => $_POST['replyID'],
							"file_extension" => $file_extension,
							"created_by" => "NOW()",
						));
					$this->db->insert();

					}else if (in_array($file_extension, $valid_file_extensions)){
						if($_POST['replyID']!= ""){
							$type = "fileMix";
						}else{
							$type = "file";
						}

						$this->db->table("chats");
						$this->db->insertArray(array(
						"sender_id" => $this->user->id,
						"receiver_id" => $id,
						"contents" => $location,
						"task_id" => $task_id,
						"type" => $type,
						"size" => $eachFile['size'],
						"reply_id" => $_POST['replyID'],
						"batch_id" => $batch_id,
						"file_extension" => $file_extension,
						"created_by" => "NOW()",
						));
						$this->db->insert();
					}
				}
			}
		}
		 //$filename = $_FILES['file']['name'];
	}

	public function ajaxRemoveChatMesssage($id){
		if(is_numeric($id)){
			$idIdentifier = "id";
		}else {
			$idIdentifier = "batch_id";
		}
		$this->db->table('chats');
		$this->db->updateArray(array(
			"deleted" => "1",
		));
		$this->db->whereArray(array(
			$idIdentifier => $id,
			"sender_id" => $this->user->id,
		));
		$this->db->update();

	}

	public function my_profile_employment_add(){
		$employment = $this->request('employment');
		$employment['industry'] = !empty($employment['industry']) ? $employment['industry'] : '';
		$employment['currently_working'] = !empty($employment['currently_working']) ? 1 : 0;
		$employment['date_end'] = !empty($employment['date_end']) ? (!$employment['currently_working'] ? $employment['date_end'] : '') : '';
		$employment['date_start'] = date('Y-m-d', strtotime(str_replace('/', '-', $employment['date_start'])));
		$employment['date_end'] = $employment['date_end'] ? date('Y-m-d', strtotime(str_replace('/', '-', $employment['date_end']))) : '';
		
		if(!$employment['company_name']){
			$return['error'] = true;
			$return['msg'] = lang('sys_company_name_required');
            $return['title'] = lang('error');

		}else if(!$employment['position']){
			$return['error'] = true;
			$return['msg'] = lang('sys_position_required');
            $return['title'] = lang('error');

		}else if(!$employment['date_start']){
			$return['error'] = true;
			$return['msg'] = lang('sys_start_date_required');
            $return['title'] = lang('error');

		}else if(!$employment['currently_working'] && !$employment['date_end']){
			$return['error'] = true;
			$return['msg'] = lang('sys_end_date_required');
            $return['title'] = lang('error');

		}else if(!$employment['department']){
			$return['error'] = true;
			$return['msg'] = lang('sys_department_required');
            $return['title'] = lang('error');

		}else if(!$employment['industry']){
			$return['error'] = true;
			$return['msg'] = lang('sys_industry_is_required');
            $return['title'] = lang('error');

		}else if(!$employment['job_desc']){
			$return['error'] = true;
			$return['msg'] = lang('sys_job_desc_required');
            $return['title'] = lang('error');

		}else if(!$employment['job_responsibilities']){
			$return['error'] = true;
			$return['msg'] = lang('sys_job_responsibility_required');
            $return['title'] = lang('error');

		}else if(!$employment['country']){
			$return['error'] = true;
			$return['msg'] = lang('sys_country_required');
            $return['title'] = lang('error');

		}else if(!$employment['state']){
			$return['error'] = true;
			$return['msg'] = lang('sys_state_required');
            $return['title'] = lang('error');

		}else if(!$employment['remuneration']){
			$return['error'] = true;
			$return['msg'] = lang('sys_remuneration_required');
            $return['title'] = lang('error');

		}else{

			$this->db->table('employment_details');
			$this->db->insertArray(array(
				"company_name" => $employment['company_name'],
				"position" => $employment['position'],
				"date_start" => $employment['date_start'],
				"date_end" => $employment['date_end'],
				"currently_working" => $employment['currently_working'],
				"department" => $employment['department'],
				"industry" => $employment['industry'],
				"job_desc" => $employment['job_desc'],
				"job_responsibilities" => $employment['job_responsibilities'],
				"country" => $employment['country'],
				"state" => $employment['state'],
				"remuneration" => $employment['remuneration'],
				"member_id" => $this->user->id,
			));
			$this->db->insert();

			$id = $this->db->insertid();

			$this->db->query("SELECT * FROM employment_details WHERE id = " . $this->db->escape($id));
			$employment = $this->db->getSingleRow();

			$html = '
			<div class="row-form-data" id="emp-' . $id . '">
				<div class="talent-details-workxp-row">
					<div class="talent-details-workxp-compname">' . $employment['company_name'] . '</div>
					<div class="talent-details-workxp-pos">' . $employment['position'] . ', ' .  $this->cms->getIndustryName($employment['industry']) . '</div>
					<div class="talent-details-workxp-pos-duration">
						<div class="talent-details-workxp-location">' . ($employment['state'] ? $this->cms->getStateName($employment['state']) . ', ' : '') . $this->cms->getCountryName($employment['country']) . '</div>
						<div class="talent-details-workxp-duration">' . dateRangeToDays($employment['date_start'], $employment['date_end']) . '</div>
					</div>

					<div class="talent-details-workxp-resp">
						<span class="talent-details-workxp-list-lbl"><b>'. lang('cv_responsibilities') .'</b></span>
						<div>' . nl2br($employment['job_responsibilities']) . '</div>
					</div>
					<div class="talent-details-workxp-resp">
						<span class="talent-details-workxp-list-lbl"><b>'. lang('cv_description') .'</b></span>
						<div>' . nl2br($employment['job_desc']) . '</div>
					</div>';

					$employment['date_start'] = date('d/m/Y', strtotime($employment['date_start']));
					$employment['date_end'] = date('d/m/Y', strtotime($employment['date_end']));

					$html .= '
					<div class="edit-in-modal">
                        <a href="#" class="edit-employment" data-data="' . htmlspecialchars(json_encode($employment)) . '">'. lang('cv_edit') .'</a>
                        <a href="#" class="remove-employment" data-id="' . $employment['id'] . '"><i class="far fa-trash-alt"></i></a>
                    </div>
				</div>';

			$return['msg'] = lang('sys_added');
            $return['title'] = lang('success');
			$return['error'] = false;
			$return['html'] = $html;
		}

		return json_encode($return);
	}

	public function my_profile_employment_edit(){
		$employment = $this->request('employment');
		$employment['industry'] = $employment['industry'] ? $employment['industry'] : '';
		$employment['currently_working'] = !empty($employment['currently_working']) ? 1 : 0;
		$employment['date_end'] = !empty($employment['date_end']) ? (!$employment['currently_working'] ? $employment['date_end'] : '') : '';
		$employment['date_start'] = date('Y-m-d', strtotime(str_replace('/', '-', $employment['date_start'])));
		$employment['date_end'] = $employment['date_end'] ? date('Y-m-d', strtotime(str_replace('/', '-', $employment['date_end']))) : '';

		if(!$employment['company_name']){
			$return['error'] = true;
			$return['msg'] = lang('sys_company_name_required');
            $return['title'] = lang('error');

		}else if(!$employment['position']){
			$return['error'] = true;
			$return['msg'] = lang('sys_position_required');
            $return['title'] = lang('error');

		}else if(!$employment['date_start']){
			$return['error'] = true;
			$return['msg'] = lang('sys_start_date_required');
            $return['title'] = lang('error');

		}else if(!$employment['currently_working'] && !$employment['date_end']){
			$return['error'] = true;
			$return['msg'] = lang('sys_end_date_required');
            $return['title'] = lang('error');

		}else if(!$employment['department']){
			$return['error'] = true;
			$return['msg'] = lang('sys_department_required');
            $return['title'] = lang('error');

		}else if(!$employment['industry']){
			$return['error'] = true;
			$return['msg'] = lang('sys_industry_is_required');
            $return['title'] = lang('error');

		}else if(!$employment['job_desc']){
			$return['error'] = true;
			$return['msg'] = lang('sys_job_desc_required');
            $return['title'] = lang('error');

		}else if(!$employment['job_responsibilities']){
			$return['error'] = true;
			$return['msg'] = lang('sys_job_responsibility_required');
            $return['title'] = lang('error');

		}else if(!$employment['country']){
			$return['error'] = true;
			$return['msg'] = lang('sys_country_required');
            $return['title'] = lang('error');

		}else if(!$employment['state']){
			$return['error'] = true;
			$return['msg'] = lang('sys_state_required');
            $return['title'] = lang('error');

		}else if(!$employment['remuneration']){
			$return['error'] = true;
			$return['msg'] = lang('sys_remuneration_required');
            $return['title'] = lang('error');

		}else{

			$this->db->table('employment_details');
			$this->db->updateArray(array(
				"company_name" => $employment['company_name'],
				"position" => $employment['position'],
				"date_start" => $employment['date_start'],
				"date_end" => $employment['date_end'],
				"currently_working" => $employment['currently_working'],
				"department" => $employment['department'],
				"industry" => $employment['industry'],
				"job_desc" => $employment['job_desc'],
				"job_responsibilities" => $employment['job_responsibilities'],
				"country" => $employment['country'],
				"state" => $employment['state'],
				"remuneration" => $employment['remuneration'],
			));
			$this->db->whereArray(array(
				"id" => $employment['id'],
				"member_id" => $this->user->id,
			));
			$this->db->update();

			$html = '
			<div class="row-form-data" id="emp-' . $employment['id'] . '">
				<div class="talent-details-workxp-row">
					<div class="talent-details-workxp-compname">' . $employment['company_name'] . '</div>
					<div class="talent-details-workxp-pos">' . $employment['position'] . ', ' .  $this->cms->getIndustryName($employment['industry']) . '</div>
					<div class="talent-details-workxp-pos-duration">
						<div class="talent-details-workxp-location">' . ($employment['state'] ? $this->cms->getStateName($employment['state']) . ', ' : '') . $this->cms->getCountryName($employment['country']) . '</div>
						<div class="talent-details-workxp-duration">' . dateRangeToDays($employment['date_start'], $employment['date_end']) . '</div>
					</div>
					<div class="talent-details-workxp-resp">
						<span class="talent-details-workxp-list-lbl"><b>'. lang('cv_responsibilities') .'</b></span>
						<div>' . nl2br($employment['job_responsibilities']) . '</div>
					</div>
					<div class="talent-details-workxp-resp">
						<span class="talent-details-workxp-list-lbl"><b>'. lang('cv_description') .'</b></span>
						<div>' . nl2br($employment['job_desc']) . '</div>
					</div>
					<div class="edit-in-modal">
                        <a href="#" class="edit-employment" data-data="' . htmlspecialchars(json_encode($employment)) . '">'. lang('cv_edit') .'</a>
                        <a href="#" class="remove-employment" data-id="' . $employment['id'] . '"><i class="far fa-trash-alt"></i></a>
                    </div>
				</div>';

			$return['msg'] = lang('sys_employment_info_updated');
            $return['title'] = lang('success');
			$return['error'] = false;
			$return['html'] = $html;
			$return['id'] = $employment['id'];
		}

		return json_encode($return);
	}

	public function my_profile_employment_delete(){
		$return['error'] = true;
		$return['msg'] = "Invalid ID.";
        $return['title'] = lang('error');

		if($this->request('id')){
			$this->db->table('employment_details');
			$this->db->whereArray(array(
				"id" => $this->request('id'),
				"member_id" => $this->user->id,
			));
			$this->db->delete();

			$return['error'] = false;
			$return['msg'] = lang('sys_employment_info_deleted');
            $return['title'] = lang('success');
		}

		return json_encode($return);
	}

	public function my_profile_education_add(){
		$education = $this->request('education');
		$education['highest_edu_level'] = !empty($education['highest_edu_level']) ? $education['highest_edu_level'] : '';
		$education['field'] = !empty($education['field']) ? $education['field'] : '';
		
		if(!$education['edu_institution']){
			$return['error'] = true;
			$return['msg'] = lang('sys_education_insistution_required');
            $return['title'] = lang('error');

		}else if(!$education['highest_edu_level']){
			$return['error'] = true;
			$return['msg'] = lang('sys_highest_education_level_required');
            $return['title'] = lang('error');

		}else if(!$education['field']){
			$return['error'] = true;
			$return['msg'] = lang('sys_field_of_study_required');
            $return['title'] = lang('error');

		}else if(!$education['grad_year']){
			$return['error'] = true;
			$return['msg'] = lang('sys_graduation_year_required');
            $return['title'] = lang('error');

		}else if(!$education['major']){
			$return['error'] = true;
			$return['msg'] = lang('sys_major_required');
            $return['title'] = lang('error');

		}else if(!$education['grade']){
			$return['error'] = true;
			$return['msg'] = lang('sys_grade_required');
            $return['title'] = lang('error');

		}else if(!$education['country']){
			$return['error'] = true;
			$return['msg'] = lang('sys_country_required');
            $return['title'] = lang('error');

		}else if(!$education['state']){
			$return['error'] = true;
			$return['msg'] = lang('sys_state_required');
            $return['title'] = lang('error');

		}else{
			$this->db->table('education');
			$this->db->insertArray(array(
				"edu_institution" => $education['edu_institution'],
				"highest_edu_level" => $education['highest_edu_level'],
				"field" => $education['field'],
				"grad_year" => $education['grad_year'],
				"major" => $education['major'],
				"grade" => $education['grade'],
				"country" => $education['country'],
				"state" => $education['state'],
				"member_id" => $this->user->id,
			));
			$this->db->insert();

			$id = $this->db->insertid();

			$this->db->query("SELECT * FROM education WHERE id = " . $this->db->escape($id));
			$education = $this->db->getSingleRow();

			$html = '
				<div class="row-form-data" id="edu-' . $id . '">
					<div class="talent-details-edu-row">
						<div class="talent-details-edu-insname">' . $education['edu_institution'] . '</div>
						<div class="talent-details-edu-level-fieldstudy-grade">
							<div class="talent-details-edu-level">' . $education['highest_edu_level'] . '</div>
							<div class="talent-details-edu-fieldstudy">' . $education['field'] . '</div>
							<div class="talent-details-edu-fieldstudy">' . $education['major'] . '</div>
							<div class="talent-details-edu-grade">' . $education['grade'] . '</div>
						</div>
						<div class="talent-details-edu-location-year">
							<div class="talent-details-edu-location">' . ($education['state'] ? $this->cms->getStateName($education['state']) . ', '  : '') . $this->cms->getCountryName($education['country']) . '</div>
							<div class="talent-details-edu-year">' . $education['grad_year'] . '</div>
						</div>
						<div class="edit-in-modal">
                            <a href="#" class="edit-education" data-data="' . htmlspecialchars(json_encode($education)) . '">'. lang('cv_edit') .'</a>
                            <a href="#" class="remove-education" data-id="' . $education['id'] . '"><i class="far fa-trash-alt"></i></a>
                        </div>
					</div>
				</div>';

			$return['msg'] = lang('sys_added');
            $return['title'] = lang('success');
			$return['error'] = false;
			$return['html'] = $html;
		}

		return json_encode($return);
	}

	public function my_profile_education_edit(){
		$education = $this->request('education');
		$education['highest_edu_level'] = !empty($education['highest_edu_level']) ? $education['highest_edu_level'] : '';
		$education['field'] = !empty($education['field']) ? $education['field'] : '';
		
		if(!$education['edu_institution']){
			$return['error'] = true;
			$return['msg'] = lang('sys_education_insistution_required');
            $return['title'] = lang('error');

		}else if(!$education['highest_edu_level']){
			$return['error'] = true;
			$return['msg'] = lang('sys_highest_education_level_required');
            $return['title'] = lang('error');

		}else if(!$education['field']){
			$return['error'] = true;
			$return['msg'] = lang('sys_field_of_study_required');
            $return['title'] = lang('error');

		}else if(!$education['grad_year']){
			$return['error'] = true;
			$return['msg'] = lang('sys_graduation_year_required');
            $return['title'] = lang('error');

		}else if(!$education['major']){
			$return['error'] = true;
			$return['msg'] = lang('sys_major_required');
            $return['title'] = lang('error');

		}else if(!$education['grade']){
			$return['error'] = true;
			$return['msg'] = lang('sys_grade_required');
            $return['title'] = lang('error');

		}else if(!$education['country']){
			$return['error'] = true;
			$return['msg'] = lang('sys_country_required');
            $return['title'] = lang('error');

		}else if(!$education['state']){
			$return['error'] = true;
			$return['msg'] = lang('sys_state_required');
            $return['title'] = lang('error');

		}else{
			$this->db->table('education');
			$this->db->updateArray(array(
				"edu_institution" => $education['edu_institution'],
				"highest_edu_level" => $education['highest_edu_level'],
				"field" => $education['field'],
				"grad_year" => $education['grad_year'],
				"major" => $education['major'],
				"grade" => $education['grade'],
				"country" => $education['country'],
				"state" => $education['state'],
				"member_id" => $this->user->id,
			));
			$this->db->whereArray(array(
				"id" => $education['id'],
				"member_id" => $this->user->id,
			));
			$this->db->update();

			$html = '
				<div class="row-form-data" id="edu-' . $education['id'] . '">
					<div class="talent-details-edu-row">
						<div class="talent-details-edu-insname">' . $education['edu_institution'] . '</div>
						<div class="talent-details-edu-level-fieldstudy-grade">
							<div class="talent-details-edu-level">' . $education['highest_edu_level'] . '</div>
							<div class="talent-details-edu-fieldstudy">' . $education['field'] . '</div>
							<div class="talent-details-edu-fieldstudy">' . $education['major'] . '</div>
							<div class="talent-details-edu-grade">' . $education['grade'] . '</div>
						</div>
						<div class="talent-details-edu-location-year">
							<div class="talent-details-edu-location">' . ($education['state'] ? $this->cms->getStateName($education['state']) . ', '  : '') . $this->cms->getCountryName($education['country']) . '</div>
							<div class="talent-details-edu-year">' . $education['grad_year'] . '</div>
						</div>
						<div class="edit-in-modal">
                            <a href="#" class="edit-education" data-data="' . htmlspecialchars(json_encode($education)) . '">'. lang('cv_edit') .'</a>
                            <a href="#" class="remove-education" data-id="' . $education['id'] . '"><i class="far fa-trash-alt"></i></a>
                        </div>
					</div>
				</div>';

			$return['msg'] = lang('sys_education_info_updated');
            $return['title'] = lang('success');
			$return['error'] = false;
			$return['html'] = $html;
			$return['id'] = $education['id'];
		}

		return json_encode($return);
	}

	public function my_profile_education_delete(){
		$return['error'] = true;
		$return['msg'] = "Invalid ID.";
        $return['title'] = lang('error');

		if($this->request('id')){
			$this->db->table('education');
			$this->db->whereArray(array(
				"id" => $this->request('id'),
				"member_id" => $this->user->id,
			));
			$this->db->delete();

			$return['error'] = false;
			$return['msg'] = lang('sys_education_info_deleted');
            $return['title'] = lang('success');
		}

		return json_encode($return);
	}

	public function settingsPage(){
		$preference = $this->request('preference');
		$preference['rm_work_location'] = !empty($preference['rm_work_location']) ? $preference['rm_work_location'] : '';
		$preference['p_work_location'] = !empty($preference['p_work_location']) ? $preference['p_work_location'] : '';

		$preference['rm_country'] = $this->user->info['country'];
		$preference['p_country'] = $this->user->info['country'];

		if($preference['rm_work_location'] == 'near'){
			$preference['rm_state'] = '';

		}else if($preference['rm_work_location'] == 'anywhere'){
			$preference['rm_radius'] = '';
			$preference['rm_state'] = '';
			
		}else if($preference['rm_work_location'] == 'same_country'){
			$preference['rm_radius'] = '';
		}

		if($preference['p_work_location'] == 'near'){
			$preference['p_state'] = '';

		}else if($preference['p_work_location'] == 'anywhere'){
			$preference['p_radius'] = '';
			$preference['p_state'] = '';
			
		}else if($preference['p_work_location'] == 'same_country'){
			$preference['p_radius'] = '';
		}

		$preference['work_day_weekday'] = !empty($preference['work_day_weekday']) ? 1 : 0;
		$preference['work_day_weekday_full'] = !empty($preference['work_day_weekday_full']) ? 1 : 0;
		$preference['work_day_weekday_after'] = !empty($preference['work_day_weekday_after']) ? 1 : 0;
		$preference['work_day_weekend'] = !empty($preference['work_day_weekend']) ? 1 : 0;
		$preference['work_day_weekend_am'] = !empty($preference['work_day_weekend_am']) ? 1 : 0;
		$preference['work_day_weekend_pm'] = !empty($preference['work_day_weekend_pm']) ? 1 : 0;
		$preference['survey'] = isset($preference['survey']) ? 1 : 0;

		$preference['work_load_hours'] = !empty($preference['work_load_hours']) ? ($preference['work_load_hours'] <= 70 ? $preference['work_load_hours'] : 70) : 1;

		if(strlen($preference['acc_num']) < 6){
			$return['error'] = true;
			$return['msg'] = lang('sys_invalid_bank_account_no');
            $return['title'] = lang('error');
			
		}else{

            if($preference){
                $this->db->table('preference');
                $this->db->updateArray($preference);
                $this->db->whereArray(array(
                    "member_id" => $this->user->id,
                ));
                $this->db->update();
            }

			$return['msg'] = lang('myprofile_saved');
            $return['title'] = lang('success');
			$return['error'] = false;
			$return['url'] = url_for('/dashboard');

            unset($_SESSION['info']);

			if( $this->user->profileCompleteness() >= 100 ){
				Event::trigger('user.profile.complete', [$this->user->info]);
			}
		}

		return json_encode($return);
	}

	// end of Eric's Code
		
	public function sort_array_of_array(&$array, $subfield, $sort = 'asc'){
		$sortarray = array();
		foreach ($array as $key => $row)
		{
			$sortarray[$key] = $row[$subfield];
		}
		if($sort == 'asc') array_multisort($sortarray, SORT_ASC, $array);
		else array_multisort($sortarray, SORT_DESC, $array);
	}
		
	public function _upload($type = 'photo'){
		$return['error'] = false;
		
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		@set_time_limit(5 * 60);
		
		$targetDir = 'files/' . $type . '/' . $this->user->id . '/';
		$cleanupTargetDir = true;
		$maxFileAge = 5 * 3600;
				
		if (!file_exists($targetDir)) createPath($targetDir);
		//if (!file_exists($targetDir)) @mkdir($targetDir);
		//array_map('unlink', glob($targetDir . "*"));
		
		$filename = basename($_FILES["file"]["name"]);
		$original = explode('.', $filename);
		$extension = array_pop($original);

		$docs = array('resume', 'cover_letter', 'cert');
		$docs_extensions = explode(',', $this->settings()['docs_file_extensions']);
		$avatar_extensions = explode(',', $this->settings()['avatar_file_extensions']);
		
		if(!in_array($type, $docs) && !in_array(strtolower($extension), array('jpg','jpeg','gif','png'))){
			$return['error'] = true;
			$return['msg'] = sprintf(lang('sys_upload_file_error'), $filename);
            $return['title'] = lang('error');

		}else if(in_array($type, $docs) && !in_array(strtolower($extension), $docs_extensions)){
			$return['error'] = true;
			$return['msg'] = sprintf(lang('sys_upload_extension_error'), $filename, $extension);
			$return['title'] = lang('error');
			
		}else if(in_array($type, ['profile','photo','company_gallery'])  && !in_array(strtolower($extension), $avatar_extensions)){
			$return['error'] = true;
			$return['msg'] = sprintf(lang('sys_upload_extension_error'), $filename, $extension);
			$return['title'] = lang('error');
		}
		
		if($return['error']){
			die(json($return));
		}
		
		$fileName = current($original) . '_' . uniqid('') . '.' . $extension;
		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
		$src = $targetDir . $fileName;
		$src = str_replace('../', '', $src);
		
		$chunks = $this->request('chunks') ? intval($this->request('chunks')) : 0;
		
		if ($cleanupTargetDir) {
			if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
				$return['error'] = true;
				$return['msg'] = lang('sys_upload_temp_dir_error');
                $return['title'] = lang('error');
			}
		
			while (($file = readdir($dir)) !== false) {
				$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
		
				if ($tmpfilePath == "{$filePath}.part") {
					continue;
				}
		
				if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
					@unlink($tmpfilePath);
				}
			}
			closedir($dir);
		}	
		
		if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
			$return['error'] = true;
			$return['msg'] = lang('sys_upload_output_error');
            $return['title'] = lang('error');
		}
		
		if (!empty($_FILES)) {
			if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
				$return['error'] = true;
				$return['msg'] = lang('sys_upload_move_error');
                $return['title'] = lang('error');
			}
		
			if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
				$return['error'] = true;
				$return['msg'] = lang('sys_upload_input_error');
                $return['title'] = lang('error');
			}
		} else {	
			if (!$in = @fopen("php://input", "rb")) {
				$return['error'] = true;
				$return['msg'] = lang('sys_upload_input_error');
                $return['title'] = lang('error');
			}
		}

		if($return['error']){
			die(json($return));
		}
		
		while ($buff = fread($in, 4096)) {
			fwrite($out, $buff);
		}
		
		@fclose($out);
		@fclose($in);
		
		if (!$chunks || $chunk == $chunks - 1) {
			rename("{$filePath}.part", $filePath);
		}
		
		$html = '';
		
		if(in_array($type, array('photo','company_gallery'))){			
			$html = '<li><input type="hidden" name="photos[]" value="' . $src. '"><img src="' . imgCrop($src, 125, 100) . '"><a href="#" class="remove-image"><i class="fa fa-times"></i></a></li>';
		}

		if($type == 'profile'){			
			$html = '<img src="' . imgCrop($src, 100, 100) . '" class="profile-photo">';
			$return['thumb'] = imgCrop($src, 100, 100);
		}

		if(in_array($type, $docs)){
			$original = explode('.', $fileName);
            $ext = array_pop($original);
			$name = strlen($fileName) < 25 ? $fileName : substr($original[0], 0, 25) . '...';
			 
			$html = '<li><a href="' . url_for($src) . '" target="_blank" data-title="' . $fileName . '">' . $name . '</a> <a href="#" class="remove-docs" data-toggle="tooltip" title="' . lang('cv_remove') . '"><i class="far fa-trash-alt"></i></a><input type="hidden" name="docs[' . $type . '][]" value="' . $src . '"></li>';
		}

		$return['src'] = $src;
		$return['html'] = $html;
		$return['msg'] = lang('sys_upload_success');
        $return['title'] = lang('success');

		echo json($return);
	}
}
?>