<?php

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../helpers.php';

use Carbon\Carbon;

class Job
{
    protected $db;
    protected $user;
    protected $date;

    /**
     * @var $db db
     * @var $user user
     * @return Job
     */
    public static function init($db, $user){
        return new static($db, $user);
    }

    /**
     * @var $db db
     * @var $user user
     */
    public function __construct($db, $user){
        $this->db = $db;

        if( isset($_SESSION['member_id']) && !empty($_SESSION['member_id']) ){
            $_SESSION[WEBSITE_PREFIX.'USER_ID'] = $_SESSION['member_id'];
        }

        $this->user = $user;
        date_default_timezone_set('Asia/Kuala_Lumpur');

        if( !isset($_SESSION[WEBSITE_PREFIX.'LANGUAGE']) || $_SESSION[WEBSITE_PREFIX.'LANGUAGE'] == 'EN' ){
            $locale = ['English', 'en_GB'];
        }else{
            $locale = ['Malay', 'ms_MY'];
        }

        setLocale(LC_TIME, $locale[1]);
        Carbon::setLocale($locale[1]);
        $this->date = Carbon::today();
    }

    private function is_logged_in(){
        if((!isset($this->user->info['email']) || empty($this->user->info['email'])) &&
            (!isset($this->user->info['status']) || $this->user->info['status'] !== '1')) {
            //halt(HTTP_UNAUTHORIZED, "This area is for logged in users!");
        }
        return true;
    }

    private function is_ajax(){
        return 'xmlhttprequest' === strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '' );
    }

    public function create(){
        //$redirect_to = $_SERVER['HTTP_REFERER'] ? url_for($_SERVER['HTTP_REFERER']) : option('site_uri') . url_for('/workspace');
        $redirect_to = option('site_uri') . url_for('/workspace/job-centre');
        $this->is_logged_in();
        $completeness = (int)$this->user->info['type'] === 0 ? (int)$this->user->info['completion'] : 100;
        if(lemon_csrf_require_valid_token()) {
            $validation = validate($_POST, [
                'ft_category'       => 'required',
                'ft_subcategory'    => 'required',
                'ft_jobtitle'       => 'required',
                'ft_jobdesc'        => 'required',
                'ft_jobrequirement' => 'required',
                'ft_jobexperience'  => 'required',
                'ft_jobcountry'     => 'required',
                'ft_jobstate'       => 'required',
                'ft_joblocation'    => '',
                'ft_salaryrange'    => 'required',
            ], [
                'ft_category.required'       => lang('category_is_required'), //Category is required!
                'ft_subcategory.required'    => lang('sub_category_is_required'), //Sub Category is required!
                'ft_jobtitle.required'       => lang('job_title_is_required'), //Job Title is required!
                'ft_jobdesc.required'        => lang('job_description_is_required'), //Job Description is required!
                'ft_jobrequirement.required' => lang('job_requirements_is_required'), //Job Requirements is required!
                'ft_jobexperience.required'  => lang('job_experience_is_required'), //Job Experience is required!
                'ft_jobcountry.required'     => lang('country_is_required'), //Country is required!
                'ft_jobstate.required'       => lang('state_is_required'), //State is required!
                'ft_joblocation.required'    => lang('job_location_is_required'), //Job Location is required!
                'ft_salaryrange.required'   => lang('job_salary_range_is_required'), //Job Salary Range is required!
            ]);

            if(empty($validation['errors'])){

                $new_job_id = guid();
                list($min, $max) = explode(',', sanitize(request('ft_salaryrange')));

                $slug = $this->create_slug(request('ft_jobtitle'), request('job_id'));

                $this->db->query("SELECT COUNT(id) as `count` FROM `jobs`");
                $all_count = (int)$this->db->getSingleRow()['count'];

                if( request('ft_joblocation') === '' ){
                    $_REQUEST['draft'] = 1;
                }

                $close_date = sanitize(request('ft_close_date'));
                if( !is_null($close_date) && strtotime($close_date) ){
                    $close_date = Carbon::parse($close_date)->toDateTimeString();
                }else{ // if date was invalid or empty, set closing date to 15 days from now
                    $close_date = Carbon::now()->addDays(15)->toDateTimeString();
                    $_REQUEST['draft'] = 1;
                }

                $skills = request('skills');
                $skill_ids = [];
                if( $skills ) {
                    $parts     = explode(',', $skills);
                    $skill_ids = array_filter($parts, function ($skill) {
                                    return stripos($skill, ':') === false;
                                });
                    $new_parts = array_diff($parts, $skill_ids);
                    $new       = array_values(array_map(function ($skill) {
                                    list($key, $value) = explode(':', $skill, 2);
                                    return sanitize($value);
                                }, $new_parts));

                    if (!empty($new)) {
                        $new_ids = [];
                        foreach ($new as $skill) {
                            $check_for_an_existing_skills = $this->db->query("SELECT * FROM `skills` WHERE `name` LIKE '{$skill}' LIMIT 1");
                            $row                          = $this->db->getSingleRow();
                            if (empty($row)) {
                                $this->db->table('skills');
                                $this->db->insertArray([
                                    'name'   => $skill,
                                    'new'    => '1',
                                    'status' => '0'
                                ]);
                                $this->db->insert();
                                array_push($new_ids, $this->db->insertid());
                            } else {
                                array_push($new_ids, $row['id']);
                            }
                        }
                        $skill_ids = array_merge($skill_ids, $new_ids);
                    }
                }

                if(!request('job_id')) {
                    $this->db->table("jobs");
                    $this->db->insertArray([
                        'id'               => $new_job_id,
                        'number'           => substr($new_job_id, -6),//('J' . (20101 + $all_count)),
                        'user_id'          => $this->user->info['id'],
                        'category'         => sanitize(request('ft_category'), 'int'),
                        'sub_category'     => sanitize(request('ft_subcategory'), 'int'),
                        'title'            => sanitize(request('ft_jobtitle')),
                        'slug'             => $slug,
                        'description'      => request('ft_jobdesc'),
                        'requirements'     => request('ft_jobrequirement'),
                        'experience_level' => sanitize(request('ft_jobexperience')),
                        'skills'           => !empty($skill_ids) ? implode(',', $skill_ids) : '',
                        'country'          => sanitize(request('ft_jobcountry'), 'int'),
                        'state'            => sanitize(request('ft_jobstate'), 'int'),
                        'location'         => sanitize(request('ft_joblocation')),
                        'salary_range_min' => $min,
                        'salary_range_max' => $max,
                        'benefits'         => json_encode((array)sanitize(request('ft_jobbenefits'))),
                        'status'           => request('draft') ? 'draft' : 'published',
                        'closed_at'        => $close_date,
                    ]);

                    $this->db->insert();

                    $this->db->query("SELECT * FROM `jobs` WHERE `id` = '{$new_job_id}' LIMIT 1");
                    $job    = $this->db->getSingleRow();
                    $job_id = $job['id'];
                    $updated = false;
                }else{
                    $job_id = sanitize(request('job_id'));
                    $user_id = $this->user->info['id'];
                    $this->db->query("SELECT * FROM `jobs` WHERE `id` = '{$job_id}' AND `user_id` = '{$user_id}' AND `status` = 'draft' LIMIT 1");
                    $job = $this->db->getSingleRow();

                    if(!empty($job)) {
                        $this->db->table("jobs");
                        $this->db->updateArray([
                            'category'         => sanitize(request('ft_category'), 'int'),
                            'sub_category'     => sanitize(request('ft_subcategory'), 'int'),
                            'title'            => sanitize(request('ft_jobtitle')),
                            'slug'             => $slug,
                            'description'      => request('ft_jobdesc'),
                            'requirements'     => request('ft_jobrequirement'),
                            'experience_level' => sanitize(request('ft_jobexperience')),
                            'skills'           => !empty($skill_ids) ? implode(',', $skill_ids) : '',
                            'country'          => sanitize(request('ft_jobcountry'), 'int'),
                            'state'            => sanitize(request('ft_jobstate'), 'int'),
                            'location'         => sanitize(request('ft_joblocation')),
                            'salary_range_min' => $min,
                            'salary_range_max' => $max,
                            'benefits'         => json_encode((array)sanitize(request('ft_jobbenefits'))),
                            'status'           => request('draft') ? 'draft' : 'published',
                            'closed_at'        => $close_date,
                        ]);

                        $this->db->whereArray([
                            'id'      => $job_id,
                            'user_id' => $user_id,
                        ]);

                        $this->db->update();
                        $updated = true;
                        $job = $this->get_job_by_id($job_id);
                    }else{
                        flash('message', lang('job_can_not_be_updated'));
                        flash('status', 'danger');
                        redirect_to($redirect_to);
                    }
                }

                $all_questions = sanitize(request('ft_qna'));
                $questions = $this->questions($job_id);
                $current_questions = [];

                if(!empty($questions)){
                    $questions = array_values($questions)[0];
                    foreach ( $questions as $cQuestion){
                        array_push($current_questions, $cQuestion['name']);
                    }
                    $all_questions = array_diff($all_questions, $current_questions);
                }

                $delete_questions = array_diff($current_questions, request('ft_qna'));

                if(!empty($delete_questions)){
                    $delete_questions = array_values($delete_questions);
                    $length = count($delete_questions);
                    $query = "DELETE FROM `job_questions` WHERE `job_id` ='{$job_id}' AND (";
                    foreach ($delete_questions as $key => $question){
                        $query .= "`question` LIKE '%{$question}%' ";
                        if($key+1 < $length) $query .= " OR ";
                    }
                    $query .= ")";
                    $this->db->queryOrDie($query);
                }

                if( $all_questions && $job_id ){
                    $this->db->table('job_questions');
                    foreach ((array)$all_questions as $question) {
                        if(strlen(trim($question)) === 0) continue;
                        $questions = [
                            'id'  => guid(),
                            'job_id' => $job_id,
                            'question' => $question
                        ];
                        $this->db->insertArray($questions);
                        $this->db->insert();
                    }
                }
            }else{
                set('create_task_errors', $validation['errors']);
            }
        }

        if(empty($validation['errors'])) {
            if($updated) flash('message', lang('job_was_updated_succussfully'));
            else flash('message', lang('job_was_created_succussfully'));
            flash('status', 'success');
        }else{
            flash('message', implode("<br/>", $validation['errors']));
            flash('status', 'danger');
        }

        redirect_to($redirect_to);
    }

    public function apply(){
        $this->is_logged_in();

        if(lemon_csrf_require_valid_token() && (request('external') || (int)(new TaskCentre($this->db, $this->user))->user_completeness() === 100)) {
            if( request('job_id') && request('answer') ) {
                $this->db->table("answers");

                foreach (request('answer') as $key => $answer) {

                    $user_id     = $this->user->info['id'];
                    $task_id     = sanitize(request('job_id'));
                    $question_id = sanitize($key);
                    $this->db->query("SELECT COUNT(*) FROM `answers` WHERE `user_id` = '{$user_id}' AND `task_id` = '{$task_id}' AND `question_id` = '{$question_id}'");
                    if($this->db->getValue() > 0) { continue; }

                    $this->db->insertArray([
                        'user_id'     => $this->user->info['id'],
                        'task_id'      => sanitize(request('job_id')),
                        'question_id' => sanitize($key),
                        'answer'      => sanitize($answer),
                        'type'        => 'job',
                        'created_at'  => Carbon::now()->toDateTimeString()
                    ]);

                    $this->db->insert();
                }
            }
            if( request('job_id') ){
                $this->db->table("user_task");
                $this->db->insertArray([
                    'user_id'    => $this->user->info['id'],
                    'task_id'    => sanitize(request('job_id')),
                    'type'       => 'job',
                    'created_at' => Carbon::now()->toDateTimeString()
                ]);
                $this->db->insert();
            }

            if( request('external') )
                $job = $this->get_external_job_by_id( sanitize(request('job_id')) );
            else
                $job = $this->get_job_by_id( sanitize(request('job_id')) );

            Event::trigger('job.applied', [$job]);
            $poster_name = $job['user_id'] !== '0' ? $this->get_job_user($job['user_id']) : null;

            /* (new Mail($this->db))->sendmail(
                [
                    'email' => $this->user->info['email'],
                    'name'  => $this->user->info['firstname']
                ],
                'new-job-application',
                [
                    'first_name' => $this->user->info['firstname'],
                    'poster_name' => !is_null($poster_name) ? ($poster_name['company']['name'] ?? "{$poster_name['firstname']} {$poster_name['lastname']}") : '',
                    'job_name' => $job['title']
                ]
            ); */

            if( ! $this->is_ajax() ) {
                flash('message', lang('applied_succussfully'));
                flash('status', 'success');
                redirect_to(request('url') ? option('site_uri') . request('url') : option('site_uri') . url_for('/search'));
            }

        }

        if( ! $this->is_ajax() ){
            flash('message', lang('failed_to_apply_please_try_again'));
            flash('status', 'danger');
            redirect_to(option('site_uri') . url_for('/'));
        }


    }

    public function assign($user_id = null, $job_id = null){
        if($this->is_logged_in() && !is_null($user_id) && !is_null($job_id)){
            $this->db->table("jobs");
            $this->db->updateArray([
                'assigned_to' => $user_id,
                'assigned'    => Carbon::now()->toDateTimeString()
            ]);

            $this->db->whereArray(['id' => $job_id]);

            $this->db->update();
            return true;
        }
        return false;
    }

    public function shortlist($user_id = null, $job_id = null){
        if($this->is_logged_in() && !is_null($user_id) && !is_null($job_id)){
            $this->db->table("job_shortlisted");
            $this->db->insertArray([
                'user_id' => $user_id,
                'job_id'  => $job_id,
                'created_at' => Carbon::now()->toDateTimeString()
            ]);

            $this->db->insert();
            return true;
        }
        return false;
    }

    public function delete(){
        if(lemon_csrf_require_valid_token() && request('job_id')) {
            $user_id = $this->user->info['id'];
            $job_id = request('job_id');
            $job = $this->get_job_by_id($job_id);
            if( $job_id && $this->owner($user_id, $job_id) && $this->is_status($job_id, $user_id, 'draft') ){
                $this->db->queryOrDie("DELETE FROM `jobs` WHERE `id` = '{$job_id}' AND `user_id` = '{$user_id}' AND `status` = 'draft' ");

                //$this->activity->add( ['job_id' => $job_id, 'task_number' => $job['number'], 'event' => 'deleted'] );
                flash('message', lang('job_was_deleted_succussfully'));
                flash('status', 'success');
            }

        }else{
            flash('message', lang('failed_to_delete_job_please_try_again'));
            flash('status', 'danger');
        }

        header('Location: ' . url_for('/workspace/job-centre'));
        exit();
    }

    public function cancel(){
        if(lemon_csrf_require_valid_token() && request('job_id')) {
            $user_id = $this->user->info['id'];
            $job_id = request('job_id');

            if( $job_id && $this->owner($user_id, $job_id) && $this->is_status($job_id, $user_id, 'published') ){
                $this->db->table('jobs');
                $this->db->updateArray(['status' => 'cancelled']);
                $this->db->whereArray(['id' => $job_id, 'user_id' => $user_id]);
                $this->db->update();

                $job = $this->get_job_by_id($job_id);

                //$this->activity->add( ['job_id' => $job_id, 'task_number' => $job['number'], 'event' => 'cancelled'] );
                $this->unfavourite_job_from_all($job_id, 'canceled');
                flash('message', lang('job_was_cancelled_succussfully'));
                flash('status', 'success');
            }

        }else{
            flash('message', lang('failed_to_cancel_job_please_try_again'));
            flash('status', 'danger');
        }

        header('Location: ' . url_for('/workspace/job-centre'));
        exit();
    }

    public function favourite(){
        $this->is_logged_in();
        if(lemon_csrf_require_valid_token()) {
            if( request('job_id') ){
                $this->db->table("favourites");
                if( request('favourited') && request('favourited') === "true" && $this->is_favourited( sanitize(request('job_id')) ) ){
                    $this->db->whereArray([
                        'user_id' => $this->user->info['id'],
                        'task_id'  => sanitize(request('job_id')),
                        'type'    => 'job',
                    ]);
                    $this->db->delete();
                }else if( !$this->is_favourited( sanitize(request('job_id')) ) ){
                    $this->db->insertArray([
                        'user_id'    => $this->user->info['id'],
                        'task_id'     => sanitize(request('job_id')),
                        'type'       => 'job',
                        'created_at' => Carbon::now()->toDateTimeString()
                    ]);
                    $this->db->insert();
                }
            }
            if( $this->is_ajax() ){
                if (request('favourited') === "true") $message = lang('job_was_removed_from_favourite_successfully');
                else $message = lang('job_was_added_to_favourite_successfully');

                return json(['message' => $message, 'status' => 'success', 'action' => (request('favourited') === "true") ? 'removed' : 'added', 'title' => lang('success')]);
            }else {
                if (request('favourited') === "true") flash('message', lang('job_was_removed_from_favourite_successfully'));
                else flash('message', lang('job_was_added_to_favourite_successfully'));
                flash('status', 'success');
                if (request('url') && strpos(request('url'), 'http') === false) {
                    if( stripos(request('url'), '?s=external') ){
                        $external = '?s=external';
                        $url = substr(request('url'), 0, strlen(request('url')) - 11);
                    }else{
                        $external = '';
                        $url = request('url');
                    }
                    $url = ltrim($url, option('base_uri'));
                    $url = request('url') ? option('site_uri') . url_for($url) . $external : option('site_uri') . url_for('/workspace/search');
                }else {
                    $url = request('url');
                }
                redirect_to($url);
            }
        }

        flash('message', lang('failed_to_favourite_please_try_again'));
        flash('status', 'danger');
        redirect(option('site_uri') . url_for('/'));
    }

    public function remove_expired_favourite(){
        $this->is_logged_in();
        if(lemon_csrf_require_valid_token()) {
            $user_id = $this->user->info['id'];

            $this->db->query("SELECT DISTINCT `task_id`   
                          FROM `favourites` 
                          WHERE `user_id` = '{$user_id}' AND `type` = 'job' 
                          AND (
                                `task_id` IN( SELECT `id` FROM `jobs` WHERE `status` IN('published') )
                                OR `task_id` IN( SELECT `id` FROM `external_jobs`  WHERE `status` IN('published') )
                          )");

            $inactive_jobs_favourites = $this->db->getRowList();
            $ids = array_column($inactive_jobs_favourites, 'task_id');

            $ids = implode("','", $ids);

            $this->db->queryOrDie("DELETE FROM `favourites` WHERE `user_id` = '{$user_id}' AND `task_id` IN('{$ids}') AND `type` = 'job'");
        }
        redirect_to(option('site_uri') . url_for('workspace/my-favourite'));
    }

    public function unfavourite_job_from_all($job_id = null, $reason = ''){
        $this->is_logged_in();
        if(!is_null($job_id)) {
            $job = $this->get_job_by_id(sanitize(request('job_id')));
            $this->db->table("favourites");
            $this->db->whereArray([
                'task_id' => sanitize($job_id),
                'type'    => 'job',
            ]);
            $this->db->delete();
            /*$this->activity->add([
                'user_id' => 0,
                'job_id' => $job['id'],
                'task_number' => $job['number'],
                'event' => 'unfavourite',
                'message' => "Unfavourited automatically, because task was {$reason}"
            ]);*/
        }
    }

    public function questions($job_id = null){
        $this->is_logged_in();
        if( null === $job_id ) return [];
        $this->db->query("SELECT * FROM `job_questions` WHERE `job_id` = '{$job_id}'");
        $results = $this->db->getRowList();
        $questions = [];
        if($results && !empty($results)) {
            foreach ($results as $question) {
                $questions[$job_id][] = [
                    'id'   => $question['id'],
                    'name' => $question['question']
                ];
            }
        }
        return $questions;
    }

    public function jobs($user_id = null){
        $this->is_logged_in();
        if(null === $user_id) $user_id = $this->user->info['id'];
        $external_jobs = [];

        if($this->user->info['type'] === '1') {
            $jobs = $this->get_jobs($user_id, 0, 100);
        }elseif ($this->user->info['type'] === '0') {
            $jobs = $this->get_applied_jobs($user_id, 0, 100);
            $external_jobs = $this->get_applied_external_jobs($user_id, 0, 100);
        }

        $jobs = array_merge($jobs, $external_jobs);
        $task_centre = new TaskCentre($this->db, $this->user);
        $questions = [];
        $closed = []; // list of jobs that passed their closing time
        $jobs_notifications = false;
        foreach ($jobs as $key => $job){
            //check for closing date, and mark it to be cancel/close it if it's passed that date
            /*if( Carbon::parse($job['closed_at'])->endOfDay()->isBefore(Carbon::now()) ){
                $job['status'] = 'cancelled';
                array_push($closed, $job['id']);
            }*/

            $questions                      = $this->questions($job['id']);
            $jobs[$key]['posted_date']      = Carbon::parse($job['created_at'])->format('d M Y');
            $jobs[$key]['close_date']       = !empty($job['closed_at']) ? Carbon::parse($job['closed_at'])->format('d M Y') : '';
            $jobs[$key]['applicants']       = $user_id === $job['user_id'] ? $this->get_applicants([$job['id']]) : [];
            $jobs[$key]['new_applicants']   = in_array('N', array_column($jobs[$key]['applicants'], 'viewed'));
            $jobs[$key]['has_chats']        = $this->has_chats($job['id']);
            $jobs[$key]['has_new_chats']    = $this->has_new_chats($job['id']);
            $jobs[$key]['salary_range_min'] = number_format($job['salary_range_min']);
            $jobs[$key]['salary_range_max'] = number_format($job['salary_range_max']);
            $poster                         = (int)$job['user_id'] !== 0 ? $this->get_job_user($job['user_id']) : $job['company'];
            if(isset($poster['password'])) unset($poster['password']);
            $jobs[$key]['poster']  = $poster;

            $jobs[$key]['country'] = $task_centre->getCountryName($job['country']);
            $jobs[$key]['state']   = is_numeric($job['state']) ? $task_centre->getStateName($job['state']) : $job['state'];
            if( $user_id !== $job['user_id'] ){
				$jobs[$key]['shortlisted'] = $this->get_applied_status($user_id,$job['id']);
            }
            $comments = $task_centre->get_comments($job['id'], $job['user_id']);
            $comments = array_filter(array_column($comments, 'viewed'), function ($viewed){ return $viewed === 'N'; });
            $jobs[$key]['has_new_comments'] = !empty($comments) && in_array($job['status'], ['published']) ? true : false;
            $jobs[$key]['has_new_updates'] = $jobs[$key]['has_new_comments'] || $jobs[$key]['has_new_chats'] || $jobs[$key]['new_applicants'];

            // setting side menu notification for job centre
            if($jobs[$key]['has_new_updates']){
                $jobs_notifications = true;
            }
        }
        // Close/Cancel jobs that have been marked to be closed
        if( !empty($closed) ){
            $this->close_jobs($closed);
        }

        $draft     = array_filter($jobs, function ($job) { return $job['status'] === 'draft'; });
        $published = array_filter($jobs, function ($job) { return $job['status'] === 'published'; });
        $cancelled = array_filter($jobs, function ($job) { return $job['status'] === 'cancelled'; });
        $completed = array_filter($jobs, function ($job) { return $job['status'] === 'completed'; });
        $assigned  = array_filter($jobs, function ($job) { return !is_null($job['assigned']); });

        set('jobs_notification', $jobs_notifications);

        return [
            'all'       => $jobs,
            'draft'     => $draft,
            'published' => $published,
            'completed' => $completed,
            'cancelled' => $cancelled,
            'assigned'  => $assigned,
            'closed'    => array_merge($completed, $cancelled),
            'questions' => $questions
        ];
    }
	
	public function get_applied_status($user_id = null, $job_id = null){
        if( is_null($user_id) && is_null($job_id) ) return false;
		$this->db->query("SELECT COUNT(*) AS `count` FROM `job_shortlisted` WHERE `user_id` = '{$user_id}' AND job_id = '{$job_id}'");
		$count = $this->db->getSingleRow();
		return (!empty($count) && isset($count['count']) && ($count['count'] > 0)) ? true : false;
	}

    public function get_job_user($user_id = null){
        if(null === $user_id) return [];

        $this->db->query("SELECT *,
                          (SELECT `name` FROM `cities` WHERE `id` = m.`city` LIMIT 1 ) AS `city`,
                          (SELECT `name` FROM `states` WHERE `id` = m.`state` LIMIT 1 ) AS `state`,
                          (SELECT `name` FROM `countries` WHERE `id` = m.`country` LIMIT 1 ) AS `country`
                          FROM `members` m 
                          WHERE m.`id` = '{$user_id}' LIMIT 1");
        $user = $this->db->getSingleRow();
        if( !empty($user) ){
            $user['number'] = (string)'TL' . ($user['id'] + 20100);
            if( $user['type'] === '1' ){
                $this->db->query("SELECT * FROM `company_details` WHERE `member_id` = '{$user['id']}' LIMIT 1");
                $user['company'] = $this->db->getSingleRow();
            }
        }
        return $user;
    }

    public function get_job($slug = null){
        if(null === $slug) return [];

        $this->db->query("SELECT *, j.`status` AS `job_status`, j.`id` AS `job_id`, j.`state` AS `state`, j.`country` AS `country`, j.`skills` AS `job_skills` 
                          FROM `jobs` j
                          INNER JOIN `members` m ON m.`id` = j.`user_id` 
                          LEFT JOIN `company_details` n ON n.`member_id` = j.`user_id`
                          WHERE j.`slug` = '{$slug}' LIMIT 1");
        return $this->db->getSingleRow();
    }

    public function get_external_job($slug = null){
        if(null === $slug) return [];

        $this->db->query("SELECT *, j.`status` AS `job_status`, j.`id` AS `job_id`, j.`state` AS `state`, 
                          j.`country` AS `country`, 'others' AS `category_name`, 'others' AS `subcategory_name` 
                          FROM `external_jobs` j
                          WHERE j.`slug` = '{$slug}' LIMIT 1");
        return $this->db->getSingleRow();
    }

    public function get_by_id($id = null){
        return $this->get_job_by_id($id);
    }

    public function get_job_by_id($id = null){
        if(null === $id) return [];

        $this->db->query("SELECT *, j.`status` AS `job_status`, m.`id` AS `user_id`, j.`id` AS `job_id`,
                                 (SELECT `name` FROM `countries` WHERE `id` = j.`country` LIMIT 1) AS `job_country`, 
                                 (SELECT `name` FROM `states` WHERE `id` = j.`state` LIMIT 1) AS `job_state`, 
                                 (SELECT `name` FROM `states` WHERE `id` = m.`state` LIMIT 1) AS `state`, 
                                 (SELECT `title` FROM `category` WHERE `id` = j.`category` LIMIT 1) AS `category`,   
                                 (SELECT `title` FROM `category` WHERE `id` = j.`sub_category` LIMIT 1) AS `sub_category`,
                                 'internal_job' AS `type` 
                          FROM `members` m
                          INNER JOIN `jobs` j ON m.`id` = j.`user_id` 
                          WHERE j.`id` = '{$id}' LIMIT 1");
        return $this->db->getSingleRow();
    }

    public function get_external_job_by_id($id = null){
        if(null === $id) return [];

        $this->db->query("SELECT *, j.`status` AS `job_status`, j.`id` AS `job_id`, 'external_job' AS `type`,
                                 (SELECT `name` FROM `countries` WHERE `id` = j.`country` LIMIT 1) AS `job_country`,
                                 j.`state` AS `job_state`, 'others' AS `category`
                          FROM `external_jobs` j 
                          WHERE j.`id` = '{$id}' LIMIT 1");
        return $this->db->getSingleRow();
    }

    public function get_jobs_by_id($ids = []){
         if( empty($ids) ) return [];

        $ids = implode("','", $ids);
        $user_id = $this->user->info['id'];

        $this->db->query("SELECT j.`number`, j.`title`, j.`slug`, j.`status` AS `job_status`, m.`id` AS `user_id`, 
                                  j.`salary_range_min`, j.`salary_range_max`, j.`id` AS `job_id`, 1 AS `priority`,
                                 (SELECT `name` FROM `countries` WHERE `id` = j.`country` LIMIT 1) AS `job_country`, 
                                 (SELECT `name` FROM `states` WHERE `id` = j.`state` LIMIT 1) AS `job_state`, 
                                 (SELECT `name` FROM `states` WHERE `id` = m.`state` LIMIT 1) AS `state`, 
                                 (SELECT `title` FROM `category` WHERE `id` = j.`category` LIMIT 1) AS `category`,   
                                 (SELECT `title` FROM `category` WHERE `id` = j.`sub_category` LIMIT 1) AS `sub_category`,
                                 (SELECT COUNT(`id`) FROM `favourites` WHERE j.`id` = `task_id` AND `type` = 'job' AND `user_id` = '{$user_id}') AS `favourited`,
                                 'internal_job' AS `type`,
                                 j.`created_at`, j.`user_id`
                          FROM `members` m
                          INNER JOIN `jobs` j ON m.`id` = j.`user_id` 
                          WHERE j.`id` IN ('{$ids}') ORDER BY j.`created_at` DESC");

        return $this->db->getRowList();
    }

    public function get_external_jobs_by_id($ids = []){
        if( empty($ids)) return [];

        $ids = implode("','", $ids);
        $user_id = $this->user->info['id'];

        $this->db->query("SELECT j.`number`, j.`title`, j.`slug`, j.`status` AS `job_status`, j.`salary_range_min`, j.`salary_range_max`,
                                 j.`id` AS `job_id`, 'external_job' AS `type`,
                                 (SELECT `name` FROM `countries` WHERE `id` = j.`country` LIMIT 1) AS `job_country`,
                                 j.`state` AS `job_state`, 2 AS `priority`, 'others' AS `category`,
                                 (SELECT COUNT(`id`) FROM `favourites` WHERE j.`id` = `task_id` AND `type` = 'job' AND `user_id` = '{$user_id}') AS `favourited`,
                                 j.`created_at`, j.`user_id`
                          FROM `external_jobs` j 
                          WHERE j.`id` IN ('{$ids}') ORDER BY j.`created_at` DESC");

        return $this->db->getRowList();
    }

    public function get_jobs($user_id = null, $offset = 0, $limit = 25){
        $this->is_logged_in();
        if(null === $user_id) return [];

        $this->db->query("SELECT * FROM `jobs` j WHERE j.`user_id` = '{$user_id}' ORDER BY j.`created_at` DESC LIMIT {$offset}, $limit");
        return $this->db->getRowList();
    }

    public function get_published_jobs($search = [], $filters = [], $offset = 0, $limit = 25){
        //SELECT DISTINCT *, j.`status` AS `job_status`, j.`id` AS `job_id`, j.`state` AS `state`, j.`country` AS `country` FROM `jobs` j
        $query = "
                    SELECT 'internal' AS `the_type`, `user_id`, j.`id` AS `job_id`, `created_at`, 1 AS `prioirty`
                    FROM `jobs` j  
                    INNER JOIN `members` m ON m.`id` = j.`user_id`
                    WHERE j.`status` = 'published' AND j.`assigned` IS NULL AND j.`assigned_to` IS NULL 
                    ";

        if(!empty($search)) {
            $query .= " AND ( ";

            $length = count($search);
            foreach ($search as $key => $str) {
                $str = mysqli_real_escape_string($this->db->connection, $str);
                $query .= "   LOWER(CONVERT(j.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(j.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(j.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) ";
        }

        if(!empty(array_filter($filters))){
            extract($filters);

            if(isset($category)){
                $query .= " AND (j.`category` IN({$category}) OR j.`sub_category` IN({$category}))";
            }

            if(isset($state)){
                $state = mysqli_real_escape_string($this->db->connection, $state);
                $query .= " AND (j.`state` IN({$state}))";
            }

            if(isset($states['ids']) && !empty($states['ids']) ){
                $states = implode(',', $states['ids']);
                $query .= " AND (j.`state` IN({$states}))";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (j.`id` IN(
                             SELECT `id` FROM `jobs` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if( isset($city_name) || isset($nearby_places) ){
                $query .= " AND ( ";
            }

            if(isset($city_name) && !empty($city_name)){
                $first = true;
                $city = mysqli_real_escape_string($this->db->connection, strtolower($city_name));
                $query .= " (LOWER(CONVERT(j.`location` USING utf8mb4)) LIKE '%{$city}%') ";
            }

            if(isset($nearby_places)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = mysqli_real_escape_string($this->db->connection, strtolower($place));
                    $query .= " (LOWER(CONVERT(j.`location` USING utf8mb4)) LIKE '%{$place}%')";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if( isset($city_name) || isset($nearby_places) ){
                $query .= " ) ";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'closeDate' => 'closed_at'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate'){
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'closeDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(j.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }
        }

        $query .= " UNION ";

        $query .= "SELECT DISTINCT 'external' AS `the_type`, `user_id`, xj.`id` AS `job_id`, `created_at`, 2 AS `prioirty`
                    FROM `external_jobs` xj  
                    WHERE xj.`status` = 'published' AND xj.`assigned` IS NULL AND xj.`assigned_to` IS NULL";

        if(!empty($search)) {
            $query .= " AND ( ";

            $length = count($search);
            foreach ($search as $key => $str) {
                $str = mysqli_real_escape_string($this->db->connection, $str);
                $query .= "   LOWER(CONVERT(xj.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(xj.`description` USING utf8mb4)) LIKE '%{$str}%'
                              OR LOWER(CONVERT(xj.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) ";
        }

        if(!empty(array_filter($filters))){
            extract($filters);
            $first = false;
            if(isset($category)){
                $query .= " AND (xj.`category` IN({$category}) OR xj.`sub_category` IN({$category}))";
            }

            if(isset($state)){
                $state = mysqli_real_escape_string($this->db->connection, $state);
                $query .= " AND ( xj.`state` IN( SELECT `name` FROM `states` WHERE `id` IN ({$state}) ) )";
            }

            if(isset($states['ids']) && !empty($states['ids']) ){
                $states = implode(',', $states['ids']);
                $query .= " AND ( xj.`state` IN( SELECT `name` FROM `states` WHERE `id` IN ({$states}) ) )";
            }

            if(isset($city_name) && !empty($city_name)){
                $city = mysqli_real_escape_string($this->db->connection, trim(strtolower($city_name)));
                $query .= " OR (LOWER(CONVERT(xj.`location` USING utf8mb4)) LIKE '%{$city}%') ";
            }

            if(isset($nearby_places)){
                $query .= " AND ( ";
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = mysqli_real_escape_string($this->db->connection, trim(strtolower($place)));
                    $query .= " (LOWER(CONVERT(xj.`state` USING utf8mb4)) LIKE '%{$place}%')";
                    $query .= " OR (LOWER(CONVERT(xj.`location` USING utf8mb4)) LIKE '%{$place}%')";
                    if($key+1 !== $length) $query .= " OR ";
                }
                $query .= " ) ";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'closeDate' => 'closed_at'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate'){
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'closeDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(xj.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }
        }

        $query .= " ORDER BY `prioirty` ASC, `created_at` DESC 
                    LIMIT {$offset}, {$limit}"; //GROUP BY xj.`number`

        $this->db->query($query);
        return $this->db->getRowList();
    }

    public function get_applied_jobs($user_id = null, $offset = 0, $limit = 25){
        $this->is_logged_in();
        if(null === $user_id) return [];

        $this->db->query("SELECT j.* 
                          FROM `user_task` u 
                          INNER JOIN `jobs` j ON j.`id` = u.`task_id`
                          WHERE u.`user_id` = '{$user_id}'
                          ORDER BY j.`created_at` DESC");
        return $this->db->getRowList();
    }

    public function get_applied_external_jobs($user_id = null, $offset = 0, $limit = 25){
        $this->is_logged_in();
        if(null === $user_id) return [];

        $this->db->query("SELECT j.*, 'external' AS `type`  
                          FROM `user_task` u 
                          INNER JOIN `external_jobs` j ON j.`id` = u.`task_id`
                          WHERE u.`user_id` = '{$user_id}'
                          ORDER BY j.`created_at` DESC");
        return $this->db->getRowList();
    }

    public function get_published_jobs_count($user_id = null){
        if(null === $user_id) return 0;

        $this->db->query("SELECT COUNT(*) AS `count` FROM `jobs` j WHERE j.`user_id` = '{$user_id}' AND j.`status` IN('published', 'completed')");
        return $this->db->getValue();
    }

    public function get_all_published_jobs_count(...$args){
        $search  = $args[0];
        $filters = $args[1];
        $query   = "SELECT SUM(`count`) FROM (SELECT COUNT(*) AS `count` FROM `jobs` j 
                    WHERE j.`status` = 'published' AND j.`assigned` IS NULL AND j.`assigned_to` IS NULL";

        if(!empty($search)) {
            $query .= " AND ( ";

            $length = count($search);
            foreach ($search as $key => $str) {
                $str = mysqli_real_escape_string($this->db->connection, $str);
                $query .= "   LOWER(CONVERT(j.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(j.`description` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) ";
        }

        if(!empty(array_filter($filters))){
            extract($filters);

            if(isset($category)){
                $query .= " AND (j.`category` IN({$category}) OR j.`sub_category` IN({$category}))";
            }

            if(isset($state)){
                $state = mysqli_real_escape_string($this->db->connection, $state);
                $query .= " AND (j.`state` IN({$state}))";
            }

            if(isset($states['ids']) && !empty($states['ids']) ){
                $states = implode(',', $states['ids']);
                $query .= " AND (j.`state` IN({$states}))";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (j.`id` IN(
                             SELECT `id` FROM `jobs` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if(isset($city_name) && !empty($city_name)){
                $city = mysqli_real_escape_string($this->db->connection, trim(strtolower($city_name)));
                $query .= " OR (LOWER(CONVERT(j.`location` USING utf8mb4)) LIKE '%{$city}%') ";
            }

            if(isset($nearby_places)){
                $query .= " AND ( ";
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = mysqli_real_escape_string($this->db->connection, trim(strtolower($place)));
                    $query .= " (LOWER(CONVERT(j.`location` USING utf8mb4)) LIKE '%{$place}%')";
                    if($key+1 !== $length) $query .= " OR ";
                }
                $query .= " ) ";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'closeDate' => 'closed_at'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate'){
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'closeDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(j.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }
        }

        $query .= " UNION ";

        $query .= "SELECT COUNT(DISTINCT(`number`)) AS `count` FROM `external_jobs` j 
                    WHERE j.`status` = 'published' AND j.`assigned` IS NULL AND j.`assigned_to` IS NULL";

        if(!empty($search)) {
            $query .= " AND ( ";

            $length = count($search);
            foreach ($search as $key => $str) {
                $str = mysqli_real_escape_string($this->db->connection, $str);
                $query .= "   LOWER(CONVERT(j.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(j.`description` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) ";
        }

        if(!empty(array_filter($filters))){
            extract($filters);
            $first = false;
            if(isset($category)){
                $query .= " AND (j.`category` IN({$category}) OR j.`sub_category` IN({$category}))";
            }

            if(isset($state)){
                $state = mysqli_real_escape_string($this->db->connection, $state);
                $query .= " AND ( j.`state` IN( SELECT `name` FROM `states` WHERE `id` IN ({$state}) ) )";
            }

            if(isset($states['ids']) && !empty($states['ids']) ){
                $states = implode(',', $states['ids']);
                $query .= " AND ( j.`state` IN( SELECT `name` FROM `states` WHERE `id` IN ({$states}) ) )";
            }

            if(isset($city_name) && !empty($city_name)){
                $city = mysqli_real_escape_string($this->db->connection, trim(strtolower($city_name)));
                $query .= " OR (LOWER(CONVERT(j.`location` USING utf8mb4)) LIKE '%{$city}%') ";
            }

            if(isset($nearby_places)){
                $query .= " AND ( ";
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = mysqli_real_escape_string($this->db->connection, trim(strtolower($place)));
                    $query .= " (LOWER(CONVERT(j.`state` USING utf8mb4)) LIKE '%{$place}%')";
                    $query .= " OR (LOWER(CONVERT(j.`location` USING utf8mb4)) LIKE '%{$place}%')";
                    if($key+1 !== $length) $query .= " OR ";
                }
                $query .= " ) ";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'closeDate' => 'closed_at'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate'){
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'closeDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(j.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }
            //$query .= ' GROUP BY j.`title` ';
        }
        $query .= ' ) AS `temp` ';

        $this->db->query($query);
        return $this->db->getValue();
    }

    public function get_favourites($user_id = null, $offset = 0, $limit = 25){
        $this->is_logged_in();
        if(null === $user_id) return [];

        $this->db->query("SELECT * FROM `favourites` t WHERE t.`user_id` = '{$user_id}' AND `type` = 'job'");
        return $this->db->getRowList();
    }

    public function is_favourited($task_id = null, $user_id = null){
        $this->is_logged_in();
        if( is_null($task_id) ) return false;
        if( is_null($user_id) ) $user_id = $this->user->info['id'];

        $this->db->query("SELECT COUNT(*) FROM `favourites` f WHERE f.`user_id` = '{$user_id}' AND f.`task_id` = '{$task_id}' AND f.`type` = 'job'");
        return $this->db->getValue() > 0;
    }

    public function owner($user_id = null, $job_id = null){
        if( is_null($job_id) || is_null($user_id) ) return false;

        $this->db->query("SELECT COUNT(`id`)
                          FROM `jobs` t
                          WHERE t.`id` = '{$job_id}' AND t.`user_id` = '{$user_id}' LIMIT 1");

        return empty($this->db->getSingleRow()) ? false : true;
    }

    public function is_status($job_id = null, $user_id = null, $status = null){
        if( is_null($job_id) || is_null($status) ) return false;
        if( is_null($user_id) ) $user_id = $this->user->info['id'];

        $this->db->query("SELECT `status`
                          FROM `jobs` j
                          WHERE j.`id` = '{$job_id}' AND j.`user_id` = '{$user_id}' LIMIT 1");

        return $this->db->getValue() === $status;
    }

    public function has_applicant($user_id = null, $job_id = null){
        if( is_null($job_id) || is_null($user_id) ) return false;

        $this->db->query("SELECT COUNT(`id`)
                          FROM `user_task` u
                          WHERE u.`task_id` = '{$job_id}' AND u.`user_id` = '{$user_id}' AND `type` = 'job' LIMIT 1");

        return empty($this->db->getSingleRow()) ? false : true;
    }

    public function get_applicants($jobs = [], $offset = 0, $limit = 5){
        if( empty($jobs) ) return [];
        $ids = "'" . implode("','", $jobs) . "'";

        $this->db->query("SELECT m.`id` AS `user_id`, CONCAT(m.`firstname`,' ', m.`lastname`) AS `name`, DATE_FORMAT(u.`created_at`, '%d/%m/%Y') AS `applied_at`,
                          (SELECT CASE WHEN COUNT(*) > 0 THEN 'true' ELSE 'false' END FROM `job_shortlisted` WHERE `job_id` IN({$ids}) AND `user_id` = m.`id` ) AS `shortlisted`,
                          (SELECT GROUP_CONCAT(`src`) AS `docs` FROM `member_docs` WHERE `member_id` = m.`id` ) AS `docs`,
                          u.`viewed`
                          FROM `members` m
                          INNER JOIN `user_task` u ON m.`id` = u.`user_id` 
                          INNER JOIN `jobs` j ON j.`id` = u.`task_id` AND j.`status` IN ('published')
                          WHERE u.`task_id` IN ({$ids}) AND u.`type` = 'job'
                          ORDER BY u.`created_at` DESC
                          LIMIT {$offset}, {$limit}");

        return $this->db->getRowList();
    }

    public function has_chats($job_id = null){
        if(null === $job_id) return false;

        $user_id = $this->db->escape($this->user->info['id']);
        $job_id = $this->db->escape($job_id);
        $this->db->query("
                        SELECT * 
                        FROM `chats`
                        WHERE (`sender_id` = {$user_id} OR `receiver_id` = {$user_id}) AND `task_id` = {$job_id}"
        );

        $chats = [];

        foreach($this->db->getRowList() as $chat){
            $chats[] = $chat['sender_id'];
        }

        return !empty($chats) ? $chats : false;
    }

    public function has_new_chats($job_id = null){
        if(null === $job_id) return false;

        $user_id = $this->db->escape($this->user->info['id']);
        $job_id = $this->db->escape($job_id);
        $this->db->query("
                        SELECT * 
                        FROM `chats`
                        WHERE `receiver_id` = {$user_id} AND `task_id` = {$job_id} AND `viewed` = '0'"
        );
        $chats = [];

        foreach($this->db->getRowList() as $chat){
            $chats[] = $chat['sender_id'];
        }

        return !empty($chats) ? $chats : false;
    }

    private function close_jobs($job_ids = []){
        if( empty($job_ids) ) return;

        if(!is_array($job_ids)) $job_ids = (array) $job_ids;

        $job_ids = "'" . implode("','", $job_ids) . "'"; /// 'id1','id2','id3'

        $this->db->queryOrDie("UPDATE `jobs` SET `status` = 'cancelled' WHERE `id` IN({$job_ids})");

    }

    private function create_slug(string $title, $job_id = null){
        $title = sanitize($title);
        $slug = trim(preg_replace('#[\s]#i', '-', preg_replace('#[^a-z0-9\s]#i', '', strtolower($title))), '-');

        if (is_null($job_id)) {
            // Let's check if the slug is avaliable, otherwise give it a counter at the end
            $this->db->query("SELECT COUNT(id) as `count` FROM `jobs` WHERE `slug` LIKE '%{$slug}%'");
            $count = (int)$this->db->getSingleRow()['count'];
            if ($count > 0) {
                $slug .= "-" . ($count + 1);
            }
        } else {
            $job_id = sanitize($job_id);
            $user_id = $this->user->info['id'];
            $this->db->query("SELECT * FROM `jobs` WHERE `id` = '{$job_id}' AND `user_id` = '{$user_id}' AND `status` = 'draft' LIMIT 1");
            $job = $this->db->getSingleRow();

            $slug = ( $job['title'] !== $title ) ? $this->create_slug($title, null) : $job['slug'];
        }

        return $slug;
    }

    /**
     * @param string $job_status default is 'published'
     * @param int $user_id default is null
     * @return int count of jobs with status passed
     */
    public function get_status_count($job_status = 'published', $user_id = null){
        if( is_null($user_id) ) $user_id = $this->user->info['id'];

        return $this->db->getValue("SELECT COUNT(*) FROM `jobs` WHERE `user_id` = '{$user_id}' AND `status` = '{$job_status}'");
    }

    // This would be called uing cronjob everyday at 12am
    public function cron(){
        $this->db->query("SELECT * FROM `jobs` WHERE `closed_at` < NOW() AND `status` = 'published'");
        $jobs   = $this->db->getRowList();
        $closed = [];

        if( !empty($jobs) ){
            foreach($jobs as $job) {
                array_push($closed, $job['id']);
                Event::trigger('job.closed', [$job]);
            }

            $this->close_jobs($closed);
        }

        echo 'DONE';
    }
}