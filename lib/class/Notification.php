<?php


class Notification{
    protected static $db;
    protected static $user;
    protected static $date;
    protected static $settings;

    /**
     * @var $db db
     * @var $user user
     */
    public function __construct($db, $user){
        date_default_timezone_set('Asia/Kuala_Lumpur');

        if( !isset($_SESSION[WEBSITE_PREFIX.'LANGUAGE']) || $_SESSION[WEBSITE_PREFIX.'LANGUAGE'] == 'EN' ){
            $locale = ['English', 'en_GB'];
        }else{
            $locale = ['Malay', 'ms_MY'];
        }

        setLocale(LC_TIME, $locale[1]);
        \Carbon\Carbon::setLocale($locale[1]);

        self::$db = $db;
        self::$user = $user;
        self::$date = new DateTime();
    }

    public static function __callStatic($method, $args){
        self::checkIfTableExists();
        $args = $args[0];
        if( isset($args['description']) ) unset($args['description']);
        if( isset($args['about']) ) unset($args['about']);
        if( isset($args['about_us']) ) unset($args['about_us']);
        if( isset($args['location']) ) unset($args['location']);
        if( isset($args['location']) ) unset($args['location']);
        if( isset($args['address1']) ) unset($args['address1']);
        if( isset($args['address2']) ) unset($args['address2']);
        if( isset($args['password']) ) unset($args['password']);
        if( isset($args['requirements']) ) unset($args['requirements']);
        if( isset($args['benefits']) ) unset($args['benefits']);
        if( isset($args['photo']) ) unset($args['photo']);
        if( isset($args['photos']) ) unset($args['photos']);
        if( isset($args['job_desc']) ) unset($args['job_desc']);
        if( isset($args['job_responsibilities']) ) unset($args['job_responsibilities']);
        $args = array_filter($args, function($arg){ return !is_null($arg) && $arg !== ''; });
        $args = array_map( function($arg){
            $arg = str_replace('"', "'", $arg);
            return stripslashes(strip_tags($arg));
        }, $args );
        switch($method){
            case 'first_login':
                $user = $args;
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => $user['id'],
                    'for_id' => $user['id'],
                    'type' => 'user',
                    'type_id' => $user['id'],
                    'event' => 'first login',
                    'message' => "widgets_to_be_displayed_on_your_dashboard", // Select up to 4 widgets to be displayed on your dashboard. Click here to customize your widget now
                    'url'   => url_for("/dashboard"),
                    'data'  => json_encode($user)
                ]);
                self::$db->insert();
                break;
            case 'profile_complete':
                $user = $args;
                if( ! self::is_notified($user['id'])){
                    self::$db->table("notifications");
                    self::$db->insertArray([
                        'by_id' => $user['id'],
                        'for_id' => $user['id'],
                        'type' => 'user',
                        'type_id' => $user['id'],
                        'event' => 'profile complete',
                        'message' => "your_profile_100_percent_completed", // Your profile is 100% completed
                        'url'   => url_for("/workspace/search"),
                        'data'  => json_encode($user)
                    ]);
                    self::$db->insert();
                }
                break;
            case 'task_created':
                $task = $args;
                $status = $task['status'] === 'draft' ? 'created' : $task['status'];
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['user_id'],
                    'type' => 'task',
                    'type_id' => $task['id'],
                    'event' => 'created',
                    'message' => $status === 'draft' ? "your_task_has_been_{$status}" : "your_task_has_been_published", // Your task has been published. Click here to view detail
                    'url'   => url_for("workspace/task/{$task['slug']}/details"),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_updated':
                $task = $args;
                $status = $task['status'] === 'draft' ? 'updated' : $task['status'];
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['user_id'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'updated',
                    'message' => $status === 'draft' ? "your_task_has_been_{$status}" : "your_task_has_been_published", // Your task has been updated | Your task has been published. Click here to view detail
                    'url'   => url_for("workspace/task/{$task['slug']}/details"),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_deleted':
                $task = $args;
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['user_id'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'deleted',
                    'message' => "your_task_has_been_deleted", // Your task has been deleted
                    'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_cancelled':
                $task = $args;
                $poster = $task['user_type'] === '1' ? $task['company_name'] : "{$task['firstname']} {$task['lastname']}";

                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['user_id'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'cancelled',
                    'message' => "your_task_has_been_cancelled", // Your task has been cancelled
                    'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                $applicants = (new Task(self::$db, self::$user))->get_applicants([$task['task_id']], 0, 999999);
                if( !empty($applicants) ) {
                    $ids = array_column($applicants, 'user_id');
                    if(!empty($ids)){
                        self::$db->table("notifications");
                        foreach ($ids as $id){
                            self::$db->insertArray([
                                'by_id' => self::$user->info['id'],
                                'for_id' => $id,
                                'type' => 'task',
                                'type_id' => $task['task_id'],
                                'event' => 'lost',
                                'message' => "task_you_applied_for_has_been_cancelled", // The task you applied for %{$task['title']}% has been cancelled by %{$poster}%
                                'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')),
                                'data'  => json_encode($task)
                            ]);
                            self::$db->insert();
                        }
                    }
                }
                break;
            case 'task_applied':
                $task = $args;
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['user_id'] ?? 0,
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'applied',
                    'message' => "you_have_received_new_application", // You have received a new application %{$task['title']}%
                    'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')) . '#' . strtolower($task['task_id']),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_payment_made':
                $task = $args;
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['user_id'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'payment made',
                    'message' => "you_have_successfully_paid_for", // You have successfully paid for %{$task['title']}%
                    'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')) . '#tcInProgressMonth',
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();

                //$the_seeker = self::$user->info($task['assigned_to']);
                //$poster = self::$user->info($task['user_id']);

                /*(new Mail(self::$db))->sendmail(
                    ['email' => $poster['email'], 'name' => isset($poster['company']) ? $poster['company']['name'] : $poster['firstname']],
                    'poster-escrow-notification',
                    [
                        'firstName' => $poster['firstname'],
                        'seekerName' => $the_seeker['name'],
                        'taskName' => $task['title'],
                    ]
                );*/

                break;
            case 'task_assigned':
                $mail = new Mail(self::$db);
                $task = $args;
                $poster = $task['user_type'] === '1' ? $task['company_name'] : "{$task['firstname']} {$task['lastname']}";
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['assigned_to'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'assigned',
                    'message' => "poster_has_selected_you_for", // %{$poster}% has selected you for %{$task['title']}%
                    'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')) . '#tcInProgressMonth',
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                //$the_seeker = self::$user->info($task['assigned_to']);
                /*$mail->sendmail(
                    ['email' => $assigned_user['email'], 'name' => $assigned_user['firstname']],
                    'new-task-application-status',
                    [
                        'firstName' => $assigned_user['firstname'],
                        'taskName' => $task['title'],
                        'posterName' => $poster,
                        'applicationStatus' => lang('success')
                    ]
                );*/

                /*$other_applicants = (new Task(self::$db, self::$user))->get_applicants([$task['task_id']], 0, 999999);
                if( !empty($other_applicants) ) {
                    $ids = array_diff(array_column($other_applicants, 'user_id'), (array)$task['assigned_to']);
                    if(!empty($ids)){
                        $lost_ids = implode("','", $ids);
                        self::$db->query("SELECT m.`id`, m.`firstname`, m.`email`, c.`name` AS `company`
                                          FROM `members` m
                                          LEFT JOIN `company_details` c ON c.`member_id` = m.`id`
                                          WHERE m.`id` IN('{$lost_ids}') ORDER BY m.`id`");
                        $lost_users = self::$db->getRowList();
                        $lost_users = array_combine(array_column($lost_users, 'id'), $lost_users);

                        self::$db->table("notifications");
                        foreach ($ids as $id){
                            self::$db->insertArray([
                                'by_id' => self::$user->info['id'],
                                'for_id' => $id,
                                'type' => 'task',
                                'type_id' => $task['task_id'],
                                'event' => 'lost',
                                'message' => "you_have_not_been_selected_for", // You have not been selected for %{$task['title']}%
                                'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')),
                                'data'  => json_encode($task)
                            ]);
                            self::$db->insert();

                            $mail->schedule(
                                [
                                    'to' => [ 'email' => $lost_users[$id]['email'], 'name' => $lost_users[$id]['company'] ?? $lost_users[$id]['firstname'] ],
                                    'data' => [
                                        'firstName' => $lost_users[$id]['company'] ?? $lost_users[$id]['firstname'],
                                        'taskName' => $task['title'],
                                        'posterName' => $poster,
                                        'applicationStatus' => lang('task_lost'),
                                        'schedule' => self::$date->format("Y-m-d H:i:s")
                                    ]
                                ],
                                'new-task-application-status',
                                'email'
                            );
                        }
                    }
                }*/
                break;
            case 'task_marked_completed':
                $task = $args;
                $user_name = self::$user->info['name'];
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['user_id'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'marked completed',
                    'message' => "talent_has_marked_task_completed", // %{$user_name}% has mark %{$task['title']}% as completed. Please respond within 3 days
                    'url'   => url_for("/workspace/task/{$task['slug']}/billing"),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_payment_request':
                $task = $args;
                $user_name = self::$user->info['name'];
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['user_id'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'payment request',
                    'message' => "talent_has_requested_payment", // %{$user_name}% has requested payment for %{$task['title']}%
                    'url'   => url_for("/workspace/task/{$task['slug']}/billing"),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_accepted':
                $task = $args;
                $poster = $task['user_type'] === '1' ? $task['company_name'] : "{$task['firstname']} {$task['lastname']}";
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['assigned_to'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'accepted',
                    'message' => "poster_has_agreed_task_completed", // %{$poster}% has agreed that task completed and please request the payment
                    'url'   => url_for("/workspace/task/{$task['slug']}/billing"),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_rejected':
                $task = $args;
                $poster = $task['user_type'] === '1' ? $task['company_name'] : "{$task['firstname']} {$task['lastname']}";
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['assigned_to'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'rejected',
                    'message' => "poster_has_rejected_task_completed", // %{$poster}% has rejected that %{$task['title']}% is completed, please respond within 3 days
                    'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')) . '#tcInProgressMonth',
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_offer_accepted':
                $task = $args;
                $poster = $task['user_type'] === '1' ? $task['company_name'] : "{$task['firstname']} {$task['lastname']}";
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['assigned_to'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'offer accepted',
                    'message' => "you_have_accepted_task_offer", // %{$poster}% has rejected that %{$task['title']}% is completed, please respond within 3 days
                    'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')) . '#tcInProgressMonth',
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_offer_retracted':
                $task = $args;
                $poster = $task['user_type'] === '1' ? $task['company_name'] : "{$task['firstname']} {$task['lastname']}";
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['assigned_to'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'offer retracted',
                    'message' => "poster_has_retracted_task_offer", // %{$poster}% has rejected that %{$task['title']}% is completed, please respond within 3 days
                    'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')) . '#tcInProgressMonth',
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_application_retracted':
                $task = $args;
                $poster = $task['user_type'] === '1' ? $task['company_name'] : "{$task['firstname']} {$task['lastname']}";
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['assigned_to'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'application retracted',
                    'message' => "you_have_retracted_task_application", // %{$poster}% has rejected that %{$task['title']}% is completed, please respond within 3 days
                    'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')) . '#tcInProgressMonth',
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_disputed':
                $task = $args;
                $amount = number_format($task['budget'] * ((100 - $task['rejection_rate']) / 100), 2);
                $refund = number_format($task['budget'] * ($task['rejection_rate'] / 100), 2);
                $poster = $task['user_type'] === '1' ? $task['company_name'] : "{$task['firstname']} {$task['lastname']}";
                $new_date = clone(self::$date);
                // for talent
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['assigned_to'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'disputed',
                    'message' => "poster_has_disputed_task", // %{$poster}% has disputed the %{$task['title']}%
                    'url'   => url_for("/workspace/billings/earning"),
                    'data'  => json_encode($task),
                    'created_at' => (clone($new_date))->modify('-1 seconds')->format("Y-m-d H:i:s") // just to make it be first
                ]);
                self::$db->insert();

                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['assigned_to'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'disputed',
                    'message' => "you_will_receive_disputed_amount", // You will receive the disputed amount of %RM{$amount}% via escrow
                    'url'   => url_for("/workspace/billings/earning"),
                    'data'  => json_encode($task),
                    'created_at' => $new_date->format("Y-m-d H:i:s")
                ]);
                self::$db->insert();

                // for poster
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['user_id'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'disputed',
                    'message' => "your_total_dispute_amount_will_be_released", // Your total dispute amount will be released via escrow for %RM{$amount}%
                    'url'   => url_for("/workspace/billings/spent"),
                    'data'  => json_encode($task),
                    'created_at' => (clone($new_date))->modify('-1 seconds')->format("Y-m-d H:i:s") // just to make it be inserted first
                ]);
                self::$db->insert();

                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['user_id'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'disputed',
                    'message' => "you_will_receive_returned_dispute_amount", // You will receive the returned dispute amount via escrow for %RM{$refund}%
                    'url'   => url_for("/workspace/billings/spent"),
                    'data'  => json_encode($task),
                    'created_at' => $new_date->format("Y-m-d H:i:s") // just to make it be inserted after
                ]);
                self::$db->insert();
                break;
            case 'task_fund_released':
                $task = $args;
                $poster = $task['user_type'] === '1' ? $task['company_name'] : "{$task['firstname']} {$task['lastname']}";
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['assigned_to'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'fund released',
                    'message' => "payment_has_been_released_for", // The payment has been released for %{$task['title']}%
                    'url'   => url_for("/workspace/billings/earning"),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();

                //$the_seeker = self::$user->info($task['assigned_to']);
                /*$mail->sendmail(
                    ['email' => $the_seeker['email'], 'name' => $the_seeker['firstname']],
                    'seeker-payment-notification',
                    [
                        'firstName' => $the_seeker['firstname'],
                        'taskName' => $task['title'],
                        'posterName' => $poster,
                        'billingAmount' => 'RM' . number_format($task['budget'], 2)
                    ]
                );*/

                //$poster_name = $task['user_type'] === '1' ? $task['company_name'] : $task['firstname'];
                /*$mail->sendmail(
                    ['email' => $the_poster['email'], 'name' => $poster_name],
                    'poster-payment-notification',
                    [
                        'firstName' => $poster_name,
                        'taskName' => $task['title'],
                        'seekerName' => $the_seeker['name'],
                        'billingAmount' => 'RM' . number_format($task['budget'], 2)
                    ]
                );*/

                break;
            case 'task_new_question':
                $task = $args;
                $user_name = self::$user->info['name'];
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $task['user_id'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'question',
                    'message' => "you_have_new_question_for", // You have a new question for %{$task['title']}%
                    'url'   => url_for("/workspace/task/{$task['slug']}/details"),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_new_answer':
                $task = $args;
                $poster = $task['user_type'] === '1' ? $task['company_name'] : "{$task['firstname']} {$task['lastname']}";
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => $task['user_id'],
                    'for_id' => $task['to'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'answer',
                    'message' => "you_have_received_answer_for", // You have received an answer for your question in %{$task['title']}%
                    'url'   => url_for("/workspace/task/{$task['slug']}/details"),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_review':
                $task = $args;
                $poster = isset($task['reviewer']['company_name']) ? $task['reviewer']['name'] : $task['reviewer']['company_name'];
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => $task['from'],
                    'for_id' => $task['to'],
                    'type' => 'task',
                    'type_id' => $task['task_id'],
                    'event' => 'review',
                    'message' => "you_have_received_rating_for", // You have received a rating for %{$task['title']}%
                    'url'   => url_for("/workspace/rating-centre"),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_review_reminder':
                $task = $args;
                $poster = isset($task['reviewer']['company_name']) ? $task['reviewer']['name'] : $task['reviewer']['company_name'];
                $page = $task['user_id'] === self::$user->info['id'] ? 'spent' : 'earning';
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => $task['from'],
                    'for_id' => $task['to'],
                    'type' => 'task',
                    'type_id' => $task['id'],
                    'event' => 'review reminder',
                    'message' => "you_have_not_rated_poster_for_task", // You have not rate (%{$poster}%) for the task %{$task['title']}%
                    'url'   => url_for("/workspace/billings/{$page}") . "?review={$task['id']}",
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_expiring':
                $task = $args;
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => $task['user_id'],
                    'for_id' => $task['user_id'],
                    'type' => 'task',
                    'type_id' => $task['id'],
                    'event' => 'task expiring',
                    'message' => "your_task_will_expire_in_24hrs", // Your task will be expiring in 24 hours with no candidates application. Click here to modify your task detail
                    'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')) . '#' . strtolower($task['id']),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_50_milestone':
                $task = $args;
                $date = date(self::settings()['date_format'], strtotime($task['complete_by']));
                $hours_left = \Carbon\Carbon::parse($task['complete_by'])->diffInRealHours(\Carbon\Carbon::now());
                self::$db->table("notifications");
                // for poster
                self::$db->insertArray([
                    'by_id' => $task['user_id'],
                    'for_id' => $task['user_id'],
                    'type' => 'task',
                    'type_id' => $task['id'],
                    'event' => 'task approaching end',
                    'message' => "task_will_end_on", // The task %{$task['title']}% will be ending on %{$date}%
                    'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')) . '#tcInProgressMonth',
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                //for seeker
                self::$db->insertArray([
                    'by_id' => $task['user_id'],
                    'for_id' => $task['assigned_to'],
                    'type' => 'task',
                    'type_id' => $task['id'],
                    'event' => 'task approaching end',
                    'message' => "task_needs_to_be_completed_by", // %{$task['title']}% need to be completed by %{$date}%. %{$hours_left} hours% to go!
                    'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')) . '#tcInProgressMonth',
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'task_full_refund':
                $task = $args;
                self::$db->table("notifications");
                //for poster
                self::$db->insertArray([
                    'by_id' => $task['user_id'],
                    'for_id' => $task['user_id'],
                    'type' => 'task',
                    'type_id' => $task['id'],
                    'event' => 'full refund',
                    'message' => "your_refund_in_process_for", // Your refund is in process for %{$task['title']}%
                    'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                //for seeker
                self::$db->insertArray([
                    'by_id' => $task['user_id'],
                    'for_id' => $task['assigned_to'],
                    'type' => 'task',
                    'type_id' => $task['id'],
                    'event' => 'cancelled',
                    'message' => "your_project_has_been_cancelled", // Your project %{$task['title']}% has been cancelled
                    'url'   => url_for('/workspace', self::$date->format('Y'), self::$date->format('m')),
                    'data'  => json_encode($task)
                ]);
                self::$db->insert();
                break;
            case 'job_closed':
                $job = $args;
                $date = self::$date->format('d M Y');
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => $job['user_id'],
                    'for_id' => $job['user_id'],
                    'type' => 'job',
                    'type_id' => $job['id'],
                    'event' => 'closed',
                    'message' => "your_job_is_closed_on", // Your job %{$job['number']}% is closed on %{$date}%
                    'url'   => url_for('/workspace/job-centre'),
                    'data'  => json_encode($job)
                ]);
                self::$db->insert();
                break;
            case 'job_applied':
                $job = $args;
                $date = self::$date->format('d M Y');
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $job['user_id'] ?? 0,
                    'type' => 'job',
                    'type_id' => $job['job_id'],
                    'event' => 'applied',
                    'message' => "you_have_received_new_application", // You have received a new application %{$job['title']}%
                    'url'   => url_for('workspace/job-centre') . '#' . strtolower($job['job_id']),
                    'data'  => json_encode($job)
                ]);
                self::$db->insert();
                break;
            case 'job_new_question':
                $job = $args;
                $user_name = self::$user->info['name'];
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => self::$user->info['id'],
                    'for_id' => $job['user_id'],
                    'type' => 'job',
                    'type_id' => $job['job_id'],
                    'event' => 'question',
                    'message' => "you_have_new_question_for", // You have a new question for %{$job['title']}%
                    'url'   => url_for("/workspace/job/{$job['slug']}/details"),
                    'data'  => json_encode($job)
                ]);
                self::$db->insert();
                break;
            case 'job_new_answer':
                $job = $args;
                $poster = $job['user_type'] === '1' ? $job['company_name'] : "{$job['firstname']} {$job['lastname']}";
                self::$db->table("notifications");
                self::$db->insertArray([
                    'by_id' => $job['user_id'],
                    'for_id' => $job['to'],
                    'type' => 'job',
                    'type_id' => $job['job_id'],
                    'event' => 'answer',
                    'message' => "you_have_received_answer_for", // You have received an answer for your question in %{$job['title']}%
                    'url'   => url_for("/workspace/job/{$job['slug']}/details"),
                    'data'  => json_encode($job)
                ]);
                self::$db->insert();
                break;
            case 'chat_new_message':
                $chat = $args;
                $post = Task::init(self::$db, self::$user)->get_task_by_id($chat['task_id']);
                if( empty($post) )
                    $post = Job::init(self::$db, self::$user)->get_job_by_id($chat['task_id']);

                if( isset($post['task_id']) )
                    $url = url_for('/workspace', self::$date->format('Y'), self::$date->format('m')) . '#' . strtolower($chat['task_id']);
                else
                    $url = url_for('/workspace/job-centre') . '#' . strtolower($chat['task_id']);

                $post_type       = isset($post['task_id']) ? 'task' : 'job';
                $sender          = self::$user->info($chat['sender_id'], true);
                $sender_name     = $sender['type'] === '1' && isset($sender['company']) ? $sender['company']['name'] : $sender['name'];
                $receiver        = self::$user->info($chat['receiver_id'], true);
                $receiver_name   = $receiver['type'] === '1' && isset($receiver['company']) ? $receiver['company']['name'] : $receiver['name'];
                $message         = (int) $chat['unread_count'] === 1 ? 'user_has_sent_you_message' : 'user_has_sent_you_messages';//"%{$sender_name}% has sent you ({$chat['unread_count']}) message" . plural($chat['unread_count']);
                $chat['url']     = $url;
                $chat['message'] = $message;
                $chat['post_type'] = $post_type;
                if(! self::is_new_message_notified(self::$user->info['id'], $chat)) {
                    unset($chat['url'], $chat['message'], $chat['post_type']);
                    self::$db->table("notifications");
                    self::$db->insertArray([
                        'by_id'   => $chat['sender_id'],
                        'for_id'  => $chat['receiver_id'],
                        'type'    => "{$post_type}-chat",
                        'type_id' => $chat['task_id'],
                        'event'   => "new {$post_type} message",
                        'message' => $message,
                        'url'     => $url,
                        'data'    => json_encode($chat)
                    ]);
                    self::$db->insert();
                }
                break;
            default:
                halt("Please implement the following: case '{$method}' in Notification Class, under __callStatic() method");
            break;
        }
    }

    public function settings(){
        if( ! isset($_SESSION['settings']) ) {
            $setting  = array();
            $settings = array();

            self::$db->query("SELECT `name`, `value` FROM settings");
            $settings = self::$db->getRowList();

            foreach ($settings as $keys) {
                $setting[$keys['name']] = $keys['value'];
            }

            if ($setting['date_format'] == 'custom') $setting['date_format'] = $setting['date_format_custom'];
            if ($setting['time_format'] == 'custom') $setting['time_format'] = $setting['time_format_custom'];
            if ($setting['session_timeout'] == 'custom') $setting['session_timeout'] = $setting['session_timeout_custom'];
            $_SESSION['settings'] = $setting;
        }else{
            $setting = $_SESSION['settings'];
        }
        return $setting;
	}

    public function notifications( $user_id = null ){
        $this->checkIfTableExists();
        if( is_null($user_id) ) return [];
        $user_id = self::$db->escape($user_id);
        self::$db->query("SELECT n.`id`, n.`type`, n.`for_id`, n.`message`, n.`data`, n.`url`, n.`type_id`, n.`event`, n.`created_at`, 
                          n.`viewed`, n.`looked_at`, CONCAT(m.`firstname`, ' ', m.`lastname`) AS `user_name`, m.`photo`, GROUP_CONCAT(m.`id`) AS `users`, c.`name` AS `company`, 
                          (
                            SELECT COUNT(*) FROM `notifications` WHERE `type` = n.`type` AND `event` = n.`event` AND `for_id` = n.`for_id` AND `type_id` = n.`type_id` AND `message` = n.`message`
                          ) AS `count` 
                          FROM `notifications` n 
                          INNER JOIN ( SELECT `for_id`, MAX(`created_at`) AS `created_at` FROM `notifications` WHERE `for_id` = {$user_id} GROUP BY `for_id`, `type`, `event`, `type_id`, `message` ORDER BY `created_at` DESC LIMIT 20 ) jn 
                              ON n.`for_id` = jn.`for_id` AND n.`created_at` = jn.`created_at` 
                          INNER JOIN `members` m ON m.`id` = n.`by_id` 
                          LEFT JOIN `company_details` c ON c.`member_id` = m.`id` 
                          WHERE n.`type` IN('task', 'user', 'task-chat') AND n.`for_id` = {$user_id} 
                          GROUP BY `type`, `event`, `type_id`, `message` 
                          ORDER BY n.`created_at` DESC");

        $on_demand['notifications'] = self::$db->getRowList(); // n.`viewed` = 'N' AND

        /*self::$db->query("SELECT COUNT(*) FROM `notifications` WHERE `viewed` = 'N' AND `looked_at` IS NULL AND `type` IN('task', 'user', 'task-chat')
                          AND `for_id` = {$user_id} GROUP BY `type`, `event`, `type_id` ORDER BY `created_at` DESC LIMIT 10");
        $new_on_demand = self::$db->getRowList();*/
        $on_demand['new'] = count(array_filter(array_column($on_demand['notifications'], 'looked_at'), function($notification){ return is_null($notification); }));

        self::$db->query("SELECT n.`id`, n.`type`, n.`for_id`, n.`message`, n.`data`, n.`url`, n.`type_id`, n.`event`, n.`created_at`, 
                          n.`viewed`, n.`looked_at`, CONCAT(m.`firstname`, ' ', m.`lastname`) AS `user_name`, m.`photo`, GROUP_CONCAT(m.`id`) AS `users`, c.`name` AS `company`, 
                          (
                            SELECT COUNT(*) FROM `notifications` WHERE `type` = n.`type` AND `event` = n.`event` AND `for_id` = n.`for_id` AND `type_id` = n.`type_id` AND `message` = n.`message`
                          ) AS `count`                             
                          FROM `notifications` n 
                          INNER JOIN ( SELECT `for_id`, MAX(`created_at`) AS `created_at` FROM `notifications` WHERE `for_id` = {$user_id} GROUP BY `for_id`, `type`, `event`, `type_id`, `message` ORDER BY `created_at` DESC LIMIT 20 ) jn 
                              ON n.`for_id` = jn.`for_id` AND n.`created_at` = jn.`created_at`
                          INNER JOIN `members` m ON m.`id` = n.`by_id` 
                          LEFT JOIN `company_details` c ON c.`member_id` = m.`id` 
                          WHERE n.`type` IN('job', 'job-chat') AND n.`for_id` = {$user_id}  
                          GROUP BY `type`, `event`, `type_id`, `message`
                          ORDER BY n.`created_at` DESC");
        $full_time['notifications'] = self::$db->getRowList(); // n.`viewed` = 'N' AND

        /*self::$db->query("SELECT COUNT(*) FROM `notifications` WHERE `viewed` = 'N' AND `looked_at` IS NULL AND `type` IN('job', 'job-chat')
                          AND `for_id` = {$user_id} GROUP BY `type`, `event`, `type_id` ORDER BY `created_at` DESC LIMIT 10");
        $new_full_time = self::$db->getRowList();*/
        $full_time['new'] = count(array_filter(array_column($full_time['notifications'], 'looked_at'), function($notification){ return is_null($notification); }));

        foreach($on_demand['notifications'] as $key => $notification){
            $data = json_decode($notification['data'], true);
            $notification['message'] = lang($notification['message']);

            if( strpos($notification['message'], '$poster') !== false ) {
                $poster = !empty($notification['company']) ? $notification['company'] : $notification['user_name'];
                $notification['message'] = str_replace('$poster', $poster, $notification['message']);
            }

            if( strpos($notification['message'], '$talent') !== false ) {
                $notification['message'] = str_replace('$talent', $notification['user_name'], $notification['message']);
            }

            if( strpos($notification['message'], '$title') !== false ) {
                $notification['message'] = str_replace('$title', $data['title'], $notification['message']);
            }

            if( strpos($notification['message'], '$sender') !== false ) {
                $sender = !empty($notification['company']) ? $notification['company'] : $notification['user_name'];
                $notification['message'] = str_replace('$sender', $sender, $notification['message']);
            }

            if( strpos($notification['message'], '$id') !== false ) {
                $notification['message'] = str_replace('$sender', $data['number'], $notification['message']);
            }

            if( strpos($notification['message'], '$count') !== false ) {
                $notification['message'] = str_replace('$count', $data['count'], $notification['message']);
            }

            if( strpos($notification['message'], '$amount') !== false ) {
                if( isset($data['rejection_rate']) )
                    $amount = 'RM' . number_format($data['budget'] * ((100 - $data['rejection_rate']) / 100), 2);
                else
                    $amount = 'RM' . number_format($data['budget'], 2);
                $notification['message'] = str_replace('$amount', $amount, $notification['message']);
            }

            if( strpos($notification['message'], '$refund') !== false ) {
                if( isset($data['rejection_rate']) )
                    $refund = 'RM' . number_format($data['budget'] * ($data['rejection_rate'] / 100), 2);
                else
                    $refund = 'RM' . number_format($data['budget'], 2);
                $notification['message'] = str_replace('$refund', $refund, $notification['message']);
            }

            if( strpos($notification['message'], '$date') !== false ) {
                $date = date(self::settings()['date_format'], strtotime($data['complete_by']));
                $hours_left = \Carbon\Carbon::parse($data['complete_by'])->diffInRealHours(\Carbon\Carbon::now());
                $notification['message'] = str_replace('$date', $date, $notification['message']);
                $notification['message'] = str_replace('$hours', $hours_left, $notification['message']);
            }

            $on_demand['notifications'][$key]['message'] = $notification['message'];
        }

        foreach($full_time['notifications'] as $key => $notification){
            $data = json_decode($notification['data'], true);
            $notification['message'] = lang($notification['message']);

            if( strpos($notification['message'], '$sender') !== false ) {
                $sender = !empty($notification['company']) ? $notification['company'] : $notification['user_name'];
                $notification['message'] = str_replace('$sender', $sender, $notification['message']);
            }

            if( strpos($notification['message'], '$id') !== false ) {
                $notification['message'] = str_replace('$sender', $data['number'], $notification['message']);
            }

            if( strpos($notification['message'], '$count') !== false ) {
                $notification['message'] = str_replace('$count', $data['count'], $notification['message']);
            }

            if( strpos($notification['message'], '$date') !== false ) {
                $date = date(self::settings()['date_format'], strtotime($data['complete_by']));
                $hours_left = \Carbon\Carbon::parse($data['complete_by'])->diffInRealHours(\Carbon\Carbon::now());
                $notification['message'] = str_replace('$date', $date, $notification['message']);
                $notification['message'] = str_replace('$hours', $hours_left, $notification['message']);
            }

            $full_time['notifications'][$key]['message'] = $notification['message'];
        }

        return ['on_demand' => $on_demand, 'full_time' => $full_time];
    }

    public function get_notification(){
        $this->checkIfTableExists();
        if( !is_null(params('id')) ){
            @list($id, $url) = explode('|', base64_decode(params('id')), 2);
            $id = sanitize($id, 'int');
            $url = urldecode($url);
            $notification = self::$db->query("SELECT * FROM `notifications` WHERE `id` = '{$id}' LIMIT 1");
            $event = self::$db->getSingleRow();
            $this->mark_read($id, $event);

            if( $event['event'] === 'assigned' ){
                $task = Task::init(self::$db, self::$user)->get_task_by_id($event['type_id']);
                $task_date = \Carbon\Carbon::parse($task['start_by']);
                $url = url_for('workspace', $task_date->year, $task_date->month) . '#tcInProgressMonth';
            }else{
                $url = url_for(str_replace(['/translation/', '/staging/'], '', $url));
                //$url = url_for(ltrim($url, ROOTPATH));
            }

            redirect_to( option('site_uri') . $url );
        }
    }

    public function looked_at(){
        $this->checkIfTableExists();
        $user_id = self::$user->info['id'];

        $looked_at = self::$db->queryOrDie("UPDATE `notifications` SET `looked_at` = NOW() WHERE `for_id` = '{$user_id}'");

        if( is_null($looked_at) ) echo json(['updated' => 'success']);
        else echo json(['updated' => 'failed']);
    }

    private function is_notified($user_id = null, $type = 'user'){
        $notification = self::$db->query("SELECT count(`id`) FROM `notifications` WHERE `for_id` = '{$user_id}' AND `type` = 'user' AND `event` = 'profile complete' LIMIT 1");
        return (int)self::$db->getValue() > 0;
    }

    private function is_new_message_notified($user_id = null, $chat){
        $chat_encoded = json_encode($chat);
        self::$db->query("SELECT * FROM `notifications` WHERE `for_id` = '{$chat['receiver_id']}' AND `by_id` = '{$chat['sender_id']}' AND `type` = '{$chat['post_type']}-chat' AND `type_id` = '{$chat['task_id']}' AND `event` = 'new {$chat['post_type']} message' AND `url` = '{$chat['url']}' AND `message` = '{$chat['message']}' AND JSON_EXTRACT(`data` ,'$.id') = '{$chat['id']}'");
        $notifications = self::$db->getRowList();

        /*if( $notifications ){
            foreach ($notifications as $notification) {
                unset($notification['id'], $notification['data'], $notification['looked_at'], $notification['updated_at'], $notification['message'], $notification['created_at']);
                self::$db->table('notifications');
                self::$db->whereArray($notification);
                self::$db->delete();
            }
        }*/
        return count($notifications) > 0;
    }

    private function mark_read($id = null, $event = null){
        $this->checkIfTableExists();
        if( is_null($id) ) return;

        if( is_null($event) )
            self::$db->queryOrDie("UPDATE `notifications` SET `viewed` = 'Y', `looked_at` = NOW() WHERE `id` = '{$id}' LIMIT 1");
        else
            self::$db->queryOrDie("UPDATE `notifications` SET `viewed` = 'Y', `looked_at` = NOW() WHERE `for_id` = '{$event['for_id']}' AND `type_id` = '{$event['type_id']}' AND `event` = '{$event['event']}'");
    }

    private function checkIfTableExists(){
        self::$db->query("SHOW TABLES LIKE 'notifications'");
        $table_exists = self::$db->getSingleRow();
        if(empty($table_exists)){
            self::create_table();
        }else{
            self::$db->query("DESC `notifications`");
            $fields = self::$db->getRowList();

            if(!in_array('for_id', array_column($fields, 'Field')) || !in_array('looked_at', array_column($fields, 'Field'))){
                self::drop_table();
                self::create_table();
            }
        }
    }

    private function create_table(){
        self::$db->queryOrDie("
                        CREATE TABLE `notifications` (
                            `id` INT(11) NOT NULL AUTO_INCREMENT,
                            `for_id` INT(11) NOT NULL,
	                        `by_id` INT(11) NOT NULL,
                            `type` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                            `type_id` VARCHAR(36) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                            `event` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                            `message` TEXT NOT NULL COLLATE 'utf8_unicode_ci',
                            `url` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                            `data` JSON NULL DEFAULT NULL,
                            `viewed` ENUM('Y','N') NOT NULL DEFAULT 'N' COLLATE 'utf8_unicode_ci',
                            `looked_at` TIMESTAMP NULL DEFAULT NULL,
                            `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                            PRIMARY KEY (`id`) USING BTREE,
                            INDEX `viewed` (`viewed`) USING BTREE,
                            INDEX `looked_at` (`looked_at`) USING BTREE,
                            INDEX `for_id` (`for_id`) USING BTREE,
                            INDEX `by_id` (`by_id`) USING BTREE
                        ) COLLATE='utf8_unicode_ci' ENGINE=MyISAM;"
        );
    }

    private function drop_table(){
        self::$db->queryOrDie("DROP TABLE `notifications`");
    }

    public function chat_cron(){
        self::$db->query("SELECT *,
                          (SELECT `title` FROM `tasks` WHERE `id` = `task_id` UNION SELECT `title` FROM `jobs` WHERE `id` = `task_id` LIMIT 1) AS `title`, 
                          (SELECT `email` FROM `members` WHERE `id` = `receiver_id` LIMIT 1) AS `email`,
                          (SELECT CONCAT(`firstname`, ' ', `lastname`) AS `name` FROM `members` WHERE `id` = `receiver_id` LIMIT 1) AS `name`,
                          (CASE WHEN `receiver_id` = (SELECT `user_id` FROM `tasks` WHERE `id` = `task_id` UNION SELECT `user_id` FROM `jobs` WHERE `id` = `task_id` LIMIT 1) THEN 'posted' ELSE 'applied' END) AS `section`,
                          LOWER((SELECT `code` FROM `language` WHERE `id` IN (SELECT `language` FROM `preference` WHERE `member_id` = `receiver_id`) LIMIT 1)) AS `language`, 
                          COUNT(*) AS `count` 
                          FROM `chats` WHERE `viewed` = '0' AND `deleted` = '0' GROUP BY `receiver_id`, `task_id` ORDER BY `created_by` ASC");
        $chats = self::$db->getRowList();

        $unread_messages_count = count($chats);
        if($unread_messages_count > 0) {
            $mail = new Mail(self::$db);
            $ids = [];

            foreach ($chats as $chat) {
                $chat['unread_count'] = $chat['count'];
                Event::trigger('chat.new.message', [$chat]);
                if( $chat['viewed_date'] === '0000-00-00 00:00:00' ) {
                    $mail->schedule([
                        'to' => [
                            'name' => $chat['name'],
                            'email' => $chat['email']
                        ],
                        'data' =>[
                            'language' => $chat['language'],
                            'subject' => "You have received new chat message for task {$chat['title']}",
                            'task_id' => $chat['task_id'],
                            'section' => $chat['section'],
                            'title' => $chat['title']
                        ]
                    ], 'you-received-chat-message', 'email');
                    $ids[] = $chat['id'];
                }
            }

            if( !empty($ids) ){
                $ids = implode(',', $ids);
                self::$db->queryOrDie("UPDATE `chats` SET `viewed_date` = NOW() WHERE `id` IN({$ids})");
            }
        }

        echo 'DONE';
    }
}