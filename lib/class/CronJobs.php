<?php

require_once __DIR__ . '/../../vendor/autoload.php';

use Carbon\Carbon;

class CronJobs{

    protected $db;
    protected $host;

    /**
     * @var $db db
     */
    public function __construct($db){
        $this->db = $db;
        //$this->host = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . ($_SERVER['HTTP_HOST'] ?? '');
    }

    public function completeProfileFirstReminder(){
        $threeDaysAgo = Carbon::now()->subDays(3)->toDateString();
        $this->db->query("
                            SELECT m.`id`, m.`firstname`, m.`email`, 
                            (SELECT LOWER(`code`) FROM `language` WHERE `id` = p.`language` LIMIT 1) AS `language`,
                            m.`date_created` FROM `members` m 
                            LEFT JOIN `preference` p ON p.`member_id` = m.`id`
                            WHERE m.`type` = 0 AND m.`completion` < 100 AND m.`id` IN(
                                SELECT `member_id` FROM `member_logs` WHERE DATE(`date`) <= '{$threeDaysAgo}'
                                GROUP BY `member_id` ORDER BY `date` 
                            )
                            GROUP BY m.`id`
                       ");

        $members = $this->db->getRowList();

        if($members){
            $mail = new Mail($this->db);
            foreach ($members as $member){
                $to = [ 'email' => $member['email'], 'name' => $member['firstname'] ];
                $data = [
                    'link' => option('site_uri') . url_for('my_profile'),
                    'first_name' => $member['firstname'],
                    'number' => Carbon::now()->diffInDays(Carbon::parse($member['date_created']))
                ];

                $mail->sendmail($to, 'complete-profile-reminder-1', $data, $member['language']);
                sleep(1);
            }
        }

    }

    public function completeProfileSecondReminder(){
        $fiveDaysAgo = Carbon::now()->subDays(5)->toDateString();

        $this->db->query("
                            SELECT m.`id`, m.`firstname`, m.`email`, m.`date_created`,
                            (SELECT LOWER(`code`) FROM `language` WHERE `id` = p.`language` LIMIT 1) AS `language`
                            FROM `members` m 
                            LEFT JOIN `preference` p ON p.`member_id` = m.`id` 
                            WHERE m.`type` = 0 AND m.`completion` < 100 AND m.`id` IN(
                                SELECT `member_id` FROM `member_logs` WHERE DATE(`date`) <= '{$fiveDaysAgo}'
                                GROUP BY `member_id` ORDER BY `date` 
                            )
                            GROUP BY m.`id`
                       ");

        $members = $this->db->getRowList();

        if($members){
            $mail = new Mail($this->db);
            foreach ($members as $member){
                $to = [ 'email' => $member['email'], 'name' => $member['firstname'] ];
                $data = [
                    'link' => option('site_uri') . url_for('my_profile'),
                    'first_name' => $member['firstname'],
                    'number' => Carbon::now()->diffInDays(Carbon::parse($member['date_created']))
                ];

                $mail->sendmail($to, 'complete-profile-reminder-2', $data, $member['language']);
                sleep(1);
            }
        }
    }

    public function newTasksSkillBased(){

        $this->db->query("SELECT m.`id`, m.`firstname`, m.`email`, m.`skills`,
                          (SELECT LOWER(`code`) FROM `language` WHERE `id` = p.`language` LIMIT 1) AS `language`
                          FROM `members` m 
                          LEFT JOIN `preference` p ON p.`member_id` = m.`id`  
                          WHERE m.`type` = 0 ");
        $members = $this->db->getRowList();

        if($members){
            $mail = new Mail($this->db);
            foreach ($members as $member){

                if( empty($member['skills']) ) continue;

                $skills = explode(',', $member['skills']);
                $where = '';

                if( !empty($skills) ) {
                    $where .= '(';
                    $count = count($skills);
                    foreach ($skills as $index => $skill) {
                        $where .= " FIND_IN_SET({$skill}, `skills`) ";
                        if($index+1 < $count) $where .= ' OR ';
                    }
                }

                if( !empty($where) ) $where .= ') AND ';

                $where .= " `status` = 'published' AND `assigned` IS NULL AND `assigned_to` IS NULL";

                $this->db->query("SELECT `number`, `title`, `slug`, SUBSTR(`description`, 1, 179) AS `description`, `budget` 
                FROM `tasks` WHERE {$where} ORDER BY `created_at` LIMIT 5");
                $tasks = $this->db->getRowList();

                if($tasks) {
                    $to   = ['email' => $member['email'], 'name' => $member['firstname']];
                    $data = [ 'tasks' => $tasks ];

                    $mail->sendmail($to, 'task-skill-matching', $data, $member['language']);
                    sleep(1);
                }
            }
        }
    }

    public function expandYourCapabilities(){

        $this->db->query("SELECT m.`id`, m.`firstname`, m.`email`,
                         (CHAR_LENGTH(TRIM(BOTH ',' FROM `skills`)) - CHAR_LENGTH(
                         REPLACE(TRIM(BOTH ',' FROM `skills`), ',', '')) + 1) AS `total`,
                         (SELECT LOWER(`code`) FROM `language` WHERE `id` = p.`language` LIMIT 1) AS `language`
                         FROM `members` m
                         LEFT JOIN `preference` p ON p.`member_id` = m.`id` 
                         WHERE `type` = 0
                         HAVING `total` < 5
        ");

        $members = $this->db->getRowList();

        if ($members) {
            $mail = new Mail($this->db);
            foreach ($members as $member) {

                $to   = ['email' => $member['email'], 'name' => $member['firstname']];
                $data = [
                    'link'       => option('site_uri') . url_for('my_cv'),
                    'first_name' => $member['firstname'],
                ];

                $mail->sendmail($to, 'expand-your-capabilities', $data, $member['language']);
                sleep(1);
            }
        }
    }

    public function newUrgentTasks(){
        $today = Carbon::now()->toDateString();

        $this->db->query("SELECT m.`id`, m.`firstname`, m.`email`, m.`skills`,
                          (SELECT LOWER(`code`) FROM `language` WHERE `id` = p.`language` LIMIT 1) AS `language`
                          FROM `members` m
                          LEFT JOIN `preference` p ON p.`member_id` = m.`id`
                          WHERE `type` = 0 ");
        $members = $this->db->getRowList();

        if($members){
            $mail = new Mail($this->db);
            foreach ($members as $member){

                if( empty($member['skills']) ) continue;

                $skills = explode(',', $member['skills']);
                $where = '';

                if( !empty($skills) ) {
                    $where .= '(';
                    $count = count($skills);
                    foreach ($skills as $index => $skill) {
                        $where .= " FIND_IN_SET({$skill}, `skills`) ";
                        if($index+1 < $count) $where .= ' OR ';
                    }
                }

                if( !empty($where) ) $where .= ') AND ';

                $where .= " `urgent` = 1 AND `status` = 'published' AND `assigned` IS NULL AND `assigned_to` IS NULL AND DATE(`created_at`) = '{$today}' ";

                $this->db->query("SELECT `number`, `title`, `slug`, SUBSTR(`description`, 1, 179) AS `description`, `budget` 
                FROM `tasks` WHERE {$where} ORDER BY `created_at` LIMIT 5");
                $tasks = $this->db->getRowList();

                if($tasks) {
                    $to   = ['email' => $member['email'], 'name' => $member['firstname']];
                    $data = [ 'tasks' => $tasks ];

                    $mail->sendmail($to, 'urgent-task-notification', $data, $member['language']);
                    sleep(1);
                }
            }
        }
    }

    public function scheduledEmails(){
        $this->db->query("SELECT * FROM `schedule` WHERE `type` = 'email' AND `updated_at` IS NULL");
        $emails = $this->db->getRowList();
        $ids = [];

        if( !empty($emails) ){
            $mail = new Mail($this->db);
            $counter = 1;
            foreach ($emails as $email){
                if( Carbon::parse($email['scheduled_at'])->isFuture() ) continue;
                if( $counter === 10 ) {
                    $counter = 1;
                    sleep(1);
                }

                $data = json_decode($email['data'], true);
                $mail->sendmail($data['to'], $email['action'], $data['data'], $data['data']['language'] ?? '');
                $ids[] = $email['id'];
                $counter++;
            }
        }

        if( !empty($ids) ){
            $ids = implode(',', $ids);
            $this->db->queryOrDie("DELETE FROM `schedule` WHERE `id` IN({$ids})");
        }

    }

}