<?php
class pagination{
	public $data;
	public $limit;
	public $page;
	public $path;
	public $totalRecord;
	public $currentPage;
	public $totalPage;
	public $startValue;
	public $pageData;
	public $format;
	
	public function __construct($data, $limit, $page, $path = '', $format = '?page='){
		$this->data = $data;
		$this->limit = $limit;
		$this->page = $page;
		$this->totalRecord = $this->getTotalData();
		$this->totalPage = $this->getTotalPage();
		$this->startValue = $this->getStartData();
		$this->path = $path;
		$this->format = $format;
	}
	
	private function getTotalData(){
		return count($this->data);
	}
	
	private function getTotalPage(){
		$pages  = intval($this->totalRecord / $this->limit);
        if($this->totalRecord % $this->limit > 0){
			$pages++;
		}
        return $pages;
	}
	
	private function getStartData(){
		if(empty($this->page)){
			$this->page = 1;
		}
		
		if(isset($this->page) && $this->page > 0){
			$this->currentPage = $this->page;
			if($this->currentPage > $this->totalPage){
				$this->currentPage = 1;
			}
			$start = ($this->currentPage * $this->limit) - $this->limit;
		}else if($this->page > $this->totalPage){
			$start = $this->totalPage;
		}else{
			$start= 0;
		}
		
		return $start;
	}

	public function setStartData($page = 1){
		$this->page = $page;

		if(isset($this->page) && $this->page > 0){
			$this->currentPage = $this->page;
			if($this->currentPage > $this->totalPage){
				$this->currentPage = 1;
			}
			$this->startValue = ($this->currentPage * $this->limit) - $this->limit;
		}else if($this->page > $this->totalPage){
			$this->startValue = $this->totalPage;
		}else{
			$this->startValue = 0;
		}
	}
	
	public function result(){
	    $this->pageData = [];
		for($i = $this->startValue; $i < ($this->startValue + $this->limit); $i++){
			if(!empty($this->data[$i])){
				$this->pageData[] = $this->data[$i];
			}
		}
		return $this->pageData;
	}
	
	public function display(){
		if($this->currentPage <= 1){
			$next = 2;
		}else{
			$next = $this->currentPage + 1;
		}
		
		$html = '';
		
		$prev = $this->currentPage - 1;
		$last = $this->totalPage;

		if($this->currentPage > 1):
			$html .= '<li class="page-item page-item-arrow page-item-prev">
                          <a class="page-link" href="'.$this->path. $this->format .$prev.'" tabindex="-1">
                              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none"
                                   stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                   stroke-linejoin="round">
                                  <path d="M15 18l-6-6 6-6" /></svg>
                          </a>
                      </li>';
		else:
			$html .= '<li class="page-item page-item-arrow page-item-prev disabled">
                          <a class="page-link" href="#" tabindex="-1">
                              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none"
                                   stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                   stroke-linejoin="round">
                                  <path d="M15 18l-6-6 6-6" /></svg>
                          </a>
                      </li>';
		endif;

		if($this->totalPage > 1):
		    if ($this->currentPage > 3):
                $html .= '<li class="page-item">
                    <a class="page-link" href="'.$this->path . $this->format . '1">1</a>
                </li>
                <li class="page-item disabled">
                    <a class="page-link" href="#">...</a>
                </li>';
            endif;

            if (($prev-1) > 0):
                $html .= '<li class="page-item">
                    <a class="page-link" href="'. $this->path . $this->format . ($prev - 1) . '">' .
                        ($prev - 1)
                    .'</a>
                </li>';
            endif;

            if ($prev > 0):
                $html .= '<li class="page-item">
                    <a class="page-link" href="' . $this->path . $this->format . $prev . '">' .
                        $prev
                    .'</a>
                </li>';
            endif;

            $html .= '<li class="page-item active disabled">
                <a class="page-link" href="#">' . $this->currentPage . '<span
                            class="sr-only">(current)</span></a>
            </li>';

            if ($next < ($this->totalPage+1)):
                $html .= '<li class="page-item">
                    <a class="page-link" href="' . $this->path . $this->format . $next . '">' .
                        $next
                    . '</a>
                </li>';
            endif;

            if (($next+1) < ($this->totalPage+1)):
                $html .= '<li class="page-item">
                    <a class="page-link" href="' . $this->path . $this->format . ($next + 1) . '">' .
                        ($next + 1)
                    . '</a>
                </li>';
            endif;

            if ($this->currentPage < ($this->totalPage-2)):
                $html .= '<li class="page-item disabled">
                    <a class="page-link" href="#">...</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="' . $this->path . $this->format . $this->totalPage . '">' .
                        $this->totalPage
                    . '</a>
                </li>';
            endif;

		else:
            $html .= '<li class="page-item active">
                          <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
                      </li>';
		endif;

        if($this->currentPage < $this->totalPage):
            $html .= '<li class="page-item page-item-arrow page-item-next">
                          <a class="page-link" href="'.$this->path.$this->format.$next.'">
                              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none"
                                   stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                   stroke-linejoin="round">
                                  <path d="M9 18l6-6-6-6" /></svg>
                          </a>
                      </li>';
		else:
			$html .= '<li class="page-item page-item-arrow page-item-next disabled">
                          <a class="page-link" href="#">
                              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none"
                                   stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                   stroke-linejoin="round">
                                  <path d="M9 18l6-6-6-6" /></svg>
                          </a>
                      </li>';
		endif;

		return $html;
    } 
}

// Farid's
class fpagination{
	var $data;
	var $limit;
	var $page;
	var $param;
	var $path;
	var $totalRecord;
	var $currentPage;
	var $totalPage;
	var $startValue;
	var $pageData;
	public $modified;
	
	public function __construct($data, $limit, $page, $param = array(), $path = 'page', $totalPages = null){
		$this->data = $data;
		$this->limit = $limit;
		$this->page = $page;
		$this->totalRecord = !is_null($totalPages) ? $totalPages : $this->getTotalData();
		$this->totalPage = $this->getTotalPage();
		$this->startValue = $this->getStartData();
		$this->param = $param;
		$this->path = $path;
		$this->modified = is_null($totalPages);
	}
	
	private function getTotalData(){
		return count($this->data);
	}
	
	private function getTotalPage(){
		$pages  = intval($this->totalRecord / $this->limit);
        if($this->totalRecord % $this->limit > 0){
			$pages++;
		}
        return $pages;
	}
	
	private function getStartData(){
		if(empty($this->page)){
			$this->page = 1;
		}
		
		if(isset($this->page) && $this->page > 0){
			$this->currentPage = $this->page;
			if($this->currentPage > $this->totalPage){
				$this->currentPage = 1;
			}
			$start = ($this->currentPage * $this->limit) - $this->limit;
		}else if($this->page > $this->totalPage){
			$start = $this->totalPage;
		}else{
			$start= 0;
		}
		
		return $start;
	}
	
	public function result(){
	    if( $this->modified ) {
            $this->pageData = [];
            for ($i = $this->startValue; $i < ($this->startValue + $this->limit); $i++) {
                if (!empty($this->data[$i])) {
                    $this->pageData[] = $this->data[$i];
                }
            }
        }else{
	        $this->pageData = $this->data;
        }
		return $this->pageData;
	}
	
	public function display(){
		if(isset($this->param[$this->path])) unset($this->param[$this->path]);
		if($this->param){
			$param = '?' . $this->param;
			if($this->path) $param .= '&' . $this->path . '=';
		}else{
			$param = '?' . $this->path . '=';
		}
		
		if($this->currentPage <= 0){
			$next = 2;
		}else{
			$next = $this->currentPage + 1;
		}
		
		$html = '';
		
		$prev = $this->currentPage - 1;
		$last = $this->totalPage;
		$stages = 2;
		
		if($last > 1){	
			if ($this->currentPage > 1){
				$html.= '<li class="page-item page-arrow"><a href="' . $param . '1" class="page-link"><i class="fa fa-angle-double-left"></i></a></li>';
			}else{
				$html.= '<li class="page-item page-arrow disabled"><a href="#" class="page-link"><i class="fa fa-angle-double-left"></i></a></li>';
			}
		
			if ($last < 7 + ($stages * 2)){	
				for ($i = 1; $i <= $last; $i++){
					if ($i == $this->currentPage){
						$html.= '<li class="page-item active"><a href="#" class="page-link"> ' . $i . ' </a></li>';
					}else{
						$html.= '<li class="page-item"><a href="' . $param . $i . '" class="page-link"> ' . $i . ' </a></li>';
					}					
				}
					
			}elseif($last > 5 + ($stages * 2)){
		
				if($this->currentPage < 1 + ($stages * 2)){
					for ($i = 1; $i < 4 + ($stages * 2); $i++){
						if ($i == $this->currentPage){
							$html.= '<li class="page-item active"><a href="#" class="page-link"> ' . $i . ' </a></li>';
						}else{
							$html.= '<li class="page-item"><a href="' . $param . $i . '" class="page-link"> ' . $i . ' </a></li>';
						}					
					}
					
					$html.= " <span class=\"page-item\">...</span> ";
					$html.= '<li data-lp="' . $this->totalPage . '" class="page-item"><a href="' . $param . $this->totalPage . '" class="page-link">' . $this->totalPage . '</a></li>';
					
				} elseif($last - ($stages * 2) > $this->currentPage && $this->currentPage > ($stages * 2)){
						
					$html.= '<li class="page-item"><a href="' . $param . '1" class="page-link">1</a></li>';
					$html.= " <span class=\"page-item\">...</span> ";
							
					for ($i = $this->currentPage - $stages; $i <= $this->currentPage + $stages; $i++){
						if ($i == $this->currentPage){
							$html.= '<li data-lp="' . $i . '" class="page-item active"><a href="#" class="page-link"> ' . $i . ' </a></li>';
						}else{
							$html.= '<li data-lp="' . $i . '" class="page-item"><a href="' . $param . $i . '" class="page-link"> ' . $i . ' </a></li>';
						}					
					}
					
					$html.= " <span class=\"page-item\">...</span> ";
					$html.= '<li class="page-item"><a href="' . $param . $this->totalPage . '" class="page-link">' . $this->totalPage . '</a></li>';
								
				}else{
							
					$html.= '<li data-lp="1" class="page-item"><a href="' . $param . '1" class="page-link">1</a></li>';
					$html.= " <span class=\"page-item\">...</span> ";
					
					for ($i = $last - (2 + ($stages * 2)); $i <= $last; $i++){
						if ($i == $this->currentPage){
							$html.= '<li data-lp="' . $i . '" class="page-item active"><a href="#" class="page-link"> ' . $i . ' </a></li>';
						}else{
							$html.= '<li data-lp="' . $i . '" class="page-item"><a href="' . $param . $i . '" class="page-link"> ' . $i . ' </a></li>';
						}					
					}
				}
			}
						
			if ($this->currentPage < $i - 1){ 
				$html.= '<li class="page-item page-arrow"><a href="' . $param . $this->totalPage . '" class="page-link" data-param="' . $this->param . '" data-next="' . $this->totalPage . '"><i class="fa fa-angle-double-right"></i></a></li>';
			}else{
				$html.= '<li class="page-item page-arrow disabled"><a href="#" class="page-link"><i class="fa fa-angle-double-right"></i></a></li>';
			}			
		}
		
		if($this->totalPage > 1) return $html;
	}
	
	public function display_ajax(){
		if(isset($this->param[$this->path])) unset($this->param[$this->path]);
		if($this->param){
			$param = '?' . http_build_query($this->param, '', '&');
			if($this->path) $param .= '&' . $this->path . '=';
		}else{
			$param = '?' . $this->path . '=';
		}
		
		if($this->currentPage <= 0){
			$next = 2;
		}else{
			$next = $this->currentPage + 1;
		}
		
		$html = '';
		
		$prev = $this->currentPage - 1;
		$last = $this->totalPage;
		$stages = 2;
		
		if($last > 1){	
			if ($this->currentPage > 1){
				$html.= '<li data-lp="1" class="prev page-item"><a href="#" data-param="' . $this->param . '" data-next="1" class="page-link page-products">«</a></li>';
			}else{
				$html.= '<li data-lp="1" class="prev page-item disabled"><a href="#" data-param="' . $this->param . '" data-next="1" class="page-link page-products">«</a></li>';
			}
		
			if ($last < 7 + ($stages * 2)){	
				for ($i = 1; $i <= $last; $i++){
					if ($i == $this->currentPage){
						$html.= '<li data-lp="' . $i . '" class="page-item active"><a href="#" class="page-link page-products" data-next="' . $i . '" data-param="' . $this->param . '"> ' . $i . ' </a></li>';
					}else{
						$html.= '<li data-lp="' . $i . '" class="page-item"><a href="#" class="page-link page-products" data-next="' . $i . '" data-param="' . $this->param . '"> ' . $i . ' </a></li>';
					}					
				}
					
			}elseif($last > 5 + ($stages * 2)){
		
				if($this->currentPage < 1 + ($stages * 2)){
					for ($i = 1; $i < 4 + ($stages * 2); $i++){
						if ($i == $this->currentPage){
							$html.= '<li data-lp="' . $i . '" class="page-item active"><a href="#" class="page-link page-products" data-next="' . $i . '" data-param="' . $this->param . '"> ' . $i . ' </a></li>';
						}else{
							$html.= '<li data-lp="' . $i . '" class="page-item"><a href="#" class="page-link page-products" data-next="' . $i . '" data-param="' . $this->param . '"> ' . $i . ' </a></li>';
						}					
					}
					
					$html.= " ... ";
					$html.= '<li data-lp="' . $this->totalPage . '" class="page-item"><a href="#" class="page-link page-products" data-next="' . $this->totalPage . '" data-param="' . $this->param . '">' . $this->totalPage . '</a></li>';
					
				} elseif($last - ($stages * 2) > $this->currentPage && $this->currentPage > ($stages * 2)){
						
					$html.= '<li data-lp="1" class="page-item"><a href="#" class="page-link page-products" data-next="1" data-param="' . $this->param . '">1</a></li>';
					$html.= " ... ";
							
					for ($i = $this->currentPage - $stages; $i <= $this->currentPage + $stages; $i++){
						if ($i == $this->currentPage){
							$html.= '<li data-lp="' . $i . '" class="page-item active"><a href="#" class="page-link page-products" data-next="' . $i . '" data-param="' . $this->param . '"> ' . $i . ' </a></li>';
						}else{
							$html.= '<li data-lp="' . $i . '" class="page-item"><a href="#" class="page-link page-products" data-next="' . $i . '" data-param="' . $this->param . '"> ' . $i . ' </a></li>';
						}					
					}
					
					$html.= " ... ";
					$html.= '<li data-lp="' . $this->totalPage . '" class="page-item"><a href="#" class="page-link page-products" data-next="' . $this->totalPage . '" data-param="' . $this->param . '">' . $this->totalPage . '</a></li>';
								
				}else{
							
					$html.= '<li data-lp="1" class="page-item"><a href="#" class="page-link page-products" data-next="1" data-param="' . $this->param . '">1</a></li>';
					$html.= " ... ";
					
					for ($i = $last - (2 + ($stages * 2)); $i <= $last; $i++){
						if ($i == $this->currentPage){
							$html.= '<li data-lp="' . $i . '" class="page-item active"><a href="#" class="page-link page-products" data-next="' . $i . '" data-param="' . $this->param . '"> ' . $i . ' </a></li>';
						}else{
							$html.= '<li data-lp="' . $i . '" class="page-item"><a href="#" class="page-link page-products" data-next="' . $i . '" data-param="' . $this->param . '"> ' . $i . ' </a></li>';
						}					
					}
				}
			}
						
			if ($this->currentPage < $i - 1){ 
				$html.= '<li data-lp="' . $this->totalPage . '" class="next page-item"><a href="#" class="page-link page-products" data-param="' . $this->param . '" data-next="' . $this->totalPage . '">»</a></li>';
			}else{
				$html.= '<li data-lp="' . $this->totalPage . '" class="next page-item disabled"><a href="#" class="page-link page-products" data-param="' . $this->param . '" data-next="' . $this->totalPage . '">»</a></li>';
			}			
		}
		
		if($this->totalPage > 1) return $html;
    }
}
?>