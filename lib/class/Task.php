<?php

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../helpers.php';
require_once __DIR__ . '/Milestone.php';

use Carbon\Carbon;

class Task
{
    protected $db;
    protected $user;
    protected $date;
    protected $activity;
    protected $site_host;
    protected $service_charge = 3;
    protected $service_percentage = 4.5;

    /**
     * @var $db db
     * @var $user user
     * @return Task
     */
    public static function init($db, $user){
        return new static($db, $user);
    }

    /**
     * @var $db db
     * @var $user user
     */
    public function __construct($db, $user){
        $this->db = $db;
        $this->site_host = (!empty($_SERVER['HTTPS']) ? 'https://' : 'http://') . ($_SERVER['HTTP_HOST'] ?? '');

        if( isset($_SESSION['member_id']) && !empty($_SESSION['member_id']) ){
            $_SESSION[WEBSITE_PREFIX.'USER_ID'] = $_SESSION['member_id'];
        }

        $this->user = $user;
        date_default_timezone_set('Asia/Kuala_Lumpur');

        if( !isset($_SESSION[WEBSITE_PREFIX.'LANGUAGE']) || $_SESSION[WEBSITE_PREFIX.'LANGUAGE'] == 'EN' ){
            $locale = ['English', 'en_GB'];
        }else{
            $locale = ['Malay', 'ms_MY'];
        }

        setLocale(LC_TIME, $locale[1]);
        Carbon::setLocale($locale[1]);
        $this->date = Carbon::today();
        $this->activity = new TaskActivity($this->db, $this->user);
    }

    private function is_logged_in(){
        if((!isset($this->user->info['email']) || empty($this->user->info['email'])) &&
            (!isset($this->user->info['status']) || $this->user->info['status'] !== '1')) {
            //halt(HTTP_UNAUTHORIZED, "This area is for logged in users!");
        }
        return true;
    }

    private function is_ajax(){
        return 'xmlhttprequest' === strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '' );
    }

    /**
     * @param $task
     * @param array $applicants_status
     * @return mixed
     */
    protected function update_task_status($task, $applicants_status = []){
        if ($this->date::parse($task['complete_by'])->lte( $this->date->copy()::now() ) && (!empty($applicants_status) && empty(array_filter($applicants_status, function ($status) { return in_array($status, ['in-progress', 'completed', 'rate']); })) || empty($applicants_status)) && empty($task['actions'])) {
            if ($this->set_closed($task['id'])) {
                $task['status'] = 'closed';
            }
        } elseif ($this->date::parse($task['complete_by'])->lte( $this->date->copy()::now() ) && !empty($applicants_status) && empty(array_filter($applicants_status, function ($status) { return in_array($status, ['in-progress', 'completed']); })) ) {
            if ($this->set_completed($task['id'])) {
                $task['status'] = 'completed';
            }
        }
        return $task;
    }

    private function cancel_task($task_id = null, $user_id = null){
        if( is_null($task_id) || is_null($user_id) ) return false;

        $this->db->table('tasks');
        $this->db->updateArray(['status' => 'cancelled']);
        $this->db->whereArray(['id' => $task_id, 'user_id' => $user_id]);
        $this->db->update();

        $task = $this->get_task_by_id($task_id);

        $this->activity->add( [
            'user_id' => 0,
            'task_id' => $task_id,
            'task_number' => $task['number'],
            'event' => 'cancelled',
            'message' => 'Cancelled automatically, no applicants and passed end date'
        ] );
    }
	
	public function settings(){
        if( ! isset($_SESSION['settings']) ) {
            $setting  = array();
            $settings = array();

            $this->db->query("SELECT `name`, `value` FROM settings");
            $settings = $this->db->getRowList();

            foreach ($settings as $keys) {
                $setting[$keys['name']] = $keys['value'];
            }

            if ($setting['date_format'] == 'custom') $setting['date_format'] = $setting['date_format_custom'];
            if ($setting['time_format'] == 'custom') $setting['time_format'] = $setting['time_format_custom'];
            if ($setting['session_timeout'] == 'custom') $setting['session_timeout'] = $setting['session_timeout_custom'];
            $_SESSION['settings'] = $setting;
        }else{
            $setting = $_SESSION['settings'];
        }
		return $setting;
	}

    public function create(){
		$redirect_to = option('site_uri') . url_for('/workspace/task-centre/posted/list');

        $this->is_logged_in();
        $completeness = (int)$this->user->info['type'] === 0 ? (int)$this->user->info['completion'] : 100;
        $updated = false;
        if(lemon_csrf_require_valid_token()) {
            $validation = validate($_POST, [
                'od_category'             => 'required',
                'od_subcategory'          => 'required',
                'od_subcategory_other'    => request('od_subcategory') && (request('od_subcategory') === 'others') ? 'required' : '',
                'od_tasktitle'            => 'required',
                'od_taskdesc'             => !request('draft') ? 'required' : '',
                'od_type'                 => !request('draft') ? 'required' : '',
                'od_hours.by-hour'        => request('od_type') && (request('od_type') === 'by-hour') && !request('draft') ? 'required' : '',
                'od_budget.by-hour'       => request('od_type') && (request('od_type') === 'by-hour') && !request('draft') ? 'required' : '',
                'od_budget.lump-sum'      => request('od_type') && (request('od_type') === 'lump-sum') && !request('draft') ? 'required' : '',
                'od_start_by.by-hour'     => request('od_type') && (request('od_type') === 'by-hour') && !request('draft') ? 'required' : '',
                'od_complete_by.by-hour'  => request('od_type') && (request('od_type') === 'by-hour') && !request('draft') ? 'required' : '',
                'od_start_by.lump-sum'    => request('od_type') && (request('od_type') === 'lump-sum') && !request('draft') ? 'required' : '',
                'od_complete_by.lump-sum' => request('od_type') && (request('od_type') === 'lump-sum') && !request('draft') ? 'required' : '',
            ],[
                'od_category.required'             => lang('category_is_required'), //Category is required!
                'od_subcategory.required'          => lang('sub_category_is_required'), //Sub Category is required!
                'od_subcategory_other.required'    => lang('sub_category_is_required'), //Sub Category is required!
                'od_tasktitle.required'            => lang('task_title_is_required'), //Task Title is required!
                'od_taskdesc.required'             => lang('task_description_is_required'), //Task Description is required!
                'od_type.required'                 => lang('task_nature_type_is_required'), //Task's Nature (Type) is required!
                'od_hours.by-hour.required'        => lang('number_of_hours_is_required'), //Number of Hours is required!
                'od_budget.by-hour.required'       => lang('budget_is_required'), //Budget is required!
                'od_budget.lump-sum.required'      => lang('budget_is_required'), //Budget is required!
                'od_start_by.by-hour.required'     => lang('start_date_is_required'), //Start Date is required!
                'od_complete_by.by-hour.required'  => lang('complete_by_date_is_required'), //Complete By Date is required!
                'od_start_by.lump-sum.required'    => lang('start_date_is_required'), //Start Date is required!
                'od_complete_by.lump-sum.required' => lang('complete_by_date_is_required'), //Complete By Date is required!
            ]);

            if(empty($validation['errors'])){
                $available_id = false;
                $new_task_id = guid();

                while( ! $available_id ){
                    if( $this->check_id_availability($new_task_id) ){
                        $available_id = true;
                    }else{
                        $new_task_id = guid();
                    }
                }

                $slug = $this->create_slug(request('od_tasktitle'), request('task_id'));

                $this->db->query("SELECT COUNT(id) as `count` FROM `tasks`");
                $all_count = (int)$this->db->getSingleRow()['count'];

                $start_date    = request('od_start_by') && request('od_type') ? str_replace('/', '-', sanitize(request('od_start_by')[request('od_type')])) : 'now';
                $complete_date = request('od_complete_by') && request('od_type') ? str_replace('/', '-', sanitize(request('od_complete_by')[request('od_type')])) : 'now';
                $start_date    = Carbon::parse($start_date)->toDateTimeString();
                $complete_date = Carbon::parse($complete_date)->toDateTimeString();

                $state_country = sanitize(request('od_state_country'));
                $state_country = str_replace('Johor Bahru', 'Johor', $state_country);
                $state_country = str_replace(['Wilayah Persekutuan Kuala Lumpur', 'Federal Territory of Kuala Lumpur'], 'Kuala Lumpur', $state_country);
                $state_country = str_replace(['Pulau Penang', 'Pulau Pinang', 'Pinang'], 'Penang', $state_country);

                $skills = request('skills');
                $skill_ids = [];
                if( $skills ) {
                    $parts     = explode(',', $skills);
                    $skill_ids = array_filter($parts, function ($skill) {
                                    return stripos($skill, ':') === false;
                                });
                    $new_parts = array_diff($parts, $skill_ids);
                    $new       = array_values(array_map(function ($skill) {
                                    list($key, $value) = explode(':', $skill, 2);
                                    return sanitize($value);
                                }, $new_parts));

                    if (!empty($new)) {
                        $new_ids = [];
                        $this->db->table('skills');
                        foreach ($new as $skill) {
                            $check_for_an_existing_skills = $this->db->query("SELECT * FROM `skills` WHERE `name` LIKE '{$skill}' LIMIT 1");
                            $row                          = $this->db->getSingleRow();
                            if (empty($row)) {
                                $this->db->insertArray([
                                    'name'   => $skill,
                                    'new'    => '1',
                                    'status' => '0'
                                ]);
                                $this->db->insert();
                                array_push($new_ids, $this->db->insertid());
                            } else {
                                array_push($new_ids, $row['id']);
                            }
                        }
                        $skill_ids = array_merge($skill_ids, $new_ids);
                    }
                }

                if(!request('task_id')){
                    $new_task = [
                        'id'             => $new_task_id,
                        'number'         => substr($new_task_id, -6),//(string)('T' . (20101 + $all_count)),
                        'user_id'        => $this->user->info['id'],
                        'category'       => (int)sanitize(request('od_category'), 'int'),
                        'sub_category'   => (int)sanitize(request('od_subcategory'), 'int'),// !== 0) ? sanitize(request('od_subcategory'), 'int') : sanitize(request('od_subcategory_other')),
                        'title'          => mysqli_real_escape_string($this->db->connection, request('od_tasktitle')),
                        'slug'           => $slug,
                        'description'    => sanitize(request('od_taskdesc')),
                        'type'           => request('od_type') ? sanitize(request('od_type')) : 'by-hour',
                        'state_country'  => $state_country,
                        'location'       => request('od_location') && request('od_location') === 'travel' ? mysqli_real_escape_string($this->db->connection, request('od_location_address')) : 'remotely',
                        'budget'         => request('od_budget') && request('od_type') ? (float)sanitize(request('od_budget')[request('od_type')], 'float') : 0,
                        'hours'          => request('od_type') === 'by-hour' ? sanitize(request('od_hours')[request('od_type')]) : null,
                        'skills'         => !empty($skill_ids) ? implode(',', $skill_ids) : '',
                        'start_by'       => $start_date,
                        'complete_by'    => $complete_date,
                        'urgent'         => request('od_urgent') && request('od_type') ? sanitize(request('od_urgent')[request('od_type')]) : 0,
                        'status'         => request('draft') || ($completeness < 100) ? 'draft' : 'published',
                    ];

                    $session_new_task = $new_task;
                    unset($session_new_task['id'], $session_new_task['number'], $session_new_task['slug']);

                    if( session('created_task') && session('created_task') === $session_new_task ){
                        unset($session_new_task);
                        flash('message', lang('invalid_action'));
                        flash('status', 'danger');
                        session('created_task', null, true);
                        redirect_to($redirect_to);
                    }

                    session('created_task', $session_new_task);
                    unset($session_new_task);

                    $this->db->table("tasks");
                    $this->db->insertArray($new_task);

                    $this->db->insert();
                    $this->db->query("SELECT * FROM `tasks` WHERE `id` = '{$new_task_id}' LIMIT 1");
                    $task = $this->db->getSingleRow();
                    $task_id = $task['id'];
                }else{
                    $task_id = sanitize(request('task_id'));
                    $user_id = $this->user->info['id'];
                    //$this->db->query("SELECT * FROM `tasks` WHERE `id` = '{$task_id}' AND `user_id` = '{$user_id}' AND `status` = 'draft' LIMIT 1");
                    $this->db->query("SELECT * FROM `tasks` WHERE `id` = '{$task_id}' AND `user_id` = '{$user_id}' LIMIT 1");
                    $task = $this->db->getSingleRow();
                    if(!empty($task)) {
                        $this->db->table("tasks");
                        $this->db->updateArray([
                            'category'       => (int)sanitize(request('od_category'), 'int'),
                            'sub_category'   => (int)sanitize(request('od_subcategory'), 'int'),
                            'title'          => mysqli_real_escape_string($this->db->connection, request('od_tasktitle')),
                            'slug'           => $slug,
                            'description'    => sanitize(request('od_taskdesc')),
                            'type'           => sanitize(request('od_type')),
                            'state_country'  => $state_country,
                            'location'       => sanitize(request('od_location')) === 'travel' ? mysqli_real_escape_string($this->db->connection, request('od_location_address')) : sanitize(request('od_location')),
                            'budget'         => (float)sanitize(request('od_budget')[request('od_type')], 'float'),
                            'hours'          => request('od_type') === 'by-hour' ? sanitize(request('od_hours')[request('od_type')]) : null,
                            'skills'         => !empty($skill_ids) ? implode(',', $skill_ids) : '',
                            'start_by'       => $start_date,
                            'complete_by'    => $complete_date,
                            'urgent'         => request('od_urgent') && request('od_type') ? sanitize(request('od_urgent')[request('od_type')]) : 0,
                            //'rejection_rate' => (int)sanitize(request('rejection_rate')[request('od_type')], 'int'),
                            'status'         => request('draft') || ($completeness < 100) ? 'draft' : 'published',
                        ]);

                        $this->db->whereArray([
                            'id'      => $task_id,
                            'user_id' => $user_id,
                        ]);

                        $this->db->update();
                        $updated = true;
                        $task = $this->get_task_by_id($task_id);
                    }else{
                        $task_id = null;
                        redirect_to($redirect_to);
                        die;
                    }
                }

                if( request('od_tags') && $task_id){
                    $all_tags = explode(',', request('od_tags'));
                    $this->db->table('task_tags');

                    $tags = $this->tags($task_id);

                    $current_tags = [];
                    if(!empty($tags)){
                        $tags = array_values($tags)[0];
                        foreach ( $tags as $cTag){
                            array_push($current_tags, $cTag['name']);
                        }
                        $all_tags = array_diff($all_tags, $current_tags);
                    }

                    $delete_tags = array_diff($current_tags, explode(',', request('od_tags')));
                    if(!empty($delete_tags)){
                        $length = count($delete_tags);
                        $query = "DELETE FROM `task_tags` WHERE `task_id` ='{$task_id}' AND (";
                        foreach ((array)$delete_tags as $key => $tag){
                            $query .= "`tag` LIKE '%{$tag}%'";
                            if($key+1 < $length){
                                $query .= " OR ";
                            }
                        }
                        $query .= ")";

                        $this->db->queryOrDie($query);
                    }

                    $this->db->table('task_tags');
                    foreach ((array)$all_tags as $tag) {
                        $tags = [
                            'id'  => guid(),
                            'task_id' => $task_id,
                            'tag' => mysqli_real_escape_string($this->db->connection, $tag)
                        ];
                        $this->db->insertArray($tags);
                        $this->db->insert();
                    }

                }

                if( request('od_qna') && $task_id){
                    $this->db->table('task_questions');
                    $all_questions = request('od_qna');

                    $questions = $this->questions($task_id);
                    $current_questions = [];

                    if(!empty($questions)){
                        $questions = array_values($questions)[0];
                        foreach ( $questions as $cQuestion){
                            array_push($current_questions, $cQuestion['name']);
                        }
                        $all_questions = array_diff($all_questions, $current_questions);
                    }

                    $delete_questions = array_diff($current_questions, request('od_qna'));
                    if(!empty($delete_questions)){
                        $delete_questions = array_values($delete_questions);
                        $length = count($delete_questions);
                        $query = "DELETE FROM `task_questions` WHERE `task_id` ='{$task_id}' AND (";
                        foreach ($delete_questions as $key => $question){
                            $query .= "`question` LIKE '%{$question}%'";
                            if($key+1 < $length){
                                $query .= " OR ";
                            }
                        }
                        $query .= ")";

                        $this->db->queryOrDie($query);
                    }

                    $this->db->table('task_questions');
                    foreach ((array)$all_questions as $question) {
                        if(strlen(trim($question)) === 0) continue;
                        $questions = [
                            'id'  => guid(),
                            'task_id' => $task_id,
                            'question' => mysqli_real_escape_string($this->db->connection, $question)
                        ];
                        $this->db->insertArray($questions);
                        $this->db->insert();
                    }
                }

                if( request('attachments') && $task_id){
                    $this->db->table('task_attachments');
                    foreach (request('attachments') as $id => $attachment) {
                        $attachments = [
                            'id'  => strtoupper($id),
                            'task_id' => $task_id,
                            'attachment' => $attachment,
                            'type' => strtolower(pathinfo($attachment, PATHINFO_EXTENSION))
                        ];
                        $this->db->insertArray($attachments);
                        $this->db->insert();
                    }
                }

                if($updated){
                    if( $task['status'] === 'published' )
                        $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'published'] );
                    else
                        $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'updated'] );

                    Event::trigger('task.updated', [$task]);
                }else{
                    if( $task['status'] === 'published' )
                        $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'published'] );
                    else
                        $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'created'] );

                    Event::trigger('task.created', [$task]);
                }

                if($updated) flash('message', lang('task_was_updated_succussfully'));
                else {
                        flash('message', lang('task_was_created_succussfully'));
                        (new Mail($this->db))->sendmail(
                            [
                                'email' => $this->user->info['email'],
                            ],
                            'task-publish',
                            [
                                'taskname'    => $new_task['title']
                            ]
                        );
                     }
                flash('status', 'success');
                redirect($redirect_to);
            }else{

                flash('message', implode("<br/>", $validation['errors']));
                flash('status', 'danger');
                redirect_to($redirect_to);
            }
        }else{
            flash('message', lang('invalid_action'));
            flash('status', 'danger');
            redirect_to($redirect_to);
        }
    }

    protected function check_id_availability($id){
        return (int) $this->db->getValue(" SELECT COUNT(`id`) as `count` FROM `tasks` WHERE `id` = '{$id}' ") === 0;
    }

    public function upload(){
        $this->is_logged_in();

        $allowed_files = explode(',', $this->settings()['task_file_extensions']);
        $user_id = $this->user->info['id'];
        if(lemon_csrf_require_valid_token()) {
            if( empty($_FILES) || !isset($_FILES['file']) )
            {
                echo lang('no_file_uploaded');
                exit;
            }
           
            $name     = $_FILES['file']['name'];
            $tmpName  = $_FILES['file']['tmp_name'];
            $type     = $_FILES['file']['type'];
            $error    = $_FILES['file']['error'];
            $size     = $_FILES['file']['size'];
            $ext      = strtolower(pathinfo($name, PATHINFO_EXTENSION));
            $fileName = preg_replace('#[\s]#i', '_', preg_replace('#[^a-z.0-9\-\s]#i', '_', $name));
            $status   = 'error';

            switch ($error) {
                case UPLOAD_ERR_OK:
                    $valid = true;
                    //validate file extensions
                    if ( $allowed_files && !in_array($ext, $allowed_files) ) {
                        $valid = false;
                        $message = lang('invalid_file_extension');
                    }
                    //validate file size
                    if ( $size/1024/1024 > $this->settings()['task_max_size'] ) {
                        $valid = false;
                        $message = sprintf(lang('sys_upload_max_size_error'), $this->settings()['task_max_size'] . 'MB');
                    }
                    //upload file
                    if ($valid) {
                        //create a user specific foler with user ID
                        if(!is_dir(UPLOAD_DIR . 'tasks' . DIRECTORY_SEPARATOR . $user_id)){
                            mkdir(UPLOAD_DIR . 'tasks' . DIRECTORY_SEPARATOR . $user_id, 0755, true);
                        }
                        if( file_exists(UPLOAD_DIR . 'tasks' . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR . $fileName) ) {
                            $ext = strrpos($fileName, '.');
                            $new_file_name = substr($fileName, 0, (strlen($fileName) - $ext) * -1) .'_'. substr(md5(time()), -4) . substr($fileName, (strlen($fileName) - $ext) * -1);
                            $targetPath = UPLOAD_DIR . 'tasks' . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR . $new_file_name;
                        }else{
                            $targetPath = UPLOAD_DIR . 'tasks' . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR . $fileName;
                        }
                        @move_uploaded_file($tmpName, $targetPath);
                        $message = "/files/tasks/{$user_id}/{$fileName}";
                        $status = 'success';
                    }
                    break;
                case UPLOAD_ERR_INI_SIZE:
                    $message = lang('file_size_is_exceeding_maximum_allowed_size');
                    break;
                case UPLOAD_ERR_PARTIAL:
                    $message = lang('uploaded_file_was_only_partially_uploaded');
                    break;
                case UPLOAD_ERR_NO_FILE:
                    $message = lang('no_file_uploaded');
                    break;
                case UPLOAD_ERR_NO_TMP_DIR:
                    $message = lang('failed_to_upload_file_please_try_again');//'Missing a temporary folder.';
                    break;
                case UPLOAD_ERR_CANT_WRITE:
                    $message = lang('failed_to_upload_file_please_try_again');//'Failed to write file to disk.';
                    break;
                default:
                    $message = lang('failed_to_upload_file_please_try_again');//'Unknown error';
                    break;
            }

            echo json_encode(['status' => $status, 'message' => $message]);

        }
    }

    public function apply(){
        $this->is_logged_in();

        if(lemon_csrf_require_valid_token() && (request('external') || (int)(new TaskCentre($this->db, $this->user))->user_completeness() === 100)) {
            if( request('task_id') && request('answer') ) {
                $this->db->table("answers");
                foreach (request('answer') as $key => $answer) {
                    $this->db->insertArray([
                        'user_id'     => $this->user->info['id'],
                        'task_id'     => sanitize(request('task_id')),
                        'question_id' => sanitize($key),
                        'answer'      => sanitize($answer),
                        'type'        => 'task',
                        'created_at'  => Carbon::now()->toDateTimeString()
                    ]);

                    $this->db->insert();
                }
            }
            if( request('task_id') ){
                $task_id = mysqli_real_escape_string($this->db->connection, request('task_id'));
                $user_id = $this->user->info['id'];
                $applied = $this->db->getValue("SELECT COUNT(`user_id`) AS `count` FROM `user_task` WHERE `task_id` = '{$task_id}' AND `user_id` = '{$user_id}' AND `type` = 'task'");
                if( !$applied ) {
                    $this->db->table("user_task");
                    $this->db->insertArray([
                        'user_id'    => $user_id,
                        'task_id'    => $task_id,
                        'type'       => 'task',
                        'created_at' => Carbon::now()->toDateTimeString()
                    ]);
                    $this->db->insert();
                }
            }
            if( request('external') )
                $task = $this->get_external_task_by_id(sanitize(request('task_id')));
            else
                $task = $this->get_task_by_id(sanitize(request('task_id')));

            $this->activity->add( ['task_id' => $task['task_id'], 'task_number' => $task['number'], 'event' => 'applied'] );
            Event::trigger('task.applied', [$task]);
            $poster = $task['user_id'] !== '0' ? $this->get_task_user($task['user_id']) : null;
            $seeker = (new TaskCentre($this->db, $this->user))->get_talent($this->user->info['id'], false);
            $photo = $this->db->getValue("SELECT `photo` FROM `members` WHERE `id` = '{$this->user->info['id']}' LIMIT 1");
            $seeker['photo'] = $photo;
            $mail = new Mail($this->db);
            //Send an email to task's poster (if any, cause aggregated tasks don't)
            if($poster) {
                $poster_email = $poster['email']; //isset($poster['company']) ? $poster['company']['email'] :
                if( ! $mail->isScheduled(['email' => $poster_email, 'slug' => $task['slug']], 'new-task-application', 'email') )
                    $mail->schedule(
                        [
                            'to' => [
                                'email' => $poster_email,
                                'name'  => isset($poster['company']) ? $poster['company']['name'] : "{$poster['firstname']} {$poster['lastname']}",
                            ],

                            'data' => [
                                'first_name'  => isset($poster['company']) ? $poster['company']['name'] : $poster['firstname'],
                                'poster_name' => !is_null($poster) ? ($poster['company']['name'] ?? "{$poster['firstname']} {$poster['lastname']}") : '',
                                'task'        => ['title' => $task['title'], 'slug' => $task['slug']],
                                'talent'      => ['name' => $seeker['name'], 'number' => $seeker['number'], 'photo' => $seeker['photo'], 'rating' => ['overall' => $seeker['rating']['overall']]]
                            ]
                        ],
                        'new-task-application',
                        'email'
                    );
            }
            // Send an email to seeker (whom applied for task)
            if( ! $mail->isScheduled(['email' => $seeker['email'], 'task' => $task['title']], 'you-have-applied-for-task', 'email') )
                $mail->schedule(
                    [
                        'to' => [
                            'email' => $seeker['email'],
                            'name'  => $seeker['name'],
                        ],

                        'data' => [
                            'poster'  => !is_null($poster) ? ($poster['company']['name'] ?? "{$poster['firstname']} {$poster['lastname']}") : '',
                            'task'    => $task['title'],
                            'subject' => "You have applied for {$task['title']}"
                        ]
                    ],
                    'you-have-applied-for-task',
                    'email'
                );

            if( ! $this->is_ajax() ) {
                flash('message', lang('applied_succussfully'));
                flash('status', 'success');
                redirect_to(option('site_uri') . url_for('/workspace', $this->date->year, $this->date->month . '#tcAll'));
            }
        }else {

            flash('message', lang('failed_to_apply_please_try_again'));
            flash('status', 'danger');
            redirect_to(request('url') ? request('url') : option('site_uri') . url_for('/search'));
        }
    }

    public function assign($user_id = null, $task_id = null){
        if($this->is_logged_in() && !is_null($user_id) && !is_null($task_id)){
            $poster_id = $this->user->info['id'];
            $this->db->query("SELECT `id` FROM `poster_seeker` 
                              WHERE `poster_id` = '{$poster_id}' AND `seeker_id` = '{$user_id}' AND `task_id` = '{$task_id}'
                              LIMIT 1");

            if( empty($this->db->getSingleRow()) ) {
                $pinned = $this->db->getValue("SELECT `pinned` FROM `user_task` WHERE `user_id` = '{$user_id}' AND `task_id` = '{$task_id}' AND `type` = 'task' LIMIT 1");
                $this->db->table("poster_seeker");
                $this->db->insertArray([
                    'poster_id' => $poster_id,
                    'seeker_id' => $user_id,
                    'task_id'   => $task_id,
                    'status'    => 'appointed',
                    'pinned'    => $pinned
                ]);

                $this->db->insert();
            }else{
                $this->db->table("poster_seeker");
                $this->db->updateArray([
                    'status'    => 'appointed'
                ]);
                $this->db->whereArray([
                    'poster_id' => $poster_id,
                    'seeker_id' => $user_id,
                    'task_id'   => $task_id,
                ]);

                $this->db->update();
            }

            $this->db->table("user_task");
            $this->db->updateArray([
                'status'    => 'appointed'
            ]);
            $this->db->whereArray([
                'user_id' => $user_id,
                'task_id' => $task_id,
                'type'    => 'task'
            ]);

            $this->db->update();

            if( empty( Milestone::get($this->db, ['task_id' => $task_id, 'by_id' => $poster_id, 'for_id' => $user_id]) ) ) {

                Milestone::save($this->db, [
                    'task_id'         => $task_id,
                    'by_id'           => $poster_id,
                    'for_id'          => $user_id,
                    'required_action' => 'appointed',
                ]);
            }
            $this->set_profile_viewed($user_id, $task_id);
            //$this->unfavourite_task_from_all($task_id, 'assigned');
            $task = $this->get_task_by_id($task_id);
            $task['assigned_to'] = $user_id;
            $this->activity->add( ['task_id' => $task['task_id'], 'task_number' => $task['number'], 'event' => 'assigned'] );
            Event::trigger('task.assigned', [$task]);
            $this->db->query("SELECT `firstname`, `email` FROM `members` WHERE `id` = '{$user_id}' LIMIT 1");
            $seeker = $this->db->getSingleRow();

            (new Mail($this->db))->schedule(
                [
                    'to' => [
                        'email' => $seeker['email'],
                        'name'  => $seeker['firstname']
                    ],

                    'data' => [
                        'poster'  => isset($this->user->info['company']) ? $this->user->info['company']['name'] : $this->user->info['name'],
                        'task'    => ['title' => $task['title'], 'slug' => $task['slug']],
                        'subject' => "You have been appointed for {$task['title']}"
                    ]
                ],
                'you-have-been-appointed',
                'email'
            );

            return true;
        }
        return false;
    }

    public function confirm($user_id = null, $task_id = null){
        if($this->is_logged_in() && !is_null($user_id) && !is_null($task_id)){
            $poster_id = $this->user->info['id'];
            $task = $this->get_task_by_id($task_id);

            $status = $this->date::parse($task['start_by'])->gt( $this->date->copy()::now() ) ? 'waiting' : 'in-progress';

            $this->db->query("SELECT `id` FROM `poster_seeker` 
                              WHERE `poster_id` = '{$poster_id}' AND `seeker_id` = '{$user_id}' AND `task_id` = '{$task_id}'
                              LIMIT 1");

            if( empty($this->db->getSingleRow()) ) {
                $this->db->table("poster_seeker");
                $this->db->insertArray([
                    'poster_id' => $poster_id,
                    'seeker_id' => $user_id,
                    'task_id'   => $task_id,
                    'status'    => $status
                ]);

                $this->db->insert();
            }else{
                $this->db->table("poster_seeker");
                $this->db->updateArray([
                    'status'    => $status
                ]);
                $this->db->whereArray([
                    'poster_id' => $poster_id,
                    'seeker_id' => $user_id,
                    'task_id'   => $task_id,
                ]);

                $this->db->update();
            }

            $this->db->table("user_task");
            $this->db->updateArray([
                'status'    => $status === 'waiting' ? 'appointed' : $status
            ]);
            $this->db->whereArray([
                'user_id' => $user_id,
                'task_id' => $task_id,
                'type'    => 'task',
            ]);

            $this->db->update();

            Milestone::update($this->db, ['performed' => '1'], [
                'task_id' => $task_id,
                'by_id' => $user_id,
                'for_id' => $poster_id
            ]);

            if( empty( Milestone::get($this->db, ['task_id' => $task_id, 'by_id' => $poster_id, 'for_id' => $user_id, 'required_action' => 'confirmed']) ) ) {
                Milestone::save($this->db, [
                    'task_id'         => $task_id,
                    'by_id'           => $poster_id,
                    'for_id'          => $user_id,
                    'required_action' => 'confirmed',
                ]);
            }

            //$this->unfavourite_task_from_all($task_id, 'assigned');

            $task['assigned_to'] = $user_id;
            $this->activity->add( ['task_id' => $task['task_id'], 'task_number' => $task['number'], 'event' => 'confirmed'] );
            Event::trigger('task.confirmed', [$task]);
            return true;
        }
        return false;
    }

    public function delete(){
        $redirect_to = option('site_uri') . url_for('/workspace/task-centre/posted/list');
        if(lemon_csrf_require_valid_token() && request('task_id')) {
            $user_id = $this->user->info['id'];
            $task_id = request('task_id');
            $task = $this->get_task_by_id($task_id);
            if( $task_id && $this->owner($user_id, $task_id) && $this->is_draft($task_id) ){
                $this->db->queryOrDie("DELETE FROM `tasks` WHERE `id` = '{$task_id}' AND `user_id` = '{$user_id}' AND `status` = 'draft' ");

                $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'deleted'] );
                Event::trigger('task.deleted', [$task]);
                flash('message', lang('task_was_deleted_succussfully'));
                flash('status', 'success');
            }

        }else{
            flash('message', lang('failed_to_delete_task_please_try_again'));
            flash('status', 'danger');
        }

        redirect_to($redirect_to);
    }

    public function cancel(){
        $redirect_to = option('site_uri') . url_for('/workspace/task-centre/posted/list');
        if(lemon_csrf_require_valid_token() && request('task_id')) {
            $user_id = $this->user->info['id'];
            $task_id = request('task_id');

            if( $task_id && $this->owner($user_id, $task_id) && $this->is_published($task_id) ){
                $this->db->table('tasks');
                $this->db->updateArray(['status' => 'cancelled']);
                $this->db->whereArray(['id' => $task_id, 'user_id' => $user_id]);
                $this->db->update();

                $task = $this->get_task_by_id($task_id);

                $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'cancelled'] );
                $this->unfavourite_task_from_all($task_id, 'canceled');
                Event::trigger('task.cancelled', [$task]);
                flash('message', lang('task_was_cancelled_succussfully'));
                flash('status', 'success');
            }

        }else{
            flash('message', lang('failed_to_cancel_task_please_try_again'));
            flash('status', 'danger');
        }

        redirect_to($redirect_to);
    }

    public function complete(){
        $redirect_to = $_SERVER['HTTP_REFERER'];//option('site_uri') . url_for('/workspace/billings/earning');
        if(lemon_csrf_require_valid_token() && request('task_id') && request('user_id')) {
            $seeker_id = sanitize(request('user_id'));
            $task_id = sanitize(request('task_id'));
            $current_user_id = $this->user->info['id'];

            $task = $this->get_task_by_id($task_id);
            $poster_id = $task['user_id'];
            $user = $this->owner($current_user_id, $task_id) ? 'poster' : 'seeker';

            if( $task && $this->is_status($task_id, $seeker_id, $poster_id, 'in-progress', $user) ){

                if($user === 'poster') {
                    $this->db->table('user_task');
                    $this->db->updateArray(['status' => 'completed']);
                    $this->db->whereArray([
                        'task_id'   => $task_id,
                        'user_id' => $seeker_id,
                        'type' => 'task'
                    ]);
                    $this->db->update();

                    if( Milestone::get($this->db, ['task_id' => $task_id, 'by_id' => $seeker_id, 'for_id' => $poster_id]) ){
                        /*Milestone::remove($this->db, ['task_id' => $task_id, 'by_id' => $poster_id, 'for_id' => $seeker_id]);
                        Milestone::remove($this->db, ['task_id' => $task_id, 'by_id' => $seeker_id, 'for_id' => $poster_id]);*/
                        Milestone::update($this->db,
                            [
                                'performed' => '1'
                            ], [
                                'task_id' => $task_id,
                                'by_id' => $seeker_id,
                                'for_id' => $poster_id
                            ]
                        );
                    }

                    Milestone::save($this->db, [
                        'task_id' => $task_id,
                        'by_id' => $poster_id,
                        'for_id' => $seeker_id,
                        'required_action' => 'completed',
                    ]);

                }else{
                    $this->db->table('poster_seeker');
                    $this->db->updateArray(['status' => 'completed']);
                    $this->db->whereArray([
                        'task_id'   => $task_id,
                        'seeker_id' => $seeker_id,
                        'poster_id' => $poster_id
                    ]);
                    $this->db->update();

                    if( Milestone::get($this->db, ['task_id' => $task_id, 'by_id' => $poster_id, 'for_id' => $seeker_id]) ){
                        /*Milestone::remove($this->db, ['task_id' => $task_id, 'by_id' => $seeker_id, 'for_id' => $poster_id]);
                        Milestone::remove($this->db, ['task_id' => $task_id, 'by_id' => $poster_id, 'for_id' => $seeker_id]);*/
                        Milestone::update($this->db,
                            [
                                'performed' => '1'
                            ], [
                                'task_id' => $task_id,
                                'by_id' => $poster_id,
                                'for_id' => $seeker_id
                            ]
                        );
                    }

                    Milestone::save($this->db, [
                        'task_id' => $task_id,
                        'by_id' => $seeker_id,
                        'for_id' => $poster_id,
                        'required_action' => 'completed',
                    ]);

                    $poster = $this->user->info( $poster_id );
                    $mail = new Mail($this->db);
                    // Send email to seeker
                    $mail->schedule(
                        [
                            'to'   =>
                                [
                                    'name'  => $this->user->info['name'],
                                    'email' => $this->user->info['email']
                                ],
                            'data' =>
                                [
                                    'poster'  => isset($poster['company']) ? $poster['company']['name'] : $poster['name'],
                                    'subject' => "{$task['number']} - You are marking task as COMPLETED"
                                ]
                        ],
                        'seeker-marking-task-completed',
                        'email'
                    );

                    // Send email to poster
                    $mail->schedule(
                        [
                            'to'   =>
                                [
                                    'name'  => isset($poster['company']) ? $poster['company']['name'] : $poster['name'],
                                    'email' => $poster['email']
                                ],
                            'data' =>
                                [
                                    'seeker'  => $this->user->info['name'],
                                    'task'    => str_replace('"', "'", $task['title']),
                                    'subject' => "{$task['number']} - {$this->user->info['name']} is marking task as COMPLETED"
                                ]
                        ],
                        'seeker-marking-task-completed',
                        'email'
                    );
                }

                $task['assigned_to'] = $seeker_id;
                $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'marked completed'] );
                Event::trigger('task.marked.completed', [$task]);
                //$this->unfavourite_task_from_all($task_id, 'completed');
                flash('message', lang('task_was_marked_as_completed_successfully'));
                flash('status', 'success');
            }

        }else{
            flash('message', lang('failed_to_mark_task_as_completed_please_try_again'));
            flash('status', 'danger');
        }

        redirect_to($redirect_to);
    }

    public function dispute(){
        // check if 'yes' was selected, then accept it
        if( request('dispute') && request('dispute') === 'no' ){
            $this->accept();
        }

        $redirect_to = option('site_uri') . url_for('workspace/task-centre/applied/list');//url_for('/workspace/billings/spent');
        if(lemon_csrf_require_valid_token() && request('task_id') && request('seeker_id')) {

            $task_id   = sanitize(request('task_id'));
            $seeker_id = sanitize(request('seeker_id'));
            $poster_id = $this->user->info['id'];
            $task      = $this->get_task_by_id($task_id);

            if( $task && $this->owner($poster_id, $task_id) ) {
                $redirect_to = option('site_uri') . url_for('workspace/task-centre/posted/list') . '#' . strtolower($task_id);

                $valid_poster_status   = $this->is_status($task_id, $seeker_id, $poster_id, 'in-progress') || $this->is_status($task_id, $seeker_id, $poster_id, 'completed');
                $valid_seeker_status   = $this->is_status($task_id, $seeker_id, $poster_id, 'in-progress', 'seeker') || $this->is_status($task_id, $seeker_id, $poster_id, 'completed', 'seeker');
                $task_reached_end_date = true;//!$this->date::parse($task['complete_by'])->isFuture();

                if ( $valid_poster_status && $valid_seeker_status && $task_reached_end_date ) {

                    $amount = (int)$task['budget'];

                    if( $percentage = request('percentage') ){
                        $percentage = (int)$percentage;
                        if( $percentage > 0 && $percentage < 100 ){
                            $amount *= $percentage/100;
                        }
                    }

                    $this->db->table('poster_seeker');
                    $this->db->updateArray(['status' => 'completed']);
                    $this->db->whereArray([
                        'task_id'   => $task_id,
                        'seeker_id' => $seeker_id,
                        'poster_id' => $poster_id
                    ]);
                    $this->db->update();

                    $this->db->table('user_task');
                    $this->db->updateArray(['status' => 'disputed']);
                    $this->db->whereArray([
                        'task_id' => $task_id,
                        'user_id' => $seeker_id,
                        'type'    => 'task'
                    ]);
                    $this->db->update();

                    $payment = $this->get_latest_payment($task_id, $poster_id, $seeker_id);
                    $seeker = $this->user->info($seeker_id);

                    $this->db->table('payment_release');
                    $this->db->insertArray([
                        'order_id'   => $payment['id'],
                        'poster_id'  => $poster_id,
                        'seeker_id'  => $seeker_id,
                        'task_id'    => $task_id,
                        'type'       => 'payment',
                        'name'       => $seeker['name'],
                        'email'      => $seeker['email'],
                        'phone'      => $seeker['mobile_number'],
                        'amount'     => $amount,
                        'percentage' => $percentage
                    ]);
                    $this->db->insert();

                    if( $percentage ){
                        $this->db->table('payment_release');
                        $this->db->insertArray([
                            'order_id' => $payment['id'],
                            'poster_id' => $poster_id,
                            'seeker_id' => $seeker_id,
                            'task_id' => $task_id,
                            'type' => 'refund',
                            'reason' => sanitizeItem(request('reason')),
                            'name' => isset($this->user->info['company']) ? $this->user->info['company']['name'] : $this->user->info['name'],
                            'email' => isset($this->user->info['company']) ? $this->user->info['company']['email'] : $this->user->info['email'],
                            'phone' => isset($this->user->info['company']) ? $this->user->info['company']['company_phone'] : $this->user->info['mobile_number'],
                            'amount' => ((int)$task['budget'] - $amount),
                            'percentage' => (100 - $percentage)
                        ]);
                        $this->db->insert();
                    }

                    Milestone::save($this->db, [
                        'task_id' => $task_id,
                        'by_id' => $poster_id,
                        'for_id' => $seeker_id,
                        'required_action' => 'disputed'
                    ]);

                    $task['assigned_to'] = $seeker_id;
                    $task['rejection_rate'] = $percentage;
                    $this->activity->add(['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'disputed']);
                    Event::trigger('task.disputed', [$task]);

                    flash('message', lang('task_was_marked_as_disputed_successfully'));
                    flash('status', 'success');
                }
            }elseif( $task ){

                $poster_id = $task['user_id'];
                $redirect_to .= '#' . strtolower($task_id);

                if( $this->get_seeker_task_status($task_id, $seeker_id) === 'disputed' ){

                    Milestone::update($this->db, ['performed' => '1'], [
                        'task_id' => $task_id,
                        'by_id'   => $poster_id,
                        'for_id'  => $seeker_id
                    ]);

                    if( !is_null( request('decline') ) ){
                        $this->db->table('poster_seeker');
                        $this->db->updateArray(['status' => 'in-progress']);
                        $this->db->whereArray([
                            'task_id'   => $task_id,
                            'poster_id' => $poster_id,
                            'seeker_id' => $seeker_id
                        ]);
                        $this->db->update();

                        $this->db->table('user_task');
                        $this->db->updateArray(['status' => 'in-progress']);
                        $this->db->whereArray([
                            'task_id' => $task_id,
                            'user_id' => $seeker_id,
                            'type'    => 'task',
                        ]);
                        $this->db->update();

                        /*Milestone::save($this->db, [
                            'task_id' => $task_id,
                            'by_id' => $seeker_id,
                            'for_id' => $poster_id,
                            'required_action' => 'declined dispute'
                        ]);*/

                        $this->db->table('payment_release');
                        $this->db->whereArray([
                            'poster_id' => $poster_id,
                            'seeker_id' => $seeker_id,
                            'task_id'   => $task_id
                        ]);
                        $this->db->delete();

                        flash('message', 'Task dispute was declined');
                        flash('status', 'success');

                    }elseif( !is_null( request('accept') ) ){

                        $this->db->table('poster_seeker');
                        $this->db->updateArray(['status' => 'rate']); //disputed
                        $this->db->whereArray([
                            'task_id'   => $task_id,
                            'poster_id' => $poster_id,
                            'seeker_id' => $seeker_id
                        ]);
                        $this->db->update();

                        $this->db->table('user_task');
                        $this->db->updateArray(['status' => 'disputed']);
                        $this->db->whereArray([
                            'task_id' => $task_id,
                            'user_id' => $seeker_id,
                            'type'    => 'task',
                        ]);
                        $this->db->update();

                        Milestone::save($this->db, [
                            'task_id'         => $task_id,
                            'by_id'           => $seeker_id,
                            'for_id'          => $poster_id,
                            'required_action' => 'rate'
                        ]);

                        Milestone::save($this->db, [
                            'task_id'         => $task_id,
                            'by_id'           => $poster_id,
                            'for_id'          => $seeker_id,
                            'required_action' => 'rate'
                        ]);

                        flash('message', 'Task dispute was accepted');
                        flash('status', 'success');
                    }

                }


            }else{
                flash('message', lang('task_not_found'));
                flash('status', 'danger');
            }

        }else{
            flash('message', lang('failed_to_mark_task_as_disputed_please_try_again'));
            flash('status', 'danger');
        }

        redirect_to($redirect_to);
    }

    public function reject_dispute(){
        $redirect_to = option('site_uri') . url_for('/workspace/billings/spent');
        if(lemon_csrf_require_valid_token() && request('task_id') && request('user_id')) {

            $task_id = sanitize(request('task_id'));
            $seeker_id = sanitize(request('user_id'));

            $task = $this->get_task_by_id($task_id);
            $user = $this->owner($current_user_id, $task_id) ? 'poster' : 'seeker';

            if( $task && $this->is_status($task_id, $seeker_id, $task['user_id'], 'completed', $user) ){

                    $poster_id = $this->user->info['id'];
                    $this->db->table('poster_seeker');
                    $this->db->updateArray(['status' => 'in-progress']);
                    $this->db->whereArray([
                        'task_id'   => $task_id,
                        'seeker_id' => $seeker_id,
                        'poster_id' => $poster_id
                    ]);
                    $this->db->update();

                    $this->db->table('user_task');
                    $this->db->updateArray(['status' => 'in-progress']);
                    $this->db->whereArray([
                        'task_id'   => $task_id,
                        'user_id' => $seeker_id,
                        'type' => 'task'
                    ]);
                    $this->db->update();

                $task['assigned_to'] = $seeker_id;
                $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'disputed'] );
                Event::trigger('task.dispute.declined', [$task]);

                flash('message', lang('Task dispute was declined'));
                flash('status', 'success');
            }

        }else{
            flash('message', lang('Failed to mark task as disputed, please try again'));
            flash('status', 'danger');
        }

        redirect_to($redirect_to);
    }

    public function accept(){
        $redirect_to = $_SERVER['HTTP_REFERER'];//option('site_uri') . url_for('/workspace/billings/spent');
        if(lemon_csrf_require_valid_token() && request('task_id') && (request('user_id') || request('seeker_id'))) {
            $poster_id = $this->user->info['id'];
            $task_id = sanitize(request('task_id'));
            $seeker_id = sanitize(request('user_id')) ?? sanitizeItem(request('seeker_id'));

            $task = $this->get_task_by_id($task_id);
            $user = $this->owner($current_user_id, $task_id) ? 'poster' : 'seeker';
            $task_poster_status = $this->get_poster_task_status($task_id, $seeker_id, $poster_id);

            if( $task && $this->owner($poster_id, $task_id) && ($this->is_status($task_id, $seeker_id, $poster_id, 'completed') || $this->is_status($task_id, $seeker_id, $poster_id, 'in-progress')) ){
                $this->db->table('poster_seeker');
                $this->db->updateArray(['status' => 'rate']);
                $this->db->whereArray(['task_id' => $task_id, 'seeker_id' => $seeker_id, 'poster_id' => $poster_id]);
                $this->db->update();

                $this->db->table('user_task');
                $this->db->updateArray(['status' => 'rate']);
                $this->db->whereArray(['task_id' => $task_id, 'user_id' => $seeker_id, 'type' => 'task']);
                $this->db->update();

                $payment = $this->get_latest_payment($task_id, $poster_id, $seeker_id);
                $seeker = $this->user->info($seeker_id);

                $this->db->table('payment_release');
                $this->db->insertArray([
                    'order_id'   => $payment['id'],
                    'poster_id'  => $poster_id,
                    'seeker_id'  => $seeker_id,
                    'task_id'    => $task_id,
                    'type'       => 'payment',
                    'name'       => $seeker['name'],
                    'email'      => $seeker['email'],
                    'phone'      => $seeker['mobile_number'],
                    'amount'     => (int)$task['budget'],
                    'percentage' => '100'
                ]);
                $this->db->insert();

                Milestone::update($this->db, ['performed' => '1'],
                    [
                        'task_id' => $task_id,
                        'by_id' => $seeker_id,
                        'for_id' => $poster_id
                    ]
                );

                Milestone::save($this->db, [
                    'task_id' => $task_id,
                    'by_id' => $poster_id,
                    'for_id' => $seeker_id,
                    'required_action' => 'accepted',
                ]);

                $task['assigned_to'] = $seeker_id;
                $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'accepted'] );
                $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'completed', 'created_at' => Carbon::now()->add(\Carbon\CarbonInterval::second(1))] );
                Event::trigger('task.accepted', [$task]);

                $mail = new Mail($this->db);
                $poster_name = isset($this->user->info['company']) ? $this->user->info['company']['name'] : $this->user->info['name'];

                // Send email to poster
                $mail->schedule(
                    [
                        'to'   => [
                            'name'  => $poster_name,
                            'email' => $this->user->info['email']
                        ],
                        'data' => [
                            'seeker'  => $seeker['name'],
                            'task'    => str_replace('"', "'", $task['title']),
                            'amount'  => (new cms($this->db, $this->user))->price($task['budget']),
                            'subject' => "{$task['number']} - You are marking task as COMPLETED"
                        ]
                    ],
                    'poster-marking-task-completed',
                    'email'
                );

                if( $task_poster_status === 'completed' ) {
                    // Send email to seeker
                    $mail->schedule(
                        [
                            'to'   =>
                                [
                                    'name'  => $seeker['name'],
                                    'email' => $seeker['email']
                                ],
                            'data' =>
                                [
                                    'poster'  => $poster_name,
                                    'task'    => str_replace('"', "'", $task['title']),
                                    'amount'  => (new cms($this->db, $this->user))->price($task['budget']),
                                    'subject' => "{$task['number']} - Congratulations! The task you marked as completed, has been accepted by the Poster"
                                ]
                        ],
                        'poster-accepted-task',
                        'email'
                    );
                }else{
                    $mail->schedule(
                        [
                            'to'   => [
                                'name'  => $seeker['name'],
                                'email' => $seeker['email']
                            ],
                            'data' => [
                                'poster'  => $poster_name,
                                'task'    => str_replace('"', "'", $task['title']),
                                'amount'  => (new cms($this->db, $this->user))->price($task['budget']),
                                'subject' => "{$task['number']} - Congratulations! {$poster_name} has marked the task as Completed"
                            ]
                        ],
                        'poster-marked-task-completed',
                        'email'
                    );
                }

                flash('message', lang('task_was_accepted_and_marked_as_completed'));
                flash('status', 'success');
            }

        }else{
            flash('message', lang('failed_to_accept_task_please_try_again'));
            flash('status', 'danger');
        }

        redirect_to($redirect_to);
    }

    public function accept_offer($task_id = null, $user_id = null){
        if(isset($task_id, $user_id)) {
            $task_id = sanitize($task_id);
            $seeker_id = sanitize($user_id);

            $task = $this->get_task_by_id($task_id);
            $poster_id = $task['user_id'];

            if( $task && $this->is_status($task_id, $seeker_id, $poster_id, 'appointed') ){
                /*$this->db->table('poster_seeker');
                $this->db->updateArray(['status' => 'appointed']);
                $this->db->whereArray(['task_id' => $task_id, 'seeker_id' => $seeker_id, 'poster_id' => $poster_id]);
                $this->db->update();*/
                if( Milestone::get($this->db, ['task_id' => $task_id, 'by_id' => $seeker_id, 'for_id' => $poster_id]) ){
                    Milestone::update($this->db,['performed' => '1'], ['task_id' => $task_id, 'by_id' => $seeker_id, 'for_id' => $poster_id]);
                }

                Milestone::update($this->db,['performed' => '1'], ['task_id' => $task_id, 'by_id' => $poster_id, 'for_id' => $seeker_id]);

                Milestone::save($this->db, [
                    'task_id' => $task_id,
                    'by_id' => $seeker_id,
                    'for_id' => $poster_id,
                    'required_action' => 'confirmed',
                ]);

                $this->db->table('user_task');
                $this->db->updateArray(['status' => 'appointed']);
                $this->db->whereArray(['task_id' => $task_id, 'user_id' => $seeker_id, 'type' => 'task']);
                $this->db->update();

                $this->db->table('poster_seeker');
                $this->db->updateArray(['status' => 'confirmed']);
                $this->db->whereArray(['task_id' => $task_id, 'seeker_id' => $seeker_id, 'poster_id' => $poster_id]);
                $this->db->update();

                $task['assigned_to'] = $seeker_id;
                $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'accepted'] );
                //$this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'completed', 'created_at' => Carbon::now()->add(\Carbon\CarbonInterval::second(1))] );
                Event::trigger('task.offer.accepted', [$task]);

                $task = $this->set_service_charge($task);
                $seeker = $this->db->getValue("SELECT CONCAT(`firstname`, ' ', `lastname`) AS `name` FROM `members` WHERE `id` = '{$seeker_id}' LIMIT 1");
                $poster = $this->user->info($poster_id);

                (new Mail($this->db))->schedule(
                    [
                        'to' => [
                            'email' => $poster['email'],
                            'name'  => isset($poster['company']) ? $poster['company']['name'] : $poster['name']
                        ],

                        'data' => [
                            'seeker'  => $seeker,
                            'task'    => ['budget' => $task['budget'], 'service_charge' => $task['service_charge']],
                            'subject' => "{$seeker} has accepted your task {$task['title']}"
                        ]
                    ],
                    'seeker-has-accepted-task',
                    'email'
                );

                $result = ['message' => lang('You have accepted task appointed to you'), 'status' => 'success'];

            }else{
                $result = ['message' => lang('Failed to accept task appointed to you, please try again'), 'status' => 'danger'];
            }

        }else{
            $result = ['message' => lang('Failed to accept task appointed to you, please try again'), 'status' => 'danger'];
        }

        return $result;
    }

    public function retract_offer($task_id = null, $user_id = null){
        if(isset($task_id, $user_id)) {
            $task_id = sanitize($task_id);
            $seeker_id = sanitize($user_id, 'int');

            $task = $this->get_task_by_id($task_id);
            $poster_id = $task['user_id'];

            if( $task && ($this->is_status($task_id, $seeker_id, $poster_id, 'appointed') || $this->is_status($task_id, $seeker_id, $poster_id, 'confirmed')) ){

                $this->db->table('poster_seeker');
                $this->db->updateArray(['status' => 'cancelled']);
                $this->db->whereArray(['task_id' => $task_id, 'seeker_id' => $seeker_id, 'poster_id' => $poster_id]);
                $this->db->update();

                $this->db->table('user_task');
                $this->db->updateArray(['status' => 'cancelled']);
                $this->db->whereArray(['task_id' => $task_id, 'user_id' => $seeker_id, 'type' => 'task']);
                $this->db->update();

                Milestone::update($this->db, ['performed' => '1'], [
                    'task_id' => $task_id,
                    'by_id' => $seeker_id,
                    'for_id' => $poster_id,
                ]);

                Milestone::save($this->db, [
                    'task_id' => $task_id,
                    'by_id' => $poster_id,
                    'for_id' => $seeker_id,
                    'required_action' => 'cancelled',
                ]);

                $task['assigned_to'] = $seeker_id;
                $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'retracted'] );
                Event::trigger('task.offer.retracted', [$task]);

                $result = ['message' => lang('You have retracted task offer'), 'status' => 'success'];

            }else{
                $result = ['message' => lang('Failed to retract task offer, please try again'), 'status' => 'danger'];
            }

        }else{
            $result = ['message' => lang('Failed to retract task offer, please try again'), 'status' => 'danger'];
        }

        return $result;
    }

    public function retract_application($task_id = null, $user_id = null){
        if(isset($task_id, $user_id)) {
            $task_id = sanitize($task_id);
            $seeker_id = sanitize($user_id, 'int');

            $task = $this->get_task_by_id($task_id);
            $poster_id = $task['user_id'];

            if( $task && $this->is_status($task_id, $seeker_id, $poster_id, 'applied', 'seeker') ){

                $this->db->table('poster_seeker');
                $this->db->whereArray(['task_id' => $task_id, 'seeker_id' => $seeker_id, 'poster_id' => $poster_id]);
                $this->db->delete();

                $this->db->table('user_task');
                $this->db->whereArray(['task_id' => $task_id, 'user_id' => $seeker_id, 'type' => 'task']);
                $this->db->delete();

                $task['assigned_to'] = $seeker_id;
                $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'retracted'] );
                Event::trigger('task.application.retracted', [$task]);

                $result = ['message' => lang('You have retracted task application'), 'status' => 'success'];

            }else{
                $result = ['message' => lang('Failed to retract task application, please try again'), 'status' => 'danger'];
            }

        }else{
            $result = ['message' => lang('Failed to retract task application, please try again'), 'status' => 'danger'];
        }

        return $result;
    }

    public function reject(){
        $redirect_to = $_SERVER['HTTP_REFERER'];//option('site_uri') . url_for('/workspace/billings/spent');
        if(lemon_csrf_require_valid_token() && request('task_id') && request('user_id')) {
            $poster_id = $this->user->info['id'];
            $task_id = sanitize(request('task_id'));
            $seeker_id = sanitize(request('user_id'));
            $task = $this->get_task_by_id($task_id);

            if( $task && $this->owner($poster_id, $task_id) && $this->is_status($task_id, $seeker_id, $poster_id, 'completed') ){
                //(new PaymentRequest($this->db, $this->user))->reject($task_id);
                $this->db->table('poster_seeker');
                $this->db->updateArray(['status' => 'in-progress']);
                $this->db->whereArray(['task_id' => $task_id, 'seeker_id' => $seeker_id, 'poster_id' => $poster_id]);
                $this->db->update();

                $this->db->table('user_task');
                $this->db->updateArray(['status' => 'in-progress']);
                $this->db->whereArray(['task_id' => $task_id, 'user_id' => $seeker_id, 'type' => 'task']);
                $this->db->update();

                if( Milestone::get($this->db, ['task_id' => $task_id, 'by_id' => $seeker_id, 'for_id' => $poster_id]) ){
                    /*Milestone::remove($this->db, ['task_id' => $task_id, 'by_id' => $poster_id, 'for_id' => $seeker_id]);
                    Milestone::remove($this->db, ['task_id' => $task_id, 'by_id' => $seeker_id, 'for_id' => $poster_id]);*/
                    Milestone::update($this->db,
                        [
                            'performed' => '1'
                        ], [
                            'task_id' => $task_id,
                            'by_id' => $seeker_id,
                            'for_id' => $poster_id
                        ]
                    );
                }

                Milestone::save($this->db, [
                    'task_id' => $task_id,
                    'by_id' => $poster_id,
                    'for_id' => $seeker_id,
                    'required_action' => 'rejected',
                ]);

                $task['assigned_to'] = $seeker_id;
                $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'rejected'] );
                Event::trigger('task.rejected', [$task]);
                flash('message', lang('task_was_rejected_successfully'));
                flash('status', 'success');
            }

        }else{
            flash('message', lang('failed_to_reject_task_please_try_again'));
            flash('status', 'danger');
        }

        redirect_to($redirect_to);
    }

    public function reject_offer($task_id = null, $user_id = null){
        if(isset($task_id, $user_id)) {
            $task_id = sanitize($task_id);
            $seeker_id = sanitize($user_id);
            $task = $this->get_task_by_id($task_id);
            $poster_id = $task['user_id'];

            if( $task && $this->is_status($task_id, $seeker_id, $poster_id, 'appointed', 'seeker') ){

                Milestone::save($this->db, [
                    'task_id' => $task_id,
                    'by_id' => $seeker_id,
                    'for_id' => $poster_id,
                    'required_action' => 'declined',
                    'performed' => '1',
                ]);

                $this->db->table('poster_seeker');
                $this->db->updateArray(['status' => 'declined']);
                $this->db->whereArray(['task_id' => $task_id, 'seeker_id' => $seeker_id, 'poster_id' => $poster_id]);
                $this->db->update();

                $this->db->table('user_task');
                $this->db->updateArray(['status' => 'declined']);
                $this->db->whereArray(['task_id' => $task_id, 'user_id' => $seeker_id, 'type' => 'task']);
                $this->db->update();

                $task['assigned_to'] = $seeker_id;
                $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'rejected'] );
                Event::trigger('task.offer.rejected', [$task]);

                $result = ['message' => lang('Task was declined'), 'status' => 'success'];
            }

        }else{
            $result = ['message' => lang('Failed to declined task, please try again'), 'status' => 'danger'];
        }

        return $result;
    }

    public function close(){
        $redirect_to = option('site_uri') . url_for('/workspace/billings/spent');
        if(lemon_csrf_require_valid_token() && request('task_id') && request('user_id')) {
            $poster_id = $this->user->info['id'];
            $task_id = sanitize(request('task_id'));
            $seeker_id = sanitize(request('user_id'));

            $task = $this->get_task_by_id($task_id);

            if( $task && $this->owner($poster_id, $task_id) && $this->is_status($task_id, $seeker_id, $poster_id, 'completed') ){
                $this->db->table('poster_seeker');
                $this->db->updateArray(['status' => 'closed']);
                $this->db->whereArray(['task_id' => $task_id, 'seeker_id' => $seeker_id, 'poster_id' => $poster_id]);
                $this->db->update();

                /*$this->db->table('user_task');
                $this->db->updateArray(['status' => 'closed']);
                $this->db->whereArray(['task_id' => $task_id, 'user_id' => $seeker_id, 'type' => 'task']);
                $this->db->update();*/

                $task['assigned_to'] = $seeker_id;
                $this->activity->add( ['task_id' => $task_id, 'task_number' => $task['number'], 'event' => 'closed'] );
                Event::trigger('task.fund.released', [$task]);
                //$this->unfavourite_task_from_all($task_id, 'closed');
                flash('message', lang('task_was_marked_as_closed_successfully'));
                flash('status', 'success');
            }

        }else{
            flash('message', lang('failed_to_mark_task_as_closed_please_try_again'));
            flash('status', 'danger');
        }

        redirect_to($redirect_to);
    }

    public function favourite(){
        $this->is_logged_in();
        if(lemon_csrf_require_valid_token()) {
            if( request('task_id') ){

                if( request('external') )
                    $task = $this->get_external_task_by_id(sanitize(request('task_id')));
                else
                    $task = $this->get_task_by_id(sanitize(request('task_id')));

                $this->db->table("favourites");
                if( request('favourited') && request('favourited') === "true" && $this->is_favourited( sanitize(request('task_id')) ) ){
                    $this->db->whereArray([
                        'user_id' => $this->user->info['id'],
                        'task_id' => sanitize(request('task_id')),
                        'type'    => 'task',
                    ]);
                    $this->db->delete();
                    $this->activity->add( ['task_id' => $task['task_id'], 'task_number' => $task['number'], 'event' => 'unfavourite'] );
                }else if(!$this->is_favourited( sanitize(request('task_id')) )){
                    $this->db->insertArray([
                        'user_id'    => $this->user->info['id'],
                        'task_id'    => sanitize(request('task_id')),
                        'type'       => 'task',
                        'created_at' => Carbon::now()->toDateTimeString()
                    ]);
                    $this->db->insert();
                    $this->activity->add( ['task_id' => $task['task_id'], 'task_number' => $task['number'], 'event' => 'favourite'] );
                }
            }

            if( $this->is_ajax() ){
                if (request('favourited') === "true") $message = lang('task_was_removed_from_favourite_successfully');
                else $message = lang('task_was_added_to_favourite_successfully');

                return json(['message' => $message, 'status' => 'success', 'action' => (request('favourited') === "true") ? 'removed' : 'added', 'title' => lang('success')]);
            }else {
                if (request('favourited') === "true") flash('message', lang('task_was_removed_from_favourite_successfully'));
                else flash('message', lang('task_was_added_to_favourite_successfully'));
                flash('status', 'success');

                if (request('url') && strpos(request('url'), 'http') === false) {
                    if( stripos(request('url'), '?s=external') ){
                        $external = '?s=external';
                        $url = substr(request('url'), 0, strlen(request('url')) - 11);
                    }else{
                        $external = '';
                        $url = request('url');
                    }
                    $url = ltrim($url, option('base_uri'));
                    $url = request('url') ? option('site_uri') . url_for($url) . $external : option('site_uri') . url_for('/workspace/search');
                }else {
                    $url = request('url');
                }

                redirect_to(urldecode($url));
            }

        }

        flash('message', lang('failed_to_favourite_please_try_again'));
        flash('status', 'danger');

        redirect(option('site_uri') . url_for('/workspace/search'));
    }

    public function remove_expired_favourite(){
        $this->is_logged_in();
        if(lemon_csrf_require_valid_token()) {
            $user_id = $this->user->info['id'];

            $this->db->query("SELECT DISTINCT `task_id`
                          FROM `favourites` 
                          WHERE `user_id` = '{$user_id}' AND `type` = 'task' 
                          AND (
                                `task_id` IN( SELECT `id` FROM `tasks` WHERE `status` NOT IN('published', 'in-progress') )
                                OR `task_id` IN( SELECT `id` FROM `external_tasks`  WHERE `status` NOT IN('published', 'in-progress') )
                          )");

            $inactive_tasks_favourites = $this->db->getRowList();
            $ids = array_column($inactive_tasks_favourites, 'task_id');

            $ids = implode("','", $ids);

            $this->db->queryOrDie("DELETE FROM `favourites` WHERE `user_id` = '{$user_id}' AND `task_id` IN('{$ids}') AND `type` = 'task'");
        }
        redirect_to(option('site_uri') . url_for('workspace/my-favourite'));
    }

    public function unfavourite_task_from_all($task_id = null, $reason = ''){
        $this->is_logged_in();
        if(!is_null($task_id)) {
            $task = $this->get_task_by_id(sanitize(request('task_id')));
            $this->db->table("favourites");
            $this->db->whereArray([
                'task_id' => sanitize($task_id),
                'type'    => 'task',
            ]);
            $this->db->delete();
            $this->activity->add([
                'user_id' => 0,
                'task_id' => $task['id'],
                'task_number' => $task['number'],
                'event' => 'unfavourite',
                'message' => "Unfavourited automatically, because task was {$reason}"
            ]);
        }
    }

    public function payment(){
        $this->is_logged_in();
        //if(lemon_csrf_require_valid_token()) {
        if( request('payment_id') ){
            $this->db->query("SELECT * FROM task_payment WHERE status = '1' AND id = " . $this->db->escape(request('payment_id')));
            $payment = $this->db->getSingleRow();
            $task = $this->get_task_by_id($task_id);

            if($task){
                if( $payment ){
                    $user_id   = $payment['assigned_to'];
                    $task_id   = $payment['task_id'];

                    $task['assigned_to'] = $user_id;
                    $this->activity->add(['task_id' => $task['task_id'], 'task_number' => $task['number'], 'event' => 'payment made']);
                    Event::trigger('task.payment', [$task]);
                    flash('message', lang('task_payment_has_been_completed_successfully'));
                    flash('status', 'success');

                    // confirm assigning the task to selected candidate
                    if( ! $this->confirm($user_id, $task_id) ){
                        flash('message', lang('failed_to_assign_task_please_try_again'));
                        flash('status', 'danger');
                        //$this->db->queryOrDie("UPDATE `tasks` SET `status` = 'in-progress' WHERE `id` = '{$task['task_id']}' AND `user_id` = '{$task['user_id']}' LIMIT 1");
                    }
                }else{
                    flash('message', lang('failed_to_make_payment_please_try_again'));
                    flash('status', 'danger');
                }

            }else{
                flash('message', lang('task_not_found'));
                flash('status', 'danger');
            }
        }else{
            flash('message', lang('failed_to_make_payment_please_try_again'));
            flash('status', 'danger');
        }

            /*$redirect = request('url') ? $this->site_host . ROOTPATH . request('url') : $this->site_host . ROOTPATH . 'workspace/' . $this->date->year . '/' . $this->date->month . '#tcInProgressMonth';
            header('Location: ' . $redirect);
            exit();*/
        //}

        /*flash('message', 'Failed to make payment, please try again');
        flash('status', 'danger');*/

        redirect_to(option('site_uri') . url_for('workspace/' . $this->date->year . '/' . $this->date->month));

        //redirect_to(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : option('site_uri') . url_for('/workspace', $this->date->year, $this->date->month));
    }

    public function paymentReturn(){
        $payment_id = !empty($_REQUEST['order_id']) ? $_REQUEST['order_id'] : 0;
        $this->db->queryOrDie("INSERT INTO senangpay_callback VALUES ('', 'return', '" . $payment_id . "', " . $this->db->escape(json_encode($_REQUEST)) . ", " . $this->db->escape(json_encode($_SERVER)) . ", NOW())");

        if(isset($_REQUEST['status_id']) && isset($_REQUEST['order_id']) && isset($_REQUEST['msg']) && isset($_REQUEST['transaction_id']) && isset($_REQUEST['hash'])){
            $this->db->query("SELECT * FROM task_payment WHERE id = " . $this->db->escape($payment_id));
            $task_payment = $this->db->getSingleRow();

            $user_id = !empty($task_payment['assigned_to']) ? $task_payment['assigned_to'] : 0;
            $task_id = !empty($task_payment['task_id']) ? $task_payment['task_id'] : 0;

            $hashed_string = hash_hmac('sha256', SENANGPAY['secret_key'].urldecode($_REQUEST['status_id']).urldecode($_REQUEST['order_id']).urldecode($_REQUEST['transaction_id']).urldecode($_REQUEST['msg']), SENANGPAY['secret_key']);
            $payment_message = str_replace('_', ' ', $_REQUEST['msg']);
            if($hashed_string == urldecode($_REQUEST['hash'])){

                $this->update_payment_transaction($payment_id, $payment_message);

                if(urldecode($_REQUEST['status_id']) == '1'){
                    // echo 'Payment was successful with message: '.urldecode($_GET['msg']);
                    $this->db->queryOrDie("UPDATE task_payment SET status = '1' WHERE id = " . $this->db->escape($payment_id));

                    $task = $this->get_task_by_id($task_id);
                    if( $task ){
                        // record into activity
                        $this->activity->add(['task_id' => $task['task_id'], 'task_number' => $task['number'], 'event' => 'payment made']);
                        $task['assigned_to'] = $user_id;
                        Event::trigger('task.payment', [$task]);

                        $poster = $this->user->info($task['user_id']);
                        $seeker = $this->user->info($user_id);

                        // Send an email to seeker (whom payment was made to)
                        (new Mail($this->db))->schedule(
                            [
                                'to'   => [
                                    'email' => $seeker['email'],
                                    'name'  => $seeker['name'],
                                ],
                                'data' => [
                                    'poster'  => !is_null($poster) ? ($poster['company']['name'] ?? $poster['name']) : '',
                                    'task'    => $task['title'],
                                    'taskID'  => $task['number'],
                                    'subject' => "{$task['number']} - The Task Poster has paid the full amount into escrow!"
                                ]
                            ],
                            'seeker-escrow-notification',
                            'email'
                        );

                        // confirm assigning the task to selected candidate
                        if(! $this->confirm($user_id, $task_id) ){
                            //$this->db->queryOrDie("UPDATE `tasks` SET `status` = 'in-progress' WHERE `id` = '{$task['task_id']}' AND `user_id` = '{$task['user_id']}' LIMIT 1");
                            flash('message', lang('failed_to_assign_task_please_try_again'));
                            flash('status', 'danger');
                        }

                    }else{
                        flash('message', lang('task_not_found'));
                        flash('status', 'danger');
                    }

                    //flash('message', 'Payment Success');
                    //flash('status', 'success');
                    $task_id = strtolower($task_id);
                    flash('msg', $payment_message);
                    flash('message', lang('task_payment_was_successful_and_was_assigned'));
                    flash('status', 'success');
                    flash('payment_id', $payment_id);
                    redirect_to(option('site_uri') . url_for('workspace/task-centre/posted/list') . "#{$task_id}");
                    /*header('Location: ' . $this->site_host . ROOTPATH . 'workspace/task/assign?user_id=' . $user_id . '&task_id=' . $task_id );
                    exit();*/

                }else{

                    //flash('message', 'Payment has failed - ' . str_replace('_', ' ', $_GET['msg']));
                    //flash('status', 'danger');
                    $task_id = strtolower($task_id);
                    flash('msg', $payment_message);
                    //flash('message', $payment_message);
                    session('payment_status', $_REQUEST['status_id']);
                    session('payment_task_id', $task_id);
                    session('payment_seeker_id', $user_id);
                    flash('payment_id', $payment_id);
                    //flash('status', 'danger');
                    redirect_to(option('site_uri') . url_for('workspace/task-centre/posted/list') . "#{$task_id}");
                    /*header('Location: ' . $this->site_host . ROOTPATH . 'workspace/task/assign?user_id=' . $user_id . '&task_id=' . $task_id );
                    exit();*/
                }
            }else{
                $task_id = strtolower($task_id);
                flash('message', lang('failed_to_make_payment_please_try_again'));
                flash('status', 'danger');
                redirect_to(option('site_uri') . url_for('workspace/task-centre/posted/list') . "#{$task_id}");
                /*header('Location: ' . $this->site_host . ROOTPATH . 'workspace/task/assign?user_id=' . $user_id . '&task_id=' . $task_id );
                exit();*/
            }
        }

        flash('message', lang('invalid_action'));
        flash('status', 'danger');
        redirect_to(option('site_uri') . url_for('workspace/task-centre/posted/list') . "#{$task_id}");
        /*header('Location: ' . $this->site_host . ROOTPATH . 'workspace/task/assign?user_id=' . $user_id . '&task_id=' . $task_id );
        exit();*/
    }

    /**
     * @param $payment_id
     */
    protected function update_payment_transaction($payment_id = null, $message = ''){
        if( is_null($payment_id) ) return;

        $order_hash = hash_hmac('sha256', SENANGPAY['merchant_id'] . SENANGPAY['secret_key'] . $payment_id, SENANGPAY['secret_key']);
        $order_url = SENANGPAY['order_status_url'] . "&order_id={$payment_id}&hash={$order_hash}";

        $content = file_get_contents($order_url);

        if( $content !== false ){
            $content = json_decode($content, true);
            $this->db->table('task_payment');
            foreach($content['data'] as $trasaction){
                $trasaction['payment_info']['transaction_date'] = date('Y-m-d H:i:s', $trasaction['payment_info']['transaction_date']);
                $this->db->updateArray([
                    'transaction_reference' => $trasaction['payment_info']['transaction_reference'],
                    'transaction_date' => $trasaction['payment_info']['transaction_date'],
                    'payment_mode' => $trasaction['payment_info']['payment_mode'],
                    'message' => $message
                ]);
                $this->db->whereArray([
                    'id' => $payment_id
                ]);
                $this->db->update();
            }
        }
    }

    public function paymentCallback(){
        $payment_id = !empty($_REQUEST['order_id']) ? $_REQUEST['order_id'] : 0;
        $this->db->queryOrDie("INSERT INTO senangpay_callback VALUES ('', 'callback', '" . $payment_id . "', " . $this->db->escape(json_encode($_REQUEST)) . ", " . $this->db->escape(json_encode($_SERVER)) . ", NOW())");

        if(isset($_REQUEST['status_id']) && isset($_REQUEST['order_id']) && isset($_REQUEST['msg']) && isset($_REQUEST['transaction_id']) && isset($_REQUEST['hash'])){
            $hashed_string = hash_hmac('sha256', SENANGPAY['secret_key'].urldecode($_REQUEST['status_id']).urldecode($_REQUEST['order_id']).urldecode($_REQUEST['transaction_id']).urldecode($_REQUEST['msg']), SENANGPAY['secret_key']);
            
            if($hashed_string == urldecode($_REQUEST['hash'])){
                if(urldecode($_REQUEST['status_id']) == '1'){
                    $this->db->queryOrDie("UPDATE task_payment SET status = '1' WHERE id = " . $this->db->escape($payment_id));

                    sleep(1);

                    $this->db->query("SELECT * FROM task_payment WHERE status = '1' AND id = " . $this->db->escape($payment_id));
                    $payment = $this->db->getSingleRow();

                    if($payment){
                        $user_id   = $payment['assigned_to'];
                        $task_id   = $payment['task_id'];
                        $task = $this->get_task_by_id($task_id);
                        if( $task ){
                            // the process to make payment
                            $this->activity->add(['task_id' => $task['task_id'], 'task_number' => $task['number'], 'event' => 'payment made']);
                            $task['assigned_to'] = $user_id;
                            Event::trigger('task.payment', [$task]);

                            // assigning the task to selected candidate
                            if($this->assign($user_id, $task_id)){
                                //$this->db->queryOrDie("UPDATE `tasks` SET `status` = 'in-progress' WHERE `id` = '{$task['task_id']}' AND `user_id` = '{$task['user_id']}' LIMIT 1");
                                
                                echo 'OK';
                                die();
                                
                            }else{
                                echo lang('failed_to_assign_task_please_try_again');
                                die();
                            }
                        }else{
                            echo lang('failed_to_make_payment_please_try_again');
                            die();
                        }
                        
                    }else{
                        echo lang('failed_to_make_payment_please_try_again');
                        die();
                    }

                   echo lang('task_payment_has_been_completed_successfully'); // 'Payment Success'
                   die();

                }else{
                    echo lang('failed_to_make_payment_please_try_again');//'Payment failed - ' . str_replace('_', ' ', $_GET['msg']);
                    die();
                }
            }else{
                echo lang('failed_to_make_payment_please_try_again');//'Payment failed - Hashed value is not correct';
                die();
            }
        }

        echo lang('invalid_action');//'Invalid action.';
    }

    public function request_payment(){
        $redirect_to = option('site_uri') . url_for('/workspace/billings/earning');
        $this->is_logged_in();
        if(lemon_csrf_require_valid_token()) {
            if( request('task_id') ){
                $task = $this->get_task_by_id(sanitize(request('task_id')));
                if( $task ){
                    $payment_request = new PaymentRequest($this->db, $this->user);
                    if( $payment_request->make($task['task_id']) ){
                        //$this->db->queryOrDie("UPDATE `tasks` SET `status` = 'in-progress' WHERE `id` = '{$task['task_id']}' AND `user_id` = '{$task['user_id']}' LIMIT 1");
                        $this->activity->add(['task_id' => $task['task_id'], 'task_number' => $task['number'], 'event' => 'payment request']);
                        Event::trigger('task.payment.request', [$task]);
                        flash('message', lang('payment_request_has_been_completed_successfully'));
                        flash('status', 'success');
                    }else{
                        flash('message', lang('failed_to_request_payment_please_try_again'));
                        flash('status', 'danger');
                        $redirect_to = $_SERVER['HTTP_REFERER'];
                    }
                }else{
                    flash('message', lang('failed_to_request_payment_please_try_again'));
                    flash('status', 'danger');
                    $redirect_to = $_SERVER['HTTP_REFERER'];
                }
            }else{
                flash('message', lang('failed_to_request_payment_please_try_again'));
                flash('status', 'danger');
                $redirect_to = $_SERVER['HTTP_REFERER'];
            }

            redirect($redirect_to);
        }

        flash('message', lang('failed_to_request_payment_please_try_again'));
        flash('status', 'danger');

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function tags($task_id = null){
        $this->is_logged_in();
        if( null === $task_id ) return [];
        $this->db->query("SELECT * FROM `task_tags` WHERE `task_id` = '{$task_id}'");
        $results = $this->db->getRowList();
        $tags = [];
        if($results && !empty($results)) {
            foreach ($results as $tag) {
                $tags[$task_id][] = [
                    'id'   => $tag['id'],
                    'name' => $tag['tag']
                ];
            }
        }
        return $tags;
    }

    public function questions($task_id = null){
        $this->is_logged_in();
        if( null === $task_id ) return [];
        $this->db->query("SELECT * FROM `task_questions` WHERE `task_id` = '{$task_id}'");
        $results = $this->db->getRowList();
        $questions = [];
        if($results && !empty($results)) {
            foreach ($results as $question) {
                $questions[$task_id][] = [
                    'id'   => $question['id'],
                    'name' => $question['question']
                ];
            }
        }
        return $questions;
    }

    public function attachments($task_id = null){
        $this->is_logged_in();
        if( null === $task_id ) return [];
        $this->db->query("SELECT * FROM `task_attachments` WHERE `task_id` = '{$task_id}'");
        $results = $this->db->getRowList();
        $attachments = [];
        if($results && !empty($results)) {
            foreach ($results as $attachment) {
                $attachments[] = [
                    'id'   => $attachment['id'],
                    'name' => $attachment['attachment']
                ];
            }
        }
        return $attachments;
    }

    public function get_attachment($attachment_id = null)
    {
        $this->is_logged_in();
        if( is_null($attachment_id) ) return null;
        $this->db->query("SELECT `attachment`, `type` FROM `task_attachments` WHERE `id` = '{$attachment_id}' LIMIT 1");
        return $this->db->getSingleRow();
    }

    public function get_task_attachment($task_id = null, $attachment_id = null)
    {
        $this->is_logged_in();
        if( is_null($attachment_id) && is_null($task_id) ) return null;
        $this->db->query("SELECT `attachment`, `type` FROM `task_attachments` WHERE `id` = '{$attachment_id}' AND `task_id` = '{$task_id}' LIMIT 1");
        return $this->db->getSingleRow();
    }

    public function get_attachment_by_file($task = null, $file = null)
    {
        $this->is_logged_in();
        if( is_null($task) && is_null($file) ) return null;
        $this->db->query("SELECT DISTINCT 
                            `task_attachments`.`attachment`,
                            `task_attachments`.`type`
                          FROM
                            `tasks`
                          INNER JOIN `task_attachments` ON (`tasks`.`id` = `task_attachments`.`task_id`)
                          WHERE
                            `tasks`.`slug` = '{$task}' AND 
                            `task_attachments`.`attachment` LIKE '%{$file}%'
                          LIMIT 1");
        return $this->db->getSingleRow();
    }

    public function tasks($user_id = null, $offset = 0){
        $this->is_logged_in();
        if($offset < 0) $offset = 0;
        if(null === $user_id) $user_id = $this->user->info['id'];
        $user = $this->get_current_user($user_id);

        if( session('posted_active_filtered') || session('posted_inactive_filtered') || session('applied_active_filtered') || session('applied_inactive_filtered') ){
            if( session('current_request_filter') === 'posted_active' || !$this->is_ajax() ) {
                if (session('posted_active_filter') && session('posted_active_filter') !== 'all') {
                    $filter    = session('posted_active_filter');
                    $order     = session('posted_active_order');
                    $all_tasks_total = $this->get_tasks_total($user_id, true, $filter);
                    $all_tasks = $this->get_tasks($user_id, $offset, 10, true, $filter, $order);
                } elseif (!$this->is_ajax() || session('posted_active_filter') === 'all') {
                    $order     = session('posted_active_order');
                    $all_tasks_total = $this->get_tasks_total($user_id, true, null);
                    $all_tasks = $this->get_tasks($user_id, $offset, 10, true, null, $order);
                }
            }

            if ( session('current_request_filter') === 'posted_inactive' || !$this->is_ajax() ) {
                if (session('posted_inactive_filter') && session('posted_inactive_filter') !== 'all') {
                    $filter             = session('posted_inactive_filter');
                    $order              = session('posted_inactive_order');
                    $all_inactive_tasks_total = $this->get_tasks_total($user_id, false, $filter);
                    $all_inactive_tasks = $this->get_tasks($user_id, $offset, 10, false, $filter, $order);
                } elseif (!$this->is_ajax() || session('posted_inactive_filter') === 'all') {
                    $order              = session('posted_inactive_order');
                    $all_inactive_tasks_total = $this->get_tasks_total($user_id, false, null);
                    $all_inactive_tasks = $this->get_tasks($user_id, $offset, 10, false, null, $order);
                }
            }

            $all_tasks = array_merge($all_tasks ?? [], $all_inactive_tasks ?? []);

            if( session('current_request_filter') === 'applied_active' || !$this->is_ajax() ) {
                if (session('applied_active_filter') && session('applied_active_filter') !== 'all') {
                    $filter        = session('applied_active_filter');
                    $order         = session('applied_active_order');
                    $tasks_applied_total = $this->get_applied_tasks_total($user_id, true, $filter);
                    $tasks_applied = $this->get_applied_tasks($user_id, $offset, 10, true, $filter, $order);
                } elseif (!$this->is_ajax() || session('applied_active_filter') === 'all') {
                    $order         = session('applied_active_order');
                    $tasks_applied_total = $this->get_applied_tasks_total($user_id, true, null);
                    $tasks_applied = $this->get_applied_tasks($user_id, $offset, 10, true, null, $order);
                }
            }

            if( session('current_request_filter') === 'applied_inactive' || !$this->is_ajax() ) {
                if (session('applied_inactive_filter') && session('applied_inactive_filter') !== 'all') {
                    $filter                 = session('applied_inactive_filter');
                    $order                  = session('applied_inactive_order');
                    $tasks_inactive_applied_total = $this->get_applied_tasks_total($user_id, false, $filter);
                    $tasks_inactive_applied = $this->get_applied_tasks($user_id, $offset, 10, false, $filter, $order);
                } elseif (!$this->is_ajax() || session('applied_inactive_filter') === 'all') {
                    $order                  = session('applied_inactive_order');
                    $tasks_inactive_applied_total = $this->get_applied_tasks_total($user_id, false, null);
                    $tasks_inactive_applied = $this->get_applied_tasks($user_id, $offset, 10, false, null, $order);
                }
            }

            $tasks_applied = array_merge($tasks_applied ?? [], $tasks_inactive_applied ?? []);

        }else{
            $all_tasks_total     = $this->get_tasks_total($user_id, true);
            $all_tasks     = $this->get_tasks($user_id, $offset, 10, true);
            $all_inactive_tasks_total = $this->get_tasks_total($user_id, false);
            $all_inactive_tasks = $this->get_tasks($user_id, $offset, 10, false);
            $all_tasks = array_merge($all_tasks, $all_inactive_tasks);
            $tasks_applied_total = $this->get_applied_tasks_total($user_id, true);
            $tasks_applied = $this->get_applied_tasks($user_id, $offset, 10, true);
            $tasks_inactive_applied_total = $this->get_applied_tasks_total($user_id, false);
            $tasks_inactive_applied = $this->get_applied_tasks($user_id, $offset, 10, false);
            $tasks_applied = array_merge($tasks_applied, $tasks_inactive_applied);
        }

        $task_centre   = new TaskCentre($this->db, $this->user);
        $rating_centre = new RatingCentre($this->db, $this->user);
        $tags = $questions = $attachments = [];
        $tasks_notifications = false;
        $reviews = (new RatingCentre($this->db, $this->user))->my_written_reviews();
        $reviews = array_merge($reviews['as_poster'], $reviews['as_talent']);
        $all_tasks_ids = array_column(array_merge($all_tasks, $tasks_applied), 'id');
        $actions = $this->get_tasks_milestone_action($all_tasks_ids);
        $payments = $this->get_tasks_payment($all_tasks_ids);
        $comments = array_column($this->get_tasks_comment($all_tasks_ids), 'post_id', 'id');
        $new_comments = array_column($this->get_tasks_new_comments($all_tasks_ids), 'post_id', 'id');
        $all_tasks_status = array_column($all_tasks, 'status', 'id');

        foreach($all_tasks as &$task){

            // Set task to be 'in-progress' status if it's published and reached start date
            if( $this->is_published($task['id']) && $this->date::parse($task['start_by'])->lte( $this->date->copy()::now() ) ){
                if( !empty($this->get_applicants([$task['id']])) ) {
                    if ($this->set_in_progress($task['id'])) {
                        $task['status'] = 'in-progress';
                    }
                }else{
                    $this->set_draft($task['id']);
                    $task['status'] = 'draft';
                }
            }

            $task['comments_count'] = count( array_filter($comments, function($comment) use($task){
                return $task['id'] === $comment;
            }) );

            $task['unanswered_comments_count'] = count( array_filter($new_comments, function($comment) use($task){
                return $task['id'] === $comment;
            }) );

            $picked_applicants = $this->get_task_picked_applicants($user_id, $task['id']);

            $selected_applicants = [];
            $applicants_status = [];

            foreach ($picked_applicants as $applicant){

                if( $this->is_status($task['id'], $applicant['seeker_id'], $task['user_id'], 'waiting') && $this->is_task_inProgress($task['id']) && $applicant['task_id'] === $task['id'] ){
                    if( $this->set_poster_seeker_in_progress($task['user_id'], $applicant['seeker_id'], $task['id']) ){
                        $applicant['status'] = 'in-progress';
                    }
                }

                // if poster has rated the task and poster task status is as 'rate', then update it to be 'closed'
                if( $applicant['status'] === 'rate' ) {
                    if ($rating_centre->has_rated_task($task['id'], $applicant['seeker_id'])) {
                        $this->db->table('poster_seeker');
                        $this->db->updateArray(['status' => 'closed']);
                        $this->db->whereArray(['task_id' => $task['id'], 'poster_id' => $task['user_id'], 'seeker_id' => $applicant['seeker_id']]);
                        $this->db->update();

                        Milestone::update($this->db, ['performed' => '1'],
                        [
                            'task_id' => $task['id'],
                            'for_id' => $task['user_id'],
                            'by_id' => $applicant['seeker_id']
                        ]);

                        $applicant['status'] = 'closed';
                    }
                }

                if( $this->date::parse($task['complete_by'])->lte( $this->date->copy()::now() ) && in_array($applicant['status'], ['appointed', 'confirmed']) ){

                    $this->db->table('poster_seeker');
                    $this->db->updateArray(['status' => 'declined']);
                    $this->db->whereArray(['task_id' => $applicant['task_id'], 'poster_id' => $applicant['poster_id'], 'seeker_id' => $applicant['seeker_id']]);
                    $this->db->update();

                    $this->db->table('user_task');
                    $this->db->updateArray(['status' => 'declined']);
                    $this->db->whereArray(['task_id' => $applicant['task_id'], 'type' => 'task', 'user_id' => $applicant['seeker_id']]);
                    $this->db->update();

                    Milestone::update($this->db, ['performed' => '1'],
                        [
                            'task_id' => $applicant['task_id'],
                            'for_id' => $applicant['poster_id'],
                            'by_id' => $applicant['seeker_id']
                        ]);

                    Milestone::update($this->db, ['performed' => '1'],
                        [
                            'task_id' => $applicant['task_id'],
                            'for_id' => $applicant['seeker_id'],
                            'by_id' => $applicant['poster_id']
                        ]);

                    Milestone::save($this->db,
                        [
                            'task_id' => $applicant['task_id'],
                            'for_id' => $applicant['seeker_id'],
                            'by_id' => $applicant['poster_id'],
                            'required_action' => 'declined'
                        ]);

                    $applicant['status'] = 'declined';
                    $actions = $this->get_tasks_milestone_action($all_tasks_ids);
                }

                $selected_applicants[$applicant['seeker_id']] = $applicant;
                $applicants_status[$applicant['seeker_id']] = $applicant['status'];
            }

            $task = $this->update_task_status($task, $applicants_status);

            $task['selected_applicants'] = $selected_applicants;
            $task['user']                = $user;
            $task['has_new_chats']       = in_array($task['status'], ['published', 'in-progress', 'marked-completed']) && $this->has_new_chats($task['id']);
            $task['has_chats']           = in_array($task['status'], ['published', 'in-progress', 'marked-completed']) && $this->has_chats($task['id']);
            $task['actions']             = array_filter($actions, function ($action) use ($task) {
                return $action['task_id'] === $task['id'] && $action['for_id'] === $task['user_id'] && in_array($task['status'], ['published', 'in-progress', 'marked-completed', 'completed']);
            });

            if( $this->is_task_inProgress($task['id']) ) {
                $task = $this->update_task_status($task, $applicants_status);
            }

            $task['has_new_comments'] = in_array($task['status'], ['published', 'in-progress', 'marked-completed']) && $this->has_new_comments($task['id']);
            $task['has_new_updates']  = in_array($task['status'], ['published', 'in-progress', 'marked-completed']) && ($task['has_new_chats'] || $task['has_new_comments'] || !empty($task['actions']));

            // setting side menu notification for task centre
            if($task['has_new_updates']){
                $tasks_notifications = true;
            }
        }

        foreach (array_merge($all_tasks, $tasks_applied) as &$task){
            $tags[$task['id']] = $this->tags($task['id']);
            $questions[$task['id']] = $this->questions($task['id']);
            $attachments[$task['id']] = $this->attachments($task['id']);

            // review reminder
            /*if( in_array($task['status'], ['closed', 'disputed']) && !is_null($task['assigned_to']) && in_array($this->user->info['id'], [$task['user_id'], $task['assigned_to']]) ){

                if( $this->user->info['id'] === $task['user_id'] ){
                    $task_reviewed = in_array($task['id'], array_column($reviews, 'task_id'));
                    $other_user = $task['assigned_to'];
                }elseif( $this->user->info['id'] === $task['assigned_to'] ){
                    $task_reviewed = in_array($task['id'], array_column($reviews, 'task_id'));
                    $other_user = $task['user_id'];
                }
                $this->db->query("SELECT CONCAT(m.`firstname`, ' ', m.`lastname`) AS `name`, d.`name` AS `company_name`
                                      FROM `members` m
                                      LEFT JOIN `company_details` d ON m.`type` = '1' AND d.`member_id` = m.`id`
                                      WHERE m.`id` = '{$other_user}'
                                      LIMIT 1");
                $task['reviewer'] = $this->db->getSingleRow();

                $task['from'] = $other_user;
                if( $this->user->info['id'] === $task['assigned_to'] ){
                    $task['to'] = $task['assigned_to'];
                }elseif( $this->user->info['id'] === $task['user_id'] ){
                    $task['to'] = $task['user_id'];
                }

                if( ! $this->has_review_reminder($this->user->info['id'], $task['id']) && ! $task_reviewed ){
                    Event::trigger('task.review.reminder', [$task]);
                    set('notifications', (new Notification($this->db, $this->user))->notifications($this->user->info['id']));
                }
            }*/
        }

        /*foreach($all_tasks as $key => $task){
            if( in_array($task['status'], ['published']) && strtotime($task['complete_by']) && Carbon::parse($task['complete_by'])->addDays(5)->lessThanOrEqualTo(Carbon::now()) ){
                //if( $this->get_total_applicants( $task['id'] )['count'] <= 0 ) {
                $this->cancel_task($task['id'], $task['user_id']);
                $all_tasks[$key]['status'] = 'cancelled';
                //}
            }

            // cancelling and full refund trigger
            $rejected = $this->rejected($task['id']);
            if(! empty($rejected) && strtotime($rejected['created_at']) && Carbon::parse($rejected['created_at'])->addDays(3)->greaterThan(Carbon::now()) && Carbon::parse($task['complete_by'])->lessThan(Carbon::now()) ){
                Event::trigger('task.full.refund', [$task]);
                $this->cancel_task($task['id'], $task['user_id']);
            }

            // task is expiring notification
            if( $task['status'] === 'published' && $this->user->info['id'] === $task['user_id'] && (int)$this->get_total_applicants( $task['id'] )['count'] === 0 && Carbon::parse($task['complete_by'])->greaterThanOrEqualTo(Carbon::now()->subHours(24)) ){
                if(! $this->has_expiring_reminder($task['user_id'], $task['id']) ){
                    Event::trigger('task.expiring', [$task]);
                    set('notifications', (new Notification($this->db, $this->user))->notifications($this->user->info['id']));
                }
            }

            // 50% milestone
            $task_days  = Carbon::parse($task['complete_by'])->diffInDays(Carbon::now()); //Carbon::parse($task['start_by'])
            $task_start = Carbon::now()->diffInDays(Carbon::parse($task['assigned'])); //Carbon::parse($task['start_by'])
            $progress   = floor(($task_start/$task_days) * 100);
            if( in_array($task['status'], ['in-progress', 'marked-completed']) &&  $progress >= 50 && $progress < 100 ){
                if(! $this->has_50_milestone_reminder($task['user_id'], $task['id']) ){
                    Event::trigger('task.50.milestone', [$task]);
                    set('notifications', (new Notification($this->db, $this->user))->notifications($this->user->info['id']));
                }
            }

            if(in_array($task['status'], ['completed', 'marked-completed', 'disputed', 'closed'])){
                $all_tasks[$key]['talent'] = $task_centre->get_talent($task['assigned_to'], false);
            }
            $all_tasks[$key]['has_chats']     = $this->has_chats($task['id']);
            $all_tasks[$key]['has_new_chats'] = $this->has_new_chats($task['id']);
            $all_tasks[$key]['has_new_comments'] = $this->has_new_comments($task['id']) && in_array($task['status'], ['published', 'in-progress', 'marked-completed']);
            $all_tasks[$key]['is_marked_completed']  = $this->activity->has_marked_completed_activity($task['id']);
            $all_tasks[$key]['has_new_updates'] = $all_tasks[$key]['is_marked_completed'] || $all_tasks[$key]['has_new_chats'] || $all_tasks[$key]['has_new_comments'];

            // setting side menu notification for task centre
            if($all_tasks[$key]['has_new_updates']){
                $tasks_notifications = true;
            }
        }*/

        $tasks_draft     = array_filter($all_tasks, function ($task) { return $task['status'] === 'draft'; });
        $tasks_published = array_filter($all_tasks, function ($task) { return in_array($task['status'], ['published', 'in-progress', 'marked-completed']); });
        $tasks_cancelled = array_filter($all_tasks, function ($task) { return $task['status'] === 'cancelled'; });
        $tasks_completed = array_filter($all_tasks, function ($task) { return $task['status'] === 'completed'; });
        $tasks_disputed  = array_filter($all_tasks, function ($task) { return $task['status'] === 'disputed'; });
        $tasks_closed    = array_filter($all_tasks, function ($task) { return $task['status'] === 'closed'; });
        $tasks_assigned  = array_filter($all_tasks, function ($task) {
            return !is_null($task['assigned']) && !is_null($task['assigned_to']) && in_array($task['status'], ['in-progress', 'marked-completed']);
            //return !empty($task['selected_applicants']) && in_array($task['status'], ['in-progress', 'marked-completed']);
        });

        $tasks_applied_ids  = [];
        $tasks_assigned_ids = [];

        foreach ($tasks_applied as $key => $task){

            // Set task to be 'in-progress' status if it's published and reached start date
            if( $this->is_published($task['id'], $task['user_id']) && $this->date::parse($task['start_by'])->lte( $this->date->copy()::now() ) ){
                if($this->set_in_progress($task['id'])) {
                    $task['status'] = 'in-progress';
                }
            }

            $seeker_status = $this->get_current_seeker_task_status($task['id'], $user_id);

            // check if seeker has rated the task and seeker's status is still as 'rate', then update it to be 'closed'
            if( $seeker_status === 'rate' ) {
                if ($rating_centre->has_rated_task($task['id'])) {
                    $this->db->table('user_task');
                    $this->db->updateArray(['status' => 'closed']);
                    $this->db->whereArray(['task_id' => $task['id'], 'user_id' => $user_id, 'type' => 'task']);
                    $this->db->update();

                    $seeker_status = 'closed';

                    Milestone::update($this->db, ['performed' => '1'],
                        [
                            'task_id' => $task['id'],
                            'for_id' => $user_id,
                            'by_id' => $task['user_id']
                        ]
                    );
                }
            }

            $user = $this->get_task_user($task['user_id']);
            unset($user['password']);
            $user['country']                      = $task_centre->getCountryName($user['country']);
            $tasks_applied[$key]['user']          = $user;
            $tasks_applied[$key]['user']['number']= (string)'TL' . ($user['id'] + 20100);

            $picked_applicants = $this->get_task_picked_applicants($user['id'], $task['id']);

            $selected_applicants = [];
            $applicants_status = [];

            foreach ($picked_applicants as $applicant){
                if( $this->is_status($task['id'], $applicant['seeker_id'], $task['user_id'], 'waiting') && $this->is_task_inProgress($task['id']) && $applicant['task_id'] === $task['id'] ){
                    if( $this->set_poster_seeker_in_progress($task['user_id'], $applicant['seeker_id'], $task['id']) ){
                        $applicant['status'] = 'in-progress';
                    }
                }
                $selected_applicants[$applicant['seeker_id']] = $applicant;
                $applicants_status[$applicant['seeker_id']] = $applicant['status'];
            }

            $task = $this->update_task_status($task, $applicants_status);

            $tasks_applied[$key]['selected_applicants'] = $selected_applicants;

            $tasks_applied[$key]['comments_count'] = count( array_filter($comments, function($comment) use($task){
                return $task['id'] === $comment;
            }) );

            if( array_key_exists($user_id, $selected_applicants) && in_array($task['status'], ['published', 'in-progress', 'marked-completed', 'completed']) ) {
                $tasks_applied[$key]['apply_status'] = in_array($selected_applicants[$user_id]['status'], ['confirmed', 'waiting']) ?
                                                        'appointed' :
                                                        $this->get_seeker_task_status($task['id'], $user_id);
            }elseif ( in_array($task['status'], ['cancelled', 'completed', 'disputed', 'closed']) ) {

                $tasks_applied[$key]['apply_status'] = $this->get_seeker_task_status($task['id'], $user_id) ?? 'closed';
            }else
                $tasks_applied[$key]['apply_status'] = $this->get_seeker_task_status($task['id'], $user_id) ?? 'applied';//$task['status'];

            $tasks_applied[$key]['poster_status'] = $this->get_poster_task_status($task['id'], $user_id, $task['user_id']);
            $tasks_applied[$key]['seeker_status'] = $seeker_status;
            $milestone_actions = Milestone::get($this->db, $task['id'], 'task_id');
            $for_ids = array_column(array_filter($milestone_actions, function($milestone){ return $milestone['performed'] === '0'; }), 'for_id');

            $tasks_applied[$key]['actions'] = array_filter($milestone_actions, function($action) use($task, $user_id){
                return $action['task_id'] === $task['id'] && $action['for_id'] === $user_id && $action['performed'] === '0';
            });

            $tasks_applied[$key]['was_rated'] = $rating_centre->has_rated_task($task['id']);
            $tasks_applied[$key]['milestone_action'] = in_array($user_id, $for_ids);
            $tasks_applied[$key]['has_new_chats'] = ($this->has_new_chats($task['id']) && !in_array($tasks_applied[$key]['apply_status'], ['lost', 'cancelled'])) ? $this->has_new_chats($task['id']) : false;
            $tasks_applied[$key]['has_chats']     = !in_array($seeker_status, ['applied', 'declined', 'cancelled']) || $this->has_chats($task['id']);
            $tasks_applied[$key]['has_new_comments'] = $this->has_new_comments($task['id']) && in_array($task['status'], ['published', 'in-progress', 'marked-completed']) && !in_array($tasks_applied[$key]['apply_status'], ['lost', 'cancelled']);
            $tasks_applied[$key]['talent']        = [];
            $tasks_applied_ids[]                  = $task['id'];
            $tasks_applied[$key]['is_rejected']  = false;//$this->activity->has_rejected_activity($task['id']);// && !in_array($tasks_applied[$key]['apply_status'], ['lost', 'cancelled']);
            $tasks_applied[$key]['is_accepted']  = false;//$this->activity->has_accepted_activity($task['id']);// && !in_array($tasks_applied[$key]['apply_status'], ['lost', 'cancelled']);
            $tasks_applied[$key]['is_disputed']  = false;//$this->activity->has_disputed_activity($task['id']);// && $task['status'] === 'disputed' && !in_array($tasks_applied[$key]['apply_status'], ['lost', 'cancelled']);
            $tasks_applied[$key]['is_completed']  = false;//$this->activity->has_completed_activity($task['id']);// && $task['status'] === 'completed' && !in_array($tasks_applied[$key]['apply_status'], ['lost', 'cancelled']);
            $tasks_applied[$key]['is_marked_completed']  = $tasks_applied[$key]['is_completed'];
            $tasks_applied[$key]['has_new_updates'] = $tasks_applied[$key]['is_rejected'] || $tasks_applied[$key]['is_accepted'] || $tasks_applied[$key]['is_disputed'] || $tasks_applied[$key]['is_completed'] || $tasks_applied[$key]['has_new_chats'] || $tasks_applied[$key]['milestone_action'];

            // setting side menu notification for task centre
            if($tasks_applied[$key]['has_new_updates']){
                $tasks_notifications = true;
            }
        }

        foreach ($tasks_assigned as $key => &$task){
            $user = $this->get_task_user($task['user_id']);
            unset($user['password']);
            $user['country']              = $task_centre->getCountryName($user['country']);
            $tasks_assigned[$key]['user'] = $user;
            $tasks_assigned_ids[]         = $task['id'];
            $task['applied_at']           = $this->get_applied_date($task['assigned_to'], $task['id']);
            $task['talent']               = $task_centre->get_talent($task['assigned_to'], false);
            $task['has_new_chats']        = $this->has_new_chats($task['id']);
            $task['has_chats']            = $this->has_chats($task['id']);
            $task['actions']              = array_filter($actions, function($action) use($task){
                                                return $action['task_id'] === $task['id'] && $action['for_id'] === $task['user_id'];
                                            });

            if ( is_null($task['assigned_to']) && is_null($task['assigned']) && $task['status'] === 'published' )
                $task['apply_status'] = 'applied';
            elseif ( $this->user->info['id'] === $task['assigned_to'] && !is_null($task['assigned']) && in_array($task['status'], ['in-progress', 'marked-completed']) )
                $task['apply_status'] = 'in-progress';
            elseif ( $this->user->info['id'] !== $task['assigned_to'] && $task['status'] !== 'published' )
                $task['apply_status'] = 'lost';
            else
                $task['apply_status'] = $task['status'];

            $task['is_rejected']  = false;//$this->activity->has_rejected_activity($task['id']) && !in_array($task['apply_status'], ['lost', 'cancelled']);
            $task['is_accepted']  = false;//$this->activity->has_accepted_activity($task['id']) && !in_array($task['apply_status'], ['lost', 'cancelled']);
            $task['is_disputed']  = false;//$this->activity->has_disputed_activity($task['id']) && $task['status'] === 'disputed' && !in_array($task['apply_status'], ['lost', 'cancelled']);
            $task['is_marked_completed']  = false;//$this->activity->has_marked_completed_activity($task['id']) && $task['status'] === 'marked-completed' && !in_array($task['apply_status'], ['lost', 'cancelled']);
            $task['has_new_updates'] = $task['is_rejected'] || $task['is_accepted'] || $task['is_disputed'] || $task['is_marked_completed'] || $task['has_new_chats'] || !empty($task['actions']);

            // setting side menu notification for task centre
            if($task['has_new_updates']){
                $tasks_notifications = true;
            }
        }

        $applicants = $applicants_count = $notifications = [];

        foreach (array_column($all_tasks, 'id') as $id){
            $count = $this->get_total_applicants( $id )['count'];
            if($count > 0) {
                $applicants_count[$id] = $count;
                $task_applicants = $this->get_applicants( [$id] );

                /*foreach ($task_applicants as &$applicant) {
                    if (isset($applicant['task_status']) && isset($applicant['task_end']) &&
                        $this->date->copy()::parse($applicant['task_end'])->lte($this->date->copy()::now()) &&
                        in_array($applicant['status'], ['applied', 'appointed', 'confirmed', 'declined'])) {
                        if($applicant['status'] !== 'declined') {
                            $applicant['status'] = 'declined';

                            $this->db->table('user_task');
                            $this->db->updateArray(['status' => $applicant['status']]);
                            $this->db->whereArray(['task_id' => $applicant['task_id'], 'user_id' => $applicant['user_id'], 'type' => 'task']);
                            $this->db->update();
                        }
                        $key = array_search($applicant['task_id'], array_column($all_tasks, 'id'));
                        $poster_id = $all_tasks[$key]['user_id'];
                        $exists = $this->db->getValue("SELECT COUNT(`id`) FROM `poster_seeker` 
                                                       WHERE `task_id` = '{$applicant['task_id']}' AND 
                                                       `seeker_id` = '{$applicant['user_id']}' AND 
                                                       `poster_id` = '{$poster_id}'");
                        if( ! $exists ) {
                            $this->db->table('poster_seeker');
                            $this->db->insertArray([
                                'task_id'   => $applicant['task_id'],
                                'poster_id' => $all_tasks[$key]['user_id'],
                                'seeker_id' => $applicant['user_id'],
                                'status'    => 'declined',
                                'pinned'    => $applicant['pinned']
                            ]);
                            $this->db->insert();

                            Milestone::save($this->db, [
                                'task_id' => $applicant['task_id'],
                                'by_id' => $all_tasks[$key]['user_id'],
                                'for_id' => $applicant['user_id'],
                                'required_action' => 'declined',
                                'performed' => '1',
                            ]);
                        }


                        if($key && !in_array($applicant['user_id'], array_keys($all_tasks[$key]['selected_applicants']))) {
                            $all_tasks[$key]['selected_applicants'][$applicant['user_id']] = [
                                'id' => $applicant['id'],
                                'poster_id' => $all_tasks[$key]['user_id'],
                                'seeker_id' => $applicant['user_id'],
                                'task_id' => $applicant['task_id'],
                                'status' => $applicant['status'],
                                'pinned' => $applicant['pinned'],
                                'created_at' => $applicant['created_at'],
                                'updated_at' => $applicant['updated_at'],
                                'firstname' => $applicant['firstname'],
                                'lastname' => $applicant['lastname'],
                                'photo' => $applicant['photo']
                            ];
                        }

                    }
                }*/

                $applicants = array_merge( $applicants, $task_applicants );
            }
        }
        //$applicants_ids = array_map(function($applicants){ return array_keys($applicants); }, array_filter(array_column($all_tasks, 'selected_applicants')));
        //dd($all_tasks, $applicants);

        foreach ($applicants as $applicant){
            $notifications[$applicant['task_id']][$applicant['user_id']] = ('N' === $applicant['viewed'] && $all_tasks_status[$applicant['task_id']] !== 'closed') ? true : false;

            // setting side menu notification for task centre
            /*if($notifications[$applicant['task_id']][$applicant['user_id']]){
                $tasks_notifications = true;
            }*/
        }


        $applied = [
            'active' => array_filter($tasks_applied, function ($task) {
                $status = in_array($task['apply_status'], ['payment', 'rate', 'disputed']) ? ($task['seeker_status'] === 'disputed' && ($task['poster_status'] === 'disputed' || $task['poster_status'] === 'rate') && empty($task['actions']) ? 'disputed' : 'completed') : $task['apply_status'];
                return in_array($status, ['applied', 'appointed', 'in-progress', 'marked-completed', 'completed']);
            }),
            'inactive' => array_filter($tasks_applied, function ($task) {
                $status = in_array($task['apply_status'], ['payment', 'rate', 'disputed']) ? ($task['seeker_status'] === 'disputed' && ($task['poster_status'] === 'disputed' || $task['poster_status'] === 'rate') && empty($task['actions']) ? 'disputed' : 'completed') : $task['apply_status'];
                return !in_array($status, ['applied', 'appointed', 'in-progress', 'marked-completed', 'completed']);
            }),
            'active_total' => $tasks_applied_total ?? 0,
            'inactive_total' => $tasks_inactive_applied_total ?? 0,
        ];

        $posted = [
            'active' => array_filter($all_tasks, function ($task) { return in_array($task['status'], ['published', 'in-progress', 'marked-completed', 'completed']); }),
            'inactive' => array_filter($all_tasks, function ($task) { return !in_array($task['status'], ['published', 'in-progress', 'marked-completed', 'completed']); }),
            'active_total' => $all_tasks_total ?? 0,
            'inactive_total' => $all_inactive_tasks_total ?? 0,
        ];

        return [
            'all'              => $all_tasks,
            'draft'            => $tasks_draft,
            'published'        => $tasks_published,
            'completed'        => $tasks_completed,
            'cancelled'        => $tasks_cancelled,
            'applied'          => $tasks_applied,
            'assigned'         => $tasks_assigned,
            'all_posted'       => $posted,
            'all_applied'      => $applied,
            'applied_ids'      => $tasks_applied_ids,
            'assigned_ids'     => $tasks_assigned_ids,
            'closed'           => array_merge($tasks_completed, $tasks_cancelled, $tasks_disputed, $tasks_closed),
            'tags'             => $tags,
            'questions'        => $questions,
            'attachments'      => $attachments,
            'applicants'       => $applicants,
            'applicants_count' => $applicants_count,
            'notifications'    => $notifications,
            'actions'          => $actions,
            'payments'         => $payments,
        ];

    }

    public function get_task_user($user_id = null){
        if(null === $user_id) return [];

        $this->db->query("SELECT * FROM `members` m WHERE m.`id` = '{$user_id}' LIMIT 1");
        $user = $this->db->getSingleRow();
        if( !empty($user) ) $user['number'] = (string)'TL' . ($user['id'] + 20100);

        if( $user['type'] === '1' ){
            $this->db->query("SELECT * FROM `company_details` WHERE `member_id` = '{$user['id']}' LIMIT 1");
            $user['company'] = $this->db->getSingleRow();
        }

        return $user;
    }

    public function get_current_user($user_id = null){
        if(null === $user_id) return [];

        $this->db->query("SELECT * FROM `members` m WHERE m.`id` = '{$user_id}' LIMIT 1");
        $user = $this->db->getSingleRow();
        if( !empty($user) ) $user['number'] = (string)'TL' . ($user['id'] + 20100);

        if( $user['type'] === '1' ){
            $this->db->query("SELECT * FROM `company_details` WHERE `member_id` = '{$user['id']}' LIMIT 1");
            $user['company'] = $this->db->getSingleRow();
        }

        return $user;
    }

    public function get_task($slug = null){
        if(null === $slug) return [];

        $this->db->query("SELECT *, t.`status` AS `task_status`, t.`id` AS `task_id`, t.`type` AS `task_type`, t.`skills` AS `task_skills` 
                          FROM `tasks` t
                          INNER JOIN `members` m ON m.`id` = t.`user_id` 
                          WHERE t.`slug` = '{$slug}' LIMIT 1");
        return $this->db->getSingleRow();
    }

    public function get_external_task($slug = null){
        if(null === $slug) return [];

        $this->db->query("SELECT *, t.`status` AS `task_status`, t.`id` AS `task_id`, t.`type` AS `task_type`, 
                          'others' AS `category_name`, 'others' AS `subcategory_name`, t.`skills` AS `task_skills` 
                          FROM `external_tasks` t
                          WHERE t.`slug` = '{$slug}' LIMIT 1");

        return $this->db->getSingleRow();
    }

    public function get_by_id($id = null){
        return $this->get_task_by_id($id);
    }

    public function get_task_by_id($id = null){
        if(null === $id) return [];
        $id = mysqli_real_escape_string($this->db->connection, $id);
        $this->db->query("SELECT t.*, m.`id` AS `id`, t.`status` AS `task_status`, t.`id` AS `task_id`, m.`id` AS `user_id`, 
                          m.`type` AS `user_type`, t.`type` AS `task_type`, m.`firstname`, m.`lastname`,
                          SUBSTRING_INDEX(SUBSTRING_INDEX(`state_country`, ',', 1), ',', -1) AS `state`,
                          SUBSTRING_INDEX(SUBSTRING_INDEX(`state_country`, ',', 2), ',', -1) AS `country`,
                          (SELECT `title` FROM `task_category` WHERE `id` = t.`category` AND `parent` = '0' LIMIT 1) AS `category_name`,   
                          (SELECT `title` FROM `task_category` WHERE `id` = t.`sub_category` AND `parent` = t.`category` LIMIT 1) AS `subcategory_name`, 
                          c.`name` AS `company_name`   
                          FROM `members` m
                          INNER JOIN `tasks` t ON m.`id` = t.`user_id` 
                          LEFT JOIN `company_details` c ON m.`id` = c.`member_id` 
                          WHERE t.`id` = '{$id}' LIMIT 1");
        return $this->db->getSingleRow();
    }

    public function get_external_task_by_id($id = null){
        if(null === $id) return [];

        $this->db->query("SELECT t.*, t.`status` AS `task_status`, t.`id` AS `task_id`, t.`type` AS `task_type`, 'others' AS `category_name`, 'others' AS `subcategory_name`,
                          SUBSTRING_INDEX(SUBSTRING_INDEX(`state_country`, ',', 1), ',', -1) AS `state`,
                          SUBSTRING_INDEX(SUBSTRING_INDEX(`state_country`, ',', 2), ',', -1) AS `country`
                          FROM `external_tasks` t 
                          WHERE t.`id` = '{$id}' LIMIT 1");
        return $this->db->getSingleRow();
    }

    public function get_tasks_by_id($ids = []){
        if( empty($ids) ) return [];

        $ids = implode("','", $ids);
        $query = "SELECT t.*, m.`id` AS `id`, t.`status` AS `task_status`, t.`id` AS `task_id`, m.`id` AS `user_id`, m.`type` AS `user_type`, t.`type` AS `task_type`, 1 AS `priority`,
                          SUBSTRING_INDEX(SUBSTRING_INDEX(`state_country`, ',', 1), ',', -1) AS `state`,
                          SUBSTRING_INDEX(SUBSTRING_INDEX(`state_country`, ',', 2), ',', -1) AS `country`,
                          (SELECT `title` FROM `task_category` WHERE `id` = t.`category` AND `parent` = '0' LIMIT 1) AS `category_name`,   
                          (SELECT `title` FROM `task_category` WHERE `id` = t.`sub_category` AND `parent` = t.`category` LIMIT 1) AS `subcategory_name`, 
                          c.`name` AS `company_name`";

        if( $this->user->info['preference']['dashboard_mode'] === 'search' ){
            if( $this->user->info['preference']['search_mode'] === 'match' ){
                $query .= ", CASE WHEN t.`id` IN( SELECT `id` FROM `tasks` WHERE ";
                $user_skills = $this->user->info['skills'];
                $user_skills = explode(',', $user_skills);
                $count = count($user_skills);
                foreach ($user_skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $count) $query .= " OR ";
                }

                $query .= " ) THEN 1 ELSE 0 END AS `matched` ";
            }
        }

        $query .= "       FROM `members` m
                          INNER JOIN `tasks` t ON m.`id` = t.`user_id` 
                          LEFT JOIN `company_details` c ON m.`id` = c.`member_id` 
                          WHERE t.`id` IN ('{$ids}') ORDER BY ";

        if( $this->user->info['preference']['dashboard_mode'] === 'search' ){
            if( $this->user->info['preference']['search_mode'] === 'match' ){
                $query .= " `matched` DESC, ";
            }
        }

        $query .= "t.`created_at` DESC";

        $this->db->query($query);
        return $this->db->getRowList();
    }

    public function get_external_tasks_by_id($ids = []){
        if( empty($ids)) return [];

        $ids = implode("','", $ids);

        $this->db->query("SELECT *, t.`status` AS `task_status`, t.`id` AS `task_id`, t.`type` AS `task_type`, 2 AS `priority`, 'others' AS `category_name`, 'others' AS `subcategory_name`, 0 AS `matched`,
                          SUBSTRING_INDEX(SUBSTRING_INDEX(t.`state_country`, ',', 1), ',', -1) AS `state`,
                          SUBSTRING_INDEX(SUBSTRING_INDEX(t.`state_country`, ',', 2), ',', -1) AS `country`
                          FROM `external_tasks` t 
                          WHERE t.`id` IN ('{$ids}') ORDER BY t.`created_at` DESC");

        return $this->db->getRowList();
    }

    public function get_tasks($user_id = null, $offset = 0, $limit = 25, $group = true, $filter = null, $order = 'latest'){
        $this->is_logged_in();
        if(null === $user_id) $user_id = $this->user->info['id'];
        if( !is_null($filter) && $filter !== 'all' && session('current_task_centre_view') !== 'calendar'){
            $statuses = "IN('{$filter}')";
        }else {
            $statuses = $group ? "IN('published', 'in-progress', 'completed')" : "NOT IN('published', 'in-progress', 'completed')";
        }

        $orders = [
            'latest' => "t.`created_at` DESC",
            'value_high_to_low' => "`total_budget` DESC, t.`budget` DESC",
            'value_low_to_high' => "`total_budget` ASC, t.`budget` ASC",
            'applicants_high_to_low' => "`applicants_count` DESC",
            'applicants_low_to_high' => "`applicants_count` ASC"
        ];

        $orderBy = $orders[$order] ?? $orders['latest'];
        $this->db->query("SELECT *, 
                          (SELECT COUNT(DISTINCT `user_id`) FROM `user_task` u WHERE u.`task_id` = t.`id`) AS `applicants`,
                          (SELECT `title` FROM `task_category` WHERE `id` = t.`category` AND `parent` = '0' LIMIT 1) AS `category_name`,
                          (SELECT `title` FROM `task_category` WHERE `id` = t.`sub_category` AND `parent` = t.`category` LIMIT 1) AS `subcategory_name`,
                          CASE WHEN t.`status` = 'in-progress' THEN '2' WHEN t.`status` = 'published' THEN '1' ELSE '3' END AS `priority`,
                          (SELECT COUNT(DISTINCT `user_id`) FROM `user_task` WHERE `task_id` = t.`id` AND `type` = 'task') AS `applicants_count`,
                          (t.`budget` * (SELECT COUNT(DISTINCT p.`seeker_id`) FROM `poster_seeker` p WHERE p.`task_id` = t.`id` AND p.`status` NOT IN ('appointed','declined','cancelled','unfulfilled'))) AS `total_budget`
                          
                          FROM `tasks` t 
                          WHERE t.`user_id` = '{$user_id}' AND t.`status` {$statuses} ORDER BY {$orderBy} LIMIT {$offset}, {$limit}"); //`priority` ASC,

        return $this->db->getRowList();
    }

    public function get_tasks_total($user_id = null, $group = true, $filter = null){
        $this->is_logged_in();
        if(null === $user_id) $user_id = $this->user->info['id'];
        if( !is_null($filter) && $filter !== 'all' && session('current_task_centre_view') !== 'calendar'){
            $statuses = "IN('{$filter}')";
        }else {
            $statuses = $group ? "IN('published', 'in-progress', 'completed')" : "NOT IN('published', 'in-progress', 'completed')";
        }

        $this->db->query("SELECT COUNT(t.`id`) AS `count`
                          FROM `tasks` t 
                          WHERE t.`user_id` = '{$user_id}' AND t.`status` {$statuses} LIMIT 1");

        return $this->db->getValue();
    }

    public function get_applied_tasks($user_id = null, $offset = 0, $limit = 25, $group = true, $filter = null, $order = 'latest'){
        $this->is_logged_in();
        if(null === $user_id) return [];
        $statuses = $group ? "IN('published', 'in-progress', 'completed')" : "NOT IN('published', 'in-progress', 'completed')";
        if( !is_null($filter) && $filter !== 'all' && session('current_task_centre_view') !== 'calendar'){
            $seeker_statuses = "IN('{$filter}')";
        }else {
            $seeker_statuses = $group ? "IN('applied', 'appointed', 'payment', 'in-progress', 'completed', 'rate')" : "NOT IN('applied', 'appointed', 'payment', 'in-progress', 'completed', 'rate')";
        }

        $orders = [
            'latest'                 => 't.`created_at` DESC',
            'value_high_to_low'      => 't.`budget` DESC',
            'value_low_to_high'      => 't.`budget` ASC'
        ];

        $orderBy = $orders[$order] ?? $orders['latest'];

        $this->db->query("SELECT t.*, u.`created_at` AS `applied_at`,
                          (SELECT `title` FROM `task_category` WHERE `id` = t.`category` AND `parent` = '0' LIMIT 1) AS `category_name`,
                          (SELECT `title` FROM `task_category` WHERE `id` = t.`sub_category` AND `parent` = t.`category` LIMIT 1) AS `subcategory_name`,
                          (SELECT COUNT(DISTINCT `user_id`) FROM `user_task` WHERE `task_id` = t.`id` AND `type` = 'task') AS `applicants_count` 
                          FROM `user_task` u 
                          INNER JOIN `tasks` t ON t.`id` = u.`task_id`
                          WHERE u.`user_id` = '{$user_id}' AND u.`status` {$seeker_statuses}
                          ORDER BY {$orderBy} 
                          LIMIT {$offset}, {$limit}"); // AND t.`status` {$statuses}

        return $this->db->getRowList();
    }

    public function get_applied_tasks_total($user_id = null, $group = true, $filter = null){
        $this->is_logged_in();
        if(null === $user_id) return [];
        $statuses = $group ? "IN('published', 'in-progress', 'completed')" : "NOT IN('published', 'in-progress', 'completed')";
        if( !is_null($filter) && $filter !== 'all' && session('current_task_centre_view') !== 'calendar'){
            $seeker_statuses = "IN('{$filter}')";
        }else {
            $seeker_statuses = $group ? "IN('applied', 'appointed', 'payment', 'in-progress', 'completed', 'rate')" : "NOT IN('applied', 'appointed', 'payment', 'in-progress', 'completed', 'rate')";
        }

        $this->db->query("SELECT COUNT(t.`id`) AS `count` 
                          FROM `user_task` u 
                          INNER JOIN `tasks` t ON t.`id` = u.`task_id`
                          WHERE u.`user_id` = '{$user_id}' AND u.`status` {$seeker_statuses} LIMIT 1");

        return $this->db->getValue();
    }

    public function get_applied_external_tasks($user_id = null, $offset = 0, $limit = 25){
        $this->is_logged_in();
        if(null === $user_id) return [];

        $this->db->query("SELECT t.*, u.`created_at` AS `applied_at` 
                          FROM `user_task` u 
                          INNER JOIN `external_tasks` t ON t.`id` = u.`task_id`
                          WHERE u.`user_id` = {$user_id}
                          ORDER BY t.`created_at` DESC 
                          LIMIT {$offset}, {$limit}");
        return $this->db->getRowList();
    }

    public function get_applied_date($user_id = null, $task_id = null){
        $this->is_logged_in();
        if(is_null($user_id) || is_null($task_id)) return [];

        $this->db->query("SELECT u.`created_at` AS `applied_at` 
                          FROM `user_task` u 
                          INNER JOIN `tasks` t ON t.`id` = u.`task_id`
                          WHERE u.`user_id` = {$user_id} AND u.`task_id` = '{$task_id}'
                          ORDER BY t.`created_at` DESC 
                          LIMIT 1");
        return $this->db->getValue();
    }

    public function get_published_tasks_count($user_id = null){
        if(null === $user_id) return 0;

        $this->db->query("SELECT COUNT(*) AS `count` FROM `tasks` t WHERE t.`user_id` IN ({$user_id}) AND t.`status` IN('published', 'in-progress', 'marked-completed', 'completed', 'disputed', 'closed')");
        return $this->db->getValue();
    }

    public function get_all_published_tasks_count(...$args){
        $search  = $args[0];
        $filters = $args[1];
        $query = "SELECT SUM(`count`) AS `count` FROM (";
        $query .= "SELECT COUNT(*) AS `count` FROM `tasks` t INNER JOIN `members` m ON t.`user_id` = m.`id`
                  WHERE t.`status` IN ('published', 'in-progress') AND DATE(`complete_by`) > DATE(NOW()) ";

        if(!empty($search)) {

            $query .= " AND ( ";
			
			$length = count($search);
            foreach ($search as $key => $str) {
                $str = mysqli_real_escape_string($this->db->connection, $str);
                $query .= "   LOWER(CONVERT(t.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(t.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(t.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) "; /// closing for AND (

        }

        if(!empty(array_filter($filters))){
            extract($filters);

            if(isset($task_budget)){
                $query .= " AND (t.`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate') {
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'startDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(t.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (t.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if(isset($category)){
                $query .= " AND (t.`category` IN({$category}) OR t.`sub_category` IN({$category}))";
            }

            if(isset($task_type)){
                if(is_array($task_type))
                    $task_type = implode("','", $task_type);
                $task_type = "'{$task_type}'";

                $query .= " AND (t.`type` IN({$task_type}))";
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " AND ( ";
            }

            if(isset($state_name) && !empty($state_name)){
                $first = true;

                if(is_array($state_name)) {
                    $length = count( $state_name );
                    foreach ($state_name as $key => $state) {
                        $state = mysqli_real_escape_string($this->db->connection, strtolower($state));
                        $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                        $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                        if ($key + 1 !== $length) $query .= " OR ";
                    }
                }else{
                    $state = mysqli_real_escape_string($this->db->connection, strtolower($state_name));
                    $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                }
            }

            if(isset($city_name) && !empty($city_name)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $city = mysqli_real_escape_string($this->db->connection, strtolower($city_name));
                $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$city}%' ";
                $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$city}%' ";
            }

            if(isset($states['names']) && !empty($states['names'])){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count( $states['names'] );
                foreach($states['names'] as $key => $state) {
                    $state = mysqli_real_escape_string($this->db->connection, strtolower($state));
                    $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if(isset($nearby_places)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = mysqli_real_escape_string($this->db->connection, strtolower($place));
                    $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$place}%' ";
                    $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$place}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
                $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE 'remotely' ";
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " ) ";
            }

            if(empty($search)) {
                if (!isset($state_name, $city_name, $states['names'], $nearby_places)) {
                    $query .= " OR ";
                } else {
                    $query .= " OR ";
                }

                $query .= " t.`id` IN( SELECT `id` FROM `tasks` WHERE `location` = 'remotely' AND `status` IN ('published', 'in-progress') AND DATE(`complete_by`) > DATE(NOW()) ";
                if (isset($remote_state)) {
                    $remote_state = mysqli_real_escape_string($this->db->connection, $remote_state);
                    $query .= " AND LOWER(CONVERT(`state_country` USING utf8mb4)) LIKE '%{$remote_state}%'";
                }

                if (isset($task_budget)) {
                    $query .= " AND (`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
                }

                if (isset($category)) {
                    $query .= " AND (`category` IN({$category}) OR `sub_category` IN({$category}))";
                }

                if(isset($task_type)){
                    $query .= " AND (`type` IN({$task_type}))";
                }

                if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                    $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                    $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                    if($date_period !== 'customrange') {
                        if($date_type === 'postedDate') {
                            $now  = Carbon::now()->toDateString();
                            $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                        }elseif($date_type === 'startDate') {
                            $date = Carbon::now()->toDateString();
                            $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                        }
                    }elseif(isset($from, $to)){
                        $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                        $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                    }

                    $query .= " AND (DATE(`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
                }

                if(isset($skills)){
                    if(!is_array($skills)) $skills = explode(',', $skills);

                    $query .= "   AND (t.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                    $length = count($skills);
                    foreach ($skills as $key => $skill){
                        $query .= " FIND_IN_SET({$skill}, `skills`) ";
                        if($key+1 !== $length) $query .= " OR ";
                    }

                    $query .= "   )) ";
                }

                $query .= " ) ";
            }
        }

        $query .= " UNION ";

        $query .= "SELECT COUNT(DISTINCT(`number`)) AS `count` FROM `external_tasks` xt 
                    WHERE xt.`status` = 'published' AND DATE(`complete_by`) > DATE(NOW()) ";


        if(!empty($search)) {

            $query .= " AND ( ";

            $length = count($search);
            foreach ($search as $key => $str) {
                $str = mysqli_real_escape_string($this->db->connection, $str);
                $query .= "   LOWER(CONVERT(xt.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(xt.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(xt.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) "; /// closing for AND (

        }

        if(!empty(array_filter($filters))){
            extract($filters);
            $first = false;
            if(isset($task_budget)){
                $query .= " AND (xt.`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate') {
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'startDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(xt.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (xt.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if(isset($category)){
                $query .= " AND (xt.`category` IN({$category}) OR xt.`sub_category` IN({$category}))";
            }

            if(isset($task_type)){
                if(is_array($task_type))
                    $task_type = implode("','", $task_type);
                $task_type = "'{$task_type}'";

                $query .= " AND (xt.`type` IN({$task_type}))";
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " AND ( ";
            }

            if(isset($state_name) && !empty($state_name)){
                $first = true;

                if( is_array($state_name) ) {
                    $length = count($state_name);
                    foreach ($state_name as $key => $state) {
                        $state = mysqli_real_escape_string($this->db->connection, strtolower($state));
                        $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                        $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                        if ($key + 1 !== $length) $query .= " OR ";
                    }
                }else{
                    $state = mysqli_real_escape_string($this->db->connection, strtolower($state_name));
                    $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                }
            }

            if(isset($city_name) && !empty($city_name)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $city = mysqli_real_escape_string($this->db->connection, strtolower($city_name));
                $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$city}%' ";
                $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$city}%' ";
            }

            if(isset($states['names']) && !empty($states['names'])){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count( $states['names'] );
                foreach($states['names'] as $key => $state) {
                    $state = mysqli_real_escape_string($this->db->connection, strtolower($state));
                    $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if(isset($nearby_places)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = mysqli_real_escape_string($this->db->connection, strtolower($place));
                    $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$place}%' ";
                    $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$place}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
                $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE 'remotely' ";
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " ) ";
            }

            if(empty($search)) {
                if (!isset($state_name, $city_name, $states['names'], $nearby_places)) {
                    $query .= " OR ";
                } else {
                    $query .= " OR ";
                }

                $query .= " xt.`id` IN( SELECT `id` FROM `tasks` WHERE `location` = 'remotely' AND `status` IN ('published', 'in-progress') AND DATE(`complete_by`) > DATE(NOW()) ";
                if (isset($remote_state)) {
                    $remote_state = mysqli_real_escape_string($this->db->connection, $remote_state);
                    $query .= " AND LOWER(CONVERT(`state_country` USING utf8mb4)) LIKE '%{$remote_state}%'";
                }

                if (isset($task_budget)) {
                    $query .= " AND (`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
                }

                if (isset($category)) {
                    $query .= " AND (`category` IN({$category}) OR `sub_category` IN({$category}))";
                }

                if(isset($task_type)){
                    $query .= " AND (`type` IN({$task_type}))";
                }

                if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                    $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                    $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                    if($date_period !== 'customrange') {
                        if($date_type === 'postedDate') {
                            $now  = Carbon::now()->toDateString();
                            $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                        }elseif($date_type === 'startDate') {
                            $date = Carbon::now()->toDateString();
                            $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                        }
                    }elseif(isset($from, $to)){
                        $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                        $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                    }

                    $query .= " AND (DATE(`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
                }

                if(isset($skills)){
                    if(!is_array($skills)) $skills = explode(',', $skills);

                    $query .= "   AND (xt.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                    $length = count($skills);
                    foreach ($skills as $key => $skill){
                        $query .= " FIND_IN_SET({$skill}, `skills`) ";
                        if($key+1 !== $length) $query .= " OR ";
                    }

                    $query .= "   )) ";
                }

                $query .= " ) ";
            }
        }

        $query .= ") AS `temp` LIMIT 1";

        $this->db->query($query);
        return $this->db->getValue();
    }

    public function get_all_published_tasks_jobs_count(...$args){
        $search  = $args[0];
        $filters = $args[1];

        $query = "SELECT SUM(`count`) AS `count` FROM (
                    SELECT COUNT(*) AS `count` FROM `tasks` t INNER JOIN `members` m ON t.`user_id` = m.`id`
                    WHERE t.`status` IN ('published', 'in-progress') AND DATE(t.`complete_by`) > DATE(NOW()) ";

        if(!empty($search)) {

            $query .= " AND ( ";
            $length = count($search);
            foreach ($search as $key => $str) {
                $query .= "   LOWER(CONVERT(t.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(t.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(t.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) "; /// closing for AND (

        }

        if(!empty(array_filter($filters))){
            extract($filters);
            $first = false;
            if(isset($task_budget)){
                $query .= " AND (t.`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate') {
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'startDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(t.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (t.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if(isset($category)){
                $query .= " AND (t.`category` IN({$category}) OR t.`sub_category` IN({$category}))";
            }

            if(isset($task_type)){
                if(is_array($task_type))
                    $task_type = implode("','", $task_type);
                $task_type = "'{$task_type}'";

                $query .= " AND (t.`type` IN({$task_type}))";
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " AND ( ";
            }

            if(isset($state_name)){
                $first = true;
                if( is_array($state_name) ) {
                    $length = count($state_name);
                    foreach ($state_name as $key => $state) {
                        $state = mysqli_real_escape_string($this->db->connection, strtolower($state));
                        $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                        $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                        if ($key + 1 !== $length) $query .= " OR ";
                    }
                }else{
                    $state = mysqli_real_escape_string($this->db->connection, strtolower($state_name));
                    $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                }
            }

            if(isset($city_name) && !empty($city_name)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $city = mysqli_real_escape_string($this->db->connection, strtolower($city_name));
                $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$city}%' ";
                $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$city}%' ";
            }

            if(isset($states['names']) && !empty($states['names'])){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count( $states['names'] );
                foreach($states['names'] as $key => $state) {
                    $state = strtolower($state);
                    $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if(isset($nearby_places)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = strtolower($place);
                    $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$place}%' ";
                    $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$place}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " ) ";
            }

            if(empty($search)) {
                if (!isset($state_name, $city_name, $states['names'], $nearby_places)) {
                    $query .= " OR ";
                } else {
                    $query .= " OR ";
                }

                $query .= " t.`id` IN( SELECT `id` FROM `tasks` WHERE `location` = 'remotely' AND `status` IN ('published', 'in-progress') AND DATE(`complete_by`) > DATE(NOW()) ";
                if (isset($remote_state)) {
                    $query .= " AND LOWER(CONVERT(`state_country` USING utf8mb4)) LIKE '%{$remote_state}%'";
                }

                if (isset($task_budget)) {
                    $query .= " AND (`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
                }

                if (isset($category)) {
                    $query .= " AND (`category` IN({$category}) OR `sub_category` IN({$category}))";
                }

                if(isset($task_type)){
                    $query .= " AND (`type` IN({$task_type}))";
                }

                if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                    $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                    $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                    if($date_period !== 'customrange') {
                        if($date_type === 'postedDate') {
                            $now  = Carbon::now()->toDateString();
                            $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                        }elseif($date_type === 'startDate') {
                            $date = Carbon::now()->toDateString();
                            $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                        }
                    }elseif(isset($from, $to)){
                        $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                        $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                    }

                    $query .= " AND (DATE(`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
                }

                if(isset($skills)){
                    if(!is_array($skills)) $skills = explode(',', $skills);

                    $query .= "   AND (t.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                    $length = count($skills);
                    foreach ($skills as $key => $skill){
                        $query .= " FIND_IN_SET({$skill}, `skills`) ";
                        if($key+1 !== $length) $query .= " OR ";
                    }

                    $query .= "   )) ";
                }

                $query .= " ) ";
            }
        }

        $query .= " UNION ";

        $query .= "SELECT COUNT(*) AS `count` FROM `external_tasks` xt 
                    WHERE xt.`status` = 'published' AND xt.`assigned` IS NULL AND xt.`assigned_to` IS NULL ";

        if(!empty($search)) {

            $query .= " AND ( ";
            $length = count($search);
            foreach ($search as $key => $str) {
                $query .= "   LOWER(CONVERT(xt.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(xt.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(xt.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) "; /// closing for AND (

        }

        if(!empty(array_filter($filters))){
            extract($filters);
            $first = false;
            if(isset($task_budget)){
                $query .= " AND (xt.`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate') {
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'startDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(xt.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (xt.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if(isset($category)){
                $query .= " AND (xt.`category` IN({$category}) OR xt.`sub_category` IN({$category}))";
            }

            if(isset($task_type)){
                if(is_array($task_type))
                    $task_type = implode("','", $task_type);
                $task_type = "'{$task_type}'";

                $query .= " AND (xt.`type` IN({$task_type}))";
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " AND ( ";
            }

            if(isset($state_name)){
                $first = true;
                if( is_array($state_name) ) {
                    $length = count($state_name);
                    foreach ($state_name as $key => $state) {
                        $state = mysqli_real_escape_string($this->db->connection, strtolower($state));
                        $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                        $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                        if ($key + 1 !== $length) $query .= " OR ";
                    }
                }else{
                    $state = mysqli_real_escape_string($this->db->connection, strtolower($state_name));
                    $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                }
            }

            if(isset($city_name) && !empty($city_name)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $city = mysqli_real_escape_string($this->db->connection, strtolower($city_name));
                $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$city}%' ";
                $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$city}%' ";
            }

            if(isset($states['names']) && !empty($states['names'])){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count( $states['names'] );
                foreach($states['names'] as $key => $state) {
                    $state = strtolower($state);
                    $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if(isset($nearby_places)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = strtolower($place);
                    $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$place}%' ";
                    $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$place}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " ) ";
            }

            if(empty($search)) {
                if (!isset($state_name, $city_name, $states['names'], $nearby_places)) {
                    $query .= " OR ";
                } else {
                    $query .= " OR ";
                }

                $query .= " xt.`id` IN( SELECT `id` FROM `tasks` WHERE `location` = 'remotely' AND `status` IN ('published', 'in-progress') AND DATE(`complete_by`) > DATE(NOW()) ";
                if (isset($remote_state)) {
                    $query .= " AND LOWER(CONVERT(`state_country` USING utf8mb4)) LIKE '%{$remote_state}%'";
                }

                if (isset($task_budget)) {
                    $query .= " AND (`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
                }

                if (isset($category)) {
                    $query .= " AND (`category` IN({$category}) OR `sub_category` IN({$category}))";
                }

                if(isset($task_type)){
                    $query .= " AND (`type` IN({$task_type}))";
                }

                if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                    $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                    $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                    if($date_period !== 'customrange') {
                        if($date_type === 'postedDate') {
                            $now  = Carbon::now()->toDateString();
                            $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                        }elseif($date_type === 'startDate') {
                            $date = Carbon::now()->toDateString();
                            $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                        }
                    }elseif(isset($from, $to)){
                        $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                        $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                    }

                    $query .= " AND (DATE(`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
                }

                if(isset($skills)){
                    if(!is_array($skills)) $skills = explode(',', $skills);

                    $query .= "   AND (xt.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                    $length = count($skills);
                    foreach ($skills as $key => $skill){
                        $query .= " FIND_IN_SET({$skill}, `skills`) ";
                        if($key+1 !== $length) $query .= " OR ";
                    }

                    $query .= "   )) ";
                }

                $query .= " ) ";
            }
        }

        $query .= " UNION ";

        $query .= "SELECT COUNT(*) AS `count` FROM `jobs` j INNER JOIN `members` m ON j.`user_id` = m.`id`
                    WHERE j.`status` = 'published' AND j.`assigned` IS NULL AND j.`assigned_to` IS NULL";

        if(!empty($search)) {
            $query .= " AND ( ";

            $length = count($search);
            foreach ($search as $key => $str) {
                $query .= "   LOWER(CONVERT(j.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(j.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(j.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) ";
        }

        if(!empty(array_filter($filters))){
            extract($filters);
            $first = false;
            if(isset($category)){
                $query .= " AND (j.`category` IN({$category}) OR j.`sub_category` IN({$category}))";
            }

            if(isset($state)){
                $query .= " AND (j.`state` IN({$state}))";
            }

            if(isset($states['ids']) && !empty($states['ids']) ){
                $states = implode(',', $states['ids']);
                $query .= " AND (j.`state` IN({$states}))";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (j.`id` IN(
                             SELECT `id` FROM `jobs` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if( isset($city_name) || isset($nearby_places) ){
                $query .= " AND ( ";
            }

            if(isset($city_name) && !empty($city_name)){
                $second = true;
                $city = strtolower($city_name);
                $query .= " (LOWER(CONVERT(j.`location` USING utf8mb4)) LIKE '%{$city}%') ";
            }

            if(isset($nearby_places)){
                if( isset($second) && $second ) {
                    $query .= " OR ";
                }
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = strtolower($place);
                    $query .= " (LOWER(CONVERT(j.`location` USING utf8mb4)) LIKE '%{$place}%')";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if( isset($city_name) || isset($nearby_places) ){
                $query .= " ) ";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'closeDate' => 'closed_at'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate'){
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'closeDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(j.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }
        }

        $query .= " UNION ";

        $query .= "SELECT COUNT(DISTINCT(`number`)) AS `count` FROM `external_jobs` xj 
                    WHERE xj.`status` = 'published' AND xj.`assigned` IS NULL AND xj.`assigned_to` IS NULL";

        if(!empty($search)) {
            $query .= " AND ( ";

            $length = count($search);
            foreach ($search as $key => $str) {
                $query .= "   LOWER(CONVERT(xj.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(xj.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(xj.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) ";
        }

        if(!empty(array_filter($filters))){
            extract($filters);
            $first = false;
            if(isset($category)){
                $query .= " AND (xj.`category` IN({$category}) OR xj.`sub_category` IN({$category}))";
            }

            if(isset($state)){
                $query .= " AND (xj.`state` IN({$state}))";
            }

            if(isset($states['ids']) && !empty($states['ids']) ){
                $states = implode(',', $states['ids']);
                $query .= " AND (xj.`state` IN({$states}))";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (xj.`id` IN(
                             SELECT `id` FROM `jobs` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if( isset($city_name) || isset($nearby_places) ){
                $query .= " AND ( ";
            }

            if(isset($city_name) && !empty($city_name)){
                $second = true;
                $city = strtolower($city_name);
                $query .= " (LOWER(CONVERT(xj.`location` USING utf8mb4)) LIKE '%{$city}%') ";
            }

            if(isset($nearby_places)){
                if( isset($second) && $second ) {
                    $query .= " OR ";
                }
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = strtolower($place);
                    $query .= " (LOWER(CONVERT(xj.`location` USING utf8mb4)) LIKE '%{$place}%')";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if( isset($city_name) || isset($nearby_places) ){
                $query .= " ) ";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'closeDate' => 'closed_at'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate'){
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'closeDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(xj.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }
        }
        $query .= ") AS `temp` LIMIT 1";

        return $this->db->getValue($query);
    }

    public function get_published_tasks($search = [], $filters = [], $offset = 0, $limit = 25){
        $user_id = $this->user->info['id'];
        $query = "SELECT DISTINCT t.`status` AS `task_status`, t.`id` AS `task_id`, t.`type` AS `task_type`, `created_at`, 'internal' AS `the_type`, t.`skills`, 1 AS `prioirty`";

        if( $this->user->info['preference']['dashboard_mode'] === 'search' ){
            if( $this->user->info['preference']['search_mode'] === 'match' ){
                $query .= ", CASE WHEN t.`id` IN( SELECT `id` FROM `tasks` WHERE ";
                $user_skills = $this->user->info['skills'];
                $user_skills = explode(',', $user_skills);
                $count = count($user_skills);
                foreach ($user_skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $count) $query .= " OR ";
                }

                $query .= " ) THEN 1 ELSE 0 END AS `matched` ";
            }
        }

        $query .=" FROM `tasks` t 
                    INNER JOIN `members` m ON m.`id` = t.`user_id`
                    WHERE t.`status` IN ('published', 'in-progress') AND DATE(t.`complete_by`) > DATE(NOW()) ";

        if(!empty($search)) {

            $query .= " AND ( ";

			$length = count($search);
            foreach ($search as $key => $str) {
                $query .= "   LOWER(CONVERT(t.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(t.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(t.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) "; /// closing for AND (

        }

        if(!empty(array_filter($filters))){
            extract($filters);

            if(isset($task_budget)){
                $query .= " AND (t.`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate') {
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'startDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(t.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (t.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if(isset($category)){
                $query .= " AND (t.`category` IN({$category}) OR t.`sub_category` IN({$category}))";
            }

            if(isset($task_type)){
                if(is_array($task_type))
                    $task_type = implode("','", $task_type);
                $task_type = "'{$task_type}'";

                $query .= " AND (t.`type` IN({$task_type}))";
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " AND ( ";
            }

            if(isset($state_name)){
                $first = true;
                if( is_array($state_name) ) {
                    $length = count($state_name);
                    foreach ($state_name as $key => $state) {
                        $state = mysqli_real_escape_string($this->db->connection, strtolower($state));
                        $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                        $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                        if ($key + 1 !== $length) $query .= " OR ";
                    }
                }else{
                    $state = mysqli_real_escape_string($this->db->connection, strtolower($state_name));
                    $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                }
            }

            if(isset($city_name) && !empty($city_name)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $city = mysqli_real_escape_string($this->db->connection, strtolower($city_name));
                $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$city}%' ";
                $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$city}%' ";
            }

            if(isset($states['names']) && !empty($states['names'])){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count( $states['names'] );
                foreach($states['names'] as $key => $state) {
                    $state = mysqli_real_escape_string($this->db->connection, strtolower($state));
                    $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if(isset($nearby_places)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = mysqli_real_escape_string($this->db->connection, strtolower($place));
                    $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$place}%' ";
                    $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$place}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
                $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE 'remotely' ";
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " ) ";
            }

            if(empty($search)) {
                if (!isset($state_name, $city_name, $states['names'], $nearby_places)) {
                    $query .= " OR ";
                } else {
                    $query .= " OR ";
                }

                $query .= " t.`id` IN( SELECT `id` FROM `tasks` WHERE `location` = 'remotely' AND `status` IN ('published', 'in-progress') AND DATE(`complete_by`) > DATE(NOW()) ";
                if (isset($remote_state)) {
                    $remote_state = mysqli_real_escape_string($this->db->connection, $remote_state);
                    $query .= " AND LOWER(CONVERT(`state_country` USING utf8mb4)) LIKE '%{$remote_state}%'";
                }

                if (isset($task_budget)) {
                    $query .= " AND (`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
                }

                if (isset($category)) {
                    $query .= " AND (`category` IN({$category}) OR `sub_category` IN({$category}))";
                }

                if(isset($task_type)){
                    $query .= " AND (`type` IN({$task_type}))";
                }

                if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                    $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                    $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                    if($date_period !== 'customrange') {
                        if($date_type === 'postedDate') {
                            $now  = Carbon::now()->toDateString();
                            $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                        }elseif($date_type === 'startDate') {
                            $date = Carbon::now()->toDateString();
                            $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                        }
                    }elseif(isset($from, $to)){
                        $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                        $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                    }

                    $query .= " AND (DATE(`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
                }

                if(isset($skills)){
                    if(!is_array($skills)) $skills = explode(',', $skills);

                    $query .= "   AND (t.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                    $length = count($skills);
                    foreach ($skills as $key => $skill){
                        $query .= " FIND_IN_SET({$skill}, `skills`) ";
                        if($key+1 !== $length) $query .= " OR ";
                    }

                    $query .= "   )) ";
                }

                $query .= " ) ";
            }
        }

        $query .= " UNION ";

        $query .= "SELECT DISTINCT xt.`status` AS `task_status`, xt.`id` AS `task_id`, xt.`type` AS `task_type`, `created_at`, 'external' AS `the_type`, xt.`skills`, 2 AS `prioirty`";
        if( $this->user->info['preference']['dashboard_mode'] === 'search' ){
            if( $this->user->info['preference']['search_mode'] === 'match' ){
                $query .= ", 0 AS `matched`";
            }
        }
        $query .= " FROM `external_tasks` xt
                    WHERE xt.`status` = 'published' AND DATE(xt.`complete_by`) > DATE(NOW()) ";


        if(!empty($search)) {

            $query .= " AND ( ";

            $length = count($search);
            foreach ($search as $key => $str) {
                $str = mysqli_real_escape_string($this->db->connection, $str);
                $query .= "   LOWER(CONVERT(xt.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(xt.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(xt.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) "; /// closing for AND (

        }

        if(!empty(array_filter($filters))){
            extract($filters);
            $first = false;
            if(isset($task_budget)){
                $query .= " AND (xt.`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate') {
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'startDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(xt.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (xt.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if(isset($category)){
                $query .= " AND (xt.`category` IN({$category}) OR xt.`sub_category` IN({$category}))";
            }

            if(isset($task_type)){
                if(is_array($task_type))
                    $task_type = implode("','", $task_type);
                $task_type = "'{$task_type}'";

                $query .= " AND (xt.`type` IN({$task_type}))";
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " AND ( ";
            }

            if(isset($state_name)){
                $first = true;
                if( is_array($state_name) ) {
                    $length = count($state_name);
                    foreach ($state_name as $key => $state) {
                        $state = mysqli_real_escape_string($this->db->connection, strtolower($state));
                        $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                        $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                        if ($key + 1 !== $length) $query .= " OR ";
                    }
                }else{
                    $state = mysqli_real_escape_string($this->db->connection, strtolower($state_name));
                    $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                }
            }

            if(isset($city_name) && !empty($city_name)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $city = mysqli_real_escape_string($this->db->connection, strtolower($city_name));
                $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$city}%' ";
                $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$city}%' ";
            }

            if(isset($states['names']) && !empty($states['names'])){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count( $states['names'] );
                foreach($states['names'] as $key => $state) {
                    $state = mysqli_real_escape_string($this->db->connection, strtolower($state));
                    $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if(isset($nearby_places)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = mysqli_real_escape_string($this->db->connection, strtolower($place));
                    $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$place}%' ";
                    $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$place}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
                $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE 'remotely' ";
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " ) ";
            }

            if(empty($search)) {
                if (!isset($state_name, $city_name, $states['names'], $nearby_places)) {
                    $query .= " OR ";
                } else {
                    $query .= " OR ";
                }

                $query .= " xt.`id` IN( SELECT `id` FROM `tasks` WHERE `location` = 'remotely' AND `status` IN ('published', 'in-progress') AND DATE(`complete_by`) > DATE(NOW()) ";
                if (isset($remote_state)) {
                    $remote_state = mysqli_real_escape_string($this->db->connection, $remote_state);
                    $query .= " AND LOWER(CONVERT(`state_country` USING utf8mb4)) LIKE '%{$remote_state}%'";
                }

                if (isset($task_budget)) {
                    $query .= " AND (`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
                }

                if (isset($category)) {
                    $query .= " AND (`category` IN({$category}) OR `sub_category` IN({$category}))";
                }

                if(isset($task_type)){
                    $query .= " AND (`type` IN({$task_type}))";
                }

                if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                    $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                    $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                    if($date_period !== 'customrange') {
                        if($date_type === 'postedDate') {
                            $now  = Carbon::now()->toDateString();
                            $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                        }elseif($date_type === 'startDate') {
                            $date = Carbon::now()->toDateString();
                            $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                        }
                    }elseif(isset($from, $to)){
                        $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                        $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                    }

                    $query .= " AND (DATE(`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
                }

                if(isset($skills)){
                    if(!is_array($skills)) $skills = explode(',', $skills);

                    $query .= "   AND (xt.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                    $length = count($skills);
                    foreach ($skills as $key => $skill){
                        $query .= " FIND_IN_SET({$skill}, `skills`) ";
                        if($key+1 !== $length) $query .= " OR ";
                    }

                    $query .= "   )) ";
                }

                $query .= " ) ";
            }
        }

        $query .= " GROUP BY `id` ORDER BY `prioirty` ASC, ";
        if( $this->user->info['preference']['dashboard_mode'] === 'search' ){
            if( $this->user->info['preference']['search_mode'] === 'match' ){
                $query .= " `matched` DESC, ";
            }
        }
        $query .= "`created_at` DESC
                          LIMIT {$offset}, {$limit}";

        $this->db->query($query);
        return $this->db->getRowList();
    }

    public function get_published_tasks_jobs($search = [], $filters = [], $offset = 0, $limit = 25){
        $user_id = $this->user->info['id'];
        $query = "(SELECT 'internal_task' AS `the_type`, `user_id`, t.`id` AS `task_id`, `created_at`, 1 AS `prioirty`
                   FROM `tasks` t 
                   INNER JOIN `members` m ON m.`id` = t.`user_id`
                   WHERE t.`status` IN ('published', 'in-progress')  AND DATE(t.`complete_by`) > DATE(NOW()) ";

        if(!empty($search)) {
            $query .= " AND ( ";
            $length = count($search);
            foreach ($search as $key => $str) {
                $query .= "   LOWER(CONVERT(t.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(t.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(t.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) "; /// closing for AND (

        }

        if(!empty(array_filter($filters))){
            extract($filters);

            if(isset($task_budget)){
                $query .= " AND (t.`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate') {
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'startDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(t.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (t.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if(isset($category)){
                $query .= " AND (t.`category` IN({$category}) OR t.`sub_category` IN({$category}))";
            }

            if(isset($task_type)){
                if(is_array($task_type))
                    $task_type = implode("','", $task_type);
                $task_type = "'{$task_type}'";

                $query .= " AND (t.`type` IN({$task_type}))";
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " AND ( ";
            }

            if(isset($state_name)){
                $first = true;
                if( is_array($state_name) ) {
                    $length = count($state_name);
                    foreach ($state_name as $key => $state) {
                        $state = mysqli_real_escape_string($this->db->connection, strtolower($state));
                        $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                        $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                        if ($key + 1 !== $length) $query .= " OR ";
                    }
                }else{
                    $state = mysqli_real_escape_string($this->db->connection, strtolower($state_name));
                    $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                }
            }

            if(isset($city_name) && !empty($city_name)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $city = mysqli_real_escape_string($this->db->connection, strtolower($city_name));
                $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$city}%' ";
                $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$city}%' ";
            }

            if(isset($states['names']) && !empty($states['names'])){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count( $states['names'] );
                foreach($states['names'] as $key => $state) {
                    $state = strtolower($state);
                    $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if(isset($nearby_places)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = strtolower($place);
                    $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$place}%' ";
                    $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$place}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " ) ";
            }

            if(empty($search)) {
                if (!isset($state_name, $city_name, $states['names'], $nearby_places)) {
                    $query .= " OR ";
                } else {
                    $query .= " OR ";
                }

                $query .= " t.`id` IN( SELECT `id` FROM `tasks` WHERE `location` = 'remotely' AND `status` IN ('published', 'in-progress') AND DATE(`complete_by`) > DATE(NOW()) ";
                if (isset($remote_state)) {
                    $query .= " AND LOWER(CONVERT(`state_country` USING utf8mb4)) LIKE '%{$remote_state}%'";
                }

                if (isset($task_budget)) {
                    $query .= " AND (`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
                }

                if (isset($category)) {
                    $query .= " AND (`category` IN({$category}) OR `sub_category` IN({$category}))";
                }

                if(isset($task_type)){
                    $query .= " AND (`type` IN({$task_type}))";
                }

                if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                    $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                    $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                    if($date_period !== 'customrange') {
                        if($date_type === 'postedDate') {
                            $now  = Carbon::now()->toDateString();
                            $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                        }elseif($date_type === 'startDate') {
                            $date = Carbon::now()->toDateString();
                            $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                        }
                    }elseif(isset($from, $to)){
                        $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                        $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                    }

                    $query .= " AND (DATE(`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
                }

                if(isset($skills)){
                    if(!is_array($skills)) $skills = explode(',', $skills);

                    $query .= "   AND (t.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                    $length = count($skills);
                    foreach ($skills as $key => $skill){
                        $query .= " FIND_IN_SET({$skill}, `skills`) ";
                        if($key+1 !== $length) $query .= " OR ";
                    }

                    $query .= "   )) ";
                }

                $query .= " ) ";
            }
        }



        $query .= " ) UNION ( ";
        $query .= " SELECT 'external_task' AS `the_type`, `user_id`, xt.`id` AS `task_id`, `created_at`, 2 AS `prioirty`
                    FROM `external_tasks` xt 
                    WHERE xt.`status` = 'published' AND DATE(xt.`complete_by`) > DATE(NOW()) ";

        if(!empty($search)) {
            $query .= " AND ( ";
            $length = count($search);
            foreach ($search as $key => $str) {
                $query .= "   LOWER(CONVERT(xt.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(xt.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(xt.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) "; /// closing for AND (

        }

        if(!empty(array_filter($filters))){
            extract($filters);
            $first = false;
            if(isset($task_budget)){
                $query .= " AND (xt.`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate') {
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'startDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(xt.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (xt.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if(isset($category)){
                $query .= " AND (xt.`category` IN({$category}) OR xt.`sub_category` IN({$category}))";
            }

            if(isset($task_type)){
                if(is_array($task_type))
                    $task_type = implode("','", $task_type);
                $task_type = "'{$task_type}'";

                $query .= " AND (xt.`type` IN({$task_type}))";
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " AND ( ";
            }

            if(isset($state_name)){
                $first = true;
                if( is_array($state_name) ) {
                    $length = count($state_name);
                    foreach ($state_name as $key => $state) {
                        $state = mysqli_real_escape_string($this->db->connection, strtolower($state));
                        $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                        $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                        if ($key + 1 !== $length) $query .= " OR ";
                    }
                }else{
                    $state = mysqli_real_escape_string($this->db->connection, strtolower($state_name));
                    $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                }
            }

            if(isset($city_name) && !empty($city_name)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $city = mysqli_real_escape_string($this->db->connection, strtolower($city_name));
                $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$city}%' ";
                $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$city}%' ";
            }

            if(isset($states['names']) && !empty($states['names'])){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count( $states['names'] );
                foreach($states['names'] as $key => $state) {
                    $state = strtolower($state);
                    $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if(isset($nearby_places)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = strtolower($place);
                    $query .= " LOWER(CONVERT(xt.`state_country` USING utf8mb4)) LIKE '%{$place}%' ";
                    $query .= " OR LOWER(CONVERT(xt.`location` USING utf8mb4)) LIKE '%{$place}%' ";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if( isset($state_name) || isset($city_name) || isset($states['names']) || isset($nearby_places) ){
                $query .= " ) ";
            }

            if(empty($search)) {
                if (!isset($state_name, $city_name, $states['names'], $nearby_places)) {
                    $query .= " OR ";
                } else {
                    $query .= " OR ";
                }

                $query .= " xt.`id` IN( SELECT `id` FROM `tasks` WHERE `location` = 'remotely' AND `status` IN ('published', 'in-progress') AND DATE(`complete_by`) > DATE(NOW()) ";
                if (isset($remote_state)) {
                    $query .= " AND LOWER(CONVERT(`state_country` USING utf8mb4)) LIKE '%{$remote_state}%'";
                }

                if (isset($task_budget)) {
                    $query .= " AND (`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
                }

                if (isset($category)) {
                    $query .= " AND (`category` IN({$category}) OR `sub_category` IN({$category}))";
                }

                if(isset($task_type)){
                    $query .= " AND (`type` IN({$task_type}))";
                }

                if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                    $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                    $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                    if($date_period !== 'customrange') {
                        if($date_type === 'postedDate') {
                            $now  = Carbon::now()->toDateString();
                            $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                        }elseif($date_type === 'startDate') {
                            $date = Carbon::now()->toDateString();
                            $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                        }
                    }elseif(isset($from, $to)){
                        $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                        $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                    }

                    $query .= " AND (DATE(`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
                }

                if(isset($skills)){
                    if(!is_array($skills)) $skills = explode(',', $skills);

                    $query .= "   AND (xt.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                    $length = count($skills);
                    foreach ($skills as $key => $skill){
                        $query .= " FIND_IN_SET({$skill}, `skills`) ";
                        if($key+1 !== $length) $query .= " OR ";
                    }

                    $query .= "   )) ";
                }

                $query .= " ) ";
            }
        }

        $query .= " ) UNION ( ";
        $query .= " SELECT 'internal_job' AS `the_type`, `user_id`, j.`id` AS `task_id`, `created_at`, 1 AS `prioirty`
                    FROM `jobs` j 
                    INNER JOIN `members` m ON m.`id` = j.`user_id`
                    WHERE j.`status` = 'published' AND j.`assigned` IS NULL AND j.`assigned_to` IS NULL 
                    ";

        if(!empty($search)) {
            $query .= " AND ( ";

            $length = count($search);
            foreach ($search as $key => $str) {
                $query .= "   LOWER(CONVERT(j.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(j.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(j.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) ";
        }

        if(!empty(array_filter($filters))){
            extract($filters);
            $first = false;
            if(isset($category)){
                $query .= " AND (j.`category` IN({$category}) OR j.`sub_category` IN({$category}))";
            }

            if(isset($state)){
                $query .= " AND (j.`state` IN({$state}))";
            }

            if(isset($states['ids']) && !empty($states['ids']) ){
                $states = implode(',', $states['ids']);
                $query .= " AND (j.`state` IN({$states}))";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (j.`id` IN(
                             SELECT `id` FROM `jobs` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if( isset($city_name) || isset($nearby_places) ){
                $query .= " AND ( ";
            }

            if(isset($city_name) && !empty($city_name)){
                $second = true;
                $city = strtolower($city_name);
                $query .= " (LOWER(CONVERT(j.`location` USING utf8mb4)) LIKE '%{$city}%') ";
            }

            if(isset($nearby_places)){
                if( isset($second) && $second ) {
                    $query .= " OR ";
                }
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = strtolower($place);
                    $query .= " (LOWER(CONVERT(j.`location` USING utf8mb4)) LIKE '%{$place}%')";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if( isset($city_name) || isset($nearby_places) ){
                $query .= " ) ";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'closeDate' => 'closed_at'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate'){
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'closeDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(j.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }
        }

        $query .= " ) UNION ( ";
        $query .= " SELECT DISTINCT 'external_job' AS `the_type`, `user_id`, xj.`id` AS `job_id`, `created_at`, 3 AS `prioirty`
                    FROM `external_jobs` xj  
                    WHERE xj.`status` = 'published' AND xj.`assigned` IS NULL AND xj.`assigned_to` IS NULL
                    ";

        if(!empty($search)) {
            $query .= " AND ( ";

            $length = count($search);
            foreach ($search as $key => $str) {
                $query .= "   LOWER(CONVERT(xj.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(xj.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(xj.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length) $query .= " OR ";
            }
            $query .= " ) ";
        }

        if(!empty(array_filter($filters))){
            extract($filters);
            $first = false;
            if(isset($category)){
                $query .= " AND (xj.`category` IN({$category}) OR xj.`sub_category` IN({$category}))";
            }

            if(isset($state)){
                $query .= " AND (xj.`state` IN({$state}))";
            }

            if(isset($states['ids']) && !empty($states['ids']) ){
                $states = implode(',', $states['ids']);
                $query .= " AND (xj.`state` IN({$states}))";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (xj.`id` IN(
                             SELECT `id` FROM `jobs` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if( isset($city_name) || isset($nearby_places) ){
                $query .= " AND ( ";
            }

            if(isset($city_name) && !empty($city_name)){
                $second = true;
                $city = strtolower($city_name);
                $query .= " (LOWER(CONVERT(xj.`location` USING utf8mb4)) LIKE '%{$city}%') ";
            }

            if(isset($nearby_places)){
                if( isset($second) && $second ) {
                    $query .= " OR ";
                }
                $length = count($nearby_places);
                foreach ($nearby_places as $key => $place){
                    $place = strtolower($place);
                    $query .= " (LOWER(CONVERT(xj.`location` USING utf8mb4)) LIKE '%{$place}%')";
                    if($key+1 !== $length) $query .= " OR ";
                }
            }

            if( isset($city_name) || isset($nearby_places) ){
                $query .= " ) ";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'closeDate' => 'closed_at'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    if($date_type === 'postedDate'){
                        $now  = Carbon::now()->toDateString();
                        $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                    }elseif($date_type === 'closeDate') {
                        $date = Carbon::now()->toDateString();
                        $now  = Carbon::now()->copy()->modify("+{$period[$date_period]}")->toDateString();
                    }
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(xj.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }
        }

        $query .= " GROUP BY xj.`number` ) 
                    ORDER BY `prioirty` ASC, `created_at` DESC 
                    LIMIT {$offset}, {$limit}";

        $this->db->query($query);
        return $this->db->getRowList();
    }

    public function get_my_published_tasks_jobs($user_id = null, $search = [], $filters = [], $offset = 0, $limit = 25){
        $user_id = !is_null($user_id) ? $user_id : $this->user->info['id'];
        $query = "(SELECT 'task' AS `the_type`, `user_id`, t.`id` AS `task_id`, `created_at`
                   FROM `tasks` t 
                   INNER JOIN `members` m ON m.`id` = t.`user_id`
                   WHERE t.`user_id` = '{$user_id}'";

        if( $user_id !== $this->user->info['id'] ){
            $query .= " AND t.`status` IN('published', 'in-progress') ";
        }

        if(!empty($search)) {
            /*$query .= "   AND (t.`id` IN(
                             SELECT `task_id` FROM `task_tags` WHERE
                             ";

            $length = count($search);

            foreach ($search as $key => $str){
                $query .= "   LOWER(CONVERT(`tag` USING utf8mb4)) LIKE '%{$str}%'";
                if($key+1 !== $length){
                    $query .= "   OR 
                          ";
                }
            }

            $query .= "   )) ";*/

                $query .= " AND ( ";
            $length = count($search);
            foreach ($search as $key => $str) {
                $query .= "   LOWER(CONVERT(t.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(t.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(t.`number` USING utf8mb4)) LIKE '%{$str}%' ";
                if($key+1 !== $length){
                    $query .= " OR ";
                }
            }
            $query .= " ) ";
        }

        if(!empty(array_filter($filters))){
            extract($filters);

            if(isset($task_budget)){
                $query .= " AND (t.`budget` BETWEEN {$task_budget[0]} AND {$task_budget[1]})";
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'startDate' => 'start_by'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    $now  = Carbon::now()->toDateString();
                    $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(t.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (t.`id` IN(
                             SELECT `id` FROM `tasks` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if(isset($category)){
                $query .= " AND (t.`category` IN({$category}) OR t.`sub_category` IN({$category}))";
            }

            if(isset($task_type)){
                if(is_array($task_type))
                    $task_type = implode("','", $task_type);
                $task_type = "'{$task_type}'";

                $query .= " AND (t.`type` IN({$task_type}))";
            }

            if(isset($state_name)){
                $first = true;
                if( is_array($state_name) ) {
                    $length = count($state_name);
                    foreach ($state_name as $key => $state) {
                        $state = mysqli_real_escape_string($this->db->connection, strtolower($state));
                        $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                        $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                        if ($key + 1 !== $length) $query .= " OR ";
                    }
                }else{
                    $state = mysqli_real_escape_string($this->db->connection, strtolower($state_name));
                    $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%' ";
                }
            }

            if(isset($city_name) && !empty($city_name)){
                if( isset($first) && $first ) {
                    $query .= " OR ";
                }else{
                    $first = true;
                }
                $city = mysqli_real_escape_string($this->db->connection, strtolower($city_name));
                $query .= " LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$city}%' ";
                $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$city}%' ";
            }

            if(isset($states['names']) && !empty($states['names'])){
                foreach($states['names'] as $state) {
                    $state = strtolower($state);
                    $query .= " OR (LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$state}%' ";
                    $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$state}%')";
                }
            }

            if(isset($nearby_places)){
                foreach ($nearby_places as $places){
                    foreach($places as $place){
                        $place = trim(strtolower($place));
                        $query .= " OR (LOWER(CONVERT(t.`state_country` USING utf8mb4)) LIKE '%{$place}%' ";
                        $query .= " OR LOWER(CONVERT(t.`location` USING utf8mb4)) LIKE '%{$place}%')";
                    }
                }
            }
        }
        $query .= " ) UNION ( ";
        $query .= " SELECT 'job' AS `the_type`, `user_id`, j.`id` AS `task_id`, `created_at`
                    FROM `jobs` j 
                    INNER JOIN `members` m ON m.`id` = j.`user_id`
                    WHERE j.`user_id` = '{$user_id}' 
                    ";

        if( $user_id !== $this->user->info['id'] ){
            $query .= " AND j.`status` = 'published' ";
        }

        if(!empty($search)) {
            if(!empty(array_filter($filters))){
                $query .= " AND ( ";
            }else{
                $query .= " OR ( ";
            }

            $length = count($search);

            foreach ($search as $key => $str) {
                $query .= "   LOWER(CONVERT(j.`title` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(j.`description` USING utf8mb4)) LIKE '%{$str}%' 
                              OR LOWER(CONVERT(j.`number` USING utf8mb4)) LIKE '%{$str}%' ";

                if($key+1 !== $length){
                    $query .= " OR ";
                }
            }

            $query .= " ) ";
        }

        if(!empty(array_filter($filters))){
            extract($filters);
            $first = false;
            if(isset($category)){
                $query .= " AND (j.`category` IN({$category}) OR j.`sub_category` IN({$category}))";
            }

            if(isset($state)){
                $query .= " AND (m.`state` IN({$state}))";
            }

            if(isset($states['ids']) && !empty($states['ids'])){
                $states = implode(',', $states['ids']);
                $query .= " AND (m.`state` IN({$states}))";
            }

            if(isset($state_name) && !empty($state_name)){
                $length = count( $state_name );
                foreach($state_name as $key => $state) {
                    $state = mysqli_real_escape_string($this->db->connection, strtolower($state));
                    $query .= " OR (LOWER(CONVERT(j.`location` USING utf8mb4)) LIKE '%{$state}%') ";
                    //if($key+1 !== $length) $query .= " OR ";
                }
            }

            if(isset($city_name) && !empty($city_name)){
                $city = strtolower($city_name);
                $query .= " OR (LOWER(CONVERT(j.`location` USING utf8mb4)) LIKE '%{$city}%') ";
            }

            if(isset($skills)){
                if(!is_array($skills)) $skills = explode(',', $skills);

                $query .= "   AND (j.`id` IN(
                             SELECT `id` FROM `jobs` WHERE ";

                $length = count($skills);
                foreach ($skills as $key => $skill){
                    $query .= " FIND_IN_SET({$skill}, `skills`) ";
                    if($key+1 !== $length) $query .= " OR ";
                }

                $query .= "   )) ";
            }

            if(isset($nearby_places)){
                foreach ($nearby_places as $places){
                    foreach($places as $place){
                        $place = trim(strtolower($place));
                        $query .= " OR (LOWER(CONVERT(j.`location` USING utf8mb4)) LIKE '%{$place}%')";
                    }
                }
            }

            if(isset($date_type) && isset($date_period) && $date_period !== 'any'){
                $type = ['postedDate' => 'created_at', 'startDate' => 'created_at'];
                $period = ['24h' => '24 hours', '3d' => '3 days', '7d' => '7 days', '14d' => '14 days', '30d' => '30 days'];

                if($date_period !== 'customrange') {
                    $now  = Carbon::now()->toDateString();
                    $date = Carbon::now()->copy()->modify("-{$period[$date_period]}")->toDateString();
                }elseif(isset($from, $to)){
                    $date = Carbon::parse(str_replace('/', '-', $from))->toDateString();
                    $now = Carbon::parse(str_replace('/', '-', $to))->toDateString();
                }

                $query .= " AND (DATE(j.`{$type[$date_type]}`) BETWEEN '{$date}' AND '{$now}')";
            }
        }

        $query .= " ) 
                    ORDER BY `created_at` DESC 
                    LIMIT {$offset}, {$limit}";

        $this->db->query($query);
        return $this->db->getRowList();
    }

    public function get_completed_tasks($offset = 0, $limit = 25){
        $user_id = $this->user->info['id'];
        $this->db->query("
        SELECT t.*, m.*, `t`.`status` AS `task_status`, t.`id` AS `project_id` FROM `tasks` t 
        INNER JOIN `user_task` u ON u.`user_id` = t.`user_id`
        INNER JOIN `members` m ON m.`id` = t.`assigned_to`
        WHERE t.`status` = 'completed' AND t.`user_id` IN('{$user_id}') AND t.`assigned` IS NOT NULL
        ORDER BY t.`created_at` DESC 
        LIMIT {$offset}, {$limit}");

        return $this->db->getRowList();
    }

    public function get_my_posted_and_performed_tasks(){
        $user_id = $this->user->info['id'];
        $this->db->query("
        SELECT DISTINCT t.`id`, t.`title`, t.`user_id`, ps.`seeker_id` AS `assigned_to`, CONCAT(m.`firstname`, ' ', m.`lastname`) AS `seeker_name` 
        FROM `tasks` t 
        RIGHT JOIN `poster_seeker` ps ON ps.`poster_id` = '{$user_id}' OR ps.`seeker_id` = '{$user_id}'
        RIGHT JOIN `members` m ON m.`id` = ps.`seeker_id`
        WHERE t.`status` IN ('in-progress', 'completed', 'disputed', 'closed') 
        AND ( t.`user_id` IN('{$user_id}') OR t.`id` IN( SELECT `task_id` FROM `poster_seeker` WHERE `seeker_id` = '{$user_id}' AND `status` IN('rate','closed') )  )
        ORDER BY t.`created_at` DESC");

        return $this->db->getRowList();
    }

    public function get_my_posted_tasks_ids(){
        $user_id = $this->user->info['id'];
        $this->db->query("
        SELECT
            DISTINCT t.`id`,
            t.`title`,
            ps.`seeker_id` AS `assigned_to`,
            CONCAT(m.`firstname`, ' ', m.`lastname`) AS `seeker_name`
        FROM
            `tasks` t
            LEFT JOIN `poster_seeker` ps ON ps.`task_id` = t.`id` AND ps.`poster_id` = t.`user_id`
            LEFT JOIN `members` m ON ps.`seeker_id` = m.`id`
        WHERE
            t.`status` IN ('in-progress', 'completed', 'disputed', 'closed')
            AND t.`user_id` IN('{$user_id}')
            AND ps.`status` IN('rate','closed','disputed')
        ORDER BY
            t.`created_at` DESC");

        return $this->db->getRowList();
    }

    public function get_my_performed_tasks_ids(){
        $user_id = $this->user->info['id'];
        $this->db->query("
        SELECT ps.`task_id` AS `id`, t.`title`, t.`user_id`
        FROM `poster_seeker` ps
        LEFT JOIN `tasks` t ON t.`id` = ps.`task_id`
        WHERE ps.`seeker_id` IN('{$user_id}') AND ps.`status` IN('rate', 'closed', 'disputed')
        ORDER BY ps.`created_at` DESC");

        return $this->db->getRowList();
    }

    public function get_favourites($user_id = null, $offset = 0, $limit = 25){
        $this->is_logged_in();
        if(null === $user_id) return [];

        $this->db->query("SELECT * FROM `favourites` t WHERE t.`user_id` = '{$user_id}' AND `type` = 'task'");
        return $this->db->getRowList();
    }

    public function get_applicants($tasks = [], $offset = 0, $limit = 10){
        if( empty($tasks) ) return [];
        $ids = "'" . implode("','", $tasks) . "'";

        $this->db->query("SELECT DISTINCT m.*, u.*,
                            ( SELECT t.`status` FROM `tasks` t WHERE t.`id` = u.`task_id` LIMIT 1 ) AS `task_status`,
                            ( SELECT t.`complete_by` FROM `tasks` t WHERE t.`id` = u.`task_id` LIMIT 1 ) AS `task_end`,
                            ( SELECT COUNT(t.`id`) FROM `tasks` t
                                WHERE
                                    t.`id` IN ( SELECT `task_id` FROM `poster_seeker` WHERE `seeker_id` = m.`id` AND `status` IN('closed', 'rate', 'disputed') )
                                    AND t.`status` IN('closed', 'completed', 'disputed')
                            ) AS `completed`,
                            ( SELECT COUNT(t.`id`) FROM `tasks` t
                                WHERE
                                    t.`id` IN ( SELECT `task_id` FROM `poster_seeker` WHERE `seeker_id` = m.`id` AND `status` IN('in-progress', 'completed', 'waiting') )
                                    AND t.`status` IN('in-progress', 'accepted', 'rejected')
                            ) AS `on_going`,
                            COALESCE(
                                ( SELECT SUM(t.`hours`) FROM `tasks` t
                                    WHERE
                                        t.`id` IN ( SELECT `task_id` FROM `poster_seeker` WHERE `seeker_id` = m.`id` AND `status` IN('closed', 'rate', 'disputed') )
                                        AND t.`status` IN('closed', 'completed', 'disputed')
                                        AND t.`type` = 'by-hour'
                                ),
                                0
                            ) AS `hours_completed`,
                            COALESCE(
                                ( SELECT SUM( TIMESTAMPDIFF(HOUR, t.`assigned`, t.`updated_at`) ) FROM `tasks` t
                                    WHERE
                                        t.`id` IN ( SELECT `task_id` FROM `poster_seeker` WHERE `seeker_id` = m.`id` AND `status` IN('closed', 'rate', 'disputed') )
                                        AND t.`updated_at` IS NOT NULL
                                        AND t.`type` = 'lump-sum'
                                ),
                                0
                            ) AS `lumpsum_hours`,
                            u.`created_at` AS `applied_at`,
                            CASE
                                WHEN p.`pinned` = '1' THEN 4
                                WHEN u.`pinned` = '1' THEN 3
                                WHEN p.`status` IS NOT NULL THEN 2
                                ELSE 1
                            END AS `priority`
                        FROM
                            `members` m
                            INNER JOIN `user_task` u ON m.`id` = u.`user_id`
                            LEFT JOIN `poster_seeker` p ON m.`id` = p.`seeker_id`
                        WHERE u.`task_id` IN ({$ids})
                        GROUP BY u.`user_id`, u.`task_id`
                        ORDER BY u.`pinned` DESC, `priority` DESC, u.`created_at` DESC
                          LIMIT {$offset}, {$limit}");

        return $this->db->getRowList();
    }

    public function get_tasks_picked_applicants($user_id = null){
        if( is_null($user_id) ) $user_id = $this->user->info['id'];

        $this->db->query("SELECT p.*, m.`firstname`, m.`lastname`, m.`photo` FROM `poster_seeker` p
                          LEFT JOIN `members` m ON m.`id` = p.`seeker_id`  
                          WHERE p.`poster_id` = '{$user_id}'");
        return $this->db->getRowList();
    }

    public function get_tasks_milestone_action($task_ids = []){
        if( empty($task_ids) ) return [];
        if( ! is_array($task_ids) ) $task_ids = (array) $task_ids;
        $ids = implode("','", $task_ids);
        $this->db->query("SELECT ms.* FROM `milestone_action` ms
                          WHERE ms.`task_id` IN ('{$ids}') AND ms.`performed` = '0'");
        return $this->db->getRowList();
    }

    public function get_cancelled_milestone_action($task_id = null, $user_id = null){
        if( is_null($task_id) || is_null($user_id) ) return [];

        $this->db->query("SELECT ms.* FROM `milestone_action` ms
                          WHERE ms.`task_id` = '{$task_id}' AND ms.`required_action` = 'cancelled' 
                          AND (ms.`by_id` = '{$user_id}' OR ms.`for_id` = '{$user_id}')");

        return $this->db->getSingleRow();
    }

    public function get_declined_milestone_action($task_id = null, $user_id = null){
        if( is_null($task_id) || is_null($user_id) ) return [];

        $this->db->query("SELECT ms.* FROM `milestone_action` ms
                          WHERE ms.`task_id` = '{$task_id}' AND ms.`required_action` = 'declined' 
                          AND (ms.`by_id` = '{$user_id}' OR ms.`for_id` = '{$user_id}')");

        return $this->db->getSingleRow();
    }

    public function get_disputed_milestone_action($task_id = null, $user_id = null){
        if( is_null($task_id) || is_null($user_id) ) return [];

        $this->db->query("SELECT ms.* FROM `milestone_action` ms
                          WHERE ms.`task_id` = '{$task_id}' AND ms.`required_action` = 'disputed' 
                          AND (ms.`by_id` = '{$user_id}' OR ms.`for_id` = '{$user_id}')");

        return $this->db->getSingleRow();
    }

    public function get_task_picked_applicants($user_id = null, $task_id = null){
        if( is_null($user_id) ) $user_id = $this->user->info['id'];
        if( is_null($task_id) ) return [];

        $this->db->query("SELECT p.*, m.`firstname`, m.`lastname`, m.`photo` FROM `poster_seeker` p
                          LEFT JOIN `members` m ON m.`id` = p.`seeker_id`  
                          WHERE p.`poster_id` = '{$user_id}' AND p.`task_id` = '{$task_id}'");
        return $this->db->getRowList();
    }

    public function get_tasks_payment($task_ids = []){
        if( empty($task_ids) ) return [];
        if( ! is_array($task_ids) ) $task_ids = (array) $task_ids;
        $ids = implode("','", $task_ids);
        $user_id = $this->user->info['id'];

        $this->db->query("SELECT tp.* FROM `task_payment` tp
                          WHERE tp.`task_id` IN ('{$ids}') AND tp.`user_id` = '{$user_id}' AND tp.`status` != '0'
                          GROUP BY tp.`user_id`, tp.`task_id`, tp.`assigned_to`
                          ORDER BY tp.`date` DESC");
        return $this->db->getRowList();
    }

    public function get_total_applicants($task = null){
        if( is_null($task) ) return [];

        $this->db->query("SELECT COUNT(u.`id`) AS `count`, u.`task_id`
                          FROM `user_task` u
                          WHERE u.`task_id` IN ('{$task}') AND u.`user_id` IN ( SELECT `id` FROM `members` )");

        return $this->db->getSingleRow();
    }

    public function get_tasks_comment($tasks = []){
        if( empty($tasks) ) return [];

        if( ! is_array($tasks) ) (array) $tasks;

        $tasks = implode("','", $tasks);

        $query = "SELECT *, c.`id` AS `comment_id`, c.`viewed` FROM `comments` c 
                  WHERE c.`post_id` IN ('{$tasks}') AND c.`parent_id` = '0'
                  ORDER BY c.`created_at` DESC";

        $this->db->query($query);
        return $this->db->getRowList();
    }

    public function owner($user_id = null, $task_id = null){
        if( is_null($task_id) || is_null($user_id) ) return false;

        $this->db->query("SELECT COUNT(`id`)
                          FROM `tasks` t
                          WHERE t.`id` = '{$task_id}' AND t.`user_id` = '{$user_id}' LIMIT 1");

        return (int)$this->db->getValue() !== 0;
    }

    public function is_draft($task_id = null, $user_id = null){
        if( is_null($task_id) ) return false;
        if( is_null($user_id) ) $user_id = $this->user->info['id'];

        $this->db->query("SELECT `status`
                          FROM `tasks` t
                          WHERE t.`id` = '{$task_id}' AND t.`user_id` = '{$user_id}' LIMIT 1");

        return $this->db->getValue() === 'draft';
    }

    public function is_published($task_id = null, $user_id = null){
        if( is_null($task_id) ) return false;
        if( is_null($user_id) ) $user_id = $this->user->info['id'];

        $this->db->query("SELECT `status`
                          FROM `tasks` t
                          WHERE t.`id` = '{$task_id}' AND t.`user_id` = '{$user_id}' LIMIT 1");

        return $this->db->getValue() === 'published';
    }

    public function is_inProgress($task_id = null, $user_id = null){
        if( is_null($task_id) || is_null($user_id) ) return false;
        $poster = $this->user->info['id'];

        $this->db->query("SELECT `status`
                          FROM `poster_seeker` t
                          WHERE t.`task_id` = '{$task_id}' AND t.`seeker_id` = '{$user_id}' AND t.`poster_id` = '{$poster}' LIMIT 1");

        return $this->db->getValue() === 'in-progress';
    }

    public function is_task_inProgress($task_id = null){
        if( is_null($task_id) ) return false;

        $this->db->query("SELECT `status`
                          FROM `tasks` t
                          WHERE t.`id` = '{$task_id}' LIMIT 1");

        return $this->db->getValue() === 'in-progress';
    }

    public function is_completed($task_id = null, $user_id = null){
        if( is_null($task_id) ) return false;
        if( is_null($user_id) ) $user_id = $this->user->info['id'];

        $this->db->query("SELECT `status`
                          FROM `tasks` t
                          WHERE t.`id` = '{$task_id}' AND t.`user_id` = '{$user_id}' LIMIT 1");

        return $this->db->getValue() === 'completed';
    }

    public function is_accepted($task_id = null, $user_id = null){
        if( is_null($task_id) ) return false;
        if( is_null($user_id) ) $user_id = $this->user->info['id'];

        $this->db->query("SELECT `status`
                          FROM `tasks` t
                          WHERE t.`id` = '{$task_id}' AND t.`user_id` = '{$user_id}' LIMIT 1");

        return $this->db->getValue() === 'accepted';
    }

    public function is_favourited($task_id = null, $user_id = null){
        if( is_null($task_id) ) return false;
        if( is_null($user_id) ) $user_id = $this->user->info['id'];

        $this->db->query("SELECT COUNT(*)
                          FROM `favourites` f
                          WHERE f.`task_id` = '{$task_id}' AND f.`user_id` = '{$user_id}' AND `type` = 'task'");

        return $this->db->getValue() > 0;
    }

    public function rejected($task_id = null){
        if( is_null($task_id) ) return false;

        $this->db->query("SELECT ta.*
                          FROM `task_activity` ta 
                          INNER JOIN `tasks` t ON t.`id` = ta.`task_id` AND ta.`created_at` >= t.`updated_at` AND t.`status` = 'in-progress'
                          WHERE ta.`event` = 'rejected' AND ta.`task_id` = '{$task_id}'
                          ORDER BY ta.`created_at` DESC LIMIT 1");

        return $this->db->getSingleRow();
    }

    public function is_status($task_id = null, $user_id = null, $poster_id = null, $status = null, $user_type = 'poster'){
        if( is_null($task_id) || is_null($status) || is_null($user_id) ) return false;

        if( $user_type === 'poster') {
            $poster_id = $poster_id ?? $this->user->info['id'];
            $this->db->query("SELECT `status`
                          FROM `poster_seeker` t
                          WHERE t.`task_id` = '{$task_id}' AND t.`seeker_id` = '{$user_id}' AND `poster_id` = '{$poster_id}' LIMIT 1");
        }else{
            $this->db->query("SELECT `status`
                          FROM `user_task` t
                          WHERE t.`task_id` = '{$task_id}' AND t.`user_id` = '{$user_id}' AND `type` = 'task' LIMIT 1");
        }

        return $this->db->getValue() === $status;
    }

    public function get_seeker_task_status($task_id = null, $user_id = null){
        if( is_null($task_id) || is_null($user_id) ) return '';

        $this->db->query("SELECT `status`
                      FROM `user_task` t
                      WHERE t.`task_id` = '{$task_id}' AND t.`user_id` = '{$user_id}' AND `type` = 'task' LIMIT 1");

        $status = $this->db->getValue();
        return in_array($status, ['payment','rate']) ? 'completed' : $status;
    }

    public function get_current_seeker_task_status($task_id = null, $user_id = null){
        if( is_null($task_id) || is_null($user_id) ) return '';

        $this->db->query("SELECT `status`
                      FROM `user_task` t
                      WHERE t.`task_id` = '{$task_id}' AND t.`user_id` = '{$user_id}' AND `type` = 'task' LIMIT 1");

        return $this->db->getValue();
    }

    public function get_poster_task_status($task_id = null, $user_id = null, $poster_id = null){
        if( is_null($task_id) || is_null($user_id) || is_null($poster_id) ) return '';

        $this->db->query("SELECT `status`
                          FROM `poster_seeker` t
                          WHERE t.`task_id` = '{$task_id}' AND t.`seeker_id` = '{$user_id}' AND `poster_id` = '{$poster_id}' LIMIT 1");

        return $this->db->getValue();
    }

    public function has_applicant($user_id = null, $task_id = null){
        if( is_null($task_id) || is_null($user_id) ) return false;

        $this->db->query("SELECT COUNT(`id`)
                          FROM `user_task` u
                          WHERE u.`task_id` = '{$task_id}' AND u.`user_id` = '{$user_id}' LIMIT 1");

        return empty($this->db->getSingleRow()) ? false : true;
    }

    public function time_planning(){
        $this->is_logged_in();
        if(lemon_csrf_require_valid_token()){
            $user_id = $this->user->info['id'];
            $task_id = sanitize(request('task_id'));
            $dates = sanitize(request('date'));
            $starts = sanitize(request('start'));
            $ends = sanitize(request('end'));
            $data = [];
            if(isset($user_id, $task_id, $dates, $starts, $ends)) {
                $dates  = is_array($dates) ? array_values(array_filter($dates)) : [$dates];
                $starts = is_array($starts) ? array_values(array_filter($starts)) : [$starts];
                $ends   = is_array($ends) ? array_values(array_filter($ends)) : [$ends];

                if(is_array($dates) && count($dates) > 1) {
                    foreach (range(0, count($dates) - 1) as $count) {
                        if( strtotime($dates[$count]) && strtotime($starts[$count]) && strtotime($ends[$count]) ) {
                            $data[] = [
                                'user_id' => $user_id,
                                'task_id' => $task_id,
                                'date'    => Carbon::parse($dates[$count])->toDateString(),
                                'start'   => Carbon::parse($starts[$count])->toTimeString(),
                                'end'     => Carbon::parse($ends[$count])->toTimeString()
                            ];
                        }
                    }
                }else{
                    if( strtotime(current($dates)) && strtotime(current($starts)) && strtotime(current($ends)) ) {
                        $data[] = [
                            'user_id' => $user_id,
                            'task_id' => $task_id,
                            'date'    => Carbon::parse(current($dates))->toDateString(),
                            'start'   => Carbon::parse(current($starts))->toTimeString(),
                            'end'     => Carbon::parse(current($ends))->toTimeString()
                        ];
                    }
                }

                if(!empty($data)) {
                    $this->clear_time_planning($user_id, $task_id);
                    $this->db->table('task_planning');
                    foreach ($data as $row) {
                        $this->db->insertArray($row);
                        $this->db->insert();
                    }
                    $first = is_array($dates) && isset($dates[0]) ? Carbon::parse($dates[0]) : Carbon::parse($dates);

                    //flash('message', lang('task_planning_was_saved_succussfully'));
                    //flash('status', 'success');

                }else{

                    flash('message', lang('failed_to_save_task_planning_please_try_again'));
                    flash('status', 'danger');

                    $tab = strpos($_SERVER['HTTP_REFERER'], 'day') !== false ? '#tcInProgressDay' : (strpos($_SERVER['HTTP_REFERER'], 'week') !== false ? '#tcInProgressWeek' : '#tcInProgressMonth');
                    redirect_to( $_SERVER['HTTP_REFERER'] );
                    //redirect_to(option('site_uri') . url_for('/workspace', $this->date->year, $this->date->month.'#tcInProgressMonth'));
                }

            }else{
                flash('message', lang('failed_to_save_task_planning_please_try_again'));
                flash('status', 'danger');

                $tab = strpos($_SERVER['HTTP_REFERER'], 'day') !== false ? '#tcInProgressDay' : (strpos($_SERVER['HTTP_REFERER'], 'week') !== false ? '#tcInProgressWeek' : '#tcInProgressMonth');
                redirect_to($_SERVER['HTTP_REFERER'] );
                //redirect_to(option('site_uri') . url_for('/workspace', $this->date->year, $this->date->month.'#tcInProgressMonth'));
            }

            if(!isset($first)) $first = Carbon::now();
            //$tab = strpos($_SERVER['HTTP_REFERER'], 'day') !== false ? '#tcInProgressDay' : (strpos($_SERVER['HTTP_REFERER'], 'week') !== false ? '#tcInProgressWeek' : '#tcInProgressMonth');
            //redirect_to($_SERVER['HTTP_REFERER'] . $tab);
            redirect_to(option('site_uri') . url_for('/workspace/task-centre/applied/calendar') . '?show=hour');
        }
    }

    public function download_attachment(){
        $this->is_logged_in();
        //$redirect_to = $_SERVER['HTTP_REFERER'] ? url_for($_SERVER['HTTP_REFERER']) : url_for('/workspace');
        $redirect_to = option('site_uri') . url_for('/workspace');
        if( is_null( params('task') ) && is_null(params('file')) ) {
            redirect($redirect_to);
            exit;
        }
        
        $task = sanitizeItem(params('task'));
        $file = sanitizeItem(params('file'));

        if($file = $this->get_attachment_by_file($task, $file)){
            $file_path = explode('/', trim($file['attachment'], '/'));
            $user_folder = $file_path[ count($file_path) - 2 ];
            $the_file = UPLOAD_DIR . 'tasks' . DIRECTORY_SEPARATOR . $user_folder . DIRECTORY_SEPARATOR . end($file_path);

            if(file_exists($the_file)) {
                header("Content-Description: File Transfer");
                //header("Content-Type: " . mime_content_type($the_file));
                header("Content-Disposition: attachment; filename=\"".end($file_path)."\"");
                header("Content-Transfer-Encoding: chunked");
                header("Expires: 0");
                header("Cache-Control: private, must-revalidate, post-check=0, pre-check=0");
                header("Pragma: public");
                readfile("{$the_file}");
            }
        }

        redirect($redirect_to);
        exit;
    }

    public function delete_attachment(){
        $this->is_logged_in();

        if( is_null( params('task') ) && is_null(params('file')) ) {
            echo json(['status' => 'failed', 'message' => lang('file_does_not_exist')]);
        }

        $task = sanitizeItem(params('task'));
        $file_id = sanitizeItem(params('file'));

        if($file = $this->get_task_attachment($task, $file_id)){
            $this->db->table('task_attachments');
            $this->db->whereArray([
               'id' => $file_id,
               'task_id' => $task
            ]);
            $this->db->delete();

            $file_path = explode('/', trim($file['attachment'], '/'));
            $user_folder = $file_path[ count($file_path) - 2 ];
            $the_file = UPLOAD_DIR . 'tasks' . DIRECTORY_SEPARATOR . $user_folder . DIRECTORY_SEPARATOR . end($file_path);
            @unlink($the_file);
            echo json(['status' => 'success', 'message' => lang('file_was_deleted_successfully')]);

        }else{
            echo json(['status' => 'failed', 'message' => lang('file_does_not_exist')]);
        }
        exit;
    }

    public function get_planning(Carbon $date){
        $yesterday = $date->copy()->subDay()->toDateString();
        $tomorrow = $date->copy()->addDay()->toDateString();
        $user_id = $this->user->info['id'];
        $query = "SELECT `task_id`, `date`, `start`, `end`,
                        (
                            SELECT SUM(TIMESTAMPDIFF(SECOND, `start`, `end`))/3600
                            FROM `task_planning` tp
                            WHERE tp.`task_id` = p.`task_id`
                        ) AS `total_hours` 
                  FROM `task_planning` p
                  INNER JOIN `tasks` t ON t.`id` = p.`task_id`
                  WHERE (DATE(p.`date`) BETWEEN '{$yesterday}' AND '{$tomorrow}') AND p.`user_id` = '{$user_id}' AND t.`status` IN('in-progress', 'marked-completed')
                  ORDER BY p.`date`, p.`start`";

        $this->db->query($query);
        return $this->db->getRowList();
    }

    public function get_all_planning(){
        $user_id = $this->user->info['id'];
        $query = "SELECT `task_id`, `date`, `start`, `end`,
                        (
                            SELECT ABS(SUM(TIMESTAMPDIFF(HOUR, `start`, `end`)))
                            FROM `task_planning` tp
                            WHERE tp.`task_id` = p.`task_id`
                        ) AS `total_hours` 
                  FROM `task_planning` p
                  INNER JOIN `tasks` t ON t.`id` = p.`task_id`
                  WHERE p.`user_id` = '{$user_id}' AND t.`status` IN('in-progress', 'marked-completed')
                  ORDER BY p.`date`, p.`start`";

        $this->db->query($query);
        return $this->db->getRowList();
    }

    public function has_new_chats($task_id = null){
        if(null === $task_id) return false;

        $user_id = $this->db->escape($this->user->info['id']);
        $task_id = $this->db->escape($task_id);
        $this->db->query("
                        SELECT * 
                        FROM `chats`
                        WHERE `receiver_id` = {$user_id} AND `task_id` = {$task_id} AND `viewed` = '0'"
        );
        $chats = [];

        foreach($this->db->getRowList() as $chat){
            $chats[] = $chat['sender_id'];
        }

        return !empty($chats) ? $chats : false;
    }

    public function has_chats($task_id = null){
        if(null === $task_id) return false;

        $user_id = $this->db->escape($this->user->info['id']);
        $task_id = $this->db->escape($task_id);
        $this->db->query("
                        SELECT * 
                        FROM `chats`
                        WHERE (`sender_id` = {$user_id} OR `receiver_id` = {$user_id}) AND `task_id` = {$task_id}"
        );
        $chats = [];

        foreach($this->db->getRowList() as $chat){
            $chats[] = $chat['sender_id'];
        }

        return !empty($chats) ? $chats : false;
    }

    public function has_new_comments($post_id = null, $user_id = null){
        if(null === $post_id) return [];
        if( is_null($user_id) ) $user_id = $this->user->info['id'];

        $this->db->query("
                        SELECT *, c.`id` AS `comment_id`, c.`viewed` FROM `comments` c
                        INNER JOIN `members` m ON m.`id` = c.`user_id`
                        WHERE c.`post_id` = '{$post_id}' AND c.`user_id` != '{$user_id}' AND c.`viewed` = 'N'
                        ORDER BY c.`created_at` DESC");
        return $this->db->getRowList();
    }

    public function get_tasks_new_comments($tasks = []){
        if( empty($tasks) ) return [];
        $user_id = $this->user->info['id'];
        if( ! is_array($tasks) ) (array) $tasks;

        $tasks = implode("','", $tasks);

        $this->db->query("
                        SELECT *, c.`id` AS `comment_id`, c.`viewed` FROM `comments` c 
                        WHERE c.`post_id` IN ('{$tasks}') AND c.`user_id` != '{$user_id}' 
                        AND c.`id` NOT IN( SELECT `parent_id` FROM `comments` WHERE `parent_id` != '0' )
                        ORDER BY c.`created_at` DESC");

        return $this->db->getRowList();
    }

    public function get_all_tasks($status = [], $dates = [], $type = 'owned'){
        if( !in_array($type, ['owned', 'assigned']) ) return [];
        if( !empty($status) ) $statuses = "'" . implode("','", $status) . "'";
        $user_id = $this->user->info['id'];

        $this->db->table('tasks');
        $query = "SELECT t.* FROM `tasks` t ";

        if( 'owned' === $type) {
            $query .= "WHERE t.`user_id` = '{$user_id}' ";
        }elseif('assigned' === $type){
            $query .= "WHERE t.`assigned_to` = '{$user_id}' ";
        }

        $query .= "AND (t.`assigned` IS NOT NULL AND t.`assigned_to` IS NOT NULL) ";

        if( isset($statuses) ){
            $query .= "AND t.`status` IN({$statuses}) ";
        }

        if( !empty($dates) && count($dates) === 2 ){
            $query .= "AND (DATE(t.`created_at`) BETWEEN '{$dates[0]}' AND '{$dates[1]}') ";
        }

        $query .= "ORDER BY t.`created_at` DESC";

        $this->db->query($query);
        return $this->db->getRowList();
    }

    private function has_expiring_reminder($user_id = null, $task_id = null){
        if( is_null($user_id) && is_null($task_id) ) return false;

        $this->db->query("SELECT COUNT(*) FROM `notifications` WHERE `for_id` = '{$user_id}' AND `type` = 'task' AND `type_id` = '{$task_id}' AND `event` = 'task expiring'");
        return $this->db->getValue() > 0;
    }

    private function has_50_milestone_reminder($user_id = null, $task_id = null){
        if( is_null($user_id) && is_null($task_id) ) return false;

        $this->db->query("SELECT COUNT(*) FROM `notifications` WHERE `for_id` = '{$user_id}' AND `type` = 'task' AND `type_id` = '{$task_id}' AND `event` = 'task approaching end'");
        return $this->db->getValue() > 0;
    }

    private function has_review_reminder($user_id = null, $task_id = null){
        if( is_null($user_id) && is_null($task_id) ) return false;

        $this->db->query("SELECT COUNT(*) FROM `notifications` WHERE `for_id` = '{$user_id}' AND `type` = 'task' AND `type_id` = '{$task_id}' AND `event` = 'review reminder'");
        return $this->db->getValue() > 0;
    }

    /**
     * @param string $title
     * @return string
     */
    private function create_slug(string $title, $task_id = null){
        $title = sanitize($title);
        $slug = trim(preg_replace('#[\s]#i', '-', preg_replace('#[^a-z0-9\s]#i', '', strtolower($title))), '-');

        if (is_null($task_id)) {
            // Let's check if the slug is avaliable, otherwise give it a counter at the end
            $this->db->query("SELECT COUNT(id) as `count` FROM `tasks` WHERE `slug` LIKE '%{$slug}%'");
            $count = (int)$this->db->getSingleRow()['count'];
            if ($count > 0) {
                $slug .= "-" . ($count + 1);
            }
        } else {
            $task_id = sanitize($task_id);
            $user_id = $this->user->info['id'];
            $this->db->query("SELECT * FROM `tasks` WHERE `id` = '{$task_id}' AND `user_id` = '{$user_id}' AND `status` = 'draft' LIMIT 1");
            $task = $this->db->getSingleRow();

            $slug = ( $task['title'] !== $title ) ? $this->create_slug($title, null) : $task['slug'];
        }

        return $slug;
    }

    public function clear_time_planning($user_id = null, $task_id = null){
        if( is_null($user_id) ) $user_id = $this->user->info['id'];
        if( is_null($task_id) ) $task_id = sanitize(request('task_id'));

        if( !$user_id && !$task_id ) return;

        $this->db->table('task_planning');

        $this->db->whereArray([
            'user_id' => $user_id,
            'task_id' => $task_id,
        ]);
        $this->db->delete();
    }

    /**
     * @param string $task_status default is 'published'
     * @param int $user_id default is null
     * @return int count of tasks with status passed
     */
    public function get_status_count($task_status = 'published', $user_id = null){
        if( is_null($user_id) ) $user_id = $this->user->info['id'];

        return $this->db->getValue("SELECT COUNT(*) FROM `tasks` WHERE `user_id` = '{$user_id}' AND `status` = '{$task_status}'");
    }

    public function set_service_charge($task){
        $percent                = ($this->service_percentage / 100) * (int)$task['budget'];
        $task['service_charge'] = ($percent > $this->service_charge) ? $percent : $this->service_charge;

        return $task;
    }

    public function get_latest_payment($task_id = null, $poster_id = null, $seeker_id = null){
        if( ! isset( $task_id, $poster_id, $seeker_id ) ) return null;

        $this->db->query("SELECT * FROM `task_payment` 
                          WHERE `task_id` = '{$task_id}' AND `user_id` = '{$poster_id}' AND `assigned_to` = '{$seeker_id}'
                          ORDER BY `date` DESC LIMIT 1 ");
        return $this->db->getSingleRow();
    }

    public function get_latest_release_payment($task_id = null, $poster_id = null, $seeker_id = null){
        if( ! isset( $task_id, $poster_id, $seeker_id ) ) return null;

        $this->db->query("SELECT * FROM `payment_release` 
                          WHERE `task_id` = '{$task_id}' AND `poster_id` = '{$poster_id}' AND `seeker_id` = '{$seeker_id}' AND `type` = 'payment'
                          ORDER BY `created_at` DESC LIMIT 1 ");
        return $this->db->getSingleRow();
    }

    public function set_profile_viewed($user_id = null, $task_id = null){
        if( isset($user_id, $task_id) ){
            $this->db->table('user_task');
            $this->db->updateArray(['viewed' => 'Y']);
            $this->db->whereArray(['user_id' => $user_id, 'task_id' => $task_id]);
            $this->db->update();
        }

    }

    public function set_seeker_declined($task_id = null, $seeker_id = null){
        if( !isset($seeker_id, $task_id) ) return false;

        $this->db->table('user_task');
        $this->db->whereArray(['user_id' => $seeker_id, 'type' => 'task', 'task_id' => $task_id]);
        $this->db->updateArray(['status' => 'declined']);
        $this->db->update();

        return true;
    }

    protected function set_in_progress($task_id = null){
        if( !isset($task_id) ) return false;

        $this->db->table('tasks');
        $this->db->whereArray(['id' => $task_id]);
        $this->db->updateArray(['status' => 'in-progress']);
        $this->db->update();

        return true;
    }

    protected function set_completed($task_id = null){
        if( !isset($task_id) ) return false;

        $this->db->table('tasks');
        $this->db->whereArray(['id' => $task_id]);
        $this->db->updateArray(['status' => 'completed']);
        $this->db->update();

        return true;
    }

    protected function set_closed($task_id = null){
        if( !isset($task_id) ) return false;

        $this->db->table('tasks');
        $this->db->whereArray(['id' => $task_id]);
        $this->db->updateArray(['status' => 'closed']);
        $this->db->update();

        return true;
    }

    protected function set_draft($task_id = null){
        if( !isset($task_id) ) return false;

        $this->db->table('tasks');
        $this->db->whereArray(['id' => $task_id]);
        $this->db->updateArray(['status' => 'draft']);
        $this->db->update();

        return true;
    }

    protected function set_poster_seeker_in_progress($poster_id = null, $seeker_id = null, $task_id = null){
        if( !isset($poster_id, $seeker_id, $task_id) ) return false;

        $this->db->table('poster_seeker');
        $this->db->whereArray(['poster_id' => $poster_id, 'seeker_id' => $seeker_id, 'task_id' => $task_id]);
        $this->db->updateArray(['status' => 'in-progress']);
        $this->db->update();

        $this->db->table('user_task');
        $this->db->whereArray(['user_id' => $seeker_id, 'type' => 'task', 'task_id' => $task_id]);
        $this->db->updateArray(['status' => 'in-progress']);
        $this->db->update();

        return true;
    }
}