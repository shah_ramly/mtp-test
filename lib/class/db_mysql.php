<?php
/* 
// Start Connection
$db = new db();
 
//INSERT 
$db->table("tableName");
$db->insertArray(array("columnName_1"=>"value_1","columnName_2"=>"value_2","columnName_3"=>"value_3"));
$db->insert();

//UPDATE
$db->table("tableName");
$db->updateArray(array("columnName_1"=>"value_1","columnName_2"=>"value_2","columnName_3"=>"value_3"));
$db->whereArray(array("columnName_1"=>"value_1","columnName_2"=>"value_2","columnName_3"=>"value_3"));
$db->update();

//SELECT
$db->query("SELECT * FROM tableName");
$db->getRowList();

//GET SINGLE VALUE
$db->query("SELECT * FROM tableName WHERE id = 'value'");
$db->getValue();

// Close Connection
$db->disconnect();

*/

class db{
	var $db_server;
	var $db_user;
	var $db_pass;
	var $db_name;
	var $conn,$db,$tableName,$insertColumn,$insertValue,$updateArrayValue,$whereArrayValue,$query;
	
	//start connection
	public function __construct($server,$user,$pass,$name){
		$this->db_server = $server;
		$this->db_user = $user;
		$this->db_pass = $pass;
		$this->db_name = $name;
		$this->conn = mysql_connect($this->db_server,$this->db_user,$this->db_pass) or die ('Error : Failed to connect to server');
		$this->db = mysql_select_db($this->db_name,$this->conn) or die ('Error : Failed to connect to database');
	}
	
	//Table Name
	public function table($table){
		$this->tableName = $table;
	}
	
	//Insert value's array
	public function insertArray($array){
		foreach($array as $column => $value){
			$columns[] = $column;
			$values[] = $this->checkSQLValue($value);
		}
		$this->insertColumn = implode(',',$columns);
		$this->insertValue = implode(',',$values);
	}
	
	//Update value's array
	public function updateArray($array){
		foreach($array as $column => $value){
			$arrays[] = $column." = ".$this->checkSQLValue($value);
		}
		$this->updateArrayValue = implode(',',$arrays);
	}
	
	//Where value's array
	public function whereArray($array){
		foreach($array as $column => $value){
			$arrays[] = $column." = ".$this->checkSQLValue($value);
		}
		$this->whereArrayValue = implode(' AND ',$arrays);
	}
	
	//insert new record
	public function insert(){
		$query = "INSERT INTO ".$this->tableName." (".$this->insertColumn.") VALUES (".$this->insertValue.")";
		$this->queryOrDie($query);
	}
	
	//Insert last id
	public function insertid(){
		return mysql_insert_id($this->conn);
	}
	
	//update database 
	public function update(){
		$query = "UPDATE ".$this->tableName." SET ".$this->updateArrayValue." WHERE ".$this->whereArrayValue;
		$this->queryOrDie($query);
	}
	
	public function delete(){
		$query = "DELETE FROM ".$this->tableName." WHERE ".$this->whereArrayValue;
		$this->queryOrDie($query);
	}
		
	//mysql query
	public function query($query){
		$this->query = mysql_query($query) or die(mysql_error());
	}
	
	//get rows list
	public function getRowList(){	
		while($rows = mysql_fetch_assoc($this->query)){
			$records[] = $rows;	
		}

		if(mysql_num_rows($this->query) > 0){
       		return $records; 
		}
	}
	
	public function getSingleRow(){
		$rows = mysql_fetch_assoc($this->query);
		$records = $rows;

		if(mysql_num_rows($this->query) > 0){
       		return $records; 
		}
	}
	
	public function getTotal($query){
		return $this->getCount($query);
	}

	//Get single value
	public function getValue($query){
		if($this->getCount($query) > 0){
			return stripslashes(mysql_result(mysql_query($query),0));
		}
	}
	
	//Disconnect
	public function __destruct(){
		mysql_close($this->conn);
	}
	
	//Query value filter
	public function escape($value){
		if(get_magic_quotes_gpc()){
			$value = stripslashes($value);
		}
		if(!is_numeric($value)){
			return "'".mysql_real_escape_string($value)."'";
		}else{
			return "'".$value."'";
		}
	}
	
	//check loading time spent
	public function startTime(){
		$startTime = microtime();
		$startArray = explode(" ", $startTime);
		$this->startTime = $startArray[1] + $startArray[0];
	}
	
	public function endTime(){
		$endTime = microtime();
		$endArray = explode(" ", $endTime);
		$this->endTime = $endArray[1] + $endArray[0];
	}
	
	public function checkTime(){
		return round(($this->endTime - $this->startTime),5);
	}
	
	//Check (NOW())Date value in mysql query
	private function checkSQLValue($value){
		if(!is_numeric($value) && $value == "NOW()"){
			return "NOW()";
		}else{
			return $this->escape($value);
		}
	}
	
	//Get query total
	private function getCount($query){
		return mysql_num_rows(mysql_query($query));
	}
	
	//Check query error
	private function queryOrDie($query){
		$result = mysql_query($query);	
		if(isset($result)){
			return $result;
		}else{
			$this->queryError($query);
		}
	}
	
	private function queryError($query){
		echo mysql_error($this->conn).'<br/>';
		die('Error : '. $query);
	}
}

?>
