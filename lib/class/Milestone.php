<?php


class Milestone
{
    /**
     * @param $db db
     * @param $id int
     * @param $key column name in the table to use in retrieving data
     * @return array
     */
    public static function get($db, $value = null, $key = 'id'){
        if( isset($db, $value) ){
            if( is_array($value) ){
                $where = '';
                foreach ($value as $k => $v){
                    $v = mysqli_real_escape_string($db->connection, $v);
                    $where .= "`{$k}` = '{$v}' AND ";
                }
                $where = rtrim($where, " AND ");
                $db->query("SELECT * FROM `milestone_action` WHERE $where");
            }else {
                $value = mysqli_real_escape_string($db->connection, $value);
                $db->query("SELECT * FROM `milestone_action` WHERE `{$key}` = '{$value}'");
            }

            return $db->getRowList();
        }

        return [];
    }

    /**
     * @param $db db
     * @param $data array
     * @return int|null
     */
    public static function save($db = null, $data = []){

        if( isset($db) && !empty($data) ){
            $db->table('milestone_action');
            $db->insertArray($data);
            $db->insert();
            return $db->insertid();
        }

        return null;
    }

    /**
     * @param $db db
     * @param $data array
     * @param $condition array
     * @return bool
     */
    public static function update($db = null, $data = [], $condition = []){

        if( isset($db) && !empty($data) ){
            $db->table('milestone_action');
            $db->updateArray($data);
            $db->whereArray($condition);
            $db->update();
            return true;
        }

        return false;
    }

    /**
     * @param $db db
     * @param $condition array
     * @return bool
     */
    public static function remove($db = null, $condition = []){

        if( isset($db) && !empty($condition) ){
            $db->table('milestone_action');
            $db->whereArray($condition);
            $db->delete();

            return true;
        }

        return false;
    }

}