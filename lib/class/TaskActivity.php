<?php

class TaskActivity
{
    protected $db;
    protected $user;

    /**
     * @var $db db
     * @var $user user
     */
    public function __construct($db, $user){
        $this->db = $db;

        if( isset($_SESSION['member_id']) && !empty($_SESSION['member_id']) ){
            $_SESSION[WEBSITE_PREFIX.'USER_ID'] = $_SESSION['member_id'];
        }

        $this->user = $user;
        date_default_timezone_set('Asia/Kuala_Lumpur');

        if( !isset($_SESSION[WEBSITE_PREFIX.'LANGUAGE']) || $_SESSION[WEBSITE_PREFIX.'LANGUAGE'] == 'EN' ){
            $locale = ['English', 'en_GB'];
        }else{
            $locale = ['Malay', 'ms_MY'];
        }

        setLocale(LC_TIME, $locale[1]);
        \Carbon\Carbon::setLocale($locale[1]);
    }

    public function add($args = []){
        extract($args, EXTR_OVERWRITE);
        $user_id = isset($user_id) ? $user_id : $this->user->info['id'];
        $created_at = isset($created_at) ? $created_at : \Carbon\Carbon::now();
        $user_type = (new Task($this->db, $this->user))->owner($user_id, $task_id) ? 'poster' : 'seeker';
        $this->db->table('task_activity');
        $this->db->insertArray([
            'user_id'     => $user_id,
            'user_type'   => $user_type,
            'task_id'     => $task_id,
            'task_number' => $task_number,
            'event'       => $event,
            'message'     => isset($message) ? $message : null,
            'created_at'  => $created_at->toDateTimeString()
        ]);
        $this->db->insert();
    }

    public function get_activities( $task_id = null ){
        if( empty($task_id) ) return [];

        $query = "SELECT ta.*,
                  (
                    SELECT CONCAT(m.`firstname`, ' ', m.`lastname`) AS `name` 
                    FROM `members` m
                    WHERE m.`id` = ta.`user_id`
                  ) AS `user_name`,
                  (
                    SELECT `name` AS `name` 
                    FROM `company_details` c
                    WHERE c.`member_id` = ta.`user_id`
                  ) AS `company_name`
                  
                  FROM `task_activity` ta";

        if( is_array($task_id) ){
            $query .= " WHERE ta.`task_id` IN('" . implode("','", $task_id) . "')";
        }else{
            $query .= " WHERE ta.`task_id` = " . $this->db->escape($task_id);
        }

        $query .= " ORDER BY ta.`created_at` DESC";

        $this->db->query($query);
        return $this->db->getRowList();
    }

    public function formatted_activities( $the_task = null, $user_id = null ){
        $activities   = isset($the_task['task_id']) ? $this->get_activities($the_task['task_id']) : $this->get_activities($the_task);
        $current_user = !is_null($user_id) ? $user_id : $this->user->info['id'];

        $task         = new Task($this->db, $this->user);
        $talent       = isset($the_task['task_id']) ?
                        (new TaskCentre($this->db, $this->user))->get_talent($the_task['assigned_to'], false) :
                        (new TaskCentre($this->db, $this->user))->get_talent($user_id, false);
        $logs         = [];

        foreach ($activities as $activity):
            $the_task = isset($the_task['task_id']) ? $the_task : $activity;

            if( !isset($the_task['assigned_to']) ){
                $the_task = $task->get_task_by_id($the_task['task_id']);
                $the_task['user'] = $task->get_task_user($the_task['user_id']);

                if($the_task['user']['type'] === '0')
                    $the_task['user']['name'] = "{$the_task['user']['firstname']} {$the_task['user']['lastname']}";
                else
                    $the_task['user']['name'] = $the_task['company_name'];

                $talent =  !empty($talent) ? $talent : (new TaskCentre($this->db, $this->user))->get_talent($the_task['assigned_to'], false);
            }

            $log  = [];
            $user = ($current_user === $activity['user_id']) ? 'You' : (is_null($activity['company_name']) ? $activity['user_name'] : $activity['company_name']);
            $date['date'] = \Carbon\Carbon::parse($activity['created_at'])->format("d-M-Y");
            $date['time'] = \Carbon\Carbon::parse($activity['created_at'])->format("H:i:s");
            switch($activity['event']):
                case 'created':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$task'], $activity['task_number'], lang("task_activity_task_has_been_created")); // Task #{$activity['task_number']} has been created
                    endif;
                    break;

                case 'published':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$task'], $activity['task_number'], lang("task_activity_task_has_been_published")); //The Task id #{$activity['task_number']} has been published
                    endif;
                    break;

                case 'applied':
                    if( !$task->owner($current_user, $the_task['task_id']) && $activity['user_id'] === $current_user ):
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$task'], $activity['task_number'], lang("task_activity_you_applied_for_task")); //You applied for Task id #{$activity['task_number']}
                    endif;
                    break;

                case 'assigned':
                    $log['date'] = $date;
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['message'] = str_replace(['$talent', '$task'], [$talent['name'], $activity['task_number']], lang("task_activity_talent_has_been_selected")); //{$talent['name']} has been selected to complete the Task id #{$activity['task_number']}
                    elseif( $the_task['assigned_to'] === $current_user ):
                        $log['message'] = str_replace(['$poster', '$task'], [$the_task['user']['name'], $activity['task_number']], lang("task_activity_poster_assigned_you")); //{$the_task['user']['name']} assigned you for Task id #{$activity['task_number']}
                    else:
                        $log['message'] = str_replace(['$task'], $activity['task_number'], lang("task_activity_you_have_lost")); //You have lost for Task id #{$activity['task_number']}
                    endif;
                    break;

                case 'payment request':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$talent', '$task'], [$talent['name'], $activity['task_number']], lang("task_activity_talent_has_requested_payment")); //{$talent['name']} has requested payment for Task id #{$activity['task_number']}
                    elseif( $the_task['assigned_to'] === $current_user ):
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$task'], $activity['task_number'], lang("task_activity_you_have_requested_payment")); //You have requested payment for Task id #{$activity['task_number']}
                    endif;
                    break;

                case 'payment made':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$task'], $activity['task_number'], lang("task_activity_payment_has_been_made_for")); //Payment has been made to Escrow for Task id #{$activity['task_number']}
                    endif;
                    break;

                case 'marked completed':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$talent', '$task'], [$talent['name'], $activity['task_number']], lang("task_activity_talent_has_marked_completed")); //{$talent['name']} has mark Task id #{$activity['task_number']} as completed
                    elseif( $current_user === $the_task['assigned_to'] ):
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$task'], $activity['task_number'], lang("task_activity_you_have_marked_completed")); //You have mark Task id #{$activity['task_number']} as completed
                    endif;
                    break;

                case 'rejected':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$task'], $activity['task_number'], lang("task_activity_you_have_rejected")); //You have rejected for Task id #{$activity['task_number']}
                    elseif($current_user === $the_task['assigned_to']):
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$poster', '$task'], [$user, $activity['task_number']], lang("task_activity_poster_has_rejected")); //$the_task['user']['name'], {$user} has rejected Task id #{$activity['task_number']}
                    endif;
                    break;

                case 'accepted':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$task'], $activity['task_number'], lang("task_activity_you_have_accepted")); //You have accepted for Task id #{$activity['task_number']}
                    elseif($current_user === $the_task['assigned_to']):
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$poster', '$task'], [$user, $activity['task_number']], lang("task_activity_poster_has_accepted")); //$the_task['user']['name'], {$user} has accepted Task id #{$activity['task_number']}
                    endif;
                    break;

                case 'completed':
                    /*if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = "You have set the Task id #{$activity['task_number']} as completed";
                    elseif( $the_task['assigned_to'] === $current_user ):
                        $log['date'] = $date;
                        $log['message'] = "{$user} has set the Task id #{$activity['task_number']} as completed";//$the_task['user']['name']
                    endif;*/
                    break;

                case 'disputed':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $disputed_amount = number_format($the_task['budget'] * ($the_task['rejection_rate'])/100, 2);
                        $disputed_percent = $the_task['rejection_rate'];
                        $log['message'] = str_replace(['$percent', '$amount'], [$disputed_percent, $disputed_amount], lang("task_activity_task_disputed_and_total_disputed")); //{$disputed_percent}% disputed & total disputed amount RM{$disputed_amount}
                        array_push($logs, $log);
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$task'], $activity['task_number'], lang("task_activity_you_have_disputed")); //You have disputed for The Task id #{$activity['task_number']}
                    elseif( $the_task['assigned_to'] === $current_user ):
                        $log['date'] = $date;
                        $disputed_amount = number_format($the_task['budget'] * ($the_task['rejection_rate'])/100, 2);
                        $disputed_percent = $the_task['rejection_rate'];
                        $log['message'] = str_replace(['$percent', '$amount'], [$disputed_percent, $disputed_amount], lang("task_activity_task_disputed_and_total_disputed")); //{$disputed_percent}% disputed & total disputed amount RM{$disputed_amount}
                        array_push($logs, $log);
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$poster', '$task'], [$user, $activity['task_number']], lang("task_activity_poster_has_disputed"));//$the_task['user']['name'], {$user} has disputed for The Task id #{$activity['task_number']}
                    endif;
                    break;

                case 'closed':
                    if( $task->owner($current_user, $the_task['task_id']) ):
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$talent', '$task'], [$talent['name'], $activity['task_number']], lang("task_activity_payment_has_been_released_to_talent")); //The payment has been released to {$talent['name']} for Task id #{$activity['task_number']} and the status now is closed
                    elseif($the_task['assigned_to'] === $current_user):
                        $log['date'] = $date;
                        $log['message'] = str_replace(['$task'], $activity['task_number'], lang("task_activity_payment_has_been_released_to_you")); //The payment has been released to you for Task id #{$activity['task_number']} and the status now is closed
                    endif;
                    break;

                case 'cancelled':
                    $log['date'] = $date;
                    if(is_null($activity['user_name'])) $user = "The System";
                    $log['message'] = str_replace(['$task', '$poster'], [$activity['task_number'], $user], lang("task_activity_task_has_been_cancelled")); //The Task id #{$activity['task_number']} has been cancelled by {$user}
                    break;
            endswitch;

            if(!empty($log)) array_push($logs, $log);
        endforeach;

        return $logs;
    }

    public function has_marked_completed_activity( $task_id = null ){
        if( is_null($task_id) ) return false;

        $query = "SELECT `id` FROM `task_activity` WHERE `event` = 'marked completed' AND `updated_at` IS NULL 
                  AND `task_id` = " . $this->db->escape($task_id);
        $query .= " ORDER BY `created_at` DESC LIMIT 1";

        $this->db->query($query);
        return !empty( $this->db->getRowList() );
    }

    public function set_marked_completed_as_seen( $task_id = null ){
        if( is_null($task_id) ) return false;

        $now = \Carbon\Carbon::now()->toDateTimeString();
        $query = "UPDATE `task_activity` SET `updated_at` = '{$now}' 
                  WHERE `event` = 'marked completed' AND `task_id` = " . $this->db->escape($task_id);

        $this->db->queryOrDie($query);
    }

    public function has_been_rejected_twice($task_id = null){
        if( is_null($task_id) ) return false;

        $this->db->query("SELECT COUNT(*) FROM `task_activity` WHERE `task_id` = '{$task_id}' AND `event` IN ('rejected')");
        return (int)$this->db->getValue() >= 2;
    }

    public function has_rejected_activity( $task_id = null ){
        if( is_null($task_id) ) return false;

        $query = "SELECT `id` FROM `task_activity` WHERE `event` = 'rejected' AND `updated_at` IS NULL 
                  AND `task_id` = " . $this->db->escape($task_id);
        $query .= " ORDER BY `created_at` DESC LIMIT 1";

        $this->db->query($query);
        return !empty( $this->db->getRowList() );
    }

    public function has_accepted_activity( $task_id = null ){
        if( is_null($task_id) ) return false;

        $query = "SELECT `id` FROM `task_activity` WHERE `event` = 'accepted' AND `updated_at` IS NULL 
                  AND `task_id` = " . $this->db->escape($task_id);
        $query .= " ORDER BY `created_at` DESC LIMIT 1";

        $this->db->query($query);
        return !empty( $this->db->getRowList() );
    }

    public function has_disputed_activity( $task_id = null ){
        if( is_null($task_id) ) return false;

        $query = "SELECT `id` FROM `task_activity` WHERE `event` = 'disputed' AND `updated_at` IS NULL 
                  AND `task_id` = " . $this->db->escape($task_id);
        $query .= " ORDER BY `created_at` DESC LIMIT 1";

        $this->db->query($query);
        return !empty( $this->db->getRowList() );
    }

    public function set_rejected_as_seen( $task_id = null ){
        if( is_null($task_id) ) return false;

        $now = \Carbon\Carbon::now()->toDateTimeString();
        $query = "UPDATE `task_activity` SET `updated_at` = '{$now}' 
                  WHERE `event` = 'rejected' AND `task_id` = " . $this->db->escape($task_id);

        $this->db->queryOrDie($query);
    }

    public function set_accepted_as_seen( $task_id = null ){
        if( is_null($task_id) ) return false;

        $now = \Carbon\Carbon::now()->toDateTimeString();
        $query = "UPDATE `task_activity` SET `updated_at` = '{$now}' 
                  WHERE `event` = 'accepted' AND `task_id` = " . $this->db->escape($task_id);

        $this->db->queryOrDie($query);
    }

    public function set_disputed_as_seen( $task_id = null ){
        if( is_null($task_id) ) return false;

        $now = \Carbon\Carbon::now()->toDateTimeString();
        $query = "UPDATE `task_activity` SET `updated_at` = '{$now}' 
                  WHERE `event` = 'disputed' AND `task_id` = " . $this->db->escape($task_id);

        $this->db->queryOrDie($query);
    }

    public function has_completed_activity( $task_id = null ){
        if( is_null($task_id) ) return false;

        $query = "SELECT `id` FROM `task_activity` WHERE `event` = 'completed' AND `updated_at` IS NULL 
                  AND `task_id` = " . $this->db->escape($task_id);
        $query .= " ORDER BY `created_at` DESC LIMIT 1";

        $this->db->query($query);
        return !empty( $this->db->getRowList() );
    }

    public function set_completed_as_seen( $task_id = null ){
        if( is_null($task_id) ) return false;

        $now = \Carbon\Carbon::now()->toDateTimeString();
        $query = "UPDATE `task_activity` SET `updated_at` = '{$now}' 
                  WHERE `event` = 'completed' AND `task_id` = " . $this->db->escape($task_id);

        $this->db->queryOrDie($query);
    }
}