<?php

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../helpers.php';

use Carbon\Carbon;

class JobCentre
{
    protected $db;
    protected $user;
    protected $date;
    protected $job;
    protected $job_activity;

    /**
     * @var $db db
     * @var $user user
     */
    public function __construct($db, $user){
        $this->db = $db;

        if( isset($_SESSION['member_id']) && !empty($_SESSION['member_id']) ){
            $_SESSION[WEBSITE_PREFIX.'USER_ID'] = $_SESSION['member_id'];
        }

        $this->user = $user;
        $this->job = new Job($this->db, $this->user);
        $this->job_activity = new JobActivity($this->db, $this->user);
        date_default_timezone_set('Asia/Kuala_Lumpur');

        if( !isset($_SESSION[WEBSITE_PREFIX.'LANGUAGE']) || $_SESSION[WEBSITE_PREFIX.'LANGUAGE'] == 'EN' ){
            $locale = ['English', 'en_GB'];
        }else{
            $locale = ['Malay', 'ms_MY'];
        }

        setLocale(LC_TIME, $locale[1]);
        Carbon::setLocale($locale[1]);
        $this->date = Carbon::today();
    }

    public function job_centre()
    {
        set_latest_url( request_uri() );
        $user_id = $this->user->info['id'];
        $type    = $this->user->info['type'] === '1' ? 'posted' : 'applied';
        $jobs    = $this->job->jobs($user_id);

        set('jobs', $jobs);
        set('type', $type);
        set('mode', 'ft-mode');
        set('page_title', 'Job Centre');
        return render('/workspace/job-centre.html.php', 'layout/app.html.php');
    }

    public function get_answers(){
        if(is_null(params('job')) || is_null(params('user'))) {
            echo json(['answers' => []]);
        }

        $user_id = (int)sanitizeItem(params('user'));
        $job_id = sanitizeItem(params('job'));

        $questions = $this->db->query("SELECT q.`id`, q.`question` FROM `job_questions` q WHERE q.`job_id` = '{$job_id}'");
        $questions = $this->db->getRowList();

        $answers = $this->db->query("SELECT a.`question_id` AS `id`, a.`answer` FROM `answers` a WHERE a.`user_id` = '{$user_id}' AND a.`task_id` = '{$job_id}'");
        $answers = $this->db->getRowList();
        $qna     = [];

        array_map(function ($question) use($answers, &$qna){
            $answer = array_filter($answers, function($answer)use($question){ return $answer['id'] === $question['id'] ? $answer['answer'] : false; });
            $qna[] = [
                'question' => $question['question'],
                'answer' => !empty(end($answer)['answer']) ? end($answer)['answer'] : 'Not answered',
            ];
        }, $questions);

        echo json(['answers' => $qna]);
    }

    public function shortlist(){
        $redirect_to = option('site_uri') . url_for('/workspace/job-centre');
        if(lemon_csrf_require_valid_token() && request('job_id') && request('user_id')) {
            $user_id = request('user_id');
            $job_id  = request('job_id');
            $owner_id = $this->user->info['id'];

            $job = new Job($this->db, $this->user);

            if( $job && $job->owner($owner_id, $job_id) ){
                if( $job->has_applicant($user_id, $job_id) ){
                    $talent = (new TaskCentre($this->db, $this->user))->get_talent($user_id, false);
                    if($job->shortlist($user_id, $job_id)){
                        flash('message', lang('candidate_shortlisted_successfully'));
                        flash('status', 'success');
                    }else{
                        flash('message', lang('failed_to_shortlist_candidate_please_try_again'));
                        flash('status', 'danger');
                    }
                }else{
                    flash('message', lang('failed_to_shortlist_candidate_please_try_again'));
                    flash('status', 'danger');
                }
            }else{
                flash('message', lang('you_do_not_have_permission_to_shortlist_to_this_job'));
                flash('status', 'danger');
            }
        }else{
            flash('message', lang('failed_to_shortlist_candidate_please_try_again'));
            flash('status', 'danger');
        }

        redirect_to($redirect_to);
    }

    public function overall_notifications(){

        $user_id = $this->user->info['id'];

        $query = "SELECT j.`id`,
                  (SELECT COUNT(`id`) FROM `chats` WHERE `receiver_id` = j.`user_id` AND `viewed` = '0' AND `task_id` = j.`id` ) AS `chats`,
                  (SELECT COUNT(`id`) FROM `comments` WHERE `post_id` = j.`id` AND `user_id` != j.`user_id` AND `viewed` = 'N' AND `hidden` = '0') AS `comments`,
                  (SELECT COUNT(`id`) FROM `user_task` WHERE `task_id` = j.`id` AND `type` = 'job' AND `viewed` = 'N') AS `applicants`
                  FROM `jobs` j WHERE j.`status` IN ('published') AND j.`user_id` = '{$user_id}'
                    
                  UNION 
                    
                  SELECT ut.`task_id`, 
                  (SELECT COUNT(`id`) FROM `chats` WHERE `receiver_id` = ut.`user_id` AND `viewed` = '0' AND `task_id` = ut.`task_id` ) AS `chats`,
                  (SELECT COUNT(`id`) FROM `comments` WHERE `post_id` = ut.`task_id` AND `user_id` != ut.`user_id` AND `viewed` = 'N' AND `hidden` = '0') AS `comments`,
                  0 AS `applicants`
                  
                  FROM `user_task` ut 
                  INNER JOIN `jobs` j ON j.`id` = ut.`task_id` AND j.`status` IN ('published')
                  WHERE ut.`user_id` = '{$user_id}' AND ut.`type` = 'job' AND ut.`status` NOT IN ('closed', 'cancelled', 'disputed')";
        $this->db->query($query);
        $notifications = $this->db->getRowList();
        $notifications = array_map(function ($notification){ unset($notification['id']); return array_filter($notification); }, $notifications);

        set('jobs_notification', (bool) count(array_filter($notifications)));
    }
}