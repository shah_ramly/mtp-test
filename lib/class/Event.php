<?php

class Event{

    public static $events = [];

    public static function trigger($event, $args = []){
        if (isset(self::$events[$event])) {
            foreach (self::$events[$event] as $callback) {
                //@list($class, $method) = explode('.', $callback);
                //call_user_func_array([ucfirst($class), $method], $args);
                call_user_func_array(['Notification', $callback], $args);
            }
        }
    }

    public static function register($event, $callback){
        self::$events[$event][] = $callback;
    }
}