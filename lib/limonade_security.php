<?php

function lemon_csrf_token($token_name = 'form_token', $token_expiration_time = 600){
    static $name = null;
    static $expiration_time = null;
    static $token;

    if (isset($_SESSION[$token_name])) {
        $name            = $token_name;
        $token           = $_SESSION[$token_name];
        $expiration_time = $_SESSION[$token_name . '_expiration_time'];
    } else {
        if (isset($name) && !is_null($name)) lemon_csrf_unset_token($name); // unset previous token
        $name  = $token_name;
        $token = md5(uniqid('auth', true));
        if (is_null($expiration_time)) $expiration_time = $token_expiration_time;
        $_SESSION[$name]                      = $token;
        $_SESSION[$name . '_time']            = time();
        $_SESSION[$name . '_expiration_time'] = $expiration_time;
    }
    return ['name' => $name, 'value' => $token, 'expiration_time' => $expiration_time];
}

function lemon_csrf_unset_token($token = null){
    if (is_null($token)) $token = lemon_csrf_token();
    $token_name = is_array($token) ? $token['name'] : $token;
    if (!is_null($token_name) && isset($_SESSION[$token_name])) {
        unset($_SESSION[$token_name]);
        unset($_SESSION[$token_name . '_time']);
        unset($_SESSION[$token_name . '_expiration_time']);
    }
}

function lemon_csrf_token_age($token = null)
{
  if(is_null($token)) $token = lemon_csrf_token();
  $token_name = is_array($token) ? $token['name'] : $token;
  return time() - $_SESSION[$token_name.'_time'];
}

function lemon_csrf_token_expired($token = null){
  if(is_null($token)) $token = lemon_csrf_token();
  return lemon_csrf_token_age($token) > $token['expiration_time'];
}

function lemon_csrf_require_valid_token($msg = 'Cross site request forgery detected. Request aborted', $token = null){
  if(is_null($token)) $token = lemon_csrf_token();
  $token_name = $token['name'];
  if(isset($_POST[$token_name], $_SESSION[$token_name]) && $_POST[$token_name] != $_SESSION[$token_name]) return false;//halt(HTTP_FORBIDDEN, $msg);
  return true;
}

# HELPERS

function html_form_token_field($token = null, $only_token = false){
  if(is_null($token)) $token = lemon_csrf_token();
  $token_value = is_array($token) ? $token['value'] : $token;
  return !$only_token ? '<input type="hidden" name="form_token" value="'.$token_value.'" id="form_token">' : $token_value;
}

function sanitize($items, $type = 'string')
{
    if( is_null($items) ) return null;
    if(!is_array($items)){
        $items = (array)$items;
    }
    foreach($items as $key=>$val)
    {
        $items[$key] = sanitizeItem($val, $type);
    }

    return count($items) === 1 ? current($items) : $items;
}

function sanitizeItem($var, $type = 'string')
{
    $flags = NULL;
    switch($type)
    {
        case 'url':
            $filter = FILTER_SANITIZE_URL;
            break;
        case 'int':
            $filter = FILTER_SANITIZE_NUMBER_INT;
            break;
        case 'float':
            $filter = FILTER_SANITIZE_NUMBER_FLOAT;
            $flags = FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND;
            break;
        case 'email':
            $var = substr($var, 0, 254);
            $filter = FILTER_SANITIZE_EMAIL;
            break;
        case 'string':
        default:
            $filter = FILTER_SANITIZE_STRING;
            $flags = FILTER_FLAG_NO_ENCODE_QUOTES;
            break;

    }
    $output = filter_var($var, $filter, $flags);
    return $output;
}

function guid()
{
    if (function_exists('com_create_guid') === true)
    {
        return trim(com_create_guid(), '{}');
    }

    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}