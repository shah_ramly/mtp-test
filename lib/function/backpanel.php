<?php
function navActive($currPath, $curr){
	if(is_array($curr)){
		if(in_array($currPath, $curr)){
			echo ' active';
		}
	}else{
		if($currPath == $curr){
			echo ' active';
		}
	}
}

function lstatus($status){
	switch($status){
		case 0:
		  	return '<span class="badge badge-danger">Disabled</span>';
		 	break;
		case 1:
		  	return '<span class="badge badge-success">Enabled</span>';
		 	break;
	}
}

function mstatus($status){
	switch($status){
		case 0:
		  	return '<span style="color:#FFA500">Inactive</span>';
		 	break;
		case 1:
		  	return '<span style="color:#25AB16">Active</span>';
		 	break;
		case 9:
		  	return '<span style="color:#FF0000">Suspended</span>';
		 	break;
	}
}

function pstatus($status){
	switch($status){
		case 0:
		  	return '<span style="color:#FFA500">Inactive</span>';
		 	break;
		case 1:
		  	return '<span style="color:#25AB16">Active</span>';
		 	break;
		case 8:
		  	return '<span style="color:#FF0000">Out Of Stock</span>';
		 	break;
		case 9:
		  	return '<span style="color:#FF0000">Rejected</span>';
		 	break;
	}
}

function dateTimeFormat($date){
	$date = strtotime($date);
	$date = date("Y/m/d g:i:s A", $date);
	return $date;
}
?>
