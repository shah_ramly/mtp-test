<?php
// here we register all events we need for the system, so we could trigger it later in the code using Event::trigger('event.name')
// Event::register('task.created', 'task_created')
// Event::trigger('task.created')  -> will call a function 'task_created' we declared in Event::register for the same event name 'task.created'
// which would call it in Notification class method '__callStatic' in switch block

// User related events
Event::register('user.first.login', 'first_login');
Event::register('user.profile.complete', 'profile_complete');

//Task related events
Event::register('task.created', 'task_created');
Event::register('task.updated', 'task_updated');
Event::register('task.deleted', 'task_deleted');
Event::register('task.cancelled', 'task_cancelled');
Event::register('task.applied', 'task_applied');
Event::register('task.payment', 'task_payment_made');
Event::register('task.assigned', 'task_assigned');
Event::register('task.marked.completed', 'task_marked_completed');
Event::register('task.accepted', 'task_accepted');
Event::register('task.rejected', 'task_rejected');
Event::register('task.offer.accepted', 'task_offer_accepted');
Event::register('task.offer.retracted', 'task_offer_retracted');
Event::register('task.application.retracted', 'task_application_retracted');
Event::register('task.disputed', 'task_disputed');
Event::register('task.payment.request', 'task_payment_request');
Event::register('task.fund.released', 'task_fund_released');
Event::register('task.new.question', 'task_new_question');
Event::register('task.new.answer', 'task_new_answer');
Event::register('task.review', 'task_review');
Event::register('task.review.reminder', 'task_review_reminder');
Event::register('task.full.refund', 'task_full_refund');
Event::register('task.expiring', 'task_expiring');
Event::register('task.50.milestone', 'task_50_milestone');

//Job related events
Event::register('job.applied', 'job_applied');
Event::register('job.new.question', 'job_new_question');
Event::register('job.new.answer', 'job_new_answer');
Event::register('job.closed', 'job_closed');

//Chat related events
Event::register('chat.new.message', 'chat_new_message');
