<?php
function languageList(){
    $languages = ['Afrikaans','Albanian','Amharic','Arabic','Armenian','Azerbaijani','Basque','Belarusian','Bengali','Bosnian','Bulgarian','Catalan','Cebuano','Chichewa','Chinese','Corsican','Croatian','Czech','Danish','Dutch','English','Esperanto','Estonian','Filipino','Finnish','French','Frisian','Galician','Georgian','German','Greek','Gujarati','Haitian Creole','Hausa','Hawaiian','Hebrew','Hindi','Hmong','Hungarian','Icelandic','Igbo','Indonesian','Irish','Italian','Japanese','Javanese','Kannada','Kazakh','Khmer','Kinyarwanda','Korean','Kurdish (Kurmanji)','Kyrgyz','Lao','Latin','Latvian','Lithuanian','Luxembourgish','Macedonian','Malagasy','Malay','Malayalam','Maltese','Maori','Marathi','Mongolian','Myanmar (Burmese)','Nepali','Norwegian','Odia (Oriya)','Pashto','Persian','Polish','Portuguese','Punjabi','Romanian','Russian','Samoan','Scots Gaelic','Serbian','Sesotho','Shona','Sindhi','Sinhala','Slovak','Slovenian','Somali','Spanish','Sundanese','Swahili','Swedish','Tajik','Tamil','Tatar','Telugu','Thai','Turkish','Turkmen','Ukrainian','Urdu'];

    return $languages;
}

function expectedEarningList(){
    $values[99] = 'Less than RM100';
    $values[500] = 'RM101 - RM500';
    $values[999] = 'RM501 - RM999';
    $values[1000] = 'RM1000 and above';

    return $values;
}

function roleList(){
    $roles = [
        'analytics_report' => 'Analytics Report',
        'individual' => 'Individual Management',
        'company' => 'Company Management',
        'task_organic' => 'Task Management / Organic',
        'task_aggregate' => 'Task Management / Aggregate',
        'job_organic' => 'Job Management / Organic',
        'job_aggregate' => 'Job Management / Aggregate',
        'master_data_country' => 'Master Data Controller / Countries',
        'master_data_state' => 'Master Data Controller / States',
        'master_data_city' => 'Master Data Controller / Cities',
        'master_data_language' => 'Master Data Controller / Language',
        'master_data_industry_existing' => 'Master Data Controller / Industries / Existing',
        'master_data_industry_entered' => 'Master Data Controller / Industries / Entered By User',
        'master_data_industry_language' => 'Master Data Controller / Industries / Language',
        'master_data_category_existing' => 'Master Data Controller / Categories / Existing',
        'master_data_category_entered' => 'Master Data Controller / Categories / Entered By User',
        'master_data_category_language' => 'Master Data Controller / Categories / Language',
        'master_data_skill_existing' => 'Master Data Controller / Skills / Existing',
        'master_data_skill_entered' => 'Master Data Controller / Skills / Entered By User',
        'master_data_skill_language' => 'Master Data Controller / Skills / Language',
        'master_data_interest_existing' => 'Master Data Controller / Interests / Existing',
        'master_data_interest_entered' => 'Master Data Controller / Interests / Entered By User',
        'master_data_interest_language' => 'Master Data Controller / Interests / Language',
        'subscription' => 'Subscription Management',
        'master_config_admin_admin' => 'Master Configurations / Admin / Admin',
        'master_config_admin_role' => 'Master Configurations / Admin / Roles',
        'master_config_system' => 'Master Configurations / System Settings',
        'master_config_notification' => 'Master Configurations / Notifications',
    ];

    return $roles;
}
?>
