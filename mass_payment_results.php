<?php
/*require __DIR__ . '/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;*/

set_include_path( get_include_path().PATH_SEPARATOR."..");

include_once __DIR__ . "/lib/class/SimpleXLSX.php";
include_once __DIR__ . "/lib/config.php";

$files = glob(__DIR__ . DIRECTORY_SEPARATOR . 'mass_pymt' . DIRECTORY_SEPARATOR . '*_result.xlsx');

foreach ($files as $index => $file){
    $filename = explode(DIRECTORY_SEPARATOR, $file);
    $filename = end($filename);
    if( strpos($filename, '_') === 0 )
        unset($files[$index]);
}

$filename = end($files);

if ( $xlsx = SimpleXLSX::parse($filename) ) {
    // Produce array keys from the array values of 1st array element
    $header_values = $result = [];
    foreach ( $xlsx->rows() as $k => $r ) {
        if ( $k === 0 ) {
            $header_values = array_map('strtolower', array_map(function($str){ return str_replace([' ', '-'], '_', $str);}, $r));
            continue;
        }
        $result[] = array_combine( $header_values, $r );
    }

    $oldname_path = explode(DIRECTORY_SEPARATOR, $filename);
    $oldname = end($oldname_path);
    array_pop($oldname_path);
    $new_filename =  implode(DIRECTORY_SEPARATOR, $oldname_path) . DIRECTORY_SEPARATOR .'_'. $oldname;

    rename($filename, $new_filename);
}

$mtp_filename = str_replace('_result', '', $filename);
$mtp_filename = str_replace('mass_pymt', 'mass_pymt_mtp', $mtp_filename);

if ( $xlsx = SimpleXLSX::parse($mtp_filename) ) {
    // Produce array keys from the array values of 1st array element
    $header_values = $original = [];
    foreach ( $xlsx->rows() as $k => $r ) {
        if ( $k === 0 ) {
            $header_values = array_map('strtolower', array_map(function($str){ return str_replace([' ', '-'], '_', $str);}, $r));
            continue;
        }
        $original[] = array_combine( $header_values, $r );
    }
}

if( isset($result, $original) ){
    $final = [];
    foreach ($original as $key => $row){
        foreach ($result as $rkey => &$rrow){
            if( (float)$row['amount'] === (float)$rrow['amount'] && $row['name'] === $rrow['name'] && $row['email'] === $rrow['email'] && !isset($rrow['done']) ){
                $final[] = ['id' => $row['id'], 'amount' => $row['amount'], 'status' => $rrow['status']];
                $rrow['done'] = true;
                break;
            }
        }
    }
}

if( isset($final) ){
    $query = '';
    foreach ($final as $row){
        $query .= "UPDATE `payment_release` SET `status` = '{$row['status']}' WHERE `id` = '{$row['id']}' AND `amount` = {$row['amount']} LIMIT 1;";
    }

    $db->queryOrDie($query);
}

exit(0);