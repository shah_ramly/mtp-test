			
			<div class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-chart">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-12 col-header-filter">
                                        <h2 class="card-title">Analytics Report</h2>
                                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
                                            <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                            </svg>
                                        </button>
                                        <ul class="col-account-header">
                                            <li class="dropdown col-right-flex col-account">
                                                <button type="button" class="btn btn-success btn-block btn-logout" onclick="location.href='<?php echo url_for('/logout'); ?>';">
                                                    <span class="plus-icon-lbl">Log Out</span>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body-backend card-body-backend-analytics">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="row">
                                            <!-- Task Overview -->
                                            <div class="col-xl-12 col-dashboard-top col-dashboard-task">
                                                <div class="col-dashboard-total-task">
                                                    <div class="sub-col-total-task total-all-task">
	                                                    <div class="progress-bar-total-task">
		                                                    <div class="sub-col-header-title">
			                                                    <div class="sub-col-title">Total Tasks</div>
			                                                    <div class="sub-col-val"><?php
                                                                    echo number_format($dashboard_data['tasks_total']) ?></div>
		                                                    </div>

		                                                    <div class="multiple-progress-bar-container">
			                                                    <div class="mutiple-progress-bar">
				                                                    <div class="progress-bar-left" title="<?php
                                                                    echo format_percentage($dashboard_data['organic_percentage']) ?>%"
				                                                         style="width:<?php
                                                                         echo $dashboard_data['organic_percentage'] ?>%;">
					                                                    <span class="progress-bar-val"><?php
                                                                            echo format_percentage($dashboard_data['organic_percentage']) ?>%</span>
				                                                    </div>
				                                                    <div class="progress-bar-right" title="<?php
                                                                    echo format_percentage($dashboard_data['aggregated_percentage']) ?>%"
				                                                         style="width:<?php
                                                                         echo $dashboard_data['aggregated_percentage'] ?>%;">
					                                                    <span class="progress-bar-val"><?php
                                                                            echo format_percentage($dashboard_data['aggregated_percentage']) ?>%</span>
				                                                    </div>
			                                                    </div>
			                                                    <div class="multiple-progress-bar-legend">
				                                                    <div class="progress-bar-legend legend-left">
					                                                    <span class="legend-color"></span>
					                                                    <span class="legend-value">Organic Tasks</span>
				                                                    </div>
				                                                    <div class="progress-bar-legend legend-right">
					                                                    <span class="legend-color"></span>
					                                                    <span class="legend-value">Aggregated Tasks</span>
				                                                    </div>
			                                                    </div>
		                                                    </div>
	                                                    </div>
	                                                    <div class="progress-bar-total-job">
		                                                    <div class="sub-col-header-title">
			                                                    <div class="sub-col-title">Total Jobs</div>
			                                                    <div class="sub-col-val"><?php
                                                                    echo number_format($dashboard_data['jobs_total']) ?></div>
		                                                    </div>

		                                                    <div class="multiple-progress-bar-container">
			                                                    <div class="mutiple-progress-bar">
				                                                    <div class="progress-bar-left" title="<?php
                                                                    echo number_format($dashboard_data['jobs_organic_percentage'], 2) ?>%"
				                                                         style="width:<?php
                                                                         echo number_format($dashboard_data['jobs_organic_percentage'], 2) ?>%;">
					                                                    <span class="progress-bar-val"><?php
                                                                            echo number_format($dashboard_data['jobs_organic_percentage'], 2) ?>%</span>
				                                                    </div>
				                                                    <div class="progress-bar-right" title="<?php
                                                                    echo number_format($dashboard_data['jobs_aggregated_percentage'], 2) ?>%"
				                                                         style="width:<?php
                                                                         echo number_format($dashboard_data['jobs_aggregated_percentage'], 2) ?>%;">
					                                                    <span class="progress-bar-val"><?php
                                                                            echo number_format($dashboard_data['jobs_aggregated_percentage'], 2) ?>%</span>
				                                                    </div>
			                                                    </div>
			                                                    <div class="multiple-progress-bar-legend">
				                                                    <div class="progress-bar-legend legend-left">
					                                                    <span class="legend-color"></span>
					                                                    <span class="legend-value">Organic Jobs</span>
				                                                    </div>
				                                                    <div class="progress-bar-legend legend-right">
					                                                    <span class="legend-color"></span>
					                                                    <span class="legend-value">Aggregated Jobs</span>
				                                                    </div>
			                                                    </div>
		                                                    </div>
	                                                    </div>
                                                    </div>
                                                    <div class="sub-col-task-right-panel">
                                                        <div class="sub-col-task-details-container">
                                                            <div class="sub-col-task-header-container">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-header-title">
                                                                        <div class="sub-col-title">Count</div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-header-title positive">
                                                                        <div class="sub-col-title">Last 7 Days</div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-header-title">
                                                                        <div class="sub-col-title">Last 30 Days</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="sub-col-task-organic-container">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-title">Organic Tasks</div>
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['organic']['total']) ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['organic']['last_7_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['organic']['last_7_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['organic']['last_7_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['organic']['last_7_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['organic']['last_30_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['organic']['last_30_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['organic']['last_30_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['organic']['last_30_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                            <div class="sub-col-task-aggregate-container" style="border-bottom-width:1px">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-title">Aggregated Tasks</div>
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['aggregated']['total']) ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['aggregated']['last_7_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['aggregated']['last_7_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['aggregated']['last_7_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['aggregated']['last_7_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['aggregated']['last_30_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['aggregated']['last_30_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['aggregated']['last_30_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['aggregated']['last_30_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                            <div class="sub-col-task-organic-container">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-title">Organic Jobs</div>
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['organic_jobs']['total']) ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['organic_jobs']['last_7_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['organic_jobs']['last_7_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['organic_jobs']['last_7_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['organic_jobs']['last_7_days_percentage']) ?>%</div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['organic_jobs']['last_30_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['organic_jobs']['last_30_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['organic_jobs']['last_30_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['organic_jobs']['last_30_days_percentage']) ?>%</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="sub-col-task-aggregate-container">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-title">Aggregated Jobs</div>
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['aggregated_jobs']['total']) ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['aggregated_jobs']['last_7_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['aggregated_jobs']['last_7_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['aggregated_jobs']['last_7_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['aggregated_jobs']['last_7_days_percentage']) ?>%</div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['aggregated_jobs']['last_30_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['aggregated_jobs']['last_30_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['aggregated_jobs']['last_30_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['aggregated_jobs']['last_30_days_percentage']) ?>%</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="sub-col-task-grand-total-container">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-title">Total</div>
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['tasks_total']+$dashboard_data['jobs_total']) ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['organic']['last_7_days'] + $dashboard_data['aggregated']['last_7_days']+$dashboard_data['organic_jobs']['last_7_days'] + $dashboard_data['aggregated_jobs']['last_7_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['tasks_total_last_7_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['tasks_total_last_7_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['tasks_total_last_7_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['organic']['last_30_days'] + $dashboard_data['aggregated']['last_30_days']+$dashboard_data['organic_jobs']['last_30_days'] + $dashboard_data['aggregated_jobs']['last_30_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['tasks_total_last_30_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['tasks_total_last_30_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['tasks_total_last_30_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="sub-col-revenue-container">
                                                            <div class="sub-col-task-header-container">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-header-title">
                                                                        <div class="sub-col-title">Revenue (RM)</div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-header-title positive">
                                                                        <div class="sub-col-title">Last 7 Days</div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-header-title">
                                                                        <div class="sub-col-title">Last 30 Days</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="sub-col-rev-collection-container">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-title">Receipts</div>
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['collection']['total'], 2) ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['collection']['last_7_days'], 2) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['collection']['last_7_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['collection']['last_7_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['collection']['last_7_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['collection']['last_30_days'], 2) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['collection']['last_30_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['collection']['last_30_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['collection']['last_30_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                            <div class="sub-col-rev-cost-container">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-title">Paid to Seekers</div>
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['cost']['total'], 2) ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['cost']['last_7_days'], 2) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['cost']['last_7_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['cost']['last_7_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['cost']['last_7_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['cost']['last_30_days'], 2) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['cost']['last_30_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['cost']['last_30_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['cost']['last_30_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                            <div class="sub-col-rev-senangpay-container">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-title">Paid to SenangPay</div>
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['sp_cost']['total'], 2) ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['sp_cost']['last_7_days'], 2) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['sp_cost']['last_7_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['sp_cost']['last_7_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['sp_cost']['last_7_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['sp_cost']['last_30_days'], 2) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['sp_cost']['last_30_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['sp_cost']['last_30_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['sp_cost']['last_30_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                            <div class="sub-col-rev-mtpcollection-container">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-title">Net Receipts</div>
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['collection']['total'] - $dashboard_data['cost']['total'] - $dashboard_data['sp_cost']['total'] , 2) ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['collection']['last_7_days'] - $dashboard_data['cost']['last_7_days'] - $dashboard_data['sp_cost']['last_7_days'] , 2) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['collection_total_last_7_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['collection_total_last_7_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['collection_total_last_7_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['collection']['last_30_days'] - $dashboard_data['cost']['last_30_days'] - $dashboard_data['sp_cost']['last_30_days'] , 2) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['collection_total_last_30_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['collection_total_last_30_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['collection_total_last_30_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Task Overview -->

                                            <!-- User Overview -->
                                            <div class="col-xl-12 col-dashboard-top col-dashboard-task">
                                                <div class="col-dashboard-total-task">
                                                    <div class="sub-col-total-task total-all-user">
	                                                    <div class="progress-bar-total-user">
                                                            <div class="sub-col-header-title">
                                                                <div class="sub-col-title">Total User</div>
                                                                <div class="sub-col-val"><?php echo number_format($dashboard_data['users_total']) ?></div>
                                                            </div>
                                                            <div class="multiple-progress-bar-container">
                                                                <div class="mutiple-progress-bar">
                                                                    <div class="progress-bar-left" title="<?php echo format_percentage($dashboard_data['individual_percentage']) ?>%" style="width:<?php echo $dashboard_data['individual_percentage'] ?>%;">
                                                                        <span class="progress-bar-val"><?php echo format_percentage($dashboard_data['individual_percentage']) ?>%</span>
                                                                    </div>
                                                                    <div class="progress-bar-right" title="<?php echo format_percentage($dashboard_data['company_percentage']) ?>%" style="width:<?php echo $dashboard_data['company_percentage'] ?>%;">
                                                                        <span class="progress-bar-val"><?php echo format_percentage($dashboard_data['company_percentage']) ?>%</span>
                                                                    </div>
                                                                </div>
                                                                <div class="multiple-progress-bar-legend">
                                                                    <div class="progress-bar-legend legend-left">
                                                                        <span class="legend-color"></span>
                                                                        <span class="legend-value">Individual</span>
                                                                    </div>
                                                                    <div class="progress-bar-legend legend-right">
                                                                        <span class="legend-color"></span>
                                                                        <span class="legend-value">Company</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="sub-col-task-right-panel">
                                                        <div class="sub-col-user-details-container">
                                                            <div class="sub-col-task-header-container">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-header-title">
                                                                        <div class="sub-col-title">Count</div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-header-title positive">
                                                                        <div class="sub-col-title">Last 7 Days</div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-header-title">
                                                                        <div class="sub-col-title">Last 30 Days</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="sub-col-task-organic-container">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-title">Individual</div>
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['individual']['total']) ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['individual']['last_7_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['individual']['last_7_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['individual']['last_7_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['individual']['last_7_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['individual']['last_30_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['individual']['last_30_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['individual']['last_30_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['individual']['last_30_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                            <div class="sub-col-task-aggregate-container">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-title">Company</div>
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['company']['total']) ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['company']['last_7_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['company']['last_7_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['company']['last_7_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['company']['last_7_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['company']['last_30_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['company']['last_30_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['company']['last_30_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['company']['last_30_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                            <div class="sub-col-task-grand-total-container">
                                                                <div class="sub-col-total-task total-all-task-total">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-title">Total</div>
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['individual']['total']+$dashboard_data['company']['total']) ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-7days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['individual']['last_7_days'] + $dashboard_data['company']['last_7_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['users_total_last_7_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['users_total_last_7_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['users_total_last_7_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                                <div class="sub-col-total-task total-all-task-30days">
                                                                    <div class="sub-col-val-flex">
                                                                        <div class="sub-col-val"><?php echo number_format($dashboard_data['individual']['last_30_days'] + $dashboard_data['company']['last_30_days']) ?></div>
                                                                        <div class="sub-col-percentage <?php echo $dashboard_data['users_total_last_30_days_percentage'] >= 0 ? 'positive' : 'negative' ?>"><?php echo $dashboard_data['users_total_last_30_days_percentage'] > 0 ? '+' : '' ?><?php echo format_percentage($dashboard_data['users_total_last_30_days_percentage']) ?>%</div>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="sub-col-geographic-distribution-container">
                                                            <div class="col-dashboard-header">
                                                                <h2 class="section-title">Geographic Distribution</h2>
                                                                <div class="col-analytics-dashboard-header-right header-filter-wrapper">
                                                                    <div class="dropdown dropdown-filter-container">
                                                                        <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            <label class="filter-btn-lbl">Filter</label> <span class="drop-val">All</span>
                                                                        </button>
                                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                            <a class="chart-type dropdown-item active" data-type="all" href="#">All</a>
                                                                            <a class="chart-type dropdown-item" data-type="individual" href="#">Individual</a>
                                                                            <a class="chart-type dropdown-item" data-type="company" href="#">Company</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="pie-chart-flex">
                                                                <div class="pie-chart-container pie-chart-col-left">
                                                                    <canvas id="country_piechart_canvas" width="100" height="100"></canvas>
                                                                </div>
                                                                <div class="pie-chart-container pie-chart-col-right">
                                                                    <canvas id="state_piechart_canvas" width="100" height="100"></canvas>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- User Overview -->

                                            <div class="col-xl-12 col-backend-top">
                                                <div class="col-analytics-dashboard-header">
                                                    <div class="col-analytics-dashboard-header-right header-filter-wrapper">
                                                        <div class="dropdown dropdown-filter-container">
                                                            <button class="btn btn-dropdown-label dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <label class="filter-btn-lbl">Filter</label> <span class="drop-val">By <?php echo ucwords($interval); ?></span>
                                                            </button>
                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                <a class="dropdown-item<?php echo $interval == 'day' ? ' active' : ''; ?>" href="<?php echo url_for('/analytics-report'); ?>">By Day</a>
                                                                <a class="dropdown-item<?php echo $interval == 'month' ? ' active' : ''; ?>" href="<?php echo url_for('/analytics-report'); ?>?interval=month">By Month</a>
                                                                <a class="dropdown-item<?php echo $interval == 'year' ? ' active' : ''; ?>" href="<?php echo url_for('/analytics-report'); ?>?interval=year">By Year</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-backend-top-wrapper">
                                                    <div class="row">
                                                        <div class="col-xl-12 col-myprofile">
                                                            <div class="row">
                                                                <div class="col-xl-12 col-report-dashboard-top-left">
                                                                    <div class="col-report-dashboard-top-left-wrapper">
                                                                        <div class="col-report-dashboard-header">
                                                                            <div class="col-report-dashboard-header-left">
                                                                                <div class="report-dashboard-title"><?php echo $interval == 'day' ? 'Today' : ucwords($interval) . 'ly'; ?> Total Earnings</div>
                                                                                <div class="report-dashboard-val">RM<?php echo $total['earning']; ?></div>
                                                                            </div>
                                                                            <div class="col-report-dashboard-header-right">
                                                                                <div class="col-icon">
                                                                                    <span class="arrow-up-right">
                                                                                        <svg class="bi bi-arrow-up-right" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                            <path fill-rule="evenodd" d="M6.5 4a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 .5.5v5a.5.5 0 0 1-1 0V4.5H7a.5.5 0 0 1-.5-.5z" />
                                                                                            <path fill-rule="evenodd" d="M12.354 3.646a.5.5 0 0 1 0 .708l-9 9a.5.5 0 0 1-.708-.708l9-9a.5.5 0 0 1 .708 0z" />
                                                                                        </svg>
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-stats">
                                                                                    <div class="stats-filter"><?php echo $interval == 'day' ? 'Today' : 'Last ' . ($interval == 'month' ? '30' : '365')  . ' days'; ?></div>
                                                                                    <div class="stats-percent"><?php echo $total['percent']; ?></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="barchart-wrapper">
                                                                            <div id="barchart_container">
                                                                                <canvas id="barchart_canvas"></canvas>
                                                                            </div>
                                                                            <?php if($interval == 'day'){ ?>
                                                                            <?php
                                                                                $current_interval = date('Y-m-d', strtotime($year . '-' . $month . '-' . $day));
                                                                                $prev_interval = date('Y-m-d', strtotime($current_interval . ' -1 day'));
                                                                                $next_interval = date('Y-m-d', strtotime($current_interval . ' +1 day'));

                                                                                $newparam = $param;
                                                                                $newparam['year'] = date('Y', strtotime($prev_interval));
                                                                                $newparam['month'] = date('m', strtotime($prev_interval));
                                                                                $newparam['day'] = date('d', strtotime($prev_interval));
                                                                                $prev_link = url_for('/') . '?' . http_build_query($newparam);

                                                                                $newparam = $param;
                                                                                $newparam['year'] = date('Y', strtotime($next_interval));
                                                                                $newparam['month'] = date('m', strtotime($next_interval));
                                                                                $newparam['day'] = date('d', strtotime($next_interval));
                                                                                $next_link = strtotime($next_interval) >= time() ? '' : url_for('/') . '?' . http_build_query($newparam);
                                                                            ?>
                                                                            <div class="btn-calendar-fiscal">
                                                                                <button class="btn btn-link btn-border" id="download-button" onclick="location.href='<?php echo $prev_link; ?>'"><i class="tim-icons icon-minimal-left"></i></button>
                                                                                <span class="fiscal-title"><?php echo date($cms->settings['date_format'], strtotime($year . '-' . $month . '-' . $day)); ?></span>
                                                                                <button class="btn btn-link btn-border<?php echo $next_link ? '' : ' disabled'; ?>" id="download-button"<?php echo $next_link ? 'onclick="location.href=\'' . $next_link . '\'"' : ' disabled'; ?>><i class="tim-icons icon-minimal-right"></i></button>
                                                                            </div>
                                                                            <?php } ?>
                                                                            <?php if($interval == 'month'){ ?>
                                                                            <?php
                                                                                $current_interval = date('Y-m-d', strtotime($year . '-' . $month . '-01'));
                                                                                $prev_interval = date('Y-m-d', strtotime($current_interval . ' -1 month'));
                                                                                $next_interval = date('Y-m-d', strtotime($current_interval . ' +1 month'));

                                                                                $newparam = $param;
                                                                                $newparam['year'] = date('Y', strtotime($prev_interval));
                                                                                $newparam['month'] = date('m', strtotime($prev_interval));
                                                                                unset($newparam['day']);
                                                                                $prev_link = url_for('/') . '?' . http_build_query($newparam);

                                                                                $newparam = $param;
                                                                                $newparam['year'] = date('Y', strtotime($next_interval));
                                                                                $newparam['month'] = date('m', strtotime($next_interval));
                                                                                unset($newparam['day']);
                                                                                $next_link = strtotime($next_interval) >= time() ? '' : url_for('/') . '?' . http_build_query($newparam);
                                                                            ?>
                                                                            <div class="btn-calendar-fiscal">
                                                                                <button class="btn btn-link btn-border" id="download-button" onclick="location.href='<?php echo $prev_link; ?>'"><i class="tim-icons icon-minimal-left"></i></button>
                                                                                <span class="fiscal-title"><?php echo date('F Y', strtotime(date($year . '-' . $month . '-01'))); ?></span>
                                                                                <button class="btn btn-link btn-border<?php echo $next_link ? '' : ' disabled'; ?>" id="download-button"<?php echo $next_link ? 'onclick="location.href=\'' . $next_link . '\'"' : ' disabled'; ?>><i class="tim-icons icon-minimal-right"></i></button>
                                                                            </div>
                                                                            <?php } ?>
                                                                            <?php if($interval == 'year'){ ?>
                                                                            <?php
                                                                                $current_interval = date('Y-m-d', strtotime($year . '-01-01'));
                                                                                $prev_interval = date('Y-m-d', strtotime($current_interval . ' -1 year'));
                                                                                $next_interval = date('Y-m-d', strtotime($current_interval . ' +1 year'));

                                                                                $newparam = $param;
                                                                                $newparam['year'] = date('Y', strtotime($prev_interval));
                                                                                unset($newparam['month']);
                                                                                unset($newparam['day']);
                                                                                $prev_link = url_for('/') . '?' . http_build_query($newparam);

                                                                                $newparam = $param;
                                                                                $newparam['year'] = date('Y', strtotime($next_interval));
                                                                                unset($newparam['month']);
                                                                                unset($newparam['day']);
                                                                                $next_link = strtotime($next_interval) >= time() ? '' : url_for('/') . '?' . http_build_query($newparam);
                                                                            ?>
                                                                            <div class="btn-calendar-fiscal">
                                                                                <button class="btn btn-link btn-border" id="download-button" onclick="location.href='<?php echo $prev_link; ?>'"><i class="tim-icons icon-minimal-left"></i></button>
                                                                                <span class="fiscal-title"><?php echo $year; ?></span>
                                                                                <button class="btn btn-link btn-border<?php echo $next_link ? '' : ' disabled'; ?>" id="download-button"<?php echo $next_link ? 'onclick="location.href=\'' . $next_link . '\'"' : ' disabled'; ?>><i class="tim-icons icon-minimal-right"></i></button>
                                                                            </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php /*
                                                                <div class="col-xl-3 col-report-dashboard-top-right">
                                                                    <div class="col-report-dashboard-top-right-wrapper">
                                                                        <div class="col-report-dashboard-monthly-header">
                                                                            <div class="report-dashboard-monthly-title"><?php echo $interval == 'day' ? 'Today' : ucwords($interval) . 'ly'; ?> Sales</div>
                                                                        </div>
                                                                        <!--<svg id="donut_chart_svg"></svg>-->
                                                                        <div id="donutchart_container">
                                                                            <div class="donutchart-center-container">
                                                                                <div class="donutchart-center-lbl">Total Earnings</div>
                                                                                <div class="donutchart-center-val">RM<?php echo $total['earning']; ?></div>
                                                                            </div>
                                                                            <canvas id="doChart"></canvas>
                                                                        </div>
                                                                        <div class="donutchart-legend-container">
                                                                            <div class="donutchart-legend-flex">
                                                                                <div class="donutchart-legend-col">
                                                                                    <span class="dot-icon dot-orange"></span>
                                                                                    <div class="donutchart-lbl-container">
                                                                                        <div class="dot-lbl">SenangPay</div>
                                                                                        <div class="donutchart-legend-val">42%</div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="donutchart-legend-col">
                                                                                    <span class="dot-icon dot-blue"></span>
                                                                                    <div class="donutchart-lbl-container">
                                                                                        <div class="dot-lbl">SenangPay</div>
                                                                                        <div class="donutchart-legend-val">58%</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                */ ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-12 col-dashboard-btm col-dashboard-total-task-posted">
                                                <div class="col-dashboard col-vert-flex">
                                                    <div class="col-dashboard-top-content">
                                                        <div class="sub-col-title">Total Task Posted</div>
                                                        <div class="sub-col-content">
                                                            <div class="sub-two-rows-flex">
                                                                <div class="sub-two-rows-icon">
                                                                    <span class="sub-analytics-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M14 2H6a2 2 0 0 0-2 2v16c0 1.1.9 2 2 2h12a2 2 0 0 0 2-2V8l-6-6z"/><path d="M14 3v5h5M16 13H8M16 17H8M10 9H8"/></svg>
                                                                    </span>
                                                                </div>
                                                                <div class="sub-two-rows-num">
                                                                    <div class="sub-two-rows-val"><?php echo $total['task_posted']; ?></div>
                                                                    <div class="sub-two-rows-desc"><?php echo $interval == 'day' ? 'Today' : ucwords($interval) . 'ly'; ?></div>
                                                                </div>
                                                            </div>
                                                            <div class="linechart-container">
                                                                <canvas id="taskposted_chart_canvas"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-12 col-dashboard-btm col-dashboard-total-task-completed">
                                                <div class="col-dashboard col-vert-flex">
                                                    <div class="col-dashboard-top-content">
                                                        <div class="sub-col-title">Total Task Completed</div>
                                                        <div class="sub-col-content">
                                                            <div class="sub-two-rows-flex">
                                                                <div class="sub-two-rows-icon">
                                                                    <span class="sub-analytics-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M14 2H6a2 2 0 0 0-2 2v16c0 1.1.9 2 2 2h12a2 2 0 0 0 2-2V8l-6-6z"/><path d="M14 3v5h5M16 13H8M16 17H8M10 9H8"/></svg>
                                                                    </span>
                                                                </div>
                                                                <div class="sub-two-rows-num">
                                                                    <div class="sub-two-rows-val"><?php echo $total['task_completed']; ?></div>
                                                                    <div class="sub-two-rows-desc"><?php echo $interval == 'day' ? 'Today' : ucwords($interval) . 'ly'; ?></div>
                                                                </div>
                                                            </div>
                                                            <div class="linechart-container">
                                                                <canvas id="taskcompleted_chart_canvas"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-12 col-dashboard-btm col-dashboard-total-user-register">
                                                <div class="col-dashboard col-vert-flex">
                                                    <div class="col-dashboard-top-content">
                                                        <div class="sub-col-title">Total User Registered</div>
                                                        <div class="sub-col-content">
                                                            <div class="sub-two-rows-flex">
                                                                <div class="sub-two-rows-icon">
                                                                    <span class="sub-analytics-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                                                    </span>
                                                                </div>
                                                                <div class="sub-two-rows-num">
                                                                    <div class="sub-two-rows-val"><?php echo $total['user_registered']; ?></div>
                                                                    <div class="sub-two-rows-desc"><?php echo $interval == 'day' ? 'Today' : ucwords($interval) . 'ly'; ?></div>
                                                                </div>
                                                            </div>
                                                            <div class="linechart-container">
                                                                <canvas id="registeruser_chart_canvas"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-12 col-dashboard-btm col-dashboard-total-company-register">
                                                <div class="col-dashboard col-vert-flex">
                                                    <div class="col-dashboard-top-content">
                                                        <div class="sub-col-title">Total Company Registered</div>
                                                        <div class="sub-col-content">
                                                            <div class="sub-two-rows-flex">
                                                                <div class="sub-two-rows-icon">
                                                                    <span class="sub-analytics-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                                                                    </span>
                                                                </div>
                                                                <div class="sub-two-rows-num">
                                                                    <div class="sub-two-rows-val"><?php echo $total['company_registered']; ?></div>
                                                                    <div class="sub-two-rows-desc"><?php echo $interval == 'day' ? 'Today' : ucwords($interval) . 'ly'; ?></div>
                                                                </div>
                                                            </div>
                                                            <div class="linechart-container">
                                                                <canvas id="registercompany_chart_canvas"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    <!-- Bar Chart Function -->
    <script>
        // Bar Chart Rounded Corner custom
        Chart.elements.Rectangle.prototype.draw = function() {

            var ctx = this._chart.ctx;
            var vm = this._view;
            var left, right, top, bottom, signX, signY, borderSkipped, radius;
            var borderWidth = vm.borderWidth;
            // Set Radius Here
            // If radius is large enough to cause drawing errors a max radius is imposed
            var cornerRadius = 12;

            if (!vm.horizontal) {
                // bar
                left = vm.x - vm.width / 2;
                right = vm.x + vm.width / 2;
                top = vm.y;
                bottom = vm.base;
                signX = 1;
                signY = bottom > top ? 1 : -1;
                borderSkipped = vm.borderSkipped || 'bottom';
            } else {
                // horizontal bar
                left = vm.base;
                right = vm.x;
                top = vm.y - vm.height / 2;
                bottom = vm.y + vm.height / 2;
                signX = right > left ? 1 : -1;
                signY = 1;
                borderSkipped = vm.borderSkipped || 'left';
            }

            // Canvas doesn't allow us to stroke inside the width so we can
            // adjust the sizes to fit if we're setting a stroke on the line
            if (borderWidth) {
                // borderWidth shold be less than bar width and bar height.
                var barSize = Math.min(Math.abs(left - right), Math.abs(top - bottom));
                borderWidth = borderWidth > barSize ? barSize : borderWidth;
                var halfStroke = borderWidth / 2;
                // Adjust borderWidth when bar top position is near vm.base(zero).
                var borderLeft = left + (borderSkipped !== 'left' ? halfStroke * signX : 0);
                var borderRight = right + (borderSkipped !== 'right' ? -halfStroke * signX : 0);
                var borderTop = top + (borderSkipped !== 'top' ? halfStroke * signY : 0);
                var borderBottom = bottom + (borderSkipped !== 'bottom' ? -halfStroke * signY : 0);
                // not become a vertical line?
                if (borderLeft !== borderRight) {
                    top = borderTop;
                    bottom = borderBottom;
                }
                // not become a horizontal line?
                if (borderTop !== borderBottom) {
                    left = borderLeft;
                    right = borderRight;
                }
            }

            ctx.beginPath();
            ctx.fillStyle = vm.backgroundColor;
            ctx.strokeStyle = vm.borderColor;
            ctx.lineWidth = borderWidth;

            // Corner points, from bottom-left to bottom-right clockwise
            // | 1 2 |
            // | 0 3 |
            var corners = [
                [left, bottom],
                [left, top],
                [right, top],
                [right, bottom]
            ];

            // Find first (starting) corner with fallback to 'bottom'
            var borders = ['bottom', 'left', 'top', 'right'];
            var startCorner = borders.indexOf(borderSkipped, 0);
            if (startCorner === -1) {
                startCorner = 0;
            }

            function cornerAt(index) {
                return corners[(startCorner + index) % 4];
            }

            // Draw rectangle from 'startCorner'
            var corner = cornerAt(0);
            ctx.moveTo(corner[0], corner[1]);

            for (var i = 1; i < 4; i++) {
                corner = cornerAt(i);
                nextCornerId = i + 1;
                if (nextCornerId == 4) {
                    nextCornerId = 0
                }

                nextCorner = cornerAt(nextCornerId);

                width = corners[2][0] - corners[1][0];
                height = corners[0][1] - corners[1][1];
                x = corners[1][0];
                y = corners[1][1];

                var radius = cornerRadius;

                // Fix radius being too large
                if (radius > height / 2) {
                    radius = height / 2;
                }
                if (radius > width / 2) {
                    radius = width / 2;
                }

                ctx.moveTo(x + radius, y);
                ctx.lineTo(x + width - radius, y);
                ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
                ctx.lineTo(x + width, y + height - radius);
                ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
                ctx.lineTo(x + radius, y + height);
                ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
                ctx.lineTo(x, y + radius);
                ctx.quadraticCurveTo(x, y, x + radius, y);

            }

            ctx.fill();
            if (borderWidth) {
                ctx.stroke();
            }
        };
        // Bar Chart Rounded Corner custom

        window.onload = function() {
            var ctx = document.getElementById('barchart_canvas').getContext('2d');
            var gradient = ctx.createLinearGradient(0, 0, 0, 400);
            gradient.addColorStop(0, 'rgba(255,133,99,1)');
            gradient.addColorStop(1, 'rgba(237,89,43,1)');
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: <?php echo json_encode($labelList); ?>,
                    datasets: [{
                        data: <?php echo json_encode($valueList['earning']); ?>,
                        backgroundColor: [
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
							gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
							gradient,
							gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
                            gradient,
							gradient,
                            gradient,
                            gradient,
							gradient,
                            gradient,
                            gradient,
							gradient,
                            gradient,
                            gradient,
                        ],
                        barPercentage: 0.5,
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false,
                    },
                    title: {
                        display: false,
                    },
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false,
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                borderDash: [8, 4],
                                color: "#eeeeee",
                                drawBorder: false,
                                lineWidth: 1,
                            },
                            ticks: {
                                display: false,
                                drawBorder: false
                            },
                        }]
                    }
                }
            });

            
            
            
            var ctxtaskposted = document.getElementById('taskposted_chart_canvas').getContext('2d');
            window.myLineTaskPosted = new Chart(ctxtaskposted, {
                type: 'line',
                data: {
                    labels: <?php echo json_encode($labelList); ?>,
                    datasets: [{
                        data: <?php echo json_encode($valueList['task_posted']); ?>,
                        barPercentage: 0.5,
                        fillColor : "rgba(220,220,220,0)",
                        borderColor: 'rgba(0,123,255,1)', // Add custom color border (Line)
                        backgroundColor: 'transparent', // Add custom color background (Points and Fill)
                        borderWidth: 1 // Specify bar border width
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false,
                    },
                    title: {
                        display: true,
                    },
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false,
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                borderDash: [8, 4],
                                color: "#eeeeee",
                                drawBorder: false,
                                lineWidth: 1,
                            },
                            ticks: {
                                display: false,
                                drawBorder: false
                            },
                        }]
                    },
                    elements: {
                        line: {
                            tension: 0
                        }
                    }
                }
            });
            
            
            var ctxtaskcompleted = document.getElementById('taskcompleted_chart_canvas').getContext('2d');
            window.myLineTaskCompleted = new Chart(ctxtaskcompleted, {
                type: 'line',
                data: {
                    labels: <?php echo json_encode($labelList); ?>,
                    datasets: [{
                        data: <?php echo json_encode($valueList['task_completed']); ?>,
                        barPercentage: 0.5,
                        fillColor : "rgba(220,220,220,0)",
                        borderColor: 'rgba(0,195,188,1)', // Add custom color border (Line)
                        backgroundColor: 'transparent', // Add custom color background (Points and Fill)
                        borderWidth: 1 // Specify bar border width
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false,
                    },
                    title: {
                        display: true,
                    },
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false,
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                borderDash: [8, 4],
                                color: "#eeeeee",
                                drawBorder: false,
                                lineWidth: 1,
                            },
                            ticks: {
                                display: false,
                                drawBorder: false
                            },
                        }]
                    },
                    elements: {
                        line: {
                            tension: 0
                        }
                    }
                }
            });
            
            
            var ctxregisteruser = document.getElementById('registeruser_chart_canvas').getContext('2d');
            window.myLineRegisterUser = new Chart(ctxregisteruser, {
                type: 'line',
                data: {
                    labels: <?php echo json_encode($labelList); ?>,
                    datasets: [{
                        data: <?php echo json_encode($valueList['user_registered']); ?>,
                        barPercentage: 0.5,
                        fillColor : "rgba(220,220,220,0)",
                        borderColor: 'rgba(249,129,21,1)', // Add custom color border (Line)
                        backgroundColor: 'transparent', // Add custom color background (Points and Fill)
                        borderWidth: 1 // Specify bar border width
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false,
                    },
                    title: {
                        display: true,
                    },
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false,
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                borderDash: [8, 4],
                                color: "#eeeeee",
                                drawBorder: false,
                                lineWidth: 1,
                            },
                            ticks: {
                                display: false,
                                drawBorder: false
                            },
                        }]
                    },
                    elements: {
                        line: {
                            tension: 0
                        }
                    }
                    
                }
            });
            
            var ctxregistercompany = document.getElementById('registercompany_chart_canvas').getContext('2d');
            window.myLineRegisterCompany = new Chart(ctxregistercompany, {
                type: 'line',
                data: {
                    labels: <?php echo json_encode($labelList); ?>,
                    datasets: [{
                        data: <?php echo json_encode($valueList['company_registered']); ?>,
                        barPercentage: 0.5,
                        fillColor : "rgba(220,220,220,0)",
                        borderColor: 'rgba(148,60,0,1)', // Add custom color border (Line)
                        backgroundColor: 'transparent', // Add custom color background (Points and Fill)
                        borderWidth: 1 // Specify bar border width
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false,
                    },
                    title: {
                        display: true,
                    },
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false,
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                borderDash: [8, 4],
                                color: "#eeeeee",
                                drawBorder: false,
                                lineWidth: 1,
                            },
                            ticks: {
                                display: false,
                                drawBorder: false
                            },
                        }]
                    },
                    elements: {
                        line: {
                            tension: 0
                        }
                    }
                }
            });
            
        };
        
    </script>
    <!-- Bar Chart Function -->



    <!-- Donut Chart Function -->
    <script>
        var doughnut = document.getElementById("doChart");
        if( doughnut ) {
            var myDoughnutChart = new Chart(doughnut, {
                type: 'doughnut',
                data: {
                    labels: ["Credit Card", "FPX"],
                    datasets: [{
                        data: [4100, 2500],
                        backgroundColor: ['#3644ad', '#f0724c'],
                        borderColor: ['#3644ad', '#f0724c'],
                        hoverBorderWidth: 10,
                        hoverRadius: 1,
                        borderWidth: 0,
                    }]
                },
                options: {
                    defaultFontFamily: Chart.defaults.global.defaultFontFamily = "'Poppins'",
                    legend: {
                        display: false,
                        position: 'bottom'
                    },
                    cutoutPercentage: 70,
                    maintainAspectRatio: false,
                    responsive: true,
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];
                                var precentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                return precentage + "%";
                            }
                        }
                    }
                }
            });
        }

    </script>
    <!-- Donut Chart Function -->

    
    <!-- Pie Chart Function -->
    <script>
        var countryCanvas = document.getElementById("country_piechart_canvas");
        var stateCanvas = document.getElementById("state_piechart_canvas");
        var all_states = <?php echo json_encode($states, true) ?>;

        Chart.defaults.global.defaultFontFamily = "Tenon";
        Chart.defaults.global.defaultFontSize = 15;

        const countryData = {
            labels: [
                <?php foreach ($countries as $country){ echo "'{$country['name']}',"; } ?>
            ],
            datasets: [
                {
                    data: [<?php foreach ($countries as $country){ echo "{$country['total_count']},"; } ?>],
                    ids: [<?php foreach ($countries as $country){ echo "{$country['id']},"; } ?>],
                    backgroundColor: [
                        "#F56461",
                        "#15B6B6",
                    ],
                }]
        };

        const individualCountryData = {
            labels: [
                <?php foreach ($countries as $country){ echo "'{$country['name']}',"; } ?>
            ],
            datasets: [
                {
                    data: [<?php foreach ($countries as $country){ echo "{$country['individual_count']},"; } ?>],
                    ids: [<?php foreach ($countries as $country){ echo "{$country['id']},"; } ?>],
                    backgroundColor: [
                        "#F56461",
                        "#15B6B6",
                    ],
                }]
        };

        const companyCountryData = {
            labels: [
                <?php foreach ($countries as $country){ echo "'{$country['name']}',"; } ?>
            ],
            datasets: [
                {
                    data: [<?php foreach ($countries as $country){ echo "{$country['company_count']},"; } ?>],
                    ids: [<?php foreach ($countries as $country){ echo "{$country['id']},"; } ?>],
                    backgroundColor: [
                        "#F56461",
                        "#15B6B6",
                    ],
                }]
        };

        const stateData = {
            labels: [
                <?php foreach ($states['132'] as $state){ echo "'{$state['name']}',"; } ?>
            ],
            datasets: [
                {
                    data: [<?php foreach ($states['132'] as $state){ echo "'{$state['total_count']}',"; } ?> ],
                    backgroundColor: [
                        "#F58A1F",
                        "#F56461",
                        "#15B6B6",
                        "#28C8E5",
                        "#53B857",
                        "#9A4398",
                        "#51007C",
                        "#093B94",
                        "#F58A1F",
                        "#F56461",
                        "#15B6B6",
                        "#28C8E5",
                        "#53B857",
                        "#9A4398",
                        "#51007C",
                        "#093B94",
                    ],
                }]
        };

        const individualStateData = {
            labels: [
                <?php foreach ($states['132'] as $state){ echo "'{$state['name']}',"; } ?>
            ],
            datasets: [
                {
                    data: [<?php foreach ($states['132'] as $state){ echo "'{$state['individual_count']}',"; } ?> ],
                    backgroundColor: [
                        "#F58A1F",
                        "#F56461",
                        "#15B6B6",
                        "#28C8E5",
                        "#53B857",
                        "#9A4398",
                        "#51007C",
                        "#093B94",
                        "#F58A1F",
                        "#F56461",
                        "#15B6B6",
                        "#28C8E5",
                        "#53B857",
                        "#9A4398",
                        "#51007C",
                        "#093B94",
                    ],
                }]
        };

        const companyStateData = {
            labels: [
                <?php foreach ($states['132'] as $state){ echo "'{$state['name']}',"; } ?>
            ],
            datasets: [
                {
                    data: [<?php foreach ($states['132'] as $state){ echo "'{$state['company_count']}',"; } ?> ],
                    backgroundColor: [
                        "#F58A1F",
                        "#F56461",
                        "#15B6B6",
                        "#28C8E5",
                        "#53B857",
                        "#9A4398",
                        "#51007C",
                        "#093B94",
                        "#F58A1F",
                        "#F56461",
                        "#15B6B6",
                        "#28C8E5",
                        "#53B857",
                        "#9A4398",
                        "#51007C",
                        "#093B94",
                    ],
                }]
        };

        var pieChart = new Chart(countryCanvas, {
            type: 'pie',
            data: countryData,
            options: {
                legend: {
                    display: false
                },
                title: {
                    display: true,
                    text: 'Country',
                    position: 'bottom',
                    fontSize: 18,
                    fontColor: "#00003f"
                }
            },
            
        });
        var pieChart2 = new Chart(stateCanvas, {
            type: 'pie',
            data: stateData,
            options: {
                legend: {
                    display: false
                },
                title: {
                    display: true,
                    text: 'By State',
                    position: 'bottom',
                    fontSize: 18,
                    fontColor: "#00003f"
                }
            },
        });

        $(countryCanvas).on('click', function(e){
           var activePoint = pieChart.getElementsAtEvent(e);
           if(activePoint.length){
               var index = activePoint[0]['_index'];
               var states = all_states[pieChart.data.datasets[0].ids[index]];
               var labels = [];
               var data = [];
               if( states ) {
                   var type = $('.chart-type.dropdown-item.active').data('type');
                   if( type === 'all' ) {
                       for (var i in states) {
                           labels.push(states[i]['name']);
                           data.push(states[i]['total_count']);
                       }
                   }else if( type === 'company' ){
                       for (var i in states) {
                           labels.push(states[i]['name']);
                           data.push(states[i]['company_count']);
                       }
                   }else{
                       for (var i in states) {
                           labels.push(states[i]['name']);
                           data.push(states[i]['individual_count']);
                       }
                   }
               }else{
                   labels.push(activePoint[0]['_chart'].legend.legendItems[index].text);
                   data.push(activePoint[0]['_chart'].legend.chart.config.data.datasets[0].data[index]);
               }
               pieChart2.data.labels = labels;
               pieChart2.data.datasets[0].data = data;
               pieChart2.update();
               pieChart2.render();
           }
        });

        $('a.chart-type').on('click', function (e) {
            e.preventDefault();
            $(e.currentTarget).addClass('active').siblings().removeClass('active');
            $(e.currentTarget).parent().siblings().find('span.drop-val').text($(e.currentTarget).text());
            var type = $(e.currentTarget).data('type');
            var states = all_states['132'];
            var labels = [];
            var data = [];
            var c_data = [];
            var s_data = [];

            if( type === 'individual' ){
                // resetting states to its initial data
                for (var i in states) {
                    labels.push(states[i]['name']);
                    data.push(states[i]['individual_count']);
                }
                individualStateData.labels = labels;
                individualStateData.datasets[0].data = data;
	            //

                c_data = individualCountryData;
                s_data = individualStateData;
            }else if( type === 'company' ){
                // resetting states to its initial data
                for (var i in states) {
                    labels.push(states[i]['name']);
                    data.push(states[i]['company_count']);
                }
                companyStateData.labels = labels;
                companyStateData.datasets[0].data = data;
                //

                c_data = companyCountryData;
                s_data = companyStateData;
            }else{
                // resetting states to its initial data
                for (var i in states) {
                    labels.push(states[i]['name']);
                    data.push(states[i]['total_count']);
                }
                stateData.labels = labels;
                stateData.datasets[0].data = data;
                //

                c_data = countryData;
                s_data = stateData;
            }

            pieChart.data = c_data;
            pieChart2.data = s_data;

            pieChart.update();
            pieChart.render();
            pieChart2.update();
            pieChart2.render();
        });
    </script>
        

    <!-- Pie Chart Function -->
