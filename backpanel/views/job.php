<div class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-chart">
				<div class="card-header">
					<div class="row">
						<div class="col-sm-12 col-header-filter">
							<h2 class="card-title">Job Management</h2>
							<button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
							        data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
								<svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor"
								     xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd"
									      d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
								</svg>
							</button>
							<ul class="col-account-header">
								<li class="dropdown col-right-flex col-account">
									<button type="button" class="btn btn-success btn-block btn-logout"
									        onclick="location.href='<?php echo url_for('logout') ?>';">
										<span class="plus-icon-lbl">Log Out</span>
									</button>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card-body-backend-tab card-body-job">
					<ul class="nav nav-pills nav-pills-tabs" id="pills-tab" role="tablist">
						<?php if(in_array('job_organic', $cms->admin['permissions'])){ ?>
						<li class="nav-item">
							<a class="nav-link <?php echo (! isset($tab) || $tab === 'organic') ? 'active' : '' ?>" id="pills-job-organic-tab" data-toggle="pill" href="#pills-job-organic"
							   role="tab" aria-controls="pills-job-organic" aria-selected="false">Organic</a>
						</li>
						<?php } ?>
						<?php if(in_array('job_aggregate', $cms->admin['permissions'])){ ?>
						<li class="nav-item">
							<a class="nav-link <?php echo (isset($tab) && $tab === 'aggregate') ? 'active' : '' ?>" id="pills-job-aggregate-tab" data-toggle="pill"
							   href="#pills-job-aggregate" role="tab" aria-controls="pills-job-aggregate"
							   aria-selected="true">Aggregate</a>
						</li>
						<?php } ?>
					</ul>
					<div class="tab-content" id="pills-tabContent">
						<?php if(in_array('job_organic', $cms->admin['permissions'])){ ?>
						<div class="tab-pane fade show <?php echo (! isset($tab) || $tab === 'organic') ? 'active' : '' ?>" id="pills-job-organic" role="tabpanel">
							<div class="card-body-backend">
								<div class="col-xl-12">
									<div class="row">
										<div class="col-xl-12 col-backend-top">
											<div class="table-listing-header-container">
												<div class="table-listing-header-title">Job Listing</div>
											</div>
											<div class="col-backend-top-wrapper">
												<div class="row">
													<div class="col-xl-12 col-myprofile">
														<form id="organic-filters" action="<?php echo url_for('/job/organic') ?>" method="get">
														<div class="table-filter-col">
															<div class="form-group form-job-id">
																<input type="text"
																       class="form-control form-control-input"
																       value="<?php echo $tab === 'organic' && request('id') ? request('id') : '' ?>"
																       placeholder="Job ID" name="id">
															</div>
															<div class="form-group form-task-title">
																<input type="text"
																       class="form-control form-control-input"
																       value="<?php echo $tab === 'organic' && request('title') ? request('title') : '' ?>"
																       placeholder="Job Title" name="title">
															</div>
															<div class="form-group form-task-date form-task-posteddate">
																<div class="input-group input-group-datetimepicker date"
																     id="form_task_posteddate"
																     data-target-input="nearest">
																	<input type="text" name="posted"
																	       value="<?php echo $tab === 'organic' && request('posted') ? request('posted') : '' ?>"
																	       class="form-control datetimepicker-input"
																	       data-target="#form_task_posteddate"
																	       placeholder="Posted Date">
																	<span class="input-group-addon"
																	      data-target="#form_task_posteddate"
																	      data-toggle="datetimepicker">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
																</div>
															</div>
															<div class="form-group form-task-date form-task-closingdate">
																<div class="input-group input-group-datetimepicker date"
																     id="form_task_closingdate"
																     data-target-input="nearest">
																	<input type="text" name="closing"
																	       value="<?php echo $tab === 'organic' && request('closing') ? request('closing') : '' ?>"
																	       class="form-control datetimepicker-input"
																	       data-target="#form_task_closingdate"
																	       placeholder="Closing Date">
																	<span class="input-group-addon"
																	      data-target="#form_task_closingdate"
																	      data-toggle="datetimepicker">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
																</div>
															</div>
															<div class="form-actions">
																<button class="btn-search-public" type="submit">
																	<span class="btn-label">Search</span>
																	<span class="public-search-icon search-icon">
		                                                                <svg class="bi bi-search"
		                                                                     viewBox="0 0 16 16" fill="currentColor"
		                                                                     xmlns="http://www.w3.org/2000/svg">
		                                                                    <path fill-rule="evenodd"
		                                                                          d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
		                                                                    <path fill-rule="evenodd"
		                                                                          d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
		                                                                </svg>
		                                                            </span>
																</button>
															</div>
														</div>
														</form>
														<div class="table-listing-container">
															<table class="table" id="taskListingTbl">
																<thead>
																<tr>
																	<th class="col-check">
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox"
																			       class="custom-control-input"
																			       id="jobList_All">
																			<label class="custom-control-label label-txt-none"
																			       for="jobList_All"></label>
																		</div>
																	</th>
																	<th class="col-task-id">Job ID</th>
																	<th class="col-task-title">Job Title</th>
																	<th class="col-posted-date">Posted Date</th>
																	<th class="col-posted-date">Closing Date</th>
																	<th class="col-poster-detail">Poster Detail</th>
																	<th></th>
																</tr>
																</thead>
																<tbody>
                                                                <?php if( isset($jobs) && !empty($jobs) ): ?>
                                                                <?php foreach ($jobs as $index => $job): ?>
																<tr>
																	<td class="col-check">
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox"
																			       class="custom-control-input"
																			       id="jobList_<?php echo $index ?>">
																			<label class="custom-control-label label-txt-none"
																			       for="jobList_<?php echo $index ?>"></label>
																		</div>
																	</td>
																	<td class="info col-task-id"><?php echo $job['number'] ?></td>
																	<td class="info col-task-title"><?php echo $job['title'] ?></td>
																	<td class="info col-posted-date">
																		<?php echo date('d-M-Y', strtotime($job['created_at'])) ?>
																	</td>
																	<td class="info col-posted-date">
																		<?php echo $job['closed_at'] ? date('d-M-Y', strtotime($job['closed_at'])) : '-' ?>
																	</td>
																	<td class="info col-poster-detail">
																		<div class="col-name-comp-tbl">
																			<div class="profile-photo"><img
																						src="<?php echo imgCrop($job['photo'], 35, 35) ?>"
																						alt="Profile Photo"></div>
																			<div class="col-name-id">
																				<span class="ind-name">
																					<?php echo $job['company_name'] ?>
																				</span>
																			</div>
																		</div>
																	</td>
																	<td class="actions">
																		<div class="table-actions-container">
																			<button class="btn-icon-only btn-delete"
																			        type="submit">
                                                                                <span class="public-search-icon search-icon">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                         viewBox="0 0 24 24"
                                                                                         fill="none"
                                                                                         stroke="currentColor"
                                                                                         stroke-width="1.5"
                                                                                         stroke-linecap="round"
                                                                                         stroke-linejoin="round">
                                                                                        <polyline
                                                                                                points="3 6 5 6 21 6"></polyline>
                                                                                        <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                                                        <line x1="10"
                                                                                              y1="11"
                                                                                              x2="10"
                                                                                              y2="17"></line>
                                                                                        <line x1="14"
                                                                                              y1="11"
                                                                                              x2="14"
                                                                                              y2="17"></line>
                                                                                    </svg>
                                                                                </span>
																			</button>
																		</div>
																	</td>
																</tr>
																<?php endforeach ?>
																<?php endif ?>
																</tbody>
															</table>
														</div>
														<div class="table-listing-footer-pagination">
                                                            <?php echo partial('/partial/pagination.php', [
                                                                'url' => url_for('/job/organic'),
                                                                'format' => '/',
                                                                'current_page' => $jobs_current_page ?? 1,
                                                                'total_pages' => $jobs_total_pages ?? 1,
                                                                'query_string' =>  !empty($organic_query_params) ? '?' . $organic_query_params : ''
                                                            ]) ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
						<?php if(in_array('job_aggregate', $cms->admin['permissions'])){ ?>
						<div class="tab-pane fade show <?php echo (isset($tab) && $tab === 'aggregate') ? 'active' : '' ?>" id="pills-job-aggregate" role="tabpanel">
							<div class="card-body-backend">
								<div class="col-xl-12">
									<div class="row">
										<div class="col-xl-12 col-backend-top">
											<div class="table-listing-header-container">
												<div class="table-listing-header-title">Job Listing</div>
											</div>
											<div class="col-backend-top-wrapper">
												<div class="row">
													<div class="col-xl-12 col-myprofile">
														<form id="aggregate-filters" action="<?php echo url_for('/job/aggregate') ?>" method="get">
														<div class="table-filter-col">
															<div class="form-group form-job-id">
																<input type="text"
																       class="form-control form-control-input"
																       placeholder="Job ID"  name="id"
																       value="<?php echo $tab === 'aggregate' && request('id') ? request('id') : '' ?>">
															</div>
															<div class="form-group form-job-title">
																<input type="text"
																       class="form-control form-control-input"
																       placeholder="Job Title"  name="title"
																       value="<?php echo $tab === 'aggregate' && request('title') ? request('title') : '' ?>">
															</div>
															<div class="form-group form-job-agg-source">
																<select class="form-control form-control-input"
																        name="source">
																	<option value="">Select Aggregation Source</option>
                                                                    <?php if( isset($aggregate_sources) ): ?>
                                                                        <?php foreach ($aggregate_sources as $source): ?>
																			<option
																					<?php echo $tab === 'aggregate' && request('source') === $source ? 'selected' : '' ?>
																					value="<?php echo $source ?>"><?php echo ucwords($source) ?></option>
                                                                        <?php endforeach ?>
                                                                    <?php endif ?>
																</select>
															</div>
															<div class="form-group">
																<div class="input-group input-group-datetimepicker date"
																     id="form_job_posteddate"
																     data-target-input="nearest">
																	<input type="text" name="posted"
																	       value="<?php echo $tab === 'aggregate' && request('posted') ? request('posted') : '' ?>"
																	       class="form-control datetimepicker-input"
																	       data-target="#form_job_posteddate"
																	       placeholder="Posted Date">
																	<span class="input-group-addon"
																	      data-target="#form_job_posteddate"
																	      data-toggle="datetimepicker">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
																</div>
															</div>
															<div class="form-group">
																<div class="input-group input-group-datetimepicker date"
																     id="form_job_closingdate"
																     data-target-input="nearest">
																	<input type="text" name="closing"
																	       value="<?php echo $tab === 'aggregate' && request('closing') ? request('closing') : '' ?>"
																	       class="form-control datetimepicker-input"
																	       data-target="#form_job_closingdate"
																	       placeholder="Closing Date">
																	<span class="input-group-addon"
																	      data-target="#form_job_closingdate"
																	      data-toggle="datetimepicker">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
																</div>
															</div>
															<div class="form-group">
																<div class="input-group input-group-datetimepicker date"
																     id="form_job_aggregateddate"
																     data-target-input="nearest">
																	<input type="text" name="aggregated"
                                                                           value="<?php echo $tab === 'aggregate' && request('aggregated') ? request('aggregated') : '' ?>"
																	       class="form-control datetimepicker-input"
																	       data-target="#form_job_aggregateddate"
																	       placeholder="Aggregated Date">
																	<span class="input-group-addon"
																	      data-target="#form_job_aggregateddate"
																	      data-toggle="datetimepicker">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
																</div>
															</div>
															<div class="form-actions">
																<button class="btn-search-public" type="submit">
																	<span class="btn-label">Search</span>
																	<span class="public-search-icon search-icon">
		                                                                <svg class="bi bi-search"
		                                                                     viewBox="0 0 16 16" fill="currentColor"
		                                                                     xmlns="http://www.w3.org/2000/svg">
		                                                                    <path fill-rule="evenodd"
		                                                                          d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
		                                                                    <path fill-rule="evenodd"
		                                                                          d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
		                                                                </svg>
		                                                            </span>
																</button>
															</div>
														</div>
														</form>
														<div class="table-listing-container">
															<table class="table" id="taskListingTbl">
																<thead>
																<tr>
																	<th class="col-check">
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox"
																			       class="custom-control-input"
																			       id="jobList_All">
																			<label class="custom-control-label label-txt-none"
																			       for="jobList_All"></label>
																		</div>
																	</th>
																	<th class="col-task-id">Job ID</th>
																	<th class="col-task-title">Job Title</th>
																	<th class="col-posted-date">Posted Date</th>
																	<th class="col-close-date">Closing Date</th>
																	<th class="col-aggregated-source">Aggregation Source</th>
																	<th class="col-aggregated-date">Aggregated Date</th>
																	<th></th>
																</tr>
																</thead>
																<tbody>
                                                                <?php if( isset($external_jobs) && !empty($external_jobs) ): ?>
                                                                <?php foreach ($external_jobs as $index => $job): ?>
																<tr>
																	<td class="col-check">
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox"
																			       class="custom-control-input"
																			       id="jobAggList_<?php echo $index ?>">
																			<label class="custom-control-label label-txt-none"
																			       for="jobAggList_<?php echo $index ?>"></label>
																		</div>
																	</td>
																	<td class="info col-task-id"><?php echo $job['number'] ?></td>
																	<td class="info col-task-title"><?php echo $job['title'] ?></td>
																	<td class="info col-posted-date">
																		<?php echo date('d-M-Y', strtotime($job['created_at'])) ?>
																	</td>
																	<td class="info col-close-date">
                                                                        <?php echo $job['closed_at'] ? date('d-M-Y', strtotime($job['closed_at'])) : '-' ?>
																	</td>
																	<td class="info col-aggregated-source">
																		<?php echo ucwords($job['source']) ?>
																	</td>
																	<td class="info col-aggregated-date">
                                                                        <?php echo date('d-M-Y', strtotime($job['created_at'])) ?>
																	</td>
																	<td class="actions">
																		<div class="table-actions-container">
																			<button class="btn-icon-only btn-delete"
																			        type="submit">
	                                                                            <span class="public-search-icon search-icon">
	                                                                                <svg xmlns="http://www.w3.org/2000/svg"
	                                                                                     viewBox="0 0 24 24"
	                                                                                     fill="none"
	                                                                                     stroke="currentColor"
	                                                                                     stroke-width="1.5"
	                                                                                     stroke-linecap="round"
	                                                                                     stroke-linejoin="round">
	                                                                                    <polyline
	                                                                                            points="3 6 5 6 21 6"></polyline>
	                                                                                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
	                                                                                    <line x1="10"
	                                                                                          y1="11"
	                                                                                          x2="10"
	                                                                                          y2="17"></line>
	                                                                                    <line x1="14"
	                                                                                          y1="11"
	                                                                                          x2="14"
	                                                                                          y2="17"></line>
	                                                                                </svg>
	                                                                            </span>
																			</button>
																		</div>
																	</td>
																</tr>
																<?php endforeach ?>
																<?php endif ?>
																</tbody>
															</table>
														</div>
														<div class="table-listing-footer-pagination">
                                                            <?php echo partial('/partial/pagination.php', [
                                                                'url' => url_for('/job/aggregate'),
                                                                'format' => '/',
                                                                'current_page' => $external_jobs_current_page ?? 1,
                                                                'total_pages' => $external_jobs_total_pages ?? 1,
                                                                'query_string' =>  !empty($aggregate_query_params) ? '?' . $aggregate_query_params : ''
                                                            ]) ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(function(){
		$('.nav-pills-tabs li:first a').click();
	});

    window.addEventListener('load', function(){
        if ($('.date').length) {
            $('.date').datetimepicker({
                format: 'DD-MM-YYYY',
            });
        }

        $('#organic-filters, #aggregate-filters').on('submit', function(e){
            var inputs = $(e.currentTarget).find('input, select');
            inputs.each(function(idx, ele){
                if( ! $(ele).val().length ){
                    $(ele).prop('disabled', true);
                }
            });
        });
    });
</script>