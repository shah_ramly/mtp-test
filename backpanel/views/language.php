			
			<div class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-chart">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-12 col-header-filter">
                                        <h2 class="card-title">Master Data Controller</h2>
                                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
                                            <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                            </svg>
                                        </button>
                                        <ul class="col-account-header">
                                            <li class="dropdown col-right-flex col-account">
                                                <button type="button" class="btn btn-success btn-block btn-logout" onclick="location.href='<?php echo url_for('/logout'); ?>';">
                                                    <span class="plus-icon-lbl">Log Out</span>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body-backend card-body-backend-language">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="row">
                                            <div class="col-xl-12 col-backend-top">
                                                <div class="table-listing-header-container">
                                                    <div class="table-listing-header-title">Language</div>
                                                    <div class="table-listing-header-actions">
                                                        <button type="button" class="btn-icon-full btn-add" data-toggle="modal" data-target="#modal_add_language" data-type="add">
                                                            <span class="btn-label">Add Language</span>
                                                            <span class="btn-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                    <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                    <line x1="5" y1="12" x2="19" y2="12"></line>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-backend-top-wrapper">
                                                    <div class="row">
                                                        <div class="col-xl-12 col-myprofile">
                                                            <div class="table-listing-container">
                                                                <table class="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Language</th>
                                                                            <th></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php foreach($keys as $key){ ?>
                                                                        <tr>
                                                                            <td class="info info-title"><?php echo $key['title']; ?></td>
                                                                            <td class="info"></td>
                                                                            <td class="default"></td>
                                                                            <td class="actions">
                                                                                <div class="table-actions-container">
                                                                                <button class="btn-icon-only btn-edit" type="button" data-toggle="modal" data-target="#modal_add_language" data-type="edit" data-id="<?php echo $key['id']; ?>" data-code="<?php echo $key['code']; ?>">
                                                                                        <span class="public-search-icon search-icon">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                                                <path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path>
                                                                                                <polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon>
                                                                                            </svg>
                                                                                        </span>
                                                                                    </button>
                                                                                    <a href="<?php echo url_for('/language/' . $key['id']); ?>" class="btn-icon-only btn-edit mr-2" type="button">
                                                                                        <span class="public-search-icon search-icon">
                                                                                            <i class="fa fa-language"></i>
                                                                                        </span>
                                                                                    </a>
                                                                                    <?php /*
                                                                                    <button class="btn-icon-only btn-delete" type="submit">
                                                                                        <span class="public-search-icon search-icon">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                                                                <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                                                                <line x1="10" y1="11" x2="10" y2="17"></line>
                                                                                                <line x1="14" y1="11" x2="14" y2="17"></line>
                                                                                            </svg>
                                                                                        </span>
                                                                                    </button>
                                                                                    */ ?>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

    <!-- Modal - Add Language -->
    <div id="modal_add_language" class="modal modal-add-language modal-w-footer fade" aria-labelledby="modal_add_language" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="">
                <input type="hidden" name="id" value="">
                <div class="modal-header">Add Language</div>
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="modal-body-container">
                    <div class="form-group">
                            <input type="text" class="form-control form-control-input" name="title" placeholder="Enter language title" required="" autofocus="">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control form-control-input" name="code" placeholder="Enter language code" required="" autofocus="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <button type="submit" class="btn-icon-full btn-confirm" data-orientation="next">
                        <span class="btn-label">Submit</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <script>
    $(function(){
        $('#modal_add_language').on('show.bs.modal', function(e){
            form = $(this).find('form');
            type = $(e.relatedTarget).data('type');
            title = type == 'add' ? 'Add Language' : 'Edit Language';
            val = type == 'edit' ? $(e.relatedTarget).closest('tr').find('.info-title').text() : '';
            code = type == 'edit' ? $(e.relatedTarget).data('code') : '';
            $(this).find('.modal-header').text(title);
            form.find('[name="title"]').val(val);
            form.find('[name="code"]').val(code);
            id = type == 'edit' ? $(e.relatedTarget).data('id') : '';
            formID = type == 'add' ? 'addLanguage' : 'editLanguage';
            form.attr('id', formID);
            form.find('[name="id"]').val(id);
        });
    });
    </script>
