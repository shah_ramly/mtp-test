<div class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-chart">
				<div class="card-header">
					<div class="row">
						<div class="col-sm-12 col-header-filter">
							<h2 class="card-title">Company Management</h2>
							<button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
							        data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
								<svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor"
								     xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd"
									      d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
								</svg>
							</button>
							<ul class="col-account-header">
								<li class="dropdown col-right-flex col-account">
									<button type="button" class="btn btn-success btn-block btn-logout"
									        onclick="location.href='<?php echo url_for('/logout') ?>';">
										<span class="plus-icon-lbl">Log Out</span>
									</button>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card-body-backend card-body-backend-company">
					<div class="row">
						<div class="col-xl-12">
							<div class="row">
								<div class="col-xl-12 col-backend-top">
									<div class="table-listing-header-container">
										<div class="table-listing-header-title">Company Listing</div>
										<div class="table-listing-header-actions">
											<!--<button type="button" class="btn-icon-full btn-add" data-toggle="modal" data-target="#modal_add_category">
																							<span class="btn-label">Add Category</span>
																							<span class="btn-icon">
																								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
																									<line x1="12" y1="5" x2="12" y2="19"></line>
																									<line x1="5" y1="12" x2="19" y2="12"></line>
																								</svg>
																							</span>
																						</button>-->
										</div>
									</div>
									<div class="col-backend-top-wrapper">
										<div class="row">
											<div class="col-xl-12 col-myprofile">
												<form method="GET">
												<div class="table-filter-col">
													<div class="form-group form-task-title">
														<input type="text" class="form-control form-control-input" placeholder="Company Name" name="company_name" value="<?php echo $param['company_name'] ?? ''; ?>">
													</div>
													<div class="form-group form-task-title">
														<input type="text" class="form-control form-control-input" placeholder="Company Email" name="email" value="<?php echo $param['email'] ?? ''; ?>">
													</div>
													<div class="form-group">
														<select class="form-control form-control-input country" placeholder="Country" name="country" data-target="state" data-selected="132">
															<option value="0">Country</option>
															<?php foreach($cms->countries() as $country){ ?>
															<option value="<?php echo $country['id']; ?>"<?php echo $param['country'] == $country['id'] ? ' selected' : ''; ?>><?php echo $country['name']; ?></option>
															<?php } ?>
														</select>
													</div>
													<div class="form-group">
														<select class="form-control form-control-input state" name="state" placeholder="State">
															<option value="">State</option>
															<?php foreach($cms->states($param['country']) as $state){ ?>
															<option value="<?php echo $state['id']; ?>"<?php echo $param['state'] == $state['id'] ? ' selected' : ''; ?>><?php echo $state['name']; ?></option>
															<?php } ?>
														</select>
													</div>
													<div class="form-actions">
														<button class="btn-search-public" type="submit">
															<span class="btn-label">Search</span>
															<span class="public-search-icon search-icon">
																<svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
																	<path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
																	<path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
																</svg>
															</span>
														</button>
													</div>
												</div>
												</form>
												<div class="table-listing-container">
													<table class="table">
														<thead>
															<tr>
																<th>Company Name</th>
																<th>PIC</th>
																<th>Location</th>
																<th>Email</th>
																<th>Contact No. Mobile</th>
																<th>Member Since</th>
																<th></th>
															</tr>
														</thead>
														<tbody>
															<?php if($keys){ foreach($keys as $key){ ?>
															<tr>
																<td class="info">
																	<div class="col-name-ind-tbl">
																		<div class="profile-photo"><img src="<?php echo imgCrop($key['photo'], 35, 35); ?>" alt="Profile Photo"></div>
																		<div class="col-name-id">
																			<span class="ind-name"><?php echo $key['name']; ?></span>
																			<span class="ind-id">#<?php echo 20100 + $key['id']; ?></span>
																		</div>
																	</div>
																</td>
																<td class="info"><?php echo $key['pic']; ?></td>
																<td class="info"><?php echo $key['location']; ?></td>
																<td class="info"><?php echo $key['email']; ?></td>
																<td class="info"><?php echo $key['mobile_number']; ?></td>
																<td class="info"><?php echo date($cms->settings['date_format'], strtotime($key['date_created'])); ?></td>
																<td class="actions">
																	<div class="table-actions-container">
																		<a href="<?php echo url_for('/company-detail/' . $key['id']); ?>" class="btn-txt-only btn-edit">View Detail</a>
																	</div>
																</td>
															</tr>
															<?php }} ?>
														</tbody>
													</table>
												</div>
												<div class="table-listing-footer-pagination">
													<div class="footer-total-page">Page <?php echo $pagination->page; ?> of <?php echo $pagination->totalPage; ?></div>
													<nav class="nav-tbl-pagination">
														<?php if($pagination->totalRecord){ ?>													
														<ul class="pagination">
															<?php echo $pagination->display(); ?>
														</ul>
														<?php } ?>
													</nav>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>