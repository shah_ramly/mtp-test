
			<div class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-chart">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-12 col-header-filter">
                                        <h2 class="card-title">Master Configurations</h2>
                                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
                                            <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                            </svg>
                                        </button>
                                        <ul class="col-account-header">
                                            <li class="dropdown col-right-flex col-account">
                                                <button type="button" class="btn btn-success btn-block btn-logout" onclick="location.href='<?php echo url_for('/logout'); ?>';">
                                                    <span class="plus-icon-lbl">Log Out</span>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body-backend-tab card-body-backend-language">
                                <ul class="nav nav-pills nav-pills-tabs nav-pills-cat-tabs" id="pills-tab" role="tablist">
                                    <?php if(in_array('master_config_admin_admin', $cms->admin['permissions'])){ ?>
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-cat-admin-tab" data-toggle="pill" href="#pills-cat-admin" role="tab" aria-controls="pills-cat-admin" aria-selected="true">User</a>
                                    </li>
                                    <?php } ?>
                                    <?php if(in_array('master_config_admin_role', $cms->admin['permissions'])){ ?>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-role-tab" data-toggle="pill" href="#pills-role" role="tab" aria-controls="pills-role" aria-selected="false">Roles</a>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content" id="pills-tabContent">
                                    <?php if(in_array('master_config_admin_admin', $cms->admin['permissions'])){ ?>
                                    <div class="tab-pane fade active show" id="pills-cat-admin" role="tabpanel">
                                        <div class="card-body-backend">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="row">
                                                        <div class="col-xl-12 col-backend-top">
                                                            <div class="table-listing-header-container">
                                                                <div class="table-listing-header-title">Admin</div>
                                                                <div class="table-listing-header-actions">
                                                                    <button type="button" class="btn-icon-full btn-add" onclick="location.href='<?php echo url_for('/admin/add'); ?>'">
                                                                        <span class="btn-label">Add User</span>
                                                                        <span class="btn-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                                <line x1="5" y1="12" x2="19" y2="12"></line>
                                                                            </svg>
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="col-backend-top-wrapper">
                                                                <div class="row">
                                                                    <div class="col-xl-12 col-admin">
                                                                        <div class="table-listing-container">
                                                                            <table class="table">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="col-name">Username</th>
                                                                                        <th class="col-state">Name</th>
                                                                                        <th class="col-state">Email</th>
                                                                                        <th class="col-state">Role</th>
                                                                                        <th class="col-state">Status</th>
                                                                                        <th class="col-actions"></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php foreach($keys as $key){  ?>
                                                                                    <tr>
                                                                                        <td class="info"><?php echo $key['admin_username']; ?></td>
                                                                                        <td class="info"><?php echo $key['admin_name']; ?></td>
                                                                                        <td class="info"><?php echo $key['admin_email']; ?></td>
                                                                                        <td class="info"><?php echo $key['role']; ?></td>
                                                                                        <td class="info"><?php echo lstatus($key['admin_status']); ?></td>
                                                                                        <td class="actions">
                                                                                            <div class="table-actions-container">
                                                                                                <a href="<?php echo url_for('/admin/' . $key['admin_id']); ?>" class="btn-icon-only btn-edit mr-2">
                                                                                                    <span class="public-search-icon search-icon">
                                                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                                                            <path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path>
                                                                                                            <polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon>
                                                                                                        </svg>
                                                                                                    </span>
                                                                                                </a>
                                                                                                <?php if($key['admin_id'] != '1'){ ?>
                                                                                                <button class="btn-icon-only btn-delete" type="button" data-id="<?php echo $key['admin_id']; ?>" data-type="admin">
                                                                                                    <span class="public-search-icon search-icon">
                                                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                                                                            <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                                                                        </svg>
                                                                                                    </span>
                                                                                                </button>
                                                                                                <?php } ?>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <?php } ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>    
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if(in_array('master_config_admin_role', $cms->admin['permissions'])){ ?>
                                    <div class="tab-pane fade" id="pills-role" role="tabpanel">
                                        <div class="card-body-backend">
                                            <div class="col-xl-12">
                                                <div class="row">
                                                    <div class="col-xl-12 col-backend-top">
                                                        <div class="table-listing-header-container">
                                                            <div class="table-listing-header-title">Admin Roles</div>
                                                            <div class="table-listing-header-actions">
                                                                <button type="button" class="btn-icon-full btn-add" onclick="location.href='<?php echo url_for('/admin-role/add'); ?>'">
                                                                    <span class="btn-label">Add Role</span>
                                                                    <span class="btn-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="col-backend-top-wrapper">
                                                            <div class="row">
                                                                <div class="col-xl-12 col-admin">
                                                                    <div class="table-listing-container">
                                                                        <table class="table">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th class="col-name">Title</th>
                                                                                    <th class="col-actions"></th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php foreach($roles as $key){  ?>
                                                                                <tr>
                                                                                    <td class="info"><?php echo $key['title']; ?></td>
                                                                                    <td class="actions">
                                                                                        <div class="table-actions-container">
                                                                                            <a href="<?php echo url_for('/admin-role/' . $key['id']); ?>" class="btn-icon-only btn-edit mr-2">
                                                                                                <span class="public-search-icon search-icon">
                                                                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                                                        <path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path>
                                                                                                        <polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon>
                                                                                                    </svg>
                                                                                                </span>
                                                                                            </a>
                                                                                            <button class="btn-icon-only btn-delete" type="button" data-id="<?php echo $key['id']; ?>" data-type="role">
                                                                                                <span class="public-search-icon search-icon">
                                                                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                                                        <polyline points="3 6 5 6 21 6"></polyline>
                                                                                                        <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                                                                        <line x1="10" y1="11" x2="10" y2="17"></line>
                                                                                                        <line x1="14" y1="11" x2="14" y2="17"></line>
                                                                                                    </svg>
                                                                                                </span>
                                                                                            </button>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <?php } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<script>
$(function(){
    $('.nav-pills-cat-tabs li:first a').click();

    if(window.location.hash) {
        $('a[href="' + window.location.hash + '"]').click();
    }
    $(document).on('click', '.btn-delete', function() {
        type = $(this).data('type');
        if (confirm('Are you sure you want to delete this ' + type + '?')) {
            id = $(this).data('id');

            $.ajax({
                type: 'POST',
                url: rootPath + (type == 'admin' ? 'admin' : 'admin-role') + '/delete',
                dataType: 'json',
                data: {
                    id: id
                },
                success: function(results) {
                    if (results.error == true) {
                        t('e', results.msg);
                    } else {
                        t('s', results.msg);
                        setTimeout(function() {
                            if(type == 'admin'){
                                window.location.hash = '';
                            }else{
                                window.location.hash = 'pills-role';
                            }
                            window.location.reload();
                        }, 1000);
                    }
                },
                error: function(error) {
                    ajax_error(error);
                },
            });
        }
    });
});
</script>