<div class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-chart">
				<div class="card-header">
					<div class="row">
						<div class="col-sm-12 col-header-filter">
							<h2 class="card-title">Individual Management</h2>
							<button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
							        data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
								<svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor"
								     xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd"
									      d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
								</svg>
							</button>
							<ul class="col-account-header">
								<li class="dropdown col-right-flex col-account">
									<button type="button" class="btn btn-success btn-block btn-logout"
									        onclick="location.href='<?php echo url_for('/logout') ?>';">
										<span class="plus-icon-lbl">Log Out</span>
									</button>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card-body-backend card-body-backend-individual">
					<div class="row">
						<div class="col-xl-12">
							<div class="row">
								<div class="col-xl-12 col-backend-top">
									<div class="table-listing-header-container">
										<div class="table-listing-header-title">Individual Listing</div>
										<div class="table-listing-header-actions"></div>
									</div>
									<div class="col-backend-top-wrapper">
										<div class="row">
											<div class="col-xl-12 col-myprofile">
												<form id="filters" action="<?php echo url_for('/individual') ?>" method="get">
												<div class="table-filter-col">

													<div class="form-group form-filter-name">
														<input type="text" class="form-control form-control-input"
														       name="name"
														       value="<?php echo request('name') ?? '' ?>"
														       placeholder="Name">
													</div>
													<div class="form-group form-filter-email">
														<input type="text" class="form-control form-control-input"
														       name="email"
														       value="<?php echo request('email') ?? '' ?>"
														       placeholder="Email">
													</div>
													<div class="form-group">
														<select class="form-control form-control-input"
														        placeholder="Country" name="country"
														        id="countries">
															<option value="">Country</option>
                                                            <?php foreach ($cms->countries() as $country ): ?>
																<option value="<?php echo $country['id'] ?>"
                                                                    <?php if(request('country') && request('country') === $country['id']): ?>
																		selected
                                                                    <?php endif ?>
																>
																	<?php echo $country['name'] ?>
																</option>
                                                            <?php endforeach ?>
														</select>
													</div>
													<div class="form-group">
														<select class="form-control form-control-input"
														        placeholder="State" name="state"
														        id="states"
														>
															<option value="">State</option>
                                                            <?php foreach($cms->states(132) as $state): ?>
																<option value="<?php echo $state['id'] ?>"
																        <?php if(request('state') && request('state') === $state['id']): ?>
																        selected
																		<?php endif ?>
																>
																	<?php echo $state['name'] ?>
																</option>
                                                            <?php endforeach; ?>
														</select>
													</div>
													<div class="form-actions">
														<button class="btn-search-public" type="submit">
															<span class="btn-label">Search</span>
															<span class="public-search-icon search-icon">
                                                                <svg class="bi bi-search"
                                                                     viewBox="0 0 16 16" fill="currentColor"
                                                                     xmlns="http://www.w3.org/2000/svg">
                                                                    <path fill-rule="evenodd"
                                                                          d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                                                    <path fill-rule="evenodd"
                                                                          d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                                                </svg>
                                                            </span>
														</button>
													</div>
												</div>
												</form>
												<div class="table-listing-container">
													<table class="table">
														<thead>
														<tr>
															<th>Name</th>
															<th>Location</th>
															<th>Email</th>
															<th>Contact No. Mobile</th>
															<th>Member Since</th>
															<th></th>
														</tr>
														</thead>
														<tbody>
														<?php if( isset($users) && !empty($users) ): ?>
														<?php foreach ($users as $user): ?>
														<tr>
															<td class="info">
																<div class="col-name-ind-tbl">
																	<div class="profile-photo"><img
																				src="<?php echo imgCrop($user['photo'], 35, 35) ?>"
																				alt="Profile Photo"></div>
																	<div class="col-name-id">
																		<span class="ind-name"><?php echo $user['name'] ?></span>
																		<span class="ind-id"><?php echo hash_name($user['name']) ?></span>
																	</div>
																</div>
															</td>
															<td class="info"><?php echo $user['location'] ?></td>
															<td class="info"><?php echo $user['email'] ?></td>
															<td class="info"><?php echo $user['mobile_number'] ?></td>
															<td class="info"><?php echo date($cms->settings['date_format'], strtotime($user['date_created'])) ?></td>
															<td class="actions">
																<div class="table-actions-container">
																	<a href="<?php echo url_for('/individual-detail', $user['id']) ?>"
																	   class="btn-txt-only btn-edit">View Detail</a>
																</div>
															</td>
														</tr>
														<?php endforeach ?>
														<?php endif ?>
														</tbody>
													</table>
												</div>
												<div class="table-listing-footer-pagination">
													<?php echo partial('/partial/pagination.php', [
															'url' => url_for('/individual'),
															'format' => '/',
                                                            'query_string' => !empty($query_params) ? '?' . $query_params : '',
													]) ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
    window.addEventListener('load', function () {
        var countries = $('#countries');
        if (countries) {
            countries.on('change', function (e) {
                var country_id = $(this).val();
                if (country_id) {
                    $.getJSON('<?php echo url_for('/ajax/country') ?>/' + country_id, function (response) {
                        if (response.error == false) {
                            $('#states').html(response.states);
                        }
                    });
                }
            });
        }

        $('#filters').on('submit', function(e){
            var inputs = $(e.currentTarget).find('input, select');
            inputs.each(function(idx, ele){
                if( ! $(ele).val().length ){
                    $(ele).prop('disabled', true);
                }
            });
        })
    });

</script>