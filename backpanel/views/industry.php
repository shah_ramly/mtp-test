
			<div class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-chart">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-12 col-header-filter">
                                        <h2 class="card-title">Master Data Controller</h2>
                                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
                                            <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                            </svg>
                                        </button>
                                        <ul class="col-account-header">
                                            <li class="dropdown col-right-flex col-account">
                                                <button type="button" class="btn btn-success btn-block btn-logout" onclick="location.href='<?php echo url_for('/logout'); ?>';">
                                                    <span class="plus-icon-lbl">Log Out</span>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body-backend-tab card-body-backend-industries">
                                <ul class="nav nav-pills nav-pills-tabs nav-pills-cat-tabs" id="pills-tab" role="tablist">
                                    <?php if(in_array('master_data_industry_existing', $cms->admin['permissions'])){ ?>
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-cat-existing-tab" data-toggle="pill" href="#pills-cat-existing" role="tab" aria-controls="pills-cat-existing" aria-selected="true">Existing</a>
                                    </li>
                                    <?php } ?>
                                    <?php if(in_array('master_data_industry_entered', $cms->admin['permissions'])){ ?>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-entered-tab" data-toggle="pill" href="#pills-entered" role="tab" aria-controls="pills-entered" aria-selected="false">Entered By User</a>
                                    </li>
                                    <?php } ?>
                                    <?php if(in_array('master_data_industry_language', $cms->admin['permissions'])){ ?>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-cat-language-tab" data-toggle="pill" href="#pills-cat-language" role="tab" aria-controls="pills-cat-language" aria-selected="false">Language</a>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content" id="pills-tabContent">
                                    <?php if(in_array('master_data_industry_existing', $cms->admin['permissions'])){ ?>
                                    <div class="tab-pane fade active show" id="pills-cat-existing" role="tabpanel">
                                        <div class="card-body-backend">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="row">
                                                        <div class="col-xl-12 col-backend-top">
                                                            <div class="table-listing-header-container">
                                                                <div class="table-listing-header-title">Industries</div>
                                                                <div class="table-listing-header-actions">
                                                                    <button class="btn-icon-only btn-delete btn-delete-all btn-disabled" type="button" disabled>
                                                                        <span class="public-search-icon search-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                                                <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                                                <line x1="10" y1="11" x2="10" y2="17"></line>
                                                                                <line x1="14" y1="11" x2="14" y2="17"></line>
                                                                            </svg>
                                                                        </span>
                                                                    </button>
                                                                    <button type="button" class="btn-icon-full btn-add" onclick="location.href='<?php echo url_for('/industry/add'); ?>'">
                                                                        <span class="btn-label">Add Industry</span>
                                                                        <span class="btn-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                                <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                                <line x1="5" y1="12" x2="19" y2="12"></line>
                                                                            </svg>
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="col-backend-top-wrapper">
                                                                <div class="row">
                                                                    <div class="col-xl-12 col-industry">
                                                                        <form method="GET">
                                                                        <div class="table-filter-col">
                                                                            <div class="form-group form-task-title">
                                                                                <input type="text" class="form-control form-control-input" placeholder="Title" name="title" value="<?php echo $param['title'] ?? ''; ?>">
                                                                            </div>
                                                                            <div class="form-actions">
                                                                                <button class="btn-search-public" type="submit">
                                                                                    <span class="btn-label">Search</span>
                                                                                    <span class="public-search-icon search-icon">
                                                                                        <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                                                                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                                                                        </svg>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                        </form>
                                                                        <div class="table-listing-container">
                                                                            <table class="table">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="col-actions" width="50">
                                                                                            <div class="custom-control custom-checkbox">
                                                                                                <input type="checkbox" class="custom-control-input" id="existing_All">
                                                                                                <label class="custom-control-label" for="existing_All"></label>
                                                                                            </div>
                                                                                        </th>
                                                                                        <th class="col-title">Title</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php foreach($keys as $key){  ?>
                                                                                    <tr>
                                                                                        <td class="info">
                                                                                            <div class="custom-control custom-checkbox">
                                                                                                <input type="checkbox" class="custom-control-input existing-selected" id="delete_<?php echo $key['id']; ?>" name="selected[]" value="<?php echo $key['id']; ?>">
                                                                                                <label class="custom-control-label" for="delete_<?php echo $key['id']; ?>"></label>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td class="info"><a href="<?php echo url_for('/industry/' . $key['id']); ?>"><?php echo $key['title']; ?></a></td>
                                                                                    </tr>
                                                                                    <?php } ?>
                                                                                </tbody>
                                                                            </table>
                                                                            <div class="table-listing-footer-pagination">
                                                                                <div class="footer-total-page">Page <?php echo $pagination->page; ?> of <?php echo $pagination->totalPage; ?></div>
                                                                                <nav class="nav-tbl-pagination">
                                                                                    <?php if($pagination->totalRecord){ ?>													
                                                                                    <ul class="pagination">
                                                                                        <?php echo $pagination->display(); ?>
                                                                                    </ul>
                                                                                    <?php } ?>
                                                                                </nav>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if(in_array('master_data_industry_entered', $cms->admin['permissions'])){ ?>
                                    <div class="tab-pane fade" id="pills-entered" role="tabpanel">
                                        <div class="card-body-backend">
                                            <div class="col-xl-12">
                                                <div class="row">
                                                    <div class="col-xl-12 col-backend-top">
                                                        <div class="table-listing-header-container">
                                                            <div class="table-listing-header-title">Industries</div>
                                                            <div class="table-listing-header-actions">
                                                                <button class="btn-icon-only btn-delete btn-delete-all btn-disabled" type="button" disabled>
                                                                    <span class="public-search-icon search-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                                            <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                                        </svg>
                                                                     </span>
                                                                </button>
                                                                <button type="button" class="btn-icon-full btn-add" data-toggle="modal" data-target="#modal_add_existing">
                                                                    <span class="btn-label">Add into existing</span>
                                                                    <span class="btn-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="col-backend-top-wrapper">
                                                            <div class="row">
                                                                <div class="col-xl-12 col-myprofile">
                                                                    <div class="table-listing-container">
                                                                        <table class="table table-skills-entered-by-user">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th class="col-check">
                                                                                        <div class="custom-control custom-checkbox">
                                                                                            <input type="checkbox" class="custom-control-input" id="entered_ALL">
                                                                                            <label class="custom-control-label" for="entered_ALL"></label>
                                                                                        </div>
                                                                                    </th>
                                                                                    <th>New Industries</th>
                                                                                    <th></th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php if($newKeys){ foreach($newKeys as $key){ ?>
                                                                                <tr>
                                                                                    <td class="info col-check">
                                                                                        <div class="custom-control custom-checkbox">
                                                                                            <input type="checkbox" class="custom-control-input entered-selected" id="industries_<?php echo $key['id']; ?>" name="selected[]" value="<?php echo $key['id']; ?>">
                                                                                            <label class="custom-control-label" for="industries_<?php echo $key['id']; ?>"></label>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td class="info"><?php echo $key['name']; ?></td>
                                                                                    <td class="actions">
                                                                                        <div class="table-actions-container">
                                                                                            <button class="btn-icon-only btn-edit" type="button" onclick="location.href='<?php echo url_for('/industry/' . $key['id']); ?>'">
                                                                                                <span class="public-search-icon search-icon">
                                                                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                                                        <path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path>
                                                                                                        <polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon>
                                                                                                    </svg>
                                                                                                </span>
                                                                                            </button>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <?php }}else{ ?>
                                                                                <tr>
                                                                                    <td colspan="3" class="text-center"><i>No data..</i></td>
                                                                                </tr>
                                                                                <?php } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if(in_array('master_data_industry_language', $cms->admin['permissions'])){ ?>
                                    <div class="tab-pane fade" id="pills-cat-language" role="tabpanel">
                                        <div class="card-body-backend">
                                            <div class="col-xl-12">
                                                <div class="row">
                                                    <div class="col-xl-12 col-backend-top">
                                                        <div class="col-backend-top-wrapper">
                                                            <form id="selfForm">
                                                            <div class="row">
                                                                <div class="col-xl-12 col-myprofile col-category-language">
                                                                    <div class="row row-title">
                                                                        <div class="col col-lang-left">English</div>
                                                                        <?php foreach($languages as $language){ ?>
                                                                        <div class="col col-lang-right"><?php echo $language['title']; ?></div>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="row">
                                                                        <?php foreach($allKeys as $industry){ ?>
                                                                        <div class="lang-row">
                                                                            <div class="lang-title">
                                                                                <div class="col-existing-lbl" data-toggle="collapse" aria-expanded="false">
                                                                                    <div class="col col-lang-left"><?php echo $industry['title']; ?></div>
                                                                                    <?php foreach($languages as $language){ ?>
                                                                                    <div class="col col-lang-right">
                                                                                        <input type="text" class="form-control form-control-input" name="industry[<?php echo $industry['id']; ?>][<?php echo $language['id']; ?>]" value="<?php echo $industry['lang'][$language['id']]; ?>">
                                                                                    </div>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php } ?>
                                                                    </div>

                                                                    <div class="footer-form-action">
                                                                        <button type="submit" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                                                                            <span class="btn-label">Save</span>
                                                                            <span class="btn-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                                    <polyline points="20 6 9 17 4 12"></polyline>
                                                                                </svg>
                                                                            </span>
                                                                        </button>
                                                                        <?php /*
                                                                        <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_task_od">
                                                                            <span class="btn-label">Cancel</span>
                                                                            <span class="btn-icon">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                                                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                                                                </svg>
                                                                            </span>
                                                                        </button>
                                                                        */ ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    <!-- Modal - Add Existing -->
    <div id="modal_add_existing" class="modal modal-add-existing modal-w-footer fade" aria-labelledby="modal_add_existing" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="add-into-existing">
                <div class="modal-header">Add to</div>
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="modal-body-container">
                        <div class="form-group">
                            <select class="form-control form-control-input" name="parent_id" required>
                                <option disabled="" selected="">Select Parent</option>
                                <option value="0">None</option>
                                <?php foreach($cms->getIndustries() as $key){ ?>
                                <option value="<?php echo $key['id']; ?>"><?php echo $key['title']; ?></option>
                                <?php } ?>
                            </select>
                         </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <button type="submit" class="btn-icon-full btn-confirm">
                        <span class="btn-label">Submit</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal -->

<script>
$(function(){
    $('.nav-pills-cat-tabs li:first a').click();
    
    $(document).on('change', '#existing_All', function(){
        if( $(this).prop('checked') ){
            $('.existing-selected').prop('checked', true);
            $('.btn-delete-all:visible').removeClass('btn-disabled').prop('disabled', false);
        }else{
            $('.existing-selected').prop('checked', false);
            $('.btn-delete-all:visible').addClass('btn-disabled').prop('disabled', true);
        }
    });

    $(document).on('change', '.existing-selected', function(){
        all = $('.existing-selected').length;
        checked = $('.existing-selected:checked').length;

        if(checked){
            $('.btn-delete-all:visible').removeClass('btn-disabled').prop('disabled', false);
        }else{
            $('.btn-delete-all:visible').addClass('btn-disabled').prop('disabled', true);
        }

        if( checked == all ){
            $('#existing_All').prop('checked', true);
        }else{
            $('#existing_All').prop('checked', false);
        }
    });

    $(document).on('click', '.btn-delete-all', function(){
        if( confirm('Are you sure you want to delete selected industry?') ){
            selected = $('[name="selected[]"]:checked:visible').map(function(_, el) {
                return $(el).val();
            }).get();

            $.ajax({
                type:'POST',
                url: rootPath + 'industry/delete',
                dataType: 'json',
                data: { selected: selected },
                success: function(results){
                    if(results.error == true){
                        t('e', results.msg);
                     }else{
                        t('s', results.msg);
                        setTimeout(function(){
                            window.location.reload();
                        }, 1000);
                    }
                },
                error: function(error){
                    ajax_error(error);
                },
            });
        }
    });
});
</script>