
			<div class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-chart">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-12 col-header-filter">
                                        <h2 class="card-title">Subscription Management</h2>
                                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
                                            <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"></path>
                                            </svg>
                                        </button>
                                        <ul class="col-account-header">
                                            <li class="dropdown col-right-flex col-account">
                                                <button type="button" class="btn btn-success btn-block btn-logout" onclick="location.href='backend-login.html';">
                                                    <span class="plus-icon-lbl">Log Out</span>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body-backend-tab card-body-subscription">
                                <ul class="nav nav-pills nav-pills-tabs" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-rc-myreviews-tab" data-toggle="pill" href="#pills-sb-individual" role="tab" aria-controls="pills-rc-myreviews" aria-selected="true">Individual</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-rc-writereview-tab" data-toggle="pill" href="#pills-sb-company" role="tab" aria-controls="pills-rc-writereview" aria-selected="false">Company</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-sb-individual" role="tabpanel">
                                        <div class="col-xl-12">
                                            <div class="row">
                                                <div class="col-xl-12 col-backend-top">
                                                    <div id="table-listing-container-subscription" class="table-listing-container table-listing-container-subscription">
                                                        <div class="col-xl-12">
                                                            <form id="selfForm">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="info col-package-lbl"></th>
                                                                        <th class="info col-package col-package-a">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="individual_plan_a" name="subscription[1][status]" value="1"<?php echo $keys['1']['status'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label" for="individual_plan_a">Plan A</label>
                                                                            </div>
                                                                        </th>
                                                                        <th class="info col-package col-package-b">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="individual_plan_b" name="subscription[2][status]" value="1"<?php echo $keys['2']['status'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label" for="individual_plan_b">Plan B</label>
                                                                            </div>
                                                                        </th>
                                                                        <th class="info col-package col-package-c">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="individual_plan_c" name="subscription[3][status]" value="1"<?php echo $keys['3']['status'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label" for="individual_plan_c">Plan C</label>
                                                                            </div>
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr class="row-subs-package">
                                                                        <td class="info col-package-lbl">Package Name</td>
                                                                        <td class="info col-package col-package-a">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-a" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-a" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">    
                                                                                <div class="tab-pane fade active show" id="pills-default-a" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[1][title]" value="<?php echo $keys['1']['title']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-a" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[1][lang][<?php echo $language['id']; ?>][title]" value="<?php echo $keys['1']['languages'][$language['id']]['title']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-b" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-b" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-b" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[2][title]" value="<?php echo $keys['2']['title']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-b" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[2][lang][<?php echo $language['id']; ?>][title]" value="<?php echo $keys['2']['languages'][$language['id']]['title']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-c" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-c" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-c" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[3][title]" value="<?php echo $keys['3']['title']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-c" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[3][lang][<?php echo $language['id']; ?>][title]" value="<?php echo $keys['3']['languages'][$language['id']]['title']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-package">
                                                                        <td class="info col-package-lbl">Package Description</td>
                                                                        <td class="info col-package col-package-a">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2a" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2a" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2a" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[1][description]" value="<?php echo $keys['1']['description']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2a" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[1][lang][<?php echo $language['id']; ?>][description]" value="<?php echo $keys['1']['languages'][$language['id']]['description']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2b" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2b" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2b" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[2][description]" value="<?php echo $keys['2']['description']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2b" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[2][lang][<?php echo $language['id']; ?>][description]" value="<?php echo $keys['2']['languages'][$language['id']]['description']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2c" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2c" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2c" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[3][description]" value="<?php echo $keys['3']['description']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2c" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[3][lang][<?php echo $language['id']; ?>][description]" value="<?php echo $keys['3']['languages'][$language['id']]['description']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-price">
                                                                        <td class="info col-package-lbl">Subscription Price</td>
                                                                        <td class="info col-package col-package-a">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend dropdown-filter-container">
                                                                                    <button class="btn btn-outline dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">RM</button>
                                                                                    <div class="dropdown-menu">
                                                                                        <a class="dropdown-item active" href="#">RM</a>
                                                                                        <a class="dropdown-item" href="#">SGD</a>
                                                                                        <a class="dropdown-item" href="#">USD</a>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="text" class="form-control form-control-input" placeholder="0.00" name="subscription[1][price]" value="<?php echo $keys['1']['price']; ?>">
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend dropdown-filter-container">
                                                                                    <button class="btn btn-outline dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">RM</button>
                                                                                    <div class="dropdown-menu">
                                                                                        <a class="dropdown-item active" href="#">RM</a>
                                                                                        <a class="dropdown-item" href="#">SGD</a>
                                                                                        <a class="dropdown-item" href="#">USD</a>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="text" class="form-control form-control-input" placeholder="0.00" name="subscription[2][price]" value="<?php echo $keys['2']['price']; ?>">
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend dropdown-filter-container">
                                                                                    <button class="btn btn-outline dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">RM</button>
                                                                                    <div class="dropdown-menu">
                                                                                        <a class="dropdown-item active" href="#">RM</a>
                                                                                        <a class="dropdown-item" href="#">SGD</a>
                                                                                        <a class="dropdown-item" href="#">USD</a>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="text" class="form-control form-control-input" placeholder="0.00" name="subscription[3][price]" value="<?php echo $keys['3']['price']; ?>">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-price">
                                                                        <td class="info col-package-lbl">Promotional Price</td>
                                                                        <td class="info col-package col-package-a">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend dropdown-filter-container">
                                                                                    <button class="btn btn-outline dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">RM</button>
                                                                                    <div class="dropdown-menu">
                                                                                        <a class="dropdown-item active" href="#">RM</a>
                                                                                        <a class="dropdown-item" href="#">SGD</a>
                                                                                        <a class="dropdown-item" href="#">USD</a>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="text" class="form-control form-control-input" placeholder="0.00" name="subscription[1][price_promo]" value="<?php echo $keys['1']['price_promo']; ?>">
                                                                            </div>
                                                                            <div class="pt-2">
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="pricePromoA" name="subscription[1][price_promo_status]" value="1"<?php echo $keys['1']['price_promo_status'] ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label label-txt-none mt-1 pl-4" for="pricePromoA">Enable</label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend dropdown-filter-container">
                                                                                    <button class="btn btn-outline dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">RM</button>
                                                                                    <div class="dropdown-menu">
                                                                                        <a class="dropdown-item active" href="#">RM</a>
                                                                                        <a class="dropdown-item" href="#">SGD</a>
                                                                                        <a class="dropdown-item" href="#">USD</a>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="text" class="form-control form-control-input" placeholder="0.00" name="subscription[2][price_promo]" value="<?php echo $keys['2']['price_promo']; ?>">
                                                                            </div>
                                                                            <div class="pt-2">
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="pricePromoB" name="subscription[2][price_promo_status]" value="1"<?php echo $keys['2']['price_promo_status'] ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label label-txt-none mt-1 pl-4" for="pricePromoB">Enable</label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend dropdown-filter-container">
                                                                                    <button class="btn btn-outline dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">RM</button>
                                                                                    <div class="dropdown-menu">
                                                                                        <a class="dropdown-item active" href="#">RM</a>
                                                                                        <a class="dropdown-item" href="#">SGD</a>
                                                                                        <a class="dropdown-item" href="#">USD</a>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="text" class="form-control form-control-input" placeholder="0.00" name="subscription[3][price_promo]" value="<?php echo $keys['3']['price_promo']; ?>">
                                                                            </div>
                                                                            <div class="pt-2">
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="pricePromoC" name="subscription[3][price_promo_status]" value="1"<?php echo $keys['3']['price_promo_status'] ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label label-txt-none mt-1 pl-4" for="pricePromoC">Enable</label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-label-title-sub">
                                                                        <td class="info col-package-lbl"><label>Plan Limits</label></td>
                                                                        <td class="info col-package col-package-a"></td>
                                                                        <td class="info col-package col-package-b"></td>
                                                                        <td class="info col-package col-package-c"></td>
                                                                    </tr>
                                                                    <tr class="row-subs-plan-limit row-sub-no-of-posting">
                                                                        <td class="info col-package-lbl">
                                                                            <p># of postings</p>
                                                                        </td>
                                                                        <td class="info col-package col-package-a">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control form-control-input posting-input" placeholder="0" autofocus="" name="subscription[1][max_post]" value="<?php echo $keys['1']['max_post']; ?>">
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control form-control-input posting-input" placeholder="0" autofocus="" name="subscription[2][max_post]" value="<?php echo $keys['2']['max_post']; ?>">
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control form-control-input posting-input" placeholder="0" autofocus="" name="subscription[3][max_post]" value="<?php echo $keys['3']['max_post']; ?>">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-plan-limit">

                                                                        <td class="info col-package-lbl">
                                                                            <p>Access to Billing</p>
                                                                        </td>
                                                                        <td class="info col-package col-package-a">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="billingAccessA" name="subscription[1][billing_access]" value="1"<?php echo $keys['1']['billing_access'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="billingAccessA"></label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="billingAccessB" name="subscription[2][billing_access]" value="1"<?php echo $keys['2']['billing_access'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="billingAccessB"></label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="billingAccessC" name="subscription[3][billing_access]" value="1"<?php echo $keys['3']['billing_access'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="billingAccessC"></label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-label-title-sub">
                                                                        <td class="info col-package-lbl"><label>Support</label></td>
                                                                        <td class="info col-package col-package-a"></td>
                                                                        <td class="info col-package col-package-b"></td>
                                                                        <td class="info col-package col-package-c"></td>
                                                                    </tr>
                                                                    <tr class="row-subs-plan-limit">

                                                                        <td class="info col-package-lbl">
                                                                            <p>24/7 email</p>
                                                                        </td>
                                                                        <td class="info col-package col-package-a">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="supportEmailA" name="subscription[1][support_email]" value="1"<?php echo $keys['1']['support_email'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="supportEmailA"></label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="supportEmailB" name="subscription[2][support_email]" value="1"<?php echo $keys['2']['support_email'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="supportEmailB"></label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="supportEmailC" name="subscription[3][support_email]" value="1"<?php echo $keys['3']['support_email'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="supportEmailC"></label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-plan-limit">

                                                                        <td class="info col-package-lbl">
                                                                            <p>Dedicated Account Manager</p>
                                                                        </td>
                                                                        <td class="info col-package col-package-a">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="supportAccountManagerA" name="subscription[1][support_account_manager]" value="1"<?php echo $keys['1']['support_account_manager'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="supportAccountManagerA"></label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="supportAccountManagerB" name="subscription[2][support_account_manager]" value="1"<?php echo $keys['2']['support_account_manager'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="supportAccountManagerB"></label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="supportAccountManagerC" name="subscription[3][support_account_manager]" value="1"<?php echo $keys['3']['support_account_manager'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="supportAccountManagerC"></label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-package">
                                                                        <td class="info col-package-lbl">Package Name 2</td>
                                                                        <td class="info col-package col-package-a">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-a2" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-a2" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">    
                                                                                <div class="tab-pane fade active show" id="pills-default-a2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[1][title_2]" value="<?php echo $keys['1']['title_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-a2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[1][lang][<?php echo $language['id']; ?>][title_2]" value="<?php echo $keys['1']['languages'][$language['id']]['title_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-b2" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-b2" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-b2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[2][title_2]" value="<?php echo $keys['2']['title_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-b2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[2][lang][<?php echo $language['id']; ?>][title_2]" value="<?php echo $keys['2']['languages'][$language['id']]['title_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-c2" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-c2" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-c2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[3][title_2]" value="<?php echo $keys['3']['title_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-c2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[3][lang][<?php echo $language['id']; ?>][title_2]" value="<?php echo $keys['3']['languages'][$language['id']]['title_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-package">
                                                                        <td class="info col-package-lbl">Package Description 2</td>
                                                                        <td class="info col-package col-package-a">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2a2" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2a2" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2a2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[1][description_2]" value="<?php echo $keys['1']['description_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2a2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[1][lang][<?php echo $language['id']; ?>][description_2]" value="<?php echo $keys['1']['languages'][$language['id']]['description_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2b2" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2b2" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2b2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[2][description_2]" value="<?php echo $keys['2']['description_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2b2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[2][lang][<?php echo $language['id']; ?>][description_2]" value="<?php echo $keys['2']['languages'][$language['id']]['description_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2c2" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2c2" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2c2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[3][description_2]" value="<?php echo $keys['3']['description_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2c2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[3][lang][<?php echo $language['id']; ?>][description_2]" value="<?php echo $keys['3']['languages'][$language['id']]['description_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-package">
                                                                        <td class="info col-package-lbl">Package Features</td>
                                                                        <td class="info col-package col-package-a">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2a2f" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2a2f" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2a2f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[1][features_2]" value="<?php echo $keys['1']['features_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2a2f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[1][lang][<?php echo $language['id']; ?>][features_2]" value="<?php echo $keys['1']['languages'][$language['id']]['features_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2b2f" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2b2f" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2b2f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[2][features_2]" value="<?php echo $keys['2']['features_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2b2f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[2][lang][<?php echo $language['id']; ?>][features_2]" value="<?php echo $keys['2']['languages'][$language['id']]['features_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2c2f" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2c2f" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2c2f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[3][features_2]" value="<?php echo $keys['3']['features_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2c2f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[3][lang][<?php echo $language['id']; ?>][features_2]" value="<?php echo $keys['3']['languages'][$language['id']]['features_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <div class="footer-form-action">
                                                                <button type="submit" class="btn-icon-full btn-confirm" data-orientation="next">
                                                                    <span class="btn-label">Save</span>
                                                                    <span class="btn-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                            <polyline points="20 6 9 17 4 12"></polyline>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                                <?php /*
                                                                <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_task_od">
                                                                    <span class="btn-label">Cancel</span>
                                                                    <span class="btn-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                            <line x1="18" y1="6" x2="6" y2="18"></line>
                                                                            <line x1="6" y1="6" x2="18" y2="18"></line>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                                */ ?>
                                                            </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade show" id="pills-sb-company" role="tabpanel">
                                        <div class="col-xl-12">
                                            <div class="row">
                                                <div class="col-xl-12 col-backend-top">
                                                    <div id="table-listing-container-subscription" class="table-listing-container table-listing-container-subscription">
                                                        <div class="col-xl-12">
                                                            <form id="selfForm">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="info col-package-lbl"></th>
                                                                        <th class="info col-package col-package-a">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="company_plan_a" name="subscription[4][status]" value="1"<?php echo $keys['4']['status'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label" for="company_plan_a">Plan A</label>
                                                                            </div>
                                                                        </th>
                                                                        <th class="info col-package col-package-b">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="company_plan_b" name="subscription[5][status]" value="1"<?php echo $keys['5']['status'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label" for="company_plan_b">Plan B</label>
                                                                            </div>
                                                                        </th>
                                                                        <th class="info col-package col-package-c">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="company_plan_c" name="subscription[6][status]" value="1"<?php echo $keys['6']['status'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label" for="company_plan_c">Plan C</label>
                                                                            </div>
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr class="row-subs-package">
                                                                        <td class="info col-package-lbl">Package name</td>
                                                                        <td class="info col-package col-package-a">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-d" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-d" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">    
                                                                                <div class="tab-pane fade active show" id="pills-default-d" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[4][title]" value="<?php echo $keys['4']['title']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-d" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[4][lang][<?php echo $language['id']; ?>][title]" value="<?php echo $keys['4']['languages'][$language['id']]['title']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-e" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-e" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">    
                                                                                <div class="tab-pane fade active show" id="pills-default-e" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[5][title]" value="<?php echo $keys['5']['title']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-e" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[5][lang][<?php echo $language['id']; ?>][title]" value="<?php echo $keys['5']['languages'][$language['id']]['title']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-f" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-f" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">    
                                                                                <div class="tab-pane fade active show" id="pills-default-f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[6][title]" value="<?php echo $keys['6']['title']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[6][lang][<?php echo $language['id']; ?>][title]" value="<?php echo $keys['6']['languages'][$language['id']]['title']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-package">
                                                                        <td class="info col-package-lbl">Package Description</td>
                                                                        <td class="info col-package col-package-a">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2d" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2d" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2d" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[4][description]" value="<?php echo $keys['4']['description']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2d" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[4][lang][<?php echo $language['id']; ?>][description]" value="<?php echo $keys['4']['languages'][$language['id']]['description']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2e" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2e" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2e" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[5][description]" value="<?php echo $keys['5']['description']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2e" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[5][lang][<?php echo $language['id']; ?>][description]" value="<?php echo $keys['5']['languages'][$language['id']]['description']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2f" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2f" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[6][description]" value="<?php echo $keys['6']['description']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[6][lang][<?php echo $language['id']; ?>][description]" value="<?php echo $keys['6']['languages'][$language['id']]['description']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-price">
                                                                        <td class="info col-package-lbl">Subscription Price</td>
                                                                        <td class="info col-package col-package-a">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend dropdown-filter-container">
                                                                                    <button class="btn btn-outline dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">RM</button>
                                                                                    <div class="dropdown-menu">
                                                                                        <a class="dropdown-item active" href="#">RM</a>
                                                                                        <a class="dropdown-item" href="#">SGD</a>
                                                                                        <a class="dropdown-item" href="#">USD</a>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="text" class="form-control form-control-input" placeholder="0.00" name="subscription[4][price]" value="<?php echo $keys['4']['price']; ?>">
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend dropdown-filter-container">
                                                                                    <button class="btn btn-outline dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">RM</button>
                                                                                    <div class="dropdown-menu">
                                                                                        <a class="dropdown-item active" href="#">RM</a>
                                                                                        <a class="dropdown-item" href="#">SGD</a>
                                                                                        <a class="dropdown-item" href="#">USD</a>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="text" class="form-control form-control-input" placeholder="0.00" name="subscription[5][price]" value="<?php echo $keys['5']['price']; ?>">
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend dropdown-filter-container">
                                                                                    <button class="btn btn-outline dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">RM</button>
                                                                                    <div class="dropdown-menu">
                                                                                        <a class="dropdown-item active" href="#">RM</a>
                                                                                        <a class="dropdown-item" href="#">SGD</a>
                                                                                        <a class="dropdown-item" href="#">USD</a>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="text" class="form-control form-control-input" placeholder="0.00" name="subscription[6][price]" value="<?php echo $keys['6']['price']; ?>">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-price">
                                                                        <td class="info col-package-lbl">Promotional Price</td>
                                                                        <td class="info col-package col-package-a">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend dropdown-filter-container">
                                                                                    <button class="btn btn-outline dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">RM</button>
                                                                                    <div class="dropdown-menu">
                                                                                        <a class="dropdown-item active" href="#">RM</a>
                                                                                        <a class="dropdown-item" href="#">SGD</a>
                                                                                        <a class="dropdown-item" href="#">USD</a>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="text" class="form-control form-control-input" placeholder="0.00" name="subscription[4][price_promo]" value="<?php echo $keys['4']['price_promo']; ?>">
                                                                            </div>
                                                                            <div class="pt-2">
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="pricePromoD" name="subscription[4][price_promo_status]" value="1"<?php echo $keys['4']['price_promo_status'] ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label label-txt-none mt-1 pl-4" for="pricePromoD">Enable</label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend dropdown-filter-container">
                                                                                    <button class="btn btn-outline dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">RM</button>
                                                                                    <div class="dropdown-menu">
                                                                                        <a class="dropdown-item active" href="#">RM</a>
                                                                                        <a class="dropdown-item" href="#">SGD</a>
                                                                                        <a class="dropdown-item" href="#">USD</a>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="text" class="form-control form-control-input" placeholder="0.00" name="subscription[5][price_promo]" value="<?php echo $keys['5']['price_promo']; ?>">
                                                                            </div>
                                                                            <div class="pt-2">
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="pricePromoE" name="subscription[5][price_promo_status]" value="1"<?php echo $keys['5']['price_promo_status'] ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label label-txt-none mt-1 pl-4" for="pricePromoE">Enable</label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend dropdown-filter-container">
                                                                                    <button class="btn btn-outline dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">RM</button>
                                                                                    <div class="dropdown-menu">
                                                                                        <a class="dropdown-item active" href="#">RM</a>
                                                                                        <a class="dropdown-item" href="#">SGD</a>
                                                                                        <a class="dropdown-item" href="#">USD</a>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="text" class="form-control form-control-input" placeholder="0.00" name="subscription[6][price_promo]" value="<?php echo $keys['6']['price_promo']; ?>">
                                                                            </div>
                                                                            <div class="pt-2">
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="pricePromoF" name="subscription[6][price_promo_status]" value="1"<?php echo $keys['6']['price_promo_status'] ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label label-txt-none mt-1 pl-4" for="pricePromoF">Enable</label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-label-title-sub">
                                                                        <td class="info col-package-lbl"><label>Plan Limits</label></td>
                                                                        <td class="info col-package col-package-a"></td>
                                                                        <td class="info col-package col-package-b"></td>
                                                                        <td class="info col-package col-package-c"></td>
                                                                    </tr>
                                                                    <tr class="row-subs-plan-limit row-sub-no-of-posting">

                                                                        <td class="info col-package-lbl">
                                                                            <p># of postings</p>
                                                                        </td>
                                                                        <td class="info col-package col-package-a">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control form-control-input posting-input" placeholder="0" autofocus="" name="subscription[4][max_post]" value="<?php echo $keys['4']['max_post']; ?>">
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control form-control-input posting-input" placeholder="0" autofocus="" name="subscription[5][max_post]" value="<?php echo $keys['5']['max_post']; ?>">
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control form-control-input posting-input" placeholder="0" autofocus="" name="subscription[6][max_post]" value="<?php echo $keys['6']['max_post']; ?>">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-plan-limit">

                                                                        <td class="info col-package-lbl">
                                                                            <p>Access to Billing</p>
                                                                        </td>
                                                                        <td class="info col-package col-package-a">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="billingAccessD" name="subscription[4][billing_access]" value="1"<?php echo $keys['4']['billing_access'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="billingAccessD"></label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="billingAccessE" name="subscription[5][billing_access]" value="1"<?php echo $keys['5']['billing_access'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="billingAccessE"></label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="billingAccessF" name="subscription[6][billing_access]" value="1"<?php echo $keys['6']['billing_access'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="billingAccessF"></label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-label-title-sub">
                                                                        <td class="info col-package-lbl"><label>Support</label></td>
                                                                        <td class="info col-package col-package-a"></td>
                                                                        <td class="info col-package col-package-b"></td>
                                                                        <td class="info col-package col-package-c"></td>
                                                                    </tr>
                                                                    <tr class="row-subs-plan-limit">

                                                                        <td class="info col-package-lbl">
                                                                            <p>24/7 email</p>
                                                                        </td>
                                                                        <td class="info col-package col-package-a">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="supportEmailD" name="subscription[4][support_email]" value="1"<?php echo $keys['4']['support_email'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="supportEmailD"></label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="supportEmailE" name="subscription[5][support_email]" value="1"<?php echo $keys['5']['support_email'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="supportEmailE"></label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="supportEmailF" name="subscription[6][support_email]" value="1"<?php echo $keys['6']['support_email'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="supportEmailF"></label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-plan-limit">

                                                                        <td class="info col-package-lbl">
                                                                            <p>Dedicated Account Manager</p>
                                                                        </td>
                                                                        <td class="info col-package col-package-a">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="supportAccountManagerD" name="subscription[4][support_account_manager]" value="1"<?php echo $keys['4']['support_account_manager'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="supportAccountManagerD"></label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="supportAccountManagerE" name="subscription[5][support_account_manager]" value="1"<?php echo $keys['5']['support_account_manager'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="supportAccountManagerE"></label>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" id="supportAccountManagerF" name="subscription[6][support_account_manager]" value="1"<?php echo $keys['6']['support_account_manager'] ? ' checked' : ''; ?>>
                                                                                <label class="custom-control-label label-txt-none" for="supportAccountManagerF"></label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-package">
                                                                        <td class="info col-package-lbl">Package name 2</td>
                                                                        <td class="info col-package col-package-a">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-d2" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-d2" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">    
                                                                                <div class="tab-pane fade active show" id="pills-default-d2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[4][title_2]" value="<?php echo $keys['4']['title_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-d2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[4][lang][<?php echo $language['id']; ?>][title_2]" value="<?php echo $keys['4']['languages'][$language['id']]['title_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-e2" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-e2" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">    
                                                                                <div class="tab-pane fade active show" id="pills-default-e2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[5][title_2]" value="<?php echo $keys['5']['title_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-e2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[5][lang][<?php echo $language['id']; ?>][title_2]" value="<?php echo $keys['5']['languages'][$language['id']]['title_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-f2" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-f2" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">    
                                                                                <div class="tab-pane fade active show" id="pills-default-f2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[6][title_2]" value="<?php echo $keys['6']['title_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-f2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[6][lang][<?php echo $language['id']; ?>][title_2]" value="<?php echo $keys['6']['languages'][$language['id']]['title_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-package">
                                                                        <td class="info col-package-lbl">Package Description 2</td>
                                                                        <td class="info col-package col-package-a">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2d2" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2d2" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2d2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[4][description_2]" value="<?php echo $keys['4']['description_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2d2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[4][lang][<?php echo $language['id']; ?>][description_2]" value="<?php echo $keys['4']['languages'][$language['id']]['description_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2e2" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2e2" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2e2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[5][description_2]" value="<?php echo $keys['5']['description_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2e2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[5][lang][<?php echo $language['id']; ?>][description_2]" value="<?php echo $keys['5']['languages'][$language['id']]['description_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2f2" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2f2" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2f2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[6][description_2]" value="<?php echo $keys['6']['description_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2f2" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[6][lang][<?php echo $language['id']; ?>][description_2]" value="<?php echo $keys['6']['languages'][$language['id']]['description_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="row-subs-package">
                                                                        <td class="info col-package-lbl">Package Features 2</td>
                                                                        <td class="info col-package col-package-a">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2d2f" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2d2f" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2d2f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[4][features_2]" value="<?php echo $keys['4']['features_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2d2f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[4][lang][<?php echo $language['id']; ?>][features_2]" value="<?php echo $keys['4']['languages'][$language['id']]['features_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-b">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2e2f" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2e2f" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2e2f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[5][features_2]" value="<?php echo $keys['5']['features_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2e2f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[5][lang][<?php echo $language['id']; ?>][features_2]" value="<?php echo $keys['5']['languages'][$language['id']]['features_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                        <td class="info col-package col-package-c">
                                                                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link active" data-toggle="pill" href="#pills-default-2f2f" role="tab" aria-selected="true">EN</a>
                                                                                </li>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <li class="nav-item">
                                                                                    <a class="nav-link" data-toggle="pill" href="#pills-<?php echo $language['id']; ?>-2f2f" role="tab" aria-selected="false"><?php echo $language['code']; ?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                            <div class="tab-content mt-2" id="pills-tabContent">
                                                                                <div class="tab-pane fade active show" id="pills-default-2f2f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[6][features_2]" value="<?php echo $keys['6']['features_2']; ?>">
                                                                                </div>
                                                                                <?php foreach($cms->getLanguages(['1']) as $language){ ?>
                                                                                <div class="tab-pane fade" id="pills-<?php echo $language['id']; ?>-2f2f" role="tabpanel">
                                                                                    <input type="text" class="form-control form-control-input" name="subscription[6][lang][<?php echo $language['id']; ?>][features_2]" value="<?php echo $keys['6']['languages'][$language['id']]['features_2']; ?>">
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <div class="footer-form-action">
                                                                <button type="submit" class="btn-icon-full btn-confirm" data-orientation="next">
                                                                    <span class="btn-label">Save</span>
                                                                    <span class="btn-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                            <polyline points="20 6 9 17 4 12"></polyline>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                                <?php /*
                                                                <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal" data-target="#post_task_od">
                                                                    <span class="btn-label">Cancel</span>
                                                                    <span class="btn-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                            <line x1="18" y1="6" x2="6" y2="18"></line>
                                                                            <line x1="6" y1="6" x2="18" y2="18"></line>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                                */ ?>
                                                            </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
