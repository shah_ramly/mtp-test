<?php if( isset($activities) /*&& !empty($activities)*/ ): ?>
<div class="col-12 col-xl-12 col-activity-log-wrapper">
    <div id="table-listing-container-activitylog" class="table-listing-container">
        <table class="table">
            <thead>
            <tr>
                <th class="info col-log-date">Date & Time</th>
                <th class="info col-log-activity">Activity Log</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($activities->result() as $activity): ?>
            <tr>
                <td class="info col-log-date">
	                <span class="logdate"><?php echo date($cms->settings['date_format'], strtotime($activity['date']['date'])); ?></span>&nbsp;<span class="logtime"><?php echo date($cms->settings['time_format'], strtotime($activity['date']['time'])); ?></span>
                </td>
                <td class="info col-log-activity"><?php echo $activity['message'] ?></td>
            </tr>
			<?php endforeach ?>
            </tbody>
        </table>
    </div>
	<div class="table-listing-footer-pagination">
		<div class="footer-total-page">Page <?php echo $activities->currentPage ?> of <?php echo $activities->totalPage ?></div>
		<nav class="nav-tbl-pagination">
			<ul class="pagination">
                <?php echo $activities->display() ?>
			</ul>
		</nav>
	</div>
</div>
<?php endif ?>