<?php if( isset($url, $current_page, $total_pages) ): ?>
<?php $format = isset($format) ? $format : '?page=' ?>
<div class="footer-total-page">Page <?php echo $current_page ?> of <?php echo $total_pages ?></div>
<nav class="nav-tbl-pagination">
    <ul class="pagination">
        <li class="page-item page-item-arrow page-item-prev <?php echo ($current_page <= 1) ? 'disabled' : '' ?>">
            <a class="page-link"
               href="<?php echo ($current_page > 1) ? $url . $format . ($current_page-1) . ($query_string ?? '') : '#' ?>"
               tabindex="-1">
                <svg xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2"
                     stroke-linecap="round" stroke-linejoin="round">
                    <path d="M15 18l-6-6 6-6"/>
                </svg>
            </a>
        </li>
	    <?php if( $total_pages > 4 ): ?>

	        <?php if ($current_page > 3): ?>
	            <li class="page-item">
	                <a class="page-link" href="<?php echo $url . ($query_string ?? '') ?>">1</a>
	            </li>
	            <li class="page-item">
	                <a class="page-link" href="#">...</a>
	            </li>
	        <?php endif; ?>

	        <?php if ($current_page-2 > 0): ?>
	            <li class="page-item">
	                <a class="page-link" href="<?php echo $url . $format . ($current_page-2) . ($query_string ?? '') ?>">
	                    <?php echo $current_page-2 ?>
	                </a>
	            </li>
	        <?php endif; ?>
	        <?php if ($current_page-1 > 0): ?>
	            <li class="page-item">
	                <a class="page-link" href="<?php echo $url . $format . ($current_page-1) . ($query_string ?? '') ?>">
	                    <?php echo $current_page-1 ?>
	                </a>
	            </li>
	        <?php endif; ?>
	        <li class="page-item active disabled">
	            <a class="page-link" href="#"><?php echo $current_page ?> <span
	                    class="sr-only">(current)</span></a>
	        </li>
	        <?php if ($current_page+1 < $total_pages+1): ?>
	            <li class="page-item">
	                <a class="page-link" href="<?php echo $url . $format . ($current_page+1) . ($query_string ?? '') ?>">
	                    <?php echo $current_page+1 ?>
	                </a>
	            </li>
	        <?php endif; ?>
	        <?php if ($current_page+2 < $total_pages+1): ?>
	            <li class="page-item">
	                <a class="page-link" href="<?php echo $url . $format . ($current_page+2) . ($query_string ?? '') ?>">
	                    <?php echo $current_page+2 ?>
	                </a>
	            </li>
	        <?php endif; ?>
	        <?php if ($current_page < $total_pages-2): ?>
	            <li class="page-item">
	                <a class="page-link" href="#">...</a>
	            </li>
	            <li class="page-item">
	                <a class="page-link" href="<?php echo $url . $format . $total_pages . ($query_string ?? '') ?>">
	                    <?php echo $total_pages ?>
	                </a>
	            </li>
	        <?php endif; ?>

		<?php else: ?>

		    <?php foreach (range(1, $total_pages) as $page): ?>
			    <?php if( (int) $page === (int) $current_page ): ?>
				    <li class="page-item active disabled">
					    <a class="page-link" href="#"><?php echo $current_page ?> <span
								    class="sr-only">(current)</span></a>
				    </li>
				<?php else: ?>
				    <li class="page-item">
					    <a class="page-link" href="<?php echo $url . $format . $page . ($query_string ?? '') ?>">
			                <?php echo $page ?>
					    </a>
				    </li>
				<?php endif ?>
			<?php endforeach ?>

		<?php endif ?>

        <li class="page-item page-item-arrow page-item-next <?php echo ($current_page >= $total_pages) ? 'disabled' : '' ?>">
            <a class="page-link"
               href="<?php echo ($current_page < $total_pages) ? $url . $format . ($current_page+1) . ($query_string ?? '') : '#' ?>">
                <svg xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2"
                     stroke-linecap="round" stroke-linejoin="round">
                    <path d="M9 18l6-6-6-6"/>
                </svg>
            </a>
        </li>
    </ul>
</nav>
<?php endif ?>