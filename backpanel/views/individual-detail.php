<div class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-chart">
				<div class="card-header">
					<div class="row">
						<div class="col-sm-12 col-header-filter">
							<h2 class="card-title">Individual Management</h2>
							<button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
							        data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
								<svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor"
								     xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd"
									      d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
								</svg>
							</button>
							<ul class="col-account-header">
								<li class="dropdown col-right-flex col-account">
									<button type="button" class="btn btn-success btn-block btn-logout"
									        onclick="location.href='<?php echo url_for('/logout') ?>';">
										<span class="plus-icon-lbl">Log Out</span>
									</button>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card-body-backend card-body-backend-individual-detail">
					<div class="row">
						<div class="col-xl-12">
							<div class="row">
								<div class="col-xl-12 col-backend-top">
									<div class="table-listing-header-container">
										<div class="table-listing-header-title">Individual Detail</div>
										<div class="table-listing-header-actions"></div>
									</div>
									<div class="col-backend-top-wrapper">
										<div class="row">
											<div class="col-xl-12 col-myprofile">
												<form id="selfForm" class="form-individualprofile">
													<div class="form-collapse-row row-collapse-personal">
														<div class="collapse-title-lbl collapsed" data-toggle="collapse"
														     data-target="#collapsePersonalInfo" aria-expanded="true"
														     aria-controls="collapsePersonalInfo">
															<span class="title-collapse">Personal Information</span><i
																	class="fa fa-caret-down"></i>
														</div>
														<input type="hidden" name="user[id]" value="<?php echo $user['id'] ?>" />
														<div class="collapse" id="collapsePersonalInfo">
															<div class="collapse-wrapper">
																<div class="form-collapse-row row-collapse-personalinfo">
																	<div class="form-flex">
																		<div class="input-group-flex row-indfirstname">
																			<input type="text" id="firstName"
																			       name="user[firstname]"
																			       class="form-control form-control-input"
																			       value="<?php echo $user['firstname'] ?>"
																			       placeholder="First Name *"
																			         autofocus="">
																		</div>
																		<div class="input-group-flex row-indlastname">
																			<input type="text" id="lastName"
																			       name="user[lastname]"
																			       class="form-control form-control-input"
																			       value="<?php echo $user['lastname'] ?>"
																			       placeholder="Last Name *"
																			        >
																		</div>
																		<div class="input-group-flex row-indid">
																			<input type="text" id="identification"
																			       name="user[nric]"
																			       class="form-control form-control-input"
																			       value="<?php echo $user['nric'] ?>"
																			       placeholder="MyKad/Passport/Other ID *"
																			        >
																		</div>
																		<div class="input-group-flex row-indgender">
																			<select class="form-control form-control-input"
																			        name="user[gender]"
																			        placeholder="Gender">
																				<option disabled selected>Gender</option>
																				<option value="male" <?php echo ($user['gender']=="male") ?  "selected" : '' ?>>Male</option>
																				<option value="female" <?php echo ($user['gender']=="female") ?  "selected" : '' ?>>Female</option>
																			</select>
																		</div>
																		<div class="input-group-flex row-indbirthdate">
																			<div class="form-group">
																				<div class="input-group input-group-datetimepicker date"
																				     id="form_datebirth"
																				     data-target-input="nearest">
																					<input type="text"
																					       name="user[dob]"
																					       class="form-control datetimepicker-input"
																					       data-target="#form_datebirth"
																					       value="<?php echo $user['dob'] ? date('d/m/Y', strtotime($user['dob'])) : '' ?>"
																					       placeholder="Date of Birth *"/>
																					<span class="input-group-addon"
																					      data-target="#form_datebirth"
																					      data-toggle="datetimepicker">
                                                                                                    <span class="fa fa-calendar"></span>
                                                                                                </span>
																				</div>
																			</div>
																		</div>
																		<div class="input-group-flex row-indaddress">
																			<input type="text" id="indAddress"
																			       name="user[address1]"
																			       class="form-control form-control-input"
																			       value="<?php echo $user['address1'] ?>"
																			       placeholder="Address"  >
																		</div>
																		<div class="input-group-flex row-indpostcode">
																			<input type="text" id="indPostcode"
																			       name="user[zip]"
																			       class="form-control form-control-input"
																			       placeholder="Post Code" value="<?php echo $user['zip'] ?>"
																			        >
																		</div>
																		<div class="input-group-flex row-indcountry">
																			<select class="form-control form-control-input"
																			        name="user[country]"
																			        placeholder="Country">
																				<option disabled>Country *</option>
                                                                                <?php foreach ($cms->countries() as $key ): ?>
																				<option value="<?php echo $key['id'] ?>"<?php echo $user['country'] == $key['id'] ? " selected" : '' ?>><?php echo $key['name'] ?></option>
                                                                                <?php endforeach ?>
																			</select>
																		</div>
																		<div class="input-group-flex row-indstate">
																			<select class="form-control form-control-input"
																			        name="user[state]"
																			        placeholder="State"
																			        data-selected="<?php echo $user['state']; ?>"
																			>
																				<option disabled>State *</option>
                                                                                <?php foreach($cms->states($user['country']) as $state): ?>
																				<option value="<?php echo $state['id'] ?>" <?php echo $user['state'] == $state['id'] ? "selected" : '' ?>><?php echo $state['name'] ?></option>
                                                                                <?php endforeach; ?>
																			</select>
																		</div>
																		<div class="input-group-flex row-indcity">
																			<select class="form-control form-control-input"
																			        name="user[city]"
																			        placeholder="Town/City"
																			        data-selected="<?php echo $user['city'] ?>"
																			>
																				<option disabled>Town/City *</option>
                                                                                <?php foreach($cms->cities($user['state']) as $city): ?>
																				<option value="<?php echo $city['id'] ?>" <?php echo $user['city'] == $city['id'] ? "selected" : '' ?>><?php echo $city['name']; ?></option>
                                                                                <?php endforeach ?>
																			</select>
																		</div>
																		<div class="input-group-flex row-indnationality">
																			<select class="form-control form-control-input"
																			        name="user[nationality]"
																			        placeholder="Nationality">
																				<option disabled>Nationality</option>
																				<option value="malaysian"<?php echo $user['nationality'] == 'malaysian' ? ' selected' : '' ?>>Malaysian</option>
																				<option value="other"<?php echo $user['nationality'] == 'other' ? ' selected' : '' ?>>Others</option>
																			</select>
																		</div>
																		<div class="form-flex">
																			<div class="input-group-flex row-indmobile">
                                                                                            <span class="tel-prefix">
                                                                                                <input type="tel"
                                                                                                       name="user[mobile_number]"
                                                                                                       id="contactMobile"
                                                                                                       class="form-control form-control-input intl-tel-input"
                                                                                                       placeholder="Contact No. Mobile	"
                                                                                                       value="<?php echo $user['mobile_number'] ?>">
                                                                                            </span>
																			</div>
																			<div class="input-group-flex row-indphone">
                                                                                            <span class="tel-prefix">
                                                                                                <input type="tel"
                                                                                                       name="user[contact_number]"
                                                                                                       id="contactPhone"
                                                                                                       class="form-control form-control-input intl-tel-input"
                                                                                                       placeholder="Contact No. Fixed Line"
                                                                                                       value="<?php echo $user['contact_number'] ?>">
                                                                                            </span>
																			</div>
																		</div>
																		<div class="form-flex">
																			<div class="input-group-flex row-indemail">
																				<input type="email" id="inputEmail"
																				       name="user[email]"
																				       class="form-control form-control-input"
																				       placeholder="Email *"  
																				       autofocus=""
																				       value="<?php echo $user['email'] ?>">
																			</div>
																		</div>
																		<div class="form-flex">
																			<div class="input-group-block row-indsocmed">
																				<label class="input-lbl input-lbl-block">
																					<span class="input-label-txt">Preferred Contact Method</span>
																				</label>
																				<?php $contact_method = explode(',', $user['contact_method']) ?>
																				<div class="form-block-flex">
																					<div class="form-block">
																						<div class="custom-control custom-checkbox">
																							<input type="checkbox"
																							       name="user[contact_method][]"
																							       class="custom-control-input"
																							       id="preferred_contact_1"
																							        value="whatsapp"
																								<?php echo in_array('whatsapp', $contact_method) ? 'checked' : '' ?>
																							>
																							<label class="custom-control-label"
																							       for="preferred_contact_1">WhatsApp (Number)</label>
																						</div>
																						<div class="custom-control custom-checkbox">
																							<input type="checkbox"
																							       name="user[contact_method][]"
																							       class="custom-control-input"
																							       id="preferred_contact_2"
																							        value="telegram"
																								<?php echo in_array('telegram', $contact_method) ? 'checked' : '' ?>
																							>
																							<label class="custom-control-label"
																							       for="preferred_contact_2">Telegram (ID)</label>
																						</div>
																						<div class="custom-control custom-checkbox">
																							<input type="checkbox"
																							       name="user[contact_method][]"
																							       class="custom-control-input"
																							       id="preferred_contact_3"
																							        value="wechat"
																								<?php echo in_array('wechat', $contact_method) ? 'checked' : '' ?>
																							>
																							<label class="custom-control-label"
																							       for="preferred_contact_3">WeChat (ID)</label>
																						</div>
																						<div class="custom-control custom-checkbox">
																							<input type="checkbox"
																							       name="user[contact_method][]"
																							       class="custom-control-input"
																							       id="preferred_contact_4"
																							        value="email"
																								<?php echo in_array('email', $contact_method) ? 'checked' : '' ?>
																							>
																							<label class="custom-control-label"
																							       for="preferred_contact_4">Email</label>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																
																<div class="form-collapse-row row-collapse-profilephoto">
																	<div class="collapse-title-lbl"><span
																				class="title-collapse">Profile Photo</span>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-block row-indavatar profile_photo">
                                                                            <?php if($user['photo']): ?>
	                                                                            <img src="<?php echo imgCrop($user['photo'], 100, 100); ?>" class="profile-img"><br>
																			<?php endif ?>
																			<button type="button" class="btn btn-default btn-sm mt-1" onClick="$(this).next().click();"><span class="fa fa-upload"></span> Upload</button>
																			<input type="file" name="file" id="profileImage" class="d-none">
																			<input type="hidden" name="user[photo]" value="<?php echo  $user['photo']; ?>">
																		</div>
																	</div>
																</div>
																
																<div class="form-collapse-row row-collapse-personalstatement">
																	<div class="collapse-title-lbl"><span
																				class="title-collapse">Personal Statement</span>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-flex row-indabout">
																			<textarea
																					name="user[about]"
																					class="form-control form-control-input"
																					placeholder="About Me"
																					rows="3"><?php echo  $user['about'] ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="form-collapse-row row-collapse-emergencycontact">
																	<div class="collapse-title-lbl"><span
																				class="title-collapse">Emergency Contact</span>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-flex row-indemergencyname">
																			<input type="text" id="emergencyName"
																			       name="user[emergency_contact_name]"
																			       class="form-control form-control-input"
																			       placeholder="Emergency Contact Person"
																			       <?php echo $user['emergency_contact_name'] ?>
																			       >
																		</div>
																		<div class="input-group-flex row-indemergencycontact">
                                                                            <span class="tel-prefix">
                                                                                <input type="tel"
                                                                                       name="user[emergency_contact_number]"
                                                                                       id="emergencyContact"
                                                                                       class="form-control form-control-input intl-tel-input"
                                                                                       placeholder="Emergency Contact Number"
                                                                                       <?php echo $user['emergency_contact_number'] ?>
                                                                                       >
                                                                            </span>
																		</div>
																		<div class="input-group-flex row-indemergencyrel">
																			<input type="text"
																			       name="user[emergency_contact_relation]"
																			       id="emergencyRelationship"
																			       class="form-control form-control-input"
																			       placeholder="Relationship"
																			       <?php echo $user['emergency_contact_relation'] ?>
																			       >
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-collapse-row row-collapse-cv">
														<div class="collapse-title-lbl collapsed" data-toggle="collapse"
														     data-target="#collapseMyCV" aria-expanded="true"
														     aria-controls="collapseMyCV"><span
																	class="title-collapse">My CV</span><i
																	class="fa fa-caret-down"></i></div>
														<div class="collapse" id="collapseMyCV">
															<div class="collapse-wrapper">
																<div class="form-collapse-row row-collapse-resume">
																	<div class="form-flex">
																		<div class="input-group-block row-indresume">
																			<label class="input-lbl input-lbl-block">
																				<span class="input-label-txt">CV/Resume</span>
																			</label>
																			<div class="uploads">
																			<ul class="my-docs resume">
                                                                                <?php if(!empty($docs['resume'])):
                                                                                foreach($docs['resume'] as $resume):
                                                                                    $src = explode('/', $resume);
                                                                                    $filename = end($src);
                                                                                ?>
																					<li>
																						<a href="<?php echo url_for($resume); ?>" target="_blank"><?php echo $filename; ?></a>
																						<input type="hidden" name="docs[resume][]" value="<?php echo $resume; ?>">
																						<a href="#" class="remove-docs" data-toggle="tooltip" title="Remove"><i class="far fa-trash-alt"></i></a>
																					</li>
                                                                                <?php endforeach;
                                                                                endif ?>
																			</ul>
																			<?php /*
																			<button type="button" class="btn btn-default btn-sm mt-1" onClick="$(this).next().click();"><span class="fa fa-upload"></span> Upload</button>
																			<input type="file" name="file" id="profile-resume" class="d-none">
																			*/ ?>
																			</div>
																		</div>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-block row-indresume">
																			<label class="input-lbl input-lbl-block">
																				<span class="input-label-txt">Cover Letter</span>
																			</label>
																			<div class="uploads">
																				<ul class="my-docs cover_letter">
                                                                                    <?php if(!empty($docs['cover_letter'])):
                                                                                    foreach($docs['cover_letter'] as $cover_letter):
                                                                                        $src = explode('/', $cover_letter);
                                                                                        $filename = end($src);
                                                                                    ?>
																						<li>
																							<a href="<?php echo url_for($cover_letter); ?>" target="_blank"><?php echo $filename; ?></a>
																							<input type="hidden" name="docs[cover_letter][]" value="<?php echo $cover_letter; ?>">
																							<a href="#" class="remove-docs" data-toggle="tooltip" title="Remove"><i class="far fa-trash-alt"></i></a>
																						</li>
                                                                                    <?php endforeach;
                                                                                    endif; ?>
																				</ul>
																				<?php /*
																				<button type="button" class="btn btn-default btn-sm mt-1" onClick="$(this).next().click();"><span class="fa fa-upload"></span> Upload</button>
																				<input type="file" name="file" id="profile-cover_letter" class="d-none">
																				*/ ?>
																			</div>
																		</div>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-block row-indresume">
																			<label class="input-lbl input-lbl-block">
																				<span class="input-label-txt">Awards/Certifications/Qualifications</span>
																			</label>
																			<div class="uploads">
																				<ul class="my-docs cert">
                                                                                    <?php if(!empty($docs['cert'])):
                                                                                    foreach($docs['cert'] as $cert):
                                                                                        $src = explode('/', $cert);
                                                                                        $filename = end($src);
                                                                                    ?>
																						<li>
																							<a href="<?php echo url_for($cert); ?>" target="_blank"><?php echo $filename; ?></a>
																							<input type="hidden" name="docs[cert][]" value="<?php echo $cert; ?>">
																							<a href="#" class="remove-docs" data-toggle="tooltip" title="Remove"><i class="far fa-trash-alt"></i></a>
																						</li>
                                                                                    <?php endforeach;
                                                                                    endif; ?>
																				</ul>
																				<?php /*
																				<button type="button" class="btn btn-default btn-sm mt-1" onClick="$(this).next().click();"><span class="fa fa-upload"></span> Upload</button>
																				<input type="file" name="file" id="profile-cert" class="d-none">
																				*/ ?>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="form-collapse-row row-collapse-employment">
																	<div class="collapse-title-lbl"><span
																				class="title-collapse">Employment History</span>
																	</div>

																	<div class="row-form-data-container">
                                                                        <?php if( !empty($employments) ):
                                                                        foreach($employments as $employment): ?>
																			<div class="row-form-data" id="emp-<?php echo $employment['id']; ?>">
																				<div class="talent-details-workxp-row">
																					<div class="talent-details-workxp-compname"><?php echo $employment['company_name']; ?></div>
																					<div class="talent-details-workxp-pos"><?php echo $employment['position']; ?>, <?php echo $cms->getIndustryName($employment['industry']); ?></div>
																					<div class="talent-details-workxp-pos-duration">
																						<div class="talent-details-workxp-location"><?php echo $employment['state'] ? $cms->getStateName($employment['state']) . ', '  : ''; ?><?php echo $cms->getCountryName($employment['country']); ?></div>
																						<div class="talent-details-workxp-duration"><?php echo dateRangeToDays($employment['date_start'], $employment['date_end']); ?></div>
																					</div>

																					<div class="talent-details-workxp-resp">
																						<span class="talent-details-workxp-list-lbl"><b>Responsibilities</b></span>
																						<div>
                                                                                            <?php echo nl2br($employment['job_responsibilities']); ?>
																						</div>
																					</div>
																					<div class="talent-details-workxp-resp">
																						<span class="talent-details-workxp-list-lbl"><b>Description</b></span>
																						<div>
                                                                                            <?php echo nl2br($employment['job_desc']); ?>
																						</div>
																					</div>
                                                                                    <?php
                                                                                    $employment['date_start'] = date('d/m/Y', strtotime($employment['date_start']));
                                                                                    $employment['date_end'] = date('d/m/Y', strtotime($employment['date_end']));
                                                                                    ?>
																					<div class="edit-in-modal">
																						<a href="#" class="edit-employment" data-data="<?php echo htmlspecialchars(json_encode($employment)); ?>">Edit</a>
																						<a href="#" class="remove-employment" data-id="<?php echo $employment['id']; ?>"><i class="far fa-trash-alt"></i></a>
																					</div>
																				</div>
																			</div>
                                                                        <?php endforeach;
                                                                        endif; ?>
																	</div>
																</div>
																<div class="form-collapse-row row-collapse-education">
																	<div class="collapse-title-lbl"><span
																				class="title-collapse">Education</span>
																	</div>

																	<div class="row-form-data-container">
                                                                        <?php if( !empty($educations) ):
                                                                        foreach($educations as $education): ?>
																			<div class="row-form-data" id="edu-<?php echo $education['id']; ?>">
																				<div class="talent-details-edu-row">
																					<div class="talent-details-edu-insname"><?php echo $education['edu_institution']; ?></div>
																					<div class="talent-details-edu-level-fieldstudy-grade">
																						<div class="talent-details-edu-level"><?php echo $education['highest_edu_level']; ?></div>
																						<div class="talent-details-edu-fieldstudy"><?php echo $education['field']; ?></div>
																						<div class="talent-details-edu-fieldstudy"><?php echo $education['major']; ?></div>
																						<div class="talent-details-edu-grade"><?php echo $education['grade']; ?></div>
																					</div>
																					<div class="talent-details-edu-location-year">
																						<div class="talent-details-edu-location"><?php echo $education['state'] ? $cms->getStateName($education['state']) . ', '  : ''; ?><?php echo $cms->getCountryName($education['country']); ?></div>
																						<div class="talent-details-edu-year"><?php echo $education['grad_year']; ?></div>
																					</div>
																					<div class="edit-in-modal">
																						<a href="#" class="edit-education" data-data="<?php echo htmlspecialchars(json_encode($education)); ?>">Edit</a>
																						<a href="#" class="remove-education" data-id="<?php echo $education['id']; ?>"><i class="far fa-trash-alt"></i></a>
																					</div>
																				</div>
																			</div>
                                                                        <?php endforeach;
                                                                        endif; ?>
																	</div>
																</div>
																<div class="form-collapse-row row-collapse-skills">
																	<div class="collapse-title-lbl"><span
																				class="title-collapse">Skills</span>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-flex row-indskills">
																			<div class="bs-example">
																				<input type="text" class="bootstrap-tagsinput skills" name="user[skills]"
																				       value="<?php echo $user['skills'] ?>"
																				        />
																			</div>
																		</div>
																	</div>
																</div>
																<div class="form-collapse-row row-collapse-languages">
																	<div class="collapse-title-lbl"><span
																				class="title-collapse">Languages</span>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-flex row-indlang">
																			<div class="bs-example">
																				<input type="text" name="user[language]"
																				       class="bootstrap-tagsinput language-input"
																				       value="<?php echo $user['language'] ?>"
																				       />
																			</div>
																		</div>
																	</div>
																</div>

															</div>
														</div>
													</div>
													<div class="form-collapse-row row-collapse-preference">
														<div class="collapse-title-lbl collapsed" data-toggle="collapse"
														     data-target="#collapseMyPreferences" aria-expanded="true"
														     aria-controls="collapseMyPreferences"><span
																	class="title-collapse">My Preferences</span><i
																	class="fa fa-caret-down"></i></div>
														<div class="collapse" id="collapseMyPreferences">
															<div class="collapse-wrapper">
																<div class="form-collapse-row row-collapse-bank-info">
																	<div class="collapse-title-lbl"><span
																				class="title-collapse">Bank information</span>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-block row-bankacc">
																			<select class="form-control form-control-input" name="preference[bank]">
																				<option disabled selected>Select bank</option>
																				<option value="Maybank" <?php echo $preference['bank'] == "Maybank" ? ' selected' : ''; ?>>Maybank</option>
																				<option value="RHB" <?php echo $preference['bank'] == "RHB" ? " selected" : ''; ?>>RHB</option>
																				<option value="CIMB" <?php echo $preference['bank'] == "CIMB" ? " selected" : ''; ?>>CIMB</option>
																				<option value="Bank Islam" <?php echo $preference['bank'] == "Bank Islam" ? " selected" : ''; ?>>Bank Islam</option>
																				<option value="Hong Leong" <?php echo $preference['bank'] == "Hong Leong" ? " selected" : ''; ?>>Hong Leong</option>
																				<option value="Public Bank" <?php echo $preference['bank'] == "Public Bank" ? " selected" : ''; ?>>Public Bank</option>
																				<option value="Bank Rakyat" <?php echo $preference['bank'] == "Bank Rakyat" ? " selected" : ''; ?>>Bank Rakyat</option>
																				<option value="OCBC Bank" <?php echo $preference['bank'] == "OCBC Bank" ? " selected" : ''; ?>>OCBC Bank</option>
																				<option value="Alliance Bank" <?php echo $preference['bank'] == "Alliance Bank" ? " selected" : ''; ?>>Alliance Bank</option>
																				<option value="Alliance Islamic Bank" <?php echo $preference['bank'] == "Alliance Islamic Bank" ? " selected" : ''; ?>>Alliance Islamic Bank</option>
																				<option value="AmBank" <?php echo $preference['bank'] == "AmBank" ? " selected" : ''; ?>>AmBank</option>
																				<option value="Affin Islamic Bank Berhad" <?php echo $preference['bank'] == "Affin Islamic Bank Berhad" ? " selected" : ''; ?>>Affin Islamic Bank Berhad</option>
																				<option value="Affin Bank Berhad" <?php echo $preference['bank'] == "Affin Bank Berhad" ? " selected" : ''; ?>>Affin Bank Berhad</option>
																				<option value="United Overseas Bank Berhad" <?php echo $preference['bank'] == "United Overseas Bank Berhad" ? " selected" : ''; ?>>United Overseas Bank Berhad</option>
																				<option value="Bank Simpanan Nasional Berhad" <?php echo $preference['bank'] == "Bank Simpanan Nasional Berhad" ? " selected" : ''; ?>>Bank Simpanan Nasional Berhad</option>
																				<option value="Bank Kerjasama Rakyat Malaysia" <?php echo $preference['bank'] == "Bank Kerjasama Rakyat Malaysia" ? " selected" : ''; ?>>Bank Kerjasama Rakyat Malaysia</option>
																				<option value="Bank Muamalat Berhad" <?php echo $preference['bank'] == "Bank Muamalat Berhad" ? " selected" : ''; ?>>Bank Muamalat Berhad</option>
																				<option value="Kuwait Finance House" <?php echo $preference['bank'] == "Kuwait Finance House" ? " selected" : ''; ?>>Kuwait Finance House</option>
																				<option value="Al-Rajhi Banking &amp; Investment Corp" <?php echo $preference['bank'] == "Al-Rajhi Banking & Investment Corp" ? " selected" : ''; ?>>Al-Rajhi Banking &amp; Investment Corp</option>
																				<option value="HSBC Bank Malaysia Berhad" <?php echo $preference['bank'] == "HSBC Bank Malaysia Berhad" ? " selected" : ''; ?>>HSBC Bank Malaysia Berhad</option>
																				<option value="Standard Chartered" <?php echo $preference['bank'] == "Standard Chartered" ? " selected" : ''; ?>>Standard Chartered</option>
																			</select>
																		</div>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-flex row-accholdername">
																			<input type="text"
																			       name="preference[acc_name]"
																			       class="form-control form-control-input"
																			       placeholder="Account Holder's Name"
																			value="<?php echo  $preference['acc_name'] ?>">
																		</div>
																		<div class="input-group-flex row-accno">
																			<input type="text"
																			       name="preference[acc_num]"
																			       class="form-control form-control-input"
																			       placeholder="Bank Account Number"
																			value="<?php echo  $preference['acc_num'] ?>">
																		</div>
																	</div>
																</div>
																<div class="form-collapse-row row-collapse-preference-sub">
																	<div class="collapse-title-lbl"><span
																				class="title-collapse">Task Preferences</span>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-block row-indlocremote">
																			<label class="input-lbl input-lbl-block">
																				<span class="input-label-txt">For tasks that can be performed remotely, match me with tasks from:</span>
																			</label>
																			<div class="form-flex">
																				<div class="input-group-flex row-indlocremoteselect">
																					<select class="form-control form-control-input"
																					        name="preference[rm_work_location]"
																					        placeholder="Work Loc"
																					        id="select_loc_remote">
																						<option value="same_country" <?php echo $preference['rm_work_location'] == 'same_country' ? 'selected' : '' ?>>Same country</option>
																						<option value="anywhere" <?php echo $preference['rm_work_location'] == 'anywhere' ? 'selected' : '' ?>>Anywhere</option>
																					</select>
																				</div>
																				<div class="input-group-flex row-indlocremotestate rm-work rm-state d-none">
																					<select class="form-control form-control-input"
																					        placeholder="State" name="preference[rm_state]"
																					        data-selected="<?php echo $preference['rm_state'] ?>">
																						<option value="0" selected>All States</option>
                                                                                        <?php foreach($cms->states($user['country']) as $state): ?>
																							<option value="<?php echo $state['id'] ?>" <?php echo $state['id'] == $preference['rm_state'] ? 'selected' : '' ?>><?php echo $state['name'] ?></option>
                                                                                        <?php endforeach ?>
																					</select>
																				</div>
																				<div class="input-group-flex row-indlocremotenearest p-work p-radius d-none">
																					<select class="form-control form-control-input"
																					        name="preference[p_radius]"
																					        placeholder="Within">
																						<option disabled
																						        selected>Select Within
																						</option>
																						<option value="Within 5km" <?php echo $preference['p_radius'] == "Within 5km" ? 'selected' : '' ?>>Within 5km</option>
																						<option value="Within 10km" <?php echo $preference['p_radius'] == "Within 10km" ? 'selected' : '' ?>>Within 10km</option>
																						<option value="Within 20km" <?php echo $preference['p_radius'] == "Within 20km" ? 'selected' : '' ?>>Within 20km</option>
																						<option value="Within 30km" <?php echo $preference['p_radius'] == "Within 30km" ? 'selected' : '' ?>>Within 30km</option>
																						<option value="Within 50km" <?php echo $preference['p_radius'] == "Within 50km" ? 'selected' : '' ?>>Within 50km</option>
																					</select>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-block row-indlocphysical">
																			<label class="input-lbl input-lbl-block">
																				<span class="input-label-txt">For tasks that require my physical presence, match me with tasks from:</span>
																			</label>
																			<div class="form-flex">
																				<div class="input-group-flex row-indlocphysicalselect">
																					<select class="form-control form-control-input"
																					        name="preference[p_work_location]"
																					        placeholder="Work Loc"
																					        id="select_loc_physical">
																						<option value="same_country" <?php echo ($preference['p_work_location'] == 'same_country') ? 'selected' : '' ?>>Same Country</option>
																						<option value="anywhere" <?php echo ($preference['p_work_location'] == 'anywhere') ? 'selected' : '' ?>>Anywhere</option>
																					</select>
																				</div>
																				<div class="input-group-flex row-indlocphysicalstate p-work p-state d-none">
																					<select class="form-control form-control-input"
																					        name="preference[p_state]"
																					        placeholder="State">
																						<option value="0" selected>All States</option>
                                                                                        <?php foreach($cms->states($user['country']) as $state): ?>
																							<option value="<?php echo $state['id'] ?>" <?php echo $state['id'] == $preference['p_state'] ? 'selected' : '' ?>><?php echo $state['name'] ?></option>
                                                                                        <?php endforeach ?>
																					</select>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="form-flex" id="myProfileWorkdays">
																		<div class="input-group-block row-collapse-work-days">
																			<label class="input-lbl input-lbl-block">
																				<span class="input-label-txt">Workday Preferences</span>
																			</label>
																			<div class="form-block">
																				<div class="form-radio-row row-indallweek">
																					<div class="custom-control custom-radio">
																						<input type="radio"
																						       class="custom-control-input"
																						       id="workdaysAllWeek"
																						       name="preference[work_day]"
																						       value="all_week"
																							<?php echo $preference['work_day'] == 'all_week' ? 'checked' : '' ?>>
																						<label class="custom-control-label"
																						       for="workdaysAllWeek">All Week ( Monday to Sunday)</label>
																					</div>
																				</div>
																				<div class="form-radio-row row-indweekday">
																					<div class="custom-control custom-radio">
																						<input type="radio"
																						       class="custom-control-input"
																						       id="workdayCustom"
																						       name="preference[work_day]"
																						       value="custom"
																							<?php echo $preference['work_day'] == 'custom' ? 'checked' : '' ?>
																						>
																						<label class="custom-control-label"
																						       for="workdayCustom">Custom</label>
																					</div>
																					<div class="custom-control custom-checkbox"
																					     id="checkWeekdays">
																						<input type="checkbox"
																						       name="preference[work_day_weekend]"
																						       class="custom-control-input parent-weekdays"
																						       id="workdaysWeekdays"
																						       value="1"
																							<?php echo $preference['work_day_weekday'] ? 'checked' : '' ?>
																						>
																						<label class="custom-control-label"
																						       for="workdaysWeekdays">Weekdays ( Monday - Friday)</label>
																					</div>
																					<div class="custom-control custom-checkbox"
																					     id="checkFullday">
																						<input type="checkbox"
																						       name="preference[work_day_weekday_full]"
																						       class="custom-control-input child-weekdays"
																						       id="fullDay"
																						       value="1"
																							<?php echo $preference['work_day_weekday_full'] ? 'checked' : '' ?>
																						>
																						<label class="custom-control-label"
																						       for="fullDay">Full Day (8am – 6pm)</label>
																					</div>
																					<div class="custom-control custom-checkbox"
																					     id="checkAfterHour">
																						<input type="checkbox"
																						       name="preference[work_day_weekday_after]"
																						       class="custom-control-input child-weekdays"
																						       id="afterWorkHour"
																						       value="1"
																							<?php echo $preference['work_day_weekday_after'] ? 'checked' : '' ?>
																						>
																						<label class="custom-control-label"
																						       for="afterWorkHour">After Working Hours (6pm onwards)</label>
																					</div>
																					<div class="custom-control custom-checkbox"
																					     id="checkWeekends">
																						<input type="checkbox"
																						       name="preference[work_day_weekend]"
																						       class="custom-control-input parent-weekends"
																						       id="workdaysWeekend"
																						       value="1"
																							<?php echo $preference['work_day_weekend'] ? 'checked' : '' ?>
																						>
																						<label class="custom-control-label"
																						       for="workdaysWeekend">Weekends (Saturday & Sunday)</label>
																					</div>
																					<div class="custom-control custom-checkbox"
																					     id="checkAm">
																						<input type="checkbox"
																						       name="preference[work_day_weekend_am]"
																						       class="custom-control-input child-weekends"
																						       id="amwork"
																						       value="1"
																							<?php echo $preference['work_day_weekend_am'] ? 'checked' : '' ?>
																						>
																						<label class="custom-control-label"
																						       for="amwork">AM</label>
																					</div>
																					<div class="custom-control custom-checkbox"
																					     id="checkPm">
																						<input type="checkbox"
																						       name="preference[work_day_weekend_pm]"
																						       class="custom-control-input child-weekends"
																						       id="pmwork"
																						       value="1"
																							<?php echo $preference['work_day_weekend_pm'] ? 'checked' : '' ?>
																						>
																						<label class="custom-control-label"
																						       for="pmwork">PM</label>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="form-flex" id="myProfileWorkload">
																		<div class="input-group-block row-indworkload">
																			<label class="input-lbl row-collapse-work-load">
																				<span class="input-label-txt">Workload Preferences</span>
																			</label>
																			<div class="form-flex">
																				<label class="input-lbl input-lbl-block">
																					<span class="input-label-txt">I would like to monetise:</span>
																				</label>
																				<input type="number" name="preference[work_load_hours]"
																				       class="form-control form-control-input"
																				       min="0" id="" value="<?php echo $preference['work_load_hours'] ?>"/>
																				<span class="postlbl-hours">hour/s of my available time per week</span>
																			</div>
																			<p class="help-notes">Note: As you approach your preferred weekly limit, MTP will scale down the number of matches sent to you.</p>
																			<p class="help-notes">Minimum hours per week: 1 Hour / Maximum hours per week: 70 Hours</p>
																		</div>
																	</div>

																	<div class="form-flex mt-5" id="myPreferredLang">
																		<label class="input-lbl">
																			<span class="input-label-txt">Preferred Language for Site</span>
																		</label>
																		<div class="form-flex">
																			<?php foreach($cms->getLanguages() as $language){ ?>
																			<div class="custom-control custom-radio mr-3">
																				<input type="radio" class="custom-control-input" id="lang<?php echo $language['title']; ?>" name="preference[language]" value="<?php echo $language['id']; ?>"<?php echo $preference['language'] == $language['id'] ? ' checked' : ''; ?>>
																				<label class="custom-control-label" for="lang<?php echo $language['title']; ?>"><?php echo $language['title']; ?></label>
																			</div>
																			<?php } ?>
																		</div>
																	</div>

																	<div class="form-flex mt-5" id="mySurvey">
																		<label class="input-lbl">
																			<span class="input-label-txt">Survey</span>
																		</label>
																		<div class="form-flex">
																			<div class="custom-control custom-checkbox" id="checkSurvey">
																				<input type="checkbox" class="custom-control-input child-weekends" id="survey" name="preference[survey]" value="1"<?php echo $preference['survey'] ? ' checked' : ''; ?>>
																				<label class="custom-control-label" for="survey">I would like to be part of surveys</label>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-collapse-row row-collapse-activity">
														<div class="collapse-title-lbl collapsed" data-toggle="collapse"
														     data-target="#collapseActivity" aria-expanded="true"
														     aria-controls="collapseActivity"><span
																	class="title-collapse">Activity Log</span><i
																	class="fa fa-caret-down"></i></div>
														<div class="collapse" id="collapseActivity">
															<div class="collapse-wrapper">
																<?php echo partial('partial/activity-log.php') ?>
															</div>
														</div>
													</div>
													<div class="form-collapse-row row-collapse-biling-individual">
														<div class="collapse-title-lbl collapsed" data-toggle="collapse" data-target="#collapseBiling" aria-expanded="true" aria-controls="collapseActivity"><span
class="title-collapse">Billing Details</span><i class="fa fa-caret-down"></i></div>
														<div class="collapse" id="collapseBiling">
															<div class="collapse-wrapper">
																<iframe src="<?php echo url_for('/analytics/' . $user['id']); ?>" frameborder="0" scrolling="no" onload="resizeIframe(this)" style="width:100%;height:500px;border:none;"></iframe>
															</div>
														</div>
													</div>
													<div class="form-collapse-row row-collapse-account-settings">
														<div class="collapse-title-lbl collapsed" data-toggle="collapse"
														     data-target="#collapseAccountSetting" aria-expanded="true"
														     aria-controls="collapseAccountSetting"><span
																	class="title-collapse">Account Settings</span><i
																	class="fa fa-caret-down"></i></div>
														<div class="collapse" id="collapseAccountSetting">
															<div class="collapse-wrapper">
																<div class="form-collapse-row row-collapse-resetpassword">
																	<div class="collapse-title-lbl mb-3"><span class="title-collapse">Subscription Plan</span></div>
																	<div class="form-flex">
																		<div class="input-group-flex row-plan">
																			<select class="form-control form-control-input" name="user[plan]" placeholder="Plan">
																				<?php foreach($subscriptions as $subscription){ ?>
																				<option value="<?php echo $subscription['id']; ?>"<?php echo $user['plan'] == $subscription['id'] ? ' selected' : '' ?>><?php echo $subscription['title']; ?></option>
																				<?php } ?>
																			</select>
																		</div>
																	</div>
																</div>
																		
																<div class="form-collapse-row row-collapse-resetpassword">
																	<div class="collapse-title-lbl mb-3"><span
																				class="title-collapse">Reset Password</span>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-flex row-indnewpassword">
																			<input type="password" id="newPassword"
																			       class="form-control form-control-input"
																			       placeholder="New Password"
																			       name="user[password]"
																			        >
																		</div>
																		<div class="input-group-flex row-indconfirmpassword">
																			<input type="password" id="confirmPassword"
																			       class="form-control form-control-input"
																			       placeholder="Confirm Password"
																			       name="user[password_confirm]"
																			        >
																		</div>
																	</div>
																</div>

																<div class="form-collapse-row row-collapse-acc-suspension">
																	<div class="collapse-title-lbl"><span
																				class="title-collapse">Account Suspension Settings</span>
																	</div>
																	<div class="btn-switch-container">
																		<label class="btn-switch btn-color-mode-switch">
																			<input type="hidden" name="user[status]" value="1" />
																			<input type="checkbox" name="user[status]" id="acc_susp" value="99"<?php echo (int) $user['status'] === 99 ? ' checked' : ''; ?>>
																			<label for="acc_susp" data-on="Yes" data-off="No" class="btn-color-mode-switch-inner"></label>
																		</label>
																	</div>
																</div>

																<div class="form-group form-acc-suspension"style="display: none;">
																	<div class="collapse-title-lbl"><span class="title-collapse">Suspension Expires</span> </div>
																	<div class="form-flex">
																		<div class="input-group-flex row-suspension-expire">
																			<div class="form-group">
																				<div class="input-group input-group-datetimepicker date" id="form_datesuspension" data-target-input="nearest" data-date-format="YYYY-MM-DD">
																					<input type="text" class="form-control datetimepicker-input" data-target="#form_datesuspension" placeholder="Date" name="user[status_date]" value="<?php echo $user['status_date'] != '0000-00-00' ? $user['status_date'] : ''; ?>">
																					<span class="input-group-addon" data-target="#form_datesuspension" data-toggle="datetimepicker">
																						<span class="fa fa-calendar"></span>
                                                                                    </span>
																				</div>
																			</div>
																			<span class="text-seperator">OR</span>
																			<div class="form-group">
																				<div class="custom-control custom-checkbox">
																					<input type="checkbox" class="custom-control-input" id="accSuspNoDate" value="1"<?php echo $user['status_date'] == '0000-00-00' ? ' checked' : ''; ?>>
																					<label class="custom-control-label" for="accSuspNoDate">Not specified</label>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-flex row-acc-suspension-notify">
																			<div class="custom-control custom-checkbox">
																				<input type="checkbox" class="custom-control-input" id="accSuspNotify" name="user[status_notify]" value="1"<?php echo $user['status_notify'] ? ' checked' : ''; ?>>
																				<label class="custom-control-label" for="accSuspNotify">Notify user</label>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="footer-form-action">
														<button type="submit" class="btn-icon-full btn-confirm">
															<span class="btn-label">Save</span>
															<span class="btn-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                     width="24" height="24"
                                                                     viewBox="0 0 24 24" fill="none"
                                                                     stroke="currentColor"
                                                                     stroke-width="2.5"
                                                                     stroke-linecap="round"
                                                                     stroke-linejoin="arcs">
                                                                    <polyline
                                                                            points="20 6 9 17 4 12"></polyline>
                                                                </svg>
                                                            </span>
														</button>
														<button type="button" class="btn-icon-full btn-cancel back">
															<span class="btn-label">Cancel</span>
															<span class="btn-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                     width="24" height="24"
                                                                     viewBox="0 0 24 24" fill="none"
                                                                     stroke="currentColor"
                                                                     stroke-width="2.5"
                                                                     stroke-linecap="round"
                                                                     stroke-linejoin="arcs">
                                                                    <line x1="18" y1="6" x2="6"
                                                                          y2="18"></line>
                                                                    <line x1="6" y1="6" x2="18"
                                                                          y2="18"></line>
                                                                </svg>
                                                            </span>
														</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal - Add Employment -->
<div id="modal_add_employment" class="modal modal-add-employment modal-w-footer fade" aria-labelledby="modal_add_employment" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<form class="form-individualprofile" id="addEmployment">
			<div class="modal-content">
				<div class="modal-header">Add Employment</div>
				<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
						<line x1="18" y1="6" x2="6" y2="18"></line>
						<line x1="6" y1="6" x2="18" y2="18"></line>
					</svg>
				</button>
				<div class="modal-body">
					<div class="modal-body-container">
						<input type="hidden" name="employment[id]">
						<input type="hidden" name="employment[user_id]" value="<?php echo $user['id'] ?>">
						<div class="form-flex">
							<div class="input-group-flex row-indcompname">
								<input type="text" id="companyName" class="form-control form-control-input" placeholder="Company Name" name="employment[company_name]" required="" maxlength="200">
							</div>
							<div class="input-group-flex row-indrole">
								<input type="text" id="positionRole" class="form-control form-control-input" placeholder="Position / Role" name="employment[position]" required="" maxlength="100">
							</div>
							<div class="input-group-flex row-indworkstart">
								<div class="form-group">
									<div class="input-group input-group-datetimepicker date_start" id="form_dateworkstart" data-target-input="nearest">
										<input type="text" class="form-control datetimepicker-input" data-target="#form_dateworkstart" placeholder="Start Date" name="employment[date_start]">
										<span class="input-group-addon" data-target="#form_dateworkstart" data-toggle="datetimepicker">
                                                <span class="fa fa-calendar"></span>
                                            </span>
									</div>
									<div class="input-group input-group-datetimepicker date_end" id="form_dateworkend" data-target-input="nearest">
										<input type="text" class="form-control datetimepicker-input" data-target="#form_dateworkend" placeholder="End Date" name="employment[date_end]">
										<span class="input-group-addon" data-target="#form_dateworkend" data-toggle="datetimepicker" id="workend">
                                                <span class="fa fa-calendar"></span>
                                            </span>
									</div>
								</div>
							</div>
							<div class="input-group-flex row-indworkend">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" id="currently_working" class="custom-control-input" name="employment[currently_working]" value="1">
									<label class="custom-control-label" for="currently_working">I am currently working on this role</label>
								</div>
							</div>
							<div class="input-group-flex row-inddept">
								<input type="text" id="department" class="form-control form-control-input" placeholder="Department" name="employment[department]" required="" maxlength="200">
							</div>
							<div class="input-group-flex row-indindustry">
								<select class="form-control form-control-input" placeholder="Industry" name="employment[industry]">
									<option disabled selected>Industry</option>
                                    <?php foreach($cms->getIndustries2Level() as $industry){ if($industry['subs']){ ?>
										<optgroup label="<?php echo $industry['title']; ?>">
                                            <?php foreach($industry['subs'] as $sub){ ?>
												<option value="<?php echo $sub['id']; ?>"><?php echo $sub['title']; ?></option>
                                            <?php } ?>
										</optgroup>
                                    <?php }} ?>
								</select>
							</div>
							<div class="input-group-flex row-indjd">
								<textarea class="form-control form-control-input summernote" placeholder="Job Description" id="od_jobd" rows="3" name="employment[job_desc]" maxlength="500" ></textarea>
							</div>
							<div class="input-group-flex row-indrespon">
								<textarea class="form-control form-control-input summernote" placeholder="Responsibilities" id="od_jobr" rows="3" name="employment[job_responsibilities]" maxlength="500"></textarea>
							</div>
							<div class="input-group-flex row-indremunerate">
								<input type="text" id="remuneration" class="form-control form-control-input" placeholder="Remuneration (Last Drawn)" name="employment[remuneration]" required="" maxlength="6">
							</div>
							<div class="input-group-flex row-indlocation">
								<select class="form-control form-control-input" placeholder="Location" name="employment[state]" data-selected="<?php echo $user['state']; ?>" required>
									<option disabled selected>State</option>
                                    <?php foreach($cms->states($user['country']) as $key){ ?>
										<option value="<?php echo $key['id']; ?>"<?php echo $key['id'] == $user['state'] ? ' selected' : '' ?>><?php echo $key['name']; ?></option>
                                    <?php } ?>
								</select>
							</div>
							<div class="input-group-flex row-indlocation">
								<select class="form-control form-control-input country" placeholder="Location" name="employment[country]" data-target="employment[state]" data-selected="<?php echo $user['country']; ?>" required>
									<option disabled selected>Country</option>
                                    <?php foreach($cms->countries() as $key){ ?>
										<option value="<?php echo $key['id']; ?>"<?php echo $key['id'] == $user['country'] ? ' selected' : '' ?>><?php echo $key['name']; ?></option>
                                    <?php } ?>
								</select>
							</div>

						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
						<span class="btn-label">Cancel</span>
						<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
					</button>
					<button type="submit" class="btn-icon-full btn-confirm">
						<span class="btn-label">Submit</span>
						<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- /.modal -->

<!-- Modal - Add Education -->
<div id="modal_add_education" class="modal modal-add-education modal-w-footer fade" aria-labelledby="modal_add_education" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-individualprofile" id="addEducation">
				<div class="modal-header">Add Education</div>
				<button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
						<line x1="18" y1="6" x2="6" y2="18"></line>
						<line x1="6" y1="6" x2="18" y2="18"></line>
					</svg>
				</button>
				<div class="modal-body">
					<div class="modal-body-container">
						<input type="hidden" name="education[id]">
						<input type="hidden" name="education[user_id]" value="<?php echo $user['id'] ?>">
						<div class="form-flex">
							<div class="input-group-flex row-indeduins">
								<input type="text" id="eduIn" class="form-control form-control-input" placeholder="Education Institution" name="education[edu_institution]" required="" maxlength="200">
							</div>
							<div class="input-group-flex row-indedulevel">
								<select class="form-control form-control-input" placeholder="Highest Education Level" name="education[highest_edu_level]">
									<option disabled selected>Highest Education Level</option>
									<option value="Elementary">Elementary</option>
									<option value="Secondary">Secondary</option>
									<option value="Certification">Certification</option>
									<option value="Diploma">Diploma</option>
									<option value="Degree">Degree</option>
									<option value="Master">Master</option>
									<option value="PhD">PhD</option>
								</select>
							</div>
							<div class="input-group-flex row-indfieldstudy">
								<select class="form-control form-control-input" placeholder="Field of Study" name="education[field]">
									<option disabled selected>Field of Study</option>
									<option value="Business">Business</option>
									<option value="Computer Science">Computer Science</option>
									<option value="Social Work">Social Work</option>
									<option value="Engineering">Engineering</option>
								</select>
							</div>
							<div class="input-group-flex row-indgradeyear">
								<div class="input-group input-group-datetimepicker date" id="grad_year" data-target-input="nearest" data-date-format="YYYY">
									<input type="text" class="form-control datetimepicker-input" placeholder="Graduation Year" name="education[grad_year]">
									<span class="input-group-addon" data-target="#grad_year" data-toggle="datetimepicker">
                                            <span class="fa fa-calendar"></span>
                                        </span>
								</div>
                                <?php /*
                                    <select class="form-control form-control-input" placeholder="Graduation Year" name="education[grad_year]">
                                        <option disabled selected>Graduation Year</option>
                                        <option value="2020">2020</option>
                                        <option value="2019">2019</option>
                                        <option value="2018">2018</option>
                                        <option value="2017">2017</option>
                                        <option value="2016">2016</option>
                                        <option value="2015">2015</option>
                                    </select>
                                    */ ?>
							</div>
							<div class="input-group-flex row-indmajor">
								<input type="text" id="major" class="form-control form-control-input" placeholder="Major" required="" name="education[major]" maxlength="200">
							</div>
							<div class="input-group-flex row-indgrade">
								<input type="text" id="grade" class="form-control form-control-input" placeholder="Grade" required="" name="education[grade]" maxlength="200">
							</div>
							<div class="input-group-flex row-indlocation">
								<select class="form-control form-control-input" placeholder="Location" name="education[state]" data-selected="<?php echo $user['state']; ?>" required>
									<option disabled selected>State</option>
                                    <?php foreach($cms->states($user['country']) as $key){ ?>
										<option value="<?php echo $key['id']; ?>"<?php echo $key['id'] == $user['state'] ? ' selected' : '' ?>><?php echo $key['name']; ?></option>
                                    <?php } ?>
								</select>
							</div>
							<div class="input-group-flex row-indlocation">
								<select class="form-control form-control-input country" placeholder="Location" name="education[country]" data-target="education[state]" data-selected="<?php echo $user['country']; ?>" required>
									<option disabled selected>Country</option>
                                    <?php foreach($cms->countries() as $key){ ?>
										<option value="<?php echo $key['id']; ?>"<?php echo $key['id'] == $user['country'] ? ' selected' : '' ?>><?php echo $key['name']; ?></option>
                                    <?php } ?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
						<span class="btn-label">Cancel</span>
						<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
					</button>
					<button type="submit" class="btn-icon-full btn-confirm">
						<span class="btn-label">Submit</span>
						<span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /.modal -->

<script>

    if ($('.date_start').length) {
        $('.date_start').datetimepicker({
            format: 'DD/MM/YYYY',
            maxDate: '<?php echo date('Y-m-d', strtotime('-1 day')); ?>',
        });
    }

    if ($('.date_end').length) {
        $('.date_end').datetimepicker({
            format: 'DD/MM/YYYY',
        });
    }

    $('#currently_working').on('change', function(){
        if($(this).prop('checked')){
            $('[name="employment[date_end]"]').val('').prop('disabled', true);
        }else{
            $('[name="employment[date_end]"]').prop('disabled', false);
        }
    });

    $('#modal_add_employment, #modal_add_education').on('show.bs.modal', function(){
        $(this).find('.country').change();
        $(this).find('[name="employment[currently_working]"]').change();
        $('.summernote').each(function(){
            val = $(this).val();
            placeholder = $(this).attr('placeholder');
            $(this).summernote({
                tooltip: false,
                placeholder: placeholder,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline']],
                    ['para', ['ul', 'ol']],
                ],
                height: 200,
                width: '100%',
            });
            $(this).summernote('code', val);
        });
    });

    $('#modal_add_employment, #modal_add_education').on('hide.bs.modal', function(){
        $(this).find('[name]:not([type="checkbox"])').val('');
        $(document).find('select').each(function(){
            if($(this).data('selected')){
                $(this).val($(this).data('selected'));
            }else{
                $(this).val($(this).find('option:first').val());
            }
        });
    });

    $('.row-collapse-employment').on('click', '.edit-employment', function (e) {
        e.preventDefault();
        modal = $('#modal_add_employment');
        data = $(this).data('data');
        $.each(data, function (key, val) {
            el = modal.find('[name="employment[' + key + ']"]');
            tag = el.prop('tagName');
            if (tag == 'INPUT') {
                if (el.prop('type') == 'text' || el.prop('type') == 'hidden') {
                    el.val(val);
                } else if (el.prop('type') == 'checkbox') {
                    el.val('1');
                    if (val == '1') {
                        el.prop('checked', true);
                    } else {
                        el.prop('checked', false);
                    }
                }
            } else if (tag == 'TEXTAREA') {
                el.val(val);
            } else if (tag == 'SELECT') {
                if (key == 'state') {
                    el.data('selected', val);
                }
                el.val(val);
            }
        });

        modal.find('.modal-header').text('Edit Employment');
        modal.modal('show');
    });

    $('#modal_add_employment').on('submit', '#addEmployment', function (e) {
        e.preventDefault();
        el = $(this);
        data = el.serializeArray();

        if (data[0].value) {
            url = "<?php echo url_for('individual/employment/update') ?>";

            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: data,
                success: function (results) {
                    if (results.error == true) {
                        t('e', results.msg);
                    } else {
                        $('#modal_add_employment').modal('hide');
                        t('s', results.msg);

                        if(data[0].value){
                            $('#emp-' + data[0].value).replaceWith(results.html);
                        }else{
                            $('.employment-container').append(results.html);
                        }
                    }
                    return false;
                },
                error: function (error) {
                    ajax_error(error);
                },
                complete: function () {
                }
            });
        }

        return false;
    });

    $('.row-collapse-education').on('click', '.edit-education', function (e) {
        e.preventDefault();
        modal = $('#modal_add_education');
        data = $(this).data('data');
        $.each(data, function (key, val) {
            el = modal.find('[name="education[' + key + ']"]');
            tag = el.prop('tagName');
            if (tag == 'INPUT') {
                if (el.prop('type') == 'text' || el.prop('type') == 'hidden') {
                    el.val(val);
                } else if (el.prop('type') == 'checkbox') {
                    el.val('1');
                    if (val == '1') {
                        el.prop('checked', true);
                    } else {
                        el.prop('checked', false);
                    }
                }
            } else if (tag == 'TEXTAREA') {
                el.val(val);
            } else if (tag == 'SELECT') {
                if (key == 'state') {
                    el.data('selected', val);
                }
                el.val(val);
            }
        });

        modal.find('.modal-header').text('Edit Education');
        modal.modal('show');
    });

    $('#modal_add_education').on('submit', '#addEducation', function (e) {
        e.preventDefault();
        el = $(this);
        data = el.serializeArray();

        if (data[0].value) {
            url = "<?php echo url_for('individual/education/update') ?>";

            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: data,
                success: function (results) {
                    if (results.error == true) {
                        t('e', results.msg);
                    } else {
                        $('#modal_add_education').modal('hide');
                        t('s', results.msg);

                        if (data[0].value) {
                            $('#edu-' + data[0].value).replaceWith(results.html);
                        } else {
                            $('.education-container').append(results.html);
                        }
                    }
                    return false;
                },
                error: function (error) {
                    ajax_error(error);
                },
                complete: function () {
                }
            });
        }

        return false;
    });


    $('.language-input').tagsinput({
        freeInput: false,
        typeaheadjs: {
            source: function (q, sync) {
                languages = <?php echo json_encode(languageList()); ?>;
                results = Array();
                $(languages).each(function (i, val) {
                    if (val.toLowerCase().indexOf(q.toLowerCase()) != '-1') {
                        results.push(val);
                    }
                });
                sync(results);
            }
        }
    });

    skills = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '<?php echo option('site_uri') ?>ajax/skills.json?all=1'
    });

    skills.clearPrefetchCache();
    skills.initialize();

    $('.skills').tagsinput({
        itemValue: function (item) {
            return item.id !== undefined ? item.id : 'new:' + item
        },
        itemText: function (item) {
            return item.name || item
        },
        typeaheadjs: {
            name: 'skills',
            displayKey: 'name',
            source: skills.ttAdapter(),
            templates: {
                empty: Handlebars.compile("<div class='tt-empty'>No skills found</div><span class='tt-suggestion-footer tt-selectable add-new-tag'>Add new '{{query}}'</span>")
            }
        },
        freeInput: true,
    });
    $('.skills').on('beforeItemAdd', function (event) {
        if (typeof (event.item) === 'string') {
            event.cancel = true;
            var target = $(this);
            target.tagsinput('add', {id: 'new:' + event.item, name: event.item});
            //target.tagsinput('refresh');
        }
    });

    <?php if($selected_skills){ foreach($selected_skills as $skill){ ?>
    $('.skills').tagsinput('add', {'id': '<?php echo $skill['id']; ?>', 'name': '<?php echo $skill['name']; ?>'});
	<?php }} ?>
	
	$(function(){
		$(document).on('change', '[name="preference[rm_work_location]"]', function(){
            $('.rm-work').hide();
            val = $(this).val();
            
            if(val == 'near'){
                $('.rm-radius').removeClass('d-none').show();
            }else if(val == 'same_country'){
                $('.rm-state').removeClass('d-none').show();
            }
        });
        $('[name="preference[rm_work_location]"]').change();

        $(document).on('change', '[name="preference[p_work_location]"]', function(){
            $('.p-work').hide();
            val = $(this).val();
            
            if(val == 'near'){
                $('.p-radius').removeClass('d-none').show();
            }else if(val == 'same_country'){
                $('.p-state').removeClass('d-none').show();
            }
        });
		$('[name="preference[p_work_location]"]').change();
	});

	$(function(){
		setTimeout(function(){
			$('#acc_susp, #accSuspNoDate').change();
		}, 100);

		$('#collapseBiling').on('show.bs.collapse', function () {
			$('iframe').attr( 'src', function ( i, val ) { return val; });
		});
	});

	function resizeIframe(obj) {
		obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';
	}
</script>