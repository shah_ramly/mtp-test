			
			<div class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-chart">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-12 col-header-filter">
                                        <h2 class="card-title">Master Data Controller</h2>
                                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
                                            <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                            </svg>
                                        </button>
                                        <ul class="col-account-header">
                                            <li class="dropdown col-right-flex col-account">
                                                <button type="button" class="btn btn-success btn-block btn-logout" onclick="location.href='<?php echo url_for('/logout'); ?>';">
                                                    <span class="plus-icon-lbl">Log Out</span>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body-backend card-body-backend-language">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="row">
                                            <div class="col-xl-12 col-backend-top">
                                                <div class="table-listing-header-container">
                                                    <div class="table-listing-header-title">State</div>
                                                    <div class="table-listing-header-actions">
                                                        <button class="btn-icon-only btn-delete btn-delete-all btn-disabled" type="button" disabled>
                                                            <span class="public-search-icon search-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                    <polyline points="3 6 5 6 21 6"></polyline>
                                                                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                                    <line x1="10" y1="11" x2="10" y2="17"></line>
                                                                    <line x1="14" y1="11" x2="14" y2="17"></line>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                        <button type="button" class="btn-icon-full btn-add" onclick="location.href='<?php echo url_for('/state/add'); ?>'">
                                                            <span class="btn-label">Add State</span>
                                                            <span class="btn-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                                                    <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                    <line x1="5" y1="12" x2="19" y2="12"></line>
                                                                </svg>
                                                            </span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-backend-top-wrapper">
                                                    <div class="row">
                                                        <div class="col-xl-12 col-myprofile">
                                                            <form method="GET">
                                                            <div class="table-filter-col">
                                                                <div class="form-group form-task-title">
                                                                    <input type="text" class="form-control form-control-input" placeholder="State Name" name="name" value="<?php echo $param['name'] ?? ''; ?>">
                                                                </div>
                                                                <div class="form-group">
                                                                    <select class="form-control form-control-input" placeholder="Country" name="country">
                                                                        <option value="0">Country</option>
                                                                        <?php foreach($cms->countries() as $country){ ?>
                                                                        <option value="<?php echo $country['id']; ?>"<?php echo !empty($param['country']) && $param['country'] == $country['id'] ? ' selected' : ''; ?>><?php echo $country['name']; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-actions">
                                                                    <button class="btn-search-public" type="submit">
                                                                        <span class="btn-label">Search</span>
                                                                        <span class="public-search-icon search-icon">
                                                                            <svg class="bi bi-search" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                                                                <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                                                            </svg>
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            </form>
                                                            <div class="table-listing-container">
                                                                <table class="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="col-actions" width="50">
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input" id="existing_All">
                                                                                    <label class="custom-control-label" for="existing_All"></label>
                                                                                </div>
                                                                            </th>
                                                                            <th class="col-state">Country</th>
                                                                            <th class="col-country">State</th>
                                                                            <th class="col-actions"></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php foreach($keys as $key){ ?>
                                                                        <tr <?php echo in_array($key['status'], ['0', '999']) ? 'class="bg-warning"' : '' ?>>
                                                                            <td class="info">
                                                                                <div class="custom-control custom-checkbox">
                                                                                    <input type="checkbox" class="custom-control-input existing-selected" id="delete_<?php echo $key['id']; ?>" name="selected[]" value="<?php echo $key['id']; ?>">
                                                                                    <label class="custom-control-label" for="delete_<?php echo $key['id']; ?>"></label>
                                                                                </div>
                                                                            </td>
                                                                            <td class="info <?php echo in_array($key['status'], ['0', '999']) ? 'text-white' : '' ?>" width="100"><?php echo $key['country_name']; ?></td>
                                                                            <td class="info <?php echo in_array($key['status'], ['0', '999']) ? 'text-white' : '' ?>"><?php echo $key['name']; ?></td>
                                                                            <td class="actions">
                                                                                <div class="table-actions-container">
                                                                                    <a class="btn-icon-only btn-edit mr-2" href="<?php echo url_for('/state/' . $key['id']); ?>">
                                                                                        <span class="public-search-icon search-icon">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                                                                <path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path>
                                                                                                <polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon>
                                                                                            </svg>
                                                                                        </span>
                                                                                    </a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="table-listing-footer-pagination">
                                                                <div class="footer-total-page">Page <?php echo $pagination->page; ?> of <?php echo $pagination->totalPage; ?></div>
                                                                <nav class="nav-tbl-pagination">
                                                                    <?php if($pagination->totalRecord){ ?>													
                                                                    <ul class="pagination">
                                                                        <?php echo $pagination->display(); ?>
                                                                    </ul>
                                                                    <?php } ?>
                                                                </nav>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <script>
    $(function(){
        $(document).on('change', '#existing_All', function(){
            if( $(this).prop('checked') ){
                $('.existing-selected').prop('checked', true);
                $('.btn-delete-all:visible').removeClass('btn-disabled').prop('disabled', false);
            }else{
                $('.existing-selected').prop('checked', false);
                $('.btn-delete-all:visible').addClass('btn-disabled').prop('disabled', true);
            }
        });

        $(document).on('change', '.existing-selected', function(){
            all = $('.existing-selected').length;
            checked = $('.existing-selected:checked').length;

            if(checked){
                $('.btn-delete-all:visible').removeClass('btn-disabled').prop('disabled', false);
            }else{
                $('.btn-delete-all:visible').addClass('btn-disabled').prop('disabled', true);
            }

            if( checked == all ){
                $('#existing_All').prop('checked', true);
            }else{
                $('#existing_All').prop('checked', false);
            }
        });

        $(document).on('click', '.btn-delete-all', function(){
            if( confirm('Are you sure you want to delete selected states?') ){
                selected = $('[name="selected[]"]:checked:visible').map(function(_, el) {
                    return $(el).val();
                }).get();

                $.ajax({
                    type:'POST',
                    url: rootPath + 'state/delete',
                    dataType: 'json',
                    data: { selected: selected },
                    success: function(results){
                        if(results.error == true){
                            t('e', results.msg);
                        }else{
                            t('s', results.msg);
                            setTimeout(function(){
                                window.location.reload();
                            }, 1000);
                        }
                    },
                    error: function(error){
                        ajax_error(error);
                    },
                });
            }
        });
    });
</script>