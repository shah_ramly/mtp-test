<?php
$data_controller = ['countryList','countryListAdd','countryListEdit','stateList','stateListAdd','stateListEdit','cityList','cityListAdd','cityListEdit','industryList','industryListAdd','industryListEdit','categoryList','categoryListAdd','categoryListEdit','skillList','skillListAdd','skillListEdit','language','languageText','interestList','interestListAdd','interestListEdit'];
$configurations = ['systemSettings','admin','adminAdd','adminEdit','adminRole','adminRoleAdd','adminRoleEdit'];
?>

                <ul class="nav top">
                    <?php if(in_array('analytics_report', $cms->admin['permissions'])){ ?>
                    <li class="nav-item<?php navActive($currPath, 'analytics'); ?>">
                        <a class="nav-link dropdown-toggle" href="<?php echo url_for('/analytics-report'); ?>" id="navbarDropdown1" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="tim-icons icon-chart-bar-32"></i>
                            <p>Analytics Reports</p>
                        </a>
                    </li>
                    <?php } ?>

                    <?php if(in_array('individual', $cms->admin['permissions'])){ ?>
                    <li class="nav-item<?php navActive($currPath, ['individualManagement','individualDetail']); ?>">
                        <a class="nav-link dropdown-toggle" href="<?php echo url_for('/individual'); ?>" id="navbarDropdown2" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="tim-icons icon-badge"></i>
                            <p>Individual Management</p>
                        </a>
                    </li>
                    <?php } ?>

                    <?php if(in_array('company', $cms->admin['permissions'])){ ?>
                    <li class="nav-item<?php navActive($currPath, ['companyManagement','companyDetail']); ?>">
                        <a class="nav-link dropdown-toggle" href="<?php echo url_for('/company'); ?>" id="navbarDropdown3" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="tim-icons icon-badge"></i>
                            <p>Company Management</p>
                        </a>
                    </li>
                    <?php } ?>

                    <?php if(array_intersect(['task_organic','task_aggregate'], $cms->admin['permissions'])){ ?>
                    <li class="nav-item<?php navActive($currPath, 'taskManagement'); ?>">
                        <a class="nav-link dropdown-toggle" href="<?php echo url_for('/task'); ?>" id="navbarDropdown4" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="tim-icons icon-badge"></i>
                            <p>Task Management</p>
                        </a>
                    </li>
                    <?php } ?>

                    <?php if(array_intersect(['job_organic','job_aggregate'], $cms->admin['permissions'])){ ?>
                    <li class="nav-item<?php navActive($currPath, 'jobManagement'); ?>">
                        <a class="nav-link dropdown-toggle" href="<?php echo url_for('/job'); ?>" id="navbarDropdown5" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="tim-icons icon-badge"></i>
                            <p>Job Management</p>
                        </a>
                    </li>
                    <?php } ?>

                    <?php if(array_intersect(['master_data_country','master_data_state','master_data_city','master_data_language','master_data_industry_existing','master_data_industry_entered','master_data_industry_language','master_data_category_existing','master_data_category_entered','master_data_category_language','master_data_skill_existing','master_data_skill_entered','master_data_skill_language','master_data_interest_existing','master_data_interest_entered','master_data_interest_language'], $cms->admin['permissions'])){ ?>
                    <li class="nav-item dropdown dropright<?php navActive($currPath, $data_controller); ?>">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown6" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="tim-icons icon-single-copy-04"></i>
                            <p>Master Data Controller</p>
                            <span class="dropright-chevron">
                                <svg class="bi bi-chevron-right" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                                </svg>
                            </span>
                        </a>
                        <div class="dropdown-menu dropright-menu" aria-labelledby="navbarDropdown6">
                            <?php if(in_array('master_data_country', $cms->admin['permissions'])){ ?>
                            <a class="dropdown-item sub-dropright<?php navActive($currPath, ['countryList','countryListAdd','countryListEdit']); ?>" href="<?php echo url_for('/country'); ?>">Countries</a>
                            <?php } ?>
                            <?php if(in_array('master_data_state', $cms->admin['permissions'])){ ?>
                            <a class="dropdown-item sub-dropright<?php navActive($currPath, ['stateList','stateListAdd','stateListEdit']); ?>" href="<?php echo url_for('/state'); ?>">States</a>
                            <?php } ?>
                            <?php if(in_array('master_data_city', $cms->admin['permissions'])){ ?>
                            <a class="dropdown-item sub-dropright<?php navActive($currPath, ['cityList','cityListAdd','cityListEdit']); ?>" href="<?php echo url_for('/city'); ?>">Cities</a>
                            <?php } ?>
                            <?php if(in_array('master_data_language', $cms->admin['permissions'])){ ?>
                            <a class="dropdown-item sub-dropright<?php navActive($currPath, ['language','languageText']); ?>" href="<?php echo url_for('/language'); ?>">Languages</a>
                            <?php } ?>
                            <?php if(array_intersect(['master_data_industry_existing','master_data_industry_entered','master_data_industry_language'], $cms->admin['permissions'])){ ?>
                            <a class="dropdown-item sub-dropright<?php navActive($currPath, ['industryList','industryListAdd','industryListEdit']); ?>" href="<?php echo url_for('/industry'); ?>">Industries</a>
                            <?php } ?>
                            <?php if(array_intersect(['master_data_category_existing','master_data_category_entered','master_data_category_language'], $cms->admin['permissions'])){ ?>
                            <a class="dropdown-item sub-dropright<?php navActive($currPath, ['categoryList','categoryListAdd','categoryListEdit']); ?>" href="<?php echo url_for('/category'); ?>">Categories</a>
                            <?php } ?>
                            <?php if(array_intersect(['master_data_skill_existing','master_data_skill_entered','master_data_skill_language'], $cms->admin['permissions'])){ ?>
                            <a class="dropdown-item sub-dropright<?php navActive($currPath, ['skillList','skillListAdd','skillListEdit']); ?>" href="<?php echo url_for('/skill'); ?>">Skills</a>
                            <?php } ?>
                            <?php if(array_intersect(['master_data_interest_existing','master_data_interest_entered','master_data_interest_language'], $cms->admin['permissions'])){ ?>
                            <a class="dropdown-item sub-dropright<?php navActive($currPath, ['interestList','interestListAdd','interestListEdit']); ?>" href="<?php echo url_for('/interest'); ?>">Interests</a>
                            <?php } ?>
                        </div>
                    </li>
                    <?php } ?>

                    <?php if(in_array('subscription', $cms->admin['permissions'])){ ?>
                    <li class="nav-item<?php navActive($currPath, 'subscription'); ?>">
                        <a class="nav-link dropdown-toggle" href="<?php echo url_for('/subscription'); ?>" id="navbarDropdown7" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="tim-icons icon-money-coins"></i>
                            <p>Subscription Management</p>
                        </a>
                    </li>
                    <?php } ?>

                    <?php if(array_intersect(['master_config_admin_admin','master_config_admin_role','master_config_system','master_config_notification'], $cms->admin['permissions'])){ ?>
                    <li class="nav-item dropdown dropright<?php navActive($currPath, $configurations); ?>">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown8" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="tim-icons icon-settings-gear-63"></i>
                            <p>Master Configurations</p>
                            <span class="dropright-chevron">
                                <svg class="bi bi-chevron-right" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                                </svg>
                            </span>
                        </a>
                        <div class="dropdown-menu dropright-menu" aria-labelledby="navbarDropdown8">
                            <?php if(array_intersect(['master_data_industry_existing','master_data_industry_entered','master_data_industry_language'], $cms->admin['permissions'])){ ?>
                            <a class="dropdown-item sub-dropright<?php navActive($currPath, ['admin','adminAdd','adminEdit','adminRole','adminRoleAdd','adminRoleEdit']); ?>" href="<?php echo url_for('/admin'); ?>">User & Roles</a>
                            <?php } ?>
                            <?php if(in_array('master_config_system', $cms->admin['permissions'])){ ?>
                            <a class="dropdown-item sub-dropright<?php navActive($currPath, 'systemSettings'); ?>" href="<?php echo url_for('/system-settings'); ?>">System Settings</a>
                            <?php } ?>
                            <?php if(in_array('master_config_notification', $cms->admin['permissions'])){ ?>
                            <a class="dropdown-item sub-dropright" href="<?php echo url_for('/notifications'); ?>">Notifications</a>
                            <?php } ?>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
