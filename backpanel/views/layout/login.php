
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo url_for('../assets/img/apple-icon.png'); ?>">
    <link rel="icon" type="image/png" href="<?php echo url_for('../assets/img/favicon.png'); ?>">
    <title>Login | Make Time Pay</title>

    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet" />
    <link href="<?php echo url_for('../assets/css/glyphicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo url_for('../assets/css/font-awesome.min.css'); ?>" rel="stylesheet">

    <!-- Nucleo Icons -->
    <link href="<?php echo url_for('../assets/css/nucleo-icons.css'); ?>" rel="stylesheet" />

    <!-- CSS Files -->
    <link href="<?php echo url_for('../assets/css/animate.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo url_for('../assets/css/bootstrap-dropdownhover.min.css'); ?>" rel="stylesheet">

    <link href="<?php echo url_for('../assets/css/tempusdominus-bootstrap-4.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo url_for('../assets/css/bootstrap-tagsinput.css'); ?>" rel="stylesheet">
    <link href="<?php echo url_for('../assets/css/dropzone.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo url_for('../assets/css/swiper.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo url_for('../assets/css/typeahead.css'); ?>" rel="stylesheet" />
    <link href="<?php echo url_for('../assets/css/bootstrap-slider.min.css'); ?>" rel="stylesheet" />

    <link href="<?php echo url_for('../assets/css/global.css'); ?>" rel="stylesheet" />
    <link href="<?php echo url_for('../assets/css/backend-style.css'); ?>" rel="stylesheet" />
</head>

<body class="white-content page-login">
    <div class="wrapper">
        
        <div class="container">
            <div class="full-content-center">
                <div class="login-wrap">
                    <div class="login-logo">
                        <img src="<?php echo url_for('../assets/img/mtp_logo_dark.png'); ?>">
                    </div>
                    <div class="login-block">
                        <form id="loginform">
                            <div class="form-group login-input"><input type="text" class="form-control text-input" name="username" placeholder="Username" required="required"> </div>
                            <div class="form-group login-input"><input type="password" class="form-control text-input" name="password" placeholder="********" required="required"> </div>
                            <div class="row text-center">
                                <div class="col-sm-12"> <button type="submit" class="btn btn-success btn-block btn-login">Login</button> </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> <!-- the overlay modal element -->

    </div>
    


    <!--   Core JS Files   -->
    <script src="<?php echo url_for('../assets/js/core/jquery.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/core/jquery-ui.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/core/popper.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/core/bootstrap.min.js'); ?>"></script>

    <!--   Plugins JS Files   -->
    <script src="<?php echo url_for('../assets/js/plugins/moment-with-locales.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/tempusdominus-bootstrap-4.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/perfect-scrollbar.jquery.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/dragscroll.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/bootstrap-tagsinput.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/swiper.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/bootstrap-slider.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/jquery.validate.js'); ?>"></script>

    <!--  Search Suggestion Plugin    -->
    <script src="<?php echo url_for('../assets/js/plugins/typeahead.bundle.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/bloodhound.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/handlebars.js'); ?>"></script>

    <!-- Chart JS -->
    <script src="<?php echo url_for('../assets/js/plugins/snap.svg-min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/chart.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/progressbar.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/d3.v3.min.js'); ?>"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo url_for('../assets/js/plugins/bootstrap-notify.js'); ?>"></script>

    <!--  Custom JS    -->
    <script src="<?php echo url_for('../assets/js/plugins/dropzone.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/main.js'); ?>"></script>
    <script src="<?php echo url_for('/assets/js/script.js'); ?>"></script>




    <script type="text/javascript">
        $(function() {
            $('#datetimepicker2').datetimepicker({
                locale: 'ru'
            });
        });

    </script>



    <!-- Sidebar Hover Function -->
    <script>
        // Sidebar Hover Function
        const $dropdown = $(".sidebar .dropdown");
        const $dropdownToggle = $(".sidebar .dropdown-toggle");
        const $dropdownMenu = $(".sidebar .dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function() {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                    function() {
                        const $this = $(this);
                        $this.addClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "true");
                        $this.find($dropdownMenu).addClass(showClass);
                    },
                    function() {
                        const $this = $(this);
                        $this.removeClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "false");
                        $this.find($dropdownMenu).removeClass(showClass);
                    }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });


        $('.card-header .dropdown-menu .nav-tabs .nav-link').on("click.bs.dropdown", function(e) {
            $(this).tab('show');
            e.stopPropagation();
        });
        $('.col-account-header .dropdown.col-account .switch-toggle .slider-toggler').on("click", function(e) {
            e.stopPropagation();
        });

    </script>
    <!-- Sidebar Hover Function -->

    <script>
        // Task Listing Filter - Range Budget Hour
        var range_budget_hour_filter = new Slider("#range_budget_hour_filter", {
            min: 10,
            max: 100,
            step: 1,
            value: [30, 65],
            range: true,
            tooltip_position: 'bottom',
            formatter: function(value) {
                return 'RM' + value[0] + ' - RM' + value[1];
            }
        });

        // Task Listing Filter - Range Budget Lumpsum
        var range_budget_lumpsum_filter = new Slider("#range_budget_lumpsum_filter", {
            min: 10,
            max: 1000,
            step: 50,
            value: [300, 650],
            range: true,
            tooltip_position: 'bottom',
            formatter: function(value) {
                return 'RM' + value[0] + ' - RM' + value[1];
            }
        });

    </script>




</body>

</html>
