
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo $settings['meta_description']; ?>">
    <meta name="keywords" content="<?php echo $settings['meta_keyword']; ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo url_for('../assets/img/apple-icon.png'); ?>">
    <link rel="icon" type="image/png" href="<?php echo url_for('../assets/img/favicon.png'); ?>">
    <title><?php echo SITE_NAME . ' :: Control Panel';?></title>

    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet" />
    <link href="<?php echo url_for('../assets/css/glyphicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo url_for('../assets/css/font-awesome.min.css'); ?>" rel="stylesheet">

    <!-- Nucleo Icons -->
    <link href="<?php echo url_for('../assets/css/nucleo-icons.css'); ?>" rel="stylesheet" />

    <!-- CSS Files -->
    <link href="<?php echo url_for('../assets/css/animate.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo url_for('../assets/css/bootstrap-dropdownhover.min.css'); ?>" rel="stylesheet">

    <link href="<?php echo url_for('../assets/css/tempusdominus-bootstrap-4.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo url_for('../assets/css/bootstrap-tagsinput.css'); ?>" rel="stylesheet">
    <link href="<?php echo url_for('../assets/css/dropzone.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo url_for('../assets/css/swiper.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo url_for('../assets/css/typeahead.css'); ?>" rel="stylesheet" />
    <link href="<?php echo url_for('../assets/css/bootstrap-slider.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo url_for('../assets/libs/summernote/summernote.min.css') ?>" rel="stylesheet" />

    <link href="<?php echo url_for('../assets/css/global.css'); ?>" rel="stylesheet" />
    <link href="<?php echo url_for('../assets/css/backend-style.css'); ?>" rel="stylesheet" />
    <link href="<?php echo url_for('../assets/css/backend-style-mtp.css'); ?>" rel="stylesheet" />

    <!--   Core JS Files   -->
    <script src="<?php echo url_for('../assets/js/core/jquery.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/core/jquery-ui.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/core/popper.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/core/bootstrap.min.js'); ?>"></script>

    <!--   Plugins JS Files   -->
    <script src="<?php echo url_for('../assets/js/plugins/moment-with-locales.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/tempusdominus-bootstrap-4.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/perfect-scrollbar.jquery.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/dragscroll.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/bootstrap-tagsinput.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/swiper.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/bootstrap-slider.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/jquery.validate.js'); ?>"></script>
	<script src="<?php echo url_for('../assets/libs/summernote/summernote.min.js') ?>"></script>

    <!--  Search Suggestion Plugin    -->
    <script src="<?php echo url_for('../assets/js/plugins/typeahead.bundle.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/bloodhound.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/handlebars.js'); ?>"></script>

    <!-- Chart JS -->
    <script src="<?php echo url_for('../assets/js/plugins/snap.svg-min.js') ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/chart.min.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/progressbar.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/plugins/d3.v3.min.js'); ?>"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo url_for('../assets/js/plugins/bootstrap-notify.js'); ?>"></script>

    <!--  Custom JS    -->
    <script src="<?php echo url_for('../assets/js/plugins/dropzone.js'); ?>"></script>
    <script src="<?php echo url_for('../assets/js/main.js'); ?>"></script>
    <script src="<?php echo url_for('/assets/js/script.js'); ?>"></script>
</head>

<body class="white-content od-mode">
    <div class="wrapper">
        <div class="main-panel bg-white">

