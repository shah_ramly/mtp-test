
            
        </div>
    </div>
    
    <!-- Modal - Add Country -->
    <div id="modal_add_industry" class="modal modal-add-industry modal-w-footer fade" aria-labelledby="modal_write_review" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">Add Industry</div>
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="modal-body-container">
                        <form class="">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-input" placeholder="Enter industry" required="" autofocus="">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                        <span class="btn-label">Submit</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->


    <!-- Modal - Add Skill -->
    <div id="modal_add_skill" class="modal modal-add-skill modal-w-footer fade" aria-labelledby="modal_add_skill" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">Add Skill</div>
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="modal-body-container">
                        <form class="">
                            <div class="form-group">
                                <select class="form-control form-control-input" placeholder="Select Category">
                                    <option disabled="" selected="">Select Category</option>
                                    <option>Art & Design</option>
                                    <option>Business, Finance & Administration</option>
                                    <option>Education</option>
                                    <option>Engineering</option>
                                    <option>Human Resource & Recruitmen</option>
                                    <option>IT & Data Management</option>
                                    <option>Office</option>
                                    <option>Project Management</option>
                                    <option>Sales & Marketing</option>
                                    <option>Communication & Interpersonal</option>
                                    <option>Management</option>
                                    <option>Personal</option>
                                    <option>Teamwork</option>
                                    <option>Thought Process</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control form-control-input" placeholder="Enter skill" required="" autofocus="">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <button type="button" class="btn-icon-full btn-confirm" data-dismiss="modal" data-orientation="next">
                        <span class="btn-label">Submit</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <script type="text/javascript">
        $(function() {
            $('#datetimepicker2').datetimepicker({
                locale: 'ru'
            });
        });

    </script>



    <!-- Sidebar Hover Function -->
    <script>
        // Sidebar Hover Function
        const $dropdown = $(".sidebar .dropdown");
        const $dropdownToggle = $(".sidebar .dropdown-toggle");
        const $dropdownMenu = $(".sidebar .dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function() {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                    function() {
                        const $this = $(this);
                        $this.addClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "true");
                        $this.find($dropdownMenu).addClass(showClass);
                    },
                    function() {
                        const $this = $(this);
                        $this.removeClass(showClass);
                        $this.find($dropdownToggle).attr("aria-expanded", "false");
                        $this.find($dropdownMenu).removeClass(showClass);
                    }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });


        $('.card-header .dropdown-menu .nav-tabs .nav-link').on("click.bs.dropdown", function(e) {
            $(this).tab('show');
            e.stopPropagation();
        });
        $('.col-account-header .dropdown.col-account .switch-toggle .slider-toggler').on("click", function(e) {
            e.stopPropagation();
        });

    </script>
    <!-- Sidebar Hover Function -->
    <script>
        $('.col-activity-log-wrapper a.page-link').on('click', function(e){ activity_paginate(e) });

        function activity_paginate(e){
            e.preventDefault();
            $.ajax({
                url: e.currentTarget.href
            }).done(function(response){
                $('.col-activity-log-wrapper').parent().html(response);
                $('.col-activity-log-wrapper a.page-link').off('click').on('click', function(e){ activity_paginate(e) });
            });
        }
    </script>
</body>

</html>
