
			<div class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-chart">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-12 col-header-filter">
                                        <h2 class="card-title">Master Data Controller</h2>
                                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
                                            <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                            </svg>
                                        </button>
                                        <ul class="col-account-header">
                                            <li class="dropdown col-right-flex col-account">
                                                <button type="button" class="btn btn-success btn-block btn-logout" onclick="location.href='<?php echo url_for('/logout'); ?>';">
                                                    <span class="plus-icon-lbl">Log Out</span>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body-backend card-body-backend-language">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="row">
                                            <div class="col-xl-12 col-backend-top">
                                                <div class="table-listing-header-container">
                                                    <div class="table-listing-header-title">Add City</div>
                                                </div>
                                                <div class="col-backend-top-wrapper">
                                                    <div class="row">
                                                        <div class="col-xl-12 col-language">
                                                            <form id="directForm" accept-charset="UTF-8">
                                                            <div class="table-listing-container">
                                                                <table class="table">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="info">City</td>
                                                                            <td class="info">
                                                                                <input type="text" class="form-control form-control-input" name="name">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="info">Country</td>
                                                                            <td class="info">
                                                                                <select class="form-control form-control-input country" data-target="state_id" placeholder="Country" name="country_id" required>
                                                                                    <option value="">Country</option>
                                                                                    <?php foreach($cms->countries() as $country){ ?>
                                                                                    <option value="<?php echo $country['id']; ?>"><?php echo $country['name']; ?></option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="info">State</td>
                                                                            <td class="info">
                                                                                <select class="form-control form-control-input" placeholder="State" name="state_id">
                                                                                    <option value="">State</option>
                                                                                    <?php /*foreach($cms->states($key['country_id']) as $state){ */?><!--
                                                                                    <option value="<?php /*echo $state['id']; */?>"><?php /*echo $state['name']; */?></option>
                                                                                    --><?php /*} */?>
                                                                                </select>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="info">State Code</td>
                                                                            <td class="info">
                                                                                <input type="text" class="form-control form-control-input" name="state_code">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="info">Latitude</td>
                                                                            <td class="info">
                                                                                <input type="text" class="form-control form-control-input" name="latitude">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="info">Longitude</td>
                                                                            <td class="info">
                                                                                <input type="text" class="form-control form-control-input" name="longitude">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="info">Wiki Data ID</td>
                                                                            <td class="info">
                                                                                <input type="text" class="form-control form-control-input" name="wikiDataId">
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="footer-form-action">
                                                                <button type="submit" class="btn-icon-full btn-confirm">
                                                                    <span class="btn-label">Add</span>
                                                                    <span class="btn-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                            <polyline points="20 6 9 17 4 12"></polyline>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                                <button type="button" class="btn-icon-full btn-cancel" onclick="location.href='<?php echo url_for('/city'); ?>'">
                                                                    <span class="btn-label">Cancel</span>
                                                                    <span class="btn-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                            <line x1="18" y1="6" x2="6" y2="18"></line>
                                                                            <line x1="6" y1="6" x2="18" y2="18"></line>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

    <!-- Modal - Add Language -->
    <div id="modal_add_language" class="modal modal-add-language modal-w-footer fade" aria-labelledby="modal_add_language" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="">
                <input type="hidden" name="id" value="">
                <div class="modal-header">Add Language</div>
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="modal-body-container">
                        <div class="form-group">
                            <input type="text" class="form-control form-control-input" name="title" placeholder="Enter language" required="" autofocus="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-icon-full btn-cancel" data-dismiss="modal" data-toggle="modal">
                        <span class="btn-label">Cancel</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </span>
                    </button>
                    <button type="submit" class="btn-icon-full btn-confirm" data-orientation="next">
                        <span class="btn-label">Submit</span>
                        <span class="btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                <polyline points="20 6 9 17 4 12"></polyline>
                            </svg>
                        </span>
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <script>
    $(function(){
        $('#modal_add_language').on('show.bs.modal', function(e){
            form = $(this).find('form');
            type = $(e.relatedTarget).data('type');
            title = type == 'add' ? 'Add Language' : 'Edit Language';
            val = type == 'edit' ? $(e.relatedTarget).closest('tr').find('.info-title').text() : '';
            $(this).find('.modal-header').text(title);
            form.find('[name="title"]').val(val);
            id = type == 'edit' ? $(e.relatedTarget).data('id') : '';
            formID = type == 'add' ? 'addLanguage' : 'editLanguage';
            form.attr('id', formID);
            form.find('[name="id"]').val(id);
        });
    });
    </script>
