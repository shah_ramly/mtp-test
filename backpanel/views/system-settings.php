
			<div class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-chart">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-12 col-header-filter">
                                        <h2 class="card-title">Master Configurations</h2>
                                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
                                            <svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                            </svg>
                                        </button>
                                        <ul class="col-account-header">
                                            <li class="dropdown col-right-flex col-account">
                                                <button type="button" class="btn btn-success btn-block btn-logout" onclick="location.href='<?php echo url_for('/logout'); ?>';">
                                                    <span class="plus-icon-lbl">Log Out</span>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body-backend card-body-backend-language">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="row">
                                            <div class="col-xl-12 col-backend-top">
                                                <div class="table-listing-header-container">
                                                    <div class="table-listing-header-title">System Settings</div>
                                                </div>
                                                <div class="col-backend-top-wrapper">
                                                    <div class="row">
                                                        <div class="col-xl-12 col-myprofile">
                                                            <form id="selfForm">
                                                            <div class="form-collapse-row row-collapse-timezone">
                                                                <div class="collapse-title-lbl collapsed" data-toggle="collapse" data-target="#collapseTimezone" aria-expanded="true" aria-controls="collapseTimezone">
                                                                    <span class="title-collapse">Timezone</span><i class="fa fa-caret-down"></i>
                                                                </div>
                                                                <div class="collapse" id="collapseTimezone">
                                                                    <div class="collapse-wrapper">
                                                                        <div class="form-flex form-row-timezone">
                                                                            <div class="input-group-block row-timezone">
                                                                                <select class="form-control form-control-input" name="timezone_id">
                                                                                    <option disabled="">Select Timezone</option>
                                                                                    <?php foreach(timezone_identifiers_list() as $timezone_id => $title){ ?>
                                                                                    <option value="<?php echo $timezone_id; ?>"<?php echo $cms->settings['timezone_id'] == $timezone_id ? ' selected' : ''; ?>><?php echo $title; ?></option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group form-row-date-format">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">Date Format</span>
                                                                            </label>
                                                                            <div class="form-radio-group">
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" class="custom-control-input" name="date_format" id="radio_dateFormat_1" value="F d, Y"<?php echo $cms->settings['date_format'] == 'F d, Y' ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label" for="radio_dateFormat_1"><?php echo date('F d, Y'); ?></label>
                                                                                </div>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" class="custom-control-input" name="date_format" id="radio_dateFormat_4" value="d M Y"<?php echo $cms->settings['date_format'] == 'd M Y' ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label" for="radio_dateFormat_4"><?php echo date('d M Y'); ?></label>
                                                                                </div>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" class="custom-control-input" name="date_format" id="radio_dateFormat_2" value="Y-m-d"<?php echo $cms->settings['date_format'] == 'Y-m-d' ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label" for="radio_dateFormat_2"><?php echo date('Y-m-d'); ?></label>
                                                                                </div>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" class="custom-control-input" name="date_format" id="radio_dateFormat_3" value="d/m/Y"<?php echo $cms->settings['date_format'] == 'd/m/Y' ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label" for="radio_dateFormat_3"><?php echo date('d/m/Y'); ?></label>
                                                                                </div>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" class="custom-control-input" name="date_format" id="radio_dateFormat_custom" value="custom"<?php echo $cms->settings['date_format'] == 'custom' ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label" for="radio_dateFormat_custom">Custom Format</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-flex row-date-format-custom">
                                                                                <input type="text" name="date_format_custom" class="form-control form-control-input" name="date_format_custom" value="<?php echo $cms->settings['date_format_custom']; ?>">
                                                                                <?php /*<span class="postlbl-hours">November 6, 2014</span>*/ ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group form-row-time-format">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">Time Format</span>
                                                                            </label>
                                                                            <div class="form-radio-group">
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" class="custom-control-input" name="time_format" id="radio_timeFormat_1" value="g:i a"<?php echo $cms->settings['time_format'] == 'g:i a' ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label" for="radio_timeFormat_1">9:07 pm</label>
                                                                                </div>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" class="custom-control-input" name="time_format" id="radio_timeFormat_2" value="g:i A"<?php echo $cms->settings['time_format'] == 'g:i A' ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label" for="radio_timeFormat_2">9:07 PM</label>
                                                                                </div>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" class="custom-control-input" name="time_format" id="radio_timeFormat_3" value="H:i"<?php echo $cms->settings['time_format'] == 'H:i' ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label" for="radio_timeFormat_3">21:07</label>
                                                                                </div>
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" class="custom-control-input" name="time_format" id="radio_timeFormat_custom" value="custom"<?php echo $cms->settings['time_format'] == 'custom' ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label" for="radio_timeFormat_custom">Custom Format</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-flex row-time-format-custom">
                                                                                <input type="text" name="time_format_custom" class="form-control form-control-input" value="<?php echo $cms->settings['time_format_custom']; ?>">
                                                                                <?php /*<span class="postlbl-hours">9:07 pm</span>*/ ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-collapse-row row-collapse-nationality">
                                                                <div class="collapse-title-lbl collapsed" data-toggle="collapse" data-target="#collapseNationality" aria-expanded="true" aria-controls="collapseNationality">
                                                                    <span class="title-collapse">Nationality Management</span><i class="fa fa-caret-down"></i>
                                                                </div>
                                                                <div class="collapse" id="collapseNationality">
                                                                    <div class="collapse-wrapper">
                                                                        <div class="bs-example">
                                                                            <input type="text" name="nationality" value="<?php echo $cms->settings['nationality']; ?>" data-role="tagsinput" required="" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-collapse-row row-collapse-cvmanagement">
                                                                <div class="collapse-title-lbl collapsed" data-toggle="collapse" data-target="#collapseCvManagement" aria-expanded="true" aria-controls="collapseCvManagement">
                                                                    <span class="title-collapse">CV Management</span><i class="fa fa-caret-down"></i>
                                                                </div>
                                                                <div class="collapse" id="collapseCvManagement">
                                                                    <div class="collapse-wrapper">
                                                                        <div class="form-group row-cvresume">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">CV/Resume</span>
                                                                            </label>
                                                                            <div class="form-flex form-file-flex">
                                                                                <input type="text" name="resume_max_file" class="form-control form-control-input" value="<?php echo $cms->settings['resume_max_file']; ?>">
                                                                                <span class="postlbl-hours">maximum upload number of file(s)</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row-coverletter">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">Cover Letter</span>
                                                                            </label>
                                                                            <div class="form-flex form-file-flex">
                                                                                <input type="text" name="cover_letter_max_file" class="form-control form-control-input" value="<?php echo $cms->settings['cover_letter_max_file']; ?>">
                                                                                <span class="postlbl-hours">maximum upload number of file(s)</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row-awardcert">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">Awards/Certifications/Qualifications</span>
                                                                            </label>
                                                                            <div class="form-flex form-file-flex">
                                                                                <input type="text" name="cert_max_file" class="form-control form-control-input" value="<?php echo $cms->settings['cert_max_file']; ?>">
                                                                                <span class="postlbl-hours">maximum upload number of file(s)</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row-fileformat">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">Allowed File Extensions</span>
                                                                            </label>
                                                                            <div class="form-flex form-fileformat-flex">
                                                                                <input type="text" name="docs_file_extensions" value="<?php echo $cms->settings['docs_file_extensions']; ?>" data-role="tagsinput" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row-filesize">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">File Size</span>
                                                                            </label>
                                                                            <div class="form-flex form-file-flex">
                                                                                <input type="number" name="docs_max_size" class="form-control form-control-input" value="<?php echo $cms->settings['docs_max_size']; ?>">
                                                                                <span class="postlbl-hours">maximum size of file(s) in megabytes (MB)</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-collapse-row row-collapse-avatarmanagement">
                                                                <div class="collapse-title-lbl collapsed" data-toggle="collapse" data-target="#collapseAvatarManagement" aria-expanded="true" aria-controls="collapseAvatarManagement">
                                                                    <span class="title-collapse">Avatar Management</span><i class="fa fa-caret-down"></i>
                                                                </div>
                                                                <div class="collapse" id="collapseAvatarManagement">
                                                                    <div class="collapse-wrapper">
                                                                        <div class="form-group row-fileformat">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">Allowed File Estensions</span>
                                                                            </label>
                                                                            <div class="form-flex form-fileformat-flex">
                                                                                <input type="text" name="avatar_file_extensions" value="<?php echo $cms->settings['avatar_file_extensions']; ?>" data-role="tagsinput" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row-filesize">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">File Size</span>
                                                                            </label>
                                                                            <div class="form-flex form-file-flex">
                                                                                <input type="number" name="avatar_max_size" class="form-control form-control-input" value="<?php echo $cms->settings['avatar_max_size']; ?>">
                                                                                <span class="postlbl-hours">maximum size of file(s) in megabytes (MB)</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-collapse-row row-collapse-taskmanagement">
                                                                <div class="collapse-title-lbl collapsed" data-toggle="collapse" data-target="#collapseTaskManagement" aria-expanded="true" aria-controls="collapseTaskManagement">
                                                                    <span class="title-collapse">Task Management</span><i class="fa fa-caret-down"></i>
                                                                </div>
                                                                <div class="collapse" id="collapseTaskManagement">
                                                                    <div class="collapse-wrapper">
                                                                        <div class="form-group row-cvresume">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">Task Material</span>
                                                                            </label>
                                                                            <div class="form-flex form-file-flex">
                                                                                <input type="number" name="task_max_file" class="form-control form-control-input" value="<?php echo $cms->settings['task_max_file']; ?>">
                                                                                <span class="postlbl-hours">maximum upload number of file(s)</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row-fileformat">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">Allowed File Extensions</span>
                                                                            </label>
                                                                            <div class="form-flex form-fileformat-flex">
                                                                                <input type="text" name="task_file_extensions" value="<?php echo $cms->settings['task_file_extensions']; ?>" data-role="tagsinput" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row-filesize">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">File Size</span>
                                                                            </label>
                                                                            <div class="form-flex form-file-flex">
                                                                                <input type="number" name="task_max_size" class="form-control form-control-input" value="<?php echo $cms->settings['task_max_size']; ?>">
                                                                                <span class="postlbl-hours">maximum size of file(s) in megabytes (MB)</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-collapse-row row-collapse-chatmanagement">
                                                                <div class="collapse-title-lbl collapsed" data-toggle="collapse" data-target="#collapseChatManagement" aria-expanded="true" aria-controls="collapseChatManagement">
                                                                    <span class="title-collapse">Chat Management</span><i class="fa fa-caret-down"></i>
                                                                </div>
                                                                <div class="collapse" id="collapseChatManagement">
                                                                    <div class="collapse-wrapper">
                                                                        <div class="form-group row-cvresume">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">Chat Material</span>
                                                                            </label>
                                                                            <div class="form-flex form-file-flex">
                                                                                <input type="text" name="chat_max_file" class="form-control form-control-input" value="<?php echo $cms->settings['chat_max_file']; ?>">
                                                                                <span class="postlbl-hours">maximum upload number of file(s)</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row-fileformat">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">Allowed File Extensions</span>
                                                                            </label>
                                                                            <div class="form-flex form-fileformat-flex">
                                                                                <input type="text" name="chat_file_extensions" value="<?php echo $cms->settings['chat_file_extensions']; ?>" data-role="tagsinput" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row-filesize">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">File Size</span>
                                                                            </label>
                                                                            <div class="form-flex form-file-flex">
                                                                                <input type="text" name="chat_max_size" class="form-control form-control-input" value="<?php echo $cms->settings['chat_max_size']; ?>">
                                                                                <span class="postlbl-hours">maximum size of file(s) in megabytes (MB)</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-collapse-row row-collapse-sessionmanagement">
                                                                <div class="collapse-title-lbl collapsed" data-toggle="collapse" data-target="#collapseSessionManagement" aria-expanded="true" aria-controls="collapseSessionManagement">
                                                                    <span class="title-collapse">Session Management</span><i class="fa fa-caret-down"></i>
                                                                </div>
                                                                <div class="collapse" id="collapseSessionManagement">
                                                                    <div class="collapse-wrapper">
                                                                        <div class="form-group form-row-session-timeout">
                                                                            <label class="input-lbl input-lbl-block">
                                                                                <span class="input-label-txt">Session Timeout</span>
                                                                            </label>
                                                                            <div class="form-radio-group">
                                                                                <div class="custom-control custom-radio">
                                                                                    <input type="radio" class="custom-control-input" name="session_timeout" id="radio_session_1" value="0"<?php echo !$cms->settings['session_timeout'] ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label" for="radio_session_1">Never</label>
                                                                                </div>
                                                                                <div class="custom-control custom-radio custom-radio-timeout">
                                                                                    <input type="radio" class="custom-control-input" name="session_timeout" id="radio_session_2" value="custom"<?php echo $cms->settings['session_timeout'] ? ' checked' : ''; ?>>
                                                                                    <label class="custom-control-label" for="radio_session_2">
                                                                                        <span class="prelbl-hours">Log out after</span>
                                                                                        <input type="number" name="session_timeout_custom" class="form-control form-control-input" value="<?php echo $cms->settings['session_timeout_custom']; ?>">
                                                                                        <span class="postlbl-hours">minutes</span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="footer-form-action">
                                                                <button type="submit" class="btn-icon-full btn-confirm" data-orientation="next">
                                                                    <span class="btn-label">Save</span>
                                                                    <span class="btn-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                            <polyline points="20 6 9 17 4 12"></polyline>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                                <button type="button" class="btn-icon-full btn-cancel back" data-dismiss="modal">
                                                                    <span class="btn-label">Cancel</span>
                                                                    <span class="btn-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
                                                                            <line x1="18" y1="6" x2="6" y2="18"></line>
                                                                            <line x1="6" y1="6" x2="18" y2="18"></line>
                                                                        </svg>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<script>
$(function(){
    $(document).on('change', '[name="date_format_custom"]', function(){
        if( $(this).val().length ){
            $('[name="date_format"]').prop('checked', false);
            $('[name="date_format"][value="custom"]').prop('checked', true);
        }
    });

    $(document).on('change', '[name="date_format"]', function(){
        if( $('[name="date_format"]:checked').val() != 'custom' ){
            $('[name="date_format_custom"]').val('');
        }
    });

    $(document).on('change', '[name="time_format_custom"]', function(){
        if( $(this).val().length ){
            $('[name="time_format"]').prop('checked', false);
            $('[name="time_format"][value="custom"]').prop('checked', true);
        }
    });

    $(document).on('change', '[name="time_format"]', function(){
        if( $('[name="time_format"]:checked').val() != 'custom' ){
            $('[name="time_format_custom"]').val('');
        }
    });

    $(document).on('change', '[name="session_timeout_custom"]', function(){
        if( $(this).val() > 0){
            $('[name="session_timeout"]').prop('checked', false);
            $('[name="session_timeout"][value="custom"]').prop('checked', true);
        }
    });

    $(document).on('change', '[name="session_timeout"]', function(){
        if( $('[name="session_timeout"]:checked').val() == '0' ){
            $('[name="session_timeout_custom"]').val('');
        }
    });
});
</script>