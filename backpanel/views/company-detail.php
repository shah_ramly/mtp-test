<div class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-chart">
				<div class="card-header">
					<div class="row">
						<div class="col-sm-12 col-header-filter">
							<h2 class="card-title">Company Management</h2>
							<button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
							        data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
								<svg class="bi bi-three-dots-vertical" viewBox="0 0 16 16" fill="currentColor"
								     xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd"
									      d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
								</svg>
							</button>
							<ul class="col-account-header">
								<li class="dropdown col-right-flex col-account">
									<button type="button" class="btn btn-success btn-block btn-logout"
									        onclick="location.href='<?php echo url_for('/logout') ?>';">
										<span class="plus-icon-lbl">Log Out</span>
									</button>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card-body-backend card-body-backend-company">
					<div class="row">
						<div class="col-xl-12">
							<div class="row">
								<div class="col-xl-12 col-backend-top">
									<div class="table-listing-header-container">
										<div class="table-listing-header-title">Company Detail</div>
										<div class="table-listing-header-actions">
											<!--<button type="button" class="btn-icon-full btn-add" data-toggle="modal" data-target="#modal_add_category">
																							<span class="btn-label">Add Category</span>
																							<span class="btn-icon">
																								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
																									<line x1="12" y1="5" x2="12" y2="19"></line>
																									<line x1="5" y1="12" x2="19" y2="12"></line>
																								</svg>
																							</span>
																						</button>-->
										</div>
									</div>
									<div class="col-backend-top-wrapper">
										<div class="row">
											<div class="col-xl-12 col-myprofile">
												<form id="selfForm" class="form-companyprofile">
													<div class="form-collapse-row row-collapse-pic">
														<div class="collapse-title-lbl collapsed" data-toggle="collapse" data-target="#collapsePersonIncharge" aria-expanded="true" aria-controls="collapsePersonIncharge"><span class="title-collapse">Person in Charge</span><i class="fa fa-caret-down"></i></div>
														<div class="collapse" id="collapsePersonIncharge">
															<div class="collapse-wrapper">
																<div class="form-flex">
																	<div class="input-group-flex row-cpfirstname">
																		<input type="text" id="firstName" class="form-control form-control-input" placeholder="First Name" autofocus="" name="firstname" onkeypress="return blockSpecialChar(event)" value="<?php echo $key['firstname']; ?>" maxlength="80">
																	</div>
																	<div class="input-group-flex row-cplastname">
																		<input type="text" id="lastName" class="form-control form-control-input" placeholder="Last Name" name="lastname" onkeypress="return blockSpecialChar(event)" value="<?php echo $key['lastname']; ?>" maxlength="80">
																	</div>
																</div>
																<div class="form-flex">
																	<div class="input-group-flex row-cpid">
																		<input type="text" id="identification" class="form-control form-control-input" placeholder="MyKad/Passport/Other ID" name="nric" value="<?php echo $key['nric']; ?>">
																	</div>
																</div>
																<div class="form-flex">
																	<div class="input-group-flex row-cpmobile">
																		<span class="tel-prefix">
																			<input type="tel" id="contactMobile" class="form-control form-control-input intl-tel-input" placeholder="Contact No. Fixed Line" name="mobile_number" value="<?php echo $key['mobile_number']; ?>" maxlength="20">
																		</span>
																	</div>
																	<div class="input-group-flex row-cpphone">
																		<span class="tel-prefix">
																			<input type="tel" id="contactPhone" class="form-control form-control-input intl-tel-input" placeholder="Contact No. Mobile" name="contact_number" value="<?php echo $key['contact_number']; ?>" maxlength="20">
																		</span>
																	</div>
																</div>
																<div class="form-flex">
																	<div class="input-group-flex row-cpemail">
																		<input type="email" id="inputEmail" class="form-control form-control-input" placeholder="Email" name="email" value="<?php echo $key['email']; ?>" readonly maxlength="100">
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-collapse-row row-collapse-compdetails">
														<div class="collapse-title-lbl collapsed" data-toggle="collapse" data-target="#collapseCompanyDetails" aria-expanded="true" aria-controls="collapseCompanyDetails"><span class="title-collapse">Company Details</span><i class="fa fa-caret-down"></i></div>
														<div class="collapse" id="collapseCompanyDetails">
															<div class="collapse-wrapper">
																<div class="form-flex">
																	<div class="input-group-flex row-cpcompname">
																		<input type="text" id="companyName" class="form-control form-control-input" placeholder="Company Name" name="company_name" value="<?php echo $company['name']; ?>" maxlength="300">
																	</div>
																	<div class="input-group-flex row-cpcompreg">
																		<input type="text" id="companyReg" class="form-control form-control-input" placeholder="Company Registration" name="reg_num" value="<?php echo $company['reg_num']; ?>" maxlength="20">
																	</div>
																	<div class="input-group-flex row-cpcompaddress">
																		<input type="text" id="companyAddress" class="form-control form-control-input" placeholder="Address" name="address1" value="<?php echo $key['address1']; ?>" maxlength="255">
																		<input type="hidden" id="companyAddress" class="form-control form-control-input" placeholder="Address 2" name="address2" value="<?php echo $key['address2']; ?>">
																	</div>
																	<div class="input-group-flex row-cpcomppostal">
																		<input type="text" id="companyPostal" class="form-control form-control-input" placeholder="Postal Code" name="zip" value="<?php echo $key['zip']; ?>" maxlength="10">
																	</div>
																	<div class="input-group-flex row-cpcomppostal">
																		<select class="form-control form-control-input city"  name="city" placeholder="City" data-selected="<?php echo $key['city']; ?>">
																			<option disabled selected>Town/City</option>
																			<?php foreach($cms->cities($key['state']) as $city){ ?>
																			<option value="<?php echo $city['id']; ?>" <?php echo $key['city'] == $city['id'] ? "selected" : ""; ?>><?php echo $city['name']; ?></option>
																			<?php } ?>
																		</select>
																	</div>
																	<div class="input-group-flex row-cpcompstate">
																		<select class="form-control form-control-input state" placeholder="State" name="state" data-target="city" data-selected="<?php echo $key['state']; ?>">
																			<option disabled selected>State</option>
																			<?php foreach($cms->states($key['country']) as $state){ ?>
																				<option value="<?php echo $state['id']; ?>" <?php echo $key['state'] == $state['id'] ? "selected" : ""; ?>><?php echo $state['name']; ?></option>
																			<?php } ?>
																		</select>
																	</div>
																	<div class="input-group-flex row-cpcompcountry">
																		<select class="form-control form-control-input country" placeholder="Country" name="country" data-target="state">
																			<option disabled selected>Country</option>
																			<?php foreach ($countries as $country) { ?>
																				<option value="<?php echo $country['id']; ?>"<?php echo $key['country'] == $country['id'] ? " selected" : ""; ?>><?php echo $country['name']; ?></option>
																			<?php } ?>
																		</select>
																	</div>
																	<div class="input-group-flex row-cpcompemail">
																		<select name="nationality" class="form-control form-control-input" placeholder="Nationality">
																			<option disabled selected>Nationality</option>
																			<option value="malaysian"<?php echo $key['nationality'] == 'malaysian' ? ' selected' : ''; ?>>Malaysian</option>
																			<option value="others"<?php echo $key['nationality'] == 'others' ? ' selected' : ''; ?>>Others</option>
																		</select>
																	</div>
																	<div class="input-group-flex row-cpcompemail">
																		<input type="email" id="companyEmail" class="form-control form-control-input" placeholder="Company Email" autofocus="" name="company_email" value="<?php echo $company['email']; ?>">
																	</div>
																	<div class="input-group-flex row-cpcompphone">
																		<span class="tel-prefix">
																			<input type="tel" id="companyPhone" class="form-control form-control-input intl-tel-input" placeholder="Company Contact No. Fixed Line" name="company_phone" value="<?php echo $company['company_phone']; ?>">
																		</span>
																	</div>
																	<div class="input-group-flex row-cpcompindustry">
																		<select class="form-control form-control-input" name="industry" placeholder="Industry">
																			<option disabled selected>Industry</option>
																			<?php foreach($cms->getIndustries2Level() as $industry){ if($industry['subs']){ ?>
																			<optgroup label="<?php echo $industry['title']; ?>">
																			<?php foreach($industry['subs'] as $sub){ ?>
																			<option value="<?php echo $sub['id']; ?>"<?php echo $sub['id'] == $company['industry'] ? ' selected' : ''; ?>><?php echo $sub['title']; ?></option>
																			<?php } ?>
																			</optgroup>
																			<?php }} ?>
																		</select>
																	</div>
																</div>

																<div class="form-collapse-row row-collapse-profilephoto">
																	<div class="collapse-title-lbl"><span class="title-collapse">Company Profile Photo</span>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-block row-indavatar profile_photo">
                                                                            <?php if($key['photo']): ?>
	                                                                            <img src="<?php echo imgCrop($key['photo'], 100, 100); ?>" class="profile-img"><br>
																			<?php endif ?>
																			<button type="button" class="btn btn-default btn-sm mt-1" onClick="$(this).next().click();"><span class="fa fa-upload"></span> Upload</button>
																			<input type="file" name="file" id="profileImage" class="d-none">
																			<input type="hidden" name="photo" value="<?php echo $key['photo']; ?>">
																		</div>
																	</div>
																</div>

																<div class="form-collapse-row row-collapse-profilephoto">
																	<div class="collapse-title-lbl"><span class="title-collapse">Company Image Gallery</span>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-block row-cpcompgallery">
																			<div class="uploads">
																				<ul class="image-list company_gallery">
																					<?php if($company['photos']){ foreach($company['photos'] as $photo){ ?>
																					<li><input type="hidden" name="photos[]" value="<?php echo $photo; ?>"><img src="<?php echo imgCrop($photo, 125, 100); ?>"><a href="#" class="remove-image"><i class="fa fa-times"></i></a></li>
																					<?php }} ?>
																				</ul>
																				<button type="button" class="btn btn-default btn-sm mt-1" onClick="$(this).next().click();"><span class="fa fa-upload"></span> Upload</button>
																				<input type="file" name="file" id="profile-company_gallery" class="d-none" multiple>
																			</div>
																		</div>
																	</div>
																</div>

																<div class="form-collapse-row row-collapse-compstatement">
																	<div class="collapse-title-lbl mb-3"><span class="title-collapse">Company Statement</span></div>
																	<div class="form-flex">
																		<div class="input-group-flex row-cpcompabout">
																			<textarea class="form-control form-control-input" placeholder="About the Company" rows="3" name="about_us" maxlength="500"><?php echo $company['about_us']; ?> </textarea>
																		</div>
																	</div>
																</div>

																<div class="form-collapse-row row-collapse-compstatement mt-4">
																	<div class="collapse-title-lbl mb-3"><span class="title-collapse">Bank information</span></div>
																	<div class="form-flex">
																		<div class="input-group-block row-bankacc">
																			<select class="form-control form-control-input" name="preference[bank]">
																				<option disabled selected><?php echo lang('pref_select_bank') ?></option>
																				<option value="Maybank" <?php echo $preference['bank'] == "Maybank" ? ' selected' : ''; ?>>Maybank</option>
																				<option value="RHB" <?php echo $preference['bank'] == "RHB" ? " selected" : ''; ?>>RHB</option>
																				<option value="CIMB" <?php echo $preference['bank'] == "CIMB" ? " selected" : ''; ?>>CIMB</option>
																				<option value="Bank Islam" <?php echo $preference['bank'] == "Bank Islam" ? " selected" : ''; ?>>Bank Islam</option>
																				<option value="Hong Leong" <?php echo $preference['bank'] == "Hong Leong" ? " selected" : ''; ?>>Hong Leong</option>
																				<option value="Public Bank" <?php echo $preference['bank'] == "Public Bank" ? " selected" : ''; ?>>Public Bank</option>
																				<option value="Bank Rakyat" <?php echo $preference['bank'] == "Bank Rakyat" ? " selected" : ''; ?>>Bank Rakyat</option>
																				<option value="OCBC Bank" <?php echo $preference['bank'] == "OCBC Bank" ? " selected" : ''; ?>>OCBC Bank</option>
																				<option value="Alliance Bank" <?php echo $preference['bank'] == "Alliance Bank" ? " selected" : ''; ?>>Alliance Bank</option>
																				<option value="Alliance Islamic Bank" <?php echo $preference['bank'] == "Alliance Islamic Bank" ? " selected" : ''; ?>>Alliance Islamic Bank</option>
																				<option value="AmBank" <?php echo $preference['bank'] == "AmBank" ? " selected" : ''; ?>>AmBank</option>
																				<option value="Affin Islamic Bank Berhad" <?php echo $preference['bank'] == "Affin Islamic Bank Berhad" ? " selected" : ''; ?>>Affin Islamic Bank Berhad</option>
																				<option value="Affin Bank Berhad" <?php echo $preference['bank'] == "Affin Bank Berhad" ? " selected" : ''; ?>>Affin Bank Berhad</option>
																				<option value="United Overseas Bank Berhad" <?php echo $preference['bank'] == "United Overseas Bank Berhad" ? " selected" : ''; ?>>United Overseas Bank Berhad</option>
																				<option value="Bank Simpanan Nasional Berhad" <?php echo $preference['bank'] == "Bank Simpanan Nasional Berhad" ? " selected" : ''; ?>>Bank Simpanan Nasional Berhad</option>
																				<option value="Bank Kerjasama Rakyat Malaysia" <?php echo $preference['bank'] == "Bank Kerjasama Rakyat Malaysia" ? " selected" : ''; ?>>Bank Kerjasama Rakyat Malaysia</option>
																				<option value="Bank Muamalat Berhad" <?php echo $preference['bank'] == "Bank Muamalat Berhad" ? " selected" : ''; ?>>Bank Muamalat Berhad</option>
																				<option value="Kuwait Finance House" <?php echo $preference['bank'] == "Kuwait Finance House" ? " selected" : ''; ?>>Kuwait Finance House</option>
																				<option value="Al-Rajhi Banking &amp; Investment Corp" <?php echo $preference['bank'] == "Al-Rajhi Banking & Investment Corp" ? " selected" : ''; ?>>Al-Rajhi Banking &amp; Investment Corp</option>
																				<option value="HSBC Bank Malaysia Berhad" <?php echo $preference['bank'] == "HSBC Bank Malaysia Berhad" ? " selected" : ''; ?>>HSBC Bank Malaysia Berhad</option>
																				<option value="Standard Chartered" <?php echo $preference['bank'] == "Standard Chartered" ? " selected" : ''; ?>>Standard Chartered</option>
																			</select>
																		</div>
																	</div>
																	<div class="form-flex" id="work_location">
																		<div class="input-group-flex row-accholdername">
																			<input type="text" class="form-control form-control-input"
																				placeholder="<?php echo lang('pref_account_name') ?>" name="preference[acc_name]" value="<?php echo  $preference['acc_name']; ?>" required>
																		</div>
																		<div class="input-group-flex row-accno">
																			<input type="text" class="form-control form-control-input"
																				placeholder="<?php echo lang('pref_account_num') ?>" name="preference[acc_num]" value="<?php echo  $preference['acc_num']; ?>" required>
																		</div>
																	</div>
																</div>
																
																<div class="form-flex mt-3" id="myPreferredLang">
																	<label class="input-lbl">
																		<span class="input-label-txt">Preferred Language for Site</span>
																	</label>
																	<div class="form-flex">
																		<?php foreach($cms->getLanguages() as $language){ ?>
																		<div class="custom-control custom-radio mr-3">
																			<input type="radio" class="custom-control-input" id="lang<?php echo $language['title']; ?>" name="preference[language]" value="<?php echo $language['id']; ?>"<?php echo $preference['language'] == $language['id'] ? ' checked' : ''; ?>>
																			<label class="custom-control-label" for="lang<?php echo $language['title']; ?>"><?php echo $language['title']; ?></label>
																		</div>
																		<?php } ?>
																	</div>
																</div>

																<div class="form-flex mt-4" id="mySurvey">
																	<label class="input-lbl">
																		<span class="input-label-txt">Survey</span>
																	</label>
																	<div class="form-flex">
																		<div class="custom-control custom-checkbox" id="checkSurvey">
																			<input type="checkbox" class="custom-control-input child-weekends" id="survey" name="preference[survey]" value="1"<?php echo $preference['survey'] ? ' checked' : ''; ?>>
																			<label class="custom-control-label" for="survey">I would like to be part of surveys</label>
																		</div>
																	</div>
																</div>

															</div>
														</div>
													</div>
													<div class="form-collapse-row row-collapse-activity">
														<div class="collapse-title-lbl collapsed" data-toggle="collapse" data-target="#collapseActivity" aria-expanded="true" aria-controls="collapseActivity"><span class="title-collapse">Activity Log</span><i class="fa fa-caret-down"></i></div>
														<div class="collapse" id="collapseActivity">
															<div class="collapse-wrapper">
                                                                <?php echo partial('partial/activity-log.php') ?>
															</div>
														</div>
													</div>
													<div class="form-collapse-row row-collapse-biling-individual">
														<div class="collapse-title-lbl collapsed" data-toggle="collapse" data-target="#collapseBiling" aria-expanded="true" aria-controls="collapseActivity"><span class="title-collapse">Billing Details</span><i class="fa fa-caret-down"></i></div>
														<div class="collapse" id="collapseBiling">
															<iframe src="<?php echo url_for('/analytics/' . $key['id']); ?>" frameborder="0" scrolling="no" onload="resizeIframe(this)" style="width:100%;height:500px;border:none;"></iframe>
														</div>
													</div>
													<div class="form-collapse-row row-collapse-account-settings">
														<div class="collapse-title-lbl collapsed" data-toggle="collapse" data-target="#collapseAccountSetting" aria-expanded="true" aria-controls="collapseAccountSetting"><span class="title-collapse">Account Settings</span><i class="fa fa-caret-down"></i></div>
														<div class="collapse" id="collapseAccountSetting">
															<div class="collapse-wrapper">
																<div class="form-collapse-row row-collapse-resetpassword">
																	<div class="collapse-title-lbl"><span class="title-collapse">Reset Password</span></div>
																	<div class="form-flex">
																		<div class="input-group-flex row-indnewpassword">
																			<input type="password" id="newPassword" class="form-control form-control-input" placeholder="New Password">
																		</div>
																		<div class="input-group-flex row-indconfirmpassword">
																			<input type="password" id="confirmPassword" class="form-control form-control-input" placeholder="Confirm Password">
																		</div>
																	</div>
																</div>

																<div class="form-collapse-row row-collapse-acc-suspension">
																	<div class="collapse-title-lbl"><span
																				class="title-collapse">Account Suspension Settings</span>
																	</div>
																	<div class="btn-switch-container">
																		<label class="btn-switch btn-color-mode-switch">
																			<input type="hidden" name="status" value="1" />
																			<input type="checkbox" name="status" id="acc_susp" value="99"<?php echo (int) $key['status'] === 99 ? ' checked' : ''; ?>>
																			<label for="acc_susp" data-on="Yes" data-off="No" class="btn-color-mode-switch-inner"></label>
																		</label>
																	</div>
																</div>

																<div class="form-group form-acc-suspension"style="display: none;">
																	<div class="collapse-title-lbl"><span class="title-collapse">Suspension Expires</span> </div>
																	<div class="form-flex">
																		<div class="input-group-flex row-suspension-expire">
																			<div class="form-group">
																				<div class="input-group input-group-datetimepicker date" id="form_datesuspension" data-target-input="nearest" data-date-format="YYYY-MM-DD">
																					<input type="text" class="form-control datetimepicker-input" data-target="#form_datesuspension" placeholder="Date" name="status_date" value="<?php echo $key['status_date'] != '0000-00-00' ? $key['status_date'] : ''; ?>">
																					<span class="input-group-addon" data-target="#form_datesuspension" data-toggle="datetimepicker">
																						<span class="fa fa-calendar"></span>
                                                                                    </span>
																				</div>
																			</div>
																			<span class="text-seperator">OR</span>
																			<div class="form-group">
																				<div class="custom-control custom-checkbox">
																					<input type="checkbox" class="custom-control-input" id="accSuspNoDate" value="1"<?php echo $key['status_date'] == '0000-00-00' ? ' checked' : ''; ?>>
																					<label class="custom-control-label" for="accSuspNoDate">Not specified</label>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="form-flex">
																		<div class="input-group-flex row-acc-suspension-notify">
																			<div class="custom-control custom-checkbox">
																				<input type="checkbox" class="custom-control-input" id="accSuspNotify" name="status_notify" value="1"<?php echo $key['status_notify'] ? ' checked' : ''; ?>>
																				<label class="custom-control-label" for="accSuspNotify">Notify user</label>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="footer-form-action">
														<button type="submit" class="btn-icon-full btn-confirm">
															<span class="btn-label">Save</span>
															<span class="btn-icon">
																<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
																	<polyline points="20 6 9 17 4 12"></polyline>
																</svg>
															</span>
														</button>
														<button type="button" class="btn-icon-full btn-cancel back">
															<span class="btn-label">Cancel</span>
															<span class="btn-icon">
																<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="arcs">
																	<line x1="18" y1="6" x2="6" y2="18"></line>
																	<line x1="6" y1="6" x2="18" y2="18"></line>
																</svg>
															</span>
														</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
function blockSpecialChar(event) {
	var regex = new RegExp("^[-@/ a-zA-Z]");
	var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
	if (!regex.test(key)) {
		event.preventDefault();
		return false;
	}
}
</script>

    <!-- Mobile Phone Number - IntlTelInput -->
	<link href="<?php echo url_for('../assets/css'); ?>/intlTelInput.css" rel="stylesheet" />
    <script src="<?php echo url_for('../assets/js'); ?>/plugins/intlTelInput.min.js"></script>
    <script>
	$(function(){
		$('#collapsePersonIncharge').on('shown.bs.collapse', function(){
			var inputHome = document.querySelector("#contactPhone");
			window.intlTelInput(inputHome, {
			autoHideDialCode: false,
			autoPlaceholder: "off",
			initialCountry: "my",
			preferredCountries: ['my'],
			separateDialCode: true,
			utilsScript: rootPath + "../assets/js/plugins/utils.js",
			hiddenInput: "contact_number",
			});
			
			var inputMobile = document.querySelector("#contactMobile");
			window.intlTelInput(inputMobile, {
			autoHideDialCode: false,
			autoPlaceholder: "off",
			initialCountry: "my",
			preferredCountries: ['my'],
			separateDialCode: true,
			utilsScript: rootPath + "../assets/js/plugins/utils.js",
			hiddenInput: "mobile_number",
			});
		});

		$('#collapseCompanyDetails').on('shown.bs.collapse', function(){
			var inputCompany = document.querySelector("#companyPhone");
			window.intlTelInput(inputCompany, {
			autoHideDialCode: false,
			autoPlaceholder: "off",
			initialCountry: "my",
			preferredCountries: ['my'],
			separateDialCode: true,
			utilsScript: rootPath + "../assets/js/plugins/utils.js",
			hiddenInput: "company_phone",
			});
		});
	});

	$(function(){
		setTimeout(function(){
			$('#acc_susp, #accSuspNoDate').change();
		}, 100);

		$('#collapseBiling').on('show.bs.collapse', function () {
			$('iframe').attr( 'src', function ( i, val ) { return val; });
		});

		$(document).on('click', '.remove-image', function(e){
			e.preventDefault();
			el = $(this);
			el.parent().slideUp(function(){
				el.parent().remove();
			});
		});
	});

	function resizeIframe(obj) {
		obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';
	}
    </script>
    <!-- Mobile Phone Number - IntTelInput -->