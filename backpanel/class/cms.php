<?php
require_once __DIR__ . '/../../lib/class/Task.php';
require_once __DIR__ . '/../../lib/class/TaskCentre.php';
require_once __DIR__ . '/../../lib/class/RatingCentre.php';
require_once __DIR__ . '/../../lib/class/TaskActivity.php';

class cms{
	public $db;
		
	public function __construct($db){
		$this->db = $db;
		$this->settings = $this->settings();
		$this->logged = !empty($_SESSION[BACKEND_PREFIX.'ADMIN_ID']) ? true : false;
		$this->no_image = 'assets/img/no_image.jpg';
		$this->shipping_time = $this->shipping_time();
		$this->defaultLimit = 12;
		$this->admin = $this->adminInfo();
	}

	public function request($field){
		$val = !empty($_REQUEST[$field]) ? $_REQUEST[$field] : (!empty($_REQUEST[$field]) && $_REQUEST[$field] == '0' ? '0' : false);
		return $val;
	}
	
	public function settings(){
		$setting = array();
		$settings = array();
		
		$this->db->query("SELECT * FROM settings");	
		$settings = $this->db->getRowList();
		
		foreach($settings as $keys){
			$setting[$keys['name']] = $keys['value'];
		}

		if($setting['date_format'] == 'custom') $setting['date_format'] = $setting['date_format_custom'];
		if($setting['time_format'] == 'custom') $setting['time_format'] = $setting['time_format_custom'];
		
		return $setting;
	}

	public function adminInfo(){
		$id = !empty($_SESSION[BACKEND_PREFIX.'ADMIN_ID']) ? $_SESSION[BACKEND_PREFIX.'ADMIN_ID']: 0;
		$this->db->query("SELECT * FROM user_admins WHERE admin_id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();

		if($key){
			$permissions = $this->db->getValue("SELECT permissions FROM user_admin_role WHERE id = " . $this->db->escape($key['admin_role']));
			$key['permissions'] = $permissions ? explode(',', $permissions) : array();

			if($key['admin_id'] == '1'){
				$key['permissions'] = array_keys(roleList());
			}
		}

		return $key;
	}
	
	public function price($price){
		$currency = 'RM ';
		return $currency . '' . number_format($price, 2);
	}
		
	public function countries(){
		$this->db->query("SELECT `id`, `name` FROM countries WHERE status = '1' ORDER BY name");
		return $this->db->getRowList();
	}

    public function states($country = 132){
        $this->db->query("SELECT * FROM states WHERE status = '1' AND country_id = " . $this->db->escape($country));
        return $this->db->getRowList();
    }

    public function cities($state){
        $this->db->query("SELECT * FROM cities WHERE status = '1' AND state_id = " . $this->db->escape($state));
        return $this->db->getRowList();
    }
	
	public function country($id){
		$this->db->query("SELECT * FROM country WHERE id = " . $this->db->escape($id));	
		return $this->db->getSingleRow();
	}

    public function getCountryName($id){
        return $this->db->getValue("SELECT `name` FROM `countries` WHERE `id` = " . $this->db->escape($id));
    }

	public function getState($country){
		$states = $this->state($country);
		$stateList = '';
		
		if($states){
			foreach($states as $keys){
				$stateList .= '<option value="' . $keys['id'] . '">' . $keys['name'] . '</option>';	
			}
		}
		
		$return['states'] = $stateList;
		$return['error'] = false;
		$return['msg'] = "Request success."; 
		
		return json($return);
	}

    public function getStateName($id){
        $this->db->query("SELECT `name` FROM `states` WHERE `id` = " . $this->db->escape($id));
        $state = $this->db->getSingleRow();
        return !empty($state['name']) ? $state['name'] : '';
    }

    public function getIndustryName($id = 0){
        return $this->db->getValue("SELECT `title` FROM `industry` WHERE `status` = '1' AND `id` = " .  $this->db->escape($id));
    }

    public function getIndustries2Level(){
        $this->db->query("SELECT * FROM `industry` WHERE `status` = '1' AND `parent` = '0'");
        $keys = $this->db->getRowList();

        if($keys){
            foreach($keys as $i => $key){
                $this->db->query("SELECT * FROM industry WHERE status = '1' AND `parent` = " . $this->db->escape($key['id']));
                $keys[$i]['subs'] = $this->db->getRowList();
            }
        }

        return $keys;
    }

    public function getIndustriesByParent($parent_id = 0){
        $this->db->query("SELECT * FROM `industry` WHERE `status` = '1' AND `parent` = " . $this->db->escape($parent_id));
        return $this->db->getRowList();
	}
	
	public function getInterestName($id = 0){
        return $this->db->getValue("SELECT `title` FROM `interest` WHERE `status` = '1' AND `id` = " .  $this->db->escape($id));
    }

    public function getInterests2Level(){
        $this->db->query("SELECT * FROM `interest` WHERE `status` = '1' AND `parent` = '0'");
        $keys = $this->db->getRowList();

        if($keys){
            foreach($keys as $i => $key){
                $this->db->query("SELECT * FROM interest WHERE status = '1' AND `parent` = " . $this->db->escape($key['id']));
                $keys[$i]['subs'] = $this->db->getRowList();
            }
        }

        return $keys;
    }

    public function getInterestByParent($parent_id = 0){
        $this->db->query("SELECT * FROM `interest` WHERE `status` = '1' AND `parent` = " . $this->db->escape($parent_id));
        return $this->db->getRowList();
    }
	
	public function login(){
	    if( $this->logged ) redirect_to('/analytics-report');
		return render('login', 'layout/login.php');
	}
	
	public function logout(){
		session_destroy();
		return '<meta http-equiv="refresh" content="0; URL=' . url_for('/') . '">';
	}

	public function home(){
		$url = '';
		$fpermission = $this->admin['permissions'][0];
		if($fpermission == 'analytics_report') $url = url_for('/analytics-report');
		if($fpermission == 'individual') $url = url_for('/individual');
		if($fpermission == 'company') $url = url_for('/company');
		if(preg_match('/task_/', $fpermission)) $url = url_for('/task');
		if(preg_match('/job_/', $fpermission)) $url = url_for('/job');
		if(preg_match('/master_data_country/', $fpermission)) $url = url_for('/country');
		if(preg_match('/master_data_state/', $fpermission)) $url = url_for('/state');
		if(preg_match('/master_data_city/', $fpermission)) $url = url_for('/city');
		if(preg_match('/master_data_language/', $fpermission)) $url = url_for('/language');
		if(preg_match('/master_data_industry_/', $fpermission)) $url = url_for('/industry');
		if(preg_match('/master_data_category_/', $fpermission)) $url = url_for('/category');
		if(preg_match('/master_data_skill_/', $fpermission)) $url = url_for('/skill');
		if(preg_match('/master_data_interest_/', $fpermission)) $url = url_for('/interest');
		if($fpermission == 'subscription') $url = url_for('/subscription');
		if(preg_match('/master_config_admin_/', $fpermission)) $url = url_for('/admin');
		if($fpermission == 'master_config_system') $url = url_for('/system-settings');
		if($fpermission == 'master_config_notification') $url = url_for('/notifications');

		if($url){
			header('Location: ' . $url);
			exit();
		}
	}
			
	public function analytics(){
		$interval = $this->request('interval') ? $this->request('interval') : '';
		if(!in_array($interval, ['day', 'month', 'year'])) $interval = 'day';
		$day = $this->request('day') ? str_pad($this->request('day'), 2, '0', STR_PAD_LEFT) : date('d');
		$month = $this->request('month') ? str_pad($this->request('month'), 2, '0', STR_PAD_LEFT) : date('m');
		$year = $this->request('year') ? $this->request('year') : date('Y');

		$param = array();
		if($this->request('interval')) $param['interval'] = $interval;
		$param['day'] = $day;
		$param['month'] = $month;
		$param['year'] = $year;
		$paramUri = http_build_query($param);

		$total = array();
		$labelList = array();
		$valueList = array();
		$status = array();

		//$statuses['earning'] = "'in-progress','completed','mark-completed','disputed'";
		$statuses['task_posted'] = "'published','in-progress','completed','mark-completed','disputed'";
		$statuses['task_completed'] = "'completed','mark-completed','disputed'";

		if($interval == 'day'){
			$date = $year . '-' . $month . '-' . $day;
			$total['earning_last'] = $this->db->getValue("SELECT SUM(service_charge) FROM task_payment WHERE status = '1' AND DATE(date) = '" . date('Y-m-d', strtotime($date . ' -1 day')) . "'") ?? 0;
			$total['earning'] = $this->db->getValue("SELECT SUM(service_charge) FROM task_payment WHERE status = '1' AND DATE(date) = '" . $date . "'") ?? 0;

			if( $total['earning'] > 0 && $total['earning_last'] > 0 ){
				$percent = round(($total['earning'] / $total['earning_last']) * 100, 2);		
				$percent = $percent < 100 ? $percent - 100 : $percent;
			}else{
				$percent = $total['earning'] > $total['earning_last'] ? 100 : 0;
			}
			$total['percent'] = ($percent >= 0 ? '+' : '')  . $percent . '%';

			$total['task_posted'] = $this->db->getValue("SELECT COUNT(*) FROM tasks WHERE status IN (" . $statuses['task_posted'] . ") AND DATE(created_at) = '" . $date . "'") ?? 0;
			$total['task_completed'] = $this->db->getValue("SELECT COUNT(*) FROM tasks WHERE status IN (" . $statuses['task_completed'] . ") AND DATE(updated_at) = '" . $date . "'") ?? 0;
			$total['user_registered'] = $this->db->getValue("SELECT COUNT(*) FROM members WHERE type = '0' AND DATE(date_created) = '" . $date . "'") ?? 0;
			$total['company_registered'] = $this->db->getValue("SELECT COUNT(*) FROM members WHERE type = '1' AND DATE(date_created) = '" . $date . "'") ?? 0;

			$arrayVal = array();
			foreach(range(0, 23) as $hour){
				$hour = str_pad($hour, 2, '0', STR_PAD_LEFT);
				$labelList[] = date('ga', strtotime('0000-00-00 ' . $hour . ':00:00'));
				$arrayVal['earning'][] = $this->db->getValue("SELECT SUM(service_charge) FROM task_payment WHERE status = '1' AND `date` LIKE '" . $date . " " . $hour . "%'") ?? 0;
				$arrayVal['task_posted'][] = $this->db->getValue("SELECT COUNT(*) FROM tasks WHERE status IN (" . $statuses['task_posted'] . ") AND created_at LIKE '" . $date . " " . $hour . "%'") ?? 0;
				$arrayVal['task_completed'][] = $this->db->getValue("SELECT COUNT(*) FROM tasks WHERE status IN (" . $statuses['task_completed'] . ") AND updated_at LIKE '" . $date . " " . $hour . "%'") ?? 0;
				$arrayVal['user_registered'][] = $total['user_registered'] = $this->db->getValue("SELECT COUNT(*) FROM members WHERE type = '0' AND date_created LIKE '" . $date . " " . $hour . "%'") ?? 0;
				$arrayVal['company_registered'][] = $total['company_registered'] = $this->db->getValue("SELECT COUNT(*) FROM members WHERE type = '1' AND date_created LIKE '" . $date . " " . $hour . "%'") ?? 0;
			}

			$valueList['earning'] = $arrayVal['earning'];
			$valueList['task_posted'] = $arrayVal['task_posted'];
			$valueList['task_completed'] = $arrayVal['task_completed'];
			$valueList['user_registered'] = $arrayVal['user_registered'];
			$valueList['company_registered'] = $arrayVal['company_registered'];
		}

		if($interval == 'month'){
			$date = $year . '-' . $month;
			$total['earning_last'] = $this->db->getValue("SELECT SUM(service_charge) FROM task_payment WHERE status = '1' AND DATE(date) LIKE '" . date('Y-m', strtotime($date . '-01 -1 month')) . "%'") ?? 0;
			$total['earning'] = $this->db->getValue("SELECT SUM(service_charge) FROM task_payment WHERE status = '1' AND DATE(date) LIKE '" . date('Y-m', strtotime($date)) . "%'") ?? 0;

			if( $total['earning'] > 0 && $total['earning_last'] > 0 ){
				$percent = round(($total['earning'] / $total['earning_last']) * 100, 2);		
				$percent = $percent < 100 ? $percent - 100 : $percent;
			}else{
				$percent = $total['earning'] > $total['earning_last'] ? 100 : 0;
			}
			$total['percent'] = ($percent >= 0 ? '+' : '')  . $percent . '%';

			$total['task_posted'] = $this->db->getValue("SELECT COUNT(*) FROM tasks WHERE status IN (" . $statuses['task_posted'] . ") AND DATE(created_at) LIKE '" . date('Y-m', strtotime($date)) . "%'") ?? 0;
			$total['task_completed'] = $this->db->getValue("SELECT COUNT(*) FROM tasks WHERE status IN (" . $statuses['task_completed'] . ") AND DATE(updated_at) LIKE '" . date('Y-m', strtotime($date)) . "%'") ?? 0;
			$total['user_registered'] = $this->db->getValue("SELECT COUNT(*) FROM members WHERE type = '0' AND DATE(date_created) LIKE '" . date('Y-m', strtotime($date)) . "%'") ?? 0;
			$total['company_registered'] = $this->db->getValue("SELECT COUNT(*) FROM members WHERE type = '1' AND DATE(date_created) LIKE '" . date('Y-m', strtotime($date)) . "%'") ?? 0;

			$arrayVal = array();
			foreach(range(1, date('t')) as $day){
				$day = str_pad($day, 2, '0', STR_PAD_LEFT);
				$labelList[] = $day;
				$arrayVal['earning'][] = $this->db->getValue("SELECT SUM(service_charge) FROM task_payment WHERE status = '1' AND DATE(date) LIKE '" . $date . '-' . $day . "%'") ?? 0;
				$arrayVal['task_posted'][] = $this->db->getValue("SELECT COUNT(*) FROM tasks WHERE status IN (" . $statuses['task_posted'] . ") AND created_at LIKE '" . $date . '-' . $day . "%'") ?? 0;
				$arrayVal['task_completed'][] = $this->db->getValue("SELECT COUNT(*) FROM tasks WHERE status IN (" . $statuses['task_completed'] . ") AND updated_at LIKE '" . $date . '-' . $day . "%'") ?? 0;
				$arrayVal['user_registered'][] = $this->db->getValue("SELECT COUNT(*) FROM members WHERE type = '0' AND date_created LIKE '" . $date . '-' . $day . "%'") ?? 0;
				$arrayVal['company_registered'][] = $this->db->getValue("SELECT COUNT(*) FROM members WHERE type = '1' AND date_created LIKE '" . $date . '-' . $day . "%'") ?? 0;
			}

			$valueList['earning'] = $arrayVal['earning'];
			$valueList['task_posted'] = $arrayVal['task_posted'];
			$valueList['task_completed'] = $arrayVal['task_completed'];
			$valueList['user_registered'] = $arrayVal['user_registered'];
			$valueList['company_registered'] = $arrayVal['company_registered'];
		}

		if($interval == 'year'){
			$total['earning_last'] = $this->db->getValue("SELECT SUM(service_charge) FROM task_payment WHERE status = '1' AND YEAR(date) = '" . date('Y', strtotime($year . '-01-01' . ' -1 year')) . "'") ?? 0;
			$total['earning'] = $this->db->getValue("SELECT SUM(service_charge) FROM task_payment WHERE status = '1' AND YEAR(date) = '" . $year . "'") ?? 0;

			if( $total['earning'] > 0 && $total['earning_last'] > 0 ){
				$percent = round(($total['earning'] / $total['earning_last']) * 100, 2);		
				$percent = $percent < 100 ? $percent - 100 : $percent;
			}else{
				$percent = $total['earning'] > $total['earning_last'] ? 100 : 0;
			}
			$total['percent'] = ($percent >= 0 ? '+' : '')  . $percent . '%';

			$total['task_posted'] = $this->db->getValue("SELECT COUNT(*) FROM tasks WHERE status IN (" . $statuses['task_posted'] . ") AND YEAR(created_at) = '" . $year . "'") ?? 0;
			$total['task_completed'] = $this->db->getValue("SELECT COUNT(*) FROM tasks WHERE status IN (" . $statuses['task_completed'] . ") AND YEAR(updated_at) = '" . $year . "'") ?? 0;
			$total['user_registered'] = $this->db->getValue("SELECT COUNT(*) FROM members WHERE type = '0' AND YEAR(date_created) = '" . $year . "'") ?? 0;
			$total['company_registered'] = $this->db->getValue("SELECT COUNT(*) FROM members WHERE type = '1' AND YEAR(date_created) = '" . $year . "'") ?? 0;

			$arrayVal = array();
			foreach(range(1, 12) as $month){
				$month = str_pad($month, 2, '0', STR_PAD_LEFT);
				$labelList[] = date('M', strtotime(date('Y') . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01'));
				$arrayVal['earning'][] = $this->db->getValue("SELECT SUM(service_charge) FROM task_payment WHERE status = '1' AND DATE(date) LIKE '" . $year . '-' . $month . "%'") ?? 0;
				$arrayVal['task_posted'][] = $this->db->getValue("SELECT COUNT(*) FROM tasks WHERE status IN (" . $statuses['task_posted'] . ") AND created_at LIKE '" . $year . '-' . $month . "%'") ?? 0;
				$arrayVal['task_completed'][] = $this->db->getValue("SELECT COUNT(*) FROM tasks WHERE status IN (" . $statuses['task_completed'] . ") AND updated_at LIKE '" . $year . '-' . $month . "%'") ?? 0;
				$arrayVal['user_registered'][] = $this->db->getValue("SELECT COUNT(*) FROM members WHERE type = '0' AND date_created LIKE '" . $year . '-' . $month . "%'") ?? 0;
				$arrayVal['company_registered'][] = $this->db->getValue("SELECT COUNT(*) FROM members WHERE type = '1' AND date_created LIKE '" . $year . '-' . $month . "%'") ?? 0;
			}

			$valueList['earning'] = $arrayVal['earning'];
			$valueList['task_posted'] = $arrayVal['task_posted'];
			$valueList['task_completed'] = $arrayVal['task_completed'];
			$valueList['user_registered'] = $arrayVal['user_registered'];
			$valueList['company_registered'] = $arrayVal['company_registered'];
		}

        $query = "SELECT 'organic' AS `name`, COUNT(`id`) AS `total`,
                ( SELECT count(`id`) FROM `tasks` WHERE DATE(`created_at`) <= (DATE_SUB(CURDATE(), INTERVAL 7 DAY)) ) AS `last_7_days`,
                ( SELECT count(`id`) FROM `tasks` WHERE DATE(`created_at`) <= (DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) ) AS `last_30_days`,
                ( SELECT count(`id`) FROM `tasks` WHERE DATE(`created_at`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 7 DAY)) ) AS `prev_last_7_days`,
                ( SELECT count(`id`) FROM `tasks` WHERE DATE(`created_at`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 MONTH)) ) AS `prev_last_30_days`
                FROM `tasks`
                
                UNION
                
                SELECT 'aggregated' AS `name`, COUNT(`id`) AS `total`,
                ( SELECT count(`id`) FROM `external_tasks` WHERE DATE(`created_at`) <= (DATE_SUB(CURDATE(), INTERVAL 7 DAY)) ) AS `last_7_days`,
                ( SELECT count(`id`) FROM `external_tasks` WHERE DATE(`created_at`) <= (DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) ) AS `last_30_days`,
                ( SELECT count(`id`) FROM `external_tasks` WHERE DATE(`created_at`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 7 DAY)) ) AS `prev_last_7_days`,
                ( SELECT count(`id`) FROM `external_tasks` WHERE DATE(`created_at`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 MONTH)) ) AS `prev_last_30_days`
                FROM `external_tasks`
                
                UNION

                SELECT 'organic_jobs' AS `name`, COUNT(`id`) AS `total`,
                ( SELECT count(`id`) FROM `jobs` WHERE DATE(`created_at`) <= (DATE_SUB(CURDATE(), INTERVAL 7 DAY)) ) AS `last_7_days`,
                ( SELECT count(`id`) FROM `jobs` WHERE DATE(`created_at`) <= (DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) ) AS `last_30_days`,
                ( SELECT count(`id`) FROM `jobs` WHERE DATE(`created_at`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 7 DAY)) ) AS `prev_last_7_days`,
                ( SELECT count(`id`) FROM `jobs` WHERE DATE(`created_at`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 MONTH)) ) AS `prev_last_30_days`
                FROM `jobs`
                
                UNION
                
                SELECT 'aggregated_jobs' AS `name`, COUNT(`id`) + 100000 AS `total`,
                ( SELECT count(`id`) + 100000 FROM `external_jobs` WHERE DATE(`created_at`) <= (DATE_SUB(CURDATE(), INTERVAL 7 DAY)) ) AS `last_7_days`,
                ( SELECT count(`id`) + 100000 FROM `external_jobs` WHERE DATE(`created_at`) <= (DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) ) AS `last_30_days`,
                ( SELECT count(`id`) + 100000 FROM `external_jobs` WHERE DATE(`created_at`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 7 DAY)) ) AS `prev_last_7_days`,
                ( SELECT count(`id`) + 100000 FROM `external_jobs` WHERE DATE(`created_at`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 MONTH)) ) AS `prev_last_30_days`
                FROM `external_jobs`
                
                UNION
                
                SELECT 'individual' AS `name`, COUNT(`id`) AS `total`,
                ( SELECT count(`id`) FROM `members` WHERE `type` = '0' AND DATE(`date_created`) <= (DATE_SUB(CURDATE(), INTERVAL 7 DAY)) ) AS `last_7_days`,
                ( SELECT count(`id`) FROM `members` WHERE `type` = '0' AND DATE(`date_created`) <= (DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) ) AS `last_30_days`,
                ( SELECT count(`id`) FROM `members` WHERE `type` = '0' AND DATE(`date_created`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 7 DAY)) ) AS `prev_last_7_days`,
                ( SELECT count(`id`) FROM `members` WHERE `type` = '0' AND DATE(`date_created`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 MONTH)) ) AS `prev_last_30_days`
                FROM `members`
                WHERE `type` = '0'
                
                UNION
                
                SELECT 'company' AS `name`, COUNT(`id`) AS `total`,
                ( SELECT count(`id`) FROM `members` WHERE `type` = '1' AND DATE(`date_created`) <= (DATE_SUB(CURDATE(), INTERVAL 7 DAY)) ) AS `last_7_days`,
                ( SELECT count(`id`) FROM `members` WHERE `type` = '1' AND DATE(`date_created`) <= (DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) ) AS `last_30_days`,
                ( SELECT count(`id`) FROM `members` WHERE `type` = '1' AND DATE(`date_created`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 7 DAY)) ) AS `prev_last_7_days`,
                ( SELECT count(`id`) FROM `members` WHERE `type` = '1' AND DATE(`date_created`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 MONTH)) ) AS `prev_last_30_days`
                FROM `members`
                WHERE `type` = '1'
                
                UNION
                
                SELECT 
                'collection' AS `name`,
                SUM(`total` + `service_charge`) AS `total`, 
                ( SELECT SUM(`total` + `service_charge`) FROM `task_payment` WHERE `status` = '1' AND DATE(`date`) <= (DATE_SUB(CURDATE(), INTERVAL 7 DAY)) ) AS `last_7_days`,
                ( SELECT SUM(`total` + `service_charge`) FROM `task_payment` WHERE `status` = '1' AND DATE(`date`) <= (DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) ) AS `last_30_days`, 
                ( SELECT SUM(`total` + `service_charge`) FROM `task_payment` WHERE `status` = '1' AND DATE(`date`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 7 DAY)) ) AS `prev_last_7_days`,
                ( SELECT SUM(`total` + `service_charge`) FROM `task_payment` WHERE `status` = '1' AND DATE(`date`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 MONTH)) ) AS `prev_last_30_days`
                FROM `task_payment`
                WHERE  `status` = '1'
                
                UNION
                
                SELECT 
                'cost' AS `name`,
                SUM(`total`) AS `total`, 
                ( SELECT SUM(`total`) FROM `task_payment` WHERE `status` = '1' AND DATE(`date`) <= (DATE_SUB(CURDATE(), INTERVAL 7 DAY)) ) AS `last_7_days`,
                ( SELECT SUM(`total`) FROM `task_payment` WHERE `status` = '1' AND DATE(`date`) <= (DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) ) AS `last_30_days`, 
                ( SELECT SUM(`total`) FROM `task_payment` WHERE `status` = '1' AND DATE(`date`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 7 DAY)) ) AS `prev_last_7_days`,
                ( SELECT SUM(`total`) FROM `task_payment` WHERE `status` = '1' AND DATE(`date`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 MONTH)) ) AS `prev_last_30_days`
                FROM `task_payment`
                WHERE  `status` = '1'
                
                UNION
                
                SELECT 
                'sp_cost' AS `name`,
                COUNT(`id`) * 0.5 AS `total`, 
                ( SELECT COUNT(`id`) * 0.5 FROM `task_payment` WHERE `status` = '1' AND DATE(`date`) <= (DATE_SUB(CURDATE(), INTERVAL 7 DAY)) ) AS `last_7_days`,
                ( SELECT COUNT(`id`) * 0.5 FROM `task_payment` WHERE `status` = '1' AND DATE(`date`) <= (DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) ) AS `last_30_days`, 
                ( SELECT COUNT(`id`) * 0.5 FROM `task_payment` WHERE `status` = '1' AND DATE(`date`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 7 DAY)) ) AS `prev_last_7_days`,
                ( SELECT COUNT(`id`) * 0.5 FROM `task_payment` WHERE `status` = '1' AND DATE(`date`) <= (DATE_SUB(DATE_SUB(CURDATE(), INTERVAL 1 DAY), INTERVAL 1 MONTH)) ) AS `prev_last_30_days`
                FROM `task_payment`
                WHERE  `status` = '1'";

		$this->db->query($query);
		$overall = $this->db->getRowList();
		$dashboard_data = [];

		foreach ($overall as $value){
		    $name = $value['name'];
		    unset($value['name']);
		    $dashboard_data[$name] = $value;
        }

		$dashboard_data['tasks_total'] = $dashboard_data['organic']['total']+$dashboard_data['aggregated']['total'];
		$dashboard_data['organic_percentage'] = number_format($dashboard_data['organic']['total'] / $dashboard_data['tasks_total'] * 100, 1);
		$dashboard_data['aggregated_percentage'] = number_format($dashboard_data['aggregated']['total'] / $dashboard_data['tasks_total'] * 100, 1);

		$dashboard_data['jobs_total'] = $dashboard_data['organic_jobs']['total']+$dashboard_data['aggregated_jobs']['total'];
		$dashboard_data['jobs_organic_percentage'] = ($dashboard_data['organic_jobs']['total'] / $dashboard_data['jobs_total'] * 100);
		$dashboard_data['jobs_aggregated_percentage'] = ($dashboard_data['aggregated_jobs']['total'] / $dashboard_data['jobs_total'] * 100);

		$dashboard_data['organic']['last_7_days_percentage'] = $dashboard_data['organic']['last_7_days'] <= 0 ? ($dashboard_data['organic']['total'] * 100) : ($dashboard_data['organic']['total']/$dashboard_data['organic']['last_7_days'] * 100) - 100;
		$dashboard_data['organic']['last_30_days_percentage'] = $dashboard_data['organic']['last_30_days'] <= 0 ? ($dashboard_data['organic']['total'] * 100) : ($dashboard_data['organic']['total']/$dashboard_data['organic']['last_30_days'] * 100) - 100;

		$dashboard_data['organic_jobs']['last_7_days_percentage'] = $dashboard_data['organic_jobs']['last_7_days'] <= 0 ? ($dashboard_data['organic_jobs']['total'] * 100) : ($dashboard_data['organic_jobs']['total']/$dashboard_data['organic_jobs']['last_7_days'] * 100) - 100;
		$dashboard_data['organic_jobs']['last_30_days_percentage'] = $dashboard_data['organic_jobs']['last_30_days'] <= 0 ? ($dashboard_data['organic_jobs']['total'] * 100) : ($dashboard_data['organic_jobs']['total']/$dashboard_data['organic_jobs']['last_30_days'] * 100) - 100;

		$dashboard_data['aggregated']['last_7_days_percentage'] = $dashboard_data['aggregated']['last_7_days'] <= 0 ? ($dashboard_data['aggregated']['total'] * 100) : ($dashboard_data['aggregated']['total'] / $dashboard_data['aggregated']['last_7_days'] * 100) - 100;
		$dashboard_data['aggregated']['last_30_days_percentage'] = $dashboard_data['aggregated']['last_30_days'] <= 0 ? ($dashboard_data['aggregated']['total'] * 100) : ($dashboard_data['aggregated']['total']/$dashboard_data['aggregated']['last_30_days'] * 100) - 100;

		$dashboard_data['aggregated_jobs']['last_7_days_percentage'] = $dashboard_data['aggregated_jobs']['last_7_days'] <= 0 ? ($dashboard_data['aggregated_jobs']['total'] * 100) : ($dashboard_data['aggregated_jobs']['total'] / $dashboard_data['aggregated_jobs']['last_7_days'] * 100) - 100;
		$dashboard_data['aggregated_jobs']['last_30_days_percentage'] = $dashboard_data['aggregated_jobs']['last_30_days'] <= 0 ? ($dashboard_data['aggregated_jobs']['total'] * 100) : ($dashboard_data['aggregated_jobs']['total']/$dashboard_data['aggregated_jobs']['last_30_days'] * 100) - 100;

		$dashboard_data['tasks_total_last_7_days_percentage'] = ($dashboard_data['organic']['last_7_days'] + $dashboard_data['aggregated']['last_7_days']+$dashboard_data['organic_jobs']['last_7_days'] + $dashboard_data['aggregated_jobs']['last_7_days']) <= 0 ? ($dashboard_data['tasks_total']+$dashboard_data['jobs_total'] * 100) : (($dashboard_data['tasks_total']+$dashboard_data['jobs_total'])/($dashboard_data['organic']['last_7_days'] + $dashboard_data['aggregated']['last_7_days']+$dashboard_data['organic_jobs']['last_7_days'] + $dashboard_data['aggregated_jobs']['last_7_days']) * 100) - 100;
		$dashboard_data['tasks_total_last_30_days_percentage'] = ($dashboard_data['organic']['last_30_days'] + $dashboard_data['aggregated']['last_30_days']+$dashboard_data['organic_jobs']['last_30_days'] + $dashboard_data['aggregated_jobs']['last_30_days']) <= 0 ? ($dashboard_data['tasks_total']+$dashboard_data['jobs_total'] * 100) : (($dashboard_data['tasks_total']+$dashboard_data['jobs_total'])/($dashboard_data['organic']['last_30_days'] + $dashboard_data['aggregated']['last_30_days']+$dashboard_data['organic_jobs']['last_30_days'] + $dashboard_data['aggregated_jobs']['last_30_days']) * 100) - 100;

        $dashboard_data['collection']['last_7_days_percentage'] = (float)$dashboard_data['collection']['last_7_days'] <= 0 ? ($dashboard_data['collection']['total'] * 100) : ($dashboard_data['collection']['total']/(float)$dashboard_data['collection']['last_7_days'] * 100) - 100;
        $dashboard_data['collection']['last_30_days_percentage'] = (float)$dashboard_data['collection']['last_30_days'] <= 0 ? ($dashboard_data['collection']['total'] * 100) : ($dashboard_data['collection']['total']/(float)$dashboard_data['collection']['last_30_days'] * 100) - 100;

        $dashboard_data['cost']['last_7_days_percentage'] = (float)$dashboard_data['cost']['last_7_days'] <= 0 ? ($dashboard_data['cost']['total'] * 100) : ($dashboard_data['cost']['total']/(float)$dashboard_data['cost']['last_7_days'] * 100) - 100;
        $dashboard_data['cost']['last_30_days_percentage'] = (float)$dashboard_data['cost']['last_30_days'] <= 0 ? ($dashboard_data['cost']['total'] * 100) : ($dashboard_data['cost']['total']/(float)$dashboard_data['cost']['last_30_days'] * 100) - 100;

        $dashboard_data['sp_cost']['last_7_days_percentage'] = (float)$dashboard_data['sp_cost']['last_7_days'] <= 0 ? ($dashboard_data['sp_cost']['total'] * 100) : ($dashboard_data['sp_cost']['total']/(float)$dashboard_data['sp_cost']['last_7_days'] * 100) - 100;
        $dashboard_data['sp_cost']['last_30_days_percentage'] = (float)$dashboard_data['sp_cost']['last_30_days'] <= 0 ? ($dashboard_data['sp_cost']['total'] * 100) : ($dashboard_data['sp_cost']['total']/(float)$dashboard_data['sp_cost']['last_30_days'] * 100) - 100;

        $dashboard_data['collection_total_last_7_days_percentage'] = (float)$dashboard_data['collection']['last_7_days'] <= 0 ? (($dashboard_data['collection']['total'] - (float)$dashboard_data['cost']['total'] - (float)$dashboard_data['sp_cost']['total']) * 100) : (((float)$dashboard_data['collection']['last_7_days'] - (float)$dashboard_data['sp_cost']['last_7_days'] - (float)$dashboard_data['cost']['last_7_days'])/$dashboard_data['collection']['total'] * 100);
        $dashboard_data['collection_total_last_30_days_percentage'] = (float)$dashboard_data['collection']['last_30_days'] <= 0 ? (($dashboard_data['collection']['total'] - (float)$dashboard_data['cost']['total'] - (float)$dashboard_data['sp_cost']['total']) * 100) : (((float)$dashboard_data['collection']['last_30_days'] - (float)$dashboard_data['cost']['last_30_days'] - (float)$dashboard_data['sp_cost']['last_30_days'])/$dashboard_data['collection']['total'] * 100);

        $dashboard_data['users_total'] = $dashboard_data['individual']['total']+$dashboard_data['company']['total'];
        $dashboard_data['individual_percentage'] = $dashboard_data['users_total'] <= 0 ? ($dashboard_data['individual']['total'] * 100) : ($dashboard_data['individual']['total']/$dashboard_data['users_total'] * 100);
        $dashboard_data['company_percentage'] = $dashboard_data['users_total'] <= 0 ? ($dashboard_data['company']['total'] * 100) : ($dashboard_data['company']['total']/$dashboard_data['users_total'] * 100);

        $dashboard_data['individual']['last_7_days_percentage'] = $dashboard_data['individual']['last_7_days'] <= 0 ? ($dashboard_data['individual']['total'] * 100) : ($dashboard_data['individual']['total']/$dashboard_data['individual']['last_7_days'] * 100) - 100;
        $dashboard_data['individual']['last_30_days_percentage'] = $dashboard_data['individual']['last_30_days'] <= 0 ? ($dashboard_data['individual']['total'] * 100) : ($dashboard_data['individual']['total']/$dashboard_data['individual']['last_30_days'] * 100) - 100;

		$dashboard_data['company']['last_7_days_percentage'] = $dashboard_data['company']['last_7_days'] <= 0 ? ($dashboard_data['company']['total'] * 100) : ($dashboard_data['company']['total']/$dashboard_data['company']['last_7_days'] * 100) - 100;
        $dashboard_data['company']['last_30_days_percentage'] = $dashboard_data['company']['last_30_days'] <= 0 ? ($dashboard_data['company']['total'] * 100) : ($dashboard_data['company']['total']/$dashboard_data['company']['last_30_days'] * 100) - 100;

        $dashboard_data['users_total_last_7_days_percentage'] = ($dashboard_data['individual']['last_7_days'] + $dashboard_data['company']['last_7_days']) <= 0 ? ($dashboard_data['users_total'] * 100) : ($dashboard_data['users_total']/($dashboard_data['individual']['last_7_days'] + $dashboard_data['company']['last_7_days']) * 100) - 100;
        $dashboard_data['users_total_last_30_days_percentage'] = ($dashboard_data['individual']['last_30_days'] + $dashboard_data['company']['last_30_days']) <= 0 ? ($dashboard_data['users_total'] * 100) : ($dashboard_data['users_total']/($dashboard_data['individual']['last_30_days'] + $dashboard_data['company']['last_30_days']) * 100) - 100;

        $countries_query = "SELECT c.`id`, c.`name`,
                        COALESCE(( SELECT COUNT(`id`) FROM `members` WHERE `country` = c.`id` AND `country` IS NOT NULL GROUP BY `country` ), 0) AS `total_count`,
                        COALESCE(( SELECT COUNT(`id`) FROM `members` WHERE `country` = c.`id` AND `type` = '0' AND `country` IS NOT NULL GROUP BY `country` ), 0) AS `individual_count`,
                        COALESCE(( SELECT COUNT(`id`) FROM `members` WHERE `country` = c.`id` AND `type` = '1' AND `country` IS NOT NULL GROUP BY `country` ), 0) AS `company_count`
                        
                        FROM `countries` c WHERE c.`id` IN(
                            SELECT `country` FROM `members` WHERE `country` IS NOT NULL GROUP BY `country`
                        )";

        $this->db->query($countries_query);
        $countries = $this->db->getRowList();

        $others_query = "SELECT 
                        '0' AS `id`,
                        'Others' AS `name`,
                        COUNT(m.`id`) AS `total_count`,
                        COALESCE(( SELECT COUNT(`id`) FROM `members` WHERE `type` = '0' AND `country` IS NULL GROUP BY `country` ), 0) AS `individual_count`,
                        COALESCE(( SELECT COUNT(`id`) FROM `members` WHERE `type` = '1' AND `country` IS NULL GROUP BY `country` ), 0) AS `company_count`
                        
                        FROM `members` m WHERE m.`country` IS NULL GROUP BY m.`country`";
        $this->db->query($others_query);
        $others = $this->db->getRowList();

        $states_query = "SELECT s.`id`, s.`name`, s.`country_id`,
                        COALESCE(( SELECT COUNT(`id`) FROM `members` WHERE `state` = s.`id` GROUP BY `state` ), 0) AS `total_count`,
                        COALESCE(( SELECT COUNT(`id`) FROM `members` WHERE `type` = '0' AND `state` = s.`id` GROUP BY `state` ), 0) AS `individual_count`,
                        COALESCE(( SELECT COUNT(`id`) FROM `members` WHERE `type` = '1' AND `state` = s.`id` GROUP BY `state` ), 0) AS `company_count`
                        FROM `states` s WHERE s.`id` IN(
                            SELECT `state` FROM `members` WHERE `state` IS NOT NULL GROUP BY `state`
                        )";

        $this->db->query($states_query);
        $states = $this->db->getRowList();

        $all_states = [];
        foreach ($countries as $country){
            $all_states[$country['id']] = array_values(array_filter($states, function ($state) use($country){ return $state['country_id'] === $country['id']; }));
        }

        $all_countries = array_merge($countries, $others);

        //dd($countries, $others, array_merge($countries, $others), $all_states);

		//dd($dashboard_data);

		set('interval', $interval);
		set('day', $day);
		set('month', $month);
		set('year', $year);
		set('total', $total);
		set('labelList', $labelList);
		set('valueList', $valueList);
		set('param', $param);
		set('paramUri', $paramUri);
		set('dashboard_data', $dashboard_data);
		set('countries', $all_countries);
		set('states', $all_states);
		return render('analytics-report.php', 'layout/default.php');
	}

	public function countryList(){
		$name = $this->request('name');
		$searchQry = '';
		$param = array();

		if($name){
			$searchQry .= " AND name LIKE " . $this->db->escape('%' . $name . '%');
			$param['name'] = $name;
		}

		$paramQry = $param ? http_build_query($param) : '';
		$page = $this->request('page') ? $this->request('page') : 1;
		
        $this->db->query("SELECT * FROM countries WHERE status = '1' " . $searchQry . " ORDER BY name");
		$pagination = new fpagination($this->db->getRowList(), $this->defaultLimit, $page, $paramQry);
		$keys = $pagination->result();
		
        set('param', $param);
        set('keys', $keys);
		set('pagination', $pagination);
		return render('country.php', 'layout/default.php');
	}

	public function countryListAdd(){
		return render('country-add.php', 'layout/default.php');
	}

	public function countryListEdit($id = 0){
		$this->db->query("SELECT * FROM countries WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
		
		if($key){
			set('key', $key);
			return render('country-edit.php', 'layout/default.php');
		}else{
			return not_found();
		}
	}

	public function stateList(){
		$name = $this->request('name');
		$country = $this->request('country');
		$searchQry = '';
		$param = array();

		$searchQry = '';
		$param = array();

		if($name){
			$searchQry .= " AND s.name LIKE " . $this->db->escape('%' . $name . '%');
			$param['name'] = $name;
		}

		if($country){
			$searchQry .= " AND country_id = " . $this->db->escape($country);
			$param['country'] = $country;
		}

		$paramQry = $param ? http_build_query($param) : '';
		$page = $this->request('page') ? $this->request('page') : 1;
		
        $this->db->query("SELECT s.*, c.name AS country_name FROM `states` s LEFT JOIN `countries` c ON c.id = s.country_id WHERE s.status IN ('0','1','999') " . $searchQry . " ORDER BY country_id, name");
		$pagination = new fpagination($this->db->getRowList(), $this->defaultLimit, $page, $paramQry);
		$keys = $pagination->result();
		
        set('param', $param);
        set('keys', $keys);
		set('pagination', $pagination);
		return render('state.php', 'layout/default.php');
	}

	public function stateListAdd(){
		return render('state-add.php', 'layout/default.php');
	}

	public function stateListEdit($id = 0){
		$this->db->query("SELECT * FROM states WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
		
		if($key){
			set('key', $key);
			return render('state-edit.php', 'layout/default.php');
		}else{
			return not_found();
		}
	}

	public function cityList(){
		$name = $this->request('name');
		$country = $this->request('country');
		$state = $this->request('state');
		$searchQry = '';
		$param = [];

		if($name){
			$searchQry .= " AND ct.name LIKE " . $this->db->escape('%' . $name . '%');
			$param['name'] = $name;
		}

		if($country){
			$searchQry .= " AND ct.country_id = " . $this->db->escape($country);
			$param['country'] = $country;
		}

		if($state){
			$searchQry .= " AND ct.state_id = " . $this->db->escape($state);
			$param['state'] = $state;
		}

		$paramQry = $param ? http_build_query($param) : '';
		$page = $this->request('page') ? $this->request('page') : 1;
		$offset = ($page - 1) * $this->defaultLimit;
		$totalCount = $this->db->getValue("SELECT COUNT(`id`) FROM `cities` ct WHERE `status` IN('0','1', '999') {$searchQry}");
        $this->db->query("SELECT ct.`id`, ct.`name`, ct.`state_id`, ct.`country_id`, ct.`status`, s.`name` AS `state_name`, c.`name` AS `country_name` FROM `cities` ct INNER JOIN `countries` c ON c.`id` = ct.`country_id` INNER JOIN `states` s ON ct.`state_id` = s.`id` WHERE ct.`status` IN('0','1', '999') {$searchQry} ORDER BY c.`name`, s.`name`, ct.`name` LIMIT {$offset}, {$this->defaultLimit}");
        $data = $this->db->getRowList();

        $pagination = new fpagination($data, $this->defaultLimit, $page, $paramQry, 'page', $totalCount);
		$keys = $pagination->result();
		
        set('param', $param);
        set('keys', $keys);
		set('pagination', $pagination);
		return render('city.php', 'layout/default.php');
	}

	public function cityListAdd(){
		return render('city-add.php', 'layout/default.php');
	}

	public function cityListEdit($id = 0){
		$this->db->query("SELECT * FROM cities WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
		
		if($key){
			set('key', $key);
			return render('city-edit.php', 'layout/default.php');
		}else{
			return not_found();
		}
	}

	public function getLanguages($except = array()){
		if(!empty($except)) $this->db->query("SELECT * FROM `language` WHERE status = '1' AND id NOT IN (" . implode(",", $except) . ")");
		else $this->db->query("SELECT * FROM `language` WHERE status = '1'");
		$keys = $this->db->getRowList();

		return $keys;
	}

	public function language(){
		set('keys', $this->getLanguages());
		return render('language.php', 'layout/default.php');
	}

	public function languageText($id = 0){
		$this->db->query("SELECT * FROM `language` WHERE id = " . $this->db->escape($id));
		$language = $this->db->getSingleRow();

		if($language){
			$this->db->query("SELECT * FROM `language_text` WHERE language_id = " . $this->db->escape($id));
			$keys = $this->db->getRowList();

			$_lang = lang_vars();
			foreach($keys as $key){
				if(isset($_lang[$key['var']]) && $key['text']){
					$_lang[$key['var']] = $key['text'];
				}
			}
			
			$this->db->query("SELECT * FROM `language_text` WHERE language_id = 1");
			$keys = $this->db->getRowList();
			
			$default_lang = lang_vars();
			foreach($keys as $key){
				if(isset($default_lang[$key['var']]) && $key['text']){
					$default_lang[$key['var']] = $key['text'];
				}
			}

			set('language', $language);
			set('_lang', $_lang);
			set('default_lang', $default_lang);
			return render('language-text.php', 'layout/default.php');

		}else{
			return not_found();
		}
	}

	public function getIndustries($except = array(), $param = ''){
		$titleQry = '';
		if(!empty($param['title'])) $titleQry = " AND title LIKE " . $this->db->escape('%' . $param['title'] . '%');

		if(!empty($except)) $this->db->query("SELECT * FROM industry WHERE status < 999 AND id NOT IN (" . implode(",", $except) . ") " . $titleQry  . " ORDER BY parent, sortby");
		else $this->db->query("SELECT * FROM industry WHERE status < 999 " . $titleQry  . " ORDER BY parent, sortby");
		$keys = $this->db->getRowList();
		
		if($keys){
			foreach($keys as $i => $cat){
				$main_cat = 0;
				if($cat['parent'] == '0') $main_cat = $cat['id'];
				$keys[$i]['main_cat'] = $main_cat;

				if(!empty($cat['parent'])){
					$parentID = $cat['parent'];
					
					while(!empty($parentID)){
						$this->db->query("SELECT * FROM industry WHERE id = " . $this->db->escape($parentID));
						$parent = $this->db->getSingleRow();
						$keys[$i]['parents'][] = $parent['title'];
						$parentID = $parent['parent'];
						$main_cat = $parent['id'];
					}
					
					$keys[$i]['main_cat'] = $main_cat;
					$keys[$i]['parents'] = array_reverse($keys[$i]['parents']);
					$keys[$i]['title'] = implode(' > ', $keys[$i]['parents']) . ' > ' . $keys[$i]['title'];
				}
			}
		}
		
		if(!empty($keys)) $this->sort_array_of_array($keys, 'title');
				
		return $keys;
	}

	public function industryList(){
		$title = $this->request('title');
		$type = $this->request('type');
		$searchQry = '';
		$param = array();

		if($title){
			$param['title'] = $title;
		}

		if($type){
			$param['type'] = $type;
		}

		$param['new'] = '0';
		
		$paramQry = $param ? http_build_query($param) : '';
		$page = $this->request('page') ? $this->request('page') : 1;
		
		$pagination = new fpagination($this->getIndustries('', $param), $this->defaultLimit, $page, $paramQry);
		$keys = $pagination->result();

		$this->db->query("SELECT * FROM industry WHERE status < 999 ORDER BY title");
		$allKeys = $this->db->getRowList();

		$languages = $this->getLanguages(['1']);

		if($allKeys){
			foreach($allKeys as $i => $key){
				$lang = array();
				foreach($languages as $language){
					$lang[$language['id']] = $this->db->getValue("SELECT title FROM industry_language_text WHERE industry_id = " . $this->db->escape($key['id']));
				}
				$allKeys[$i]['lang'] = $lang;
			}
		}
		
        set('param', $param);
        set('keys', $keys);
		set('pagination', $pagination);
		set('allKeys', $allKeys);
		set('languages', $languages);
		set('newKeys', $this->getCategories('', ['new' => '1']));
		return render('industry.php', 'layout/default.php');
	}

	public function industryListAdd(){
		set('industries', $this->getIndustries());
		return render('industry-add.php', 'layout/default.php');
	}

	public function industryListEdit($id = 0){
		$this->db->query("SELECT * FROM industry WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
		
		if($key){
			$available_languages = $this->getLanguages(['1']);
			$languages = array();
			foreach($available_languages as $i => $language){
				$languages[$language['id']]['title'] = $this->db->getValue("SELECT title FROM industry_language_text WHERE industry_id = " . $this->db->escape($id) . " AND language_id = " . $this->db->escape($language['id']));
			}

			$key['languages'] = $languages;

			set('key', $key);
			set('industries', $this->getIndustries([$key['id']]));
			return render('industry-edit.php', 'layout/default.php');
		}else{
			return not_found();
		}
	}

	public function getCategories($except = array(), $param = array()){
		$filterQry = array();
		if(!empty($param['title'])) $filterQry[] = " AND title LIKE " . $this->db->escape('%' . $param['title'] . '%');
		if(isset($param['new'])) $filterQry[] = " AND `new` = '" . $param['new'] . "'";
		$filterQry = $filterQry ? implode(' ', $filterQry) : '';

		if(!empty($except)) $this->db->query("SELECT * FROM task_category WHERE status < 999 AND id NOT IN (" . implode(",", $except) . ") " . $filterQry . " ORDER BY parent, sortby");
		else $this->db->query("SELECT * FROM task_category WHERE status < 999 " . $filterQry . " ORDER BY parent, sortby");
		$categories = $this->db->getRowList();
		
		if($categories){
			foreach($categories as $i => $cat){
				if(!empty($cat['parent'])){
					$parentID = $cat['parent'];
					
					while(!empty($parentID)){
						$this->db->query("SELECT * FROM task_category WHERE id = " . $this->db->escape($parentID));
						$parent = $this->db->getSingleRow();

						if(!$parent || $parentID == $parent['parent']){
							break;
						}

						$categories[$i]['parents'][] = $parent['title'];
						$parentID = $parent['parent'];
					}
					
					if(!empty($categories[$i]['parents'])){
						$categories[$i]['parents'] = array_reverse($categories[$i]['parents']);
						$categories[$i]['title'] = implode(' > ', $categories[$i]['parents']) . ' > ' . $categories[$i]['title'];
					}
				}
			}
		}
		
		if(!empty($categories)) $this->sort_array_of_array($categories, 'title');
				
		return $categories;
	}

	public function getFirstCategories($except = array(), $param = array()){
		$filterQry = array();
		if(!empty($param['title'])) $filterQry[] = " AND title LIKE " . $this->db->escape('%' . $param['title'] . '%');
		if(isset($param['new'])) $filterQry[] = " AND `new` = '" . $param['new'] . "'";
		if(empty($param['title'])) $filterQry[] = " AND parent = '0'";
		$filterQry = $filterQry ? implode(' ', $filterQry) : '';

		if(!empty($except)) $this->db->query("SELECT * FROM task_category WHERE status < 999 AND id NOT IN (" . implode(",", $except) . ") " . $filterQry . " ORDER BY title, sortby");
		else $this->db->query("SELECT * FROM task_category WHERE status < 999 " . $filterQry . " ORDER BY title, sortby");
		$categories = $this->db->getRowList();
				
		return $categories;
	}

	public function getChildCategories($category_id = 0){
		$this->db->query("SELECT * FROM task_category WHERE parent = " . $this->db->escape($category_id));
		$keys = $this->db->getRowList();

		if($keys){
			foreach($keys as $i => $key){
				$keys[$i]['subs'] = $this->getChildCategories($key['id']);
			}
		}

		return $keys;
	}

	public function categoryList(){
		$title = $this->request('title');
		$param = array();

		if($title){
			$param['title'] = $title;
		}

		$param['new'] = '0';

		$paramQry = $param ? http_build_query($param) : '';
		$page = $this->request('page') ? $this->request('page') : 1;
		
		$pagination = new fpagination($this->getCategories('', $param), $this->defaultLimit, $page, $paramQry);
		$keys = $pagination->result();

		$this->db->query("SELECT * FROM task_category WHERE status < 999 ORDER BY title");
		$allKeys = $this->db->getRowList();

		$languages = $this->getLanguages(['1']);

		if($allKeys){
			foreach($allKeys as $i => $key){
				$lang = array();
				foreach($languages as $language){
					$lang[$language['id']] = $this->db->getValue("SELECT title FROM task_category_language_text WHERE category_id = " . $this->db->escape($key['id']));
				}
				$allKeys[$i]['lang'] = $lang;
			}
		}
		
        set('param', $param);
        set('keys', $keys);
		set('pagination', $pagination);
		set('allKeys', $allKeys);
		set('languages', $languages);
		set('newKeys', $this->getCategories('', ['new' => '1']));
		return render('category.php', 'layout/default.php');
	}

	public function categoryListAdd(){
		set('categories', $this->getCategories());
		return render('category-add.php', 'layout/default.php');
	}

	public function categoryListEdit($id = 0){
		$this->db->query("SELECT * FROM task_category WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
		
		if($key){
			$available_languages = $this->getLanguages(['1']);
			$languages = array();
			foreach($available_languages as $i => $language){
				$languages[$language['id']]['title'] = $this->db->getValue("SELECT title FROM task_category_language_text WHERE category_id = " . $this->db->escape($id) . " AND language_id = " . $this->db->escape($language['id']));
			}

			$key['languages'] = $languages;

			set('key', $key);
			set('categories', $this->getCategories([$key['id']]));
			return render('category-edit.php', 'layout/default.php');
		}else{
			return not_found();
		}
	}

	public function getSkills($except = array(), $param = array()){
		$filterQry = array();
		if(!empty($param['title'])) $filterQry[] = " AND name LIKE " . $this->db->escape('%' . $param['title'] . '%');
		if(isset($param['new'])) $filterQry[] = " AND `new` = '" . $param['new'] . "'";
		$filterQry = $filterQry ? implode(' ', $filterQry) : '';

		if(!empty($except)) $this->db->query("SELECT * FROM skills WHERE status < 999 AND id NOT IN (" . implode(",", $except) . ") " . $filterQry . " ORDER BY parent, name");
		else $this->db->query("SELECT * FROM skills WHERE status < 999 " . $filterQry . " ORDER BY parent, name");
		$categories = $this->db->getRowList();
		
		if($categories){
			foreach($categories as $i => $cat){
				if(!empty($cat['parent'])){
					$parentID = $cat['parent'];
					
					while(!empty($parentID)){
						$this->db->query("SELECT * FROM skills WHERE id = " . $this->db->escape($parentID));
						$parent = $this->db->getSingleRow();
						$categories[$i]['parents'][] = $parent['name'];
						$parentID = $parent['parent'];
					}
					
					$categories[$i]['parents'] = array_reverse($categories[$i]['parents']);
					$categories[$i]['name'] = implode(' > ', $categories[$i]['parents']) . ' > ' . $categories[$i]['name'];
				}
			}
		}
		
		if(!empty($categories)) $this->sort_array_of_array($categories, 'name');
				
		return $categories;
	}

	public function skillList(){
		$title = $this->request('title');
		$param = array();

		if($title){
			$param['title'] = $title;
		}

		$param['new'] = '0';

		$paramQry = $param ? http_build_query($param) : '';
		$page = $this->request('page') ? $this->request('page') : 1;
		
		$pagination = new fpagination($this->getSkills('', $param), $this->defaultLimit, $page, $paramQry);
		$keys = $pagination->result();

		$this->db->query("SELECT * FROM skills WHERE status < 999 ORDER BY name");
		$allKeys = $this->db->getRowList();

		$languages = $this->getLanguages(['1']);

		if($allKeys){
			foreach($allKeys as $i => $key){
				$lang = array();
				foreach($languages as $language){
					$lang[$language['id']] = $this->db->getValue("SELECT name FROM skill_language_text WHERE skill_id = " . $this->db->escape($key['id']));
				}
				$allKeys[$i]['lang'] = $lang;
			}
		}
		
        set('param', $param);
        set('keys', $keys);
		set('pagination', $pagination);
		set('allKeys', $allKeys);
		set('languages', $languages);
		set('newKeys', $this->getSkills('', ['new' => '1']));
		return render('skill.php', 'layout/default.php');
	}

	public function skillListAdd(){
		set('skills', $this->getSkills());
		return render('skill-add.php', 'layout/default.php');
	}

	public function skillListEdit($id = 0){
		$this->db->query("SELECT * FROM skills WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
		
		if($key){
			$available_languages = $this->getLanguages(['1']);
			$languages = array();
			foreach($available_languages as $i => $language){
				$languages[$language['id']]['name'] = $this->db->getValue("SELECT `name` FROM skill_language_text WHERE skill_id = " . $this->db->escape($id) . " AND language_id = " . $this->db->escape($language['id']));
			}

			$key['languages'] = $languages;

			set('key', $key);
			set('skills', $this->getSkills([$key['id']]));
			return render('skill-edit.php', 'layout/default.php');
		}else{
			return not_found();
		}
	}

	public function getInterests($except = array(), $param = ''){
		$titleQry = '';
		if(!empty($param['title'])) $titleQry = " AND title LIKE " . $this->db->escape('%' . $param['title'] . '%');

		if(!empty($except)) $this->db->query("SELECT * FROM interest WHERE status < 999 AND id NOT IN (" . implode(",", $except) . ") " . $titleQry  . " ORDER BY parent, sortby");
		else $this->db->query("SELECT * FROM interest WHERE status < 999 " . $titleQry  . " ORDER BY parent, sortby");
		$keys = $this->db->getRowList();
		
		if($keys){
			foreach($keys as $i => $cat){
				$main_cat = 0;
				if($cat['parent'] == '0') $main_cat = $cat['id'];
				$keys[$i]['main_cat'] = $main_cat;

				if(!empty($cat['parent'])){
					$parentID = $cat['parent'];
					
					while(!empty($parentID)){
						$this->db->query("SELECT * FROM interest WHERE id = " . $this->db->escape($parentID));
						$parent = $this->db->getSingleRow();
						$keys[$i]['parents'][] = $parent['title'];
						$parentID = $parent['parent'];
						$main_cat = $parent['id'];
					}
					
					$keys[$i]['main_cat'] = $main_cat;
					$keys[$i]['parents'] = array_reverse($keys[$i]['parents']);
					$keys[$i]['title'] = implode(' > ', $keys[$i]['parents']) . ' > ' . $keys[$i]['title'];
				}
			}
		}
		
		if(!empty($keys)) $this->sort_array_of_array($keys, 'title');
				
		return $keys;
	}

	public function interestList(){
		$title = $this->request('title');
		$type = $this->request('type');
		$searchQry = '';
		$param = array();

		if($title){
			$param['title'] = $title;
		}

		if($type){
			$param['type'] = $type;
		}

		$param['new'] = '0';
		
		$paramQry = $param ? http_build_query($param) : '';
		$page = $this->request('page') ? $this->request('page') : 1;
		
		$pagination = new fpagination($this->getInterests('', $param), $this->defaultLimit, $page, $paramQry);
		$keys = $pagination->result();

		$this->db->query("SELECT * FROM interest WHERE status < 999 ORDER BY title");
		$allKeys = $this->db->getRowList();

		$languages = $this->getLanguages(['1']);

		if($allKeys){
			foreach($allKeys as $i => $key){
				$lang = array();
				foreach($languages as $language){
					$lang[$language['id']] = $this->db->getValue("SELECT title FROM interest_language_text WHERE interest_id = " . $this->db->escape($key['id']));
				}
				$allKeys[$i]['lang'] = $lang;
			}
		}
		
        set('param', $param);
        set('keys', $keys);
		set('pagination', $pagination);
		set('allKeys', $allKeys);
		set('languages', $languages);
		set('newKeys', $this->getCategories('', ['new' => '1']));
		return render('interest.php', 'layout/default.php');
	}

	public function interestListAdd(){
		set('interests', $this->getInterests());
		return render('interest-add.php', 'layout/default.php');
	}

	public function interestListEdit($id = 0){
		$this->db->query("SELECT * FROM interest WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
		
		if($key){
			$available_languages = $this->getLanguages(['1']);
			$languages = array();
			foreach($available_languages as $i => $language){
				$languages[$language['id']]['title'] = $this->db->getValue("SELECT title FROM interest_language_text WHERE interest_id = " . $this->db->escape($id) . " AND language_id = " . $this->db->escape($language['id']));
			}

			$key['languages'] = $languages;

			set('key', $key);
			set('interests', $this->getInterests([$key['id']]));
			return render('interest-edit.php', 'layout/default.php');
		}else{
			return not_found();
		}
	}

	public function subscription(){
		$this->db->query("SELECT * FROM subscription");
		$subscriptions = $this->db->getRowList();

		$keys = array();
		if($subscriptions){
			foreach($subscriptions as $i => $subscription){
				$keys[$subscription['id']] = $subscription;
			}
		}

		if($keys){
			foreach($keys as $i => $key){
				$available_languages = $this->getLanguages(['1']);
				foreach($available_languages as $j => $language){
					$languages = array();
					$languages[$language['id']]['title'] = $this->db->getValue("SELECT `title` FROM subscription_language_text WHERE subscription_id = " . $this->db->escape($key['id']) . " AND language_id = " . $this->db->escape($language['id']));
					$languages[$language['id']]['description'] = $this->db->getValue("SELECT `description` FROM subscription_language_text WHERE subscription_id = " . $this->db->escape($key['id']) . " AND language_id = " . $this->db->escape($language['id']));

					$languages[$language['id']]['title_2'] = $this->db->getValue("SELECT `title_2` FROM subscription_language_text WHERE subscription_id = " . $this->db->escape($key['id']) . " AND language_id = " . $this->db->escape($language['id']));
					$languages[$language['id']]['description_2'] = $this->db->getValue("SELECT `description_2` FROM subscription_language_text WHERE subscription_id = " . $this->db->escape($key['id']) . " AND language_id = " . $this->db->escape($language['id']));
					$languages[$language['id']]['features_2'] = $this->db->getValue("SELECT `features_2` FROM subscription_language_text WHERE subscription_id = " . $this->db->escape($key['id']) . " AND language_id = " . $this->db->escape($language['id']));

					$keys[$i]['languages'] = $languages;
				}
			}
		}

		set('keys', $keys);
		return render('subscription.php', 'layout/default.php');
	}

	public function systemSettings(){
		return render('system-settings.php', 'layout/default.php');
	}

	public function individualManagement(){
	    // individual?page=2 OR individual/2
        $page = (request('page') && is_numeric(request('page'))) ? request('page') :
                ((params('page') && is_numeric(params('page'))) ? params('page') :
                1);
        $per_page = 10;

        // filter/search
        $filters = ['name', 'email', 'country', 'state'];
        $search_query = '';
        $query_params = [];
        foreach ($filters as $filter){
            if( request($filter) && !empty( request($filter) ) ){
                $value =  mysqli_real_escape_string($this->db->connection, request($filter));
                $query_params[$filter] = $value;
                if( $filter === 'name' ) {
                    $search_query .= " AND (`firstname` LIKE '%{$value}%' OR `lastname` LIKE '%{$value}%') ";
                }elseif($filter === 'email'){
                    $search_query .= " AND (`email` LIKE '%{$value}%') ";
                }elseif($filter === 'country'){
                    $search_query .= " AND (`country` = '{$value}') ";
                }elseif($filter === 'state'){
                    $search_query .= " AND (`state` = '{$value}') ";
                }
            }
        }

        $query_params  = !empty($query_params) ? http_build_query($query_params) : '';

        $total_users = $this->db->getValue("SELECT COUNT(*) FROM `members` WHERE `type` = '0' {$search_query}");
        $offset = ($page - 1) * $per_page;
        $this->db->query("
                            SELECT `id`, `photo`, CONCAT(`firstname`, ' ', `lastname`) AS `name`, `email`, `mobile_number`, `date_created`,
                            CONCAT(
                            ( SELECT `name` FROM `states` WHERE `id` = m.`state` LIMIT 1 ), ', ',
                            ( SELECT `name` FROM `countries` WHERE `id` = m.`country` LIMIT 1 )
                            ) AS `location`
                            FROM `members` m
                            WHERE `type` = '0' {$search_query}
                            ORDER BY `date_created`
                            LIMIT {$offset}, {$per_page}
        ");
        $users = $this->db->getRowList();

        set('total_pages', ceil($total_users / $per_page));
        set('current_page', $page);
        set('users', $users);
        set('query_params', $query_params);
        return render('individual.php', 'layout/default.php');
    }

	public function individualDetail(){
	    $id = params('id');

	    if( ! $id ) redirect_to('/individual');

        $id = $this->db->escape($id);

        $this->db->query("SELECT * FROM `members` m WHERE `id` = {$id} AND `type` = '0' LIMIT 1");
        $user = $this->db->getSingleRow();

        if( empty($user) ) return not_found();

        $this->db->query("SELECT * FROM `employment_details` WHERE `member_id` = {$id}");
        $employment = $this->db->getRowList();

        $this->db->query("SELECT * FROM `education` WHERE `member_id` = {$id}");
        $educations = $this->db->getRowList();

        $this->db->query("SELECT * FROM `preference` WHERE `member_id` = {$id} LIMIT 1");
        $preference = $this->db->getSingleRow();

        $this->db->query("SELECT * FROM `member_docs` WHERE `member_id` = {$id}");
        $docs = $this->db->getRowList();

        $tasks = (new Task($this->db, null))->get_tasks(trim($id, "'"), 0, 9999);
        $applied_tasks = (new Task($this->db, new stdClass))->get_applied_tasks(trim($id, "'"), 0, 9999);
        $task_ids = array_merge(array_column($tasks, 'id'), array_column($applied_tasks, 'id'));
        $activities = (new TaskActivity($this->db, new stdClass))->formatted_activities($task_ids, trim($id, "'"));

        $pagination = new pagination($activities, 6, 1, url_for('ajax/activity-log', trim($id, "'")), '/');
        $_SESSION['activity_log_' . trim($id, "'")] = $pagination;
        $pagination->setStartData(1);

        $_docs = [];
        foreach($docs as $doc){
            $_docs[$doc['type']][] = $doc['src'];
        }

        $selected_skills = [];

        if($user['skills']){
            $this->db->query("SELECT * FROM skills WHERE id IN (" . $user['skills'] . ")");
            $skills = $this->db->getRowList();

            if($skills){
                foreach($skills as $skill){
                    $selected_skills[] = [ "id" => $skill['id'], "name" => $skill['name'] ];
                }
            }
		}
		
		$this->db->query("SELECT * FROM `subscription` WHERE status = '1'");
        $subscriptions = $this->db->getRowList();
		
        set('user', $user);
        set("employments", $employment);
        set("educations", $educations);
        set("preference", $preference);
        set("countries", $this->countries());
        set("states", $this->states(132));
        set("docs", $_docs);
        set("activities", $pagination);
        set('selected_skills', $selected_skills);
        set('subscriptions', $subscriptions);

        return render('individual-detail.php', 'layout/default.php');
	}

	public function memberAnalytics($id = 0){
		$member = $this->returnMemberDetails($id);
		$base_date = 'updated_at';
		$interval = $this->request('interval') ? $this->request('interval') : 'month';
		if(!in_array($interval, array('month', 'day'))) $interval = 'month';
		$day = $this->request('day') ? (int) $this->request('day') : date('d');
		$month = $this->request('month') ? (int) $this->request('month') : date('n');
		$year = $this->request('year') ? $this->request('year') : date('Y');
		$type = $this->request('type') ? $this->request('type') : 'seeker';
		if(!in_array($type, array('seeker', 'poster'))) $type = 'seeker';

		$param = array();
		if($this->request('interval')) $param['interval'] = $interval;
		if($this->request('day')) $param['day'] = $day;
		if($this->request('month')) $param['month'] = $month;
		if($this->request('year')) $param['year'] = $year;
		if($this->request('type')) $param['type'] = $type;
		$paramUri = http_build_query($param);

		$labels = array();
		$monthList = array();
		$monthList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01 -3 month'));
		$monthList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01 -2 month'));
		$monthList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01 -1 month'));
		$monthList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01'));
		$monthList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01 +1 month'));
		$monthList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01 +2 month'));
		$monthList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01 +3 month'));

		$dayList = array();
		$dayList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($day, 2, '0', STR_PAD_LEFT) . ' -3 day'));
		$dayList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($day, 2, '0', STR_PAD_LEFT) . ' -2 day'));
		$dayList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($day, 2, '0', STR_PAD_LEFT) . ' -1 day'));
		$dayList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($day, 2, '0', STR_PAD_LEFT) . ''));
		$dayList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($day, 2, '0', STR_PAD_LEFT) . ' +1 day'));
		$dayList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($day, 2, '0', STR_PAD_LEFT) . ' +2 day'));
		$dayList[] = date('Y-m-d', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($day, 2, '0', STR_PAD_LEFT) . ' +3 day'));

		if($interval == 'month'){
			$hours = array();
			$hours_highest['value'] = 0;
			$hours_highest['date'] = 0;

			$earnings = array();
			$earnings_highest['value'] = 0;
			$earnings_highest['date'] = 0;

			foreach(range(1, 12) as $m){
				$labels[] = date('M', mktime(0, 0, 0, $m, 10));

				if($type == 'seeker'){
					$this->db->query("SELECT SUM(hours) AS total_hours, SUM(budget) AS total_earnings, SUM(CASE WHEN status = 'disputed' THEN (budget*(100-rejection_rate)/100) ELSE budget END) AS total_final FROM `tasks` WHERE assigned_to = " . $this->db->escape($id) . " AND status IN ('closed','disputed') AND MONTH(" . $base_date . ") = " . $this->db->escape($m) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " GROUP BY MONTH(" . $base_date . ")");
				}else if($type == 'poster'){
					$this->db->query("SELECT SUM(hours) AS total_hours, SUM(budget) AS total_earnings, SUM(CASE WHEN status = 'disputed' THEN (budget*(100-rejection_rate)/100) ELSE budget END) AS total_final FROM `tasks` WHERE user_id = " . $this->db->escape($id) . " AND status IN ('closed','disputed') AND MONTH(" . $base_date . ") = " . $this->db->escape($m) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " GROUP BY MONTH(" . $base_date . ")");
				}
				$row = $this->db->getSingleRow();

				$hours[$m] = !empty($row['total_hours']) ? $row['total_hours'] : 0;
				$earnings[$m] = !empty($row['total_final']) ? $row['total_final'] : 0;
				if($hours[$m] > $hours_highest['value']){
					$hours_highest['value'] = $hours[$m];
					$hours_highest['date'] = $year . '-' . $m . '-01';
				}

				if($earnings[$m] > $earnings_highest['value']){
					$earnings_highest['value'] = $earnings[$m];
					$earnings_highest['date'] = $year . '-' . $m . '-01';
				}
			}

			set('id', $id);
			set('day', $day);
			set('month', $month);
			set('year', $year);
			set('hours', $hours);
			set('hours_highest', $hours_highest);
			set('earnings', $earnings);
			set('earnings_highest', $earnings_highest);
		}

		if($interval == 'day'){
			$hours = array();
			$hours_highest['value'] = 0;
			$hours_highest['date'] = 0;

			$earnings = array();
			$earnings_highest['value'] = 0;
			$earnings_highest['date'] = 0;

			foreach(range(1, date('t', strtotime($year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-01'))) as $d){
				$labels[] = $d;

				if($type == 'seeker'){
					$this->db->query("SELECT SUM(hours) AS total_hours, SUM(budget) AS total_earnings, SUM(CASE WHEN status = 'disputed' THEN (budget*(100-rejection_rate)/100) ELSE budget END) AS total_final FROM `tasks` WHERE assigned_to = " . $this->db->escape($id) . " AND status IN ('closed','disputed') AND DAY(" . $base_date . ") = " . $this->db->escape($d) . " AND DAY(" . $base_date . ") = " . $this->db->escape($d) . " AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " GROUP BY DAY(" . $base_date . ")");
				}else if($type == 'poster'){
					
					$this->db->query("SELECT SUM(hours) AS total_hours, SUM(budget) AS total_earnings, SUM(CASE WHEN status = 'disputed' THEN (budget*(100-rejection_rate)/100) ELSE budget END) AS total_final FROM `tasks` WHERE user_id = " . $this->db->escape($id) . " AND status IN ('closed','disputed') AND DAY(" . $base_date . ") = " . $this->db->escape($d) . " AND DAY(" . $base_date . ") = " . $this->db->escape($d) . " AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " GROUP BY DAY(" . $base_date . ")");
				}

				$row = $this->db->getSingleRow();

				$hours[$d] = !empty($row['total_hours']) ? $row['total_hours'] : 0;
				$earnings[$d] = !empty($row['total_final']) ? $row['total_final'] : 0;

				if($hours[$d] > $hours_highest['value']){
					$hours_highest['value'] = $hours[$d];
					$hours_highest['date'] = $year . '-' . $month . '-' . str_pad($d, 2, '0', STR_PAD_LEFT);
				}

				if($earnings[$d] > $earnings_highest['value']){
					$earnings_highest['value'] = $earnings[$d];
					$earnings_highest['date'] = $year . '-' . $month . '-' . str_pad($d, 2, '0', STR_PAD_LEFT);
				}
			}

			set('hours', $hours);
			set('hours_highest', $hours_highest);
			set('earnings', $earnings);
			set('earnings_highest', $earnings_highest);
		}

		if($interval == 'month'){
			$aggregated_data['hours'] = $hours[$month];
			$aggregated_data['earnings'] = $earnings[$month];

			if($type == 'seeker'){
				$this->db->query("SELECT * FROM `tasks` WHERE assigned_to = " . $this->db->escape($id) . " AND status IN ('completed','published','in-progress','closed','disputed') AND MONTH(assigned) = " . $this->db->escape($month) . " AND YEAR(assigned) = " . $this->db->escape($year) . " ORDER BY assigned");
			}else if($type == 'poster'){
				$this->db->query("SELECT * FROM `tasks` WHERE user_id = " . $this->db->escape($id) . " AND status IN ('completed','published','in-progress','closed','disputed') AND MONTH(created_at) = " . $this->db->escape($month) . " AND YEAR(created_at) = " . $this->db->escape($year) . " ORDER BY created_at");
			}
			
			$tasks = $this->db->getRowList();

			if($type == 'seeker'){
				$this->db->query("SELECT * FROM `tasks` WHERE assigned_to = " . $this->db->escape($id) . " AND status IN ('closed','disputed') AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " ORDER BY " . $base_date . "");
			}else if($type == 'poster'){
				$this->db->query("SELECT * FROM `tasks` WHERE user_id = " . $this->db->escape($id) . " AND status IN ('closed','disputed') AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " ORDER BY " . $base_date . "");
			}
			
			$tasks_completed = $this->db->getRowList();

		}else{

			if($type == 'seeker'){
				$this->db->query("SELECT SUM(hours) AS total_hours, SUM(budget) AS total_earnings, SUM(CASE WHEN status = 'disputed' THEN (budget*(100-rejection_rate)/100) ELSE budget END) AS total_final FROM `tasks` WHERE assigned_to = " . $this->db->escape($id) . " AND status IN ('closed','disputed') AND DAY(" . $base_date . ") = " . $this->db->escape($day) . " AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " GROUP BY MONTH(" . $base_date . ")");
			}else if($type == 'poster'){
				$this->db->query("SELECT SUM(hours) AS total_hours, SUM(budget) AS total_earnings, SUM(CASE WHEN status = 'disputed' THEN (budget*(100-rejection_rate)/100) ELSE budget END) AS total_final FROM `tasks` WHERE user_id = " . $this->db->escape($id) . " AND status IN ('closed','disputed') AND DAY(" . $base_date . ") = " . $this->db->escape($day) . " AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " GROUP BY MONTH(" . $base_date . ")");
			}

			$aggregated = $this->db->getSingleRow();

			$aggregated_data['hours'] = !empty($aggregated['total_hours']) ? $aggregated['total_hours'] : 0;
			$aggregated_data['earnings'] = !empty($aggregated['total_final']) ? $aggregated['total_final'] : 0;

			if($type == 'seeker'){
				$this->db->query("SELECT * FROM `tasks` WHERE assigned_to = " . $this->db->escape($id) . " AND status IN ('completed','published','in-progress','closed','disputed') AND DAY(assigned) = " . $this->db->escape($day) . " AND MONTH(assigned) = " . $this->db->escape($month) . " AND YEAR(assigned) = " . $this->db->escape($year) . " ORDER BY assigned");
			}else if($type == 'poster'){
				$this->db->query("SELECT * FROM `tasks` WHERE user_id = " . $this->db->escape($id) . " AND status IN ('completed','published','in-progress','closed','disputed') AND DAY(created_at) = " . $this->db->escape($day) . " AND MONTH(created_at) = " . $this->db->escape($month) . " AND YEAR(created_at) = " . $this->db->escape($year) . " ORDER BY created_at");
			}
			
			$tasks = $this->db->getRowList();

			if($type == 'seeker'){
				$this->db->query("SELECT * FROM `tasks` WHERE assigned_to = " . $this->db->escape($id) . " AND status IN ('closed','disputed') AND DAY(" . $base_date . ") = " . $this->db->escape($day) . " AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " ORDER BY " . $base_date . "");
			}else if($type == 'poster'){
				$this->db->query("SELECT * FROM `tasks` WHERE user_id = " . $this->db->escape($id) . " AND status IN ('closed','disputed') AND DAY(" . $base_date . ") = " . $this->db->escape($day) . " AND MONTH(" . $base_date . ") = " . $this->db->escape($month) . " AND YEAR(" . $base_date . ") = " . $this->db->escape($year) . " ORDER BY " . $base_date . "");
			}
			
			$tasks_completed = $this->db->getRowList();
		}
		
		set('id', $id);
		set('member', $member);
		set('param', $param);
		set('paramUri', $paramUri);
		set('interval', $interval);
		set('year', $year);
		set('month', $month);
		set('labels', $labels);
		set('monthList', $monthList);
		set('dayList', $dayList);
		set('aggregated', $aggregated_data);
		set('tasks', $tasks);
		set('tasks_completed', $tasks_completed);
		set('type', $type);
		return render('analytics.php', 'layout/iframe.php');
	}
	
	public function companyManagement(){
		$company_name = $this->request('company_name');
		$email = $this->request('email');
		$country = $this->request('country');
		$state = $this->request('state');

		$searchQry = '';
		$param = array();

		if($company_name){
			$searchQry .= " AND cd.name LIKE " . $this->db->escape("%" . $company_name . "%");
			$param['company_name'] = $company_name;
		}
		if($email){
			$searchQry .= " AND m.email LIKE " . $this->db->escape("%" . $email . "%");
			$param['email'] = $email;
		}
		if($country){
			$searchQry .= " AND country = " . $this->db->escape($country);
			$param['country'] = $country;
		}
		if($state){
			$searchQry .= " AND state = " . $this->db->escape($state);
			$param['state'] = $state;
		}

		$paramQry = $param ? http_build_query($param) : '';
		
        $this->db->query("SELECT m.*, cd.name, CONCAT(m.firstname, ' ', m.lastname) AS pic, CONCAT((SELECT `name` FROM `states` WHERE `id` = m.`state` LIMIT 1), ', ', 
		(SELECT `name` FROM `countries` WHERE `id` = m.`country` LIMIT 1)) AS location FROM `members` m LEFT JOIN `company_details` cd ON m.id = cd.member_id WHERE type = '1'" . $searchQry);

		$page = $this->request('page') ? $this->request('page') : 1;
		$pagination = new fpagination($this->db->getRowList(), $this->defaultLimit, $page, $paramQry);
		$keys = $pagination->result();
		
        set('param', $param);
        set('keys', $keys);
        set('pagination', $pagination);
        return render('company.php', 'layout/default.php');
	}
	
	public function companyDetail($id = 0){
		$this->db->query("SELECT * FROM `members` WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();

		if($key){
			$this->db->query("SELECT * FROM `company_details` WHERE member_id = " . $this->db->escape($id));
			$company = $this->db->getSingleRow();

			if($company){
				$company['photos'] = unserialize($company['photos']);
			}

			$this->db->query("SELECT * FROM `preference` WHERE `member_id` = {$id} LIMIT 1");
			$preference = $this->db->getSingleRow();

            $tasks = (new Task($this->db, null))->get_tasks($id, 0, 9999);
            $task_ids = array_column($tasks, 'id');
            $activities = (new TaskActivity($this->db, new stdClass))->formatted_activities($task_ids, $id);
            $pagination = new pagination($activities, 6, 1, url_for('ajax/activity-log', $id), '/');
            $_SESSION['activity_log_' . $id] = $pagination;
            $pagination->setStartData(1);
	
			set('key', $key);
			set('preference', $preference);
			set('company', $company);
			set('countries', $this->countries());
			set('activities', $pagination);

			return render('company-detail.php', 'layout/default.php');

		}else{
			return not_found();
		}
	}

	public function taskManagement(){
        $tab = params('tab')  ? params('tab') : 'organic';

        $page = (request('page') && is_numeric(request('page'))) ? request('page') :
            ((params('page') && is_numeric(params('page'))) ? params('page') :
                1);

        $per_page = 8;

        $organic_page = $tab === 'organic' ? $page : 1;
        $aggregate_page = $tab === 'aggregate' ? $page : 1;

        $organic_offset = ($organic_page - 1) * $per_page;
        $aggregate_offset = ($aggregate_page - 1) * $per_page;

        $organic_search_query = $organic_query_params = '';
        $aggregate_search_query = $aggregate_query_params = '';

        if( $tab === 'organic' ) {
            $organic_filters = ['id', 'title', 'posted', 'closing'];
            $organic_params = [];
            foreach ($organic_filters as $filter){
                if( request($filter) && !empty( request($filter) ) ){
                    $value =  mysqli_real_escape_string($this->db->connection, request($filter));
                    $organic_params[$filter] = $value;
                    if( $filter === 'posted' || $filter === 'closing' ){
                        $date = date('Y-m-d', strtotime($value));
                    }

                    if( $filter === 'id' ) {
                        $organic_search_query .= " AND (t.`number` = '{$value}') ";
                    }elseif($filter === 'title'){
                        $organic_search_query .= " AND (t.`title` LIKE '%{$value}%') ";
                    }elseif($filter === 'posted'){
                        $organic_search_query .= " AND ( DATE(t.`created_at`) = '{$date}') ";
                    }elseif($filter === 'closing'){
                        $organic_search_query .= " AND ( DATE(t.`complete_by`) = '{$date}') ";
                    }
                }
            }

            $organic_search_query  = !empty($organic_search_query) ? ' WHERE ' . ltrim($organic_search_query, ' AND') : '';
            $organic_query_params  = !empty($organic_params) ? http_build_query($organic_params) : '';

        }elseif ( $tab === 'aggregate' ){

            $aggregate_filters = ['id', 'title', 'posted', 'closing', 'aggregated', 'source'];
            $aggregate_params = [];
            foreach ($aggregate_filters as $filter){
                if( request($filter) && !empty( request($filter) ) ){
                    $value =  mysqli_real_escape_string($this->db->connection, request($filter));
                    $aggregate_params[$filter] = $value;
                    if( $filter === 'posted' || $filter === 'closing' || 'aggregated' ){
                        $date = date('Y-m-d', strtotime($value));
                    }

                    if( $filter === 'id' ) {
                        $aggregate_search_query .= " AND (t.`number` = '{$value}') ";
                    }elseif($filter === 'title'){
                        $aggregate_search_query .= " AND (t.`title` LIKE '%{$value}%') ";
                    }elseif($filter === 'posted' || $filter === 'aggregated'){
                        $aggregate_search_query .= " AND ( DATE(t.`created_at`) = '{$date}') ";
                    }elseif($filter === 'closing'){
                        $aggregate_search_query .= " AND ( DATE(t.`complete_by`) = '{$date}') ";
                    }elseif($filter === 'source'){
                        $aggregate_search_query .= " AND ( t.`source` = '{$value}') ";
                    }
                }
            }

            $aggregate_search_query  = !empty($aggregate_search_query) ? ' WHERE ' . ltrim($aggregate_search_query, ' AND') : '';
            $aggregate_query_params  = !empty($aggregate_params) ? http_build_query($aggregate_params) : '';

        }

	    $total_tasks = $this->db->getValue("SELECT COUNT(*) FROM `tasks` t {$organic_search_query}");
	    $this->db->query("SELECT t.`id`, t.`number`, t.`title`, t.`created_at`, t.`complete_by`, m.`photo`, 
	                      CONCAT(m.`firstname`, ' ', m.`lastname`) AS `poster_name`, c.`name` AS `company_name`
	                      FROM `tasks` t
                          INNER JOIN `members` m ON	m.`id` = t.`user_id`  
                          LEFT JOIN `company_details` c ON c.`member_id` = m.`id`
                          {$organic_search_query}
	                      ORDER BY t.`created_at` LIMIT {$organic_offset}, {$per_page}");
	    $tasks = $this->db->getRowList();

	    $total_aggregate = $this->db->getValue("SELECT COUNT(*) FROM `external_tasks` t {$aggregate_search_query}");
	    $this->db->query("SELECT t.`id`, t.`number`, t.`title`, t.`created_at`, t.`complete_by`, t.`source`
	                      FROM `external_tasks` t
	                      {$aggregate_search_query}
	                      ORDER BY t.`created_at` LIMIT {$aggregate_offset}, {$per_page}");
	    $aggregates = $this->db->getRowList();

	    $this->db->query("SELECT DISTINCT `source` FROM `external_tasks` GROUP BY `source`");
	    $aggregate_sources = $this->db->getRowList() ? array_column($this->db->getRowList(), 'source') : [];

	    set('tab', $tab);
	    set('tasks_total_pages', ceil($total_tasks / $per_page));
	    set('tasks_current_page', $organic_page);
	    set('tasks', $tasks);
	    set('organic_query_params', $organic_query_params);
        set('external_tasks_total_pages', ceil($total_aggregate / $per_page));
        set('external_tasks_current_page', $aggregate_page);
	    set('external_tasks', $aggregates);
	    set('aggregate_sources', $aggregate_sources);
        set('aggregate_query_params', $aggregate_query_params);
        return render('task.php', 'layout/default.php');
    }

	public function jobManagement(){
        $tab = params('tab')  ? params('tab') : 'organic';

        $page = (request('page') && is_numeric(request('page'))) ? request('page') :
            ((params('page') && is_numeric(params('page'))) ? params('page') :
                1);

        $per_page = 8;

        $organic_page = $tab === 'organic' ? $page : 1;
        $aggregate_page = $tab === 'aggregate' ? $page : 1;

        $organic_offset = ($organic_page - 1) * $per_page;
        $aggregate_offset = ($aggregate_page - 1) * $per_page;

        $organic_search_query = $organic_query_params = '';
        $aggregate_search_query = $aggregate_query_params = '';

        if( $tab === 'organic' ) {
            $organic_filters = ['id', 'title', 'posted', 'closing'];
            $organic_params = [];
            foreach ($organic_filters as $filter){
                if( request($filter) && !empty( request($filter) ) ){
                    $value =  mysqli_real_escape_string($this->db->connection, request($filter));
                    $organic_params[$filter] = $value;
                    if( $filter === 'posted' || $filter === 'closing' ){
                        $date = date('Y-m-d', strtotime($value));
                    }

                    if( $filter === 'id' ) {
                        $organic_search_query .= " AND (j.`number` = '{$value}') ";
                    }elseif($filter === 'title'){
                        $organic_search_query .= " AND (j.`title` LIKE '%{$value}%') ";
                    }elseif($filter === 'posted'){
                        $organic_search_query .= " AND ( DATE(j.`created_at`) = '{$date}') ";
                    }elseif($filter === 'closing'){
                        $organic_search_query .= " AND ( DATE(j.`closed_at`) = '{$date}') ";
                    }
                }
            }

            $organic_search_query  = !empty($organic_search_query) ? ' WHERE ' . ltrim($organic_search_query, ' AND') : '';
            $organic_query_params  = !empty($organic_params) ? http_build_query($organic_params) : '';

        }elseif ( $tab === 'aggregate' ){

            $aggregate_filters = ['id', 'title', 'posted', 'closing', 'aggregated', 'source'];
            $aggregate_params = [];
            foreach ($aggregate_filters as $filter){
                if( request($filter) && !empty( request($filter) ) ){
                    $value =  mysqli_real_escape_string($this->db->connection, request($filter));
                    $aggregate_params[$filter] = $value;
                    if( $filter === 'posted' || $filter === 'closing' || 'aggregated' ){
                        $date = date('Y-m-d', strtotime($value));
                    }

                    if( $filter === 'id' ) {
                        $aggregate_search_query .= " AND (j.`number` = '{$value}') ";
                    }elseif($filter === 'title'){
                        $aggregate_search_query .= " AND (j.`title` LIKE '%{$value}%') ";
                    }elseif($filter === 'posted' || $filter === 'aggregated'){
                        $aggregate_search_query .= " AND ( DATE(j.`created_at`) = '{$date}') ";
                    }elseif($filter === 'closing'){
                        $aggregate_search_query .= " AND ( DATE(j.`complete_by`) = '{$date}') ";
                    }elseif($filter === 'source'){
                        $aggregate_search_query .= " AND ( j.`source` = '{$value}') ";
                    }
                }
            }

            $aggregate_search_query  = !empty($aggregate_search_query) ? ' WHERE ' . ltrim($aggregate_search_query, ' AND') : '';
            $aggregate_query_params  = !empty($aggregate_params) ? http_build_query($aggregate_params) : '';

        }

	    $total_jobs = $this->db->getValue("SELECT COUNT(*) FROM `jobs` j {$organic_search_query}");
	    $this->db->query("SELECT j.`id`, j.`number`, j.`title`, j.`created_at`, j.`closed_at`, m.`photo`, 
	                      CONCAT(m.`firstname`, ' ', m.`lastname`) AS `poster_name`, c.`name` AS `company_name`
	                      FROM `jobs` j
                          INNER JOIN `members` m ON	m.`id` = j.`user_id`  
                          LEFT JOIN `company_details` c ON c.`member_id` = m.`id`
                          {$organic_search_query}
	                      ORDER BY j.`created_at` LIMIT {$organic_offset}, {$per_page}");
	    $jobs = $this->db->getRowList();

	    $total_aggregate = $this->db->getValue("SELECT COUNT(*) FROM `external_jobs` j {$aggregate_search_query}");
	    $this->db->query("SELECT j.`id`, j.`number`, j.`title`, j.`created_at`, j.`closed_at`, j.`source`
	                      FROM `external_jobs` j
	                      {$aggregate_search_query}
	                      ORDER BY j.`created_at` LIMIT {$aggregate_offset}, {$per_page}");
	    $aggregates = $this->db->getRowList();

	    $this->db->query("SELECT DISTINCT `source` FROM `external_jobs` GROUP BY `source`");
	    $aggregate_sources = $this->db->getRowList() ? array_column($this->db->getRowList(), 'source') : [];

	    set('tab', $tab);
	    set('jobs_total_pages', ceil($total_jobs / $per_page));
	    set('jobs_current_page', $organic_page);
	    set('jobs', $jobs);
	    set('organic_query_params', $organic_query_params);
        set('external_jobs_total_pages', ceil($total_aggregate / $per_page));
        set('external_jobs_current_page', $aggregate_page);
	    set('external_jobs', $aggregates);
	    set('aggregate_sources', $aggregate_sources);
	    set('aggregate_query_params', $aggregate_query_params);
        return render('job.php', 'layout/default.php');
    }

	public function ajaxGetCategories($parent_id = 0, $type = null){
		if( !is_null($type) && $type === 'task' ) $table = 'task_category'; else $table = 'category';
		$categories = $this->getCategories($parent_id, $table);
		$categoryList = '<option disabled selected>Sub Category</option>';
		
		if($categories){
			foreach($categories as $key){
				$categoryList .= '<option value="' . $key['id'] . '">' . $key['title'] . '</option>';	
			}
		}

		//$categoryList .= '<option value="' . $key['id'] . '">' . $key['title'] . '</option>';	
		
		$return['html'] = $categoryList;
		$return['error'] = false;
		$return['msg'] = "Request success."; 
		
		return json($return);
	}

	public function ajaxGetStates($country){
		$states = $this->states($country);
		$stateList = '<option value="" selected>State</option>';
		//$stateList = '';
		
		if($states){
			foreach($states as $keys){
				$stateList .= '<option value="' . $keys['id'] . '">' . $keys['name'] . '</option>';	
			}
		}
		
		$return['states'] = $stateList;
		$return['error'] = false;
		$return['msg'] = "Request success."; 
		
		return json($return);
	}

	public function ajaxGetCities($state_id = 1944 ){
		$states = $this->cities($state_id);
		$stateList = '<option value="">Town/City</option>';
		//$stateList = '';
		
		if($states){
			foreach($states as $keys){
				$stateList .= '<option value="' . $keys['id'] . '">' . $keys['name'] . '</option>';	
			}
		}
		
		$return['states'] = $stateList;
		$return['error'] = false;
		$return['msg'] = "Request success."; 
		
		return json($return);
	}

	public function ajaxSkillList(){
		$all = $this->request('all') ? true : false;
		$rows = $this->getSkillList($all);
		$skills = array();
		
		if($rows){
			foreach($rows as $row){
				$skills[] = array(
					"id" => $row['id'],
					"name" => $row['name'],
				);
			}
		}

		return json($skills);
	}

	public function getSkillList($all = false){
		if(!$all){
			$this->db->query("SELECT * FROM skills WHERE status = '1' AND parent = '0'");
			$rows = $this->db->getRowList();

			if($rows){
				foreach($rows as $i => $row){
					$this->db->query("SELECT * FROM skills WHERE status = '1' AND parent = " . $this->db->escape($row['id']));
					$subs = $this->db->getRowList();			
					
					if($subs){
						$rows[$i]['regimes'] = $subs;
					}
				}
			}

		}else{
			
			$this->db->query("SELECT * FROM skills WHERE status = '1'");
			$rows = $this->db->getRowList();
		}

		return $rows;
	}

    public function paginate_activity($user_id, $page){
        $activities = $_SESSION['activity_log_' . $user_id];
        $activities->setStartData($page);
        echo partial('partial/activity-log.php', ['activities' => $activities]);
    }

	/**********************************************************/
	public function profile(){
		$this->db->query("SELECT * FROM user_admins WHERE admin_id = ".$this->db->escape($_SESSION[BACKEND_PREFIX.'ADMIN_ID'])."");
		$keys = $this->db->getSingleRow();
		set('keys', $keys);
		return render('settings/profile.php', 'layout/default.php');
	}
	
	public function admin(){
		$this->db->query("SELECT *, (SELECT title FROM user_admin_role WHERE id = admin_role) AS role FROM user_admins WHERE admin_status < 999");
		$keys = $this->db->getRowList();

		if($keys){
			foreach($keys as $i => $key){
				if($key['admin_id'] == '1'){
					$keys[$i]['role'] = 'Root';
				}
			}
		}

		$this->db->query("SELECT * FROM user_admin_role ORDER BY title");
		$roles = $this->db->getRowList();

		set('keys', $keys);
		set('roles', $roles);
		return render('admin.php', 'layout/default.php');
	}
	
	public function adminAdd(){
		$this->db->query("SELECT * FROM user_admin_role ORDER BY title");
		$roles = $this->db->getRowList();

		set('roles', $roles);
		return render('admin-add.php', 'layout/default.php');
	}
	
	public function adminEdit($id = 0){
		$this->db->query("SELECT * FROM user_admins WHERE admin_status < 999 AND admin_id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
		
		if($key){
			$this->db->query("SELECT * FROM user_admin_role ORDER BY title");
			$roles = $this->db->getRowList();

			set('key', $key);
			set('roles', $roles);
			return render('admin-edit.php', 'layout/default.php');
		}
	}

	public function adminRole(){
		$this->db->query("SELECT * FROM user_admin_role ORDER BY title");
		$keys = $this->db->getRowList();

		set('keys', $keys);
		return render('admin-role.php', 'layout/default.php');
	}
	
	public function adminRoleAdd(){
		return render('admin-role-add.php', 'layout/default.php');
	}
	
	public function adminRoleEdit($id = 0){
		$this->db->query("SELECT * FROM user_admin_role WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
		
		if($key){
			$permissions = explode(',', $key['permissions']);
			set('key', $key);
			set('permissions', $permissions);
			return render('admin-role-edit.php', 'layout/default.php');
		}
	}
	
	public function pages(){
		$this->db->query("SELECT * FROM pages ORDER BY title");
		$keys = $this->db->getRowList();
		
		set('keys', $keys);
		return render('management/pages.php', 'layout/default.php');
	}
	
	public function pageInsert(){
		return render('management/page-insert.php', 'layout/default.php');
	}
	
	public function pageUpdate($id){
		$this->db->query("SELECT * FROM pages WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
		
		if($key){
			set('key', $key);
			return render('management/page-update.php', 'layout/default.php');
		}
	}
	
	public function members(){
		$this->db->query("SELECT * FROM members WHERE status < 999");
		$keys = $this->db->getRowList();
		
		set('keys', $keys);
		return render('management/members.php', 'layout/default.php');
	}

	public function returnMemberDetails($id){
		$this->db->query("SELECT * FROM members WHERE id = ".$id);
		$keys = $this->db->getSingleRow();

		return $keys;
	}
	
	public function memberInsert(){
		$states = $this->state('129');
		set('countries', $this->countries());
		set('states', $states);
		return render('management/members-insert.php', 'layout/default.php');
	}
	
	public function memberUpdate($id){
		$key = $this->member($id);
		if($key){
			$states = $this->state($key['country']) ? $this->state($key['country']) : array();
			set('key', $key);
			set('countries', $this->countries());
			set('states', $states);
			return render('management/members-update.php', 'layout/default.php');
		}
	}
	
	public function member($id){
		$this->db->query("SELECT * FROM members WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
		
		if($id == '0'){
			$key['photo'] = 'assets/images/clogo.png';
			$key['id'] = '0';
			$key['username'] = 'guest';
			$key['firstname'] = 'Guest';
			$key['lastname'] = '';
		}
		
		return $key;
	}
		
	public function getMemberInfo($id, $col = '*'){
		$col = is_array($col) ? implode(',', $col) : $col;
		$this->db->query("SELECT " . $col . " FROM members WHERE id = " . $this->db->escape($id));	
		return $this->db->getSingleRow();
	}
	
	public function products(){
		$this->db->query("SELECT * FROM products WHERE status < 999");
		$keys = $this->db->getRowList();
		
		if($keys){
			foreach($keys as $i => $key){
				$keys[$i]['categories'] = array();
				if($key['categories']){
					$categories = explode(',', $key['categories']);
					foreach($categories as $cat){
						$cats = array();
						
						$this->db->query("SELECT * FROM category WHERE id = " . $this->db->escape($cat));
						$parent = $this->db->getSingleRow();
						
						$cats[] = $parent['title'];
					
						while(!empty($parent['parent'])){
							$this->db->query("SELECT * FROM category WHERE id = " . $this->db->escape($parent['parent']));
							$parent = $this->db->getSingleRow();
							$cats[] = $parent['title'];
						}
						
						$cats = array_reverse($cats);
						$keys[$i]['categories'][] = implode(' > ', $cats);
					}
				}
			}
		}
		
		if(!empty($keys)) $this->sort_array_of_array($keys, 'categories');
		
		set('keys', $keys);
		return render('management/products.php', 'layout/default.php');
	}
	
	public function productUpdate($id){
		$this->db->query("SELECT * FROM products WHERE status < 999 AND id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
		
		if($key){
			$countries = $this->countries();
			$countryList = '<option value="0">All Countries</option>';
			if($countries){
				foreach($countries as $keys){
					$countryList .= '<option value="' . $keys['id'] . '">' . $keys['name'] . '</option>';
				}
			}
			
			$this->db->query("SELECT * FROM products WHERE status = '1' AND id != " . $this->db->escape($id));
			$products = $this->db->getRowList();
			
			set('key', $key);
			set('images', unserialize($key['images']));
			set('options', unserialize($key['options']));
			set('colors', unserialize($key['colors']));
			set('countries', $countries);
			set('countryList', $countryList);
			set('categories', $this->getCategories());
			set('products', $products);
			return render('management/products-update.php', 'layout/default.php');
		
		}else{
			return render('layout/404.php', '');
		}
	}
			
	public function sort_array_of_array(&$array, $subfield, $sort = 'asc'){
		$sortarray = array();
		foreach ($array as $key => $row)
		{
			$sortarray[$key] = $row[$subfield];
		}
		if($sort == 'asc') array_multisort($sortarray, SORT_ASC, $array);
		else array_multisort($sortarray, SORT_DESC, $array);
	}
	
	public function orders(){
		$this->db->query("SELECT * FROM shopping_checkout WHERE status < 999 AND carts != '' ORDER BY id DESC");
		$keys = $this->db->getRowList();
		
		if($keys){
			foreach($keys as $i => $key){
				$keys[$i]['member'] = $this->member($key['member_id']);
				$keys[$i]['lastTracking'] = $this->lastTracking($key['id']);
			}
		}
		
		set('keys', $keys);
		return render('orders.php', 'layout/default.php');
	}
	
	public function orderUpdate($id){
		$this->db->query("SELECT * FROM shopping_checkout WHERE status < 999 AND id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
		
		if($key){
			$promo = false;
			$key['member'] =  $this->member($key['member_id']);
			$key['shipping_state'] = $this->getStateName($key['delivery_state']);
			$key['shipping_country'] = $this->getCountryName($key['delivery_country']);
			
			$this->db->query("SELECT * FROM shopping_cart WHERE id IN (" . $key['carts'] . ")");	
			$carts = $this->db->getRowList();
			
			if($carts){
				foreach($carts as $i => $cart){
					$this->db->query("SELECT * FROM products WHERE status = '1' AND id = " . $this->db->escape($cart['item_id']));	
					$carts[$i]['product'] = $this->db->getSingleRow();
					$carts[$i]['product']['images'] = unserialize($carts[$i]['product']['images']);
					$carts[$i]['product']['options'] = unserialize($carts[$i]['product']['options']);
					$carts[$i]['options'] = unserialize($carts[$i]['item_option']);
					
					if(!empty($cart['promo_code'])){
						$this->db->query("SELECT * FROM promo_code WHERE code = " . $this->db->escape($cart['promo_code']));
						$promo = $this->db->getSingleRow();
						$promo = $cart['promo_code'] . ' (' . ($promo['type'] == 'percent' ? $promo['value'] . '%' : '-' . $this->price($promo['value'])) . ' product discount)';
					}
				}
			}
		}
		
		set('key', $key);
		set('carts', $carts);
		set('promo', $promo);
		set('lastTracking', $this->lastTracking($id));
		set('receipts', unserialize($key['payment_receipts']));
		set('GetExpressCheckoutDetails', unserialize($key['pp_GetExpressCheckoutDetails']));
		set('DoExpressCheckoutPayment', unserialize($key['pp_DoExpressCheckoutPayment']));
		return render('order-update.php', 'layout/default.php');
	}
	
	public function lastTracking($id){
		$this->db->query("SELECT * FROM tracking WHERE checkout_id = " . $this->db->escape($id) . " ORDER BY date DESC LIMIT 1");
		return $this->db->getSingleRow();
	}

	public function getCategoryName($id){
		return $this->db->getValue("SELECT title FROM category WHERE id = " . $this->db->escape($id));	
	}
	
	public function holiday(){
		$this->db->query("SELECT * FROM holidays WHERE status < 999 ORDER BY date_start DESC");
		$keys = $this->db->getRowList();
		
		set('keys', $keys);
		return render('management/holiday.php', 'layout/default.php');
	}
	
	public function holidayUpdate($id){
		$this->db->query("SELECT * FROM holidays WHERE status < 999 AND id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
				
		if($key){
			set('key', $key);
			return render('management/holiday-update.php', 'layout/default.php');
		}else{
			return render('layout/404.php', '');
		}
	}
	
	public function holidayInsert(){		
		return render('management/holiday-insert.php', 'layout/default.php');
	}
	
	public function promoCode(){
		$this->db->query("SELECT * FROM promo_code WHERE status < 999 ORDER BY date_start DESC");
		$keys = $this->db->getRowList();
		
		if($keys){
			foreach($keys as $i => $key){
				if(!empty($key['category_id'])){
					if($key['category_id'] != '-1'){
						$this->db->query("SELECT * FROM category WHERE id = " . $this->db->escape($key['category_id']));
						$cat = $this->db->getSingleRow();
						$keys[$i]['category'] = $cat['title'];
					}else{
						$keys[$i]['category'] = 'All Categories';
					}
				}
				
				if(!empty($key['product_id'])){
					$ids = explode(',', $key['product_id']);
					if($ids){
						foreach($ids as $id){
							$this->db->query("SELECT * FROM products WHERE id = " . $this->db->escape($id));
							$product = $this->db->getSingleRow();
							if($product){
								$keys[$i]['products'][] = $product['title'];
							}
						}
					}
				}
				
			}
		}
		
		set('keys', $keys);
		return render('management/promo-code.php', 'layout/default.php');
	}
	
	public function promoCodeUpdate($id){
		$this->db->query("SELECT * FROM promo_code WHERE status < 999 AND id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();
				
		if($key){
			$this->db->query("SELECT * FROM products WHERE status = '1' ORDER BY title");
			$products = $this->db->getRowList();
			
			set('key', $key);
			set('categories', $this->getCategories());
			set('products', $products);
			return render('management/promo-code-update.php', 'layout/default.php');
		}else{
			return render('layout/404.php', '');
		}
	}
	
	public function promoCodeInsert(){		
		$this->db->query("SELECT * FROM products WHERE status = '1' ORDER BY title");
		$products = $this->db->getRowList();
		set('categories', $this->getCategories());
		set('products', $products);
		return render('management/promo-code-insert.php', 'layout/default.php');
	}
	
	public function shipping_time(){
		return array(
			'0' => '9:30 am - 10:30 am',
			'1' => '10:30 am - 12:30 pm',
			'2' => '12:30 am - 3:30 pm',
			'3' => '3:30 pm - 6:30 pm',
		);
	}
}
?>