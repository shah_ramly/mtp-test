<?php
require_once 'cms.php';
class post{
	public $db;
	public $cms;
	
	public function __construct($db){
		$this->db = $db;
		$this->posts = !empty($_POST) ? $_POST : '';
		$this->cms = new cms($db);
	}
	
	public function request($field){
		$val = !empty($_REQUEST[$field]) ? $_REQUEST[$field] : false;
		return $val;
	}
	
	public function settings(){
		$setting = array();
		$settings = array();
		
		$this->db->query("SELECT * FROM settings");	
		$settings = $this->db->getRowList();
		
		foreach($settings as $keys){
			$setting[$keys['name']] = $keys['value'];
		}
		
		return $setting;
	}
	
	public function login(){	
		$this->db->query("SELECT * FROM user_admins WHERE admin_username = ".$this->db->escape($this->request('username'))." AND admin_password = ".$this->db->escape(md5($this->request('password')))." AND admin_status = '1'");
		$admin = $this->db->getSingleRow();
		
		if($admin){
		
			$_SESSION[BACKEND_PREFIX.'ADMIN_ID'] = $admin['admin_id'];
			$_SESSION[BACKEND_PREFIX.'ADMIN_USERNAME'] = $admin['admin_username'];
			$_SESSION[BACKEND_PREFIX.'ADMIN_PASSWORD'] = $admin['admin_password'];
			$_SESSION[BACKEND_PREFIX.'ADMIN_ROLE'] = $admin['admin_role'];;
									
			$this->db->query("UPDATE user_admins SET admin_last_activity = NOW() WHERE admin_username = ".$this->db->escape($this->request('username'))." AND admin_password = ".$this->db->escape(md5($this->request('password')))." AND admin_status = '1'");
					
			$return['error'] = false;
			$return['msg'] = "You have been logged in successfully."; 
		}else{
			$return['error'] = true;
			$return['msg'] = "Invalid account.<br>Please try again."; 
		}
				
		return json_encode($return);
	}

	public function delete(){
		$type = $this->request('type');
		
		$return['error'] = true;
		$return['msg'] = "Invalid action!";
		
		if($type == 'industry'){
			$this->db->table("industry");
			$this->db->updateArray(array(
				"status" => '999',
			));
			$this->db->whereArray(array(
				"id" => $this->request('id'),
			));
			$this->db->update();
			
			$return['error'] = false;
			$return['msg'] =  "Industry deleted.";
		}

		if($type == 'category'){
			$this->db->table("task_category");
			$this->db->updateArray(array(
				"status" => '999',
			));
			$this->db->whereArray(array(
				"id" => $this->request('id'),
			));
			$this->db->update();
			
			$return['error'] = false;
			$return['msg'] =  "Category deleted.";
		}

		if($type == 'skill'){
			$this->db->table("skills");
			$this->db->updateArray(array(
				"status" => '999',
			));
			$this->db->whereArray(array(
				"id" => $this->request('id'),
			));
			$this->db->update();
			
			$return['error'] = false;
			$return['msg'] =  "Skill deleted.";
		}

		if($type == 'interest'){
			$this->db->table("interest");
			$this->db->updateArray(array(
				"status" => '999',
			));
			$this->db->whereArray(array(
				"id" => $this->request('id'),
			));
			$this->db->update();
			
			$return['error'] = false;
			$return['msg'] =  "Interest deleted.";
		}
		
		return json_encode($return);
	}

	public function addCountry(){
		if($this->request('name')){
			$this->db->table("countries");
			$this->db->insertArray(array(
				"name" => $this->request('name'),
				"phonecode" => $this->request('phonecode'),
				"capital" => $this->request('capital'),
				"currency" => $this->request('currency'),
				"created_at" => 'NOW()',
				"updated_at" => 'NOW()',
			));
			$this->db->insert();

			$return['error'] = false;
			$return['msg'] = "State added.";
			$return['url'] = url_for('/country');
			
		}else{
			$return['error'] = true;
			$return['msg'] =  "Name is required.";
		}

		return json_encode($return);
	}

	public function editCountry($id = 0){
		$this->db->query("SELECT * FROM `countries` WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();

		if($key){
			$this->db->table("countries");
			$this->db->updateArray(array(
				"name" => $this->request('name'),
				"phonecode" => $this->request('phonecode'),
				"capital" => $this->request('titcapitalle'),
				"currency" => $this->request('currency'),
				"updated_at" => 'NOW()',
			));
			$this->db->whereArray(array(
				"id" => $id,
			));
			$this->db->update();

			$return['error'] = false;
			$return['msg'] = "Country updated.";
			$return['url'] = url_for('/country');
			
		}else{
			$return['error'] = true;
			$return['msg'] =  "Invalid ID.";
		}

		return json_encode($return);
	}
	
	public function deleteCountry($id = 0){
		$selected = $this->request('selected') ? $this->request('selected') : array();

		if(!$selected && !$id){
			$return['error'] = true;
			$return['msg'] =  "No country selected";

		}else{

			if($selected){
				$this->db->queryOrDie("UPDATE countries SET status = '999' WHERE id IN (" . implode(',', $selected) . ")");

				$return['error'] = false;
				$return['msg'] = "Selected countries deleted.";
				
			}else{

				$this->db->queryOrDie("UPDATE countries SET status = '999' WHERE id = " . $this->db->escape($id));

				$return['error'] = false;
				$return['msg'] = "Country deleted.";
			}
		}

		return json_encode($return);
	}

	public function addState(){
		if(!$this->request('name')){
			$return['error'] = true;
			$return['msg'] =  "Name is required.";

		}else if(!$this->request('country_id')){
			$return['error'] = true;
			$return['msg'] =  "Country is required.";

		}else{

			$this->db->table("states");
			$this->db->insertArray(array(
				"name" => $this->request('name'),
				"country_id" => $this->request('country_id'),
				"country_code" => $this->request('country_code'),
				"created_at" => 'NOW()',
				"updated_at" => 'NOW()',
			));
			$this->db->insert();

			$return['error'] = false;
			$return['msg'] = "Country added.";
			$return['url'] = url_for('/state');
		}

		return json_encode($return);
	}

	public function editState($id = 0){
		$this->db->query("SELECT * FROM `states` WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();

		if($key){
			$this->db->table("states");
			$this->db->updateArray(array(
				'name' => $this->request('name'),
				'country_id' => $this->request('country_id'),
				'country_code' => $this->request('country_code'),
				'updated_at' => 'NOW()',
                'status' => '1'
			));
			$this->db->whereArray(array(
				"id" => $id,
			));
			$this->db->update();

			$return['error'] = false;
			$return['msg'] = "State updated.";
			$return['url'] = url_for('/state');
			
		}else{
			$return['error'] = true;
			$return['msg'] =  "Invalid ID.";
		}

		return json_encode($return);
	}

	public function deleteState($id = 0){
		$selected = $this->request('selected') ? $this->request('selected') : array();

		if(!$selected && !$id){
			$return['error'] = true;
			$return['msg'] =  "No state selected";

		}else{

			if($selected){
				$this->db->queryOrDie("UPDATE states SET status = '999' WHERE id IN (" . implode(',', $selected) . ")");

				$return['error'] = false;
				$return['msg'] = "Selected state deleted.";
				
			}else{

				$this->db->queryOrDie("UPDATE states SET status = '999' WHERE id = " . $this->db->escape($id));

				$return['error'] = false;
				$return['msg'] = "State deleted.";
			}
		}

		return json_encode($return);
	}

	public function addCity(){
		if(!$this->request('name')){
			$return['error'] = true;
			$return['msg'] =  "City is required.";

		}else if(!$this->request('country_id')){
			$return['error'] = true;
			$return['msg'] =  "Country is required.";

		}else{

			$this->db->table("cities");
			$this->db->insertArray(array(
				'name' => $this->request('name'),
				'country_id' => $this->request('country_id'),
				'state_id' => $this->request('state_id'),
				'state_code' => $this->request('state_code'),
				'latitude' => $this->request('latitude'),
				'longitude' => $this->request('longitude'),
				'created_at' => 'NOW()',
				'updated_on' => 'NOW()'
			));
			$this->db->insert();

			$return['error'] = false;
			$return['msg'] = "City added.";
			$return['url'] = url_for('/city');
		}

		return json_encode($return);
	}

	public function editCity($id = 0){
		$this->db->query("SELECT * FROM `cities` WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();

		if($key){
			$this->db->table("cities");
			$this->db->updateArray(array(
				'name' => $this->request('name'),
				'country_id' => $this->request('country_id'),
				'state_id' => $this->request('state_id'),
				'state_code' => $this->request('state_code'),
				'latitude' => $this->request('latitude'),
				'longitude' => $this->request('longitude'),
				'updated_on' => 'NOW()',
                'status' => '1'
			));
			$this->db->whereArray(array(
				"id" => $id,
			));
			$this->db->update();

			$return['error'] = false;
			$return['msg'] = "City updated.";
			$return['url'] = url_for('/city');
			
		}else{
			$return['error'] = true;
			$return['msg'] =  "Invalid ID.";
		}

		return json_encode($return);
	}

	public function deleteCity($id = 0){
		$selected = $this->request('selected') ? $this->request('selected') : array();

		if(!$selected && !$id){
			$return['error'] = true;
			$return['msg'] =  "No city selected";

		}else{

			if($selected){
				$this->db->queryOrDie("UPDATE cities SET status = '999' WHERE id IN (" . implode(',', $selected) . ")");

				$return['error'] = false;
				$return['msg'] = "Selected cities deleted.";
				
			}else{

				$this->db->queryOrDie("UPDATE cities SET status = '999' WHERE id = " . $this->db->escape($id));

				$return['error'] = false;
				$return['msg'] = "City deleted.";
			}
		}

		return json_encode($return);
	}

	public function addIndustry(){
		if(!$this->request('title')){
			$return['error'] = true;
			$return['msg'] =  "Title is required.";

		}else{

			$this->db->table("industry");
			$this->db->insertArray(array(
				"parent" => $this->request('parent'),
				"title" => $this->request('title'),
				"sortby" => $this->request('sortby'),
				"status" => $this->request('status'),
			));
			$this->db->insert();

			$industry_id = $this->db->insertid();

			$lang = array_filter($this->request('lang'));

			foreach($lang as $language_id => $val){
				$this->db->table("industry_language_text");
				$this->db->insertArray(array(
					"industry_id" => $industry_id,
					"language_id" => $language_id,
					"title" => $val['title'],
				));
				$this->db->insert();
			}

			$return['error'] = false;
			$return['msg'] = "Industry added.";
			$return['url'] = url_for('/industry');
		}

		return json_encode($return);
	}

	public function editIndustry($id = 0){
		if($id == 0){
			$industry = $this->request('industry') ? $this->request('industry') : array();

			if($industry){
				$this->db->queryOrDie("TRUNCATE `industry_language_text`");

				foreach($industry as $industry_id => $lang){
					foreach($lang as $lang_id => $title){
						if($title){
							$this->db->table("industry_language_text");
							$this->db->insertArray(array(
								"industry_id" => $industry_id,
								"language_id" => $lang_id,
								"title" => $title,
							));
							$this->db->insert();
						}
					}
				}
			}

			$return['error'] = false;
			$return['msg'] =  "Industry language updated.";

		}else{

			$this->db->query("SELECT * FROM `industry` WHERE id = " . $this->db->escape($id));
			$key = $this->db->getSingleRow();

			if($key){
				$this->db->table("industry");
				$this->db->updateArray(array(
					"parent" => $this->request('parent'),
					"title" => $this->request('title'),
					"sortby" => $this->request('sortby'),
					"status" => $this->request('status'),
				));
				$this->db->whereArray(array(
					"id" => $id,
				));
				$this->db->update();

				$lang = array_filter($this->request('lang'));

				$this->db->table("industry_language_text");
				$this->db->whereArray(array(
					"industry_id" => $id,
				));
				$this->db->delete();

				foreach($lang as $language_id => $val){
					$this->db->table("industry_language_text");
					$this->db->insertArray(array(
						"industry_id" => $id,
						"language_id" => $language_id,
						"title" => $val['title'],
					));
					$this->db->insert();
				}

				$return['error'] = false;
				$return['msg'] = "Industry updated.";
				$return['url'] = url_for('/industry') . ($key['new'] ? '#pills-entered' : '');
				
			}else{
				$return['error'] = true;
				$return['msg'] =  "Invalid ID.";
			}
		}

		return json_encode($return);
	}

	public function deleteIndustry(){
		$selected = $this->request('selected') ? $this->request('selected') : array();

		if(!$selected){
			$return['error'] = true;
			$return['msg'] =  "No industry selected";

		}else{

			$this->db->queryOrDie("UPDATE industry SET status = '999' WHERE id IN (" . implode(',', $selected) . ")");

			$return['error'] = false;
			$return['msg'] = "Selected industry deleted.";
		}

		return json_encode($return);
	}

	public function addCategory(){
		if(!$this->request('title')){
			$return['error'] = true;
			$return['msg'] =  "Title is required.";

		}else{

			$this->db->table("task_category");
			$this->db->insertArray(array(
				"parent" => $this->request('parent'),
				"title" => $this->request('title'),
				"sortby" => $this->request('sortby'),
				"status" => $this->request('status'),
			));
			$this->db->insert();

			$category_id = $this->db->insertid();

			$lang = array_filter($this->request('lang'));

			$this->db->table("task_category_language_text");
			$this->db->whereArray(array(
				"category_id" => $category_id,
			));
			$this->db->delete();

			foreach($lang as $language_id => $val){
				$this->db->table("task_category_language_text");
				$this->db->insertArray(array(
					"category_id" => $category_id,
					"language_id" => $language_id,
					"title" => $val['title'],
				));
				$this->db->insert();
			}

			$return['error'] = false;
			$return['msg'] = "Category added.";
			$return['url'] = url_for('/category');
		}

		return json_encode($return);
	}

	public function addCategoryIntoExisting(){
		$parent_id = $this->request('parent_id') ? $this->request('parent_id') : 0;
		$selected = $this->request('selected') ? $this->request('selected') : array();

		if(!$selected){
			$return['error'] = true;
			$return['msg'] =  "No skill selected";

		}else{

			$this->db->queryOrDie("UPDATE task_category SET new = '0', parent = " . $this->db->escape($parent_id) . " WHERE id IN (" . implode(',', $selected) . ")");

			$return['error'] = false;
			$return['msg'] = "Category added to existing.";
			$return['url'] = url_for('/category#pills-entered');
		}

		return json_encode($return);
	}

	public function deleteCategory(){
		$selected = $this->request('selected') ? $this->request('selected') : array();

		if(!$selected){
			$return['error'] = true;
			$return['msg'] =  "No category selected";

		}else{

			$this->db->queryOrDie("UPDATE task_category SET status = '999' WHERE id IN (" . implode(',', $selected) . ")");

			$return['error'] = false;
			$return['msg'] = "Selected category deleted.";
		}

		return json_encode($return);
	}

	public function editCategory($id = 0){
		if($id == 0){
			$category = $this->request('category') ? $this->request('category') : array();

			if($category){
				$this->db->queryOrDie("TRUNCATE `task_category_language_text`");

				foreach($category as $category_id => $lang){
					foreach($lang as $lang_id => $title){
						if($title){
							$this->db->table("task_category_language_text");
							$this->db->insertArray(array(
								"category_id" => $category_id,
								"language_id" => $lang_id,
								"title" => $title,
							));
							$this->db->insert();
						}
					}
				}
			}

			$return['error'] = false;
			$return['msg'] =  "Category language updated.";

		}else{

			$this->db->query("SELECT * FROM `task_category` WHERE id = " . $this->db->escape($id));
			$key = $this->db->getSingleRow();

			if($key){
				$this->db->table("task_category");
				$this->db->updateArray(array(
					"parent" => $this->request('parent'),
					"title" => $this->request('title'),
					"sortby" => $this->request('sortby'),
					"status" => $this->request('status'),
				));
				$this->db->whereArray(array(
					"id" => $id,
				));
				$this->db->update();

				$lang = array_filter($this->request('lang'));

				$this->db->table("task_category_language_text");
				$this->db->whereArray(array(
					"category_id" => $id,
				));
				$this->db->delete();

				foreach($lang as $language_id => $val){
					$this->db->table("task_category_language_text");
					$this->db->insertArray(array(
						"category_id" => $id,
						"language_id" => $language_id,
						"title" => $val['title'],
					));
					$this->db->insert();
				}

				$return['error'] = false;
				$return['msg'] = "Category updated.";
				$return['url'] = url_for('/category') . ($key['new'] ? '#pills-entered' : '');
				
			}else{
				$return['error'] = true;
				$return['msg'] =  "Invalid ID.";
			}
		}

		return json_encode($return);
	}

	public function addSkill(){
		if(!$this->request('name')){
			$return['error'] = true;
			$return['msg'] =  "Name is required.";

		}else{

			$this->db->table("skills");
			$this->db->insertArray(array(
				"parent" => $this->request('parent'),
				"name" => $this->request('name'),
				"status" => $this->request('status'),
			));
			$this->db->insert();

			$skill_id = $this->db->insertid();

			$lang = array_filter($this->request('lang'));

			foreach($lang as $language_id => $val){
				$this->db->table("skill_language_text");
				$this->db->insertArray(array(
					"skill_id" => $skill_id,
					"language_id" => $language_id,
					"name" => $val['name'],
				));
				$this->db->insert();
			}

			$return['error'] = false;
			$return['msg'] = "Skill added.";
			$return['url'] = url_for('/skill');
		}

		return json_encode($return);
	}

	public function addskillIntoExisting(){
		$parent_id = $this->request('parent_id') ? $this->request('parent_id') : 0;
		$selected = $this->request('selected') ? $this->request('selected') : array();

		if(!$selected){
			$return['error'] = true;
			$return['msg'] =  "No skill selected";

		}else{

			$this->db->queryOrDie("UPDATE skills SET new = '0', parent = " . $this->db->escape($parent_id) . " WHERE id IN (" . implode(',', $selected) . ")");

			$return['error'] = false;
			$return['msg'] = "Skill added to existing.";
			$return['url'] = url_for('/skill#pills-entered');
		}

		return json_encode($return);
	}

	public function editSkill($id = 0){
		if($id == 0){
			$skill = $this->request('skill') ? $this->request('skill') : array();

			if($skill){
				$this->db->queryOrDie("TRUNCATE `skill_language_text`");

				foreach($skill as $skill_id => $lang){
					foreach($lang as $lang_id => $name){
						if($name){
							$this->db->table("skill_language_text");
							$this->db->insertArray(array(
								"skill_id" => $skill_id,
								"language_id" => $lang_id,
								"name" => $name,
							));
							$this->db->insert();
						}
					}
				}
			}

			$return['error'] = false;
			$return['msg'] =  "Skill language updated.";

		}else{

			$this->db->query("SELECT * FROM `skills` WHERE id = " . $this->db->escape($id));
			$key = $this->db->getSingleRow();

			if($key){
				$this->db->table("skills");
				$this->db->updateArray(array(
					"parent" => $this->request('parent'),
					"name" => $this->request('name'),
					"status" => $this->request('status'),
				));
				$this->db->whereArray(array(
					"id" => $id,
				));
				$this->db->update();

				$lang = array_filter($this->request('lang'));

				$this->db->table("skill_language_text");
				$this->db->whereArray(array(
					"skill_id" => $id,
				));
				$this->db->delete();

				foreach($lang as $language_id => $val){
					$this->db->table("skill_language_text");
					$this->db->insertArray(array(
						"skill_id" => $id,
						"language_id" => $language_id,
						"name" => $val['name'],
					));
					$this->db->insert();
				}

				$return['error'] = false;
				$return['msg'] = "Skill updated.";
				$return['url'] = url_for('/skill') . ($key['new'] ? '#pills-entered' : '');
				
			}else{
				$return['error'] = true;
				$return['msg'] =  "Invalid ID.";
			}
		}

		return json_encode($return);
	}

	public function deleteSkill(){
		$selected = $this->request('selected') ? $this->request('selected') : array();

		if(!$selected){
			$return['error'] = true;
			$return['msg'] =  "No skill selected";

		}else{

			$this->db->queryOrDie("UPDATE skills SET status = '999' WHERE id IN (" . implode(',', $selected) . ")");

			$return['error'] = false;
			$return['msg'] = "Selected skill deleted.";
		}

		return json_encode($return);
	}

	public function addLanguage(){
		$this->db->query("SELECT * FROM language WHERE status < 999 AND title = " . $this->db->escape($this->request('title')));
		$key = $this->db->getSingleRow();

		if(!$key){
			$this->db->query("SELECT * FROM `language` WHERE status < 999 AND id != " . $this->db->escape($this->request('id')) . " AND code = " . $this->db->escape(strtoupper($this->request('code'))));
			$codeExists = $this->db->getSingleRow();

			if(!$codeExists){
				$this->db->table("language");
				$this->db->insertArray(array(
					"title" => $this->request('title'),
					"code" => strtoupper($this->request('code')),
					"status" => '1',
				));
				$this->db->insert();

				$return['error'] = false;
				$return['msg'] =  "New language added.";

			}else{
				$return['error'] = true;
				$return['msg'] =  "Language code " . strtoupper($this->request('code')) . " already exists.";
			}
			
		}else{
			$return['error'] = true;
			$return['msg'] =  "Language already exists.";
		}

		return json_encode($return);
	}

	public function editLanguage(){
		$this->db->query("SELECT * FROM `language` WHERE status < 999 AND id = " . $this->db->escape($this->request('id')));
		$key = $this->db->getSingleRow();

		if($key){
			$this->db->query("SELECT * FROM `language` WHERE status < 999 AND id != " . $this->db->escape($this->request('id')) . " AND code = " . $this->db->escape(strtoupper($this->request('code'))));
			$codeExists = $this->db->getSingleRow();

			if(!$codeExists){
				$this->db->table("language");
				$this->db->updateArray(array(
					"title" => $this->request('title'),
					"code" => strtoupper($this->request('code')),
				));
				$this->db->whereArray(array(
					"id" => $this->request('id'),
				));
				$this->db->update();

				$return['error'] = false;
				$return['msg'] = "Language updated.";
				$return['id'] = $key['id'];
				$return['title'] = $this->request('title');
				$return['code'] = strtoupper($this->request('code'));

			}else{
				$return['error'] = true;
				$return['msg'] =  "Language code " . strtoupper($this->request('code')) . " already exists.";
			}
			
		}else{
			$return['error'] = true;
			$return['msg'] =  "Invalid ID.";
		}

		return json_encode($return);
	}

	public function editLanguageText($id = 0){
		$this->db->query("SELECT * FROM `language` WHERE status < 999 AND id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();

		if($key){
			$lang = array_filter($this->request('lang'));

			$this->db->table("language_text");
			$this->db->whereArray(array(
				"language_id" => $id,
			));
			$this->db->delete();
			
			foreach($lang as $var => $text){
				$this->db->table("language_text");
				$this->db->insertArray(array(
					"language_id" => $id,
					"var" => $var,
					"text" => $text
				));
				$this->db->insert();
			}

			$return['error'] = false;
			$return['msg'] = "Language translation updated.";
            create_translation_files($id == 1 ? 'en' : 'bm', true);
		}else{
			$return['error'] = true;
			$return['msg'] =  "Invalid ID.";
		}

		return json_encode($return);
	}

	////////////////////////////////////////
	public function addInterest(){
		if(!$this->request('title')){
			$return['error'] = true;
			$return['msg'] =  "Title is required.";

		}else{

			$this->db->table("interest");
			$this->db->insertArray(array(
				"parent" => $this->request('parent'),
				"title" => $this->request('title'),
				"sortby" => $this->request('sortby'),
				"status" => $this->request('status'),
			));
			$this->db->insert();

			$interest_id = $this->db->insertid();

			$lang = array_filter($this->request('lang'));

			foreach($lang as $language_id => $val){
				$this->db->table("interest_language_text");
				$this->db->insertArray(array(
					"interest_id" => $interest_id,
					"language_id" => $language_id,
					"title" => $val['title'],
				));
				$this->db->insert();
			}

			$return['error'] = false;
			$return['msg'] = "Interest added.";
			$return['url'] = url_for('/interest');
		}

		return json_encode($return);
	}

	public function editInterest($id = 0){
		if($id == 0){
			$interest = $this->request('interest') ? $this->request('interest') : array();

			if($interest){
				$this->db->queryOrDie("TRUNCATE `interest_language_text`");

				foreach($interest as $interest_id => $lang){
					foreach($lang as $lang_id => $title){
						if($title){
							$this->db->table("interest_language_text");
							$this->db->insertArray(array(
								"interest_id" => $interest_id,
								"language_id" => $lang_id,
								"title" => $title,
							));
							$this->db->insert();
						}
					}
				}
			}

			$return['error'] = false;
			$return['msg'] =  "Interest language updated.";

		}else{

			$this->db->query("SELECT * FROM `interest` WHERE id = " . $this->db->escape($id));
			$key = $this->db->getSingleRow();

			if($key){
				$this->db->table("interest");
				$this->db->updateArray(array(
					"parent" => $this->request('parent'),
					"title" => $this->request('title'),
					"sortby" => $this->request('sortby'),
					"status" => $this->request('status'),
				));
				$this->db->whereArray(array(
					"id" => $id,
				));
				$this->db->update();

				$lang = array_filter($this->request('lang'));

				$this->db->table("interest_language_text");
				$this->db->whereArray(array(
					"interest_id" => $id,
				));
				$this->db->delete();

				foreach($lang as $language_id => $val){
					$this->db->table("interest_language_text");
					$this->db->insertArray(array(
						"interest_id" => $id,
						"language_id" => $language_id,
						"title" => $val['title'],
					));
					$this->db->insert();
				}

				$return['error'] = false;
				$return['msg'] = "Interest updated.";
				$return['url'] = url_for('/interest') . ($key['new'] ? '#pills-entered' : '');
				
			}else{
				$return['error'] = true;
				$return['msg'] =  "Invalid ID.";
			}
		}

		return json_encode($return);
	}

	public function deleteInterest(){
		$selected = $this->request('selected') ? $this->request('selected') : array();

		if(!$selected){
			$return['error'] = true;
			$return['msg'] =  "No interest selected";

		}else{

			$this->db->queryOrDie("UPDATE interest SET status = '999' WHERE id IN (" . implode(',', $selected) . ")");

			$return['error'] = false;
			$return['msg'] = "Selected interest deleted.";
		}

		return json_encode($return);
	}

	public function updateSubscription(){
		$subscriptions = $this->request('subscription');

		if($subscriptions){
			foreach($subscriptions as $id => $subscription){
				$this->db->table("subscription");
				$this->db->updateArray(array(
					"title" => $subscription['title'],
					"description" => $subscription['description'],
					"price" => number_format($subscription['price'], 2, '.', ''),
					"price_promo" => number_format($subscription['price_promo'], 2, '.', ''),
					"price_promo_status" => !empty($subscription['price_promo_status']) ? '1' : '0',
					"max_post" => $subscription['title'],
					"billing_access" => !empty($subscription['billing_access']) ? '1' : '0',
					"support_email" => !empty($subscription['support_email']) ? '1' : '0',
					"support_account_manager" => !empty($subscription['support_account_manager']) ? '1' : '0',
					"title_2" => $subscription['title_2'],
					"description_2" => $subscription['description_2'],
					"features_2" => $subscription['features_2'],
					"status" => !empty($subscription['status']) ? '1' : '0',
				));
				$this->db->whereArray(array(
					"id" => $id,
				));
				$this->db->update();

				$this->db->table("subscription_language_text");
				$this->db->whereArray(array(
					"subscription_id" => $id,
				));
				$this->db->delete();

				if(!empty($subscription['lang'])){
					$lang = array_filter($subscription['lang']);

					foreach($lang as $language_id => $val){
						$this->db->table("subscription_language_text");
						$this->db->insertArray(array(
							"subscription_id" => $id,
							"language_id" => $language_id,
							"title" => $val['title'],
							"description" => $val['description'],
							"title_2" => $val['title_2'],
							"description_2" => $val['description_2'],
							"features_2" => $val['features_2'],
						));
						$this->db->insert();
					}
				}
			}
		}

		$return['error'] = false;
		$return['msg'] = "Subscription updated.";

		return json($return);
	}

    public function individualDetailUpdate(){
        $return['error'] = false;
        $return['msg'] = "Saved";
        $member = request('user');
		$preference = request('preference');
		$preference['survey'] = !empty($preference['survey']) ? 1 : 0;

        if( empty($member['password']) || empty($member['password_confirm']) ){
            unset($member['password'], $member['password_confirm']);
        }else{
            if($member['password'] === $member['password_confirm']){
                $member['password'] = encryptPassword($member['password']);
                unset($member['password_confirm']);
            }else{
                unset($member['password'], $member['password_confirm']);
                $return['msg'] = "Password and Confirm Password do not match!";
                $return['warning'] = true;
            }
        }

        $skills = $member['skills'];
        $skill_ids = [];
        if( $skills ) {
            $parts     = explode(',', $skills);
            $skill_ids = array_filter($parts, function ($skill) {
                return stripos($skill, ':') === false;
            });
            $new_parts = array_diff($parts, $skill_ids);
            $new       = array_values(array_map(function ($skill) {
                list($key, $value) = explode(':', $skill, 2);
                return sanitize($value);
            }, $new_parts));

            if (!empty($new)) {
                $new_ids = [];
                foreach ($new as $skill) {
                    $check_for_an_existing_skills = $this->db->query("SELECT * FROM `skills` WHERE `name` LIKE '{$skill}' LIMIT 1");
                    $row                          = $this->db->getSingleRow();
                    if (empty($row)) {
                        $this->db->table('skills');
                        $this->db->insertArray([
                            'name'   => $skill,
                            'new'    => '1',
                            'status' => '0'
                        ]);
                        $this->db->insert();
                        array_push($new_ids, $this->db->insertid());
                    } else {
                        array_push($new_ids, $row['id']);
                    }
                }
                $skill_ids = array_merge($skill_ids, $new_ids);
            }
        }
		$member['skills'] = !empty($skill_ids) ? implode(',', $skill_ids) : '';
		$member['status_date'] = !empty($member['status_date']) ? $member['status_date'] : '';
		$member['status_notify'] = !empty($member['status_notify']) ? 1 : 0;
        $keys = [];

        foreach ($member as $key => $value) {
            if($key == 'dob') $value = $value ? date('Y-m-d', strtotime(str_replace('/', '-', $value))) : '';
            if($key == 'contact_method') $value = implode(',', $value);
            $keys[$key] = trim($value);
        }

        $this->db->table('members');
        $this->db->updateArray($keys);
        $this->db->whereArray([
            "id" => $member['id'],
        ]);
        $this->db->update();

        $this->db->queryOrDie("DELETE FROM `member_docs` WHERE `member_id` = " . $this->db->escape($member['id']) );
        $docs = $this->request('docs') ? array_filter($this->request('docs')) : '';

        if($docs){
            foreach($docs as $type => $srcs){
                foreach($srcs as $src){
                    $this->db->table('member_docs');
                    $this->db->insertArray(array(
                        "member_id" => $member['id'],
                        "type" => $type,
                        "src" => $src
                    ));
                    $this->db->insert();
                }
            }
        }

        if($preference){
            $this->db->table('preference');
            $this->db->updateArray($preference);
            $this->db->whereArray(array(
                "member_id" => $member['id'],
            ));
            $this->db->update();
        }

        return json($return);
    }

	public function companyDetailUpdate($id = 0){
		$photos = $this->request('photos') ? array_filter($this->request('photos')) : array();
		$validEmail = filter_var(strtolower($this->request('company_email')), FILTER_VALIDATE_EMAIL);

		if(!$this->request('photo')){
			$return['error'] = true;
			$return['msg'] = "Profile Avatar/Photo is required.";

		}else
		if(!$this->request('mobile_number')){
			$return['error'] = true;
			$return['msg'] = "Contact No. Mobile is required.";

		}else if(!$this->request('contact_number')){
			$return['error'] = true;
			$return['msg'] = "Contact No. Phone is required.";

		}else if(!$validEmail){
			$return['error'] = true;
			$return['msg'] = "Invalid Company Email!";
			
		}else if(!$this->request('company_phone')){
			$return['error'] = true;
			$return['msg'] = "Company No. Phone is required.";

		}else{

			$member['status_date'] = !empty($member['status_date']) ? $member['status_date'] : '';
			$member['status_notify'] = !empty($member['status_notify']) ? 1 : 0;

			$this->db->table('members');
			$this->db->updateArray(array(
				"firstname" => $this->request('firstname'),
				"lastname" => $this->request('lastname'),
				"nric" => $this->request('nric'),
				"contact_number" => $this->request('contact_number'),
				"mobile_number" => $this->request('mobile_number'),
				"address1" => $this->request('address1'),
				"address2" => $this->request('address2'),
				"zip" => $this->request('zip'),
				"city" => $this->request('city'),
				"country" => $this->request('country'),
				"state" => $this->request('state'),
				"nationality" => $this->request('nationality'),
				"photo" => $this->request('photo'),
				"status" => $this->request('status'),
				"status_date" => $this->request('status_date') ? $this->request('status_date') : '',
				"status_notify" => $this->request('status_notify') ? 1 : 0,
			));
			$this->db->whereArray(array(
				"id" => $id, 
			));
			$this->db->update();

			$this->db->table('company_details');
			$this->db->updateArray(array(
				"name" => $this->request('company_name'),
				"reg_num" => $this->request('reg_num'),
				"email" => $this->request('company_email'),
				"company_phone" => $this->request('company_phone'),
				"about_us" => $this->request('about_us'),
				"industry" => $this->request('industry'),
				"photos" => serialize($photos),
			));
			$this->db->whereArray(array(
				"member_id" => $id, 
			));
			$this->db->update();

			$preference = request('preference');
			$preference['survey'] = !empty($preference['survey']) ? 1 : 0;

			if($preference){
				$this->db->table('preference');
				$this->db->updateArray($preference);
				$this->db->whereArray(array(
					"member_id" => $id,
				));
				$this->db->update();
			}

			/*
			$preference = $this->request('preference');

            if(strlen($preference['acc_num']) < 6){
                $return['error'] = true;
                $return['msg'] = 'Your Bank Account number is invalid.';

            }else {

                if($preference){
                    $this->db->table('preference');
                    $this->db->updateArray($preference);
                    $this->db->whereArray(array(
                        "member_id" => $id,
                    ));
                    $this->db->update();
                }
			}
			*/

			$return['error'] = false;
			$return['msg']   = "Saved";
		}

		return json_encode($return);
	}

    public function individualEmploymentUpdate(){
        $employment = $this->request('employment');
        $employment['industry'] = $employment['industry'] ? $employment['industry'] : '';
        $employment['currently_working'] = !empty($employment['currently_working']) ? 1 : 0;
        $employment['date_end'] = !empty($employment['date_end']) ? (!$employment['currently_working'] ? $employment['date_end'] : '') : '';
        $employment['date_start'] = date('Y-m-d', strtotime(str_replace('/', '-', $employment['date_start'])));
        $employment['date_end'] = $employment['date_end'] ? date('Y-m-d', strtotime(str_replace('/', '-', $employment['date_end']))) : '';

        if(!$employment['company_name']){
            $return['error'] = true;
            $return['msg'] = 'Company Name is required.';

        }else if(!$employment['position']){
            $return['error'] = true;
            $return['msg'] = 'Position is required.';

        }else if(!$employment['date_start']){
            $return['error'] = true;
            $return['msg'] = 'Start Date is required.';

        }else if(!$employment['currently_working'] && !$employment['date_end']){
            $return['error'] = true;
            $return['msg'] = 'End Date is required.';

        }else if(!$employment['department']){
            $return['error'] = true;
            $return['msg'] = 'Department is required.';

        }else if(!$employment['industry']){
            $return['error'] = true;
            $return['msg'] = 'Industry is required.';

        }else if(!$employment['job_desc']){
            $return['error'] = true;
            $return['msg'] = 'Job Description is required.';

        }else if(!$employment['job_responsibilities']){
            $return['error'] = true;
            $return['msg'] = 'Job Responsibilities is required.';

        }else if(!$employment['country']){
            $return['error'] = true;
            $return['msg'] = 'Country is required.';

        }else if(!$employment['state']){
            $return['error'] = true;
            $return['msg'] = 'State is required.';

        }else if(!$employment['remuneration']){
            $return['error'] = true;
            $return['msg'] = 'Remuneration is required.';

        }else{

            $this->db->table('employment_details');
            $this->db->updateArray(array(
                "company_name" => $employment['company_name'],
                "position" => $employment['position'],
                "date_start" => $employment['date_start'],
                "date_end" => $employment['date_end'],
                "currently_working" => $employment['currently_working'],
                "department" => $employment['department'],
                "industry" => $employment['industry'],
                "job_desc" => $employment['job_desc'],
                "job_responsibilities" => $employment['job_responsibilities'],
                "country" => $employment['country'],
                "state" => $employment['state'],
                "remuneration" => $employment['remuneration'],
            ));
            $this->db->whereArray(array(
                "id" => $employment['id'],
                "member_id" => $employment['user_id'],
            ));
            $this->db->update();

            $html = '
			<div class="row-form-data" id="emp-' . $employment['id'] . '">
				<div class="talent-details-workxp-row">
					<div class="talent-details-workxp-compname">' . $employment['company_name'] . '</div>
					<div class="talent-details-workxp-pos">' . $employment['position'] . ', ' .  $this->cms->getIndustryName($employment['industry']) . '</div>
					<div class="talent-details-workxp-pos-duration">
						<div class="talent-details-workxp-location">' . ($employment['state'] ? $this->cms->getStateName($employment['state']) . ', ' : '') . $this->cms->getCountryName($employment['country']) . '</div>
						<div class="talent-details-workxp-duration">' . dateRangeToDays($employment['date_start'], $employment['date_end']) . '</div>
					</div>
					<div class="talent-details-workxp-resp">
						<span class="talent-details-workxp-list-lbl"><b>Responsibilities</b></span>
						<div>' . nl2br($employment['job_responsibilities']) . '</div>
					</div>
					<div class="talent-details-workxp-resp">
						<span class="talent-details-workxp-list-lbl"><b>Description</b></span>
						<div>' . nl2br($employment['job_desc']) . '</div>
					</div>
					<div class="edit-in-modal">
                        <a href="#" class="edit-employment" data-data="' . htmlspecialchars(json_encode($employment)) . '">Edit</a>
                        <a href="#" class="remove-employment" data-id="' . $employment['id'] . '"><i class="far fa-trash-alt"></i></a>
                    </div>
				</div>';

            $return['msg'] = "Employment information updated.";
            $return['error'] = false;
            $return['html'] = $html;
            $return['id'] = $employment['id'];
        }

        return json($return);
    }

    public function individualEducationUpdate(){
        $education = $this->request('education');
        $education['highest_edu_level'] = !empty($education['highest_edu_level']) ? $education['highest_edu_level'] : '';
        $education['field'] = !empty($education['field']) ? $education['field'] : '';

        if(!$education['edu_institution']){
            $return['error'] = true;
            $return['msg'] = 'Education Institution is required.';

        }else if(!$education['highest_edu_level']){
            $return['error'] = true;
            $return['msg'] = 'Highest Education Level is required.';

        }else if(!$education['field']){
            $return['error'] = true;
            $return['msg'] = 'Field of Study is required.';

        }else if(!$education['grad_year']){
            $return['error'] = true;
            $return['msg'] = 'Graduation Year is required.';

        }else if(!$education['major']){
            $return['error'] = true;
            $return['msg'] = 'Major is required.';

        }else if(!$education['grade']){
            $return['error'] = true;
            $return['msg'] = 'Grade is required.';

        }else if(!$education['country']){
            $return['error'] = true;
            $return['msg'] = 'Country is required.';

        }else if(!$education['state']){
            $return['error'] = true;
            $return['msg'] = 'State is required.';

        }else{
            $this->db->table('education');
            $this->db->updateArray(array(
                "edu_institution" => $education['edu_institution'],
                "highest_edu_level" => $education['highest_edu_level'],
                "field" => $education['field'],
                "grad_year" => $education['grad_year'],
                "major" => $education['major'],
                "grade" => $education['grade'],
                "country" => $education['country'],
                "state" => $education['state'],
                "member_id" => $this->user->id,
            ));
            $this->db->whereArray(array(
                "id" => $education['id'],
                "member_id" => $education['user_id'],
            ));
            $this->db->update();

            $html = '
				<div class="row-form-data" id="edu-' . $education['id'] . '">
					<div class="talent-details-edu-row">
						<div class="talent-details-edu-insname">' . $education['edu_institution'] . '</div>
						<div class="talent-details-edu-level-fieldstudy-grade">
							<div class="talent-details-edu-level">' . $education['highest_edu_level'] . '</div>
							<div class="talent-details-edu-fieldstudy">' . $education['field'] . '</div>
							<div class="talent-details-edu-fieldstudy">' . $education['major'] . '</div>
							<div class="talent-details-edu-grade">' . $education['grade'] . '</div>
						</div>
						<div class="talent-details-edu-location-year">
							<div class="talent-details-edu-location">' . ($education['state'] ? $this->cms->getStateName($education['state']) . ', '  : '') . $this->cms->getCountryName($education['country']) . '</div>
							<div class="talent-details-edu-year">' . $education['grad_year'] . '</div>
						</div>
						<div class="edit-in-modal">
                            <a href="#" class="edit-education" data-data="' . htmlspecialchars(json_encode($education)) . '">Edit</a>
                            <a href="#" class="remove-education" data-id="' . $education['id'] . '"><i class="far fa-trash-alt"></i></a>
                        </div>
					</div>
				</div>';

            $return['msg'] = "Education information updated.";
            $return['error'] = false;
            $return['html'] = $html;
            $return['id'] = $education['id'];
        }

        return json($return);
    }
		
	public function profile(){

		if(!$this->request('name') || !$this->request('contact') || !$this->request('email')){
			$return['error'] = true;
			$return['msg'] = "
			All the information below required. 
			<ul>
			  <li>Full Name</li>
			  <li>Contact Number</li>
			  <li>Email Address</li>
			</ul>"; 
					
		}else if($this->request('Password') != $this->request('ConfirmPassword')){
			$return['error'] = true;
			$return['msg'] = "Password are not match."; 
					
		}else{
			if(!$this->request('Password')){
				$password = $_SESSION[BACKEND_PREFIX.'ADMIN_PASSWORD'];
			}else{
				$password = md5($this->request('Password'));
			}
				
			$this->db->table("user_admins");
			$this->db->updateArray(array(
				"admin_name" => $this->request('name'),
				"admin_email" => $this->request('email'),
				"admin_contact" => $this->request('contact'),
				"admin_password" => $password,
			));
			$this->db->whereArray(array(
				"admin_id" => $_SESSION[BACKEND_PREFIX.'ADMIN_ID'],
			));
			$this->db->update();
					
			$return['error'] = false;
			$return['msg'] = "Profile updated"; 
		}
			
		return json($return);
	}
	
	public function admin(){
		$act = $this->request('act');
		
		if($act == 'delete'){
			if(empty($this->adminRole) || $this->adminRole != '1'){
				$return['error'] = true;
				$return['msg'] = "
				You are not permission to add new administrator account for this system. <br/> 
				It must be done by super admin. <br/> 
				Please contact your super admin to do this."; 
				
			}else{
				
				$this->db->table("user_admins");
				$this->db->updateArray(array(
					"admin_status" => "999",
				));
				$this->db->whereArray(array(
					"admin_id" => $this->request('id'),
				));
				$this->db->update();
					
				$return['error'] = false;
				$return['msg'] = "Administrator account deleted from system."; 
				
			}
			return json($return);
		}
	}
	
	public function adminAdd(){
		$this->db->query("SELECT * FROM user_admins WHERE admin_username = ".$this->db->escape($this->request('admin_username'))."");
		$admin = $this->db->getSingleRow();
		
			if(!$admin){
				if(!$this->request('admin_username') || !$this->request('admin_name') || !$this->request('password') || !$this->request('confirmPassword') || !$this->request('admin_contact') || !$this->request('admin_email')){
					$return['error'] = true;
					$return['msg'] = "
					All the information below required. 
					<ul>
					  <li>Username</li>
					  <li>Password</li>
					  <li>Confirm Passsword</li>
					  <li>Full Name</li>
					  <li>Phone Number</li>
					  <li>Email Address</li>
					</ul>"; 
					
				}else if($this->request('password') != $this->request('confirmPassword')){
					$return['error'] = true;
					$return['msg'] = "Confirm password does not match."; 
					
				}else{
					$this->db->table("user_admins");
					$this->db->insertArray(array(
						"admin_name" => $this->request('admin_name'),
						"admin_email" => $this->request('admin_email'),
						"admin_contact" => $this->request('admin_contact'),
						"admin_username" => $this->request('admin_username'),
						"admin_password" => md5($this->request('password')),
						"admin_status" => "1",
						"admin_role" => $this->request('admin_role'),
						"admin_registered" => "NOW()",
						"admin_last_activity" => "NOW()",
					));
					$this->db->insert();
					
					$return['error'] = false;
					$return['msg'] = "New admin created."; 
					$return['url'] = url_for('/admin');
				}
			
			}else{
				$return['error'] = true;
				$return['msg'] = "Username <strong>" . $this->request('username') . "</strong> already exists.<br>Please choose another."; 
			}

			return json($return);
	}
	
	public function adminEdit($id){

			$this->db->query("SELECT * FROM user_admins WHERE admin_id = ".$this->db->escape($id));
			$admin = $this->db->getSingleRow();
			
			if(!$admin){
				$return['error'] = true;
				$return['msg'] = "Invalid ID!"; 
			 
			}else if(!$this->request('admin_name') || !$this->request('admin_contact') || !$this->request('admin_email')){
				$return['error'] = true;
				$return['msg'] = "
					All the information below required. 
					<ul>
					  <li>Password</li>
					  <li>Confirm Passsword</li>
					  <li>Full Name</li>
					  <li>Phone Number</li>
					  <li>Email Address</li>
					</ul>"; 
					
			}else if($this->request('password') != $this->request('confirmPassword')){
				$return['error'] = true;
				$return['msg'] = "Confirm password does not match."; 
					
			}else{
				if(!$this->request('password')){
					$password = $admin['admin_password'];
				}else{
					$password = md5($this->request('password'));
				}
				
				$this->db->table("user_admins");
				$this->db->updateArray(array(
					"admin_name" => $this->request('admin_name'),
					"admin_email" => $this->request('admin_email'),
					"admin_contact" => $this->request('admin_contact'),
					"admin_password" => $password,
					"admin_status" => $this->request('admin_status'),
					"admin_role" => $this->request('admin_role'),
				));
				$this->db->whereArray(array(
					"admin_id" => $id,
				));
				$this->db->update();
					
				$return['error'] = false;
				$return['msg'] = "Admin updated."; 
			}

			return json($return);
	}

	public function adminDelete(){
		$return['error'] = true;
		$return['msg'] = "Invalid ID!"; 

		$id = $this->request('id') ? $this->request('id') : 0;

		$this->db->query("SELECT * FROM user_admins WHERE admin_id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();

		if($key){
			if($key['admin_id'] == '1'){
				$return['msg'] = "Root admin cannot be removed."; 

			}else{
				$this->db->table("user_admins");
				$this->db->whereArray(array(
					"admin_id" => $id,
				));
				$this->db->delete();

				$return['error'] = false;
				$return['msg'] = "Admin deleted."; 
			}
		}

		return json($return);
	}

	public function adminRoleAdd(){
		$permissions = $this->request('permissions') ? array_filter($this->request('permissions')) : $this->request('permissions');
		
		if(!$this->request('title')){
			$return['error'] = true;
			$return['msg'] = "Title is required!"; 

		}else if(!$permissions){
			$return['error'] = true;
			$return['msg'] = "Please select at least 1 permission!"; 

		}else{
			$this->db->table("user_admin_role");
			$this->db->insertArray(array(
				"title" => $this->request('title'),
				"permissions" => implode(',', $permissions),
			));
			$this->db->insert();
					
			$return['error'] = false;
			$return['msg'] = "New admin role created."; 
			$return['url'] = url_for('/admin') . '#pills-role';
		}
		return json($return);
	}
	
	public function adminRoleEdit($id = 0){
		$return['error'] = true;
		$return['msg'] = "Invalid ID!"; 

		$this->db->query("SELECT * FROM user_admin_role WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();

		if($key){
			$permissions = $this->request('permissions') ? array_filter($this->request('permissions')) : array();

			if(!$this->request('title')){
				$return['error'] = true;
				$return['msg'] = "Title is required!"; 

			}else if(!$permissions){
				$return['error'] = true;
				$return['msg'] = "Please select at least 1 permission!"; 
							
			}else{

				$this->db->table("user_admin_role");
				$this->db->updateArray(array(
					"title" => $this->request('title'),
					"permissions" => implode(',', $permissions),
				));
				$this->db->whereArray(array(
					"id" => $id,
				));
				$this->db->update();

				$return['error'] = false;
				$return['msg'] = "Admin role updated."; 
			}
		}

		return json($return);
	}

	public function adminRoleDelete(){
		$return['error'] = true;
		$return['msg'] = "Invalid ID!"; 

		$id = $this->request('id') ? $this->request('id') : 0;

		$this->db->query("SELECT * FROM user_admin_role WHERE id = " . $this->db->escape($id));
		$key = $this->db->getSingleRow();

		if($key){
			$this->db->table("user_admin_role");
			$this->db->whereArray(array(
				"id" => $id,
			));
			$this->db->delete();

			$this->db->table("user_admins");
			$this->db->updateArray(array(
				"admin_role" => '0',
			));
			$this->db->whereArray(array(
				"admin_role" => $id,
			));
			$this->db->update();

			$return['error'] = false;
			$return['msg'] = "Admin role deleted."; 
		}

		return json($return);
	}
	
	public function systemSettings(){
		if(!empty($this->posts)){
			foreach($this->posts as $name => $value){
				$this->db->table("settings");
				$this->db->updateArray(array(
					"value" => $value
				));
				$this->db->whereArray(array(
					"name" => $name,
				));
				$this->db->update();
			}
		}
					
		$return['error'] = false;
		$return['msg'] = "Systen settings updated."; 

		return json($return);
	}
	
	public function sliderUpdate(){
		
		$slider = $this->request('slide') ? array_filter($this->request('slide')) : array();
		
		$this->db->table("settings");
		$this->db->updateArray(array(
			"value" => serialize($slider),
		));
		$this->db->whereArray(array(
			"name" => "slider",
		));
		$this->db->update();
					
		$return['error'] = false;
		$return['msg'] = "Slider updated."; 

		return json($return);
	}
	
	public function pageUpdate($id){
		if(!empty($id)){
			$this->db->table("pages");
			$this->db->updateArray(array(
				"title" => $this->request('title'),
				"content" => $this->request('content'),
			));
			$this->db->whereArray(array(
				"id" => $id,
			));
			$this->db->update();
							
			$return['error'] = false;
			$return['msg'] = "Page updated."; 
			
		}else{
			$return['error'] = true;
			$return['msg'] = "Invalid ID."; 
		}
		return json($return);
	}
	
	public function memberUpdate($id){
		if(!empty($id)){
			
			$this->db->query("SELECT * FROM members WHERE id != " . $this->db->escape($id) . " AND email = " . $this->db->escape(strtolower($this->request('email'))));
			$emailExists = $this->db->getSingleRow();
			
			if($emailExists){
				$return['error'] = true;
				$return['msg'] = "Email <b>" . strtolower($this->request('email')) . "</b> already in used.<br>Please choose another.";
				die(json($return));
				
			}else{
				$this->db->table("members");
				$this->db->updateArray(array(
					"email" => strtolower($this->request('email')),
				));
				$this->db->whereArray(array(
					"id" => $id,
				));
				$this->db->update();
			}
			
			if($this->request('password')){
				if($this->request('password') != $this->request('password2')){
					$return['error'] = true;
					$return['msg'] = "Confirm password does not match.";
					die(json($return));
					
				}else{
					$this->db->table("members");
					$this->db->updateArray(array(
						"password" => encryptPassword($this->request('password')),
					));
					$this->db->whereArray(array(
						"id" => $id,
					));
					$this->db->update();
				}
			}
			
			$this->db->table("members");
			$this->db->updateArray(array(
				"firstname" => $this->request('firstname'),
				"lastname" => $this->request('lastname'),
				"contact" => $this->request('contact'),
				"address1" => $this->request('address1'),
				"address2" => $this->request('address2'),
				"zip" => $this->request('zip'),
				"city" => $this->request('city'),
				"country" => $this->request('country'),
				"state" => $this->request('state'),
				"status" => $this->request('status'),
			));
			$this->db->whereArray(array(
				"id" => $id,
			));
			$this->db->update();
										
			$return['error'] = false;
			$return['msg'] = "Member updated."; 
			
		}else{
			$return['error'] = true;
			$return['msg'] = "Invalid ID."; 
		}
		return json($return);
	}
	
	public function memberInsert(){
			
		$this->db->query("SELECT * FROM members WHERE email = " . $this->db->escape(strtolower($this->request('email'))));
		$emailExists = $this->db->getSingleRow();
		
		if($emailExists){
			$return['error'] = true;
			$return['msg'] = "Email <b>" . strtolower($this->request('email')) . "</b> already in used.<br>Please choose another.";
			
		}else if($this->request('password') != $this->request('password2')){
			$return['error'] = true;
			$return['msg'] = "Confirm password does not match.";
		}else{
		
			$this->db->table("members");
			$this->db->insertArray(array(
				"email" => strtolower($this->request('email')),
				"password" => encryptPassword($this->request('password')),
				"firstname" => $this->request('firstname'),
				"lastname" => $this->request('lastname'),
				"contact" => $this->request('contact'),
				"address1" => $this->request('address1'),
				"address2" => $this->request('address2'),
				"zip" => $this->request('zip'),
				"city" => $this->request('city'),
				"country" => $this->request('country'),
				"state" => $this->request('state'),
				"status" => $this->request('status'),
			));
			$this->db->insert();
							
			$return['error'] = false;
			$return['msg'] = "New member inserted."; 
		}
		return json($return);
	}

	public function categoryUpdate($id){
		if(!empty($id)){				
			$images = $this->request('images') ? array_filter($this->request('images')) : array();
			
			$this->db->table("category");
			$this->db->updateArray(array(
				"images" => serialize($images),
				"title" => $this->request('title'),
				"description" => $this->request('description'),
				"parent" => $this->request('parent'),
				"sortby" => $this->request('sortby'),
				"status" => $this->request('status'),
			));
			$this->db->whereArray(array(
				"id" => $id,
			));
			$this->db->update();
							
			$return['error'] = false;
			$return['msg'] = "Category updated."; 
			
		}else{
			$return['error'] = true;
			$return['msg'] = "Invalid ID."; 
		}
		return json($return);
	}
	
	public function categoryInsert(){
		$images = $this->request('images') ? array_filter($this->request('images')) : array();
		
		$this->db->table("category");
		$this->db->insertArray(array(
			"images" => serialize($images),
			"title" => $this->request('title'),
			"description" => $this->request('description'),
			"parent" => $this->request('parent'),
			"sortby" => $this->request('sortby'),
			"status" => $this->request('status'),
		));
		$this->db->insert();
						
		$return['error'] = false;
		$return['msg'] = "New category inserted."; 
			
		return json($return);
	}
	
	public function holidayUpdate($id){
		$this->db->table("holidays");
		$this->db->updateArray(array(
			"title" => $this->request('title'),
			"date_start" => $this->request('date_start'),
			"date_end" => $this->request('date_end'),
			"status" => $this->request('status'),
		));
		$this->db->whereArray(array(
			"id" => $id,
		));
		$this->db->update();
						
		$return['error'] = false;
		$return['msg'] = "Holiday updated."; 
		
		return json($return);
	}
	
	public function holidayInsert(){
		$this->db->table("holidays");
		$this->db->insertArray(array(
			"title" => $this->request('title'),
			"date_start" => $this->request('date_start'),
			"date_end" => $this->request('date_end'),
			"status" => $this->request('status'),
		));
		$this->db->insert();
						
		$return['error'] = false;
		$return['msg'] = "New holiday inserted."; 
			
		return json($return);
	}
	
	public function promoCodeUpdate($id){
		$categories = $this->request('category_ids') ? $this->request('category_ids') : array();
		$categories = implode(',', $categories);
		
		$products = $this->request('products') ? $this->request('products') : array();
		$products = implode(',', $products);
					
		$this->db->table("promo_code");
		$this->db->updateArray(array(
			"title" => $this->request('title'),
			"code" => $this->request('code'),
			"category_id" => $categories,
			"product_id" => $this->request('category_id') == '0' ? $products : '',
			"type" => $this->request('type'),
			"value" => $this->request('value'),
			"date_start" => $this->request('date_start'),
			"date_end" => $this->request('date_end'),
			"qty" => $this->request('qty'),
			"status" => $this->request('status'),
		));
		$this->db->whereArray(array(
			"id" => $id,
		));
		$this->db->update();
		
		$return['error'] = false;
		$return['msg'] = "Promo code succesfully updated."; 
	
		return json($return);
	}
	
	public function promoCodeInsert(){		
		$categories = $this->request('category_ids') ? $this->request('category_ids') : array();
		$categories = implode(',', $categories);
		
		$products = $this->request('products') ? $this->request('products') : array();
		$products = implode(',', $products);
				
		$this->db->table("promo_code");
		$this->db->insertArray(array(
			"title" => $this->request('title'),
			"code" => $this->request('code'),
			"category_id" => $categories,
			"product_id" => $this->request('category_id') == '0' ? $products : '',
			"type" => $this->request('type'),
			"value" => $this->request('value'),
			"date_start" => $this->request('date_start'),
			"date_end" => $this->request('date_end'),
			"qty" => $this->request('qty'),
			"status" => $this->request('status'),
		));
		$this->db->insert();
		
		$return['error'] = false;
		$return['msg'] = "New promo code inserted."; 
	
		return json($return);
	}
	
	public function newProductUpdate(){
		$products = $this->request('products') ? array_filter($this->request('products')) : array();
		
		$this->db->table("settings");
		$this->db->updateArray(array(
			"value" => implode(',', $products),
		));
		$this->db->whereArray(array(
			"name" => "new_products",
		));
		$this->db->update();
					
		$return['error'] = false;
		$return['msg'] = "New products updated."; 

		return json($return);
	}
	
	public function featuredProductUpdate(){
		$products = $this->request('products') ? array_filter($this->request('products')) : array();
		
		$this->db->table("settings");
		$this->db->updateArray(array(
			"value" => implode(',', $products),
		));
		$this->db->whereArray(array(
			"name" => "featured_products",
		));
		$this->db->update();
					
		$return['error'] = false;
		$return['msg'] = "Featured products updated."; 

		return json($return);
	}
}
?>