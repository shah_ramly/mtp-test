<?php
require_once 'config.php';
require_once '../lib/limonade.php';

$cms = new cms($db);
$post = new post($db, $cms);


function configure(){
  option('env', ENV_DEVELOPMENT);
  option('site_uri', ROOTPATH);
  option('base_uri', ROOTPATH . 'backpanel/');
}

function before($route){
	global $cms;
    set('currPath', $route['callback'][1]);
	set('settings', $cms->settings);
	set('cms', $cms);
}

if($cms->logged){	
	dispatch('/', [$cms, 'home']);

	if(in_array('analytics_report', $cms->admin['permissions'])){
		dispatch('/analytics-report', [$cms, 'analytics']);
	}

	if(in_array('individual', $cms->admin['permissions'])){
		dispatch('/individual/:page', [$cms, 'individualManagement']);
		dispatch('/individual-detail/:id', [$cms, 'individualDetail']);
		dispatch_post('/individual-detail/:id', [$post, 'individualDetailUpdate']);
		dispatch_post('/individual/employment/update', [$post, 'individualEmploymentUpdate']);
		dispatch_post('/individual/education/update', [$post, 'individualEducationUpdate']);
	}
	
	if(in_array('company', $cms->admin['permissions'])){
		dispatch('/company', [$cms, 'companyManagement']);
		dispatch('/company-detail/:id', [$cms, 'companyDetail']);
		dispatch_post('/company-detail/:id', [$post, 'companyDetailUpdate']);
	}

	if(array_intersect(['individual','company'], $cms->admin['permissions'])){
		dispatch('/analytics/:id', [$cms, 'memberAnalytics']);
	}

	if(array_intersect(['task_organic','task_aggregate'], $cms->admin['permissions'])){
		dispatch('/task/:tab/:page', [$cms, 'taskManagement']);
	}
	
	if(array_intersect(['job_organic','job_aggregate'], $cms->admin['permissions'])){
		dispatch('/job/:tab/:page', [$cms, 'jobManagement']);
	}

	if(array_intersect(['master_data_country','master_data_state','master_data_city','master_data_language','master_data_industry_existing','master_data_industry_entered','master_data_industry_language','master_data_category_existing','master_data_category_entered','master_data_category_language','master_data_skill_existing','master_data_skill_entered','master_data_skill_language','master_data_interest_existing','master_data_interest_entered','master_data_interest_language'], $cms->admin['permissions'])){
		if(in_array('master_data_country', $cms->admin['permissions'])){
			dispatch('/country', [$cms, 'countryList']);
			dispatch('/country/add', [$cms, 'countryListAdd']);
			dispatch('/country/:id', [$cms, 'countryListEdit']);
			dispatch_post('/country/add', [$post, 'addCountry']);
			dispatch_post('/country/delete', [$post, 'deleteCountry']);
			dispatch_post('/country/:id', [$post, 'editCountry']);
		}
		if(in_array('master_data_state', $cms->admin['permissions'])){
			dispatch('/state', [$cms, 'stateList']);
			dispatch('/state/add', [$cms, 'stateListAdd']);
			dispatch('/state/:id', [$cms, 'stateListEdit']);
			dispatch_post('/state/add', [$post, 'addState']);
			dispatch_post('/state/delete', [$post, 'deleteState']);
			dispatch_post('/state/:id', [$post, 'editState']);
		}
		if(in_array('master_data_city', $cms->admin['permissions'])){
			dispatch('/city', [$cms, 'cityList']);
			dispatch('/city/add', [$cms, 'cityListAdd']);
			dispatch('/city/:id', [$cms, 'cityListEdit']);
			dispatch_post('/city/add', [$post, 'addcity']);
			dispatch_post('/city/delete', [$post, 'deletecity']);
			dispatch_post('/city/:id', [$post, 'editcity']);
		}
		if(in_array('master_data_language', $cms->admin['permissions'])){
			dispatch('/language', [$cms, 'language']);
			dispatch('/language/:id', [$cms, 'languageText']);
			dispatch_post('/language/add', [$post, 'addLanguage']);
			dispatch_post('/language/edit', [$post, 'editLanguage']);
			dispatch_post('/language/:id', [$post, 'editLanguageText']);
		}
		if(array_intersect(['master_data_industry_existing','master_data_industry_entered','master_data_industry_language'], $cms->admin['permissions'])){
			dispatch('/industry', [$cms, 'industryList']);
			dispatch('/industry/add', [$cms, 'industryListAdd']);
			dispatch('/industry/:id', [$cms, 'industryListEdit']);
			dispatch_post('/industry/add', [$post, 'addIndustry']);
			dispatch_post('/industry/delete', [$post, 'deleteIndustry']);
			dispatch_post('/industry/:id', [$post, 'editIndustry']);
		}
		if(array_intersect(['master_data_category_existing','master_data_category_entered','master_data_category_language'], $cms->admin['permissions'])){
			dispatch('/category', [$cms, 'categoryList']);
			dispatch('/category/add', [$cms, 'categoryListAdd']);
			dispatch('/category/:id', [$cms, 'categoryListEdit']);
			dispatch_post('/category/add', [$post, 'addcategory']);
			dispatch_post('/category/add-into-existing', [$post, 'addCategoryIntoExisting']);
			dispatch_post('/category/delete', [$post, 'deleteCategory']);
			dispatch_post('/category/:id', [$post, 'editcategory']);
		}
		if(array_intersect(['master_data_skill_existing','master_data_skill_entered','master_data_skill_language'], $cms->admin['permissions'])){
			dispatch('/skill', [$cms, 'skillList']);
			dispatch('/skill/add', [$cms, 'skillListAdd']);
			dispatch('/skill/:id', [$cms, 'skillListEdit']);
			dispatch_post('/skill/add', [$post, 'addskill']);
			dispatch_post('/skill/add-into-existing', [$post, 'addskillIntoExisting']);
			dispatch_post('/skill/delete', [$post, 'deleteSkill']);
			dispatch_post('/skill/:id', [$post, 'editskill']);
		}
		if(array_intersect(['master_data_interest_existing','master_data_interest_entered','master_data_interest_language'], $cms->admin['permissions'])){
			dispatch('/interest', [$cms, 'interestList']);
			dispatch('/interest/add', [$cms, 'interestListAdd']);
			dispatch('/interest/:id', [$cms, 'interestListEdit']);
			dispatch_post('/interest/add', [$post, 'addInterest']);
			dispatch_post('/interest/delete', [$post, 'deleteInterest']);
			dispatch_post('/interest/:id', [$post, 'editInterest']);
		}	
	}

	if(in_array('subscription', $cms->admin['permissions'])){
		dispatch('/subscription', [$cms, 'subscription']);
		dispatch_post('/subscription', [$post, 'updateSubscription']);
	}

	if(array_intersect(['master_config_admin_admin','master_config_admin_role','master_config_system','master_config_notification'], $cms->admin['permissions'])){
		if(in_array('master_config_system', $cms->admin['permissions'])){
			dispatch('/system-settings', [$cms, 'systemSettings']);
			dispatch_post('/system-settings', [$post, 'systemSettings']);
		}
		if(array_intersect(['master_config_admin_admin','master_config_admin_role'], $cms->admin['permissions'])){
			dispatch('/admin', [$cms, 'admin']);
			dispatch('/admin/add', [$cms, 'adminAdd']);
			dispatch('/admin/:id', [$cms, 'adminEdit']);
			dispatch('/admin-role/add', [$cms, 'adminRoleAdd']);
			dispatch('/admin-role/:id', [$cms, 'adminRoleEdit']);
			dispatch_post('/admin', [$post, 'admin']);
			dispatch_post('/admin/add', [$post, 'adminAdd']);
			dispatch_post('/admin/delete', [$post, 'adminDelete']);
			dispatch_post('/admin/:id', [$post, 'adminEdit']);
			dispatch_post('/admin-role/add', [$post, 'adminRoleAdd']);
			dispatch_post('/admin-role/delete', [$post, 'adminRoleDelete']);
			dispatch_post('/admin-role/:id', [$post, 'adminRoleEdit']);
		}
	}

	dispatch('/ajax/category/:id/:type', array($cms, 'ajaxGetCategories'));
	dispatch('/ajax/country/:id', array($cms, 'ajaxGetStates'));
	dispatch('/ajax/cities/:state_id', array($cms, 'ajaxGetCities'));
	dispatch('/ajax/skills.json', array($cms, 'ajaxSkillList'));
	dispatch('/ajax/activity-log/:user_id/:page', [$cms, 'paginate_activity']);
	
	/*
	dispatch('/profile', array($cms, 'profile'));
	
	dispatch('/settings', array($cms, 'site_settings'));
	dispatch('/slider', array($cms, 'sliderUpdate'));
	dispatch('/pages', array($cms, 'pages'));
	dispatch('/pages/:id', array($cms, 'pageUpdate'));
	dispatch('/members', array($cms, 'members'));
	dispatch('/members/insert', array($cms, 'memberInsert'));
	dispatch('/members/:id', array($cms, 'memberUpdate'));
	dispatch('/products', array($cms, 'products'));
	dispatch('/products/insert', array($cms, 'productInsert'));
	dispatch('/products/:id', array($cms, 'productUpdate'));
	dispatch('/new-products', array($cms, 'newProductUpdate'));
	dispatch('/featured-products', array($cms, 'featuredProductUpdate'));
	dispatch('/category', array($cms, 'category'));
	dispatch('/category/insert', array($cms, 'categorytInsert'));
	dispatch('/category/:id', array($cms, 'categoryUpdate'));
	dispatch('/country/:id', array($cms, 'getState'));
	dispatch('/orders', array($cms, 'orders'));
	dispatch('/orders/:id', array($cms, 'orderUpdate'));
	dispatch('/holiday', array($cms, 'holiday'));
	dispatch('/holiday/insert', array($cms, 'holidayInsert'));
	dispatch('/holiday/:id', array($cms, 'holidayUpdate'));
	dispatch('/promo-code', array($cms, 'promoCode'));
	dispatch('/promo-code/insert', array($cms, 'promoCodeInsert'));
	dispatch('/promo-code/:id', array($cms, 'promoCodeUpdate'));
	
	dispatch_post('/delete', array($post, 'delete'));
	dispatch_post('/profile', array($post, 'profile'));
	dispatch_post('/settings', array($post, 'site_settings'));
	dispatch_post('/slider', array($post, 'sliderUpdate'));
	dispatch_post('/pages/:id', array($post, 'pageUpdate'));
	dispatch_post('/members/insert', array($post, 'memberInsert'));
	dispatch_post('/members/:id', array($post, 'memberUpdate'));
	dispatch_post('/products/insert', array($post, 'productInsert'));
	dispatch_post('/products/:id', array($post, 'productUpdate'));
	dispatch_post('/new-products', array($post, 'newProductUpdate'));
	dispatch_post('/featured-products', array($post, 'featuredProductUpdate'));
	dispatch_post('/category/insert', array($post, 'categoryInsert'));
	dispatch_post('/category/:id', array($post, 'categoryUpdate'));
	dispatch_post('/orders/:id', array($post, 'orderUpdate'));
	dispatch_post('/holiday/insert', array($post, 'holidayInsert'));
	dispatch_post('/holiday/:id', array($post, 'holidayUpdate'));
	dispatch_post('/promo-code/insert', array($post, 'promoCodeInsert'));
	dispatch_post('/promo-code/:id', array($post, 'promoCodeUpdate'));
	dispatch_post('/delete', array($post, 'delete'));	
	*/

}else{
	dispatch('*', array($cms, 'login'));
}

dispatch_post('/login', array($post, 'login'));
dispatch('/login', array($cms, 'login'));
dispatch('/logout', array($cms, 'logout'));

function not_found($errno = 0, $errstr = '', $errfile = null, $errline = null){
	global $cms;
	set('cms', $cms);
    set('errno', $errno);
    set('errstr', $errstr);
    set('errfile', $errfile);
    set('errline', $errline);
	return render('404.php', 'layout/default.php');
}

run();
?>
