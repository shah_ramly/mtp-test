scriptTags = document.getElementsByTagName('script');
jsPath = scriptTags[scriptTags.length - 1].src.split('?')[0].split('/').slice(0, -1).join('/') + '/';
rootPath = jsPath.split('assets'); rootPath.pop();
rootPath = rootPath.join('assets');
sitePath = rootPath.split('backpanel'); sitePath.pop();
sitePath = sitePath.join('backpanel');

url = document.URL.split('/');
thelast = url[url.length - 1] ? url[url.length - 1] : url[url.length - 2];
backUrl = url[url.length - 1] ? document.URL.replace(thelast, '') : document.URL.replace(thelast+'/', '');

$(function(){
	
	$('html').bind('ajaxStart', function(){  
		$(this).addClass('busy');  
	}).bind('ajaxStop', function(){  
		$(this).removeClass('busy');  
	});
	
	$(window).resize(function() {
		winWidth = $(window).width();
		winHeight = $(window).height();		
	}).resize();
	
	$(document).on('click', '.back', function(e){
		e.preventDefault();
		window.history.back();
	});

	$(document).on('click', '.delete', function(e){
		e.preventDefault();

		row = $(this).closest('tr');
		id = $(this).data('id');
		type = $(this).data('type');
		var data = { act: 'delete', type: type, id: id }
		
		if(confirm('Are you sure to delete this ' + type + '?')){
			ajax_form(data, rootPath + 'delete');
				
			row.fadeOut(function(){
				row.remove();
			});
		} 
	});
	
	$(document).on('change', '.category', function(){
		var el = $(this);
		var id = el.val();
		var target = el.data('target');
		var type = el.data('type');
		if( type === undefined ) type = '';
		if(target){
			$.ajax({
				type: 'GET',
				url: rootPath + 'ajax/category/' + id + '/' + type,
				dataType : 'json',
				data: { },
				success: function(results){
					if(results.error == true){
						t('e', results.msg);
					}else{
						$('[name="' + target + '"]').html(results.html);
						selected = $('[name="' + target + '"]').data('selected');
						if(selected){
							$('[name="' + target + '"]').val(selected).change();
						}
						var categoryUpdatedEvent = $.Event("categoryUpdated");
						$('[name="' + target + '"]').trigger(categoryUpdatedEvent);
					}
				},
				error: function(error){
					ajax_error(error);
				},
				complete: function(){
					
				}
			});
		}
	});	
	
	$(document).on('change', '.country', function(){
		var el = $(this);
		var id = el.val();
		var target = el.data('target');
		
		if(target){
			$.ajax({
				type: 'GET',
				url: rootPath + 'ajax/country/' + id,
				dataType : 'json',
				data: { },
				success: function(results){
					if(results.error == true){
						t('e', results.msg);
					}else{
						$('select[name="' + target + '"]').html(results.states);
						selected = $('select[name="' + target + '"]').data('selected');
						if(selected){
							if( $('select[name="' + target + '"] option[value="' + selected + '"]').length ){
								$('select[name="' + target + '"]').val(selected).change();
							}else{
								$('select[name="' + target + '"]').val('').change();
							}
						}
						var countryUpdatedEvent = $.Event("countryUpdated");
						$('select[name="' + target + '"]').trigger(countryUpdatedEvent);
					}
				},
				error: function(error){
					ajax_error(error);
				},
				complete: function(){
					$('select[name="' + target + '"]').change();
				}
			});
		}
	});	
	
	$(document).on('change', '.state', function(){
		var el = $(this);
		var id = el.val();
		var target = el.data('target');
				
		if(target){
			$('select[name="' + target + '"]').html('<option value="">Town/City</option>');
			if(id){
				$.ajax({
					type: 'GET',
					url: rootPath + 'ajax/cities/' + id,
					dataType : 'json',
					data: { },
					success: function(results){
						if(results.error == true){
							t('e', results.msg);
						}else{
							$('select[name="' + target + '"]').html(results.states);
							selected = $('select[name="' + target + '"]').data('selected');
							if(selected){
								if( $('select[name="' + target + '"] option[value="' + selected + '"]').length ){
									$('select[name="' + target + '"]').val(selected);
								}else{
									$('select[name="' + target + '"]').val('');
								}
							}
							var stateUpdatedEvent = $.Event("stateUpdated");
							$('select[name="' + target + '"]').trigger(stateUpdatedEvent);
						}
					},
					error: function(error){
						ajax_error(error);
					},
					complete: function(){
						
					}
				});
			}
		}
	});	
	
	$(document).on('submit', '#loginform', function(){
		$.ajax({
			type:'POST',
			url: rootPath + 'login',
			dataType : 'json',
			data: $('#loginform').serialize(),
			success: function(results){
				if(results.error == true){
					t('e', results.msg);
				}else{
					t('s', results.msg);
					 setTimeout(function(){
						  window.location.reload();
					 }, 1000);
				}
			},
			error: function(error){
				ajax_error(error);
			},
			complete: function(){}
		});
		return false;
	});

	$(document).on('submit', '#addLanguage', function(){
		$.ajax({
			type:'POST',
			url: rootPath + 'language/add',
			dataType : 'json',
			data: $('#addLanguage').serialize(),
			success: function(results){
				if(results.error == true){
					t('e', results.msg);
				}else{
					t('s', results.msg);
					 setTimeout(function(){
						  window.location.reload();
					 }, 1000);
				}
			},
			error: function(error){
				ajax_error(error);
			},
			complete: function(){}
		});
		return false;
	});

	$(document).on('submit', '#editLanguage', function(){
		$.ajax({
			type:'POST',
			url: rootPath + 'language/edit',
			dataType : 'json',
			data: $('#editLanguage').serialize(),
			success: function(results){
				if(results.error == true){
					t('e', results.msg);
				}else{
					t('s', results.msg);
					$('[data-id="' + results.id + '"]').closest('tr').find('.info-title').text(results.title);
					$('[data-id="' + results.id + '"]').data('code', results.code);
					$('#modal_add_language').modal('hide');
				}
			},
			error: function(error){
				ajax_error(error);
			},
			complete: function(){}
		});
		return false;
	});
	
	$(document).on('submit', '#selfForm', function(){
		var data = $(this).serializeArray();
		ajax_form(data);
		return false;
	});

	$(document).on('submit', '#directForm', function(){
		var data = $(this).serializeArray();
		ajax_form_action(data, '', 'direct');
		return false;
	});
		
	$("#backForm").submit(function(){
		var data = $(this).serializeArray();
		if( $(this).find('#html').length == 1 ) data.push({name: 'content', value: CKEDITOR.instances.html.getData() });
		if( $(this).find('#description').length == 1 ) data.push({name: 'description', value: CKEDITOR.instances.description.getData() });
		if( $(this).find('#description2').length == 1 ) data.push({name: 'description2', value: CKEDITOR.instances.description2.getData() });
		ajax_form_action(data, '', 'back');
		return false;
	});

	// Backend Acc Suspension Toggler
    $(document).ready(function () {
        $("#acc_susp").on("change", function () {
            accSuspension(this);
        })
    });
    function accSuspension(ele) {
        if ($(ele).prop("checked") == true) {
            $('.form-acc-suspension').show();
        } else if ($(ele).prop("checked") == false) {
            $('.form-acc-suspension').hide();
        }
    }
    
    // Backend Acc Suspension Disable Date
    $('#accSuspNoDate').on('change', function () {
        if (this.checked) {
            $('#form_datesuspension input').prop( "disabled", true );
        } else {
            $('#form_datesuspension input').prop('disabled', false);
        }
    });
    
    // Backend Custom Date Format
    $('#radio_dateFormat_custom').on('change', function () {
        if (this.checked) {
            $('.row-date-format-custom input').prop( "disabled", false);
        } else {
            $('.row-date-format-custom input').prop('disabled', true);
        }
    });
    
    // Backend Custom Time Format
    $('#radio_timeFormat_custom').on('change', function () {
        if (this.checked) {
            $('.row-time-format-custom input').prop( "disabled", false);
        } else {
            $('.row-time-format-custom input').prop('disabled', true);
        }
    });
    // Backend Custom Session Timeout
    $('#radio_session_1').on('change', function () {
        if (this.checked) {
            $('.custom-radio-timeout input[type="number"]').prop( "disabled", true);
        } else {
            $('.custom-radio-timeout input[type="number"]').prop( "disabled", false);
        }
    });
    $('#radio_session_2').on('change', function () {
        if (this.checked) {
            $('.custom-radio-timeout input[type="number"]').prop( "disabled", false);
        } else {
            $('.custom-radio-timeout input[type="number"]').prop( "disabled", true);
        }
    });
});

t = function(type, text) {
	if(type == "e"){
		icon = "fa fa-exclamation";
		title = 'Error!';
		type = 'danger';
	}else if(type == "w"){
		icon = "fa fa-warning";
		title = 'Warning!';
		type = 'warning';
	}else if(type == "s"){
		icon = "fa fa-check";
		title = 'Success!';
		type = 'success';
	}else if(type == "i"){
		icon = "fa fa-question";
		title = 'Information';
		type = 'info';
	}else{
		icon = "fa fa-circle-o";
		title = 'Information';
		type = '';
	}
	
    $.notify({
        title: title,
		message: text,
		icon: icon
    }, {
		element: 'body',
		type: type,
		allow_dismiss: true,
		template: '<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert">' +
			'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
			'<span data-notify="icon"></span> ' +
			'<span data-notify="title">{1}</span> ' +
			'<span data-notify="message">{2}</span>' +
			'<div class="progress" data-notify="progressbar">' +
				'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
			'</div>' +
			'<a href="{3}" target="{4}" data-notify="url"></a>' +
		'</div>'
    });
}

ajax_form = function(data, url, dyntable){
	$.ajax({
		type:'POST',
		url: url,
		dataType : 'json',
		data:data,
		success: function(results){
			if(results.warning !== undefined){
				t('w', results.msg);
			}else if(results.error == true){
				t('e', results.msg);
			}else{
				t('s', results.msg);
			}
		},
		error: function(error){
			ajax_error(error);
		},
		complete: function(){
			if(dyntable){
				$(dyntable).dataTable().fnDraw();
			}
		}
	});           
}
	
ajax_form_action = function(data, url, action){
	$.ajax({
		type:'POST',
		url: url,
		dataType : 'json',
		data:data,
		success: function(results){
			if(results.error == true){
				t('e', results.msg);
			}else{		
				t('s', results.msg);
				setTimeout(function(){
					if(action == 'reload') window.location.reload();
					if(action == 'direct') window.location = results.url;
					if(action == 'back'){
						window.location = backUrl;
					}
				}, 1000);
			}
		},
		error: function(error){
			ajax_error(error);
		},
		complete: function(){}
	});           
}

ajax_error = function(error){
	if(error.status == 0){
		t('e', 'You network currently are offline!!');
	}else if(error.status == 404){
		t('e', 'Requested URL not found.');
	}else if(error.status == 500){
		t('e', 'Internel Server Error.');
	}else if(error == 'parsererror'){
		t('e', 'Parsing JSON Request failed.');
	}else if(error == 'timeout'){
		t('e', 'Request Time out.');
	}else {
		t('e', 'Unknow Error : '+ error.responseText);
	}
}
