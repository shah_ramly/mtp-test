$(window).on('load', function(){
	if( $('#profileImage').length ){
		'use strict';
		
		var url = rootPath + '../up/profile';
		avatar_max_size = 0;

		$('#profileImage').fileupload({
			url: url,
			dataType: 'json',
			add: function(e, data) {
                if(avatar_max_size > 0 && data.originalFiles[0]['size'] > avatar_max_size) {
					t('e', 'File too large. File must be less than avatar_max_size.');
                }else {
                    data.submit();
                }
			},
			done: function (e, data) {
				curLoadStop();
				if(data.result.error == true){
					t('e', data.result.msg);
					
				}else{
					if( $('.profile_photo img.profile-img').length ){
						$('.profile_photo img.profile-img').attr('src', data.result.thumb);
					}else{
						$('.profile_photo').prepend('<img src="' + data.result.thumb + '" class="profile-img"><br>');
					}
					$('.profile_photo input[type="hidden"]').val(data.result.src);
				}
			},
			progressall: function (e, data) {
				var progress = parseInt(data.loaded / data.total * 100, 10);
				curLoadStart(progress + '%');
			}
		});
	}

	docs = Array();
	if( $('#profile-resume').length ){
		docs.push('resume');
	}

	if( $('#profile-cover_letter').length ){
		docs.push('cover_letter');
	}

	if( $('#profile-cert').length ){
		docs.push('cert');
	}
	
	if(docs){
		docs_max_size = 0;
		resume_max_file = 0;
		cover_letter_max_file = 0;
		cert_max_file = 0;

		$(docs).each(function(i, type){
			'use strict';

			var url = rootPath + '../up/' + type;
			$('#profile-' + type).fileupload({
				url: url,
				dataType: 'json',
				beforeSend: function(){
					if(type == 'resume'){
						max_file = resume_max_file;
					}else if(type == 'cover_letter'){
						max_file = cover_letter_max_file;
					}else if(type == 'cert'){
						max_file = cert_max_file;
					}

					if( type == 'resume' && $('.' + type + ' li').length >= resume_max_file){
						t('e', 'Maximum files allowed for resume has reach limit.');
						return false;

					}else if(type == 'cover_letter'  && $('.' + type + ' li').length >= cover_letter_max_file){
						t('e', 'Maximum files allowed for cover_letter has reach limit.');
						return false;

					}else if(type == 'cert'  && $('.' + type + ' li').length >= cert_max_file){
						t('e', 'Maximum files allowed for cert has reach limit.');
						return false;
					}
				},
				add: function(e, data) {
					if(docs_max_size > 0 && data.originalFiles[0]['size'] > docs_max_size) {
						t('e', 'File too large. File must be less than docs_max_size.');
					}else {
						data.submit();
					}
				},
				done: function (e, data) {
					curLoadStop();
					if(data.result.error == true){
						t('e', data.result.msg);
						
					}else{
						$('.my-docs.' + type).append(data.result.html);
						$('.my-docs.' + type + ' li').each(function(i, v){
							$('a:first-child', this).text((i+1));
						});
						$('[data-title]').tooltip();
					}
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					curLoadStart(progress + '%');
				}
			});
		});
	}

	if( $('#profile-company_gallery').length ){
		'use strict';
		
		var url = rootPath + '../up/company_gallery';
		avatar_max_size = 0;
		$('#profile-company_gallery').fileupload({
			url: url,
			dataType: 'json',
			add: function(e, data) {
                if(avatar_max_size > 0 && data.originalFiles[0]['size'] > avatar_max_size) {
					t('e', 'File too large. File must be less than avatar_max_size.');
                }else {
                    data.submit();
                }
			},
			done: function (e, data) {
				curLoadStop();
				if(data.result.error == true){
					t('e', data.result.msg);
					
				}else{
					$('.company_gallery').append(data.result.html);
				}
			},
			progressall: function (e, data) {
				var progress = parseInt(data.loaded / data.total * 100, 10);
				curLoadStart(progress + '%');
			}
		});
	}
});

curLoadStart = function(content){
	$('html').addClass('busy');
	if( $('.cursorLoading').length ){
		$('.cursorLoading').html(content);
	}else{
		$('body').prepend('<div class="cursorLoading" style="display: inline-block;">' + content + '</div>');
	}
	
	if( $('#progress').length ){
		$('#progress').css('width', content);
	}else{
		$('body').prepend('<div id="progress" class="waiting"><dt></dt><dd></dd></div>');
	}
}

curLoadStop = function(){
	$('html').removeClass('busy');
	$('.cursorLoading').fadeOut(function(){
		$('.cursorLoading').remove();
	});
	$('#progress').fadeOut(function(){
		$('#progress').remove();
	});
}
