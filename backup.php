<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/lib/config.php';

use Ifsnop\Mysqldump as IMysqldump;

try {
    $dumpSettingsDefault = array(
        'compress' => IMysqldump\Mysqldump::BZIP2,
        'databases' => false,
        'default-character-set' => IMysqldump\Mysqldump::UTF8MB4,
    );

    // create directory if it doesn't exist
    if( !is_dir(__DIR__ . '/backups/') ) {
        mkdir(__DIR__ . '/backups/');
    }

    $dump = new IMysqldump\Mysqldump('mysql:host=localhost;dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD, $dumpSettingsDefault);
    $dump->start('backups/'. DB_DATABASE . '-' . date('YmdHis') .'.bzip2');

} catch (\Exception $e) {
    echo 'mysqldump-php error: ' . $e->getMessage();
}

// zip files folder
//exec('zip -r backups/files-'. date('YmdHis') .'.zip "files"');
exec('tar -cjvf ./backups/files-'. date('YmdHis') .'.bzip2 files');