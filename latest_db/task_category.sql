-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 10, 2020 at 02:13 PM
-- Server version: 5.7.21
-- PHP Version: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mtp`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `task_category` (
  `id` int(11) NOT NULL,
  `images` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) NOT NULL,
  `sortby` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `task_category` (`id`, `images`, `title`, `description`, `parent`, `sortby`, `status`) VALUES
(1, '', 'Chores', '', 0, 0, 0),
(2, '', 'Food & Beverage', '', 0, 0, 0),
(3, '', 'Kids', '', 0, 0, 0),
(4, '', 'Entertainment', '', 0, 0, 0),
(5, '', 'Technology', '', 0, 0, 0),
(11, '', 'Vacuumming', '', 1, 0, 1),
(12, '', 'Washing Dishes', '', 1, 0, 1),
(13, '', 'Feeding Pets', '', 1, 0, 1),
(14, '', 'Laundry', '', 1, 0, 1),
(15, '', 'Beverage Server', '', 2, 0, 1),
(16, '', 'Chef', '', 2, 0, 1),
(17, '', 'Cleaner', '', 2, 0, 1),
(18, '', 'Food Delivery', '', 2, 0, 1),
(19, '', 'Waiter', '', 2, 0, 1),
(20, '', 'Arts & Craft', '', 3, 0, 1),
(21, '', 'Babysit', '', 3, 0, 1),
(22, '', 'Learning & Education', '', 3, 0, 1),
(23, '', 'Nature Activities', '', 3, 0, 1),
(24, '', 'Play Kids Games', '', 3, 0, 1),
(25, '', 'Actor', '', 4, 0, 1),
(26, '', 'Emcee', '', 4, 0, 1),
(27, '', 'Musician', '', 4, 0, 1),
(28, '', 'Performer', '', 4, 0, 1),
(29, '', 'Voice Acting', '', 4, 0, 1),
(30, '', 'Database Administrator', '', 5, 0, 1),
(31, '', 'Electrician', '', 5, 0, 1),
(32, '', 'Hardware Engineer', '', 5, 0, 1),
(33, '', 'Softrware Engineer', '', 5, 0, 1),
(34, '', 'Technician', '', 5, 0, 1);
COMMIT;
-- --------------------------------------------------------