-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.27 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mtp_dev_2.category
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) NOT NULL,
  `sortby` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.chats
CREATE TABLE IF NOT EXISTS `chats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` text,
  `receiver_id` text,
  `contents` text,
  `created_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.cities
CREATE TABLE IF NOT EXISTS `cities` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` mediumint(8) unsigned NOT NULL,
  `state_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` mediumint(8) unsigned NOT NULL,
  `country_code` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` decimal(10,8) NOT NULL,
  `longitude` decimal(11,8) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2014-01-01 09:01:01',
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  `wikiDataId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Rapid API GeoDB Cities',
  PRIMARY KEY (`id`),
  KEY `cities_test_ibfk_1` (`state_id`),
  KEY `cities_test_ibfk_2` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=143913 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `post_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'If it''s a reply of another comment',
  `comment` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.company_details
CREATE TABLE IF NOT EXISTS `company_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `reg_num` text,
  `address` text,
  `postal_code` text,
  `state` text,
  `country` text,
  `email` text,
  `contact_number` text,
  `industry` text,
  `member_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iso3` char(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iso2` char(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phonecode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `capital` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `native` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emoji` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emojiU` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  `wikiDataId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Rapid API GeoDB Cities',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.document
CREATE TABLE IF NOT EXISTS `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` text,
  `doc_location` text,
  `member_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.education
CREATE TABLE IF NOT EXISTS `education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `highest_edu_level` text,
  `grad_year` text,
  `edu_institution` text,
  `field` text,
  `major` text,
  `grade` text,
  `skills` text,
  `language` text,
  `member_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.employment_details
CREATE TABLE IF NOT EXISTS `employment_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` text,
  `position` text,
  `duration` text,
  `department` text,
  `job_desc` text,
  `responsibilities` text,
  `remuneration` text,
  `location` text,
  `industry` text,
  `member_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.holidays
CREATE TABLE IF NOT EXISTS `holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `sub_category` int(11) NOT NULL,
  `title` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `requirements` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `experience_level` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `location` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `salary_range_min` decimal(10,0) NOT NULL DEFAULT '0',
  `salary_range_max` decimal(10,0) NOT NULL DEFAULT '0',
  `benefits` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `status` enum('draft','published','completed','cancelled') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `assigned` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `jobs_user_id_index` (`user_id`) USING BTREE,
  KEY `jobs_category_index` (`category`) USING BTREE,
  KEY `jobs_sub_category_index` (`sub_category`) USING BTREE,
  KEY `jobs_experience_level_index` (`experience_level`) USING BTREE,
  KEY `jobs_country_index` (`country`) USING BTREE,
  KEY `jobs_state_index` (`state`) USING BTREE,
  KEY `jobs_status_index` (`status`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.job_questions
CREATE TABLE IF NOT EXISTS `job_questions` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `job_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `job_questions_id_job_id_unique` (`id`,`job_id`),
  KEY `job_questions_job_id_index` (`job_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.members
CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fb_uid` text,
  `google_id` text NOT NULL,
  `fb_email` text,
  `email` text,
  `password` text,
  `photo` text,
  `salutation` text,
  `firstname` text,
  `lastname` text,
  `contact` text,
  `address1` text,
  `address2` text,
  `zip` text,
  `city` text,
  `country` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `nric` text,
  `gender` text,
  `dob` text,
  `nationality` text,
  `contact_number` text,
  `mobile_number` text,
  `profile_pic` text,
  `insta_id` text,
  `other_social_media` text,
  `emergency_contact_name` text,
  `emergency_contact_number` text,
  `emergency_contact_relation` text,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT '0' COMMENT '0- Active 99- Suspended',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '0- New Member 1- Existing Member ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.member_docs
CREATE TABLE IF NOT EXISTS `member_docs` (
  `member_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `src` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.member_logs
CREATE TABLE IF NOT EXISTS `member_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `ip_address` text NOT NULL,
  `user_agent` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` text NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.preference
CREATE TABLE IF NOT EXISTS `preference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rm_work_location` text COMMENT 'rm- Remote Work',
  `rm_country` text,
  `rm_state` text,
  `p_work_location` text COMMENT 'p - physical ',
  `p_country` text,
  `p_state` text,
  `work_day` text,
  `working_hours` text,
  `work_load_hours` text,
  `expected_earning` text,
  `member_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` text NOT NULL,
  `images` text NOT NULL,
  `title` text NOT NULL,
  `model` text NOT NULL,
  `description` text NOT NULL,
  `description2` text NOT NULL,
  `size` text NOT NULL,
  `material` text NOT NULL,
  `color` text NOT NULL,
  `fabric` text NOT NULL,
  `category` int(11) NOT NULL,
  `categories` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `price_original` decimal(10,2) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `prices` text NOT NULL,
  `options` text NOT NULL,
  `colors` text NOT NULL,
  `is_new` int(11) NOT NULL,
  `sale_price` decimal(10,2) NOT NULL,
  `product_suggestion` text NOT NULL,
  `date_added` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.promo_code
CREATE TABLE IF NOT EXISTS `promo_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `category_id` text NOT NULL,
  `product_id` text NOT NULL,
  `type` text NOT NULL,
  `value` int(11) NOT NULL,
  `code` text NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `qty` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.shopping_cart
CREATE TABLE IF NOT EXISTS `shopping_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `item_option` text NOT NULL,
  `item_color` text NOT NULL,
  `item_ship_country` int(11) NOT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `promo_code` text NOT NULL,
  `date` datetime NOT NULL,
  `member_id` int(11) NOT NULL,
  `guest_id` text NOT NULL,
  `checkout_id` int(11) NOT NULL,
  `notified` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.shopping_checkout
CREATE TABLE IF NOT EXISTS `shopping_checkout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carts` text NOT NULL,
  `member_id` int(11) NOT NULL,
  `guest_id` text NOT NULL,
  `total` double NOT NULL,
  `email` text NOT NULL,
  `comment` text NOT NULL,
  `shipping` double NOT NULL,
  `shipping_method` text NOT NULL,
  `shipping_date` date NOT NULL,
  `shipping_time` int(11) NOT NULL,
  `delivery_salutation` text NOT NULL,
  `delivery_firstname` text NOT NULL,
  `delivery_lastname` text NOT NULL,
  `delivery_address1` text NOT NULL,
  `delivery_address2` text NOT NULL,
  `delivery_city` text NOT NULL,
  `delivery_zip` text NOT NULL,
  `delivery_country` int(11) NOT NULL,
  `delivery_state` int(11) NOT NULL,
  `delivery_contact` text NOT NULL,
  `billing_salutation` text NOT NULL,
  `billing_firstname` text NOT NULL,
  `billing_lastname` text NOT NULL,
  `billing_address1` text NOT NULL,
  `billing_address2` text NOT NULL,
  `billing_city` text NOT NULL,
  `billing_zip` text NOT NULL,
  `billing_country` int(11) NOT NULL,
  `billing_state` int(11) NOT NULL,
  `billing_contact` text NOT NULL,
  `pp_token` text NOT NULL,
  `pp_payer_id` text NOT NULL,
  `payment_date` datetime NOT NULL,
  `payment_method` text NOT NULL,
  `payment_reference` longtext NOT NULL,
  `payment_receipts` longtext NOT NULL,
  `status` int(11) NOT NULL,
  `tracking_no` text NOT NULL,
  `pp_GetExpressCheckoutDetails` text NOT NULL,
  `pp_DoExpressCheckoutPayment` text NOT NULL,
  `pp_build` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.states
CREATE TABLE IF NOT EXISTS `states` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` mediumint(8) unsigned NOT NULL,
  `country_code` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fips_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iso2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `flag` tinyint(1) NOT NULL DEFAULT '1',
  `wikiDataId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Rapid API GeoDB Cities',
  PRIMARY KEY (`id`),
  KEY `country_region` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4881 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.tasks
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` CHAR(36) NOT NULL COLLATE 'utf8_unicode_ci',
  `user_id` CHAR(36) NOT NULL COLLATE 'utf8_unicode_ci',
  `category` INT(11) NOT NULL,
  `sub_category` INT(11) NOT NULL,
  `title` MEDIUMTEXT NOT NULL COLLATE 'utf8_unicode_ci',
  `slug` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_unicode_ci',
  `description` MEDIUMTEXT NOT NULL COLLATE 'utf8_unicode_ci',
  `type` ENUM('by-hour','lump-sum') NOT NULL COLLATE 'utf8_unicode_ci',
  `location` MEDIUMTEXT NOT NULL COLLATE 'utf8_unicode_ci',
  `budget` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
  `hours` INT(11) NULL DEFAULT NULL,
  `start_by` DATETIME NOT NULL,
  `complete_by` DATETIME NOT NULL,
  `assigned` DATETIME NULL DEFAULT NULL,
  `assigned_to` INT(11) NULL DEFAULT NULL,
  `status` ENUM('draft','published','completed','cancelled') NOT NULL DEFAULT 'draft' COLLATE 'utf8_unicode_ci',
  `urgent` ENUM('0','1') NOT NULL DEFAULT '0' COLLATE 'utf8_unicode_ci',
  `rejection_rate` INT(11) NOT NULL DEFAULT '50' COMMENT 'This value is in percentage %',
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `tasks_user_id_index` (`user_id`) USING BTREE,
  INDEX `tasks_category_index` (`category`) USING BTREE,
  INDEX `tasks_sub_category_index` (`sub_category`) USING BTREE,
  INDEX `tasks_type_index` (`type`) USING BTREE,
  INDEX `tasks_status_index` (`status`) USING BTREE,
  FULLTEXT INDEX `title_slug` (`title`, `slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.task_attachments
CREATE TABLE IF NOT EXISTS `task_attachments` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_attachments_task_id_attachment_unique` (`task_id`,`attachment`),
  KEY `task_attachments_task_id_index` (`task_id`),
  KEY `task_attachments_type_index` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.task_favourites
CREATE TABLE IF NOT EXISTS `task_favourites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.task_questions
CREATE TABLE IF NOT EXISTS `task_questions` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_questions_id_task_id_unique` (`id`,`task_id`),
  KEY `task_questions_task_id_index` (`task_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.task_tags
CREATE TABLE IF NOT EXISTS `task_tags` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_tags_task_id_tag_unique` (`task_id`,`tag`),
  KEY `task_tags_task_id_index` (`task_id`),
  KEY `task_tags_tag_index` (`tag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.temp_token
CREATE TABLE IF NOT EXISTS `temp_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` text,
  `email` text,
  `token_status` int(11) DEFAULT '0' COMMENT '0- Not Used 99 - Deleted',
  `created_by` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `expiry_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.user_rating
CREATE TABLE IF NOT EXISTS `user_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` char(36) CHARACTER SET utf8 NOT NULL,
  `project_id` char(36) CHARACTER SET utf8 NOT NULL,
  `project_type` enum('task','job') CHARACTER SET utf8 NOT NULL DEFAULT 'task',
  `timeliness` decimal(10,1) NOT NULL DEFAULT '0.0',
  `quality` decimal(10,1) NOT NULL DEFAULT '0.0',
  `communication` decimal(10,1) NOT NULL DEFAULT '0.0',
  `responsiveness` decimal(10,1) NOT NULL DEFAULT '0.0',
  `review` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `author_id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT 'This would contain the user_id whom wrote the review',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table mtp_dev_2.user_task
CREATE TABLE IF NOT EXISTS `user_task` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` CHAR(36) NOT NULL COLLATE 'utf8_unicode_ci',
  `task_id` CHAR(36) NOT NULL COLLATE 'utf8_unicode_ci',
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
