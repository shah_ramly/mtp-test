-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 09, 2020 at 10:44 AM
-- Server version: 5.7.32
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maketime_staging`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `question_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `answer` mediumtext COLLATE utf8_unicode_ci,
  `type` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'task',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`task_id`, `question_id`, `user_id`, `answer`, `type`, `created_at`, `updated_at`, `id`) VALUES
('A0702622-963C-4AFA-873C-00EF58BF7859', '9E9E0BA5-9334-45B9-8F85-56913C673BB7', 1, '1', 'task', '2020-11-05 10:30:21', NULL, 1),
('A0702622-963C-4AFA-873C-00EF58BF7859', '58FC071A-3579-4406-9A05-027A7DF37A58', 1, '2', 'task', '2020-11-05 10:30:21', NULL, 2),
('A0702622-963C-4AFA-873C-00EF58BF7859', '020B9F58-707A-4DAD-94E3-7CC2C2181CDA', 1, '3', 'task', '2020-11-05 10:30:21', NULL, 3),
('A0702622-963C-4AFA-873C-00EF58BF7859', '9E9E0BA5-9334-45B9-8F85-56913C673BB7', 3, '1', 'task', '2020-11-05 10:32:35', NULL, 4),
('A0702622-963C-4AFA-873C-00EF58BF7859', '58FC071A-3579-4406-9A05-027A7DF37A58', 3, '2', 'task', '2020-11-05 10:32:35', NULL, 5),
('A0702622-963C-4AFA-873C-00EF58BF7859', '020B9F58-707A-4DAD-94E3-7CC2C2181CDA', 3, '3', 'task', '2020-11-05 10:32:35', NULL, 6),
('42ACC3E3-2A22-457E-8A22-034780D791DE', '404ABE25-CA9F-43ED-AB59-07943517F2BB', 1, '1', 'task', '2020-11-05 10:34:26', NULL, 7),
('42ACC3E3-2A22-457E-8A22-034780D791DE', '560D7C05-C7CE-4730-ACE0-C0E58EED3ECA', 1, '2', 'task', '2020-11-05 10:34:26', NULL, 8),
('42ACC3E3-2A22-457E-8A22-034780D791DE', '404ABE25-CA9F-43ED-AB59-07943517F2BB', 4, '1', 'task', '2020-11-05 10:42:58', NULL, 9),
('42ACC3E3-2A22-457E-8A22-034780D791DE', '560D7C05-C7CE-4730-ACE0-C0E58EED3ECA', 4, '2', 'task', '2020-11-05 10:42:58', NULL, 10),
('A0702622-963C-4AFA-873C-00EF58BF7859', '9E9E0BA5-9334-45B9-8F85-56913C673BB7', 4, '1', 'task', '2020-11-05 10:43:11', NULL, 11),
('A0702622-963C-4AFA-873C-00EF58BF7859', '58FC071A-3579-4406-9A05-027A7DF37A58', 4, '2', 'task', '2020-11-05 10:43:11', NULL, 12),
('A0702622-963C-4AFA-873C-00EF58BF7859', '020B9F58-707A-4DAD-94E3-7CC2C2181CDA', 4, '3', 'task', '2020-11-05 10:43:11', NULL, 13),
('AB5E0455-6370-45E0-8C19-EF9FFEB7FDBE', '3AA810FF-07EF-4441-BEA1-96D72FD223C0', 2, 'Yes', 'task', '2020-11-06 09:04:14', NULL, 14),
('AB5E0455-6370-45E0-8C19-EF9FFEB7FDBE', 'B1AA4D02-3644-4A63-8638-867F98C3463E', 2, 'No', 'task', '2020-11-06 09:04:14', NULL, 15);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `images` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) NOT NULL,
  `sortby` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `images`, `title`, `description`, `parent`, `sortby`, `status`) VALUES
(1, '', 'Chores', '', 0, 0, 0),
(2, '', 'Food & Beverage', '', 0, 0, 0),
(3, '', 'Kids', '', 0, 0, 0),
(4, '', 'Entertainment', '', 0, 0, 0),
(5, '', 'Technology', '', 0, 0, 0),
(11, '', 'Vacuumming', '', 1, 0, 1),
(12, '', 'Washing Dishes', '', 1, 0, 1),
(13, '', 'Feeding Pets', '', 1, 0, 1),
(14, '', 'Laundry', '', 1, 0, 1),
(15, '', 'Beverage Server', '', 2, 0, 1),
(16, '', 'Chef', '', 2, 0, 1),
(17, '', 'Cleaner', '', 2, 0, 1),
(18, '', 'Food Delivery', '', 2, 0, 1),
(19, '', 'Waiter', '', 2, 0, 1),
(20, '', 'Arts & Craft', '', 3, 0, 1),
(21, '', 'Babysit', '', 3, 0, 1),
(22, '', 'Learning & Education', '', 3, 0, 1),
(23, '', 'Nature Activities', '', 3, 0, 1),
(24, '', 'Play Kids Games', '', 3, 0, 1),
(25, '', 'Actor', '', 4, 0, 1),
(26, '', 'Emcee', '', 4, 0, 1),
(27, '', 'Musician', '', 4, 0, 1),
(28, '', 'Performer', '', 4, 0, 1),
(29, '', 'Voice Acting', '', 4, 0, 1),
(30, '', 'Database Administrator', '', 5, 0, 1),
(31, '', 'Electrician', '', 5, 0, 1),
(32, '', 'Hardware Engineer', '', 5, 0, 1),
(33, '', 'Softrware Engineer', '', 5, 0, 1),
(34, '', 'Technician', '', 5, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(11) NOT NULL,
  `sender_id` text,
  `receiver_id` text,
  `contents` text,
  `task_id` text,
  `type` varchar(365) DEFAULT 'text',
  `file_extension` text,
  `batch_id` text,
  `deleted` tinyint(4) DEFAULT '0',
  `reply_id` text,
  `size` text,
  `created_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `viewed` int(11) NOT NULL,
  `viewed_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `sender_id`, `receiver_id`, `contents`, `task_id`, `type`, `file_extension`, `batch_id`, `deleted`, `reply_id`, `size`, `created_by`, `viewed`, `viewed_date`) VALUES
(1, '2', '1', 'Hello', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'text', NULL, NULL, 0, '', NULL, '2020-11-05 10:31:27', 1, '2020-11-05 19:41:26'),
(2, '1', '2', 'yes', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'text', NULL, NULL, 0, '', NULL, '2020-11-05 10:32:34', 1, '2020-11-05 19:41:36'),
(3, '2', '3', 'Hello', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'text', NULL, NULL, 0, '', NULL, '2020-11-05 10:32:59', 1, '2020-11-05 20:47:59'),
(4, '2', '3', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/chats.jpeg', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'image', 'jpeg', '5fa3d4e897321', 0, '', '74249', '2020-11-05 10:33:12', 1, '2020-11-05 20:47:59'),
(5, '2', '3', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Foo-Bar.ppt', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'file', 'ppt', '5fa3d507d0a1f', 0, '', '63488', '2020-11-05 10:33:43', 1, '2020-11-05 20:47:59'),
(6, '1', '2', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/ImpactReport_Community&Health.pdf', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'file', 'pdf', '5fa3d5458bcbc', 0, '', '3491415', '2020-11-05 10:34:45', 1, '2020-11-05 19:41:36'),
(7, '1', '2', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/1-intro-photo-final.jpg', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'image', 'jpg', '5fa3d551f2568', 0, '', '48446', '2020-11-05 10:34:57', 1, '2020-11-05 19:41:36'),
(8, '2', '1', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Hello-World.xlsx', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'file', 'xlsx', '5fa3d55d4b44d', 0, '', '8379', '2020-11-05 10:35:09', 1, '2020-11-05 19:41:26'),
(9, '1', '2', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/1-intro-photo-final.jpg', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'image', 'jpg', '5fa3d56386430', 0, '', '48446', '2020-11-05 10:35:15', 1, '2020-11-05 19:41:36'),
(10, '1', '2', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/butterfly-floor-mop-500x500.jpg', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'image', 'jpg', '5fa3d56386430', 0, '', '22143', '2020-11-05 10:35:15', 1, '2020-11-05 19:41:36'),
(11, '2', '1', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Listing.csv', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'file', 'csv', '5fa3d563c73df', 0, '', '12', '2020-11-05 10:35:15', 1, '2020-11-05 19:41:26'),
(12, '2', '1', 'Yes', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'text', NULL, NULL, 0, '5fa3d56386430', NULL, '2020-11-05 10:35:28', 1, '2020-11-05 19:41:26'),
(13, '2', '1', 'with text and attachment', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'textMix', '', '5fa3d5851dcd9', 0, '5fa3d56386430', '', '2020-11-05 10:35:49', 1, '2020-11-05 19:41:26'),
(14, '2', '1', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Foo-Bar.xls', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'fileMix', 'xls', '5fa3d5851dcd9', 0, '5fa3d56386430', '18944', '2020-11-05 10:35:49', 1, '2020-11-05 19:41:26'),
(15, '1', '2', 'Image ni kan', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'textMix', '', '5fa3d5a89cadf', 0, '5fa3d5851dcd9', '', '2020-11-05 10:36:24', 1, '2020-11-05 19:41:36'),
(16, '1', '2', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/ImpactReport_Environment.pdf', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'fileMix', 'pdf', '5fa3d5a89cadf', 0, '5fa3d5851dcd9', '3965573', '2020-11-05 10:36:24', 1, '2020-11-05 19:41:36'),
(17, '2', '1', 'Together with another attachment ', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'textMix', '', '5fa3d5b2439ed', 0, '6', '', '2020-11-05 10:36:34', 1, '2020-11-05 19:41:26'),
(18, '2', '1', 'This message is reply with files attachment', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'textMix', '', '5fa3d5e43ae38', 0, '6', '', '2020-11-05 10:37:24', 1, '2020-11-05 19:41:26'),
(19, '2', '1', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Make Time Pay (MTP).pdf', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'fileMix', 'pdf', '5fa3d5e43ae38', 0, '6', '2371146', '2020-11-05 10:37:24', 1, '2020-11-05 19:41:26'),
(20, '2', '1', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/chat-footer.png', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'image', 'png', '5fa3d5f9ced77', 0, '', '99652', '2020-11-05 10:37:45', 1, '2020-11-05 19:41:26'),
(21, '2', '1', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/pexels-photo-220453.jpeg', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'image', 'jpeg', '5fa3d5f9ced77', 0, '', '25275', '2020-11-05 10:37:45', 1, '2020-11-05 19:41:26'),
(22, '2', '1', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/profile-fb-1524636056-f77a465db24d.jpg', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'image', 'jpg', '5fa3d5f9ced77', 0, '', '4331', '2020-11-05 10:37:45', 1, '2020-11-05 19:41:26'),
(23, '1', '2', 'what?', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'textMix', '', '5fa3d6004fdfd', 0, '', '', '2020-11-05 10:37:52', 1, '2020-11-05 19:41:36'),
(24, '1', '2', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/download.jpg', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'imageMix', 'jpg', '5fa3d6004fdfd', 0, '', '6958', '2020-11-05 10:37:52', 1, '2020-11-05 19:41:36'),
(25, '2', '1', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et.', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'text', NULL, NULL, 0, '', NULL, '2020-11-05 10:39:56', 1, '2020-11-05 19:41:26'),
(26, '1', '2', 'hello', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'text', NULL, NULL, 0, '', NULL, '2020-11-05 10:39:58', 1, '2020-11-05 19:41:36'),
(27, '1', '2', 'hai', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'text', NULL, NULL, 0, '', NULL, '2020-11-05 10:40:00', 1, '2020-11-05 19:41:36'),
(28, '2', '1', 'Get the broom.Sweep the floor.Get paid.', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'text', NULL, NULL, 0, '', NULL, '2020-11-05 10:40:08', 1, '2020-11-05 19:41:26'),
(29, '4', '2', 'test', '42ACC3E3-2A22-457E-8A22-034780D791DE', 'text', NULL, NULL, 0, '', NULL, '2020-11-05 10:43:26', 1, '2020-11-05 19:03:50'),
(30, '4', '2', 'hello', '42ACC3E3-2A22-457E-8A22-034780D791DE', 'text', NULL, NULL, 0, '', NULL, '2020-11-05 10:43:37', 1, '2020-11-05 19:03:50'),
(31, '3', '2', 'pdf', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'textMix', '', '5fa3d7619e89d', 0, '5', '', '2020-11-05 10:43:45', 1, '2020-11-05 18:55:39'),
(32, '3', '2', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/ROBINHO-profile-20201019 (1).pdf', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'fileMix', 'pdf', '5fa3d7619e89d', 0, '5', '367773', '2020-11-05 10:43:45', 1, '2020-11-05 18:55:39'),
(33, '2', '1', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et.', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'textMix', '', '5fa3d7805714f', 0, '', '', '2020-11-05 10:44:16', 1, '2020-11-05 19:41:26'),
(34, '2', '1', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/03-11-2020-public-homepage.txt', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'fileMix', 'txt', '5fa3d7805714f', 0, '', '564', '2020-11-05 10:44:16', 1, '2020-11-05 19:41:26'),
(35, '2', '1', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/chat-footer.png', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'imageMix', 'png', '5fa3d7805714f', 0, '', '99652', '2020-11-05 10:44:16', 1, '2020-11-05 19:41:26'),
(36, '2', '1', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Foo-Bar.xls', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'fileMix', 'xls', '5fa3d7805714f', 0, '', '18944', '2020-11-05 10:44:16', 1, '2020-11-05 19:41:26'),
(37, '2', '1', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Hello-World.xlsx', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'fileMix', 'xlsx', '5fa3d7805714f', 0, '', '8379', '2020-11-05 10:44:16', 1, '2020-11-05 19:41:26'),
(38, '2', '1', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Listing.csv', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'fileMix', 'csv', '5fa3d7805714f', 0, '', '12', '2020-11-05 10:44:16', 1, '2020-11-05 19:41:26'),
(39, '2', '1', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Make Time Pay (MTP).pdf', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'fileMix', 'pdf', '5fa3d7805714f', 0, '', '2371146', '2020-11-05 10:44:16', 1, '2020-11-05 19:41:26'),
(40, '2', '3', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/chats.jpeg', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'image', 'jpeg', '5fa3d9ae3c0be', 0, '', '74249', '2020-11-05 10:53:34', 1, '2020-11-05 20:47:59'),
(41, '2', '3', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Foo-Bar.xls', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'file', 'xls', '5fa3d9ae3c0be', 0, '', '18944', '2020-11-05 10:53:34', 1, '2020-11-05 20:47:59'),
(42, '2', '3', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Hello-World.pptx', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'file', 'pptx', '5fa3d9ae3c0be', 0, '', '30314', '2020-11-05 10:53:34', 1, '2020-11-05 20:47:59'),
(43, '2', '3', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Hello-World.xlsx', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'file', 'xlsx', '5fa3d9ae3c0be', 0, '', '8379', '2020-11-05 10:53:34', 1, '2020-11-05 20:47:59'),
(44, '2', '3', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Listing.csv', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'file', 'csv', '5fa3d9ae3c0be', 0, '', '12', '2020-11-05 10:53:34', 1, '2020-11-05 20:47:59'),
(45, '2', '3', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Make Time Pay (MTP).pdf', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'file', 'pdf', '5fa3d9ae3c0be', 0, '', '2371146', '2020-11-05 10:53:34', 1, '2020-11-05 20:47:59'),
(46, '1', '2', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/Hello-World.xlsx', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'file', 'xlsx', '5fa3d9cd10b9c', 0, '', '8379', '2020-11-05 10:54:05', 1, '2020-11-05 19:41:36'),
(47, '3', '2', 'files/chats/A0702622-963C-4AFA-873C-00EF58BF7859/b2c.png', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'image', 'png', '5fa3da29e652a', 0, '45', '15302', '2020-11-05 10:55:37', 1, '2020-11-05 18:55:39'),
(48, '1', '2', 'syafiq', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'text', NULL, NULL, 0, '', NULL, '2020-11-05 11:17:31', 1, '2020-11-05 19:41:36'),
(49, '2', '1', '.btn-primary.btn-send-msg', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'text', NULL, NULL, 0, '48', NULL, '2020-11-05 11:18:02', 1, '2020-11-05 19:41:26');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `post_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'If it''s a reply of another comment',
  `comment` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `viewed` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `parent_id`, `comment`, `viewed`, `created_at`, `updated_at`) VALUES
(1, '1', 'A0702622-963C-4AFA-873C-00EF58BF7859', 0, 'Hello\r\n', 'Y', '2020-11-05 10:23:47', '2020-11-05 10:23:59'),
(2, '1', 'A0702622-963C-4AFA-873C-00EF58BF7859', 0, 'hello', 'Y', '2020-11-05 10:23:56', '2020-11-05 10:23:59'),
(3, '1', 'A0702622-963C-4AFA-873C-00EF58BF7859', 0, 'hello', 'Y', '2020-11-05 10:24:09', '2020-11-05 10:24:36'),
(4, '1', 'A0702622-963C-4AFA-873C-00EF58BF7859', 0, 'hello', 'Y', '2020-11-05 10:24:26', '2020-11-05 10:24:36'),
(5, '2', 'A0702622-963C-4AFA-873C-00EF58BF7859', 2, 'Hello', 'Y', '2020-11-05 10:24:28', '2020-11-05 10:24:36'),
(6, '2', 'A0702622-963C-4AFA-873C-00EF58BF7859', 3, 'Yes', 'Y', '2020-11-05 10:24:44', '2020-11-05 10:24:50'),
(7, '1', 'A0702622-963C-4AFA-873C-00EF58BF7859', 0, 'h4llo', 'Y', '2020-11-05 10:30:05', '2020-11-05 10:31:03');

-- --------------------------------------------------------

--
-- Table structure for table `company_details`
--

CREATE TABLE `company_details` (
  `id` int(11) NOT NULL,
  `name` text,
  `reg_num` text,
  `email` text,
  `company_phone_prefix` varchar(8) NOT NULL,
  `company_phone` varchar(16) NOT NULL,
  `about_us` text NOT NULL,
  `industry` text,
  `photos` text NOT NULL,
  `member_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_details`
--

INSERT INTO `company_details` (`id`, `name`, `reg_num`, `email`, `company_phone_prefix`, `company_phone`, `about_us`, `industry`, `photos`, `member_id`) VALUES
(1, 'InspireNow Sdn Bhd', '83834343-X', 'info@dataristic.com', '03', '383033020', '', 'Technology', 'a:0:{}', 5),
(2, 'Byte2c', 'ASDAD-12', NULL, '', '', '', 'Oil & Gas', '', 6),
(3, 'Byte2c', 'ASDAD-12', NULL, '', '', '', 'Technology', '', 7);

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `type` text,
  `doc_location` text,
  `member_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE `education` (
  `id` int(11) NOT NULL,
  `highest_edu_level` text,
  `grad_year` text,
  `edu_institution` text,
  `field` text,
  `major` text,
  `grade` text,
  `country` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`id`, `highest_edu_level`, `grad_year`, `edu_institution`, `field`, `major`, `grade`, `country`, `state`, `member_id`) VALUES
(1, 'PhD', '2017', 'UTM', 'Computer Science', 'Artificial Intelligence', '4.00', 0, 0, 2),
(2, 'Diploma', '2016', 'UKM', 'Computer Science', 'Electrical', '3.00', 0, 0, 2),
(3, 'Degree', '2016', 'UKM', 'Business', 'IT', '3.9', 0, 0, 1),
(4, 'Certification', '2018', 'UPM', 'Computer Science', 'IT', '2.3', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `employment_details`
--

CREATE TABLE `employment_details` (
  `id` int(11) NOT NULL,
  `company_name` text,
  `position` text,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `currently_working` int(11) NOT NULL,
  `department` text,
  `job_desc` text,
  `job_responsibilities` text,
  `remuneration` text,
  `country` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `industry` text,
  `member_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employment_details`
--

INSERT INTO `employment_details` (`id`, `company_name`, `position`, `date_start`, `date_end`, `currently_working`, `department`, `job_desc`, `job_responsibilities`, `remuneration`, `country`, `state`, `industry`, `member_id`) VALUES
(1, 'Inspirenow Sdn Bhd', 'UI/UX Developer', '2017-04-14', '2020-11-05', 0, 'Technical', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '10000', 1945, 0, 'Engineering', 2),
(2, 'Inspirenow Sdn Bhd', 'Web developer', '2012-07-29', '2020-11-05', 0, 'IT', 'Setting up workstations with computers and necessary peripheral devices\r\n\r\nChecking computer hardware (HDD, mouses, keyboards etc.) to ensure functionality\r\n\r\nInstalling and configuring appropriate software and functions according to specifications', 'Set up workstations with computers and necessary peripheral devices (routers, printers etc.)\r\n\r\nCheck computer hardware (HDD, mouses, keyboards etc.) to ensure functionality\r\n\r\nInstall and configure appropriate software and functions according to specifications\r\n\r\nDevelop and maintain local networks in ways that optimize performance\r\n\r\nEnsure security and privacy of networks and computer systems\r\n\r\nProvide orientation and guidance to users on how to operate new software and computer equipment\r\n\r\nOrganize and schedule upgrades and maintenance without deterring others from completing their work\r\n\r\nPerform troubleshooting to diagnose and resolve problems (repair or replace', '5000', 1949, 0, 'Human Resource', 1),
(3, 'INKA Digital Agency', 'UI/UX Developer', '2017-03-27', '2020-11-26', 0, 'Creative', 'lorem ipsum dolor sit amet\r\nlorem ipsum dolor sit amet\r\nlorem ipsum dolor sit amet\r\n\r\nlorem ipsum dolor sit amet\r\n\r\nlorem ipsum dolor sit amet', 'lorem ipsum dolor sit amet\r\nlorem ipsum dolor sit amet\r\nlorem ipsum dolor sit amet\r\n\r\nlorem ipsum dolor sit amet\r\n\r\nlorem ipsum dolor sit amet', '8995', 1937, 0, 'Human Resource', 2);

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
  `id` int(11) NOT NULL,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `type` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'task',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `favourites`
--

INSERT INTO `favourites` (`id`, `user_id`, `task_id`, `type`, `created_at`, `updated_at`) VALUES
(17, '1', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'task', '2020-11-05 10:31:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `number` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `sub_category` int(11) NOT NULL,
  `title` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `requirements` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `experience_level` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `location` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `salary_range_min` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salary_range_max` decimal(10,2) NOT NULL DEFAULT '0.00',
  `benefits` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `status` enum('draft','published','completed','cancelled') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `assigned` timestamp NULL DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job_questions`
--

CREATE TABLE `job_questions` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `job_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `fb_uid` text,
  `google_id` text NOT NULL,
  `fb_email` text,
  `email` text,
  `password` text,
  `photo` text,
  `salutation` text,
  `firstname` text,
  `lastname` text,
  `contact` text,
  `address1` text,
  `address2` text,
  `zip` text,
  `city` text,
  `country` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `nric` text,
  `gender` text,
  `dob` text,
  `nationality` text,
  `contact_number_prefix` text,
  `contact_number` text,
  `mobile_number_prefix` text,
  `mobile_number` text,
  `profile_pic` text,
  `insta_id` text,
  `other_social_media` text,
  `emergency_contact_name` text,
  `emergency_contact_number_prefix` text,
  `emergency_contact_number` text,
  `emergency_contact_relation` text,
  `contact_method` varchar(32) NOT NULL,
  `about` text,
  `skills` text NOT NULL,
  `language` text,
  `dashboard_widget` varchar(128) DEFAULT '2,4,5,6',
  `completion` tinyint(4) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT '0' COMMENT '0- Active 99- Suspended',
  `type` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `fb_uid`, `google_id`, `fb_email`, `email`, `password`, `photo`, `salutation`, `firstname`, `lastname`, `contact`, `address1`, `address2`, `zip`, `city`, `country`, `state`, `nric`, `gender`, `dob`, `nationality`, `contact_number_prefix`, `contact_number`, `mobile_number_prefix`, `mobile_number`, `profile_pic`, `insta_id`, `other_social_media`, `emergency_contact_name`, `emergency_contact_number_prefix`, `emergency_contact_number`, `emergency_contact_relation`, `contact_method`, `about`, `skills`, `language`, `dashboard_widget`, `completion`, `date_created`, `status`, `type`) VALUES
(1, NULL, '', NULL, 'shah@inspirenow.com.my', '$2a$07$np65plSrOACM8PPpL57zcOmDOOmbjunuWgz.Ot5UZeARX4tubWoqG', 'files/profile/1/gambar baru_5fa3cc2d5c6a8.jpg', NULL, 'shah', 'ramly', NULL, '4-8-3 Putra one apartment, Jalan persiaran putra 2', NULL, NULL, '76423', 132, 1943, '830629075433', 'male', '1983-06-29', 'malaysian', '03', '4949393992', '019', '39399399393', NULL, NULL, NULL, 'Syafiq', '', '', 'Friend', '', 'I am a person who is positive about every aspect of life. There are many things I like to do, to see, and to experience. I like to read, I like to write; I like to think, I like to dream; I like to talk, I like to listen. I like to see the sunrise in the morning, I like to see the moonlight at night; I like to feel the music flowing on my face, I like to smell the wind coming from the ocean. I like to look at the clouds in the sky with a blank mind, I like to do thought experiment when I cannot sleep in the middle of the night. I like flowers in spring, rain in summer, leaves in autumn, and snow in winter. I like to sleep early, I like to get up late; I like to be alone, I like to be surrounded by people. I like country’s peace, I like metropolis’ noise; I like the beautiful west lake in Hangzhou, I like the flat cornfield in Champaign. I like delicious food and comfortable shoes; I like good books and romantic movies. I like the land and the nature, I like people. And, I like to laugh.', '1,75,202', 'English,Malay,Tamil', '2,4,5,6', 100, '2020-11-05 17:18:21', 0, 0),
(2, NULL, '', NULL, 'syafiq@inspirenow.com.my', '$2a$07$np65plSrOACM8PPpL57zcOmDOOmbjunuWgz.Ot5UZeARX4tubWoqG', 'files/profile/2/profile-fb-12312312314_5fa3cc511289d.jpg', NULL, 'Syafiq', 'Syazre', NULL, '21-02 Vertical Business Suite', NULL, NULL, '76514', 132, 1947, '900714016671', NULL, '1990-07-14', NULL, '03', '224113231', '012', '756345785', NULL, NULL, NULL, 'John Doe', '', '', 'Friend', '', 'I am ambitious and driven. I thrive on challenge and constantly set goals for myself, so I have something to strive toward. I\'m not comfortable with settling, and I\'m always looking for an opportunity to do better and achieve greatness.', '1,24,161,226,234,202,83,62,66,75,208,151,185', 'English,Malay,Chinese,Tamil', '2,4,5,6', 100, '2020-11-05 17:19:00', 0, 0),
(3, NULL, '114604337756719687566', NULL, 'kevinchew@itechbs.com', '$2a$07$np65plSrOACM8PPpL57zcO3cdlky.WAho2G.4WUFOaxHRvNWndTmS', 'files/3/profile/googleProfilePhoto.jpg', NULL, 'Kevin', 'Chew', NULL, '', NULL, NULL, '76419', 132, 1944, '840630145493', NULL, '1984-06-30', 'malaysian', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', '', '1', 'Malay', '2,4,5,6', 100, '2020-11-05 17:43:56', 1, 0),
(4, '10158710870049321', '', NULL, 'skaterzee@gmail.com', NULL, 'files/4/profile/fbProfilePhoto.jpg', NULL, 'Farid', 'Edil', NULL, '', NULL, NULL, '76411', 132, 1944, '880831888888', NULL, '1988-08-31', NULL, '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', '', '1,24', 'English,Malay', '2,4,5,6', 100, '2020-11-05 17:49:22', 1, 0),
(5, NULL, '', NULL, 'syafiq.inspirenow@gmail.com', '$2a$07$np65plSrOACM8PPpL57zcOmDOOmbjunuWgz.Ot5UZeARX4tubWoqG', 'files/profile/5/profile_5fa3ed904813e.png', NULL, 'Mohamad', 'Syafiq', NULL, 'A-21-05 Vertical Business Suite', 'Jalan Kerinchi', '59200', 'Bangsar South', 132, 1949, '902001010202', NULL, NULL, NULL, '03', '22422140', '012', '732829239', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '5,8,9,11', 70, '2020-11-05 20:12:35', 1, 1),
(7, NULL, '', NULL, 'kevin@byte2c.com', '$2a$07$np65plSrOACM8PPpL57zcO.e9ubd6kyHkmSW7DRIPn9sgEUgwpvmq', 'assets/img/default-avatar.png', NULL, 'Kevin', 'Chew', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '5,8,9,11', 50, '2020-11-06 01:05:41', 1, 1),
(8, NULL, '115577827226804288990', NULL, 'abdulsalam.masoud@gmail.com', NULL, 'files/8/profile/googleProfilePhoto.jpg', NULL, 'Abdulsalam', 'Masoud', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1,83,3,11,9,23', NULL, '2,4,5,6', 60, '2020-11-06 04:29:35', 1, 0),
(9, NULL, '', NULL, 'kevinmvp@gmail.com', NULL, 'assets/img/default-avatar.png', NULL, 'Kevin', 'Gmail', NULL, NULL, NULL, NULL, '76561', 132, 1944, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '2,4,5,6', 0, '2020-11-06 10:51:30', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `member_docs`
--

CREATE TABLE `member_docs` (
  `member_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `src` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `member_docs`
--

INSERT INTO `member_docs` (`member_id`, `type`, `src`) VALUES
(2, 'resume', 'files/resume/2/Make Time Pay (MTP)_5fa3cc58492c2.pdf'),
(2, 'cover_letter', 'files/cover_letter/2/Make Time Pay (MTP)_5fa3cc6226129.pdf'),
(2, 'cover_letter', 'files/cover_letter/2/make_time_pay_task_flow_horizontal_5fa3cc67c337f.pdf'),
(2, 'cert', 'files/cert/2/Make Time Pay (MTP)_5fa3cc6e8d5a0.pdf'),
(1, 'resume', 'files/resume/1/ImpactReport_Arts&Culture_5fa3cc3d79249.pdf'),
(1, 'cover_letter', 'files/cover_letter/1/ImpactReport_Community&Health_5fa3cc404e46b.pdf'),
(1, 'cert', 'files/cert/1/ImpactReport_Environment_5fa3cc4335d5d.pdf'),
(4, 'resume', 'files/resume/4/2 (3)_5fa3cd359c7db.png');

-- --------------------------------------------------------

--
-- Table structure for table `member_logs`
--

CREATE TABLE `member_logs` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `ip_address` text NOT NULL,
  `user_agent` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_logs`
--

INSERT INTO `member_logs` (`id`, `member_id`, `date`, `ip_address`, `user_agent`) VALUES
(1, 19, '2020-11-05 16:57:47', '115.133.25.79', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(2, 1, '2020-11-05 17:22:29', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(3, 2, '2020-11-05 17:22:33', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(4, 1, '2020-11-05 17:23:00', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(5, 2, '2020-11-05 17:23:04', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(6, 2, '2020-11-05 17:23:12', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(7, 2, '2020-11-05 17:23:17', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(8, 1, '2020-11-05 17:23:31', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(9, 1, '2020-11-05 17:23:32', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(10, 1, '2020-11-05 17:23:35', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(11, 1, '2020-11-05 17:23:36', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(12, 1, '2020-11-05 17:23:37', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(13, 1, '2020-11-05 17:23:41', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(14, 2, '2020-11-05 17:23:43', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(15, 1, '2020-11-05 17:23:43', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(16, 1, '2020-11-05 17:24:03', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(17, 1, '2020-11-05 17:24:07', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(18, 1, '2020-11-05 17:24:09', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(19, 1, '2020-11-05 17:24:14', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(20, 1, '2020-11-05 17:24:15', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(21, 1, '2020-11-05 17:24:17', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(22, 1, '2020-11-05 17:24:18', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(23, 1, '2020-11-05 17:24:20', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(24, 1, '2020-11-05 17:25:18', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(25, 2, '2020-11-05 17:25:21', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(26, 1, '2020-11-05 17:25:23', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(27, 1, '2020-11-05 17:25:32', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(28, 2, '2020-11-05 17:25:39', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(29, 1, '2020-11-05 17:25:55', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(30, 2, '2020-11-05 17:26:25', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(31, 1, '2020-11-05 17:26:39', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(32, 2, '2020-11-05 17:26:55', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(33, 2, '2020-11-05 17:27:15', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(34, 1, '2020-11-05 17:27:15', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(35, 2, '2020-11-05 17:27:48', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(36, 3, '2020-11-05 17:44:00', '115.133.25.79', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(37, 1, '2020-11-05 17:56:02', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(38, 2, '2020-11-05 17:56:03', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(39, 1, '2020-11-05 17:56:23', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(40, 1, '2020-11-05 17:56:50', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(41, 1, '2020-11-05 17:57:15', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(42, 1, '2020-11-05 17:58:28', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(43, 1, '2020-11-05 17:58:50', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(44, 1, '2020-11-05 17:59:14', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(45, 1, '2020-11-05 18:01:22', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(46, 2, '2020-11-05 18:02:46', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(47, 2, '2020-11-05 18:03:00', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(48, 1, '2020-11-05 18:03:34', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(49, 1, '2020-11-05 18:03:51', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(50, 1, '2020-11-05 18:04:08', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(51, 2, '2020-11-05 18:04:11', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(52, 2, '2020-11-05 18:04:24', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(53, 1, '2020-11-05 18:04:55', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(54, 2, '2020-11-05 18:05:01', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(55, 1, '2020-11-05 18:07:21', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(56, 2, '2020-11-05 18:13:32', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(57, 1, '2020-11-05 18:13:33', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(58, 1, '2020-11-05 18:20:25', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(59, 2, '2020-11-05 18:23:36', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(60, 3, '2020-11-05 18:26:40', '115.133.25.79', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(61, 3, '2020-11-05 18:27:05', '115.133.25.79', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(62, 4, '2020-11-05 18:41:27', '115.132.199.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(63, 4, '2020-11-05 18:41:28', '115.132.199.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(64, 4, '2020-11-05 18:41:54', '115.132.199.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(65, 4, '2020-11-05 19:01:34', '115.132.199.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(66, 1, '2020-11-05 19:16:59', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(67, 1, '2020-11-05 19:43:40', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(68, 1, '2020-11-05 19:46:50', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(69, 1, '2020-11-05 19:48:09', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(70, 1, '2020-11-05 19:48:57', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(71, 1, '2020-11-05 19:49:05', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(72, 2, '2020-11-05 19:49:13', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(73, 2, '2020-11-05 19:49:33', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(74, 2, '2020-11-05 19:50:07', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(75, 2, '2020-11-05 19:50:53', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(76, 2, '2020-11-05 19:52:01', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(77, 2, '2020-11-05 19:53:19', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(78, 1, '2020-11-05 19:58:20', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(79, 2, '2020-11-05 19:58:39', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(80, 3, '2020-11-05 20:04:32', '115.133.25.79', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(81, 5, '2020-11-05 20:14:08', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(82, 5, '2020-11-05 20:16:23', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(83, 5, '2020-11-05 20:32:15', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(84, 5, '2020-11-05 20:32:39', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(85, 4, '2020-11-05 20:45:24', '115.132.199.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(86, 4, '2020-11-05 20:45:24', '115.132.199.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(87, 4, '2020-11-05 20:46:45', '115.132.199.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(88, 3, '2020-11-05 20:47:40', '115.132.199.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(89, 3, '2020-11-05 20:51:10', '115.132.199.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(90, 7, '2020-11-06 01:06:20', '115.133.25.79', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(91, 7, '2020-11-06 04:21:35', '115.133.25.79', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.61'),
(92, 1, '2020-11-06 04:25:21', '115.133.25.79', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(93, 1, '2020-11-06 04:27:47', '115.133.25.79', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(94, 8, '2020-11-06 04:29:57', '210.195.46.138', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(95, 2, '2020-11-06 11:28:53', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(96, 5, '2020-11-06 11:32:28', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(97, 5, '2020-11-06 11:33:02', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(98, 1, '2020-11-06 13:52:10', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(99, 2, '2020-11-06 13:53:07', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(100, 1, '2020-11-06 13:58:03', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(101, 1, '2020-11-06 13:59:11', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(102, 4, '2020-11-06 14:36:56', '115.132.199.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(103, 4, '2020-11-06 14:36:56', '115.132.199.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(104, 4, '2020-11-06 14:51:23', '115.132.199.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(105, 1, '2020-11-06 14:56:28', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(106, 1, '2020-11-06 16:24:20', '183.171.110.128', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(107, 3, '2020-11-06 16:24:32', '183.171.110.128', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(108, 3, '2020-11-06 16:24:32', '183.171.110.128', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(109, 7, '2020-11-06 16:24:59', '183.171.110.128', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.61'),
(110, 1, '2020-11-06 16:37:44', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(111, 1, '2020-11-06 16:38:09', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(112, 1, '2020-11-06 16:38:41', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(113, 2, '2020-11-06 16:41:13', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(114, 2, '2020-11-06 17:01:04', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'),
(115, 1, '2020-11-06 17:03:52', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(116, 4, '2020-11-06 22:50:04', '115.132.199.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(117, 4, '2020-11-06 22:50:04', '115.132.199.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(118, 2, '2020-11-07 01:07:27', '210.195.14.197', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_requests`
--

CREATE TABLE `payment_requests` (
  `id` int(11) NOT NULL,
  `task_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `times` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pinned_messages`
--

CREATE TABLE `pinned_messages` (
  `id` int(11) NOT NULL,
  `message_id` text,
  `pinned_by` int(11) DEFAULT NULL,
  `pinned_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `task_id` text,
  `message_type` text,
  `pin_status` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pinned_messages`
--

INSERT INTO `pinned_messages` (`id`, `message_id`, `pinned_by`, `pinned_time`, `task_id`, `message_type`, `pin_status`) VALUES
(3, '26', 2, '2020-11-05 10:40:17', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'text', 0),
(8, '4', 3, '2020-11-05 10:42:28', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'image', 0),
(10, '30', 4, '2020-11-05 11:01:23', '42ACC3E3-2A22-457E-8A22-034780D791DE', 'text', 0),
(11, '29', 4, '2020-11-05 11:01:24', '42ACC3E3-2A22-457E-8A22-034780D791DE', 'text', 0),
(18, '25', 1, '2020-11-05 11:08:52', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'text', 0);

-- --------------------------------------------------------

--
-- Table structure for table `poster_reviews`
--

CREATE TABLE `poster_reviews` (
  `id` int(11) NOT NULL,
  `task_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `reviewer_id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `review` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `timeliness` tinyint(4) NOT NULL DEFAULT '0',
  `expertise` tinyint(4) NOT NULL DEFAULT '0',
  `satisfactory` tinyint(4) NOT NULL DEFAULT '0',
  `easy` tinyint(4) NOT NULL DEFAULT '0',
  `viewed` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `poster_reviews`
--

INSERT INTO `poster_reviews` (`id`, `task_id`, `reviewer_id`, `talent_id`, `review`, `timeliness`, `expertise`, `satisfactory`, `easy`, `viewed`, `created_at`, `updated_at`) VALUES
(1, 'A0702622-963C-4AFA-873C-00EF58BF7859', 2, 1, 'Good job!', 9, 9, 9, 9, 'N', '2020-11-05 11:45:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `preference`
--

CREATE TABLE `preference` (
  `id` int(11) NOT NULL,
  `rm_work_location` text COMMENT 'rm- Remote Work',
  `rm_radius` varchar(32) NOT NULL,
  `rm_country` text,
  `rm_state` text,
  `p_work_location` text COMMENT 'p - physical ',
  `p_radius` varchar(32) NOT NULL,
  `p_country` text,
  `p_state` text,
  `work_day` text,
  `work_day_full` int(11) NOT NULL,
  `work_day_after` int(11) NOT NULL,
  `work_day_am` int(11) NOT NULL,
  `work_day_pm` int(11) NOT NULL,
  `working_hours` text,
  `work_load_hours` int(11) NOT NULL,
  `expected_earning` text,
  `bank` varchar(64) NOT NULL,
  `acc_name` varchar(128) NOT NULL,
  `acc_num` varchar(32) NOT NULL,
  `member_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `preference`
--

INSERT INTO `preference` (`id`, `rm_work_location`, `rm_radius`, `rm_country`, `rm_state`, `p_work_location`, `p_radius`, `p_country`, `p_state`, `work_day`, `work_day_full`, `work_day_after`, `work_day_am`, `work_day_pm`, `working_hours`, `work_load_hours`, `expected_earning`, `bank`, `acc_name`, `acc_num`, `member_id`) VALUES
(1, NULL, '', NULL, NULL, NULL, '', NULL, NULL, NULL, 0, 0, 0, 0, NULL, 0, NULL, '', '', '', 1),
(2, 'anywhere', '', '132', '', 'anywhere', '', '132', '', 'allWeek', 0, 0, 0, 0, NULL, 50, NULL, '', '', '', 2),
(3, NULL, '', NULL, NULL, NULL, '', NULL, NULL, NULL, 0, 0, 0, 0, NULL, 0, NULL, '', '', '', 3),
(4, NULL, '', NULL, NULL, NULL, '', NULL, NULL, NULL, 0, 0, 0, 0, NULL, 0, NULL, '', '', '', 4),
(5, NULL, '', NULL, NULL, NULL, '', NULL, NULL, NULL, 0, 0, 0, 0, NULL, 0, NULL, '', '', '', 5),
(6, NULL, '', NULL, NULL, NULL, '', NULL, NULL, NULL, 0, 0, 0, 0, NULL, 0, NULL, '', '', '', 7),
(7, NULL, '', NULL, NULL, NULL, '', NULL, NULL, NULL, 0, 0, 0, 0, NULL, 0, NULL, '', '', '', 8);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `code` text NOT NULL,
  `images` text NOT NULL,
  `title` text NOT NULL,
  `model` text NOT NULL,
  `description` text NOT NULL,
  `description2` text NOT NULL,
  `size` text NOT NULL,
  `material` text NOT NULL,
  `color` text NOT NULL,
  `fabric` text NOT NULL,
  `category` int(11) NOT NULL,
  `categories` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `price_original` decimal(10,2) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `prices` text NOT NULL,
  `options` text NOT NULL,
  `colors` text NOT NULL,
  `is_new` int(11) NOT NULL,
  `sale_price` decimal(10,2) NOT NULL,
  `product_suggestion` text NOT NULL,
  `date_added` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promo_code`
--

CREATE TABLE `promo_code` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `category_id` text NOT NULL,
  `product_id` text NOT NULL,
  `type` text NOT NULL,
  `value` int(11) NOT NULL,
  `code` text NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `qty` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `search`
--

CREATE TABLE `search` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `params` json NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'meta_keyword', 'nicchris,furniture'),
(2, 'meta_description', 'Meta Description'),
(3, 'logo', 'upload/images/logo/NC_ONLY.png'),
(4, 'email', 'admin@nicchris.com'),
(5, 'facebook', 'https://www.facebook.com/nicchris'),
(6, 'instagram', 'https://instagram.com/nicchris'),
(7, 'google_analytics', ''),
(8, 'slider', 'a:5:{i:0;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/170911.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}i:1;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/170915.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}i:2;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/170919.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}i:3;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/170922.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}i:4;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/171018.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}}'),
(9, 'site_name', 'MTP'),
(10, 'whatsapp', ''),
(11, 'new_products', '1,3,7,8,53'),
(12, 'featured_products', '3,6,9');

-- --------------------------------------------------------

--
-- Table structure for table `shopping_cart`
--

CREATE TABLE `shopping_cart` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_option` text NOT NULL,
  `item_color` text NOT NULL,
  `item_ship_country` int(11) NOT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `promo_code` text NOT NULL,
  `date` datetime NOT NULL,
  `member_id` int(11) NOT NULL,
  `guest_id` text NOT NULL,
  `checkout_id` int(11) NOT NULL,
  `notified` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shopping_checkout`
--

CREATE TABLE `shopping_checkout` (
  `id` int(11) NOT NULL,
  `carts` text NOT NULL,
  `member_id` int(11) NOT NULL,
  `guest_id` text NOT NULL,
  `total` double NOT NULL,
  `email` text NOT NULL,
  `comment` text NOT NULL,
  `shipping` double NOT NULL,
  `shipping_method` text NOT NULL,
  `shipping_date` date NOT NULL,
  `shipping_time` int(11) NOT NULL,
  `delivery_salutation` text NOT NULL,
  `delivery_firstname` text NOT NULL,
  `delivery_lastname` text NOT NULL,
  `delivery_address1` text NOT NULL,
  `delivery_address2` text NOT NULL,
  `delivery_city` text NOT NULL,
  `delivery_zip` text NOT NULL,
  `delivery_country` int(11) NOT NULL,
  `delivery_state` int(11) NOT NULL,
  `delivery_contact` text NOT NULL,
  `billing_salutation` text NOT NULL,
  `billing_firstname` text NOT NULL,
  `billing_lastname` text NOT NULL,
  `billing_address1` text NOT NULL,
  `billing_address2` text NOT NULL,
  `billing_city` text NOT NULL,
  `billing_zip` text NOT NULL,
  `billing_country` int(11) NOT NULL,
  `billing_state` int(11) NOT NULL,
  `billing_contact` text NOT NULL,
  `pp_token` text NOT NULL,
  `pp_payer_id` text NOT NULL,
  `payment_date` datetime NOT NULL,
  `payment_method` text NOT NULL,
  `payment_reference` longtext NOT NULL,
  `payment_receipts` longtext NOT NULL,
  `status` int(11) NOT NULL,
  `tracking_no` text NOT NULL,
  `pp_GetExpressCheckoutDetails` text NOT NULL,
  `pp_DoExpressCheckoutPayment` text NOT NULL,
  `pp_build` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `talent_reviews`
--

CREATE TABLE `talent_reviews` (
  `id` int(11) NOT NULL,
  `task_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `reviewer_id` int(11) NOT NULL,
  `poster_id` int(11) NOT NULL,
  `review` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `easy` tinyint(4) NOT NULL DEFAULT '0',
  `clear` tinyint(4) NOT NULL DEFAULT '0',
  `flexibility` tinyint(4) NOT NULL DEFAULT '0',
  `trustworthiness` tinyint(4) NOT NULL DEFAULT '0',
  `viewed` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `talent_reviews`
--

INSERT INTO `talent_reviews` (`id`, `task_id`, `reviewer_id`, `poster_id`, `review`, `easy`, `clear`, `flexibility`, `trustworthiness`, `viewed`, `created_at`, `updated_at`) VALUES
(1, 'A0702622-963C-4AFA-873C-00EF58BF7859', 1, 2, 'Boleh la', 7, 8, 8, 8, 'N', '2020-11-05 11:45:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `number` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `sub_category` int(11) NOT NULL,
  `title` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('by-hour','lump-sum') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'by-hour',
  `state_country` text COLLATE utf8_unicode_ci,
  `location` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `budget` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hours` int(11) DEFAULT NULL,
  `start_by` datetime DEFAULT NULL,
  `complete_by` datetime DEFAULT NULL,
  `assigned` datetime DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `status` enum('draft','published','in-progress','completed','accepted','rejected','cancelled','disputed','closed') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `urgent` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `rejection_rate` int(11) NOT NULL DEFAULT '50' COMMENT 'This value is in percentage %',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `number`, `user_id`, `category`, `sub_category`, `title`, `slug`, `description`, `type`, `state_country`, `location`, `budget`, `hours`, `start_by`, `complete_by`, `assigned`, `assigned_to`, `status`, `urgent`, `rejection_rate`, `created_at`, `updated_at`) VALUES
('A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', '2', 2, 18, 'Deliver my food', 'deliver-my-food', 'I need a dispatch rider that can deliver my food to Sri Hartamas. My house located in Shah Alam, Selangor', 'by-hour', 'Wilayah Persekutuan Kuala Lumpur, Malaysia', '2, Jalan 27/70a, Desa Sri Hartamas, 50480 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur, Malaysia', 130.00, 5, '2020-11-09 00:00:00', '2020-11-12 00:00:00', '2020-11-05 19:19:53', 1, 'closed', '0', 50, '2020-11-05 10:20:25', '2020-11-05 11:41:03'),
('42ACC3E3-2A22-457E-8A22-034780D791DE', 'T20102', '2', 3, 21, 'Babysit 2 children', 'babysit-2-children', 'Babysitter needed urgently. Female, 9-6pm monday-friday. Love playing teaching and cooking for two adorable boy toddler age two and four. Come to our house in Kajang, Selangor.', 'lump-sum', 'Kedah, Malaysia', 'Jalan Padang Gaong, Kelibang, 07000 Langkawi, Kedah, Malaysia', 2500.00, 0, '2020-11-10 00:00:00', '2020-11-13 00:00:00', '2020-11-05 19:48:39', 1, 'completed', '0', 10, '2020-11-05 10:30:09', '2020-11-05 11:48:53'),
('355D3AD2-108C-4A50-A759-015850D91C32', 'T20103', '7', 2, 16, 'New Task by Kevin', 'new-task-by-kevin', 'Desccription here', 'by-hour', '', 'remotely', 200.00, 3, '2020-11-07 00:00:00', '2020-11-14 00:00:00', '2020-11-06 16:28:19', 3, 'in-progress', '0', 90, '2020-11-06 08:26:19', '2020-11-06 08:28:19'),
('AB5E0455-6370-45E0-8C19-EF9FFEB7FDBE', 'T20104', '1', 2, 16, 'Cleaning House', 'cleaning-house', 'Aparment at Level 22', 'by-hour', 'Selangor, Malaysia', '7, Jalan Seri Putra 3/13, Bandar Seri Putra, 43000 Kajang, Selangor, Malaysia', 100.00, 2, '2020-11-07 00:00:00', '2020-11-08 00:00:00', NULL, NULL, 'published', '1', 60, '2020-11-06 09:03:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_activity`
--

CREATE TABLE `task_activity` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `task_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_number` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `event` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` tinytext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_activity`
--

INSERT INTO `task_activity` (`id`, `user_id`, `task_id`, `task_number`, `event`, `message`, `created_at`, `updated_at`) VALUES
(1, 2, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'published', '', '2020-11-05 10:20:25', NULL),
(2, 1, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'favourite', '', '2020-11-05 10:25:28', NULL),
(3, 1, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'unfavourite', '', '2020-11-05 10:25:29', NULL),
(4, 1, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'favourite', '', '2020-11-05 10:25:29', NULL),
(5, 1, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'unfavourite', '', '2020-11-05 10:25:50', NULL),
(6, 1, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'favourite', '', '2020-11-05 10:26:08', NULL),
(7, 1, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'unfavourite', '', '2020-11-05 10:29:03', NULL),
(8, 1, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'favourite', '', '2020-11-05 10:29:03', NULL),
(9, 1, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'unfavourite', '', '2020-11-05 10:29:48', NULL),
(10, 2, '42ACC3E3-2A22-457E-8A22-034780D791DE', 'T20102', 'published', '', '2020-11-05 10:30:09', NULL),
(11, 1, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'applied', '', '2020-11-05 10:30:21', NULL),
(12, 1, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'favourite', '', '2020-11-05 10:31:05', NULL),
(13, 3, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'applied', '', '2020-11-05 10:32:35', NULL),
(14, 1, '42ACC3E3-2A22-457E-8A22-034780D791DE', 'T20102', 'favourite', '', '2020-11-05 10:34:18', NULL),
(15, 1, '42ACC3E3-2A22-457E-8A22-034780D791DE', 'T20102', 'unfavourite', '', '2020-11-05 10:34:19', NULL),
(16, 1, '42ACC3E3-2A22-457E-8A22-034780D791DE', 'T20102', 'applied', '', '2020-11-05 10:34:26', NULL),
(17, 4, '42ACC3E3-2A22-457E-8A22-034780D791DE', 'T20102', 'applied', '', '2020-11-05 10:42:58', NULL),
(18, 4, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'applied', '', '2020-11-05 10:43:11', NULL),
(19, 2, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'assigned', '', '2020-11-05 11:19:53', NULL),
(20, 1, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'completed', '', '2020-11-05 11:40:46', NULL),
(21, 2, 'A0702622-963C-4AFA-873C-00EF58BF7859', 'T20101', 'closed', '', '2020-11-05 11:41:03', NULL),
(22, 2, '42ACC3E3-2A22-457E-8A22-034780D791DE', 'T20102', 'assigned', '', '2020-11-05 11:48:39', NULL),
(23, 1, '42ACC3E3-2A22-457E-8A22-034780D791DE', 'T20102', 'completed', '', '2020-11-05 11:48:53', NULL),
(24, 7, '355D3AD2-108C-4A50-A759-015850D91C32', 'T20103', 'created', '', '2020-11-06 08:26:19', NULL),
(25, 3, '355D3AD2-108C-4A50-A759-015850D91C32', 'T20103', 'applied', '', '2020-11-06 08:27:57', NULL),
(26, 7, '355D3AD2-108C-4A50-A759-015850D91C32', 'T20103', 'assigned', '', '2020-11-06 08:28:19', NULL),
(27, 1, 'AB5E0455-6370-45E0-8C19-EF9FFEB7FDBE', 'T20104', 'published', '', '2020-11-06 09:03:49', NULL),
(28, 2, 'AB5E0455-6370-45E0-8C19-EF9FFEB7FDBE', 'T20104', 'applied', '', '2020-11-06 09:04:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_attachments`
--

CREATE TABLE `task_attachments` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_attachments`
--

INSERT INTO `task_attachments` (`id`, `task_id`, `attachment`, `type`, `created_at`, `updated_at`) VALUES
('68DF86BD-0215-46E7-99FD-186425D8FAD8', 'A0702622-963C-4AFA-873C-00EF58BF7859', '/files/tasks/2/chats.jpeg', 'jpeg', '2020-11-05 10:20:25', NULL),
('F5F80288-4D19-4493-8317-C183BB4F5FCA', 'A0702622-963C-4AFA-873C-00EF58BF7859', '/files/tasks/2/Make_Time_Pay__MTP_.pdf', 'pdf', '2020-11-05 10:20:25', NULL),
('B9F1F544-EF2B-4B58-99C7-7323FFAF1E34', '42ACC3E3-2A22-457E-8A22-034780D791DE', '/files/tasks/2/chats.jpeg', 'jpeg', '2020-11-05 10:30:09', NULL),
('9007B7C3-BFD8-4E5A-8922-0AA99C539C32', '42ACC3E3-2A22-457E-8A22-034780D791DE', '/files/tasks/2/Make_Time_Pay__MTP_.pdf', 'pdf', '2020-11-05 10:30:09', NULL),
('B552DF94-16DD-458D-A824-DFF27F159FF3', 'AB5E0455-6370-45E0-8C19-EF9FFEB7FDBE', '/files/tasks/1/chats__5_.jpeg', 'jpeg', '2020-11-06 09:03:49', NULL),
('90B6E4F0-26B6-4F14-B949-72E304205715', 'AB5E0455-6370-45E0-8C19-EF9FFEB7FDBE', '/files/tasks/1/ImpactReport_Education__12_.pdf', 'pdf', '2020-11-06 09:03:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_planning`
--

CREATE TABLE `task_planning` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `task_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_planning`
--

INSERT INTO `task_planning` (`id`, `user_id`, `task_id`, `date`, `start`, `end`, `created_at`, `updated_at`) VALUES
(4, 1, 'A0702622-963C-4AFA-873C-00EF58BF7859', '2020-11-10', '23:31:00', '06:31:00', '2020-11-05 11:33:57', NULL),
(3, 1, 'A0702622-963C-4AFA-873C-00EF58BF7859', '2020-11-09', '23:00:00', '00:00:00', '2020-11-05 11:33:57', NULL),
(5, 1, 'A0702622-963C-4AFA-873C-00EF58BF7859', '2020-11-11', '22:33:00', '23:33:00', '2020-11-05 11:33:57', NULL),
(8, 3, '355D3AD2-108C-4A50-A759-015850D91C32', '2020-11-07', '16:30:00', '23:30:00', '2020-11-06 08:31:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_questions`
--

CREATE TABLE `task_questions` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_questions`
--

INSERT INTO `task_questions` (`id`, `task_id`, `question`, `created_at`, `updated_at`) VALUES
('9E9E0BA5-9334-45B9-8F85-56913C673BB7', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'Question 1?', '2020-11-05 10:20:25', NULL),
('58FC071A-3579-4406-9A05-027A7DF37A58', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'Question 2?', '2020-11-05 10:20:25', NULL),
('020B9F58-707A-4DAD-94E3-7CC2C2181CDA', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'Question 3?', '2020-11-05 10:20:25', NULL),
('404ABE25-CA9F-43ED-AB59-07943517F2BB', '42ACC3E3-2A22-457E-8A22-034780D791DE', 'Question 1?', '2020-11-05 10:30:09', NULL),
('560D7C05-C7CE-4730-ACE0-C0E58EED3ECA', '42ACC3E3-2A22-457E-8A22-034780D791DE', 'Question 2?', '2020-11-05 10:30:09', NULL),
('3AA810FF-07EF-4441-BEA1-96D72FD223C0', 'AB5E0455-6370-45E0-8C19-EF9FFEB7FDBE', 'Do you do full cleaning?', '2020-11-06 09:03:49', NULL),
('B1AA4D02-3644-4A63-8638-867F98C3463E', 'AB5E0455-6370-45E0-8C19-EF9FFEB7FDBE', 'Do you provide equipments?', '2020-11-06 09:03:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_tags`
--

CREATE TABLE `task_tags` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_tags`
--

INSERT INTO `task_tags` (`id`, `task_id`, `tag`, `created_at`, `updated_at`) VALUES
('75DFAB8E-69A4-4A0B-B1DA-151CB079C63B', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'food delivery', '2020-11-05 10:20:25', NULL),
('205D3F21-6187-4255-B15F-DF1860A84821', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'grabfood', '2020-11-05 10:20:25', NULL),
('C510F73D-2415-4EF2-B12A-8BB6CFE7FA5E', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'foodpanda', '2020-11-05 10:20:25', NULL),
('367AF964-1EF5-47E8-A423-F31FD3E3E9F3', '42ACC3E3-2A22-457E-8A22-034780D791DE', 'babysit', '2020-11-05 10:30:09', NULL),
('B0EC88C6-6D0E-46E7-8085-549A57731014', '42ACC3E3-2A22-457E-8A22-034780D791DE', 'kids', '2020-11-05 10:30:09', NULL),
('0564B73A-2D10-4D50-B5FA-E8E0AA6EFB00', 'AB5E0455-6370-45E0-8C19-EF9FFEB7FDBE', 'clean', '2020-11-06 09:03:49', NULL),
('F2B280E6-B9BB-40E4-8E37-B853D5B71947', 'AB5E0455-6370-45E0-8C19-EF9FFEB7FDBE', 'house', '2020-11-06 09:03:49', NULL),
('4A713EFF-5F3E-4DE7-AF07-5A809E3012FB', 'AB5E0455-6370-45E0-8C19-EF9FFEB7FDBE', 'maid', '2020-11-06 09:03:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `temp_token`
--

CREATE TABLE `temp_token` (
  `id` int(11) NOT NULL,
  `token` text,
  `email` text,
  `token_status` int(11) DEFAULT '0' COMMENT '0- Not Used 99 - Deleted',
  `created_by` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `expiry_date` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_token`
--

INSERT INTO `temp_token` (`id`, `token`, `email`, `token_status`, `created_by`, `expiry_date`) VALUES
(1, '9528779a46b6c8f1', 'shah@inspirenow.com.my', 99, '2020-11-05 09:18:21', '2020-11-05 10:18:21'),
(2, '9714e69d5b102ea4', 'syafiq@inspirenow.com.my', 99, '2020-11-05 09:19:00', '2020-11-05 10:19:00'),
(3, '6cad0b19365b237b', 'syafiq.inspirenow@gmail.com', 99, '2020-11-05 12:12:35', '2020-11-05 13:12:35'),
(5, '885172fb0bceadf4', 'kevin@byte2c.com', 99, '2020-11-05 17:05:41', '2020-11-05 18:05:41'),
(6, 'e0f2c52fa4fdc472', 'kevinmvp@gmail.com', 0, '2020-11-06 02:51:30', '2020-11-06 03:51:30');

-- --------------------------------------------------------

--
-- Table structure for table `tracking`
--

CREATE TABLE `tracking` (
  `id` int(11) NOT NULL,
  `checkout_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_admins`
--

CREATE TABLE `user_admins` (
  `admin_id` int(11) NOT NULL,
  `admin_username` text COLLATE utf8_bin NOT NULL,
  `admin_password` text COLLATE utf8_bin NOT NULL,
  `admin_name` text COLLATE utf8_bin NOT NULL,
  `admin_contact` text COLLATE utf8_bin NOT NULL,
  `admin_email` text COLLATE utf8_bin NOT NULL,
  `admin_status` int(1) NOT NULL,
  `admin_role` tinyint(1) NOT NULL,
  `admin_registered` datetime NOT NULL,
  `admin_last_activity` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user_admins`
--

INSERT INTO `user_admins` (`admin_id`, `admin_username`, `admin_password`, `admin_name`, `admin_contact`, `admin_email`, `admin_status`, `admin_role`, `admin_registered`, `admin_last_activity`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Kev', '60123456789', 'kevin@byte2c.com', 1, 1, '2012-10-24 16:00:58', '2018-01-30 16:22:03'),
(2, 'b2cadmin', 'd9f2e096af0c36ce0c509f779220a0b7', 'Byte2c Admin', '0176995737', 'admin@byte2c.com', 1, 1, '2017-10-25 16:14:02', '2017-10-25 16:14:02');

-- --------------------------------------------------------

--
-- Table structure for table `user_rating`
--

CREATE TABLE `user_rating` (
  `id` int(11) NOT NULL,
  `user_id` char(36) CHARACTER SET utf8 NOT NULL,
  `project_id` char(36) CHARACTER SET utf8 NOT NULL,
  `project_type` enum('task','job') CHARACTER SET utf8 NOT NULL DEFAULT 'task',
  `timeliness` decimal(10,1) NOT NULL DEFAULT '0.0',
  `quality` decimal(10,1) NOT NULL DEFAULT '0.0',
  `communication` decimal(10,1) NOT NULL DEFAULT '0.0',
  `responsiveness` decimal(10,1) NOT NULL DEFAULT '0.0',
  `review` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `author_id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT 'This would contain the user_id whom wrote the review',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_task`
--

CREATE TABLE `user_task` (
  `id` int(11) NOT NULL,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `type` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'task',
  `viewed` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_task`
--

INSERT INTO `user_task` (`id`, `user_id`, `task_id`, `type`, `viewed`, `created_at`, `updated_at`) VALUES
(1, '1', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'task', 'Y', '2020-11-05 10:30:21', '2020-11-05 10:31:18'),
(2, '3', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'task', 'Y', '2020-11-05 10:32:35', '2020-11-05 10:32:52'),
(3, '1', '42ACC3E3-2A22-457E-8A22-034780D791DE', 'task', 'Y', '2020-11-05 10:34:26', '2020-11-05 10:34:41'),
(4, '4', '42ACC3E3-2A22-457E-8A22-034780D791DE', 'task', 'Y', '2020-11-05 10:42:58', '2020-11-05 11:14:53'),
(5, '4', 'A0702622-963C-4AFA-873C-00EF58BF7859', 'task', 'Y', '2020-11-05 10:43:11', '2020-11-05 11:12:40'),
(6, '3', '355D3AD2-108C-4A50-A759-015850D91C32', 'task', 'Y', '2020-11-06 08:27:57', '2020-11-06 08:28:10'),
(7, '2', 'AB5E0455-6370-45E0-8C19-EF9FFEB7FDBE', 'task', 'N', '2020-11-06 09:04:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(11) NOT NULL,
  `ip_address` text NOT NULL,
  `visit_count` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`,`question_id`) USING BTREE,
  ADD KEY `FK_question` (`question_id`),
  ADD KEY `FK_user` (`user_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `company_details`
--
ALTER TABLE `company_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employment_details`
--
ALTER TABLE `employment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `FK_task_id` (`task_id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `jobs_user_id_index` (`user_id`) USING BTREE,
  ADD KEY `jobs_category_index` (`category`) USING BTREE,
  ADD KEY `jobs_sub_category_index` (`sub_category`) USING BTREE,
  ADD KEY `jobs_experience_level_index` (`experience_level`) USING BTREE,
  ADD KEY `jobs_country_index` (`country`) USING BTREE,
  ADD KEY `jobs_state_index` (`state`) USING BTREE,
  ADD KEY `jobs_status_index` (`status`) USING BTREE;

--
-- Indexes for table `job_questions`
--
ALTER TABLE `job_questions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `job_questions_id_job_id_unique` (`id`,`job_id`),
  ADD KEY `job_questions_job_id_index` (`job_id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_logs`
--
ALTER TABLE `member_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_requests`
--
ALTER TABLE `payment_requests`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `pinned_messages`
--
ALTER TABLE `pinned_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poster_reviews`
--
ALTER TABLE `poster_reviews`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `preference`
--
ALTER TABLE `preference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promo_code`
--
ALTER TABLE `promo_code`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `search`
--
ALTER TABLE `search`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shopping_cart`
--
ALTER TABLE `shopping_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shopping_checkout`
--
ALTER TABLE `shopping_checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `talent_reviews`
--
ALTER TABLE `talent_reviews`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tasks_user_id_index` (`user_id`) USING BTREE,
  ADD KEY `tasks_category_index` (`category`) USING BTREE,
  ADD KEY `tasks_sub_category_index` (`sub_category`) USING BTREE,
  ADD KEY `tasks_type_index` (`type`) USING BTREE,
  ADD KEY `tasks_status_index` (`status`) USING BTREE;
ALTER TABLE `tasks` ADD FULLTEXT KEY `title_slug` (`title`,`slug`);

--
-- Indexes for table `task_activity`
--
ALTER TABLE `task_activity`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `user_id` (`user_id`) USING BTREE,
  ADD KEY `task_id` (`task_id`) USING BTREE;

--
-- Indexes for table `task_attachments`
--
ALTER TABLE `task_attachments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `task_attachments_task_id_attachment_unique` (`task_id`,`attachment`),
  ADD KEY `task_attachments_task_id_index` (`task_id`),
  ADD KEY `task_attachments_type_index` (`type`);

--
-- Indexes for table `task_planning`
--
ALTER TABLE `task_planning`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `task_id` (`task_id`),
  ADD KEY `date` (`date`);

--
-- Indexes for table `task_questions`
--
ALTER TABLE `task_questions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `task_questions_id_task_id_unique` (`id`,`task_id`),
  ADD KEY `task_questions_task_id_index` (`task_id`);

--
-- Indexes for table `task_tags`
--
ALTER TABLE `task_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `task_tags_task_id_tag_unique` (`task_id`,`tag`),
  ADD KEY `task_tags_task_id_index` (`task_id`),
  ADD KEY `task_tags_tag_index` (`tag`);

--
-- Indexes for table `temp_token`
--
ALTER TABLE `temp_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tracking`
--
ALTER TABLE `tracking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_admins`
--
ALTER TABLE `user_admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `user_rating`
--
ALTER TABLE `user_rating`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `user_task`
--
ALTER TABLE `user_task`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `company_details`
--
ALTER TABLE `company_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `education`
--
ALTER TABLE `education`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `employment_details`
--
ALTER TABLE `employment_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `member_logs`
--
ALTER TABLE `member_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_requests`
--
ALTER TABLE `payment_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pinned_messages`
--
ALTER TABLE `pinned_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `poster_reviews`
--
ALTER TABLE `poster_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `preference`
--
ALTER TABLE `preference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `promo_code`
--
ALTER TABLE `promo_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `search`
--
ALTER TABLE `search`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `shopping_cart`
--
ALTER TABLE `shopping_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shopping_checkout`
--
ALTER TABLE `shopping_checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `talent_reviews`
--
ALTER TABLE `talent_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `task_activity`
--
ALTER TABLE `task_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `task_planning`
--
ALTER TABLE `task_planning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `temp_token`
--
ALTER TABLE `temp_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tracking`
--
ALTER TABLE `tracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_admins`
--
ALTER TABLE `user_admins`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_rating`
--
ALTER TABLE `user_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_task`
--
ALTER TABLE `user_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
