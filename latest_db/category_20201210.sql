-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 10, 2020 at 02:13 PM
-- Server version: 5.7.21
-- PHP Version: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mtp`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) NOT NULL,
  `sortby` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `images`, `title`, `description`, `parent`, `sortby`, `status`) VALUES
(1, '', 'Accounting/Finance', '', 0, 0, 1),
(2, '', 'Audit & Taxation', '', 1, 0, 1),
(3, '', 'General/Cost Accounting', '', 1, 0, 1),
(4, '', 'Corporate Finance/Investment', '', 1, 0, 1),
(5, '', 'Banking/Financial', '', 1, 0, 1),
(6, '', 'Admin/Human Resources', '', 0, 0, 1),
(7, '', 'Clerical/General Admin', '', 6, 0, 1),
(8, '', 'Human Resources', '', 6, 0, 1),
(9, '', 'Secretarial/Executive Personal Assistant', '', 6, 0, 1),
(10, '', 'Top Management', '', 6, 0, 1),
(11, '', 'Sales/Marketing', '', 0, 0, 1),
(12, '', 'Marketing/Business Dev', '', 11, 0, 1),
(13, '', 'Sales - Corporate', '', 11, 0, 1),
(14, '', 'Sales - Eng/Tech/IT', '', 11, 0, 1),
(15, '', 'Sales - Financial Services', '', 11, 0, 1),
(16, '', 'Retail Sales', '', 11, 0, 1),
(17, '', 'Merchandising', '', 11, 0, 1),
(18, '', 'Telesales/Telemarketing', '', 11, 0, 1),
(19, '', 'E-commerce', '', 11, 0, 1),
(20, '', 'Digital Marketing', '', 11, 0, 1),
(21, '', 'Arts/Media/Communications', '', 0, 0, 1),
(22, '', 'Advertising', '', 21, 0, 1),
(23, '', 'Arts/Creative Design', '', 21, 0, 1),
(24, '', 'Entertainment', '', 21, 0, 1),
(25, '', 'Public Relations', '', 21, 0, 1),
(26, '', 'Services', '', 0, 0, 1),
(27, '', 'Personal Care', '', 26, 0, 1),
(28, '', 'Armed Forces', '', 26, 0, 1),
(29, '', 'Social Services', '', 26, 0, 1),
(30, '', 'Customer Service', '', 26, 0, 1),
(31, '', 'Lawyer/Legal Asst', '', 26, 0, 1),
(32, '', 'Logistics/Supply Chain', '', 26, 0, 1),
(33, '', 'Tech & Helpdesk Support', '', 26, 0, 1),
(34, '', 'Hotel/Restaurant', '', 0, 0, 1),
(35, '', 'Food/Beverage/Restaurant', '', 34, 0, 1),
(36, '', 'Hotel/Tourism', '', 34, 0, 1),
(37, '', 'Education/Training', '', 0, 0, 1),
(38, '', 'Education', '', 37, 0, 1),
(39, '', 'Training & Development', '', 37, 0, 1),
(40, '', 'Computer/Information Technology', '', 0, 0, 1),
(41, '', 'IT-Software', '', 40, 0, 1),
(42, '', 'IT-Hardware', '', 40, 0, 1),
(43, '', 'IT-Network/Sys/DB Admin', '', 40, 0, 1),
(44, '', 'Engineering', '', 0, 0, 1),
(45, '', 'Chemical Engineering', '', 44, 0, 1),
(46, '', 'Electronics', '', 44, 0, 1),
(47, '', 'Electrical', '', 44, 0, 1),
(48, '', 'Other Engineering', '', 44, 0, 1),
(49, '', 'Environmental', '', 44, 0, 1),
(50, '', 'Oil/Gas', '', 44, 0, 1),
(51, '', 'Mechanical', '', 44, 0, 1),
(52, '', 'Industrial Engineering', '', 44, 0, 1),
(53, '', 'Manufacturing', '', 0, 0, 1),
(54, '', 'Maintenance', '', 53, 0, 1),
(55, '', 'Purchasing/Material Mgmt', '', 53, 0, 1),
(56, '', 'Manufacturing', '', 53, 0, 1),
(57, '', 'Process Control', '', 53, 0, 1),
(58, '', 'Quality Assurance', '', 53, 0, 1),
(59, '', 'Building/Construction', '', 0, 0, 1),
(60, '', 'Property/Real Estate', '', 59, 0, 1),
(61, '', 'Architect/Interior Design', '', 59, 0, 1),
(62, '', 'Architect/Interior Design', '', 59, 0, 1),
(63, '', 'Civil/Construction', '', 59, 0, 1),
(64, '', 'Quantity Surveying', '', 59, 0, 1),
(65, '', 'Sciences', '', 0, 0, 1),
(66, '', 'Agriculture', '', 65, 0, 1),
(67, '', 'Actuarial/Statistics', '', 65, 0, 1),
(68, '', 'Food Tech/Nutritionist', '', 65, 0, 1),
(69, '', 'Geology/Geophysics', '', 65, 0, 1),
(70, '', 'Aviation', '', 65, 0, 1),
(71, '', 'Biotechnology', '', 65, 0, 1),
(72, '', 'Chemistry', '', 65, 0, 1),
(73, '', 'Science & Technology', '', 65, 0, 1),
(74, '', 'Biomedical', '', 65, 0, 1),
(75, '', 'Healthcare', '', 0, 0, 1),
(76, '', 'Practitioner/Medical Asst', '', 75, 0, 1),
(77, '', 'Pharmacy', '', 75, 0, 1),
(78, '', 'Diagnosis/Others', '', 75, 0, 1),
(79, '', 'Others', '', 0, 0, 1),
(80, '', 'Journalist/Editors', '', 79, 0, 1),
(81, '', 'General Work', '', 79, 0, 1),
(82, '', 'Publishing', '', 79, 0, 1),
(83, '', 'Others', '', 79, 0, 1);
COMMIT;
