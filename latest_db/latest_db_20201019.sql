-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 19, 2020 at 07:50 PM
-- Server version: 5.7.32
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maketime_staging`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `question_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `answer` mediumtext COLLATE utf8_unicode_ci,
  `type` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'task',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`task_id`, `question_id`, `user_id`, `answer`, `type`, `created_at`, `updated_at`, `id`) VALUES
('C74C4FE0-71C5-4530-8ED4-9CEBCBEAF1E9', '77534D0E-5088-4EB7-A2CE-BEE6081B3EA6', 2, 'first', 'task', '2020-10-18 05:42:14', NULL, 1),
('3954FC5A-2101-41DF-AD2B-8AEC647B3948', '879C4521-3222-4CD3-BEB0-08C316093EC5', 4, '1', 'task', '2020-10-19 10:19:47', NULL, 2),
('3954FC5A-2101-41DF-AD2B-8AEC647B3948', 'C262AB44-F906-4D91-B35E-B9201D5DEF5A', 4, '2', 'task', '2020-10-19 10:19:47', NULL, 3),
('3954FC5A-2101-41DF-AD2B-8AEC647B3948', 'E8FC94BC-1FE9-4729-A6E6-BEF7979807FB', 4, '3', 'task', '2020-10-19 10:19:47', NULL, 4),
('3954FC5A-2101-41DF-AD2B-8AEC647B3948', 'FC7D6BCD-316F-44CE-AC9A-2D5DE02E1529', 4, '4', 'task', '2020-10-19 10:19:47', NULL, 5),
('3954FC5A-2101-41DF-AD2B-8AEC647B3948', 'AFFDD9FA-800C-424A-B048-E0A4AAE25611', 4, '5', 'task', '2020-10-19 10:19:47', NULL, 6),
('5DB33E41-106A-45D6-B334-B22FDEAC2E79', '0D1EB337-3692-4034-955E-5359089D019F', 4, '1', 'task', '2020-10-19 10:22:52', NULL, 7),
('5DB33E41-106A-45D6-B334-B22FDEAC2E79', '291DC3D3-BC78-4635-A474-A8CC9EC7909D', 4, '2', 'task', '2020-10-19 10:22:52', NULL, 8),
('5DB33E41-106A-45D6-B334-B22FDEAC2E79', '0EB1A90F-27B7-48D7-8887-685FB59ED8C6', 4, '3', 'task', '2020-10-19 10:22:52', NULL, 9),
('5DB33E41-106A-45D6-B334-B22FDEAC2E79', '0BA77089-04A7-4E84-B1B9-7F113A48AB95', 4, '4', 'task', '2020-10-19 10:22:52', NULL, 10),
('5DB33E41-106A-45D6-B334-B22FDEAC2E79', 'A544C4AC-08F7-42A2-9491-A9AC678D3BEC', 4, '5', 'task', '2020-10-19 10:22:52', NULL, 11),
('0AE1A0D5-ECD7-4223-A0CB-3CF1B7C9A7EF', '488161CA-A3B1-4A83-A427-252E1D0B94F5', 4, '1', 'task', '2020-10-19 10:50:32', NULL, 12),
('0AE1A0D5-ECD7-4223-A0CB-3CF1B7C9A7EF', '488161CA-A3B1-4A83-A427-252E1D0B94F5', 2, 'a', 'task', '2020-10-19 10:50:54', NULL, 13);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `images` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) NOT NULL,
  `sortby` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `images`, `title`, `description`, `parent`, `sortby`, `status`) VALUES
(1, '', 'Chores', '', 0, 0, 0),
(2, '', 'Food & Beverage', '', 0, 0, 0),
(3, '', 'Kids', '', 0, 0, 0),
(4, '', 'Entertainment', '', 0, 0, 0),
(5, '', 'Technology', '', 0, 0, 0),
(11, '', 'Vacuumming', '', 1, 0, 1),
(12, '', 'Washing Dishes', '', 1, 0, 1),
(13, '', 'Feeding Pets', '', 1, 0, 1),
(14, '', 'Laundry', '', 1, 0, 1),
(15, '', 'Beverage Server', '', 2, 0, 1),
(16, '', 'Chef', '', 2, 0, 1),
(17, '', 'Cleaner', '', 2, 0, 1),
(18, '', 'Food Delivery', '', 2, 0, 1),
(19, '', 'Waiter', '', 2, 0, 1),
(20, '', 'Arts & Craft', '', 3, 0, 1),
(21, '', 'Babysit', '', 3, 0, 1),
(22, '', 'Learning & Education', '', 3, 0, 1),
(23, '', 'Nature Activities', '', 3, 0, 1),
(24, '', 'Play Kids Games', '', 3, 0, 1),
(25, '', 'Actor', '', 4, 0, 1),
(26, '', 'Emcee', '', 4, 0, 1),
(27, '', 'Musician', '', 4, 0, 1),
(28, '', 'Performer', '', 4, 0, 1),
(29, '', 'Voice Acting', '', 4, 0, 1),
(30, '', 'Database Administrator', '', 5, 0, 1),
(31, '', 'Electrician', '', 5, 0, 1),
(32, '', 'Hardware Engineer', '', 5, 0, 1),
(33, '', 'Softrware Engineer', '', 5, 0, 1),
(34, '', 'Technician', '', 5, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(11) NOT NULL,
  `sender_id` text,
  `receiver_id` text,
  `contents` text,
  `created_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `viewed` int(11) NOT NULL,
  `viewed_dated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `sender_id`, `receiver_id`, `contents`, `created_by`, `viewed`, `viewed_dated`) VALUES
(1, '2', '1', 'Test, msg sent via db', '2020-10-18 05:44:31', 0, '0000-00-00 00:00:00'),
(2, '1', '2', 'test success', '2020-10-18 14:15:51', 1, '0000-00-00 00:00:00'),
(3, '2', '1', 'okay thanks', '2020-10-19 05:13:39', 0, '0000-00-00 00:00:00'),
(4, '3', '4', 'Hello.', '2020-10-19 10:23:53', 0, '0000-00-00 00:00:00'),
(5, '4', '3', 'Hai', '2020-10-19 10:24:24', 0, '0000-00-00 00:00:00'),
(6, '3', '4', 'What\'s up?', '2020-10-19 10:24:34', 0, '0000-00-00 00:00:00'),
(7, '3', '2', 'Hllpo', '2020-10-19 10:54:44', 0, '0000-00-00 00:00:00'),
(8, '2', '3', 'assign pls', '2020-10-19 11:02:36', 0, '0000-00-00 00:00:00'),
(9, '2', '3', 'test', '2020-10-19 11:25:04', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `post_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'If it''s a reply of another comment',
  `comment` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `parent_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, '4', '3954FC5A-2101-41DF-AD2B-8AEC647B3948', 0, 'Hello', '2020-10-19 10:18:27', NULL),
(2, '3', '3954FC5A-2101-41DF-AD2B-8AEC647B3948', 1, 'Hi', '2020-10-19 10:19:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_details`
--

CREATE TABLE `company_details` (
  `id` int(11) NOT NULL,
  `name` text,
  `reg_num` text,
  `address` text,
  `postal_code` text,
  `state` text,
  `country` text,
  `email` text,
  `contact_number` text,
  `industry` text,
  `member_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `type` text,
  `doc_location` text,
  `member_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE `education` (
  `id` int(11) NOT NULL,
  `highest_edu_level` text,
  `grad_year` text,
  `edu_institution` text,
  `field` text,
  `major` text,
  `grade` text,
  `member_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`id`, `highest_edu_level`, `grad_year`, `edu_institution`, `field`, `major`, `grade`, `member_id`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(2, NULL, NULL, NULL, NULL, NULL, NULL, 3),
(3, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(4, NULL, NULL, NULL, NULL, NULL, NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `employment_details`
--

CREATE TABLE `employment_details` (
  `id` int(11) NOT NULL,
  `company_name` text,
  `position` text,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `department` text,
  `job_desc` text,
  `responsibilities` text,
  `remuneration` text,
  `location` text,
  `industry` text,
  `member_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employment_details`
--

INSERT INTO `employment_details` (`id`, `company_name`, `position`, `date_start`, `date_end`, `department`, `job_desc`, `responsibilities`, `remuneration`, `location`, `industry`, `member_id`) VALUES
(1, NULL, NULL, '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, 2),
(2, NULL, NULL, '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, 3),
(3, NULL, NULL, '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, 4),
(4, NULL, NULL, '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
  `id` int(11) NOT NULL,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `type` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'task',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `number` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `sub_category` int(11) NOT NULL,
  `title` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `requirements` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `experience_level` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `location` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `salary_range_min` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salary_range_max` decimal(10,2) NOT NULL DEFAULT '0.00',
  `benefits` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `status` enum('draft','published','completed','cancelled') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `assigned` timestamp NULL DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job_questions`
--

CREATE TABLE `job_questions` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `job_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `fb_uid` text,
  `google_id` text NOT NULL,
  `fb_email` text,
  `email` text,
  `password` text,
  `photo` text,
  `salutation` text,
  `firstname` text,
  `lastname` text,
  `contact` text,
  `address1` text,
  `address2` text,
  `zip` text,
  `city` text,
  `country` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `nric` text,
  `gender` text,
  `dob` text,
  `nationality` text,
  `contact_number_prefix` text,
  `contact_number` text,
  `mobile_number_prefix` text,
  `mobile_number` text,
  `profile_pic` text,
  `insta_id` text,
  `other_social_media` text,
  `emergency_contact_name` text,
  `emergency_contact_number_prefix` text,
  `emergency_contact_number` text,
  `emergency_contact_relation` text,
  `about` text,
  `skills` text NOT NULL,
  `language` text,
  `dashboard_widget` varchar(128) DEFAULT '2,4,5,6',
  `completion` tinyint(4) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT '0' COMMENT '0- Active 99- Suspended',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '0- New Member 1- Existing Member '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `fb_uid`, `google_id`, `fb_email`, `email`, `password`, `photo`, `salutation`, `firstname`, `lastname`, `contact`, `address1`, `address2`, `zip`, `city`, `country`, `state`, `nric`, `gender`, `dob`, `nationality`, `contact_number_prefix`, `contact_number`, `mobile_number_prefix`, `mobile_number`, `profile_pic`, `insta_id`, `other_social_media`, `emergency_contact_name`, `emergency_contact_number_prefix`, `emergency_contact_number`, `emergency_contact_relation`, `about`, `skills`, `language`, `dashboard_widget`, `completion`, `date_created`, `status`, `type`) VALUES
(1, '10158710870049321', '103749524560127799092', NULL, 'skaterzee@gmail.com', NULL, 'files/1/profile/fbProfilePhoto.jpg', NULL, 'Farid', 'Edil', NULL, NULL, NULL, NULL, '76418', 132, 1944, '880831888888', 'male', '1988-08-31', 'malaysian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Programming,Gaming', 'English', '2,4,5,6', 100, '2020-10-17 12:25:28', 1, 0),
(2, NULL, '', NULL, 'kevinmvp@gmail.com', '$2a$07$np65plSrOACM8PPpL57zcO2paWR21l.rTwNxLAFbvI7AYTF9OGAQ.', 'assets/img/default-avatar.png', NULL, 'Kevin', 'Chew', NULL, NULL, NULL, NULL, '76411', 132, 1944, '840630145493', 'male', '1984-06-30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Project Management,Office', 'english,malay', '2,4,5,6', 100, '2020-10-18 13:40:02', 0, 0),
(3, NULL, '', NULL, 'syafiq@inspirenow.com.my', '$2a$07$np65plSrOACM8PPpL57zcOmDOOmbjunuWgz.Ot5UZeARX4tubWoqG', 'assets/img/default-avatar.png', NULL, 'Syafiq', 'Syazre', NULL, NULL, NULL, NULL, '76491', 132, 1950, '930714016671', NULL, '1993-07-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Art & Design,Cooking Skills,Drawing Skills,Illustrator Skills', 'English,Malay', '2,4,5,6', 100, '2020-10-19 17:28:44', 0, 0),
(4, NULL, '', NULL, 'shah@inspirenow.com.my', '$2a$07$np65plSrOACM8PPpL57zcOmDOOmbjunuWgz.Ot5UZeARX4tubWoqG', 'assets/img/default-avatar.png', NULL, 'Shah', 'Ramly', NULL, NULL, NULL, NULL, '76478', 132, 1938, '830629075433', NULL, '1983-06-29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Art & Design,Education', 'english', '2,4,5,6', 100, '2020-10-19 17:28:48', 0, 0),
(5, NULL, '', NULL, 'kevinchewmvp@hotmail.com', NULL, 'assets/img/default-avatar.png', NULL, 'kevin', 'Chew', NULL, NULL, NULL, NULL, '76411', 132, 1944, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2,4,5,6', 0, '2020-10-19 19:37:29', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `member_docs`
--

CREATE TABLE `member_docs` (
  `member_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `src` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `member_logs`
--

CREATE TABLE `member_logs` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `ip_address` text NOT NULL,
  `user_agent` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_logs`
--

INSERT INTO `member_logs` (`id`, `member_id`, `date`, `ip_address`, `user_agent`) VALUES
(1, 0, '2020-10-17 12:25:54', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(2, 0, '2020-10-17 12:25:59', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(3, 0, '2020-10-17 12:26:40', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(4, 0, '2020-10-17 12:27:29', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(5, 0, '2020-10-17 12:35:26', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(6, 1, '2020-10-18 12:08:54', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(7, 0, '2020-10-18 12:08:54', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(8, 0, '2020-10-18 12:10:50', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(9, 0, '2020-10-18 12:13:01', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(10, 0, '2020-10-18 12:13:16', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(11, 0, '2020-10-18 13:40:36', '14.192.209.221', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36 Edg/86.0.622.43'),
(12, 0, '2020-10-18 13:41:17', '14.192.209.221', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36 Edg/86.0.622.43'),
(13, 0, '2020-10-18 13:42:02', '14.192.209.221', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36 Edg/86.0.622.43'),
(14, 1, '2020-10-18 13:45:27', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(15, 0, '2020-10-18 13:45:27', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(16, 1, '2020-10-18 17:55:49', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(17, 0, '2020-10-18 17:55:49', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(18, 1, '2020-10-18 22:14:19', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(19, 0, '2020-10-18 22:14:19', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(20, 1, '2020-10-19 12:44:35', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(21, 0, '2020-10-19 12:44:35', '210.186.18.100', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(22, 0, '2020-10-19 13:13:25', '14.192.209.221', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36 Edg/86.0.622.43'),
(23, 0, '2020-10-19 17:33:16', '210.187.182.25', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(24, 0, '2020-10-19 17:33:18', '175.138.203.225', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(25, 0, '2020-10-19 17:34:12', '210.187.182.25', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(26, 0, '2020-10-19 17:34:13', '175.138.203.225', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(27, 0, '2020-10-19 17:34:35', '210.187.182.25', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(28, 0, '2020-10-19 17:34:39', '175.138.203.225', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(29, 0, '2020-10-19 17:34:55', '210.187.182.25', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(30, 0, '2020-10-19 17:35:35', '175.138.203.225', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(31, 0, '2020-10-19 17:43:13', '175.138.203.225', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(32, 0, '2020-10-19 17:47:36', '175.138.203.225', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(33, 0, '2020-10-19 17:49:09', '175.138.203.225', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(34, 0, '2020-10-19 17:49:25', '175.138.203.225', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(35, 0, '2020-10-19 17:52:47', '175.138.203.225', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(36, 0, '2020-10-19 17:56:00', '14.192.209.221', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36 Edg/86.0.622.43'),
(37, 0, '2020-10-19 18:17:13', '175.138.203.225', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(38, 0, '2020-10-19 18:17:29', '210.187.182.25', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(39, 0, '2020-10-19 18:18:14', '210.187.182.25', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(40, 0, '2020-10-19 18:21:28', '14.192.209.221', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36 Edg/86.0.622.43'),
(41, 0, '2020-10-19 18:30:09', '175.138.203.225', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(42, 0, '2020-10-19 18:31:10', '175.138.203.225', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(43, 0, '2020-10-19 18:31:30', '175.138.203.225', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(44, 0, '2020-10-19 18:45:13', '210.187.182.25', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(45, 0, '2020-10-19 18:50:25', '210.187.182.25', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'),
(46, 0, '2020-10-19 19:24:13', '14.192.209.221', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36 Edg/86.0.622.43'),
(47, 0, '2020-10-19 19:24:36', '14.192.209.221', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36 Edg/86.0.622.43'),
(48, 0, '2020-10-19 19:35:57', '14.192.209.221', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36 Edg/86.0.622.43'),
(49, 0, '2020-10-19 19:36:51', '14.192.209.221', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36 Edg/86.0.622.43'),
(50, 0, '2020-10-19 19:38:52', '14.192.209.221', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36 Edg/86.0.622.43');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `type`, `title`, `content`) VALUES
(2, 'contact-us', 'Contact Us', '<table border=\"0\" cellpadding=\"10\" cellspacing=\"10\" style=\"width:800px;\">\r\n  <tbody>\r\n   <tr>\r\n      <td><iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3983.6670717145776!2d101.5552182!3d3.1819878999999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc4f39c85a6d47%3A0xbd09b23cae539c66!2sNicChris!5e0!3m2!1sen!2smy!4v1581997939084!5m2!1sen!2smy\" width=\"600\" height=\"545\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe></td>\r\n   </tr>\r\n </tbody>\r\n</table>'),
(3, 'careers', 'Careers', ''),
(4, 'faq', 'Faq', '<ol>\n <li><strong>How much for delivery?</strong></li>\n</ol>\n\n<p style=\"margin-left: 40px;\">As we carry all our products in stock, we are able to deliver them fast, within 14 working days of your order. 30 to&nbsp;45 working days for pre-order items or customade items.&nbsp;We deliver to any place in Klang Valley at a minimal cost from RM250. For delivery outside Klang Valley, we will quote delivery charge based on location and ordered items. Our delivery team will assemble your purchases and place them at your preferred spot.</p>\n\n<ol>\n <li value=\"2\"><strong>Do you provide disposal services?</strong></li>\n</ol>\n\n<p style=\"margin-left: 40px;\">Should you require help in disposing your existing furniture before replacing it with your new furniture from NicChris, simply inform us of your request prior to delivery. We will make the necessary arrangements for you with a service charge.</p>\n\n<ol>\n  <li value=\"3\"><strong>What happens if the delivered furniture is damaged?</strong></li>\n</ol>\n\n<p style=\"margin-left: 40px;\">From the time of receiving your order to notify us in the unlikely event that your item arrives damaged or faulty. Please send an email to hello@nicchris.com&nbsp;or call our customer service team. You may be required to provide photos of the damage or fault as assessment by our quality control team.<br />\n<br />\nFaulty claims or refund claims&nbsp;within the same day of&nbsp;delivery will be treated as warranty claim. Once assessed and approved by our specialists, you will be contacted to process a credit note or organise a replacement for you. Do not return the items before speaking to our customer service team. During such unlikely event, the customer will bear the delivery costs incurred in the claim process.</p>\n\n<ol>\n  <li value=\"4\"><strong>What happens if I changed my mind?</strong></li>\n</ol>\n\n<p style=\"margin-left: 40px;\">For any change of minds,<span style=\"color: rgb(51, 51, 51); font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;; font-size: 13px; background-color: rgb(255, 255, 255);\">&nbsp;there will be no refund or exchange.</span></p>\n\n<ol>\n  <li value=\"5\"><strong>Payment method</strong></li>\n</ol>\n\n<p style=\"margin-left: 40px;\">We accept the following methods of payment: Credit Card (Visa, MasterCard) via Paypal, direct deposit into our bank account. Currently, we do not accept oversea order. Once confirm, there will be no refund for cancellation.</p>\n\n<p style=\"margin-left: 40px;\">Please use your Order ID number as the payment reference&nbsp;and email the receipt of payment to&nbsp;<strong>hello@nicchris.com</strong>.</p>\n\n<p style=\"margin-left: 40px;\">Your order won\'t be shipped until the funds have cleared in our account.</p>\n\n<ol>\n  <li value=\"6\"><strong>Warranty</strong></li>\n</ol>\n\n<p style=\"margin-left: 40px;\">The Warranty period commences from the date of your goods received&nbsp;from NicChris. The period of the Warranty will be:</p>\n\n<p style=\"margin-left: 0.5in;\">- 1&nbsp;year&nbsp;for upholstery frames (upholstery is sofas, ottomans, daybeds, chaises and armchairs) and office chair mechanism&nbsp;</p>\n\n<p style=\"margin-left: 0.5in;\">- 1 year for furniture which NicChris identifies in writing as attaching this Warranty; and</p>\n\n<p style=\"margin-left: 40px;\">Exclusions: This Warranty will not apply if;</p>\n\n<p style=\"margin-left: 0.5in;\">- Repairs to a product are made or attempted by a service provider other than NicChris or authorised specialist.</p>\n\n<p style=\"margin-left: 0.5in;\">- The product is subject to natural wear and tear.</p>\n\n<p style=\"margin-left: 0.5in;\">- The consumer used the product is an abnormal manner for example if the product is abused, misused, dropped, crushed, impacted with any hard surface, exposed to extreme heat (including fire) or cold, not maintained properly or used after partial failure and used in Public and higher traffic areas with unrestrained behavior of patrons.</p>\n\n<p style=\"margin-left: 0.5in;\">- The product has been modified, incorrectly adjusted or operated, subjected to incorrect electrical supply or inconsistent electrical supply or used with inappropriate accessories.</p>\n\n<p style=\"margin-left: 0.5in;\">- The product is tampered with in any way.</p>\n\n<ol>\n  <li value=\"7\"><strong>Is there any hidden charges?</strong></li>\n</ol>\n\n<p style=\"margin-left: 40px;\">We take great pride in the quality and craftsmanship of our products. We offer competitive and transparent prices for our quality products. All our prices stated on our website are net. There are no hidden charges such as delivery, installation, GST, etc.</p>\n'),
(5, 'redeem-a-gift-card', 'Redeem A Gift Card', ''),
(7, 'store-pick-up', 'Store Pick-Up', ''),
(8, 'shipping-info', 'Shipping Info', '<p><strong>Delivery Lead Times</strong></p>\n\n<p>To meet your delivery requirements in the best way possible, we use a variety of professional carriers who deliver anywhere within Malaysia. Your location and order size will impact on the speediness we can get your order to you. Provided all items are in stock, delivery lead times are as follows:</p>\n\n<p>Kuala Lumpur / Penang / Johor Bahru within 5 working days.</p>\n\n<p>Other areas in Peninsular Malaysia between 10 and 15 working days.</p>\n\n<p>Remote locations in Peninsular Malaysia between 10 and 15 working days.</p>\n\n<p>For international orders outside of Malaysia, please check with customer service team before ordering.</p>\n\n<p>For urgent delivery enquiries or special requirements, please contact our customer service team on 03- 777 31 708.</p>\n\n<p><br />\n<strong>Tracking Your Order</strong></p>\n\n<p>Our policy is to keep you informed. When your order is despatched, we will contact you by email and/or SMS with the relevant tracking information so you can keep a close eye on your delivery. For larger orders consisting of bulky or fragile items, you will also receive a call from one of our professional furniture movers to confirm a suitable time for delivery.&nbsp;</p>\n\n<p><br />\n<strong>Delivery Policy</strong></p>\n\n<p>Deliveries for non-bulky and non-fragile goods are to your front door only. For bulky or fragile goods we believe your purchase deserves a special degree of handling and care. To ensure this we use professional furniture movers who call to confirm a time for delivery and take extra care throughout the entire process. Your item/s will be hand unloaded and carefully placed in your home or office by two professionals. This service does not include unpacking or rubbish removal.</p>\n\n<p>We do offer a premium assembly service for metro areas. A NicChris representative will go to your home or office, unpack and assemble your goods, and remove any rubbish. This cost will depends on your location and will be done the day after delivery.</p>\n\n<p><br />\n<strong>Picking up Your Order</strong></p>\n\n<p>We are still in the midst of setting up a location for you to pick up the order.</p>\n\n<p>&nbsp;</p>\n\n<p><br />\n<strong>Delivery Charges</strong></p>\n\n<p>On the item listing page, under the Buy Now button there is provision to enter your postcode and quantity to get an estimate of shipping costs to your area. If our shipping calculator is unable to work out your shipping, please call our customer service team on 03- 777 31 708 to obtain a price for delivery.</p>\n\n<p>All deliveries have freight charges calculated depending on the size of the order (some orders must be delivered by a 2 or 3 man crew for safety reasons and to ensure you receive the best service possible) and the delivery address. We pay a premium to our professional furniture mover to ensure the best service possible.</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Signature on Delivery</strong></p>\n\n<p>All shipments contain valuable goods. Because of this, our carriers are contracted to produce a Proof of Delivery.</p>\n\n<p>If you are comfortable with having your order left without the carrier obtaining a signature, please indicate this in the special instructions box when placing your order online.</p>\n\n<p>We are unable to accept any responsibility for your order once delivered if you select this option.</p>\n\n<p><br />\n<strong>If You Will not be Home to Accept Delivery</strong></p>\n\n<p>If you are comfortable with having your order left without the carrier obtaining a signature, please indicate this in the special instructions box when placing your order online. Should you choose this option, please note that we take no responsibility for orders left unattended. There will be additional charges by the delivery company if you are not able to receive the goods on the date you stated.</p>\n\n<p><br />\n<strong>Assembly</strong></p>\n\n<p>Unless otherwise stated, NicChris furniture does not come pre-assembled. All our products include simple step by step instructions on how to assemble. Please call our customer service line on 03- 777 31 708&nbsp;if you require information regarding specific item assembly.</p>\n\n<p>We do offer a premium assembly service for metro areas. A NicChris representative will go to your home or office, unpack and assemble your goods, and remove any rubbish. The cost will depends on your location and the items you bought and will be done the day after delivery.</p>\n\n<p><br />\n<strong>Payment Method</strong></p>\n\n<p>We accept the following methods of payment: Credit Card (Visa, MasterCard, Paypal), direct deposit into our bank account, money order, personal or business cheque. All cheque payments require 5 days to clear prior to shipment. All payments are in MYR.</p>\n\n<p><strong>Insurance</strong></p>\n\n<p>All shipping costs exclude insurance. If you need an insurance for your items, please send an enquiry to hello@nicchris.com for more info.&nbsp;&nbsp;</p>\n\n<p><br />\n<strong>Security Policy</strong></p>\n\n<p>When purchasing from NicChris your financial details are passed through Paypal secure server using the latest 128 bit SSL (secure sockets layer) encryption technology. 128 bit SSL encryption is approximated to take at least one trillion years to break and is the industry standard. If you have any questions regarding our security policy please contact our customer support centre at hello@nicchris.com.</p>\n\n<p><br />\n<strong>Pricing and Product Availability</strong></p>\n\n<p>We exercise great caution in trying to avoid errors in pricing and product information. If such mistakes occur, we reserve the right to correct them. We apologise in advance for any inconvenience this may cause.</p>\n\n<p>NicChris reserve the right to change the units available for the Daily Deal promotion at any time throughout the duration of the promotion.</p>\n'),
(9, 'returns', 'Returns', ''),
(10, 'commercial-enquiries', 'Commercial Enquiries', ''),
(11, 'marketing-and-media-enquiries', 'Marketing & Media Enquiries', ''),
(12, 'about-us', 'About Us', '<p><strong>NicChris,</strong> A story of craftsmanship</p>\n\n<p>The key of NicChris identity lies in the ability of each craftsman to express the form and comfort; marrying tradition and modern technology through each intelligent but yet emotionally sensitive hands. Added to this craftsmanship is a careful processed of material selection to increase the strength while enhancing the comfortability.</p>\n\n<p>NicChris showcases variation and opulence of collections for the comfort of homes, the demand of hospitality and the formality of commercial need.</p>\n\n<p>The story of NicChris does not stop at each finished products but it is the beginning for many local designers who hand-in-hand started each product. NicChris supports the excellence of local designers and craftsman.&nbsp;</p>\n'),
(13, 'policies', 'Policies', '<p><span style=\"font-size:18px;\"><strong>Privacy Policy</strong></span></p>\n\n<p>At NicChris we understand that you value your privacy and wish to have your personal information kept secure. Our Privacy Policy describes generally how we manage your personal information and safeguard your privacy. If you would like more information, please don\'t hesitate to contact us.</p>\n\n<p><br />\n<strong>Collecting Personal Information About You</strong><br />\nAt NicChris we only collect personal information that is necessary for us to conduct our business as an online provider of goods.</p>\n\n<p>The personal information we collect will include information you give us when you place an order for the purchase of a good we supply. We may also collect personal information about individuals who are not customers of our business, but whose personal information is given to us by those individuals or other people in the course of a transaction. This personal information will include your name, address and contact details, and may include other personal information about individuals we collect in the course of a transaction.</p>\n\n<p>We may also collect some information from you when you use our website www.nicchris.com. Your use of the information and services available through our website will determine the type of information that we collect about you.</p>\n\n<p>The only personal information that we collect about you when you use our website is what you tell us about yourself, for example, when you complete an online form when placing an order, or information you provide to us when you send us an email. Please note, we will record your email address if you send us an email.</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Using and Disclosing Your Personal Information</strong></p>\n\n<p>We respect your privacy. Any personal information that we collect about you will be used and disclosed by us so that we can provide you with the services that you have requested, or otherwise to enable us to carry out our business as suppliers of goods.</p>\n\n<p>Please be assured we will not disclose information about you unless the disclosure is required or authorised by law, you have consented to our disclosing the information about you, or for another purpose (related to the primary purpose of collecting that information) that you would reasonably expect.</p>\n\n<p>We may also use your personal information to provide you with information about other services offered by us. However, the only information that you must provide is your name and payment details (where applicable). If you would prefer not to receive promotional or other material from us, please let us know and we will respect your request.</p>\n\n<p>We do not engage in unsolicited telephone. In particular, if you are contacted by telephone by an individual or organisation claiming to represent NicChris - do not purchase from the organisation as we have a strict policy against telephone marketing.</p>\n\n<p><br />\n<strong>Access to Your Personal Information</strong></p>\n\n<p>In most cases you may have access to personal information that we hold about you. We will handle requests for access to your personal information in accordance with the law.</p>\n\n<p>We encourage all requests for access to your personal information to be directed to the Privacy Officer by emailing us or by writing to us at our email address (hello@nicchris.com). We will deal with all requests for access to personal information as quickly as possible. Requests for a large amount of information, or information which is not currently in use, may require further time before a response can be given. We may charge you a fee for access if a cost is incurred by us in order to retrieve your information, but in no case will we charge you a fee for your application for access.</p>\n\n<p>In some cases, we may refuse to give you access to personal information we hold about you. This includes, but is not limited to, circumstances where giving you access would: be unlawful (e.g., where a record which contains personal information about you is subject to a claim for legal professional privilege by one of our clients); have an unreasonable impact on other people\'s privacy; prejudice an investigation of unlawful activity.</p>\n\n<p>We will also refuse access where the personal information relates to existing or anticipated legal proceedings, and the information would not be accessible by the process of discovery in those proceedings. If we refuse to give you access we will provide you with reasons for our refusal.</p>\n\n<p><br />\n<strong>Correcting Your Personal Information</strong><br />\nIf you request us to do so we will amend any personal information about you held by us which is inaccurate, incomplete or out of date. If we disagree with your view about the accuracy, completeness or currency of a record of your personal information held by us, and you ask us to associate with that record a statement that you have a contrary view, we will take reasonable steps to do so.</p>\n\n<p><br />\n<strong>Securing and Storing Your Personal Information</strong><br />\nWe are committed to maintaining the confidentiality of the information that you provide us and we will take all reasonable precautions to protect your personal information from unauthorised use or alteration.</p>\n\n<p>In our business, personal information may be stored both electronically (on our computer system) and in hard-copy form. Firewalls, anti-virus software and email filters, as well as passwords, protect all our electronic information. Likewise, we take all necessary measures to ensure the security of hard-copy information.</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Disposal of Personal Information</strong></p>\n\n<p>Once your transaction is finalised, both the electronic and hard-copy information is securely archived. After a period of 7 years both the electronic and hard-copy information is destroyed.</p>\n\n<p><br />\nFor more information or to complain about a breach of your privacy<br />\nIf you would like more information about the way we manage personal information which we hold about you, or are concerned that we may have breached your privacy, please contact us by email:</p>\n\n<p><br />\nEmail us at hello@nicchris.com .&nbsp;</p>\n\n<p>From time to time it may be necessary for us to revise our privacy policy. Any changes will be in accordance with the Malaysia law. We may notify you about changes to this privacy policy by posting an updated version on our website www.nicchris.com.</p>\n'),
(14, 'locations', 'Locations', '<table border=\"0\" cellpadding=\"10\" cellspacing=\"10\" style=\"width:800px;\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <p><strong>â€‹</strong><strong>AL 121 Building D, D1 &amp; D2<br />\r\n            Batu 13, Jalan Subang<br />\r\n            47000 Sungai Buloh<br />\r\n            Selangor Darul Ehsan, Malaysia</strong></p>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td><i class=\"fa fa-phone\"></i>  012-3965608 / 012-8138488</td>\r\n        </tr>\r\n        <tr>\r\n            <td><iframe allowfullscreen=\"\" frameborder=\"0\" height=\"450\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1686.5539028835628!2d101.57587968848602!3d3.1952788193897103!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc4586cf81cadf%3A0x942d4b2cdaec3e72!2sMonetous+Fine+Furniture!5e0!3m2!1sen!2smy!4v1508920112067\" style=\"border:0\" width=\"600\"></iframe>â€‹â€‹</td>\r\n        </tr>\r\n    </tbody>\r\n</table>');

-- --------------------------------------------------------

--
-- Table structure for table `preference`
--

CREATE TABLE `preference` (
  `id` int(11) NOT NULL,
  `rm_work_location` text COMMENT 'rm- Remote Work',
  `rm_country` text,
  `rm_state` text,
  `p_work_location` text COMMENT 'p - physical ',
  `p_country` text,
  `p_state` text,
  `work_day` text,
  `work_day_full` int(11) NOT NULL,
  `work_day_after` int(11) NOT NULL,
  `work_day_am` int(11) NOT NULL,
  `work_day_pm` int(11) NOT NULL,
  `working_hours` text,
  `work_load_hours` int(11) NOT NULL,
  `expected_earning` text,
  `bank` varchar(64) NOT NULL,
  `acc_name` varchar(128) NOT NULL,
  `acc_num` varchar(32) NOT NULL,
  `member_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `preference`
--

INSERT INTO `preference` (`id`, `rm_work_location`, `rm_country`, `rm_state`, `p_work_location`, `p_country`, `p_state`, `work_day`, `work_day_full`, `work_day_after`, `work_day_am`, `work_day_pm`, `working_hours`, `work_load_hours`, `expected_earning`, `bank`, `acc_name`, `acc_num`, `member_id`) VALUES
(1, 'anywhere', '132 ', '1935', 'near', '132', '1944', 'allWeek', 0, 0, 0, 0, '3', 5, '2000', 'Public Bank', 'Farid Edil', '123456789', 1),
(2, 'near', '132 ', 'Selangor', 'near', '132', '1944', 'allWeek', 0, 0, 0, 0, '2', 3, '500', 'Public Bank', 'Kevin', '12391029312', 2),
(3, 'anywhere', '132 ', '1950', 'anywhere', '132', '1949', 'allWeek', 0, 0, 0, 0, '6', 40, '2000', 'Maybank2u', 'Syafiq Syazre', '99909090909090', 3),
(4, 'near', '132 ', '1939', 'near', '132', '1939', 'allWeek', 0, 0, 0, 0, '3', 40, '300', 'CIMB Clicks', 'Shah', '7564773727', 4),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, 0, NULL, '', '', '', 5);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `code` text NOT NULL,
  `images` text NOT NULL,
  `title` text NOT NULL,
  `model` text NOT NULL,
  `description` text NOT NULL,
  `description2` text NOT NULL,
  `size` text NOT NULL,
  `material` text NOT NULL,
  `color` text NOT NULL,
  `fabric` text NOT NULL,
  `category` int(11) NOT NULL,
  `categories` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `price_original` decimal(10,2) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `prices` text NOT NULL,
  `options` text NOT NULL,
  `colors` text NOT NULL,
  `is_new` int(11) NOT NULL,
  `sale_price` decimal(10,2) NOT NULL,
  `product_suggestion` text NOT NULL,
  `date_added` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promo_code`
--

CREATE TABLE `promo_code` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `category_id` text NOT NULL,
  `product_id` text NOT NULL,
  `type` text NOT NULL,
  `value` int(11) NOT NULL,
  `code` text NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `qty` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `search`
--

CREATE TABLE `search` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `params` json NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `search`
--

INSERT INTO `search` (`id`, `user_id`, `name`, `params`, `created_at`, `updated_at`) VALUES
(1, 3, 'savecooking', '{\"q\": \"cooking\", \"scope\": \"task\"}', '2020-10-19 10:31:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'meta_keyword', 'nicchris,furniture'),
(2, 'meta_description', 'Meta Description'),
(3, 'logo', 'upload/images/logo/NC_ONLY.png'),
(4, 'email', 'admin@nicchris.com'),
(5, 'facebook', 'https://www.facebook.com/nicchris'),
(6, 'instagram', 'https://instagram.com/nicchris'),
(7, 'google_analytics', ''),
(8, 'slider', 'a:5:{i:0;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/170911.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}i:1;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/170915.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}i:2;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/170919.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}i:3;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/170922.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}i:4;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/171018.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}}'),
(9, 'site_name', 'MTP'),
(10, 'whatsapp', ''),
(11, 'new_products', '1,3,7,8,53'),
(12, 'featured_products', '3,6,9');

-- --------------------------------------------------------

--
-- Table structure for table `shopping_cart`
--

CREATE TABLE `shopping_cart` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_option` text NOT NULL,
  `item_color` text NOT NULL,
  `item_ship_country` int(11) NOT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `promo_code` text NOT NULL,
  `date` datetime NOT NULL,
  `member_id` int(11) NOT NULL,
  `guest_id` text NOT NULL,
  `checkout_id` int(11) NOT NULL,
  `notified` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shopping_checkout`
--

CREATE TABLE `shopping_checkout` (
  `id` int(11) NOT NULL,
  `carts` text NOT NULL,
  `member_id` int(11) NOT NULL,
  `guest_id` text NOT NULL,
  `total` double NOT NULL,
  `email` text NOT NULL,
  `comment` text NOT NULL,
  `shipping` double NOT NULL,
  `shipping_method` text NOT NULL,
  `shipping_date` date NOT NULL,
  `shipping_time` int(11) NOT NULL,
  `delivery_salutation` text NOT NULL,
  `delivery_firstname` text NOT NULL,
  `delivery_lastname` text NOT NULL,
  `delivery_address1` text NOT NULL,
  `delivery_address2` text NOT NULL,
  `delivery_city` text NOT NULL,
  `delivery_zip` text NOT NULL,
  `delivery_country` int(11) NOT NULL,
  `delivery_state` int(11) NOT NULL,
  `delivery_contact` text NOT NULL,
  `billing_salutation` text NOT NULL,
  `billing_firstname` text NOT NULL,
  `billing_lastname` text NOT NULL,
  `billing_address1` text NOT NULL,
  `billing_address2` text NOT NULL,
  `billing_city` text NOT NULL,
  `billing_zip` text NOT NULL,
  `billing_country` int(11) NOT NULL,
  `billing_state` int(11) NOT NULL,
  `billing_contact` text NOT NULL,
  `pp_token` text NOT NULL,
  `pp_payer_id` text NOT NULL,
  `payment_date` datetime NOT NULL,
  `payment_method` text NOT NULL,
  `payment_reference` longtext NOT NULL,
  `payment_receipts` longtext NOT NULL,
  `status` int(11) NOT NULL,
  `tracking_no` text NOT NULL,
  `pp_GetExpressCheckoutDetails` text NOT NULL,
  `pp_DoExpressCheckoutPayment` text NOT NULL,
  `pp_build` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `number` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `sub_category` int(11) NOT NULL,
  `title` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('by-hour','lump-sum') COLLATE utf8_unicode_ci NOT NULL,
  `location` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `budget` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hours` int(11) DEFAULT NULL,
  `start_by` datetime NOT NULL,
  `complete_by` datetime NOT NULL,
  `assigned` datetime DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `status` enum('draft','published','in-progress','completed','cancelled','disputed') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `urgent` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `rejection_rate` int(11) NOT NULL DEFAULT '50' COMMENT 'This value is in percentage %',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `number`, `user_id`, `category`, `sub_category`, `title`, `slug`, `description`, `type`, `location`, `budget`, `hours`, `start_by`, `complete_by`, `assigned`, `assigned_to`, `status`, `urgent`, `rejection_rate`, `created_at`, `updated_at`) VALUES
('C74C4FE0-71C5-4530-8ED4-9CEBCBEAF1E9', 'T20101', '1', 5, 30, 'Test', 'test', 'Desc', 'lump-sum', 'Bangunan Dato Nazir, Jalan Hishammuddin, Bandar Kajang, 43000 Kajang, Selangor, Malaysia', 2000.00, 0, '2020-10-18 00:00:00', '2020-10-31 00:00:00', NULL, NULL, 'published', '0', 10, '2020-10-18 04:12:01', NULL),
('C01EA0AD-4E4B-43DE-8265-7C3B73C225DE', 'T20102', '2', 2, 16, 'Cooking a fish', 'cooking-a-fish-2', 'just cook it', 'by-hour', '8, Jalan Kerinchi, Bangsar South, 59200 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur, Malaysia', 200.00, 3, '2020-10-19 00:00:00', '2020-10-23 00:00:00', NULL, NULL, 'published', '1', 10, '2020-10-18 06:01:51', '2020-10-18 06:02:05'),
('3954FC5A-2101-41DF-AD2B-8AEC647B3948', 'T20103', '3', 1, 14, 'Wash 10kg clothes', 'wash-10kg-clothes-2', 'Wash 10kg clothes and deliver to my house', 'by-hour', 'LOT F-049&050,1ST floor Mid Valley Megamall, Mid Valley City, 58000 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur, Malaysia', 100.00, 8, '2020-10-20 00:00:00', '2020-10-22 00:00:00', '2020-10-19 18:28:04', 4, 'published', '1', 40, '2020-10-19 09:59:54', '2020-10-19 10:28:04'),
('5DB33E41-106A-45D6-B334-B22FDEAC2E79', 'T20104', '3', 4, 27, 'Perform 10 songs', 'perform-10-songs', 'Perform 10 songs with backup band', 'lump-sum', 'KL Eco City Road, 59200 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur, Malaysia', 100.00, 0, '2020-10-20 00:00:00', '2020-10-22 00:00:00', NULL, NULL, 'cancelled', '0', 50, '2020-10-19 10:22:28', '2020-10-19 10:33:09'),
('0AE1A0D5-ECD7-4223-A0CB-3CF1B7C9A7EF', 'T20105', '3', 2, 18, 'Deliver food to Taman Megah Ria', 'deliver-food-to-taman-megah-ria', 'Deliver 10 pax food to Taman Megah Ria', 'lump-sum', '37, Jalan Kempas 16, Taman Megah Ria, 81750 Masai, Johor, Malaysia', 50.00, 0, '2020-10-20 00:00:00', '2020-10-21 00:00:00', '2020-10-19 19:02:35', 2, 'published', '0', 40, '2020-10-19 10:50:15', '2020-10-19 11:02:35');

-- --------------------------------------------------------

--
-- Table structure for table `task_attachments`
--

CREATE TABLE `task_attachments` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_attachments`
--

INSERT INTO `task_attachments` (`id`, `task_id`, `attachment`, `type`, `created_at`, `updated_at`) VALUES
('32372426-FE48-4632-8799-79A14946AA10', 'C74C4FE0-71C5-4530-8ED4-9CEBCBEAF1E9', '/files/tasks/1/pein.png', 'png', '2020-10-18 04:12:01', NULL),
('882DBD30-F25B-45E9-9944-F0A7FCB98206', 'C01EA0AD-4E4B-43DE-8265-7C3B73C225DE', '/files/tasks/2/b2c.png', 'png', '2020-10-18 06:01:51', NULL),
('A1C7661A-422E-47B5-AD9B-A7E658FB8AB8', '3954FC5A-2101-41DF-AD2B-8AEC647B3948', '/files/tasks/3/favicon.png', 'png', '2020-10-19 09:59:54', NULL),
('88615375-7DC9-4C92-A7ED-F8CF5AC1220E', '3954FC5A-2101-41DF-AD2B-8AEC647B3948', '/files/tasks/3/OPERO_Email_Signature_Details.xlsx', 'xlsx', '2020-10-19 09:59:54', NULL),
('757FF008-53E9-429F-81FD-9702DD5F06E0', '5DB33E41-106A-45D6-B334-B22FDEAC2E79', '/files/tasks/3/OPERO_Email_Signature_Details.xlsx', 'xlsx', '2020-10-19 10:22:28', NULL),
('FF967C6F-6FC4-42C4-A84B-6F8C82C29DFE', '0AE1A0D5-ECD7-4223-A0CB-3CF1B7C9A7EF', '/files/tasks/3/O_Motif_Stroke.png', 'png', '2020-10-19 10:50:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_planning`
--

CREATE TABLE `task_planning` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `task_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `task_questions`
--

CREATE TABLE `task_questions` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_questions`
--

INSERT INTO `task_questions` (`id`, `task_id`, `question`, `created_at`, `updated_at`) VALUES
('77534D0E-5088-4EB7-A2CE-BEE6081B3EA6', 'C74C4FE0-71C5-4530-8ED4-9CEBCBEAF1E9', 'none', '2020-10-18 04:12:01', NULL),
('98EB128C-E555-41D1-A0DA-8E85E0B1C85A', 'C01EA0AD-4E4B-43DE-8265-7C3B73C225DE', 'how do you cook it', '2020-10-18 06:01:51', NULL),
('879C4521-3222-4CD3-BEB0-08C316093EC5', '3954FC5A-2101-41DF-AD2B-8AEC647B3948', 'Do you have your own transport?', '2020-10-19 09:59:54', NULL),
('C262AB44-F906-4D91-B35E-B9201D5DEF5A', '3954FC5A-2101-41DF-AD2B-8AEC647B3948', 'Do you have your own transport?', '2020-10-19 09:59:54', NULL),
('E8FC94BC-1FE9-4729-A6E6-BEF7979807FB', '3954FC5A-2101-41DF-AD2B-8AEC647B3948', 'Do you have your own transport?', '2020-10-19 09:59:54', NULL),
('FC7D6BCD-316F-44CE-AC9A-2D5DE02E1529', '3954FC5A-2101-41DF-AD2B-8AEC647B3948', 'Do you have your own transport?', '2020-10-19 09:59:54', NULL),
('AFFDD9FA-800C-424A-B048-E0A4AAE25611', '3954FC5A-2101-41DF-AD2B-8AEC647B3948', 'Do you have your own transport?', '2020-10-19 09:59:54', NULL),
('0D1EB337-3692-4034-955E-5359089D019F', '5DB33E41-106A-45D6-B334-B22FDEAC2E79', 'Do you have guitar?', '2020-10-19 10:22:28', NULL),
('291DC3D3-BC78-4635-A474-A8CC9EC7909D', '5DB33E41-106A-45D6-B334-B22FDEAC2E79', 'Do you have guitar?', '2020-10-19 10:22:28', NULL),
('0EB1A90F-27B7-48D7-8887-685FB59ED8C6', '5DB33E41-106A-45D6-B334-B22FDEAC2E79', 'Do you have guitar?', '2020-10-19 10:22:28', NULL),
('0BA77089-04A7-4E84-B1B9-7F113A48AB95', '5DB33E41-106A-45D6-B334-B22FDEAC2E79', 'Do you have guitar?', '2020-10-19 10:22:28', NULL),
('A544C4AC-08F7-42A2-9491-A9AC678D3BEC', '5DB33E41-106A-45D6-B334-B22FDEAC2E79', 'Do you have guitar?', '2020-10-19 10:22:28', NULL),
('488161CA-A3B1-4A83-A427-252E1D0B94F5', '0AE1A0D5-ECD7-4223-A0CB-3CF1B7C9A7EF', 'Do you have own transport?', '2020-10-19 10:50:15', NULL),
('07CDA521-FA5F-448B-A051-42642F3A675C', '6DCFC9DB-86DF-45B0-9BB2-88DE833EF962', 'Question 1?', '2020-10-19 10:53:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_tags`
--

CREATE TABLE `task_tags` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_tags`
--

INSERT INTO `task_tags` (`id`, `task_id`, `tag`, `created_at`, `updated_at`) VALUES
('8B7CD68E-27B5-40AD-94B9-1E775E221767', 'C01EA0AD-4E4B-43DE-8265-7C3B73C225DE', 'cook', '2020-10-18 06:01:51', NULL),
('C757E429-E3BD-4B39-808E-9E437DF9785D', 'C01EA0AD-4E4B-43DE-8265-7C3B73C225DE', 'fish', '2020-10-18 06:01:51', NULL),
('155FD134-62CE-4D39-B234-5E9DD7E912E5', '3954FC5A-2101-41DF-AD2B-8AEC647B3948', 'laundry', '2020-10-19 09:59:54', NULL),
('C5F7D66E-3818-4BE8-8A38-D45DF6B4FCE0', '3954FC5A-2101-41DF-AD2B-8AEC647B3948', 'dry cleaning', '2020-10-19 09:59:54', NULL),
('CF5222E6-79F8-4DE9-AD62-B926E5495F6D', '3954FC5A-2101-41DF-AD2B-8AEC647B3948', 'delivery', '2020-10-19 09:59:54', NULL),
('D1FD5864-B9DE-4D12-9CD5-CD90B78E7F08', '5DB33E41-106A-45D6-B334-B22FDEAC2E79', 'musician', '2020-10-19 10:22:28', NULL),
('398C4595-18C1-40C2-9455-1C69750CA26C', '5DB33E41-106A-45D6-B334-B22FDEAC2E79', 'band', '2020-10-19 10:22:28', NULL),
('661F8FFE-CC9B-4C19-B9EC-477B00D5879C', '5DB33E41-106A-45D6-B334-B22FDEAC2E79', 'busker', '2020-10-19 10:22:28', NULL),
('2062D547-9831-4F2B-99D6-733D9D589222', '5DB33E41-106A-45D6-B334-B22FDEAC2E79', 'guitar', '2020-10-19 10:22:28', NULL),
('E1EA24F2-870E-42BC-8516-ED693AB7DD1C', '0AE1A0D5-ECD7-4223-A0CB-3CF1B7C9A7EF', 'food delivery', '2020-10-19 10:50:15', NULL),
('9FD23284-B114-4DF2-8950-AA50301B9C6C', '0AE1A0D5-ECD7-4223-A0CB-3CF1B7C9A7EF', 'delivery', '2020-10-19 10:50:15', NULL),
('CD35158F-B9E8-4BB5-9242-5FE5BB11B122', '6DCFC9DB-86DF-45B0-9BB2-88DE833EF962', 'technician', '2020-10-19 10:53:40', NULL),
('9846E4E9-9172-4F1E-A055-8D82C36E7C00', '6DCFC9DB-86DF-45B0-9BB2-88DE833EF962', 'electrician', '2020-10-19 10:53:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `temp_token`
--

CREATE TABLE `temp_token` (
  `id` int(11) NOT NULL,
  `token` text,
  `email` text,
  `token_status` int(11) DEFAULT '0' COMMENT '0- Not Used 99 - Deleted',
  `created_by` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `expiry_date` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_token`
--

INSERT INTO `temp_token` (`id`, `token`, `email`, `token_status`, `created_by`, `expiry_date`) VALUES
(1, 'e52f44a01e75de28', 'kevinmvp@gmail.com', 99, '2020-10-18 05:40:02', '2020-10-18 06:40:02'),
(2, '71bd5aa0cfc76f58', 'syafiq@inspirenow.com.my', 99, '2020-10-19 09:28:44', '2020-10-19 10:28:44'),
(3, '91bb3324e47820e9', 'shah@inspirenow.com.my', 99, '2020-10-19 09:28:48', '2020-10-19 10:28:48'),
(4, 'c35310be07f5ac9e', 'kevinchewmvp@hotmail.com', 0, '2020-10-19 11:37:29', '2020-10-19 12:37:29');

-- --------------------------------------------------------

--
-- Table structure for table `tracking`
--

CREATE TABLE `tracking` (
  `id` int(11) NOT NULL,
  `checkout_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_admins`
--

CREATE TABLE `user_admins` (
  `admin_id` int(11) NOT NULL,
  `admin_username` text COLLATE utf8_bin NOT NULL,
  `admin_password` text COLLATE utf8_bin NOT NULL,
  `admin_name` text COLLATE utf8_bin NOT NULL,
  `admin_contact` text COLLATE utf8_bin NOT NULL,
  `admin_email` text COLLATE utf8_bin NOT NULL,
  `admin_status` int(1) NOT NULL,
  `admin_role` tinyint(1) NOT NULL,
  `admin_registered` datetime NOT NULL,
  `admin_last_activity` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user_admins`
--

INSERT INTO `user_admins` (`admin_id`, `admin_username`, `admin_password`, `admin_name`, `admin_contact`, `admin_email`, `admin_status`, `admin_role`, `admin_registered`, `admin_last_activity`) VALUES
(1, 'admin', 'e64b78fc3bc91bcbc7dc232ba8ec59e0', 'Kev', '60123456789', 'kevin@byte2c.com', 1, 1, '2012-10-24 16:00:58', '2018-01-30 16:22:03'),
(2, 'b2cadmin', 'd9f2e096af0c36ce0c509f779220a0b7', 'Byte2c Admin', '0176995737', 'admin@byte2c.com', 1, 1, '2017-10-25 16:14:02', '2017-10-25 16:14:02');

-- --------------------------------------------------------

--
-- Table structure for table `user_rating`
--

CREATE TABLE `user_rating` (
  `id` int(11) NOT NULL,
  `user_id` char(36) CHARACTER SET utf8 NOT NULL,
  `project_id` char(36) CHARACTER SET utf8 NOT NULL,
  `project_type` enum('task','job') CHARACTER SET utf8 NOT NULL DEFAULT 'task',
  `timeliness` decimal(10,1) NOT NULL DEFAULT '0.0',
  `quality` decimal(10,1) NOT NULL DEFAULT '0.0',
  `communication` decimal(10,1) NOT NULL DEFAULT '0.0',
  `responsiveness` decimal(10,1) NOT NULL DEFAULT '0.0',
  `review` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `author_id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT 'This would contain the user_id whom wrote the review',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_task`
--

CREATE TABLE `user_task` (
  `id` int(11) NOT NULL,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `type` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'task',
  `viewed` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_task`
--

INSERT INTO `user_task` (`id`, `user_id`, `task_id`, `type`, `viewed`, `created_at`, `updated_at`) VALUES
(1, '2', 'C74C4FE0-71C5-4530-8ED4-9CEBCBEAF1E9', 'task', 'Y', '2020-10-18 05:42:14', '2020-10-18 05:46:25'),
(2, '4', '3954FC5A-2101-41DF-AD2B-8AEC647B3948', 'task', 'Y', '2020-10-19 10:19:47', '2020-10-19 10:23:09'),
(3, '4', '5DB33E41-106A-45D6-B334-B22FDEAC2E79', 'task', 'Y', '2020-10-19 10:22:52', '2020-10-19 10:23:21'),
(4, '4', '0AE1A0D5-ECD7-4223-A0CB-3CF1B7C9A7EF', 'task', 'Y', '2020-10-19 10:50:32', '2020-10-19 10:51:27'),
(5, '2', '0AE1A0D5-ECD7-4223-A0CB-3CF1B7C9A7EF', 'task', 'Y', '2020-10-19 10:50:54', '2020-10-19 10:51:14');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(11) NOT NULL,
  `ip_address` text NOT NULL,
  `visit_count` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`,`question_id`) USING BTREE,
  ADD KEY `FK_question` (`question_id`),
  ADD KEY `FK_user` (`user_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `company_details`
--
ALTER TABLE `company_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employment_details`
--
ALTER TABLE `employment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `FK_task_id` (`task_id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `jobs_user_id_index` (`user_id`) USING BTREE,
  ADD KEY `jobs_category_index` (`category`) USING BTREE,
  ADD KEY `jobs_sub_category_index` (`sub_category`) USING BTREE,
  ADD KEY `jobs_experience_level_index` (`experience_level`) USING BTREE,
  ADD KEY `jobs_country_index` (`country`) USING BTREE,
  ADD KEY `jobs_state_index` (`state`) USING BTREE,
  ADD KEY `jobs_status_index` (`status`) USING BTREE;

--
-- Indexes for table `job_questions`
--
ALTER TABLE `job_questions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `job_questions_id_job_id_unique` (`id`,`job_id`),
  ADD KEY `job_questions_job_id_index` (`job_id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_logs`
--
ALTER TABLE `member_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preference`
--
ALTER TABLE `preference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promo_code`
--
ALTER TABLE `promo_code`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `search`
--
ALTER TABLE `search`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shopping_cart`
--
ALTER TABLE `shopping_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shopping_checkout`
--
ALTER TABLE `shopping_checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tasks_user_id_index` (`user_id`) USING BTREE,
  ADD KEY `tasks_category_index` (`category`) USING BTREE,
  ADD KEY `tasks_sub_category_index` (`sub_category`) USING BTREE,
  ADD KEY `tasks_type_index` (`type`) USING BTREE,
  ADD KEY `tasks_status_index` (`status`) USING BTREE;
ALTER TABLE `tasks` ADD FULLTEXT KEY `title_slug` (`title`,`slug`);

--
-- Indexes for table `task_attachments`
--
ALTER TABLE `task_attachments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `task_attachments_task_id_attachment_unique` (`task_id`,`attachment`),
  ADD KEY `task_attachments_task_id_index` (`task_id`),
  ADD KEY `task_attachments_type_index` (`type`);

--
-- Indexes for table `task_planning`
--
ALTER TABLE `task_planning`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `task_id` (`task_id`),
  ADD KEY `date` (`date`);

--
-- Indexes for table `task_questions`
--
ALTER TABLE `task_questions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `task_questions_id_task_id_unique` (`id`,`task_id`),
  ADD KEY `task_questions_task_id_index` (`task_id`);

--
-- Indexes for table `task_tags`
--
ALTER TABLE `task_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `task_tags_task_id_tag_unique` (`task_id`,`tag`),
  ADD KEY `task_tags_task_id_index` (`task_id`),
  ADD KEY `task_tags_tag_index` (`tag`);

--
-- Indexes for table `temp_token`
--
ALTER TABLE `temp_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tracking`
--
ALTER TABLE `tracking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_admins`
--
ALTER TABLE `user_admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `user_rating`
--
ALTER TABLE `user_rating`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `user_task`
--
ALTER TABLE `user_task`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `company_details`
--
ALTER TABLE `company_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `education`
--
ALTER TABLE `education`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employment_details`
--
ALTER TABLE `employment_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `member_logs`
--
ALTER TABLE `member_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `preference`
--
ALTER TABLE `preference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `promo_code`
--
ALTER TABLE `promo_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `search`
--
ALTER TABLE `search`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `shopping_cart`
--
ALTER TABLE `shopping_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shopping_checkout`
--
ALTER TABLE `shopping_checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `task_planning`
--
ALTER TABLE `task_planning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_token`
--
ALTER TABLE `temp_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tracking`
--
ALTER TABLE `tracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_admins`
--
ALTER TABLE `user_admins`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_rating`
--
ALTER TABLE `user_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_task`
--
ALTER TABLE `user_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
