2020-10-01
ALTER TABLE `employment_details` CHANGE `duration` `date_start` DATE NOT NULL;
ALTER TABLE `employment_details` ADD `date_end` DATE NOT NULL AFTER `date_start`; 
ALTER TABLE `members` ADD `skills` TEXT NOT NULL AFTER `emergency_contact_relation`, ADD `language` TEXT NOT NULL AFTER `skills`; 

2020-10-07
ALTER TABLE `members` ADD `dashboard_widget` TEXT NOT NULL AFTER `language`; 

2020-10-08
ALTER TABLE `members` CHANGE `dashboard_widget` `dashboard_widget` VARCHAR(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '2,4,5,6'; 
UPDATE `members` SET dashboard_widget = '2,4,5,6' WHERE dashboard_widget = ''

2020-10-09
ALTER TABLE `members` ADD `completion` TINYINT NOT NULL AFTER `dashboard_widget`; 

2020-10-12
ALTER TABLE `members` ADD `about` TEXT NULL AFTER `photo`;
ALTER TABLE `preference` ADD `bank` TEXT NULL AFTER `member_id`, ADD `acc_name` TEXT NULL AFTER `bank`, ADD `acc_num` INT NULL AFTER `acc_name`;

2020-10-13
ALTER TABLE `preference` CHANGE `work_load_hours` `work_load_hours` INT NOT NULL; 

2020-10-15
ALTER TABLE `preference` CHANGE `bank_acc_name` `acc_name` VARCHAR(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL; 
ALTER TABLE `preference` CHANGE `bank_acc_no` `acc_num` VARCHAR(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL; 

2020-10-19
ALTER TABLE `chats` ADD `viewed` INT NOT NULL AFTER `created_by`, ADD `viewed_date` DATETIME NOT NULL AFTER `viewed`; 

2020-11-02
ALTER TABLE `members` CHANGE `type` `type` TINYINT NOT NULL; 

2020-11-04
ALTER TABLE `preference` ADD `rm_radius` VARCHAR(32) NOT NULL AFTER `rm_work_location`; 
ALTER TABLE `preference` ADD `p_radius` VARCHAR(32) NOT NULL AFTER `p_work_location` 

2020-11-06
ALTER TABLE `members` ADD `contact_method` VARCHAR(32) NOT NULL AFTER `emergency_contact_relation`; 
ALTER TABLE `employment_details` ADD `currently_working` INT NOT NULL AFTER `date_end`; 
ALTER TABLE `employment_details` CHANGE `location` `country` INT NOT NULL; 
ALTER TABLE `employment_details` ADD `state` INT NOT NULL AFTER `country`; 
ALTER TABLE `education` ADD `country` INT NOT NULL AFTER `grade`, ADD `state` INT NOT NULL AFTER `country`; 
ALTER TABLE `employment_details` CHANGE `responsibilities` `job_responsibilities` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL; 

2020-11-27
ALTER TABLE `preference` CHANGE `work_day` `work_day` VARCHAR(16) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL; 

ALTER TABLE `preference`
  DROP `work_day_full`,
  DROP `work_day_after`,
  DROP `work_day_am`,
  DROP `work_day_pm`;

ALTER TABLE `preference` ADD `work_day_weekday` INT NOT NULL AFTER `work_day`, ADD `work_day_weekday_full` INT NOT NULL AFTER `work_day_weekday`, ADD `work_day_weekday_after` INT NOT NULL AFTER `work_day_weekday_full`, ADD `work_day_weekend` INT NOT NULL AFTER `work_day_weekday_after`, ADD `work_day_weekend_am` INT NOT NULL AFTER `work_day_weekend`, ADD `work_day_weekend_pm` INT NOT NULL AFTER `work_day_weekend_am`; 

-------- 08/12/2020 Kevin ------------
ALTER TABLE `tasks` CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;

2020-12-08
CREATE TABLE `task_payment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `task_id` varchar(64) NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(64) NOT NULL,
  `phone` varchar(16) NOT NULL,
  `date` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `task_payment`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `task_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

CREATE TABLE `senangpay_callback` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL,
  `order_id` text NOT NULL,
  `response` longtext NOT NULL,
  `server` longtext NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `senangpay_callback` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, add PRIMARY KEY (`id`); 

ALTER TABLE `task_payment` ADD `total` FLOAT(10,2) NOT NULL AFTER `assigned_to`, ADD `service_charge` FLOAT(10,2) NOT NULL AFTER `total`; 

2020-12-10
CREATE TABLE `industry` (
  `id` int(11) NOT NULL,
  `images` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) NOT NULL,
  `sortby` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `industry` (`id`, `images`, `title`, `description`, `parent`, `sortby`, `status`) VALUES
(1, '', 'Accounting/Finance', '', 0, 0, 1),
(2, '', 'Admin/Human Resources', '', 0, 0, 1),
(3, '', 'Sales/Marketing', '', 0, 0, 1),
(4, '', 'Arts/Media/Communications', '', 0, 0, 1),
(5, '', 'Services', '', 0, 0, 1),
(6, '', 'Hotel/Restaurant', '', 0, 0, 1),
(7, '', 'Education/Training', '', 0, 0, 1),
(8, '', 'Computer/Information Technology', '', 0, 0, 1),
(9, '', 'Engineering', '', 0, 0, 1),
(10, '', 'Manufacturing', '', 0, 0, 1),
(11, '', 'Building/Construction', '', 0, 0, 1),
(12, '', 'Sciences', '', 0, 0, 1),
(13, '', 'Sciences', '', 0, 0, 1),
(14, '', 'Others', '', 0, 0, 1),
(15, '', 'Audit & Taxation', '', 1, 0, 1),
(16, '', 'General/Cost Accounting', '', 1, 0, 1),
(17, '', 'Corporate Finance/Investment', '', 1, 0, 1),
(18, '', 'Banking/Financial', '', 1, 0, 1),
(19, '', 'Clerical/General Admin', '', 2, 0, 1),
(20, '', 'Human Resources', '', 2, 0, 1),
(21, '', 'Secretarial/Executive Personal Assistant', '', 2, 0, 1),
(22, '', 'Top Management', '', 2, 0, 1),
(23, '', 'Marketing/Business Dev', '', 3, 0, 1),
(24, '', 'Sales - Corporate', '', 3, 0, 1),
(25, '', 'Sales - Eng/Tech/IT', '', 3, 0, 1),
(26, '', 'Sales - Financial Services', '', 3, 0, 1),
(27, '', 'Retail Sales', '', 3, 0, 1),
(28, '', 'Merchandising', '', 3, 0, 1),
(29, '', 'Telesales/Telemarketing', '', 3, 0, 1),
(30, '', 'E-commerce', '', 3, 0, 1),
(31, '', 'Digital Marketing', '', 3, 0, 1),
(32, '', 'Advertising', '', 4, 0, 1),
(33, '', 'Arts/Creative Design', '', 4, 0, 1),
(34, '', 'Entertainment', '', 4, 0, 1),
(35, '', 'Public Relations', '', 4, 0, 1),
(36, '', 'Personal Care', '', 5, 0, 1),
(37, '', 'Armed Forces', '', 5, 0, 1),
(38, '', 'Social Services', '', 5, 0, 1),
(39, '', 'Customer Service', '', 5, 0, 1),
(40, '', 'Lawyer/Legal Asst', '', 5, 0, 1),
(41, '', 'Logistics/Supply Chain', '', 5, 0, 1),
(42, '', 'Tech & Helpdesk Support', '', 5, 0, 1),
(43, '', 'Food/Beverage/Restaurant', '', 6, 0, 1),
(44, '', 'Hotel/Tourism', '', 6, 0, 1),
(45, '', 'Education', '', 7, 0, 1),
(46, '', 'Training & Development', '', 7, 0, 1),
(47, '', 'IT-Software', '', 8, 0, 1),
(48, '', 'IT-Hardware', '', 8, 0, 1),
(49, '', 'IT-Network/Sys/DB Admin', '', 8, 0, 1),
(50, '', 'Chemical Engineering', '', 9, 0, 1),
(51, '', 'Electronics', '', 9, 0, 1),
(52, '', 'Electrical', '', 9, 0, 1),
(53, '', 'Other Engineering', '', 9, 0, 1),
(54, '', 'Environmental', '', 9, 0, 1),
(55, '', 'Oil/Gas', '', 9, 0, 1),
(56, '', 'Mechanical', '', 9, 0, 1),
(57, '', 'Industrial Engineering', '', 9, 0, 1),
(58, '', 'Maintenance', '', 10, 0, 1),
(59, '', 'Purchasing/Material Mgmt', '', 10, 0, 1),
(60, '', 'Manufacturing', '', 10, 0, 1),
(61, '', 'Process Control', '', 10, 0, 1),
(62, '', 'Quality Assurance', '', 10, 0, 1),
(63, '', 'Property/Real Estate', '', 11, 0, 1),
(64, '', 'Architect/Interior Design', '', 11, 0, 1),
(65, '', 'Civil/Construction', '', 11, 0, 1),
(66, '', 'Quantity Surveying', '', 11, 0, 1),
(67, '', 'Agriculture', '', 12, 0, 1),
(68, '', 'Actuarial/Statistics', '', 12, 0, 1),
(69, '', 'Food Tech/Nutritionist', '', 12, 0, 1),
(70, '', 'Geology/Geophysics', '', 12, 0, 1),
(71, '', 'Aviation', '', 12, 0, 1),
(72, '', 'Biotechnology', '', 12, 0, 1),
(73, '', 'Chemistry', '', 12, 0, 1),
(74, '', 'Science & Technology', '', 12, 0, 1),
(75, '', 'Biomedical', '', 12, 0, 1),
(76, '', 'Practitioner/Medical Asst', '', 13, 0, 1),
(77, '', 'Pharmacy', '', 13, 0, 1),
(78, '', 'Diagnosis/Others', '', 13, 0, 1),
(79, '', 'Journalist/Editors', '', 14, 0, 1),
(80, '', 'General Work', '', 14, 0, 1),
(81, '', 'Publishing', '', 14, 0, 1),
(82, '', 'Others', '', 14, 0, 1);

ALTER TABLE `industry`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
COMMIT;

------ 11/12/2020 ------
UPDATE `skills` SET `name` = 'Human Resource & Recruitment' WHERE `skills`.`id` = 75;

------ 3/1/2021 ------
UPDATE `cities` SET `name` = 'Pontian' WHERE `cities`.`id` = 76544;
UPDATE `cities` SET `name` = 'Segamat' WHERE `cities`.`id` = 76535;
UPDATE `cities` SET `name` = 'Kulaiaya' WHERE `cities`.`id` = 76508;
UPDATE `cities` SET `name` = 'Ledang' WHERE `cities`.`id` = 76510;
DELETE FROM `cities` WHERE `cities`.`id` = 76416;
DELETE FROM `cities` WHERE `cities`.`id` = 76433;
DELETE FROM `cities` WHERE `cities`.`id` = 76435;
DELETE FROM `cities` WHERE `cities`.`id` = 76437;
DELETE FROM `cities` WHERE `cities`.`id` = 76438;
DELETE FROM `cities` WHERE `cities`.`id` = 76439;
DELETE FROM `cities` WHERE `cities`.`id` = 76440;
DELETE FROM `cities` WHERE `cities`.`id` = 76441;
DELETE FROM `cities` WHERE `cities`.`id` = 76442;
DELETE FROM `cities` WHERE `cities`.`id` = 76443;
DELETE FROM `cities` WHERE `cities`.`id` = 76444;
DELETE FROM `cities` WHERE `cities`.`id` = 76480;
UPDATE `cities` SET `name` = 'Baling' WHERE `cities`.`id` = 76412;
UPDATE `cities` SET `name` = 'Bandar Baharu' WHERE `cities`.`id` = 76449;
UPDATE `cities` SET `name` = 'Kota Setar' WHERE `cities`.`id` = 76471;
UPDATE `cities` SET `name` = 'Kuala Muda' WHERE `cities`.`id` = 76503;
UPDATE `cities` SET `name` = 'Pulau Langkawi' WHERE `cities`.`id` = 76514;
UPDATE `cities` SET `name` = 'Kubang Pasu' WHERE `cities`.`id` = 76492;
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Padang Terap', '1947', '02', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Pendang', '1947', '02', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Pokok Sena', '1947', '02', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Sik', '1947', '02', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Yan', '1947', '02', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Bachok', '1946', '02', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Jeli', '1946', '02', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Kuala Krai', '1946', '02', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Machang', '1946', '02', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Pasir Puteh', '1946', '02', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Tanah Merah', '1946', '02', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
UPDATE `cities` SET `name` = 'Masjid Tanah' WHERE `cities`.`id` = 76461;
UPDATE `cities` SET `name` = 'Ayer Keroh' WHERE `cities`.`id` = 76463;
UPDATE `cities` SET `name` = 'Ayer Molek' WHERE `cities`.`id` = 76464;
UPDATE `cities` SET `name` = 'Bukit Baharu' WHERE `cities`.`id` = 76468;
UPDATE `cities` SET `name` = 'Klebang' WHERE `cities`.`id` = 76486;
UPDATE `cities` SET `name` = 'Melaka' WHERE `cities`.`id` = 76520;
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Jasin', '1941', '04', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Jelebu', '1948', '05', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Jempol', '1948', '05', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Rembau', '1948', '05', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
UPDATE `cities` SET `name` = 'Bentong' WHERE `cities`.`id` = 76428;
UPDATE `cities` SET `name` = 'Cameron Highlands' WHERE `cities`.`id` = 76575;
UPDATE `cities` SET `name` = 'Temerloh' WHERE `cities`.`id` = 76584;
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Bera', '1940', '06', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Maran', '1940', '06', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Rompin', '1940', '06', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
UPDATE `cities` SET `name` = 'Bidor' WHERE `cities`.`id` = 76429;
UPDATE `cities` SET `name` = 'Tapah' WHERE `cities`.`id` = 76579;
UPDATE `cities` SET `name` = 'Kuala Dungun' WHERE `cities`.`id` = 76472;
UPDATE `cities` SET `name` = 'Kemaman' WHERE `cities`.`id` = 76453;
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Bukit Bintang', '1949', '14', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Bandar Tun Razak', '1949', '14', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Cheras', '1949', '14', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Setiawangsa', '1949', '14', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Kepong', '1949', '14', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Lembah Pantai', '1949', '14', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Batu', '1949', '14', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Seputeh', '1949', '14', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Segambut', '1949', '14', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Titiwangsa', '1949', '14', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Wangsa Maju', '1949', '14', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
UPDATE `cities` SET `name` = 'Kuala Penyu' WHERE `cities`.`id` = 76446;
UPDATE `cities` SET `name` = 'Nabawan' WHERE `cities`.`id` = 76484;
UPDATE `cities` SET `name` = 'Labuan' WHERE `cities`.`id` = 76417;
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Sipitang', '1936', '12', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Tambunan', '1936', '12', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Tenom', '1936', '12', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Kota Marudu', '1936', '12', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Pitas', '1936', '12', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Beluran', '1936', '12', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Kinabatangan', '1936', '12', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Sandakan', '1936', '12', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Tongod', '1936', '12', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Kunak', '1936', '12', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Penampang', '1936', '12', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Tuaran', '1936', '12', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Betong', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Saratok', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Tatau', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Belaga', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Song', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Bau', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Lundu', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Dalat', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Daro', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Asajaya', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Samarahan', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Simunjan', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Julau', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Meradong', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Pekan', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Siburan', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Tebedu', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Kanowit', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Selangau', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Lubok Antu', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Sri Aman', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');
INSERT INTO `cities` (`id`, `name`, `state_id`, `state_code`, `country_id`, `country_code`, `latitude`, `longitude`, `created_at`, `updated_on`, `flag`, `wikiDataId`) VALUES (NULL, 'Sri Aman', '1937', '13', '132', 'MY', '6.12104000', '6.12104000', '2014-01-01 09:01:01.000000', CURRENT_TIMESTAMP, '1', 'Q474868');


2021-01-14
---------------
CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `title` varchar(16) NOT NULL,
  `code` varchar(8) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


INSERT INTO `language` (`id`, `title`, `code`, `status`) VALUES
(1, 'English', 'EN', 1);

CREATE TABLE `language_text` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `var` varchar(128) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `language_text` (`id`, `language_id`, `var`, `text`) VALUES
(1, 1, 'title_home', 'Home'),
(2, 1, 'title_log_in', 'Log In');

ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `language_text`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `language_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;


2021-01-19
---------------
CREATE TABLE `category_language_text` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `industry_language_text` (
  `id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `skill_language_text` (
  `id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `category_language_text`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `industry_language_text`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `skill_language_text`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `category_language_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `industry_language_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `skill_language_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;


2021-01-29
---------------
ALTER TABLE `members` ADD `plan` INT NOT NULL AFTER `type`; 
UPDATE `members` SET plan = 2

2021-02-01
--------------
CREATE TABLE `task_category_language_text` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `task_category_language_text`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `task_category_language_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

2021-02-16
---------------
ALTER TABLE `skills` CHANGE `status` `status` INT NULL DEFAULT '1' COMMENT '0 - inactive, 1- active'; 
ALTER TABLE `task_category` ADD `new` TINYINT NOT NULL DEFAULT '0' COMMENT '0 - default from system, 1 - input by user' AFTER `sortby`; 

2021-02-19
---------------
ALTER TABLE `industry` ADD `new` TINYINT NOT NULL COMMENT '0 - default from system, 1 - input by user' AFTER `sortby`; 

2021-02-27
---------------
CREATE TABLE `subscription` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `price` float(10,2) NOT NULL,
  `price_promo` float(10,2) NOT NULL,
  `max_post` int(11) NOT NULL,
  `billing_access` int(11) NOT NULL,
  `support_email` int(11) NOT NULL,
  `support_account_manager` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `subscription` (`id`, `title`, `description`, `price`, `price_promo`, `max_post`, `billing_access`, `support_email`, `support_account_manager`, `status`) VALUES
(1, 'Personal Membership', 'The basic package allows even the most diligent user full access to all our income opportunities.', 20.00, 0.00, 0, 1, 1, 1, 1),
(2, 'Premium Membership', 'For those who may want additional features and advanced capabilities as part of their MakeTimePay experience.', 25.00, 20.00, 0, 1, 1, 1, 1),
(3, '', '', 0.00, 0.00, 0, 0, 0, 0, 0),
(4, 'Corporate Membership', 'Organizations will be looking to identify and engage individuals for a range of professional tasks.', 100.00, 10.00, 0, 1, 1, 1, 1),
(5, '', '', 0.00, 0.00, 0, 0, 0, 0, 0),
(6, '', '', 0.00, 0.00, 0, 0, 0, 0, 0);

CREATE TABLE `subscription_language_text` (
  `id` int(11) NOT NULL,
  `subscription_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `subscription_language_text` (`id`, `subscription_id`, `language_id`, `title`, `description`) VALUES
(13, 0, 2, '', ''),
(65, 4, 2, 'Keahlian Korporat', 'Organisasi akan berusaha untuk mengenal pasti dan melibatkan individu untuk pelbagai tugas profesional.'),
(66, 5, 2, '', ''),
(67, 6, 2, '', ''),
(68, 1, 2, 'Keahlian Peribadi', 'Pakej asas membolehkan pengguna yang paling rajin mengakses sepenuhnya semua peluang pendapatan kami.'),
(69, 2, 2, 'Keahlian Premium', 'Bagi mereka yang mungkin mahukan ciri tambahan dan kemampuan canggih sebagai sebahagian daripada pengalaman MakeTimePay mereka.'),
(70, 3, 2, '', '');

ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `subscription_language_text`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

ALTER TABLE `subscription_language_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
COMMIT;

2021-03-02
---------------
ALTER TABLE `subscription` ADD `price_promo_status` INT NOT NULL AFTER `price_promo`; 


2021-03-02
---------------
ALTER TABLE `countries` ADD `status` INT NOT NULL DEFAULT '1' AFTER `wikiDataId`; 
ALTER TABLE `states` ADD `status` INT NOT NULL DEFAULT '1' AFTER `wikiDataId`; 
ALTER TABLE `cities` ADD `status` INT NOT NULL DEFAULT '1' AFTER `wikiDataId`; 


2021-03-06
---------------
CREATE TABLE `user_admin_role` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `permissions` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `user_admin_role` (`id`, `title`, `permissions`) VALUES
(1, 'Super Admin', 'individual,company,task,job,master_data,master_config'),
(2, 'Admin', 'individual,company,subscription');

ALTER TABLE `user_admin_role`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user_admin_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

CREATE TABLE `interest` (
  `id` int(11) NOT NULL,
  `images` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) NOT NULL,
  `sortby` int(11) NOT NULL,
  `new` tinyint(4) NOT NULL COMMENT '0 - default from system, 1 - input by user',
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `interest_language_text` (
  `id` int(11) NOT NULL,
  `interest_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `interest`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `interest_language_text`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `interest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `interest_language_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;


2021-03-08
---------------
ALTER TABLE `subscription` ADD `title_2` TEXT NOT NULL AFTER `support_account_manager`, ADD `description_2` TEXT NOT NULL AFTER `title_2`, ADD `features_2` TEXT NOT NULL AFTER `description_2`; 

ALTER TABLE `subscription_language_text` ADD `title_2` TEXT NOT NULL AFTER `description`, ADD `description_2` TEXT NOT NULL AFTER `title_2`, ADD `features_2` TEXT NOT NULL AFTER `description_2`; 


2021-03-11
---------------
ALTER TABLE `members` ADD `status_date` DATE NOT NULL AFTER `status`; 
ALTER TABLE `members` ADD `status_notify` INT NOT NULL AFTER `status_date`; 


