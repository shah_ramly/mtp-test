-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 05, 2020 at 02:53 PM
-- Server version: 5.7.21
-- PHP Version: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mtp`
--

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `description` text,
  `parent` int(11) DEFAULT '0' COMMENT '0 - parent',
  `status` tinyint(4) DEFAULT '1' COMMENT '0 - inactive, 1- active',
  `new` tinyint(4) DEFAULT '0' COMMENT '0 - default from system, 1 - input by user',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

INSERT INTO skills (name,parent) values ("Art & Design",0);
INSERT INTO skills (name,parent) values ("3d Printing Skills",1);
INSERT INTO skills (name,parent) values ("Adobe Skills",1);
INSERT INTO skills (name,parent) values ("Architecture Skills",1);
INSERT INTO skills (name, parent) values ("Autocad Skills",1);
INSERT INTO skills (name, parent) values ("Cad Skills",1);
INSERT INTO skills (name, parent) values ("Chef Skills",1);
INSERT INTO skills (name, parent) values ("Cooking Skills",1);
INSERT INTO skills (name, parent) values ("Design Skills",1);
INSERT INTO skills (name, parent) values ("Drawing Skills",1);
INSERT INTO skills (name, parent) values ("Illustrator Skills",1);
INSERT INTO skills (name, parent) values ("Indesign Skills",1);
INSERT INTO skills (name, parent) values ("Maya Skills",1);
INSERT INTO skills (name, parent) values ("Photoshop Skills",1);
INSERT INTO skills (name, parent) values ("Product Design Skills",1);
INSERT INTO skills (name, parent) values ("Prototyping Skills",1);
INSERT INTO skills (name, parent) values ("Revit Skills",1);
INSERT INTO skills (name, parent) values ("Solid Skills",1);
INSERT INTO skills (name, parent) values ("Ui Skills",1);
INSERT INTO skills (name, parent) values ("Ux Skills",1);
INSERT INTO skills (name, parent) values ("Video Production Skills",1);
INSERT INTO skills (name, parent) values ("Videography Skills",1);
INSERT INTO skills (name, parent) values ("Web Design Skills",1);
INSERT INTO skills (name,parent) values ("Business, Finance & Administration",0);
INSERT INTO skills (name, parent) values ("Account Management Skills", 24);
INSERT INTO skills (name, parent) values ("Acquisition Skills", 24);
INSERT INTO skills (name, parent) values ("Administration Skills", 24);
INSERT INTO skills (name, parent) values ("Audit Skills", 24);
INSERT INTO skills (name, parent) values ("Banking Skills", 24);
INSERT INTO skills (name, parent) values ("Billing Skills", 24);
INSERT INTO skills (name, parent) values ("Blockchain Skills", 24);
INSERT INTO skills (name, parent) values ("Budget Management Skills", 24);
INSERT INTO skills (name, parent) values ("Budgeting Skills", 24);
INSERT INTO skills (name, parent) values ("Business Analysis Skills", 24);
INSERT INTO skills (name, parent) values ("Business Analytics Skills", 24);
INSERT INTO skills (name, parent) values ("Business Development Skills", 24);
INSERT INTO skills (name, parent) values ("Business Intelligence Skills", 24);
INSERT INTO skills (name, parent) values ("Business Management Skills", 24);
INSERT INTO skills (name, parent) values ("Compliance Skills", 24);
INSERT INTO skills (name, parent) values ("Contract Management Skills", 24);
INSERT INTO skills (name, parent) values ("Event Management Skills", 24);
INSERT INTO skills (name, parent) values ("Event Planning Skills", 24);
INSERT INTO skills (name, parent) values ("Financial Analysis Skills", 24);
INSERT INTO skills (name, parent) values ("Financial Management Skills", 24);
INSERT INTO skills (name, parent) values ("Financial Modeling Skills", 24);
INSERT INTO skills (name, parent) values ("Financial Services Skills", 24);
INSERT INTO skills (name, parent) values ("Forecasting Skills", 24);
INSERT INTO skills (name, parent) values ("Fundraising Skills", 24);
INSERT INTO skills (name, parent) values ("Hospitality Skills", 24);
INSERT INTO skills (name, parent) values ("Ibm Skills", 24);
INSERT INTO skills (name, parent) values ("Insurance Skills", 24);
INSERT INTO skills (name, parent) values ("Logistics Skills", 24);
INSERT INTO skills (name, parent) values ("Market Research Skills", 24);
INSERT INTO skills (name, parent) values ("Payroll Skills", 24);
INSERT INTO skills (name, parent) values ("Procurement Skills", 24);
INSERT INTO skills (name, parent) values ("Product Development Skills", 24);
INSERT INTO skills (name, parent) values ("Risk Management Skills", 24);
INSERT INTO skills (name, parent) values ("Sap Skills", 24);
INSERT INTO skills (name, parent) values ("Stakeholder Management Skills", 24);
INSERT INTO skills (name, parent) values ("Supply Chain Skills", 24);
INSERT INTO skills (name, parent) values ("Sustainability Skills", 24);
INSERT INTO skills (name, parent) values ("Education",0);
INSERT INTO skills (name, parent) values("Bilingual Skills",62);
INSERT INTO skills (name, parent) values("English Skills",62);
INSERT INTO skills (name, parent) values("Mathmatics Skills",62);
INSERT INTO skills (name, parent) values ("Engineering",0);
INSERT INTO skills (name, parent) values ("Automation Skills", 66);
INSERT INTO skills (name, parent) values ("Automotive Skills", 66);
INSERT INTO skills (name, parent) values ("Electrical Skills", 66);
INSERT INTO skills (name, parent) values ("Hardware Skills", 66);
INSERT INTO skills (name, parent) values ("Maintenance Skills", 66);
INSERT INTO skills (name, parent) values ("Manufacturing Skills", 66);
INSERT INTO skills (name, parent) values ("Robotics Skills", 66);
INSERT INTO skills (name, parent) values ("Welding Skills", 66);
INSERT INTO skills (name, parent) values ("Human Resource & Recruitmen",0);
INSERT INTO skills (name, parent) values ("Hris Skills", 75);
INSERT INTO skills (name, parent) values ("Human Resources Skills", 75);
INSERT INTO skills (name, parent) values ("Peoplesoft Skills", 75);
INSERT INTO skills (name, parent) values ("Performance Management Skills", 75);
INSERT INTO skills (name, parent) values ("Recruitment Skills", 75);
INSERT INTO skills (name, parent) values ("Relationship Management Skills", 75);
INSERT INTO skills (name, parent) values ("Training Skills", 75);
INSERT INTO skills (name, parent) values ("IT & Data Management",0);
INSERT INTO skills (name, parent) values ("Active Directory Skills", 83);
INSERT INTO skills (name, parent) values ("Analytics Skills", 83);
INSERT INTO skills (name, parent) values ("Angular Skills", 83);
INSERT INTO skills (name, parent) values ("Artificial Intelligence Skills", 83);
INSERT INTO skills (name, parent) values ("As400 Skills", 83);
INSERT INTO skills (name, parent) values ("Aws Skills", 83);
INSERT INTO skills (name, parent) values ("Big Data Skills", 83);
INSERT INTO skills (name, parent) values ("Bpm Skills", 83);
INSERT INTO skills (name, parent) values ("C++ Skills", 83);
INSERT INTO skills (name, parent) values ("Ccna Skills", 83);
INSERT INTO skills (name, parent) values ("Cloud Skills", 83);
INSERT INTO skills (name, parent) values ("Cloud Computing Skills", 83);
INSERT INTO skills (name, parent) values ("Cms Skills", 83);
INSERT INTO skills (name, parent) values ("Coding Skills", 83);
INSERT INTO skills (name, parent) values ("Css Skills", 83);
INSERT INTO skills (name, parent) values ("Cyber Security Skills", 83);
INSERT INTO skills (name, parent) values ("Data Analysis Skills", 83);
INSERT INTO skills (name, parent) values ("Data Analytics Skills", 83);
INSERT INTO skills (name, parent) values ("Data Center Skills", 83);
INSERT INTO skills (name, parent) values ("Data Collection Skills", 83);
INSERT INTO skills (name, parent) values ("Data Management Skills", 83);
INSERT INTO skills (name, parent) values ("Data Mining Skills", 83);
INSERT INTO skills (name, parent) values ("Data Modelling Skills", 83);
INSERT INTO skills (name, parent) values ("Data Warehouse Skills", 83);
INSERT INTO skills (name, parent) values ("Database Skills", 83);
INSERT INTO skills (name, parent) values ("Database Management Skills", 83);
INSERT INTO skills (name, parent) values ("Debugging Skills", 83);
INSERT INTO skills (name, parent) values ("Devops Skills", 83);
INSERT INTO skills (name, parent) values ("Enterprise Architecture Skills", 83);
INSERT INTO skills (name, parent) values ("Etl Skills", 83);
INSERT INTO skills (name, parent) values ("Gis Skills", 83);
INSERT INTO skills (name, parent) values ("Hadoop Skills", 83);
INSERT INTO skills (name, parent) values ("Information Security Skills", 83);
INSERT INTO skills (name, parent) values ("Information Technology Skills", 83);
INSERT INTO skills (name, parent) values ("Iot Skills", 83);
INSERT INTO skills (name, parent) values ("It Security Skills", 83);
INSERT INTO skills (name, parent) values ("Java Skills", 83);
INSERT INTO skills (name, parent) values ("Javascript Skills", 83);
INSERT INTO skills (name, parent) values ("Karma Skills", 83);
INSERT INTO skills (name, parent) values ("Linux Skills", 83);
INSERT INTO skills (name, parent) values ("Machine Learning Skills", 83);
INSERT INTO skills (name, parent) values ("Mainframe Skills", 83);
INSERT INTO skills (name, parent) values ("Microsoft Access Skills", 83);
INSERT INTO skills (name, parent) values ("Microsoft Office Skills", 83);
INSERT INTO skills (name, parent) values ("Network Skills", 83);
INSERT INTO skills (name, parent) values ("Network Security Skills", 83);
INSERT INTO skills (name, parent) values ("Oop Skills", 83);
INSERT INTO skills (name, parent) values ("Oracle Skills", 83);
INSERT INTO skills (name, parent) values ("Penetration Testing Skills", 83);
INSERT INTO skills (name, parent) values ("Php Skills", 83);
INSERT INTO skills (name, parent) values ("React Skills", 83);
INSERT INTO skills (name, parent) values ("Scripting Skills", 83);
INSERT INTO skills (name, parent) values ("Security Skills", 83);
INSERT INTO skills (name, parent) values ("Software Development Skills", 83);
INSERT INTO skills (name, parent) values ("Software Engineering Skills", 83);
INSERT INTO skills (name, parent) values ("Software Testing Skills", 83);
INSERT INTO skills (name, parent) values ("Solution Architecture Skills", 83);
INSERT INTO skills (name, parent) values ("Spark Skills", 83);
INSERT INTO skills (name, parent) values ("Spss Skills", 83);
INSERT INTO skills (name, parent) values ("Sql Server Skills", 83);
INSERT INTO skills (name, parent) values ("Statistical Analysis Skills", 83);
INSERT INTO skills (name, parent) values ("Statistics Skills", 83);
INSERT INTO skills (name, parent) values ("Tableau Skills", 83);
INSERT INTO skills (name, parent) values ("Unix Skills", 83);
INSERT INTO skills (name, parent) values ("Vba Skills", 83);
INSERT INTO skills (name, parent) values ("Web Development Skills", 83);
INSERT INTO skills (name, parent) values ("Wordpress Skills", 83);
INSERT INTO skills (name, parent) values ("Office",0);;
INSERT INTO skills (name, parent) values ("Acrobat Skills", 151);
INSERT INTO skills (name, parent) values ("Internet Skills", 151);
INSERT INTO skills (name, parent) values ("Microsoft Word Skills", 151);
INSERT INTO skills (name, parent) values ("Ms Excel Skills", 151);
INSERT INTO skills (name, parent) values ("Outlook Skills", 151);
INSERT INTO skills (name, parent) values ("Powerpoint Skills", 151);
INSERT INTO skills (name, parent) values ("Scheduling Skills", 151);
INSERT INTO skills (name, parent) values ("Technical Support Skills", 151);
INSERT INTO skills (name, parent) values ("Translation Skills", 151);
INSERT INTO skills (name, parent) values ("Project Management",0);
INSERT INTO skills (name, parent) values ("Agile Skills", 161);
INSERT INTO skills (name, parent) values ("Documentation Skills", 161);
INSERT INTO skills (name, parent) values ("Editing Skills", 161);
INSERT INTO skills (name, parent) values ("Implementation Skills", 161);
INSERT INTO skills (name, parent) values ("Integration Skills", 161);
INSERT INTO skills (name, parent) values ("Inventory Management Skills", 161);
INSERT INTO skills (name, parent) values ("Lean Skills", 161);
INSERT INTO skills (name, parent) values ("Lean Six Sigma Skills", 161);
INSERT INTO skills (name, parent) values ("Microsoft Project Skills", 161);
INSERT INTO skills (name, parent) values ("Ms Project Skills", 161);
INSERT INTO skills (name, parent) values ("Operations Management Skills", 161);
INSERT INTO skills (name, parent) values ("Planning Skills", 161);
INSERT INTO skills (name, parent) values ("Pmp Skills", 161);
INSERT INTO skills (name, parent) values ("Process Improvement Skills", 161);
INSERT INTO skills (name, parent) values ("Product Management Skills", 161);
INSERT INTO skills (name, parent) values ("Quality Assurance Skills", 161);
INSERT INTO skills (name, parent) values ("Quality Management Skills", 161);
INSERT INTO skills (name, parent) values ("Report Writing Skills", 161);
INSERT INTO skills (name, parent) values ("Reporting Skills", 161);
INSERT INTO skills (name, parent) values ("Resource Management Skills", 161);
INSERT INTO skills (name, parent) values ("Sas Skills", 161);
INSERT INTO skills (name, parent) values ("Testing Skills", 161);
INSERT INTO skills (name, parent) values ("Vendor Management Skill", 161);
INSERT INTO skills (name, parent) values ("Sales & Marketing", 0);
INSERT INTO skills (name, parent) values ("Advertising Skills", 185);
INSERT INTO skills (name, parent) values ("Cold Calling Skills", 185);
INSERT INTO skills (name, parent) values ("Consulting Skills", 185);
INSERT INTO skills (name, parent) values ("Content Management Skills", 185);
INSERT INTO skills (name, parent) values ("Ecommerce Skills", 185);
INSERT INTO skills (name, parent) values ("Email Marketing Skills", 185);
INSERT INTO skills (name, parent) values ("Facebook Skills", 185);
INSERT INTO skills (name, parent) values ("Lead Generation Skills", 185);
INSERT INTO skills (name, parent) values ("Outreach Skills", 185);
INSERT INTO skills (name, parent) values ("Public Relations Skills", 185);
INSERT INTO skills (name, parent) values ("Purchasing Skills", 185);
INSERT INTO skills (name, parent) values ("Sales Management Skills", 185);
INSERT INTO skills (name, parent) values ("Storytelling Skills", 185);
INSERT INTO skills (name, parent) values ("Technical Writing Skills", 185);
INSERT INTO skills (name, parent) values ("Telecommunications Skills", 185);
INSERT INTO skills (name, parent) values ("Trading Skills", 185);
INSERT INTO skills (name, parent) values ("Communication & Interpersonal",0);
INSERT INTO skills (name, parent) values ("Accountability Skills", 202);
INSERT INTO skills (name, parent) values ("Adaptability Skills", 202);
INSERT INTO skills (name, parent) values ("Client Relations Skills", 202);
INSERT INTO skills (name, parent) values ("Community Skills", 202);
INSERT INTO skills (name, parent) values ("Persuasion Skills", 202);
INSERT INTO skills (name, parent) values ("Management",0);
INSERT INTO skills (name, parent) values ("Asset Management Skills", 208);
INSERT INTO skills (name, parent) values ("Change Management Skills", 208);
INSERT INTO skills (name, parent) values ("Client Management Skills", 208);
INSERT INTO skills (name, parent) values ("Coaching Skills", 208);
INSERT INTO skills (name, parent) values ("Crisis Management Skills", 208);
INSERT INTO skills (name, parent) values ("Facilitation Skills", 208);
INSERT INTO skills (name, parent) values ("Personal",0);
INSERT INTO skills (name, parent) values ("Flexibility Skills", 215);
INSERT INTO skills (name, parent) values ("Independent Skills", 215);
INSERT INTO skills (name, parent) values ("Innovation Skills", 215);
INSERT INTO skills (name, parent) values ("Motivation Skills", 215);
INSERT INTO skills (name, parent) values ("Multi-tasking Skills", 215);
INSERT INTO skills (name, parent) values ("Proactive Skills", 215);
INSERT INTO skills (name, parent) values ("Reliability Skills", 215);
INSERT INTO skills (name, parent) values ("Responsibility Skills", 215);
INSERT INTO skills (name, parent) values ("Strong Work Ethic Skills", 215);
INSERT INTO skills (name, parent) values ("Work Under Pressure Skills", 215);
INSERT INTO skills (name, parent) values ("Teamwork",0);
INSERT INTO skills (name, parent) values ("Collaborative Skills", 226);
INSERT INTO skills (name, parent) values ("Coordinating Skills", 226);
INSERT INTO skills (name, parent) values ("Mentoring Skills", 226);
INSERT INTO skills (name, parent) values ("People Management Skills", 226);
INSERT INTO skills (name, parent) values ("Supervision Skills", 226);
INSERT INTO skills (name, parent) values ("Team Leadership Skills", 226);
INSERT INTO skills (name, parent) values ("Team Management Skills", 226);
INSERT INTO skills (name, parent) values ("Thought Process",0);
INSERT INTO skills (name, parent) values ("Analytical Thinking Skills", 234);
INSERT INTO skills (name, parent) values ("Attention To Detail Skills", 234);
INSERT INTO skills (name, parent) values ("Awareness Skills", 234);
INSERT INTO skills (name, parent) values ("Creative Thinking Skills", 234);
INSERT INTO skills (name, parent) values ("Detail-oriented Skills", 234);
INSERT INTO skills (name, parent) values ("Evaluation Skills", 234);
INSERT INTO skills (name, parent) values ("Fast Learner Skills", 234);
INSERT INTO skills (name, parent) values ("Patience Skills", 234);
INSERT INTO skills (name, parent) values ("Strategic Thinking Skills", 234);
