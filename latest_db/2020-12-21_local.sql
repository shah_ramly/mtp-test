-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 21, 2020 at 12:10 PM
-- Server version: 5.7.21
-- PHP Version: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mtp`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
CREATE TABLE IF NOT EXISTS `answers` (
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `question_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `answer` mediumtext COLLATE utf8_unicode_ci,
  `type` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'task',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`,`question_id`) USING BTREE,
  KEY `FK_question` (`question_id`),
  KEY `FK_user` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`task_id`, `question_id`, `user_id`, `answer`, `type`, `created_at`, `updated_at`, `id`) VALUES
('494048EA-4A1C-40F6-889E-8DE858400541', '8CBDAD73-8C01-40C8-B757-ACBF003E6A37', 1, '1', 'task', '2020-11-10 10:41:30', NULL, 1),
('494048EA-4A1C-40F6-889E-8DE858400541', '7BFE73A0-DBB8-4B56-9E15-1CAC780A5DAF', 1, '2', 'task', '2020-11-10 10:41:30', NULL, 2),
('E2BCC7C7-6BB8-48AD-A0A1-20018EE91EB6', 'C1B4347E-0FEF-4F24-8F49-93DFE5950107', 1, '1', 'task', '2020-11-10 11:54:59', NULL, 3),
('E2BCC7C7-6BB8-48AD-A0A1-20018EE91EB6', '637F4562-C106-4D8A-963F-F461B047B5CC', 1, '2', 'task', '2020-11-10 11:54:59', NULL, 4),
('537ED05F-1619-4B96-A6FF-1B20157E7A42', 'A7C13D53-079A-4E87-AB46-E511D156A4FA', 1, '1', 'task', '2020-11-10 12:06:36', NULL, 5),
('537ED05F-1619-4B96-A6FF-1B20157E7A42', 'E371C680-966E-490A-9B5C-6EC1953193EB', 1, '2', 'task', '2020-11-10 12:06:36', NULL, 6),
('6C2F88C9-EEB4-4849-9A43-FCC7F4D8A605', 'D5020F1A-5873-480A-A759-59C074B7C6D9', 4, 'a', 'task', '2020-11-10 19:22:40', NULL, 7),
('9B07069A-7C92-424F-9F42-6BEE7EA0A172', '8EFDFEDF-24EE-461C-A57C-FB03F4FAE57A', 10, 'Answer 1', 'job', '2020-11-17 06:09:22', NULL, 8),
('9B07069A-7C92-424F-9F42-6BEE7EA0A172', 'F15E4583-9AF8-4E20-9323-A20E3A3E977C', 10, 'Answer 2', 'job', '2020-11-17 06:09:22', NULL, 9),
('CDA7DB4E-8703-453F-B33B-D83285F72054', '05B4F1B7-4E2C-4FAD-8D1A-C0929AAD17A0', 10, 'aaa', 'task', '2020-11-19 08:34:28', NULL, 10);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) NOT NULL,
  `sortby` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `images`, `title`, `description`, `parent`, `sortby`, `status`) VALUES
(1, '', 'Accounting/Finance', '', 0, 0, 1),
(2, '', 'Audit & Taxation', '', 1, 0, 1),
(3, '', 'General/Cost Accounting', '', 1, 0, 1),
(4, '', 'Corporate Finance/Investment', '', 1, 0, 1),
(5, '', 'Banking/Financial', '', 1, 0, 1),
(6, '', 'Admin/Human Resources', '', 0, 0, 1),
(7, '', 'Clerical/General Admin', '', 6, 0, 1),
(8, '', 'Human Resources', '', 6, 0, 1),
(9, '', 'Secretarial/Executive Personal Assistant', '', 6, 0, 1),
(10, '', 'Top Management', '', 6, 0, 1),
(11, '', 'Sales/Marketing', '', 0, 0, 1),
(12, '', 'Marketing/Business Dev', '', 11, 0, 1),
(13, '', 'Sales - Corporate', '', 11, 0, 1),
(14, '', 'Sales - Eng/Tech/IT', '', 11, 0, 1),
(15, '', 'Sales - Financial Services', '', 11, 0, 1),
(16, '', 'Retail Sales', '', 11, 0, 1),
(17, '', 'Merchandising', '', 11, 0, 1),
(18, '', 'Telesales/Telemarketing', '', 11, 0, 1),
(19, '', 'E-commerce', '', 11, 0, 1),
(20, '', 'Digital Marketing', '', 11, 0, 1),
(21, '', 'Arts/Media/Communications', '', 0, 0, 1),
(22, '', 'Advertising', '', 21, 0, 1),
(23, '', 'Arts/Creative Design', '', 21, 0, 1),
(24, '', 'Entertainment', '', 21, 0, 1),
(25, '', 'Public Relations', '', 21, 0, 1),
(26, '', 'Services', '', 0, 0, 1),
(27, '', 'Personal Care', '', 26, 0, 1),
(28, '', 'Armed Forces', '', 26, 0, 1),
(29, '', 'Social Services', '', 26, 0, 1),
(30, '', 'Customer Service', '', 26, 0, 1),
(31, '', 'Lawyer/Legal Asst', '', 26, 0, 1),
(32, '', 'Logistics/Supply Chain', '', 26, 0, 1),
(33, '', 'Tech & Helpdesk Support', '', 26, 0, 1),
(34, '', 'Hotel/Restaurant', '', 0, 0, 1),
(35, '', 'Food/Beverage/Restaurant', '', 34, 0, 1),
(36, '', 'Hotel/Tourism', '', 34, 0, 1),
(37, '', 'Education/Training', '', 0, 0, 1),
(38, '', 'Education', '', 37, 0, 1),
(39, '', 'Training & Development', '', 37, 0, 1),
(40, '', 'Computer/Information Technology', '', 0, 0, 1),
(41, '', 'IT-Software', '', 40, 0, 1),
(42, '', 'IT-Hardware', '', 40, 0, 1),
(43, '', 'IT-Network/Sys/DB Admin', '', 40, 0, 1),
(44, '', 'Engineering', '', 0, 0, 1),
(45, '', 'Chemical Engineering', '', 44, 0, 1),
(46, '', 'Electronics', '', 44, 0, 1),
(47, '', 'Electrical', '', 44, 0, 1),
(48, '', 'Other Engineering', '', 44, 0, 1),
(49, '', 'Environmental', '', 44, 0, 1),
(50, '', 'Oil/Gas', '', 44, 0, 1),
(51, '', 'Mechanical', '', 44, 0, 1),
(52, '', 'Industrial Engineering', '', 44, 0, 1),
(53, '', 'Manufacturing', '', 0, 0, 1),
(54, '', 'Maintenance', '', 53, 0, 1),
(55, '', 'Purchasing/Material Mgmt', '', 53, 0, 1),
(56, '', 'Manufacturing', '', 53, 0, 1),
(57, '', 'Process Control', '', 53, 0, 1),
(58, '', 'Quality Assurance', '', 53, 0, 1),
(59, '', 'Building/Construction', '', 0, 0, 1),
(60, '', 'Property/Real Estate', '', 59, 0, 1),
(61, '', 'Architect/Interior Design', '', 59, 0, 1),
(62, '', 'Architect/Interior Design', '', 59, 0, 1),
(63, '', 'Civil/Construction', '', 59, 0, 1),
(64, '', 'Quantity Surveying', '', 59, 0, 1),
(65, '', 'Sciences', '', 0, 0, 1),
(66, '', 'Agriculture', '', 65, 0, 1),
(67, '', 'Actuarial/Statistics', '', 65, 0, 1),
(68, '', 'Food Tech/Nutritionist', '', 65, 0, 1),
(69, '', 'Geology/Geophysics', '', 65, 0, 1),
(70, '', 'Aviation', '', 65, 0, 1),
(71, '', 'Biotechnology', '', 65, 0, 1),
(72, '', 'Chemistry', '', 65, 0, 1),
(73, '', 'Science & Technology', '', 65, 0, 1),
(74, '', 'Biomedical', '', 65, 0, 1),
(75, '', 'Healthcare', '', 0, 0, 1),
(76, '', 'Practitioner/Medical Asst', '', 75, 0, 1),
(77, '', 'Pharmacy', '', 75, 0, 1),
(78, '', 'Diagnosis/Others', '', 75, 0, 1),
(79, '', 'Others', '', 0, 0, 1),
(80, '', 'Journalist/Editors', '', 79, 0, 1),
(81, '', 'General Work', '', 79, 0, 1),
(82, '', 'Publishing', '', 79, 0, 1),
(83, '', 'Others', '', 79, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

DROP TABLE IF EXISTS `chats`;
CREATE TABLE IF NOT EXISTS `chats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` text,
  `receiver_id` text,
  `contents` text,
  `task_id` text,
  `type` varchar(365) DEFAULT 'text',
  `file_extension` text,
  `batch_id` text,
  `deleted` tinyint(4) DEFAULT '0',
  `reply_id` text,
  `size` text,
  `created_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `viewed` int(11) NOT NULL,
  `viewed_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `sender_id`, `receiver_id`, `contents`, `task_id`, `type`, `file_extension`, `batch_id`, `deleted`, `reply_id`, `size`, `created_by`, `viewed`, `viewed_date`) VALUES
(1, '2', '1', 'Hello', '494048EA-4A1C-40F6-889E-8DE858400541', 'text', NULL, NULL, 0, '', NULL, '2020-11-10 10:42:22', 1, '2020-11-10 19:49:08'),
(2, '1', '2', 'Hello', '494048EA-4A1C-40F6-889E-8DE858400541', 'text', NULL, NULL, 0, '', NULL, '2020-11-10 10:44:21', 1, '2020-11-27 13:34:45'),
(3, '2', '1', 'files/chats/494048EA-4A1C-40F6-889E-8DE858400541/chat-footer.png', '494048EA-4A1C-40F6-889E-8DE858400541', 'image', 'png', '5faa6f270a463', 0, '', '99652', '2020-11-10 10:44:55', 1, '2020-11-10 19:49:08'),
(4, '1', '2', 'files/chats/494048EA-4A1C-40F6-889E-8DE858400541/ImpactReport_Arts&Culture.pdf', '494048EA-4A1C-40F6-889E-8DE858400541', 'file', 'pdf', '5faa6f3732427', 0, '', '3413406', '2020-11-10 10:45:11', 1, '2020-11-27 13:34:45'),
(5, '1', '2', 'files/chats/494048EA-4A1C-40F6-889E-8DE858400541/ImpactReport_Sports.pdf', '494048EA-4A1C-40F6-889E-8DE858400541', 'file', 'pdf', '5faa6f4ee85d8', 0, '', '3415048', '2020-11-10 10:45:34', 1, '2020-11-27 13:34:45'),
(6, '1', '2', 'files/chats/494048EA-4A1C-40F6-889E-8DE858400541/tinder-logo.jpg', '494048EA-4A1C-40F6-889E-8DE858400541', 'image', 'jpg', '5faa6f72c722d', 0, '', '11893', '2020-11-10 10:46:10', 1, '2020-11-27 13:34:45'),
(7, '1', '2', 'Taip banyak sikit', '494048EA-4A1C-40F6-889E-8DE858400541', 'text', NULL, NULL, 0, '', NULL, '2020-11-10 10:46:42', 1, '2020-11-27 13:34:45'),
(8, '2', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '494048EA-4A1C-40F6-889E-8DE858400541', 'text', NULL, NULL, 0, '', NULL, '2020-11-10 10:47:06', 1, '2020-11-10 19:49:08'),
(9, '1', '2', 'AWS bill', '494048EA-4A1C-40F6-889E-8DE858400541', 'textMix', '', '5faa6fabcab0d', 0, '', '', '2020-11-10 10:47:07', 1, '2020-11-27 13:34:45'),
(10, '1', '2', 'files/chats/494048EA-4A1C-40F6-889E-8DE858400541/year-2020-graph.PNG', '494048EA-4A1C-40F6-889E-8DE858400541', 'imageMix', 'png', '5faa6fabcab0d', 0, '', '32724', '2020-11-10 10:47:07', 1, '2020-11-27 13:34:45'),
(11, '2', '1', 'Please find above attachments', '494048EA-4A1C-40F6-889E-8DE858400541', 'textMix', '', '5faa6fbe2a8d4', 0, '', '', '2020-11-10 10:47:26', 1, '2020-11-10 19:49:08'),
(12, '2', '1', 'Please find above attachments', '494048EA-4A1C-40F6-889E-8DE858400541', 'textMix', '', '5faa6fd29138a', 0, '', '', '2020-11-10 10:47:46', 1, '2020-11-10 19:49:08'),
(13, '1', '2', 'Please find my attachment', '494048EA-4A1C-40F6-889E-8DE858400541', 'textMix', '', '5faa6fde419e7', 0, '', '', '2020-11-10 10:47:58', 1, '2020-11-27 13:34:45'),
(14, '1', '2', 'files/chats/494048EA-4A1C-40F6-889E-8DE858400541/ImpactReport_Environment.pdf', '494048EA-4A1C-40F6-889E-8DE858400541', 'fileMix', 'pdf', '5faa6fde419e7', 0, '', '3965573', '2020-11-10 10:47:58', 1, '2020-11-27 13:34:45'),
(15, '2', '1', 'Please find above attachments', '494048EA-4A1C-40F6-889E-8DE858400541', 'textMix', '', '5faa6fe97c268', 0, '', '', '2020-11-10 10:48:09', 1, '2020-11-10 19:49:08'),
(16, '2', '1', 'files/chats/494048EA-4A1C-40F6-889E-8DE858400541/districts_of_malaysia.pdf', '494048EA-4A1C-40F6-889E-8DE858400541', 'file', 'pdf', '5faa6ffc216a7', 0, '', '555964', '2020-11-10 10:48:28', 1, '2020-11-10 19:49:08'),
(17, '2', '1', 'Please find above attachments', '494048EA-4A1C-40F6-889E-8DE858400541', 'textMix', '', '5faa7043bcd98', 0, '', '', '2020-11-10 10:49:39', 1, '2020-11-10 19:49:08'),
(18, '2', '1', 'files/chats/494048EA-4A1C-40F6-889E-8DE858400541/Foo-Bar.ppt', '494048EA-4A1C-40F6-889E-8DE858400541', 'fileMix', 'ppt', '5faa7043bcd98', 0, '', '63488', '2020-11-10 10:49:39', 1, '2020-11-10 19:49:08'),
(19, '2', '1', 'Please find video playback above', '494048EA-4A1C-40F6-889E-8DE858400541', 'textMix', '', '5faa715a6ab2b', 0, '', '', '2020-11-10 10:54:18', 1, '2020-11-10 19:49:08'),
(20, '2', '1', 'files/chats/494048EA-4A1C-40F6-889E-8DE858400541/videoplayback.mp4', '494048EA-4A1C-40F6-889E-8DE858400541', 'fileMix', 'mp4', '5faa715a6ab2b', 0, '', '4032314', '2020-11-10 10:54:18', 1, '2020-11-10 19:49:08'),
(21, '2', '1', 'files/chats/494048EA-4A1C-40F6-889E-8DE858400541/03-11-2020-list.txt', '494048EA-4A1C-40F6-889E-8DE858400541', 'file', 'txt', '5faa74cc4ef2f', 0, '', '583', '2020-11-10 11:09:00', 1, '2020-11-10 19:49:08'),
(22, '2', '1', 'files/chats/494048EA-4A1C-40F6-889E-8DE858400541/chats.jpeg', '494048EA-4A1C-40F6-889E-8DE858400541', 'image', 'jpeg', '5faa74cc4ef2f', 0, '', '74249', '2020-11-10 11:09:00', 1, '2020-11-10 19:49:08'),
(23, '2', '1', 'files/chats/494048EA-4A1C-40F6-889E-8DE858400541/districts_of_malaysia.pdf', '494048EA-4A1C-40F6-889E-8DE858400541', 'file', 'pdf', '5faa74cc4ef2f', 0, '', '555964', '2020-11-10 11:09:00', 1, '2020-11-10 19:49:08'),
(24, '4', '6', 'hi', '5F130178-1620-4912-9A7B-F0C54D145372', 'text', NULL, NULL, 0, '', NULL, '2020-11-11 07:59:11', 1, '2020-11-12 01:11:22'),
(25, '6', '4', 'oh ya', '5F130178-1620-4912-9A7B-F0C54D145372', 'text', NULL, NULL, 0, '', NULL, '2020-11-11 07:59:23', 1, '2020-11-11 16:12:17'),
(26, '6', '4', 'nah', '5F130178-1620-4912-9A7B-F0C54D145372', 'textMix', '', '5fab99eabb1dc', 0, '', '', '2020-11-11 07:59:38', 1, '2020-11-11 16:12:17'),
(27, '6', '4', 'files/chats/5F130178-1620-4912-9A7B-F0C54D145372/WhatsApp Image 2020-10-19 at 09.11.48.jpeg', '5F130178-1620-4912-9A7B-F0C54D145372', 'imageMix', 'jpeg', '5fab99eabb1dc', 0, '', '36522', '2020-11-11 07:59:38', 1, '2020-11-11 16:12:17'),
(28, '4', '6', 'okay', '5F130178-1620-4912-9A7B-F0C54D145372', 'text', NULL, NULL, 0, '5fab99eabb1dc', NULL, '2020-11-11 07:59:54', 1, '2020-11-12 01:11:22'),
(29, '4', '6', 'sure completed?', '5F130178-1620-4912-9A7B-F0C54D145372', 'text', NULL, NULL, 0, '', NULL, '2020-11-11 08:10:25', 1, '2020-11-12 01:11:22'),
(30, '6', '4', 'ya', '5F130178-1620-4912-9A7B-F0C54D145372', 'text', NULL, NULL, 0, '', NULL, '2020-11-11 08:11:03', 1, '2020-11-11 16:12:17'),
(31, '4', '6', 'wait', '5F130178-1620-4912-9A7B-F0C54D145372', 'text', NULL, NULL, 0, '', NULL, '2020-11-11 08:12:15', 1, '2020-11-12 01:11:22'),
(32, '10', '2', 'test', 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'text', NULL, NULL, 0, '', NULL, '2020-11-12 06:48:35', 1, '2020-11-13 00:48:49'),
(33, '10', '2', 'ah', '66012D96-BA2D-451F-B413-95F2330B54C1', 'text', NULL, NULL, 0, '', NULL, '2020-11-12 16:48:41', 1, '2020-11-13 00:48:49'),
(34, '11', '10', 'hi', 'EE236862-F5EE-46CA-A70B-851A50C1A7A4', 'text', NULL, NULL, 0, '', NULL, '2020-11-16 03:14:33', 1, '2020-12-03 11:22:40'),
(35, '11', '10', 'files/chats/EE236862-F5EE-46CA-A70B-851A50C1A7A4/Alex_invoice.jpeg', 'EE236862-F5EE-46CA-A70B-851A50C1A7A4', 'image', 'jpeg', '5fb1eea58bfd9', 0, '', '86820', '2020-11-16 03:14:45', 1, '2020-12-03 11:22:40'),
(36, '11', '10', 'hi', '9B07069A-7C92-424F-9F42-6BEE7EA0A172', 'text', NULL, NULL, 0, '', NULL, '2020-11-17 06:16:34', 1, '2020-12-03 11:22:40'),
(37, '11', '10', 'hi', '769D0CD3-2F98-43E4-AAD2-D36C74DA3679', 'text', NULL, NULL, 0, '', NULL, '2020-11-17 15:58:54', 1, '2020-12-03 11:22:40'),
(38, '10', '11', 'yes', '769D0CD3-2F98-43E4-AAD2-D36C74DA3679', 'text', NULL, NULL, 0, '', NULL, '2020-11-17 15:59:02', 1, '2020-11-18 18:17:51'),
(39, '10', '11', 'files/chats/769D0CD3-2F98-43E4-AAD2-D36C74DA3679/FOCS_StudF03 - Students_ Progress Report (October 2020)_Batch 2.pdf', '769D0CD3-2F98-43E4-AAD2-D36C74DA3679', 'file', 'pdf', '5fb3f35932245', 0, '', '190782', '2020-11-17 15:59:21', 1, '2020-11-18 18:17:51'),
(40, '11', '10', 'hi', '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'text', NULL, NULL, 0, '', NULL, '2020-11-18 05:34:33', 1, '2020-12-03 11:22:40'),
(41, '10', '11', 'hey', '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'text', NULL, NULL, 0, '', NULL, '2020-11-18 05:34:42', 1, '2020-11-18 18:17:51'),
(42, '10', '11', 'hi', '769D0CD3-2F98-43E4-AAD2-D36C74DA3679', 'text', NULL, NULL, 0, '', NULL, '2020-11-18 05:48:58', 1, '2020-11-18 18:17:51'),
(43, '11', '10', 'yes', '769D0CD3-2F98-43E4-AAD2-D36C74DA3679', 'text', NULL, NULL, 0, '', NULL, '2020-11-18 05:49:11', 1, '2020-12-03 11:22:40'),
(44, '10', '11', 'hi', '769D0CD3-2F98-43E4-AAD2-D36C74DA3679', 'text', NULL, NULL, 0, '', NULL, '2020-11-18 07:14:44', 1, '2020-11-18 18:17:51'),
(45, '10', '2', 'aa', '66012D96-BA2D-451F-B413-95F2330B54C1', 'text', NULL, NULL, 0, '', NULL, '2020-12-03 03:22:59', 0, '0000-00-00 00:00:00'),
(46, '10', '2', 'files/chats/66012D96-BA2D-451F-B413-95F2330B54C1/QUOTATION VE5216.pdf', '66012D96-BA2D-451F-B413-95F2330B54C1', 'file', 'pdf', '5fc85a1e4f995', 0, '', '63098', '2020-12-03 03:23:10', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `post_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'If it''s a reply of another comment',
  `comment` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `viewed` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `parent_id`, `comment`, `viewed`, `created_at`, `updated_at`) VALUES
(1, '1', '494048EA-4A1C-40F6-889E-8DE858400541', 0, 'Hello', 'Y', '2020-11-10 10:39:36', '2020-11-10 10:39:57'),
(2, '2', '494048EA-4A1C-40F6-889E-8DE858400541', 1, 'Yes', 'Y', '2020-11-10 10:40:14', '2020-11-10 10:40:14'),
(3, '1', '494048EA-4A1C-40F6-889E-8DE858400541', 1, 'Nothing', 'Y', '2020-11-10 10:40:33', '2020-11-10 11:09:27'),
(4, '2', 'F87485C2-9653-4BFA-826E-0C2A235C7144', 0, 'hi', 'Y', '2020-11-12 06:46:59', '2020-11-12 06:47:13'),
(5, '10', 'F87485C2-9653-4BFA-826E-0C2A235C7144', 4, 'yes', 'Y', '2020-11-12 06:47:19', '2020-11-12 06:47:23'),
(6, '10', '70E8134A-10DB-475B-8180-E74CA2481EF2', 0, 'yes', 'N', '2020-11-12 14:16:23', NULL),
(7, '10', '9B07069A-7C92-424F-9F42-6BEE7EA0A172', 0, 'test question', 'Y', '2020-11-17 06:04:44', '2020-11-18 07:15:49'),
(8, '10', '8A72888F-4FBF-4D14-8E3F-A9E95711E581', 0, 'how hot', 'Y', '2020-11-18 07:18:05', '2020-11-18 07:18:13'),
(9, '11', '8A72888F-4FBF-4D14-8E3F-A9E95711E581', 8, 'veli hot', 'Y', '2020-11-18 07:18:21', '2020-11-18 07:18:23'),
(10, '11', '8A72888F-4FBF-4D14-8E3F-A9E95711E581', 8, 'veli hot', 'Y', '2020-11-18 07:18:21', '2020-11-18 07:18:23'),
(11, '11', '8A72888F-4FBF-4D14-8E3F-A9E95711E581', 8, 'are you?', 'Y', '2020-11-18 07:18:45', '2020-11-18 07:18:48'),
(12, '10', '8A72888F-4FBF-4D14-8E3F-A9E95711E581', 8, 'ya gua', 'Y', '2020-11-18 07:19:39', '2020-11-19 03:06:23');

-- --------------------------------------------------------

--
-- Table structure for table `company_details`
--

DROP TABLE IF EXISTS `company_details`;
CREATE TABLE IF NOT EXISTS `company_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `reg_num` text,
  `email` text,
  `company_phone_prefix` varchar(8) NOT NULL,
  `company_phone` varchar(16) NOT NULL,
  `about_us` text NOT NULL,
  `industry` text,
  `photos` text NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_details`
--

INSERT INTO `company_details` (`id`, `name`, `reg_num`, `email`, `company_phone_prefix`, `company_phone`, `about_us`, `industry`, `photos`, `member_id`) VALUES
(1, 'Kevin Company Sdn Bhd', 'aSLKDLA-121', 'kevinaa@byte.com', '02', '+60210291102', 'ajNSLKDNASd\r\naS\r\nDASD\r\nA\r\nSd\r\nAS\r\nD', '', 'a:0:{}', 11);

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
CREATE TABLE IF NOT EXISTS `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` text,
  `doc_location` text,
  `member_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
CREATE TABLE IF NOT EXISTS `education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `highest_edu_level` text,
  `grad_year` text,
  `edu_institution` text,
  `field` text,
  `major` text,
  `grade` text,
  `country` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`id`, `highest_edu_level`, `grad_year`, `edu_institution`, `field`, `major`, `grade`, `country`, `state`, `member_id`) VALUES
(1, 'Degree', '2015', 'Universiti Kebangsaan Malaysia', 'Engineering', 'Electrical', '3.79', 132, 1944, 2),
(2, 'Master', '2016', 'Universiti Malaya', 'Computer Science', 'Multimedia Computing', '4.00', 132, 1949, 2),
(3, 'PhD', '2020', 'Universiti Teknologi Malaysia', 'Business', 'Artificial Intelligence', '3.00', 102, 1805, 2),
(4, 'Certification', '2018', 'University Africa', 'Computer Science', 'IT', '3.2', 132, 1949, 1),
(5, 'Degree', '2016', 'University California', 'Computer Science', 'Business', '3.4', 117, 974, 1),
(6, 'Master', '2015', 'Doloribus hic aliqua', 'Computer Science', 'Major in AI', '3.8', 132, 1947, 5),
(7, 'Degree', '2017', 'asdsadsad', 'Business', 'IT', 'A', 132, 1944, 6),
(8, 'Degree', '2015', 'University Putra', 'Computer Science', 'IT system', '3.6', 132, 1938, 7),
(9, 'Degree', '2015', 'Universiti Malaya', 'Computer Science', 'Artificial Intelligence', '4.00', 132, 1949, 8),
(10, 'Degree', '2019', 'Universiti Kebangsaan Malaysia', 'Engineering', 'Electrical', '3.80', 132, 1944, 8);

-- --------------------------------------------------------

--
-- Table structure for table `employment_details`
--

DROP TABLE IF EXISTS `employment_details`;
CREATE TABLE IF NOT EXISTS `employment_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` text,
  `position` text,
  `date_start` date NOT NULL,
  `date_end` date DEFAULT NULL,
  `currently_working` int(11) NOT NULL,
  `department` text,
  `job_desc` text,
  `job_responsibilities` text,
  `remuneration` text,
  `country` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `industry` text,
  `member_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employment_details`
--

INSERT INTO `employment_details` (`id`, `company_name`, `position`, `date_start`, `date_end`, `currently_working`, `department`, `job_desc`, `job_responsibilities`, `remuneration`, `country`, `state`, `industry`, `member_id`) VALUES
(1, 'InspireNow Sdn Bhd', 'UI/UX Developer', '2019-04-10', '0000-00-00', 1, 'Technical', '<ul><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li></ul>', '<ol><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li></ol>', '10000', 132, 1947, 'Engineering', 2),
(2, 'INKA Creative Sdn Bhd', 'UI/UX Developer', '2017-04-10', '2020-12-31', 0, 'Creative', '<ul><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><b>Lorem ipsum dolor sit amet</b>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><i>Lorem ipsum dolor sit amet</i>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><u>Lorem ipsum dolor sit amet</u>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><b><i><u>Lorem ipsum dolor sit amet</u></i></b>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li></ul>', '<ol><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><b>Lorem ipsum</b> dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><i>Lorem ipsum dolor sit amet</i>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><u>Lorem ipsum dolor sit amet</u>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><b><i><u>Lorem ipsum dolor sit amet</u></i></b>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li></ol>', '12000', 132, 1949, 'Engineering', 2),
(3, 'Inspirenow sdn bhd', 'IT Assistant', '2020-11-11', '2020-11-13', 0, 'IT Department', '<ul><li>A job specification could be considered a more precise job description that details the exact educational degrees, experience, skills, and requirements for a role. In most cases, these terms are used interchangeably and nearly always describe the same document.</li><li>A job specification could be considered a more precise job description that details the exact educational degrees, experience, skills, and requirements for a role. In most cases, these terms are used interchangeably and nearly always describe the same document.</li><li>A job specification could be considered a more precise job description that details the exact educational degrees, experience, skills, and requirements for a role. In most cases, these terms are used interchangeably and nearly always describe the same document.</li></ul>', '<ul><li>A job specification could be considered a more precise job description that details the exact educational degrees, experience, skills, and requirements for a role. In most cases, these terms are used interchangeably and nearly always describe the same document.</li><li>A job specification could be considered a more precise job description that details the exact educational degrees, experience, skills, and requirements for a role. In most cases, these terms are used interchangeably and nearly always describe the same document.</li><li>A job specification could be considered a more precise job description that details the exact educational degrees, experience, skills, and requirements for a role. In most cases, these terms are used interchangeably and nearly always describe the same document.</li></ul>', '5000', 132, 1949, 'Human Resource', 1),
(4, 'Inka Sdn Bhd', 'Software Engineer', '2011-03-25', '2020-11-13', 0, 'IT Deparment', '<ul><li>A job specification could be considered a more precise job description that details the exact educational degrees, experience, skills, and requirements for a role. In most cases, these terms are used interchangeably and nearly always describe the same document.</li><li>A job specification could be considered a more precise job description that details the exact educational degrees, experience, skills, and requirements for a role. In most cases, these terms are used interchangeably and nearly always describe the same document.</li><li>A job specification could be considered a more precise job description that details the exact educational degrees, experience, skills, and requirements for a role. In most cases, these terms are used interchangeably and nearly always describe the same document.</li></ul>', '<ul><li>A job specification could be considered a more precise job description that details the exact educational degrees, experience, skills, and requirements for a role. In most cases, these terms are used interchangeably and nearly always describe the same document.</li><li>A job specification could be considered a more precise job description that details the exact educational degrees, experience, skills, and requirements for a role. In most cases, these terms are used interchangeably and nearly always describe the same document.</li><li>A job specification could be considered a more precise job description that details the exact educational degrees, experience, skills, and requirements for a role. In most cases, these terms are used interchangeably and nearly always describe the same document.</li></ul>', '4000', 132, 1941, 'Engineering', 1),
(5, 'Key and Larsen Trading', 'Sequi in voluptatem ', '2010-02-02', '2012-05-04', 0, 'Laboriosam quisquam', 'Job description Job description Job description Job description Job description Job description&nbsp;', '<ul><li>Aspernatur debitis r.</li><li>Aspernatur debitis r.</li><li>Aspernatur debitis r.</li><li>Aspernatur debitis r.</li></ul>', 'Dolorem harum porro ', 132, 1944, 'Engineering', 5),
(6, 'Inspirenow Sdn Bhd', 'Software Engineer', '2000-06-08', '0000-00-00', 0, 'IT Department', '<ul><li>A job description is a document intended to provide job applicants with an outline of the main duties and responsibilities of the role for which they are applying. </li><li><span style=\"font-size: 0.875rem;\">The description is usually drawn up by the individual in the organization responsible for overseeing the selection process for the role, often with the help of the company’s HR department and/or an external recruiter.</span></li></ul>', '<ul><li>A job description is a document intended to provide job applicants with an outline of the main duties and responsibilities of the role for which they are applying.</li><li><span style=\"font-size: 0.875rem;\">The description is usually drawn up by the individual in the organization responsible for overseeing the selection process for the role, often with the help of the company’s HR department and/or an external recruiter.</span></li></ul>', '5000', 132, 1949, 'Engineering', 7),
(7, 'InspireNow Sdn Bhd', 'UI/UX Developer', '2017-04-10', '0000-00-00', 0, 'Technical', '<span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span>', '<span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span>', '100000', 132, 1949, 'Engineering', 8),
(8, 'INKA Creative Sdn Bhd', 'UI/UX Developer', '2017-04-10', '2020-12-31', 0, 'Creative', '<ul><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li></ul>', '<ol><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li><li><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></li></ol>', '100000', 132, 1948, 'Human Resource', 8),
(9, 'Comas', 'UI/UX Developer', '2019-12-30', '2020-12-04', 0, 'IT', '<p><b>aSDASd</b></p><p><b><br></b></p><p>asdasdasdas</p><ul><li>asdasd</li><li>asdasd</li><li>asdsa</li><li>cczxc<br><b></b></li></ul>', '<p><b>Osalskal</b></p><p>as</p><ol><li><b>asdasdsa</b></li><li><b>asidjneujksadf</b></li><li><b>sadfsadfs<br></b></li></ol>', '44444', 132, 1944, 'Human Resource', 10);

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

DROP TABLE IF EXISTS `favourites`;
CREATE TABLE IF NOT EXISTS `favourites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `type` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'task',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_task_id` (`task_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `favourites`
--

INSERT INTO `favourites` (`id`, `user_id`, `task_id`, `type`, `created_at`, `updated_at`) VALUES
(2, '1', '537ED05F-1619-4B96-A6FF-1B20157E7A42', 'task', '2020-11-10 12:06:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

DROP TABLE IF EXISTS `holidays`;
CREATE TABLE IF NOT EXISTS `holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `industry`
--

DROP TABLE IF EXISTS `industry`;
CREATE TABLE IF NOT EXISTS `industry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) NOT NULL,
  `sortby` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry`
--

INSERT INTO `industry` (`id`, `images`, `title`, `description`, `parent`, `sortby`, `status`) VALUES
(1, '', 'Accounting/Finance', '', 0, 0, 1),
(2, '', 'Admin/Human Resources', '', 0, 0, 1),
(3, '', 'Sales/Marketing', '', 0, 0, 1),
(4, '', 'Arts/Media/Communications', '', 0, 0, 1),
(5, '', 'Services', '', 0, 0, 1),
(6, '', 'Hotel/Restaurant', '', 0, 0, 1),
(7, '', 'Education/Training', '', 0, 0, 1),
(8, '', 'Computer/Information Technology', '', 0, 0, 1),
(9, '', 'Engineering', '', 0, 0, 1),
(10, '', 'Manufacturing', '', 0, 0, 1),
(11, '', 'Building/Construction', '', 0, 0, 1),
(12, '', 'Sciences', '', 0, 0, 1),
(13, '', 'Sciences', '', 0, 0, 1),
(14, '', 'Others', '', 0, 0, 1),
(15, '', 'Audit & Taxation', '', 1, 0, 1),
(16, '', 'General/Cost Accounting', '', 1, 0, 1),
(17, '', 'Corporate Finance/Investment', '', 1, 0, 1),
(18, '', 'Banking/Financial', '', 1, 0, 1),
(19, '', 'Clerical/General Admin', '', 2, 0, 1),
(20, '', 'Human Resources', '', 2, 0, 1),
(21, '', 'Secretarial/Executive Personal Assistant', '', 2, 0, 1),
(22, '', 'Top Management', '', 2, 0, 1),
(23, '', 'Marketing/Business Dev', '', 3, 0, 1),
(24, '', 'Sales - Corporate', '', 3, 0, 1),
(25, '', 'Sales - Eng/Tech/IT', '', 3, 0, 1),
(26, '', 'Sales - Financial Services', '', 3, 0, 1),
(27, '', 'Retail Sales', '', 3, 0, 1),
(28, '', 'Merchandising', '', 3, 0, 1),
(29, '', 'Telesales/Telemarketing', '', 3, 0, 1),
(30, '', 'E-commerce', '', 3, 0, 1),
(31, '', 'Digital Marketing', '', 3, 0, 1),
(32, '', 'Advertising', '', 4, 0, 1),
(33, '', 'Arts/Creative Design', '', 4, 0, 1),
(34, '', 'Entertainment', '', 4, 0, 1),
(35, '', 'Public Relations', '', 4, 0, 1),
(36, '', 'Personal Care', '', 5, 0, 1),
(37, '', 'Armed Forces', '', 5, 0, 1),
(38, '', 'Social Services', '', 5, 0, 1),
(39, '', 'Customer Service', '', 5, 0, 1),
(40, '', 'Lawyer/Legal Asst', '', 5, 0, 1),
(41, '', 'Logistics/Supply Chain', '', 5, 0, 1),
(42, '', 'Tech & Helpdesk Support', '', 5, 0, 1),
(43, '', 'Food/Beverage/Restaurant', '', 6, 0, 1),
(44, '', 'Hotel/Tourism', '', 6, 0, 1),
(45, '', 'Education', '', 7, 0, 1),
(46, '', 'Training & Development', '', 7, 0, 1),
(47, '', 'IT-Software', '', 8, 0, 1),
(48, '', 'IT-Hardware', '', 8, 0, 1),
(49, '', 'IT-Network/Sys/DB Admin', '', 8, 0, 1),
(50, '', 'Chemical Engineering', '', 9, 0, 1),
(51, '', 'Electronics', '', 9, 0, 1),
(52, '', 'Electrical', '', 9, 0, 1),
(53, '', 'Other Engineering', '', 9, 0, 1),
(54, '', 'Environmental', '', 9, 0, 1),
(55, '', 'Oil/Gas', '', 9, 0, 1),
(56, '', 'Mechanical', '', 9, 0, 1),
(57, '', 'Industrial Engineering', '', 9, 0, 1),
(58, '', 'Maintenance', '', 10, 0, 1),
(59, '', 'Purchasing/Material Mgmt', '', 10, 0, 1),
(60, '', 'Manufacturing', '', 10, 0, 1),
(61, '', 'Process Control', '', 10, 0, 1),
(62, '', 'Quality Assurance', '', 10, 0, 1),
(63, '', 'Property/Real Estate', '', 11, 0, 1),
(64, '', 'Architect/Interior Design', '', 11, 0, 1),
(65, '', 'Civil/Construction', '', 11, 0, 1),
(66, '', 'Quantity Surveying', '', 11, 0, 1),
(67, '', 'Agriculture', '', 12, 0, 1),
(68, '', 'Actuarial/Statistics', '', 12, 0, 1),
(69, '', 'Food Tech/Nutritionist', '', 12, 0, 1),
(70, '', 'Geology/Geophysics', '', 12, 0, 1),
(71, '', 'Aviation', '', 12, 0, 1),
(72, '', 'Biotechnology', '', 12, 0, 1),
(73, '', 'Chemistry', '', 12, 0, 1),
(74, '', 'Science & Technology', '', 12, 0, 1),
(75, '', 'Biomedical', '', 12, 0, 1),
(76, '', 'Practitioner/Medical Asst', '', 13, 0, 1),
(77, '', 'Pharmacy', '', 13, 0, 1),
(78, '', 'Diagnosis/Others', '', 13, 0, 1),
(79, '', 'Journalist/Editors', '', 14, 0, 1),
(80, '', 'General Work', '', 14, 0, 1),
(81, '', 'Publishing', '', 14, 0, 1),
(82, '', 'Others', '', 14, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `number` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `sub_category` int(11) NOT NULL,
  `title` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `requirements` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `experience_level` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `skills` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `location` mediumtext COLLATE utf8_unicode_ci,
  `salary_range_min` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salary_range_max` decimal(10,2) NOT NULL DEFAULT '0.00',
  `benefits` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `status` enum('draft','published','completed','cancelled') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `assigned` timestamp NULL DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `closed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `jobs_user_id_index` (`user_id`) USING BTREE,
  KEY `jobs_category_index` (`category`) USING BTREE,
  KEY `jobs_sub_category_index` (`sub_category`) USING BTREE,
  KEY `jobs_experience_level_index` (`experience_level`) USING BTREE,
  KEY `jobs_country_index` (`country`) USING BTREE,
  KEY `jobs_state_index` (`state`) USING BTREE,
  KEY `jobs_status_index` (`status`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `number`, `user_id`, `category`, `sub_category`, `title`, `slug`, `description`, `requirements`, `experience_level`, `skills`, `country`, `state`, `location`, `salary_range_min`, `salary_range_max`, `benefits`, `status`, `assigned`, `assigned_to`, `closed_at`, `created_at`, `updated_at`) VALUES
('9B07069A-7C92-424F-9F42-6BEE7EA0A172', 'J20103', '11', 2, 16, 'Head Chef', 'head-chef', 'Takes charge of all the cooking', 'Requirement to cook as a head chef', 'entry', '', 132, 1944, '17, Jalan Flora 3F/13, Bandar Rimbayu, 42500 Telok Panglima Garang, Selangor, Malaysia', '3500.00', '4200.00', '[\"Parking\",\"Claimable expenses\",\"Dental\"]', 'cancelled', NULL, NULL, '2020-11-16 16:00:00', '2020-11-17 05:56:50', '2020-11-17 15:52:03'),
('769D0CD3-2F98-43E4-AAD2-D36C74DA3679', 'J20101', '11', 1, 11, 'Vacuum Cleaner', 'vacuum-cleaner', 'Clean the house', 'Clean la\r\n', 'entry', '', 132, 1944, '66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia', '3500.00', '4200.00', '[\"Parking\",\"Claimable expenses\"]', 'cancelled', NULL, NULL, '2020-11-19 16:00:00', '2020-11-16 17:29:36', '2020-11-18 07:13:37'),
('B3BEC735-FC88-473B-8D84-1E04A4CB7AF0', 'J20102', '11', 3, 21, 'Babysitter', 'babysitter', 'asdasjldkasmd', 'asdsadasdsad', 'intern', '', 132, 1944, '66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia', '3500.00', '4200.00', '[]', 'cancelled', NULL, NULL, '2020-11-17 16:00:00', '2020-11-17 05:08:57', '2020-11-19 03:01:31'),
('8A72888F-4FBF-4D14-8E3F-A9E95711E581', 'J20104', '11', 3, 21, 'Nanny', 'nanny', 'hot n silky', 'lean ', 'entry', '', 132, 1944, '66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia', '3500.00', '4200.00', '[]', 'cancelled', NULL, NULL, '2020-11-19 16:00:00', '2020-11-18 07:17:42', '2020-11-23 04:23:43');

-- --------------------------------------------------------

--
-- Table structure for table `job_questions`
--

DROP TABLE IF EXISTS `job_questions`;
CREATE TABLE IF NOT EXISTS `job_questions` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `job_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `job_questions_id_job_id_unique` (`id`,`job_id`),
  KEY `job_questions_job_id_index` (`job_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `job_questions`
--

INSERT INTO `job_questions` (`id`, `job_id`, `question`, `created_at`, `updated_at`) VALUES
('CB440D0F-2AC6-4FE6-A6F2-ACF22858238D', '5D3FAD95-4DAA-4B73-B4F2-C95C7A5AE1C4', 'Can you wash?', '2020-11-16 15:02:50', NULL),
('9087FA3F-4983-4C33-9B1C-E4B67E532104', 'B3BEC735-FC88-473B-8D84-1E04A4CB7AF0', '111', '2020-11-17 05:08:57', NULL),
('09D84132-DD61-483F-969C-4E6F84FC6A5B', 'B3BEC735-FC88-473B-8D84-1E04A4CB7AF0', '222', '2020-11-17 05:08:57', NULL),
('8EFDFEDF-24EE-461C-A57C-FB03F4FAE57A', '9B07069A-7C92-424F-9F42-6BEE7EA0A172', 'Question 1', '2020-11-17 05:56:50', NULL),
('F15E4583-9AF8-4E20-9323-A20E3A3E977C', '9B07069A-7C92-424F-9F42-6BEE7EA0A172', 'Question 2', '2020-11-17 05:56:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `job_shortlisted`
--

DROP TABLE IF EXISTS `job_shortlisted`;
CREATE TABLE IF NOT EXISTS `job_shortlisted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `job_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `job_id` (`job_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `job_shortlisted`
--

INSERT INTO `job_shortlisted` (`id`, `user_id`, `job_id`, `created_at`, `updated_at`) VALUES
(5, 10, '769D0CD3-2F98-43E4-AAD2-D36C74DA3679', '2020-11-17 04:54:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fb_uid` text,
  `google_id` text NOT NULL,
  `fb_email` text,
  `email` text,
  `password` text,
  `photo` text,
  `salutation` text,
  `firstname` text,
  `lastname` text,
  `contact` text,
  `address1` text,
  `address2` text,
  `zip` text,
  `city` text,
  `country` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `nric` text,
  `gender` text,
  `dob` text,
  `nationality` text,
  `contact_number_prefix` text,
  `contact_number` text,
  `mobile_number_prefix` text,
  `mobile_number` text,
  `profile_pic` text,
  `insta_id` text,
  `other_social_media` text,
  `emergency_contact_name` text,
  `emergency_contact_number_prefix` text,
  `emergency_contact_number` text,
  `emergency_contact_relation` text,
  `contact_method` varchar(32) NOT NULL,
  `about` text,
  `skills` text NOT NULL,
  `language` text,
  `dashboard_widget` varchar(128) DEFAULT '2,4,5,6',
  `completion` tinyint(4) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT '0' COMMENT '0- Active 99- Suspended',
  `type` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `fb_uid`, `google_id`, `fb_email`, `email`, `password`, `photo`, `salutation`, `firstname`, `lastname`, `contact`, `address1`, `address2`, `zip`, `city`, `country`, `state`, `nric`, `gender`, `dob`, `nationality`, `contact_number_prefix`, `contact_number`, `mobile_number_prefix`, `mobile_number`, `profile_pic`, `insta_id`, `other_social_media`, `emergency_contact_name`, `emergency_contact_number_prefix`, `emergency_contact_number`, `emergency_contact_relation`, `contact_method`, `about`, `skills`, `language`, `dashboard_widget`, `completion`, `date_created`, `status`, `type`) VALUES
(1, NULL, '', NULL, 'shah@inspirenow.com.my', '$2a$07$np65plSrOACM8PPpL57zcOmDOOmbjunuWgz.Ot5UZeARX4tubWoqG', 'files/profile/1/1-intro-photo-final_5faa65dabcefc.jpg', NULL, 'Shah', 'Ramly', NULL, '', NULL, NULL, '76497', 132, 1949, '830629075433', NULL, '1983-06-29', NULL, '03', '22422140', '018', '33949939', NULL, NULL, NULL, 'Syafiq', '012', '339499494', 'Friend', '', 'I am a person who is positive about every aspect of life. There are many things I like to do, to see, and to experience. I like to read, I like to write; I like to think, I like to dream; I like to talk, I like to listen. I like to see the sunrise in the morning, I like to see the moonlight at night; I like to feel the music flowing on my face, I like to smell the wind coming from the ocean. I like to look at the clouds in the sky with a blank mind, I like to do thought experiment when I cannot sleep in the middle of the night. I like flowers in spring, rain in summer, leaves in autumn, and snow in winter. I like to sleep early, I like to get up late; I like to be alone, I like to be surrounded by people. I like country’s peace, I like metropolis’ noise; I like the beautiful west lake in Hangzhou, I like the flat cornfield in Champaign. I like delicious food and comfortable shoes; I like good books and romantic movies. I like the land and the nature, I like people. And, I like to laugh.', '24,26,37,42,46,57,75', 'English,Malay', '2,4,5,6', 70, '2020-11-10 17:28:23', 0, 0),
(2, NULL, '', NULL, 'syafiq@inspirenow.com.my', '$2a$07$np65plSrOACM8PPpL57zcOmDOOmbjunuWgz.Ot5UZeARX4tubWoqG', 'files/profile/2/profileIcon_g7udwwvzxuc51_5faa6482ad508.png', NULL, 'Syafiq', 'Syazre', NULL, '', NULL, NULL, '76476', 132, 1944, '890714014471', NULL, '', NULL, '03', '+602323245454', '014', '+60343485834', NULL, NULL, NULL, 'John Doe', '', '', 'Friend', '', 'I am ambitious and driven. I thrive on challenge and constantly set goals for myself, so I have something to strive toward. I\'m not comfortable with settling, and I\'m always looking for an opportunity to do better and achieve greatness.', '1,15,19,23', 'Chinese,Tamil', '2,4,5,6', 95, '2020-11-10 17:28:59', 0, 0),
(3, '10158710870049321', '', NULL, 'skaterzee@gmail.com', NULL, 'files/3/profile/fbProfilePhoto.jpg', NULL, 'Farid', 'Edil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '24,1', NULL, '2,4,5,6', 60, '2020-11-10 21:26:38', 1, 0),
(4, NULL, '107338009771497623878', NULL, 'kevinmvp@gmail.com', NULL, 'files/4/profile/googleProfilePhoto.jpg', NULL, 'Kevin', 'Chew', NULL, 'askdmaslkd', NULL, NULL, '76466', 132, 1944, '840630145493', NULL, '1984-06-30', 'malaysian', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', '', '208,215,226', 'English', '2,4,5,6', 70, '2020-11-11 02:30:25', 1, 0),
(5, NULL, '115577827226804288990', NULL, 'abdulsalam.masoud@gmail.com', NULL, 'files/5/profile/googleProfilePhoto.jpg', NULL, 'Abdulsalam', 'Masoud', NULL, 'Permai Putera Apartment, Jalan 13D, Taman Dato Ahmad Razali', NULL, NULL, '76411', 132, 1944, '830125', 'male', '1983-01-25', 'other', '03', '4739146281', '012', '157520784', NULL, NULL, NULL, 'Wylie Klein', '146449445', '', 'Omnis harum explicab', 'telegram', 'Anim dicta natus sint eaque quia ut omnis incidunt, molestiae aliquam quia et veritatis ducimus, corporis commodo ut eos, ullamco quisquam rerum possimus, et quos sint eum ex doloribus praesentium qui.', '1,3,9,11', 'English', '2,4,5,6', 100, '2020-11-11 02:30:30', 1, 0),
(6, '130247798863663', '', NULL, 'ask@byte2c.com', NULL, 'files/6/profile/fbProfilePhoto.jpg', NULL, 'Benny', 'Wong', NULL, '', NULL, NULL, '76560', 132, 1944, '840630145493', NULL, '1984-06-30', NULL, '', '', '', '', NULL, NULL, NULL, '', '333', '22321112321', '', '', '', '161,215', 'English', '2,4,5,6', 100, '2020-11-11 11:36:04', 1, 0),
(7, NULL, '', NULL, 'shah@inka.my', '$2a$07$np65plSrOACM8PPpL57zcOmDOOmbjunuWgz.Ot5UZeARX4tubWoqG', 'files/profile/7/1-intro-photo-final_5facb3d849cc4.jpg', NULL, 'Shah', 'Ramly', NULL, '4-8-3 Putra one apartment, Jalan Persiaran Putra', NULL, NULL, '76422', 132, 1939, '830629075433', NULL, '1983-06-29', NULL, '03', '22422140', '012', '884838828', NULL, NULL, NULL, 'Syafiq', '014', '3394993929', 'Friend', '', 'I am a person who is positive about every aspect of life. There are many things I like to do, to see, and to experience. I like to read, I like to write; I like to think, I like to dream; I like to talk, I like to listen. I like to see the sunrise in the morning, I like to see the moonlight at night; I like to feel the music flowing on my face, I like to smell the wind coming from the ocean. I like to look at the clouds in the sky with a blank mind, I like to do thought experiment when I cannot sleep in the middle of the night. I like flowers in spring, rain in summer, leaves in autumn, and snow in winter. I like to sleep early, I like to get up late; I like to be alone, I like to be surrounded by people. I like country’s peace, I like metropolis’ noise; I like the beautiful west lake in Hangzhou, I like the flat cornfield in Champaign. I like delicious food and comfortable shoes; I like good books and romantic movies. I like the land and the nature, I like people. And, I like to laugh.', '1,7,62,83', 'Malay,English', '2,4,5,6', 100, '2020-11-12 11:42:03', 0, 0),
(8, NULL, '', NULL, 'syafiq@inka.my', '$2a$07$np65plSrOACM8PPpL57zcOmDOOmbjunuWgz.Ot5UZeARX4tubWoqG', 'files/profile/8/profileIcon_g7udwwvzxuc51_5facb44dbf0ac.png', NULL, 'Mohamad', 'Syafiq', NULL, 'No 45 Jalan Kerinchi', NULL, NULL, '76491', 132, 1950, '930714016671', NULL, '1993-07-14', NULL, '03', '22422140', '012', '73232232', NULL, NULL, NULL, 'John Doe', '012', '78787878', 'Friend', '', 'I am ambitious and driven. I thrive on challenge and constantly set goals for myself, so I have something to strive toward. I\'m not comfortable with settling, and I\'m always looking for an opportunity to do better and achieve greatness.', '1,9,10,11,62,83,208,75', 'Malay,English,Tamil,Chinese', '1,4,5,6', 100, '2020-11-12 11:42:03', 0, 0),
(9, NULL, '', NULL, 'ken@inspirenow.com.my', '$2a$07$np65plSrOACM8PPpL57zcOmDOOmbjunuWgz.Ot5UZeARX4tubWoqG', 'files/profile/9/img_323861_5facb62d15383.png', NULL, 'Ken', 'Tai', NULL, 'Bangsar South', NULL, NULL, '76506', 132, 1937, '770712145525', 'male', '1977-07-12', 'malaysian', '', '', '012', '3034346', NULL, NULL, NULL, '', '', '', '', '', 'test', '24,28,34,38,39,43,44,46,48,49,52,54,56,60,66,161,202,226', 'English', '2,4,5,6', 100, '2020-11-12 12:10:16', 0, 0),
(10, NULL, '', NULL, 'kevin@byte2c.com', '$2a$07$np65plSrOACM8PPpL57zcOmDOOmbjunuWgz.Ot5UZeARX4tubWoqG', 'assets/img/default-avatar.png', NULL, 'Kevin', 'Test', NULL, '', NULL, NULL, '76454', 132, 1947, '840630145493', NULL, '1984-06-30', NULL, '', '+601554212321', '', '+60156677888', NULL, NULL, NULL, '', '', '+60421312321', '', '', 'aaaabbbccc', '24,29,244', 'Amharic', '4,7,8', 90, '2020-11-12 12:14:38', 0, 0),
(11, NULL, '', NULL, 'hr@byte2c.com', '$2a$07$np65plSrOACM8PPpL57zcO.e9ubd6kyHkmSW7DRIPn9sgEUgwpvmq', 'files/profile/11/WhatsApp Image 2020-11-20 at 16_5fbdd4d105c62.jpeg', NULL, 'Bytetoc', 'Computing', NULL, 'asdasd', 'sdfsa', '42133', 'Shah Alam', 132, 1944, '1283012983901283', NULL, NULL, NULL, '02', '+6010293012', '023', '+60102391023921', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '5,8,9,11', 55, '2020-11-12 13:44:13', 1, 1),
(12, NULL, '', NULL, 'asa@asda.com', NULL, 'assets/img/default-avatar.png', NULL, 'kevin', 'che', NULL, NULL, NULL, NULL, '76465', 132, 1948, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '2,4,5,6', 0, '2020-11-23 16:23:40', 0, 0),
(13, NULL, '', NULL, 'hi@rimbaride.com', NULL, 'assets/img/default-avatar.png', NULL, 'Kevin', 'Test Again', NULL, NULL, NULL, NULL, '76509', 132, 1947, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '2,4,5,6', 0, '2020-11-23 16:24:08', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `member_docs`
--

DROP TABLE IF EXISTS `member_docs`;
CREATE TABLE IF NOT EXISTS `member_docs` (
  `member_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `src` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `member_docs`
--

INSERT INTO `member_docs` (`member_id`, `type`, `src`) VALUES
(1, 'resume', 'files/resume/1/ImpactReport_Arts&Culture_5faa6839a9b7e.pdf'),
(1, 'cover_letter', 'files/cover_letter/1/ImpactReport_Community&Health_5faa683e919d5.pdf'),
(1, 'cert', 'files/cert/1/ImpactReport_Education_5faa68415dac2.pdf'),
(7, 'resume', 'files/resume/7/CEOsReview_5facb40b835bb.pdf'),
(7, 'cover_letter', 'files/cover_letter/7/CEOsReview_5facb415f0f0e.pdf'),
(7, 'cert', 'files/cert/7/CEOsReview_5facb41893a3d.pdf'),
(8, 'resume', 'files/resume/8/districts_of_malaysia_5facb46668022.pdf'),
(8, 'cover_letter', 'files/cover_letter/8/make_time_pay_task_flow_horizontal_5facb47005a6b.pdf'),
(8, 'cert', 'files/cert/8/make_time_pay_task_flow_vertical_20200604_5facb479291c1.pdf'),
(2, 'cover_letter', 'files/cover_letter/2/Make Time Pay (MTP)_5faa64b520b2e.pdf'),
(2, 'cert', 'files/cert/2/make_time_pay_task_flow_vertical_5faa64bb6d172.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `member_logs`
--

DROP TABLE IF EXISTS `member_logs`;
CREATE TABLE IF NOT EXISTS `member_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `ip_address` text NOT NULL,
  `user_agent` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=631 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_logs`
--

INSERT INTO `member_logs` (`id`, `member_id`, `date`, `ip_address`, `user_agent`) VALUES
(1, 1, '2020-11-10 17:31:30', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(2, 2, '2020-11-10 17:31:38', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(3, 2, '2020-11-10 17:33:32', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(4, 2, '2020-11-10 17:35:10', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(5, 2, '2020-11-10 17:36:27', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(6, 2, '2020-11-10 17:37:12', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(7, 1, '2020-11-10 17:37:51', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(8, 1, '2020-11-10 17:58:36', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(9, 2, '2020-11-10 17:59:36', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(10, 2, '2020-11-10 18:00:34', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(11, 2, '2020-11-10 18:14:49', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(12, 2, '2020-11-10 18:14:54', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(13, 2, '2020-11-10 18:16:55', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(14, 2, '2020-11-10 18:19:15', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(15, 1, '2020-11-10 18:19:16', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(16, 2, '2020-11-10 18:24:23', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(17, 2, '2020-11-10 18:24:53', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(18, 1, '2020-11-10 18:30:48', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(19, 2, '2020-11-10 18:56:34', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(20, 2, '2020-11-10 19:48:44', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(21, 1, '2020-11-10 19:50:02', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(22, 1, '2020-11-10 20:00:59', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(23, 1, '2020-11-10 20:02:33', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(24, 2, '2020-11-10 20:18:15', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(25, 1, '2020-11-10 20:18:17', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(26, 1, '2020-11-10 20:18:22', '175.141.97.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(27, 2, '2020-11-10 21:19:10', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(28, 3, '2020-11-10 21:26:51', '115.133.102.159', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(29, 4, '2020-11-11 02:30:31', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(30, 5, '2020-11-11 02:30:52', '210.195.150.183', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(31, 4, '2020-11-11 02:31:46', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(32, 4, '2020-11-11 02:32:11', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(33, 4, '2020-11-11 02:32:36', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(34, 4, '2020-11-11 02:32:51', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(35, 4, '2020-11-11 02:34:01', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(36, 4, '2020-11-11 02:35:03', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(37, 5, '2020-11-11 02:35:20', '210.195.150.183', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(38, 2, '2020-11-11 03:23:03', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(39, 4, '2020-11-11 11:35:07', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(40, 4, '2020-11-11 11:35:08', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(41, 6, '2020-11-11 11:36:12', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(42, 6, '2020-11-11 11:36:55', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(43, 2, '2020-11-11 12:13:05', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(44, 4, '2020-11-11 15:58:36', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(45, 4, '2020-11-11 15:58:36', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(46, 6, '2020-11-11 15:58:47', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(47, 6, '2020-11-11 15:58:47', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(48, 4, '2020-11-11 16:04:42', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(49, 6, '2020-11-11 17:02:02', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(50, 4, '2020-11-12 01:10:54', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(51, 4, '2020-11-12 01:10:54', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(52, 6, '2020-11-12 01:11:05', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(53, 6, '2020-11-12 01:11:05', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(54, 6, '2020-11-12 01:14:33', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(55, 6, '2020-11-12 01:14:49', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(56, 6, '2020-11-12 01:15:02', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(57, 6, '2020-11-12 01:15:12', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(58, 6, '2020-11-12 01:15:23', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(59, 6, '2020-11-12 01:18:17', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(60, 6, '2020-11-12 01:18:32', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(61, 6, '2020-11-12 01:26:06', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(62, 6, '2020-11-12 01:40:49', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(63, 6, '2020-11-12 01:53:33', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(64, 4, '2020-11-12 01:54:44', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(65, 4, '2020-11-12 01:54:44', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(66, 4, '2020-11-12 01:55:30', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(67, 4, '2020-11-12 01:56:44', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(68, 4, '2020-11-12 01:58:02', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(69, 4, '2020-11-12 02:02:21', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(70, 4, '2020-11-12 02:02:21', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(71, 4, '2020-11-12 02:02:24', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(72, 4, '2020-11-12 02:02:25', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(73, 6, '2020-11-12 02:02:31', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(74, 6, '2020-11-12 02:02:40', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(75, 6, '2020-11-12 02:02:49', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(76, 6, '2020-11-12 02:03:06', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(77, 4, '2020-11-12 02:04:05', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(78, 4, '2020-11-12 02:04:11', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(79, 4, '2020-11-12 02:04:12', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(80, 6, '2020-11-12 02:04:22', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(81, 6, '2020-11-12 02:04:23', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(82, 4, '2020-11-12 02:04:35', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(83, 4, '2020-11-12 02:04:45', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(84, 6, '2020-11-12 02:09:20', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(85, 4, '2020-11-12 02:14:46', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(86, 6, '2020-11-12 02:28:21', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(87, 8, '2020-11-12 11:56:44', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(88, 7, '2020-11-12 11:57:01', '175.136.53.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(89, 7, '2020-11-12 11:57:52', '175.136.53.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(90, 8, '2020-11-12 11:58:15', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(91, 7, '2020-11-12 11:58:24', '175.136.53.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(92, 7, '2020-11-12 11:59:22', '175.136.53.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(93, 7, '2020-11-12 11:59:40', '175.136.53.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(94, 8, '2020-11-12 11:59:47', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(95, 8, '2020-11-12 12:00:27', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(96, 7, '2020-11-12 12:00:33', '175.136.53.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(97, 8, '2020-11-12 12:00:46', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(98, 8, '2020-11-12 12:01:05', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(99, 8, '2020-11-12 12:01:24', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(100, 8, '2020-11-12 12:01:40', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(101, 8, '2020-11-12 12:02:00', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(102, 8, '2020-11-12 12:02:16', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(103, 8, '2020-11-12 12:02:25', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(104, 8, '2020-11-12 12:02:36', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(105, 8, '2020-11-12 12:03:09', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(106, 7, '2020-11-12 12:07:23', '175.136.53.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(107, 7, '2020-11-12 12:08:21', '175.136.53.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(108, 8, '2020-11-12 12:09:30', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(109, 8, '2020-11-12 12:09:48', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(110, 8, '2020-11-12 12:10:39', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(111, 9, '2020-11-12 12:11:03', '14.192.208.156', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(112, 9, '2020-11-12 12:11:26', '14.192.208.156', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(113, 9, '2020-11-12 12:12:41', '14.192.208.156', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(114, 7, '2020-11-12 12:12:45', '175.136.53.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(115, 7, '2020-11-12 12:12:51', '175.136.53.3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(116, 9, '2020-11-12 12:13:09', '14.192.208.156', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(117, 9, '2020-11-12 12:13:32', '14.192.208.156', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'),
(118, 8, '2020-11-12 12:13:42', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(119, 8, '2020-11-12 12:14:35', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(120, 8, '2020-11-12 12:19:53', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(121, 10, '2020-11-12 12:27:13', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(122, 10, '2020-11-12 12:27:46', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(123, 10, '2020-11-12 12:28:00', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(124, 10, '2020-11-12 12:30:18', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(125, 10, '2020-11-12 12:30:26', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(126, 10, '2020-11-12 12:30:34', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(127, 10, '2020-11-12 12:32:33', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(128, 8, '2020-11-12 12:33:17', '210.195.13.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(129, 10, '2020-11-12 12:33:37', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(130, 10, '2020-11-12 12:33:38', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(131, 10, '2020-11-12 12:34:26', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(132, 10, '2020-11-12 12:35:53', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(133, 10, '2020-11-12 12:36:05', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(134, 10, '2020-11-12 12:36:28', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(135, 10, '2020-11-12 12:36:28', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(136, 10, '2020-11-12 12:36:48', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(137, 10, '2020-11-12 12:36:49', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(138, 4, '2020-11-12 12:58:12', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(139, 4, '2020-11-12 12:58:12', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(140, 9, '2020-11-12 12:58:46', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(141, 9, '2020-11-12 12:58:57', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(142, 9, '2020-11-12 13:02:31', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(143, 9, '2020-11-12 13:02:31', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(144, 9, '2020-11-12 13:26:28', '14.192.213.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(145, 6, '2020-11-12 13:42:37', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(146, 6, '2020-11-12 13:42:37', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(147, 6, '2020-11-12 13:42:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(148, 6, '2020-11-12 13:43:12', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(149, 6, '2020-11-12 13:43:29', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(150, 11, '2020-11-12 13:45:35', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(151, 11, '2020-11-12 13:47:16', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(152, 11, '2020-11-12 13:47:39', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(153, 11, '2020-11-12 13:50:19', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(154, 11, '2020-11-12 13:50:19', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(155, 10, '2020-11-12 13:50:44', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(156, 10, '2020-11-12 13:50:56', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(157, 10, '2020-11-12 13:51:18', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(158, 10, '2020-11-12 13:51:24', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(159, 10, '2020-11-12 13:51:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(160, 11, '2020-11-12 13:54:03', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(161, 11, '2020-11-12 13:54:24', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(162, 10, '2020-11-12 14:08:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(163, 10, '2020-11-12 14:09:13', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(164, 11, '2020-11-12 14:34:44', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(165, 10, '2020-11-12 14:38:34', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(166, 10, '2020-11-12 14:38:41', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(167, 10, '2020-11-12 14:39:03', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(168, 10, '2020-11-12 14:39:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(169, 10, '2020-11-12 14:39:40', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(170, 10, '2020-11-12 14:39:40', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(171, 10, '2020-11-12 14:40:37', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(172, 10, '2020-11-12 14:44:33', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(173, 10, '2020-11-12 14:45:15', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(174, 10, '2020-11-12 14:45:42', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(175, 2, '2020-11-12 14:46:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(176, 2, '2020-11-12 14:47:55', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(177, 2, '2020-11-12 14:48:11', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(178, 2, '2020-11-12 15:00:19', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(179, 10, '2020-11-12 15:00:49', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'),
(180, 2, '2020-11-12 16:17:21', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(181, 2, '2020-11-12 16:49:09', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(182, 2, '2020-11-12 21:17:51', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(183, 10, '2020-11-12 21:18:20', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(184, 2, '2020-11-12 21:44:13', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(185, 2, '2020-11-12 21:52:03', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(186, 2, '2020-11-12 21:58:46', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(187, 11, '2020-11-12 22:07:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(188, 11, '2020-11-13 00:13:47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(189, 11, '2020-11-13 00:13:49', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(190, 10, '2020-11-13 00:14:09', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(191, 10, '2020-11-13 00:14:18', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(192, 10, '2020-11-13 00:14:44', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(193, 10, '2020-11-13 00:23:37', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(194, 2, '2020-11-13 00:40:14', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(195, 11, '2020-11-13 00:42:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(196, 11, '2020-11-13 00:47:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(197, 2, '2020-11-13 00:48:09', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(198, 10, '2020-11-13 13:40:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'),
(199, 11, '2020-11-13 13:43:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(200, 11, '2020-11-13 13:44:21', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(201, 10, '2020-11-14 01:33:41', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(202, 10, '2020-11-14 01:38:02', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(203, 10, '2020-11-14 01:38:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(204, 10, '2020-11-14 01:39:09', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(205, 10, '2020-11-14 01:39:40', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(206, 10, '2020-11-14 01:41:22', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(207, 10, '2020-11-14 01:42:44', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(208, 10, '2020-11-14 01:43:00', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(209, 10, '2020-11-14 01:43:10', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(210, 10, '2020-11-14 01:43:17', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(211, 10, '2020-11-14 01:43:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(212, 10, '2020-11-14 01:43:31', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(213, 10, '2020-11-14 01:43:46', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(214, 10, '2020-11-14 01:43:46', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(215, 10, '2020-11-14 01:43:47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(216, 10, '2020-11-14 01:43:58', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(217, 10, '2020-11-14 01:44:04', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(218, 10, '2020-11-14 01:44:19', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(219, 10, '2020-11-14 01:44:22', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(220, 10, '2020-11-14 01:44:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(221, 10, '2020-11-14 01:45:06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(222, 10, '2020-11-14 01:46:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(223, 10, '2020-11-14 01:48:04', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(224, 10, '2020-11-14 01:49:36', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(225, 10, '2020-11-14 01:49:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(226, 10, '2020-11-14 01:50:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(227, 10, '2020-11-14 01:50:18', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(228, 10, '2020-11-14 01:50:39', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(229, 10, '2020-11-14 01:50:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(230, 10, '2020-11-14 01:51:40', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(231, 10, '2020-11-14 01:52:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(232, 10, '2020-11-14 01:52:36', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(233, 10, '2020-11-14 01:52:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(234, 10, '2020-11-14 04:44:35', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(235, 10, '2020-11-14 04:44:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(236, 10, '2020-11-14 04:46:31', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(237, 10, '2020-11-14 04:46:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(238, 10, '2020-11-14 04:50:53', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(239, 10, '2020-11-14 04:52:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(240, 10, '2020-11-14 04:53:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(241, 10, '2020-11-14 04:54:00', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(242, 10, '2020-11-14 04:54:16', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(243, 10, '2020-11-14 04:54:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(244, 10, '2020-11-14 04:55:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(245, 10, '2020-11-14 04:55:08', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(246, 10, '2020-11-14 04:56:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(247, 10, '2020-11-14 04:57:40', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(248, 10, '2020-11-14 04:58:00', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(249, 10, '2020-11-14 04:58:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(250, 10, '2020-11-14 05:01:07', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(251, 10, '2020-11-14 05:01:24', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(252, 10, '2020-11-14 05:01:41', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(253, 10, '2020-11-14 05:01:58', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(254, 10, '2020-11-14 05:04:37', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(255, 10, '2020-11-14 05:08:14', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(256, 10, '2020-11-14 05:08:34', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(257, 10, '2020-11-14 05:08:43', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(258, 10, '2020-11-14 05:08:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(259, 10, '2020-11-14 05:12:15', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(260, 10, '2020-11-14 05:14:16', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(261, 10, '2020-11-14 05:14:22', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(262, 10, '2020-11-14 05:14:43', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(263, 10, '2020-11-14 05:19:38', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36 Edg/86.0.622.68'),
(264, 10, '2020-11-15 00:05:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(265, 10, '2020-11-15 00:07:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(266, 10, '2020-11-15 00:12:04', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(267, 11, '2020-11-16 01:24:21', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(268, 10, '2020-11-16 02:39:38', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(269, 10, '2020-11-16 02:41:09', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(270, 10, '2020-11-16 02:41:35', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(271, 10, '2020-11-16 04:29:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(272, 10, '2020-11-16 04:30:25', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(273, 10, '2020-11-16 04:30:59', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(274, 11, '2020-11-16 11:14:00', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(275, 10, '2020-11-16 11:34:29', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(276, 10, '2020-11-16 12:00:03', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(277, 10, '2020-11-16 12:08:33', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(278, 10, '2020-11-16 12:29:47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(279, 10, '2020-11-16 12:30:10', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(280, 10, '2020-11-16 12:30:22', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(281, 11, '2020-11-16 22:48:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(282, 11, '2020-11-16 22:49:10', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(283, 11, '2020-11-16 23:02:51', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(284, 11, '2020-11-16 23:03:12', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(285, 11, '2020-11-16 23:04:23', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(286, 11, '2020-11-16 23:05:02', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(287, 11, '2020-11-16 23:05:08', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(288, 11, '2020-11-16 23:05:16', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(289, 11, '2020-11-16 23:05:33', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(290, 11, '2020-11-16 23:05:38', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(291, 11, '2020-11-16 23:05:55', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(292, 11, '2020-11-16 23:06:11', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(293, 11, '2020-11-16 23:07:12', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(294, 11, '2020-11-16 23:07:25', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(295, 11, '2020-11-16 23:07:43', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(296, 11, '2020-11-16 23:08:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(297, 11, '2020-11-16 23:08:59', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(298, 11, '2020-11-16 23:09:36', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(299, 11, '2020-11-16 23:10:47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(300, 11, '2020-11-16 23:11:51', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(301, 11, '2020-11-16 23:12:39', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69');
INSERT INTO `member_logs` (`id`, `member_id`, `date`, `ip_address`, `user_agent`) VALUES
(302, 11, '2020-11-16 23:14:16', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(303, 11, '2020-11-16 23:14:38', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(304, 11, '2020-11-16 23:14:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(305, 11, '2020-11-16 23:14:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(306, 11, '2020-11-16 23:16:53', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(307, 11, '2020-11-16 23:17:06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(308, 11, '2020-11-16 23:17:11', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(309, 11, '2020-11-16 23:17:18', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(310, 11, '2020-11-16 23:17:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(311, 11, '2020-11-16 23:17:37', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(312, 11, '2020-11-16 23:18:31', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(313, 11, '2020-11-16 23:19:20', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(314, 11, '2020-11-16 23:20:02', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(315, 11, '2020-11-17 01:27:51', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(316, 11, '2020-11-17 01:27:56', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(317, 11, '2020-11-17 01:28:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(318, 10, '2020-11-17 03:14:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(319, 11, '2020-11-17 03:22:38', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(320, 11, '2020-11-17 10:39:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(321, 11, '2020-11-17 10:39:51', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(322, 10, '2020-11-17 13:31:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(323, 10, '2020-11-17 13:31:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(324, 10, '2020-11-17 13:40:47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(325, 10, '2020-11-17 14:00:16', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(326, 11, '2020-11-17 23:17:36', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(327, 10, '2020-11-17 23:33:03', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(328, 11, '2020-11-18 00:14:42', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(329, 11, '2020-11-18 00:14:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(330, 11, '2020-11-18 00:38:58', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(331, 11, '2020-11-18 01:07:12', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(332, 11, '2020-11-18 02:09:38', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(333, 11, '2020-11-18 02:31:14', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(334, 10, '2020-11-18 03:05:20', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(335, 10, '2020-11-18 12:12:56', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(336, 10, '2020-11-18 12:13:58', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(337, 10, '2020-11-18 12:18:24', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(338, 11, '2020-11-18 13:29:33', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(339, 11, '2020-11-18 15:13:15', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(340, 10, '2020-11-18 15:14:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(341, 10, '2020-11-18 17:55:31', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(342, 11, '2020-11-18 18:16:46', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(343, 10, '2020-11-18 18:20:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(344, 11, '2020-11-18 18:40:17', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(345, 11, '2020-11-18 18:40:21', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(346, 11, '2020-11-18 18:40:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(347, 11, '2020-11-18 18:40:53', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(348, 11, '2020-11-18 18:40:59', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(349, 11, '2020-11-18 18:41:00', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(350, 11, '2020-11-18 18:42:32', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(351, 11, '2020-11-18 18:43:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(352, 11, '2020-11-18 18:44:06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(353, 11, '2020-11-18 18:44:06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(354, 11, '2020-11-18 18:44:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(355, 11, '2020-11-18 18:44:39', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(356, 11, '2020-11-18 18:45:24', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(357, 11, '2020-11-18 18:46:23', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(358, 11, '2020-11-18 18:47:07', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(359, 11, '2020-11-18 18:49:10', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(360, 11, '2020-11-18 18:50:04', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(361, 11, '2020-11-18 18:50:59', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(362, 11, '2020-11-18 18:51:00', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(363, 11, '2020-11-18 18:51:03', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(364, 11, '2020-11-18 19:00:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(365, 11, '2020-11-18 19:01:00', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(366, 11, '2020-11-18 19:01:02', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(367, 11, '2020-11-18 19:01:09', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(368, 11, '2020-11-18 19:01:10', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(369, 11, '2020-11-18 19:03:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(370, 11, '2020-11-18 19:03:58', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(371, 11, '2020-11-18 19:03:59', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(372, 11, '2020-11-18 19:04:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(373, 11, '2020-11-18 19:04:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(374, 11, '2020-11-18 19:05:36', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(375, 11, '2020-11-18 19:14:29', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(376, 11, '2020-11-18 19:14:31', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(377, 11, '2020-11-18 19:14:32', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(378, 11, '2020-11-18 19:14:58', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(379, 10, '2020-11-19 10:27:35', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(380, 10, '2020-11-19 10:53:09', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(381, 10, '2020-11-19 10:53:55', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(382, 11, '2020-11-19 11:01:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(383, 10, '2020-11-19 16:33:24', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0'),
(384, 11, '2020-11-19 16:33:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69'),
(385, 10, '2020-11-23 10:44:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(386, 10, '2020-11-23 10:59:36', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(387, 10, '2020-11-23 11:00:58', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(388, 10, '2020-11-23 11:08:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(389, 10, '2020-11-23 11:10:15', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(390, 10, '2020-11-23 11:22:14', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(391, 10, '2020-11-23 11:46:49', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(392, 10, '2020-11-23 11:46:58', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(393, 10, '2020-11-23 12:16:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(394, 10, '2020-11-23 12:32:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(395, 10, '2020-11-23 12:41:09', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(396, 10, '2020-11-23 14:03:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(397, 10, '2020-11-23 14:03:53', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(398, 10, '2020-11-23 14:59:55', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(399, 10, '2020-11-23 15:00:06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(400, 10, '2020-11-23 15:00:44', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(401, 10, '2020-11-23 15:04:33', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(402, 10, '2020-11-23 15:05:55', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(403, 10, '2020-11-23 15:06:04', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(404, 10, '2020-11-23 15:30:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(405, 10, '2020-11-23 16:09:16', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(406, 10, '2020-11-23 17:11:04', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(407, 10, '2020-11-24 13:07:06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(408, 11, '2020-11-24 13:08:23', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(409, 10, '2020-11-24 13:11:20', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(410, 11, '2020-11-24 13:42:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(411, 11, '2020-11-24 13:43:13', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(412, 11, '2020-11-24 13:44:06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(413, 10, '2020-11-24 13:45:58', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(414, 11, '2020-11-24 13:46:10', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(415, 11, '2020-11-24 13:46:19', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(416, 11, '2020-11-24 13:52:56', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(417, 11, '2020-11-24 13:53:04', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(418, 10, '2020-11-25 10:55:05', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(419, 10, '2020-11-25 11:49:18', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(420, 11, '2020-11-25 11:51:32', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36'),
(421, 11, '2020-11-25 11:53:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36'),
(422, 11, '2020-11-25 12:08:20', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36'),
(423, 10, '2020-11-25 12:24:47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(424, 10, '2020-11-25 12:25:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(425, 10, '2020-11-25 12:27:16', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(426, 10, '2020-11-25 12:32:09', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(427, 10, '2020-11-25 13:15:18', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36'),
(428, 10, '2020-11-25 13:15:23', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36'),
(429, 10, '2020-11-25 13:29:36', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36'),
(430, 10, '2020-11-26 12:41:14', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(431, 10, '2020-11-26 12:52:46', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(432, 11, '2020-11-26 12:54:35', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(433, 11, '2020-11-26 12:54:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(434, 11, '2020-11-26 12:55:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(435, 10, '2020-11-26 12:55:17', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(436, 10, '2020-11-26 12:55:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(437, 10, '2020-11-26 12:56:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(438, 10, '2020-11-26 12:57:25', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(439, 10, '2020-11-26 13:01:14', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(440, 10, '2020-11-26 13:01:53', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(441, 11, '2020-11-26 13:04:05', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(442, 10, '2020-11-26 13:05:03', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(443, 10, '2020-11-26 13:06:17', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(444, 11, '2020-11-26 13:10:13', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(445, 10, '2020-11-26 16:06:24', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(446, 10, '2020-11-27 12:00:47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(447, 10, '2020-11-27 12:02:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(448, 10, '2020-11-27 12:03:40', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(449, 2, '2020-11-27 12:05:24', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(450, 10, '2020-11-27 12:05:55', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(451, 10, '2020-11-27 12:27:36', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(452, 10, '2020-11-27 12:36:51', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(453, 10, '2020-11-27 12:37:55', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(454, 10, '2020-11-27 12:38:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(455, 10, '2020-11-27 12:38:11', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(456, 10, '2020-11-27 12:38:16', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(457, 10, '2020-11-27 12:38:51', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(458, 10, '2020-11-27 12:39:41', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(459, 10, '2020-11-27 12:40:31', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(460, 10, '2020-11-27 12:40:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(461, 10, '2020-11-27 12:42:05', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(462, 10, '2020-11-27 12:42:10', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(463, 10, '2020-11-27 12:42:49', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(464, 10, '2020-11-27 12:43:58', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(465, 10, '2020-11-27 12:44:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(466, 10, '2020-11-27 12:44:47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(467, 10, '2020-11-27 12:44:55', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(468, 10, '2020-11-27 12:47:10', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(469, 10, '2020-11-27 12:48:29', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(470, 10, '2020-11-27 12:48:42', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(471, 10, '2020-11-27 12:49:06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(472, 10, '2020-11-27 12:51:36', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(473, 10, '2020-11-27 12:54:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(474, 10, '2020-11-27 12:56:33', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(475, 10, '2020-11-27 12:57:08', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(476, 10, '2020-11-27 12:57:16', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(477, 10, '2020-11-27 12:57:37', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(478, 10, '2020-11-27 12:57:42', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(479, 10, '2020-11-27 12:57:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(480, 10, '2020-11-27 12:57:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(481, 10, '2020-11-27 12:57:55', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(482, 2, '2020-11-27 12:58:04', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(483, 2, '2020-11-27 12:58:57', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(484, 10, '2020-11-27 12:59:42', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(485, 2, '2020-11-27 12:59:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(486, 2, '2020-11-27 13:08:00', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(487, 10, '2020-11-27 13:09:08', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(488, 2, '2020-11-27 13:09:13', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(489, 2, '2020-11-27 13:09:38', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(490, 2, '2020-11-27 13:11:11', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(491, 2, '2020-11-27 13:11:43', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(492, 2, '2020-11-27 13:13:11', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(493, 2, '2020-11-27 13:14:36', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(494, 2, '2020-11-27 13:15:47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(495, 2, '2020-11-27 13:17:15', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(496, 2, '2020-11-27 13:17:59', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(497, 2, '2020-11-27 13:18:44', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(498, 2, '2020-11-27 13:19:47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(499, 2, '2020-11-27 13:20:23', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(500, 2, '2020-11-27 13:20:49', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(501, 2, '2020-11-27 13:21:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(502, 2, '2020-11-27 13:21:58', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(503, 2, '2020-11-27 13:22:08', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(504, 2, '2020-11-27 13:22:14', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(505, 2, '2020-11-27 13:22:40', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(506, 2, '2020-11-27 13:23:05', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(507, 2, '2020-11-27 13:24:06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(508, 10, '2020-11-27 13:24:12', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(509, 10, '2020-11-27 13:24:14', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36 Edg/87.0.664.41'),
(510, 2, '2020-11-27 13:30:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(511, 2, '2020-11-27 13:31:10', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(512, 2, '2020-11-27 13:56:00', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(513, 10, '2020-11-27 16:56:47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(514, 10, '2020-11-27 16:57:02', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(515, 10, '2020-11-27 16:57:11', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(516, 10, '2020-11-27 16:57:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(517, 10, '2020-11-27 16:58:25', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(518, 10, '2020-11-27 16:59:28', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(519, 10, '2020-11-27 17:01:02', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(520, 10, '2020-11-27 17:01:17', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(521, 10, '2020-11-27 17:03:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(522, 10, '2020-11-27 17:04:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(523, 10, '2020-11-27 17:05:03', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(524, 10, '2020-11-27 17:05:15', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(525, 10, '2020-11-27 17:05:32', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(526, 10, '2020-11-27 17:05:43', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(527, 10, '2020-11-27 17:05:51', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(528, 10, '2020-11-27 17:20:14', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(529, 10, '2020-11-27 17:20:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(530, 10, '2020-11-27 17:22:47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(531, 10, '2020-11-27 17:23:37', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(532, 10, '2020-11-27 17:24:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(533, 10, '2020-11-28 01:04:34', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(534, 10, '2020-11-30 16:54:40', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(535, 10, '2020-11-30 16:55:13', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(536, 10, '2020-11-30 16:58:22', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(537, 10, '2020-11-30 16:58:28', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(538, 10, '2020-11-30 16:58:31', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(539, 10, '2020-11-30 16:58:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(540, 10, '2020-11-30 16:58:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(541, 10, '2020-11-30 16:58:57', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(542, 10, '2020-11-30 16:59:31', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(543, 10, '2020-11-30 18:20:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(544, 10, '2020-12-01 14:27:15', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(545, 10, '2020-12-01 15:14:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(546, 10, '2020-12-01 15:29:15', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(547, 10, '2020-12-01 15:32:15', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(548, 10, '2020-12-02 01:08:33', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(549, 10, '2020-12-02 01:08:51', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(550, 10, '2020-12-02 01:29:15', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(551, 10, '2020-12-02 01:29:28', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(552, 10, '2020-12-02 01:29:43', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(553, 10, '2020-12-02 01:31:35', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(554, 10, '2020-12-02 01:31:55', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(555, 10, '2020-12-02 01:32:09', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(556, 10, '2020-12-02 01:38:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(557, 10, '2020-12-02 01:40:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(558, 10, '2020-12-02 01:40:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(559, 10, '2020-12-02 01:41:14', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(560, 10, '2020-12-02 01:41:21', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(561, 10, '2020-12-02 01:41:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(562, 10, '2020-12-02 01:42:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(563, 10, '2020-12-02 01:44:12', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(564, 10, '2020-12-02 01:44:51', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(565, 10, '2020-12-02 10:09:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(566, 10, '2020-12-02 11:45:07', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(567, 10, '2020-12-02 11:45:18', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(568, 10, '2020-12-02 14:17:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(569, 10, '2020-12-02 14:17:42', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.47'),
(570, 2, '2020-12-02 14:20:12', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(571, 11, '2020-12-02 14:57:28', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(572, 10, '2020-12-03 11:07:35', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.52'),
(573, 11, '2020-12-03 11:08:27', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.52'),
(574, 10, '2020-12-03 11:09:23', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(575, 10, '2020-12-03 11:09:43', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(576, 10, '2020-12-03 11:22:31', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.52'),
(577, 10, '2020-12-08 11:52:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.55'),
(578, 10, '2020-12-08 11:52:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.55'),
(579, 10, '2020-12-08 11:52:49', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.55'),
(580, 10, '2020-12-08 11:53:07', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.55'),
(581, 10, '2020-12-08 11:56:17', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.55'),
(582, 10, '2020-12-08 11:57:34', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.55'),
(583, 10, '2020-12-08 12:08:38', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.55'),
(584, 10, '2020-12-08 13:47:23', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.55'),
(585, 10, '2020-12-08 13:47:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.55'),
(586, 10, '2020-12-08 13:47:46', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.55'),
(587, 10, '2020-12-08 13:47:59', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 Edg/87.0.664.55'),
(588, 10, '2020-12-08 13:49:09', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(589, 1, '2020-12-08 13:49:35', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(590, 10, '2020-12-10 14:57:32', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(591, 10, '2020-12-10 15:00:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(592, 10, '2020-12-10 15:43:33', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(593, 10, '2020-12-10 17:02:06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(594, 11, '2020-12-10 17:02:21', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(595, 10, '2020-12-14 13:04:17', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(596, 10, '2020-12-14 13:05:05', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(597, 10, '2020-12-14 13:05:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0'),
(598, 10, '2020-12-14 13:06:04', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'),
(599, 10, '2020-12-14 13:06:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(600, 10, '2020-12-14 13:06:37', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(601, 10, '2020-12-14 13:06:39', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(602, 10, '2020-12-14 13:06:49', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(603, 10, '2020-12-14 13:06:59', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(604, 10, '2020-12-14 13:07:45', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(605, 11, '2020-12-14 13:08:21', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'),
(606, 11, '2020-12-14 13:10:21', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'),
(607, 10, '2020-12-14 13:10:31', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(608, 10, '2020-12-14 13:11:17', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(609, 10, '2020-12-14 13:11:33', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(610, 10, '2020-12-14 13:11:37', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57');
INSERT INTO `member_logs` (`id`, `member_id`, `date`, `ip_address`, `user_agent`) VALUES
(611, 10, '2020-12-14 13:11:44', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(612, 10, '2020-12-14 13:12:04', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(613, 10, '2020-12-14 13:12:20', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(614, 10, '2020-12-14 13:12:29', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.57'),
(615, 11, '2020-12-15 11:17:43', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(616, 11, '2020-12-15 11:52:29', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(617, 10, '2020-12-21 11:10:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(618, 10, '2020-12-21 11:10:59', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(619, 10, '2020-12-21 11:13:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(620, 10, '2020-12-21 11:15:21', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(621, 10, '2020-12-21 11:17:11', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(622, 10, '2020-12-21 11:17:26', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(623, 10, '2020-12-21 11:17:43', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(624, 10, '2020-12-21 11:18:23', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(625, 10, '2020-12-21 11:27:41', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(626, 10, '2020-12-21 11:47:57', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(627, 10, '2020-12-21 11:48:38', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(628, 10, '2020-12-21 11:49:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(629, 10, '2020-12-21 11:51:31', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'),
(630, 10, '2020-12-21 11:56:34', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `for_id` int(11) NOT NULL,
  `by_id` int(11) NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` json DEFAULT NULL,
  `viewed` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `looked_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `viewed` (`viewed`) USING BTREE,
  KEY `looked_at` (`looked_at`) USING BTREE,
  KEY `for_id` (`for_id`) USING BTREE,
  KEY `by_id` (`by_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `for_id`, `by_id`, `type`, `type_id`, `event`, `message`, `url`, `data`, `viewed`, `looked_at`, `created_at`, `updated_at`) VALUES
(1, 10, 10, 'task', '09107CD4-29AE-4359-98E7-BE4CE4E9290B', 'task expiring', 'Your task will be expiring in 24 hours with no candidates application. Click here to modify your task detail', '/mtp-dev/workspace/2020/11#09107cd4-29ae-4359-98e7-be4ce4e9290b', '{\"id\": \"09107CD4-29AE-4359-98E7-BE4CE4E9290B\", \"slug\": \"drummer-for-a-day\", \"type\": \"by-hour\", \"hours\": \"3\", \"title\": \"Drummer for a day\", \"budget\": \"1202.00\", \"number\": \"T20114\", \"status\": \"published\", \"urgent\": \"0\", \"user_id\": \"10\", \"assigned\": null, \"category\": \"4\", \"location\": \"66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia\", \"start_by\": \"2020-12-04 00:00:00\", \"applicants\": \"0\", \"created_at\": \"2020-11-23 15:05:54\", \"updated_at\": \"2020-11-25 12:40:02\", \"assigned_to\": null, \"complete_by\": \"2020-12-04 00:00:00\", \"description\": \"Play drum for a day\", \"sub_category\": \"27\", \"state_country\": \"Selangor, Malaysia\", \"rejection_rate\": \"10\"}', 'N', '2020-12-03 05:20:49', '2020-11-27 05:27:40', '2020-12-03 05:20:49'),
(2, 10, 10, 'task', 'F9604331-09FE-4021-9CDB-C98CFA2BD30E', 'task expiring', 'Your task will be expiring in 24 hours with no candidates application. Click here to modify your task detail', '/mtp-dev/workspace/2020/11#f9604331-09fe-4021-9cdb-c98cfa2bd30e', '{\"id\": \"F9604331-09FE-4021-9CDB-C98CFA2BD30E\", \"slug\": \"activate-robot-vacuum-daily\", \"type\": \"by-hour\", \"hours\": \"1\", \"title\": \"Activate Robot Vacuum daily\", \"budget\": \"10.00\", \"number\": \"T20113\", \"status\": \"published\", \"urgent\": \"0\", \"user_id\": \"10\", \"assigned\": null, \"category\": \"1\", \"location\": \"remotely\", \"start_by\": \"2020-11-27 00:00:00\", \"applicants\": \"0\", \"created_at\": \"2020-11-23 15:00:43\", \"updated_at\": null, \"assigned_to\": null, \"complete_by\": \"2020-12-10 00:00:00\", \"description\": \"at 2pm sharp\", \"sub_category\": \"11\", \"state_country\": \"\", \"rejection_rate\": \"10\"}', 'N', '2020-12-03 05:20:49', '2020-11-27 05:27:40', '2020-12-03 05:20:49'),
(3, 10, 10, 'user', '10', 'profile complete', 'Your profile is 100% completed', '/mtp-dev/workspace/search', '{\"id\": \"10\", \"dob\": \"1984-06-30\", \"url\": \"/talent/TL20110\", \"zip\": null, \"city\": \"76500\", \"name\": \"Kevin Test\", \"nric\": \"840630145493\", \"type\": \"0\", \"about\": \"aaaabbbccc\", \"email\": \"kevin@byte2c.com\", \"photo\": \"assets/img/default-avatar.png\", \"state\": \"1944\", \"fb_uid\": null, \"gender\": null, \"skills\": \"24,29\", \"status\": \"0\", \"contact\": null, \"country\": \"132\", \"address1\": \"\", \"address2\": null, \"fb_email\": null, \"insta_id\": null, \"language\": \"\", \"lastname\": \"Test\", \"firstname\": \"Kevin\", \"google_id\": \"\", \"completion\": \"90\", \"preference\": {\"id\": \"10\", \"bank\": \"Public Bank\", \"acc_num\": \"09120321\", \"p_state\": \"0\", \"acc_name\": \"sadasd\", \"p_radius\": \"\", \"rm_state\": \"\", \"work_day\": \"allWeek\", \"member_id\": \"10\", \"p_country\": \"132\", \"rm_radius\": \"\", \"rm_country\": \"132\", \"work_day_am\": \"0\", \"work_day_pm\": \"0\", \"work_day_full\": \"0\", \"working_hours\": \"9\", \"work_day_after\": \"0\", \"p_work_location\": \"same_country\", \"work_load_hours\": \"10\", \"expected_earning\": \"1000\", \"rm_work_location\": \"anywhere\"}, \"salutation\": null, \"nationality\": null, \"profile_pic\": null, \"date_created\": \"2020-11-12 12:14:38\", \"mobile_number\": \"+60156677888\", \"contact_method\": [\"\"], \"contact_number\": \"+601554212321\", \"dashboard_widget\": \"4,7,8\", \"other_social_media\": null, \"mobile_number_prefix\": \"\", \"contact_number_prefix\": \"\", \"emergency_contact_name\": \"\", \"emergency_contact_number\": \"+60421312321\", \"emergency_contact_relation\": \"\", \"emergency_contact_number_prefix\": \"\"}', 'N', '2020-12-03 05:20:49', '2020-11-27 09:05:42', '2020-12-03 05:20:49'),
(4, 10, 10, 'task', 'CB438C15-C218-4262-A3C7-0527B82ED4B8', 'created', 'Your task has been published. Click here to view detail', '/mtp-dev/workspace/task/serve-drinks/details', '{\"id\": \"CB438C15-C218-4262-A3C7-0527B82ED4B8\", \"slug\": \"serve-drinks\", \"type\": \"lump-sum\", \"hours\": \"0\", \"title\": \"Serve Drinks\", \"budget\": \"100.00\", \"number\": \"T20115\", \"status\": \"published\", \"urgent\": \"0\", \"user_id\": \"10\", \"assigned\": null, \"category\": \"2\", \"location\": \"remotely\", \"start_by\": \"2020-12-02 00:00:00\", \"created_at\": \"2020-12-01 15:17:34\", \"updated_at\": null, \"assigned_to\": null, \"complete_by\": \"2020-12-10 00:00:00\", \"description\": \"hahaha\", \"sub_category\": \"15\", \"state_country\": \"\", \"rejection_rate\": \"10\"}', 'N', '2020-12-03 05:20:49', '2020-12-01 07:17:34', '2020-12-03 05:20:49'),
(5, 10, 2, 'task', '66012D96-BA2D-451F-B413-95F2330B54C1', 'review reminder', 'You have not rate (%Syafiq Syazre%) for the task %Clean my House%', '/mtp-dev/workspace/rating-centre', '{\"id\": \"66012D96-BA2D-451F-B413-95F2330B54C1\", \"to\": \"10\", \"from\": \"2\", \"slug\": \"clean-my-house\", \"type\": \"by-hour\", \"hours\": \"6\", \"title\": \"Clean my House\", \"budget\": \"200.00\", \"number\": \"T20110\", \"status\": \"completed\", \"urgent\": \"0\", \"user_id\": \"10\", \"assigned\": \"2020-11-13 00:49:18\", \"category\": \"1\", \"location\": \"66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia\", \"reviewer\": {\"name\": \"Syafiq Syazre\", \"company_name\": null}, \"start_by\": \"2020-11-13 00:00:00\", \"applicants\": \"1\", \"created_at\": \"2020-11-12 21:19:18\", \"updated_at\": \"2020-11-13 13:52:45\", \"assigned_to\": \"2\", \"complete_by\": \"2020-11-15 00:00:00\", \"description\": \"asdasdsad\", \"sub_category\": \"12\", \"state_country\": \"Selangor, Malaysia\", \"rejection_rate\": \"10\"}', 'N', '2020-12-03 05:20:49', '2020-12-02 06:19:16', '2020-12-03 05:20:49'),
(6, 10, 2, 'task', 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'review reminder', 'You have not rate (%Syafiq Syazre%) for the task %Cook a food%', '/mtp-dev/workspace/rating-centre', '{\"id\": \"F87485C2-9653-4BFA-826E-0C2A235C7144\", \"to\": \"10\", \"from\": \"2\", \"slug\": \"cook-a-food\", \"type\": \"by-hour\", \"hours\": \"3\", \"title\": \"Cook a food\", \"budget\": \"100.00\", \"number\": \"T20108\", \"status\": \"completed\", \"urgent\": \"0\", \"user_id\": \"10\", \"assigned\": \"2020-11-12 14:48:48\", \"category\": \"2\", \"location\": \"66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia\", \"reviewer\": {\"name\": \"Syafiq Syazre\", \"company_name\": null}, \"start_by\": \"2020-11-13 00:00:00\", \"applicants\": \"1\", \"created_at\": \"2020-11-12 14:45:14\", \"updated_at\": \"2020-11-12 14:55:24\", \"assigned_to\": \"2\", \"complete_by\": \"2020-11-14 00:00:00\", \"description\": \"asdsadadsad\", \"sub_category\": \"16\", \"state_country\": \"Selangor, Malaysia\", \"rejection_rate\": \"10\"}', 'N', '2020-12-03 05:20:49', '2020-12-02 06:19:16', '2020-12-03 05:20:49'),
(7, 10, 11, 'task', 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'review reminder', 'You have not rate (%Kevin Company Sdn Bhd%) for the task %Serve water%', '/mtp-dev/workspace/rating-centre', '{\"id\": \"CDA7DB4E-8703-453F-B33B-D83285F72054\", \"to\": \"10\", \"from\": \"11\", \"slug\": \"serve-water\", \"type\": \"by-hour\", \"hours\": \"4\", \"title\": \"Serve water\", \"budget\": \"0.00\", \"number\": \"T20112\", \"status\": \"closed\", \"urgent\": \"0\", \"user_id\": \"11\", \"assigned\": \"2020-11-19 16:34:46\", \"category\": \"2\", \"location\": \"66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia\", \"reviewer\": {\"name\": \"Bytetoc Computing\", \"company_name\": \"Kevin Company Sdn Bhd\"}, \"start_by\": \"2020-11-20 00:00:00\", \"applied_at\": \"2020-11-19 16:34:28\", \"created_at\": \"2020-11-19 11:02:40\", \"updated_at\": \"2020-11-27 12:49:42\", \"assigned_to\": \"10\", \"complete_by\": \"2020-11-21 00:00:00\", \"description\": \"Carry around drinks\", \"sub_category\": \"19\", \"state_country\": \"Selangor, Malaysia\", \"rejection_rate\": \"10\"}', 'N', '2020-12-03 05:20:49', '2020-12-02 06:19:16', '2020-12-03 05:20:49'),
(8, 10, 11, 'task', '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'review reminder', 'You have not rate (%Kevin Company Sdn Bhd%) for the task %Fold a bird%', '/mtp-dev/workspace/rating-centre', '{\"id\": \"4F2165B6-C1FD-4F20-8287-2F9B84BF97AA\", \"to\": \"10\", \"from\": \"11\", \"slug\": \"fold-a-bird\", \"type\": \"by-hour\", \"hours\": \"3\", \"title\": \"Fold a bird\", \"budget\": \"0.00\", \"number\": \"T20111\", \"status\": \"closed\", \"urgent\": \"0\", \"user_id\": \"11\", \"assigned\": \"2020-11-18 13:34:55\", \"category\": \"3\", \"location\": \"66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia\", \"reviewer\": {\"name\": \"Bytetoc Computing\", \"company_name\": \"Kevin Company Sdn Bhd\"}, \"start_by\": \"2020-11-19 00:00:00\", \"applied_at\": \"2020-11-18 13:30:19\", \"created_at\": \"2020-11-18 13:30:10\", \"updated_at\": \"2020-11-27 12:49:37\", \"assigned_to\": \"10\", \"complete_by\": \"2020-11-21 00:00:00\", \"description\": \"with lots of paper\", \"sub_category\": \"20\", \"state_country\": \"Selangor, Malaysia\", \"rejection_rate\": \"10\"}', 'Y', '2020-12-03 05:20:49', '2020-12-02 06:19:16', '2020-12-03 05:20:49'),
(9, 10, 2, 'task', '70E8134A-10DB-475B-8180-E74CA2481EF2', 'review reminder', 'You have not rate (%Syafiq Syazre%) for the task %Wash my cup%', '/mtp-dev/workspace/rating-centre', '{\"id\": \"70E8134A-10DB-475B-8180-E74CA2481EF2\", \"to\": \"10\", \"from\": \"2\", \"slug\": \"wash-my-cup\", \"type\": \"by-hour\", \"hours\": \"3\", \"title\": \"Wash my cup\", \"budget\": \"200.00\", \"number\": \"T20109\", \"status\": \"disputed\", \"urgent\": \"0\", \"user_id\": \"2\", \"assigned\": \"2020-11-12 15:10:15\", \"category\": \"3\", \"location\": \"66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia\", \"reviewer\": {\"name\": \"Syafiq Syazre\", \"company_name\": null}, \"start_by\": \"2020-11-13 00:00:00\", \"applied_at\": \"2020-11-12 15:09:34\", \"created_at\": \"2020-11-12 15:08:40\", \"updated_at\": \"2020-10-27 13:15:02\", \"assigned_to\": \"10\", \"complete_by\": \"2021-01-04 00:00:00\", \"description\": \"asdasdsad\", \"sub_category\": \"22\", \"state_country\": \"Selangor, Malaysia\", \"rejection_rate\": \"60\"}', 'N', '2020-12-03 05:20:49', '2020-12-02 06:19:16', '2020-12-03 05:20:49'),
(10, 2, 10, 'task', '70E8134A-10DB-475B-8180-E74CA2481EF2', 'review reminder', 'You have not rate (%Kevin Test%) for the task %Wash my cup%', '/mtp-dev/workspace/rating-centre', '{\"id\": \"70E8134A-10DB-475B-8180-E74CA2481EF2\", \"to\": \"2\", \"from\": \"10\", \"slug\": \"wash-my-cup\", \"type\": \"by-hour\", \"hours\": \"3\", \"title\": \"Wash my cup\", \"budget\": \"200.00\", \"number\": \"T20109\", \"status\": \"disputed\", \"urgent\": \"0\", \"user_id\": \"2\", \"assigned\": \"2020-11-12 15:10:15\", \"category\": \"3\", \"location\": \"66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia\", \"reviewer\": {\"name\": \"Kevin Test\", \"company_name\": null}, \"start_by\": \"2020-11-13 00:00:00\", \"applicants\": \"1\", \"created_at\": \"2020-11-12 15:08:40\", \"updated_at\": \"2020-10-27 13:15:02\", \"assigned_to\": \"10\", \"complete_by\": \"2021-01-04 00:00:00\", \"description\": \"asdasdsad\", \"sub_category\": \"22\", \"state_country\": \"Selangor, Malaysia\", \"rejection_rate\": \"60\"}', 'N', NULL, '2020-12-02 06:20:16', NULL),
(11, 2, 10, 'task', '66012D96-BA2D-451F-B413-95F2330B54C1', 'review reminder', 'You have not rate (%Kevin Test%) for the task %Clean my House%', '/mtp-dev/workspace/rating-centre', '{\"id\": \"66012D96-BA2D-451F-B413-95F2330B54C1\", \"to\": \"2\", \"from\": \"10\", \"slug\": \"clean-my-house\", \"type\": \"by-hour\", \"hours\": \"6\", \"title\": \"Clean my House\", \"budget\": \"200.00\", \"number\": \"T20110\", \"status\": \"completed\", \"urgent\": \"0\", \"user_id\": \"10\", \"assigned\": \"2020-11-13 00:49:18\", \"category\": \"1\", \"location\": \"66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia\", \"reviewer\": {\"name\": \"Kevin Test\", \"company_name\": null}, \"start_by\": \"2020-11-13 00:00:00\", \"applied_at\": \"2020-11-13 00:48:18\", \"created_at\": \"2020-11-12 21:19:18\", \"updated_at\": \"2020-11-13 13:52:45\", \"assigned_to\": \"2\", \"complete_by\": \"2020-11-15 00:00:00\", \"description\": \"asdasdsad\", \"sub_category\": \"12\", \"state_country\": \"Selangor, Malaysia\", \"rejection_rate\": \"10\"}', 'N', NULL, '2020-12-02 06:20:16', NULL),
(12, 2, 10, 'task', 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'review reminder', 'You have not rate (%Kevin Test%) for the task %Cook a food%', '/mtp-dev/workspace/rating-centre', '{\"id\": \"F87485C2-9653-4BFA-826E-0C2A235C7144\", \"to\": \"2\", \"from\": \"10\", \"slug\": \"cook-a-food\", \"type\": \"by-hour\", \"hours\": \"3\", \"title\": \"Cook a food\", \"budget\": \"100.00\", \"number\": \"T20108\", \"status\": \"completed\", \"urgent\": \"0\", \"user_id\": \"10\", \"assigned\": \"2020-11-12 14:48:48\", \"category\": \"2\", \"location\": \"66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia\", \"reviewer\": {\"name\": \"Kevin Test\", \"company_name\": null}, \"start_by\": \"2020-11-13 00:00:00\", \"applied_at\": \"2020-11-12 14:48:19\", \"created_at\": \"2020-11-12 14:45:14\", \"updated_at\": \"2020-11-12 14:55:24\", \"assigned_to\": \"2\", \"complete_by\": \"2020-11-14 00:00:00\", \"description\": \"asdsadadsad\", \"sub_category\": \"16\", \"state_country\": \"Selangor, Malaysia\", \"rejection_rate\": \"10\"}', 'N', NULL, '2020-12-02 06:20:16', NULL),
(13, 11, 10, 'task', 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'review reminder', 'You have not rate (%Kevin Test%) for the task %Serve water%', '/mtp-dev/workspace/rating-centre', '{\"id\": \"CDA7DB4E-8703-453F-B33B-D83285F72054\", \"to\": \"11\", \"from\": \"10\", \"slug\": \"serve-water\", \"type\": \"by-hour\", \"hours\": \"4\", \"title\": \"Serve water\", \"budget\": \"0.00\", \"number\": \"T20112\", \"skills\": \"\", \"status\": \"closed\", \"urgent\": \"0\", \"user_id\": \"11\", \"assigned\": \"2020-11-19 16:34:46\", \"category\": \"2\", \"location\": \"66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia\", \"reviewer\": {\"name\": \"Kevin Test\", \"company_name\": null}, \"start_by\": \"2020-11-20 00:00:00\", \"applicants\": \"1\", \"created_at\": \"2020-11-19 11:02:40\", \"updated_at\": \"2020-11-27 12:49:42\", \"assigned_to\": \"10\", \"complete_by\": \"2020-11-21 00:00:00\", \"description\": \"Carry around drinks\", \"sub_category\": \"19\", \"state_country\": \"Selangor, Malaysia\", \"rejection_rate\": \"10\"}', 'N', '2020-12-03 03:09:56', '2020-12-02 06:57:36', '2020-12-03 03:09:56'),
(14, 11, 10, 'task', '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'review reminder', 'You have not rate (%Kevin Test%) for the task %Fold a bird%', '/mtp-dev/workspace/rating-centre', '{\"id\": \"4F2165B6-C1FD-4F20-8287-2F9B84BF97AA\", \"to\": \"11\", \"from\": \"10\", \"slug\": \"fold-a-bird\", \"type\": \"by-hour\", \"hours\": \"3\", \"title\": \"Fold a bird\", \"budget\": \"0.00\", \"number\": \"T20111\", \"skills\": \"\", \"status\": \"closed\", \"urgent\": \"0\", \"user_id\": \"11\", \"assigned\": \"2020-11-18 13:34:55\", \"category\": \"3\", \"location\": \"66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia\", \"reviewer\": {\"name\": \"Kevin Test\", \"company_name\": null}, \"start_by\": \"2020-11-19 00:00:00\", \"applicants\": \"1\", \"created_at\": \"2020-11-18 13:30:10\", \"updated_at\": \"2020-11-27 12:49:37\", \"assigned_to\": \"10\", \"complete_by\": \"2020-11-21 00:00:00\", \"description\": \"with lots of paper\", \"sub_category\": \"20\", \"state_country\": \"Selangor, Malaysia\", \"rejection_rate\": \"10\"}', 'N', '2020-12-03 03:09:56', '2020-12-02 06:57:36', '2020-12-03 03:09:56'),
(15, 11, 11, 'task', 'F93AC8F1-5F1B-4459-B9DB-9EFF86B22965', 'created', 'Your task has been published. Click here to view detail', '/mtp-dev/workspace/task/clean-tables/details', '{\"id\": \"F93AC8F1-5F1B-4459-B9DB-9EFF86B22965\", \"slug\": \"clean-tables\", \"type\": \"lump-sum\", \"hours\": \"0\", \"title\": \"Clean tables\", \"budget\": \"20.00\", \"number\": \"B22965\", \"skills\": \"\", \"status\": \"published\", \"urgent\": \"0\", \"user_id\": \"11\", \"assigned\": null, \"category\": \"2\", \"location\": \"remotely\", \"start_by\": \"2020-12-18 00:00:00\", \"created_at\": \"2020-12-03 11:09:00\", \"updated_at\": null, \"assigned_to\": null, \"complete_by\": \"2020-12-19 00:00:00\", \"description\": \"wipe wipe\", \"sub_category\": \"17\", \"state_country\": \"\", \"rejection_rate\": \"10\"}', 'N', '2020-12-03 03:09:56', '2020-12-03 03:09:00', '2020-12-03 03:09:56'),
(16, 11, 11, 'task', 'F93AC8F1-5F1B-4459-B9DB-9EFF86B22965', 'task expiring', 'Your task will be expiring in 24 hours with no candidates application. Click here to modify your task detail', '/mtp-dev/workspace/2020/12#f93ac8f1-5f1b-4459-b9db-9eff86b22965', '{\"id\": \"F93AC8F1-5F1B-4459-B9DB-9EFF86B22965\", \"slug\": \"clean-tables\", \"type\": \"lump-sum\", \"hours\": \"0\", \"title\": \"Clean tables\", \"budget\": \"20.00\", \"number\": \"B22965\", \"skills\": \"\", \"status\": \"published\", \"urgent\": \"0\", \"user_id\": \"11\", \"assigned\": null, \"category\": \"2\", \"location\": \"remotely\", \"start_by\": \"2020-12-18 00:00:00\", \"applicants\": \"0\", \"created_at\": \"2020-12-03 11:09:00\", \"updated_at\": null, \"assigned_to\": null, \"complete_by\": \"2020-12-19 00:00:00\", \"description\": \"wipe wipe\", \"sub_category\": \"17\", \"state_country\": \"\", \"rejection_rate\": \"10\"}', 'N', '2020-12-03 03:09:56', '2020-12-03 03:09:02', '2020-12-03 03:09:56'),
(17, 11, 10, 'task', 'F93AC8F1-5F1B-4459-B9DB-9EFF86B22965', 'applied', 'You have received a new application %Clean tables%', '/mtp-dev/workspace/2020/12#f93ac8f1-5f1b-4459-b9db-9eff86b22965', '{\"id\": \"1\", \"dob\": null, \"zip\": \"42133\", \"city\": \"Shah Alam\", \"name\": \"Kevin Company Sdn Bhd\", \"nric\": \"1283012983901283\", \"slug\": \"clean-tables\", \"type\": \"lump-sum\", \"about\": null, \"email\": \"kevinaa@byte.com\", \"hours\": \"0\", \"photo\": \"files/profile/11/WhatsApp Image 2020-11-20 at 16_5fbdd4d105c62.jpeg\", \"state\": \"1944\", \"title\": \"Clean tables\", \"budget\": \"20.00\", \"fb_uid\": null, \"gender\": null, \"number\": \"B22965\", \"photos\": \"a:0:{}\", \"skills\": \"\", \"status\": \"published\", \"urgent\": \"0\", \"contact\": null, \"country\": \"132\", \"reg_num\": \"aSLKDLA-121\", \"task_id\": \"F93AC8F1-5F1B-4459-B9DB-9EFF86B22965\", \"user_id\": \"11\", \"about_us\": \"ajNSLKDNASd\\r\\naS\\r\\nDASD\\r\\nA\\r\\nSd\\r\\nAS\\r\\nD\", \"address1\": \"asdasd\", \"address2\": \"sdfsa\", \"assigned\": null, \"category\": \"Food & Beverage\", \"fb_email\": null, \"industry\": \"\", \"insta_id\": null, \"language\": null, \"lastname\": \"Computing\", \"location\": \"remotely\", \"start_by\": \"2020-12-18 00:00:00\", \"firstname\": \"Bytetoc\", \"google_id\": \"\", \"member_id\": \"11\", \"task_type\": \"lump-sum\", \"user_type\": \"1\", \"completion\": \"55\", \"created_at\": \"2020-12-03 11:09:00\", \"salutation\": null, \"updated_at\": null, \"assigned_to\": null, \"complete_by\": \"2020-12-19 00:00:00\", \"description\": \"wipe wipe\", \"nationality\": null, \"profile_pic\": null, \"task_status\": \"published\", \"company_name\": \"Kevin Company Sdn Bhd\", \"date_created\": \"2020-11-12 13:44:13\", \"sub_category\": \"Cleaner\", \"company_phone\": \"+60210291102\", \"mobile_number\": \"+60102391023921\", \"state_country\": \"\", \"contact_method\": \"\", \"contact_number\": \"+6010293012\", \"rejection_rate\": \"10\", \"dashboard_widget\": \"5,8,9,11\", \"other_social_media\": null, \"company_phone_prefix\": \"02\", \"mobile_number_prefix\": \"023\", \"contact_number_prefix\": \"02\", \"emergency_contact_name\": null, \"emergency_contact_number\": null, \"emergency_contact_relation\": null, \"emergency_contact_number_prefix\": null}', 'Y', '2020-12-03 03:09:56', '2020-12-03 03:09:49', '2020-12-03 03:09:57'),
(18, 2, 10, 'task-chat', '66012D96-BA2D-451F-B413-95F2330B54C1', 'new task message', '%Kevin Test% has sent you (2) messages', '/mtp-dev/workspace/2020/12#66012d96-ba2d-451f-b413-95f2330b54c1', '{\"id\": \"45\", \"size\": null, \"type\": \"text\", \"count\": \"2\", \"viewed\": \"0\", \"deleted\": \"0\", \"task_id\": \"66012D96-BA2D-451F-B413-95F2330B54C1\", \"batch_id\": null, \"contents\": \"aa\", \"reply_id\": \"\", \"sender_id\": \"10\", \"created_by\": \"2020-12-03 11:22:59\", \"receiver_id\": \"2\", \"viewed_date\": \"0000-00-00 00:00:00\", \"unread_count\": \"2\", \"file_extension\": null}', 'N', NULL, '2020-12-08 04:44:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` text NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_requests`
--

DROP TABLE IF EXISTS `payment_requests`;
CREATE TABLE IF NOT EXISTS `payment_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `times` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_requests`
--

INSERT INTO `payment_requests` (`id`, `task_id`, `times`, `created_at`, `updated_at`) VALUES
(9, '70E8134A-10DB-475B-8180-E74CA2481EF2', 1, '2020-11-27 04:03:46', NULL),
(8, 'CDA7DB4E-8703-453F-B33B-D83285F72054', 1, '2020-11-19 08:36:53', NULL),
(7, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 1, '2020-11-18 05:38:29', NULL),
(6, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 1, '2020-11-12 16:40:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pinned_messages`
--

DROP TABLE IF EXISTS `pinned_messages`;
CREATE TABLE IF NOT EXISTS `pinned_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` text,
  `pinned_by` int(11) DEFAULT NULL,
  `pinned_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `task_id` text,
  `message_type` text,
  `pin_status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pinned_messages`
--

INSERT INTO `pinned_messages` (`id`, `message_id`, `pinned_by`, `pinned_time`, `task_id`, `message_type`, `pin_status`) VALUES
(2, '2', 2, '2020-11-10 10:46:52', '494048EA-4A1C-40F6-889E-8DE858400541', 'text', 0),
(4, '13', 2, '2020-11-10 10:50:56', '494048EA-4A1C-40F6-889E-8DE858400541', 'mix', 0),
(5, '6', 2, '2020-11-10 10:51:11', '494048EA-4A1C-40F6-889E-8DE858400541', 'image', 0),
(7, '26', 4, '2020-11-11 07:59:49', '5F130178-1620-4912-9A7B-F0C54D145372', 'mix', 0),
(8, '33', 10, '2020-11-12 17:12:37', '66012D96-BA2D-451F-B413-95F2330B54C1', 'text', 0),
(9, '40', 10, '2020-11-27 05:35:06', '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'text', 0);

-- --------------------------------------------------------

--
-- Table structure for table `poster_reviews`
--

DROP TABLE IF EXISTS `poster_reviews`;
CREATE TABLE IF NOT EXISTS `poster_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `reviewer_id` int(11) NOT NULL,
  `talent_id` int(11) NOT NULL,
  `review` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `timeliness` tinyint(4) NOT NULL DEFAULT '0',
  `expertise` tinyint(4) NOT NULL DEFAULT '0',
  `satisfactory` tinyint(4) NOT NULL DEFAULT '0',
  `easy` tinyint(4) NOT NULL DEFAULT '0',
  `viewed` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `poster_reviews`
--

INSERT INTO `poster_reviews` (`id`, `task_id`, `reviewer_id`, `talent_id`, `review`, `timeliness`, `expertise`, `satisfactory`, `easy`, `viewed`, `created_at`, `updated_at`) VALUES
(1, '494048EA-4A1C-40F6-889E-8DE858400541', 2, 1, 'Good job for take care 10 children!', 7, 7, 7, 7, 'N', '2020-11-10 11:51:46', NULL),
(2, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 4, 5, 'Job Done', 10, 9, 9, 8, 'N', '2020-11-10 19:03:55', NULL),
(3, '31EDC848-FED9-4DE4-A3A1-F228551E4064', 4, 5, 'Use hand to sweep', 5, 5, 5, 5, 'N', '2020-11-10 19:17:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `preference`
--

DROP TABLE IF EXISTS `preference`;
CREATE TABLE IF NOT EXISTS `preference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rm_work_location` text COMMENT 'rm- Remote Work',
  `rm_radius` varchar(32) NOT NULL,
  `rm_country` text,
  `rm_state` text,
  `p_work_location` text COMMENT 'p - physical ',
  `p_radius` varchar(32) NOT NULL,
  `p_country` text,
  `p_state` text,
  `work_day` text,
  `work_day_weekday` int(11) NOT NULL,
  `work_day_weekday_full` int(11) NOT NULL,
  `work_day_weekday_after` int(11) NOT NULL,
  `work_day_weekend` int(11) NOT NULL,
  `work_day_weekend_am` int(11) NOT NULL,
  `work_day_weekend_pm` int(11) NOT NULL,
  `working_hours` text,
  `work_load_hours` int(11) NOT NULL,
  `expected_earning` text,
  `bank` varchar(64) NOT NULL,
  `acc_name` varchar(128) NOT NULL,
  `acc_num` varchar(32) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `preference`
--

INSERT INTO `preference` (`id`, `rm_work_location`, `rm_radius`, `rm_country`, `rm_state`, `p_work_location`, `p_radius`, `p_country`, `p_state`, `work_day`, `work_day_weekday`, `work_day_weekday_full`, `work_day_weekday_after`, `work_day_weekend`, `work_day_weekend_am`, `work_day_weekend_pm`, `working_hours`, `work_load_hours`, `expected_earning`, `bank`, `acc_name`, `acc_num`, `member_id`) VALUES
(1, NULL, '', NULL, NULL, NULL, '', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '', '', '', 1),
(2, 'same_country', '', '132', '1944', 'same_country', '', '132', '0', 'allWeek', 0, 0, 0, 0, 0, 0, '9', 4, '1000', 'CIMB Clicks', 'asdsad', '123123213', 2),
(3, NULL, '', NULL, NULL, NULL, '', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '', '', '', 3),
(4, 'anywhere', '', '132', '', 'same_country', '', '132', '0', 'allWeek', 0, 0, 0, 0, 0, 0, NULL, 0, NULL, '', '', '', 4),
(5, 'anywhere', '', '132', '', 'same_country', '', '132', '', 'weekdaysOnly,afterWorkHour', 0, 0, 0, 0, 0, 0, '5', 20, '2000', 'RHB Now', 'Hiram Gonzalez', '12312312312', 5),
(6, 'anywhere', '', '132', '', '', '', '132', '', 'allWeek', 0, 0, 0, 0, 0, 0, '6', 3, '100', 'CIMB Clicks', 'asdasd', '2131231', 6),
(7, 'anywhere', '', '132', '', 'same_country', '', '132', '0', 'weekdaysOnly,fullDay,afterWorkHour', 0, 0, 0, 0, 0, 0, '6', 35, '2000', 'Maybank2u', 'Syafiq Syazre', '100020003000', 8),
(8, 'anywhere', '', '132', '', 'same_country', '', '132', '0', 'weekdaysOnly,fullDay,afterWorkHour', 0, 0, 0, 0, 0, 0, '5', 12, '500', 'CIMB Clicks', 'Mohamad Shah Nusi', '76123374737', 7),
(9, 'anywhere', '', '132', '', 'same_country', '', '132', '0', 'allWeek', 0, 0, 0, 0, 0, 0, '6', 5, '1000', 'Public Bank', 'fsdfsdfsdf', '424234234234', 9),
(10, 'anywhere', '', '132', '', 'same_country', '', '132', '0', 'custom', 0, 0, 0, 0, 0, 0, '9', 1, NULL, 'Public Bank', 'sadasd', '', 10),
(11, NULL, '', NULL, NULL, NULL, '', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, 'Kuwait Finance House', 'asdas', '123123213', 11);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` text NOT NULL,
  `images` text NOT NULL,
  `title` text NOT NULL,
  `model` text NOT NULL,
  `description` text NOT NULL,
  `description2` text NOT NULL,
  `size` text NOT NULL,
  `material` text NOT NULL,
  `color` text NOT NULL,
  `fabric` text NOT NULL,
  `category` int(11) NOT NULL,
  `categories` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `price_original` decimal(10,2) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `prices` text NOT NULL,
  `options` text NOT NULL,
  `colors` text NOT NULL,
  `is_new` int(11) NOT NULL,
  `sale_price` decimal(10,2) NOT NULL,
  `product_suggestion` text NOT NULL,
  `date_added` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promo_code`
--

DROP TABLE IF EXISTS `promo_code`;
CREATE TABLE IF NOT EXISTS `promo_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `category_id` text NOT NULL,
  `product_id` text NOT NULL,
  `type` text NOT NULL,
  `value` int(11) NOT NULL,
  `code` text NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `qty` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `search`
--

DROP TABLE IF EXISTS `search`;
CREATE TABLE IF NOT EXISTS `search` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `params` json NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `search`
--

INSERT INTO `search` (`id`, `user_id`, `name`, `params`, `created_at`, `updated_at`) VALUES
(1, 8, 'My save nothing', '{\"q\": \"\", \"scope\": \"task\"}', '2020-11-12 04:19:49', NULL),
(2, 8, 'Complete task 10+', '{\"q\": \"\", \"scope\": \"talent\", \"completed\": \"10+\"}', '2020-11-12 04:33:04', NULL),
(8, 10, 'aiya', '{\"q\": \"\", \"type\": \"task\", \"scope\": \"task\"}', '2020-12-01 17:44:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `senangpay_callback`
--

DROP TABLE IF EXISTS `senangpay_callback`;
CREATE TABLE IF NOT EXISTS `senangpay_callback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` text NOT NULL,
  `order_id` text NOT NULL,
  `response` longtext NOT NULL,
  `server` longtext NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'meta_keyword', 'nicchris,furniture'),
(2, 'meta_description', 'Meta Description'),
(3, 'logo', 'upload/images/logo/NC_ONLY.png'),
(4, 'email', 'admin@nicchris.com'),
(5, 'facebook', 'https://www.facebook.com/nicchris'),
(6, 'instagram', 'https://instagram.com/nicchris'),
(7, 'google_analytics', ''),
(8, 'slider', 'a:5:{i:0;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/170911.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}i:1;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/170915.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}i:2;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/170919.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}i:3;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/170922.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}i:4;a:2:{s:3:\"src\";s:53:\"upload/images/Sliders/171018.Digital%20Management.jpg\";s:4:\"href\";s:0:\"\";}}'),
(9, 'site_name', 'MTP'),
(10, 'whatsapp', ''),
(11, 'new_products', '1,3,7,8,53'),
(12, 'featured_products', '3,6,9');

-- --------------------------------------------------------

--
-- Table structure for table `shopping_cart`
--

DROP TABLE IF EXISTS `shopping_cart`;
CREATE TABLE IF NOT EXISTS `shopping_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `item_option` text NOT NULL,
  `item_color` text NOT NULL,
  `item_ship_country` int(11) NOT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `promo_code` text NOT NULL,
  `date` datetime NOT NULL,
  `member_id` int(11) NOT NULL,
  `guest_id` text NOT NULL,
  `checkout_id` int(11) NOT NULL,
  `notified` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shopping_checkout`
--

DROP TABLE IF EXISTS `shopping_checkout`;
CREATE TABLE IF NOT EXISTS `shopping_checkout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carts` text NOT NULL,
  `member_id` int(11) NOT NULL,
  `guest_id` text NOT NULL,
  `total` double NOT NULL,
  `email` text NOT NULL,
  `comment` text NOT NULL,
  `shipping` double NOT NULL,
  `shipping_method` text NOT NULL,
  `shipping_date` date NOT NULL,
  `shipping_time` int(11) NOT NULL,
  `delivery_salutation` text NOT NULL,
  `delivery_firstname` text NOT NULL,
  `delivery_lastname` text NOT NULL,
  `delivery_address1` text NOT NULL,
  `delivery_address2` text NOT NULL,
  `delivery_city` text NOT NULL,
  `delivery_zip` text NOT NULL,
  `delivery_country` int(11) NOT NULL,
  `delivery_state` int(11) NOT NULL,
  `delivery_contact` text NOT NULL,
  `billing_salutation` text NOT NULL,
  `billing_firstname` text NOT NULL,
  `billing_lastname` text NOT NULL,
  `billing_address1` text NOT NULL,
  `billing_address2` text NOT NULL,
  `billing_city` text NOT NULL,
  `billing_zip` text NOT NULL,
  `billing_country` int(11) NOT NULL,
  `billing_state` int(11) NOT NULL,
  `billing_contact` text NOT NULL,
  `pp_token` text NOT NULL,
  `pp_payer_id` text NOT NULL,
  `payment_date` datetime NOT NULL,
  `payment_method` text NOT NULL,
  `payment_reference` longtext NOT NULL,
  `payment_receipts` longtext NOT NULL,
  `status` int(11) NOT NULL,
  `tracking_no` text NOT NULL,
  `pp_GetExpressCheckoutDetails` text NOT NULL,
  `pp_DoExpressCheckoutPayment` text NOT NULL,
  `pp_build` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `description` text,
  `parent` int(11) DEFAULT '0' COMMENT '0 - parent',
  `status` tinyint(4) DEFAULT '1' COMMENT '0 - inactive, 1- active',
  `new` tinyint(4) DEFAULT '0' COMMENT '0 - default from system, 1 - input by user',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=245 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `name`, `description`, `parent`, `status`, `new`, `date_created`) VALUES
(1, 'Art & Design', NULL, 0, 1, 0, '2020-11-12 05:40:55'),
(2, '3d Printing Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(3, 'Adobe Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(4, 'Architecture Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(5, 'Autocad Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(6, 'Cad Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(7, 'Chef Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(8, 'Cooking Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(9, 'Design Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(10, 'Drawing Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(11, 'Illustrator Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(12, 'Indesign Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(13, 'Maya Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(14, 'Photoshop Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(15, 'Product Design Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(16, 'Prototyping Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(17, 'Revit Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(18, 'Solid Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(19, 'Ui Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(20, 'Ux Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(21, 'Video Production Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(22, 'Videography Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(23, 'Web Design Skills', NULL, 1, 1, 0, '2020-11-12 05:40:55'),
(24, 'Business, Finance & Administration', NULL, 0, 1, 0, '2020-11-12 05:40:55'),
(25, 'Account Management Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(26, 'Acquisition Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(27, 'Administration Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(28, 'Audit Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(29, 'Banking Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(30, 'Billing Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(31, 'Blockchain Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(32, 'Budget Management Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(33, 'Budgeting Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(34, 'Business Analysis Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(35, 'Business Analytics Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(36, 'Business Development Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(37, 'Business Intelligence Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(38, 'Business Management Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(39, 'Compliance Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(40, 'Contract Management Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(41, 'Event Management Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(42, 'Event Planning Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(43, 'Financial Analysis Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(44, 'Financial Management Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(45, 'Financial Modeling Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(46, 'Financial Services Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(47, 'Forecasting Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(48, 'Fundraising Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(49, 'Hospitality Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(50, 'Ibm Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(51, 'Insurance Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(52, 'Logistics Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(53, 'Market Research Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(54, 'Payroll Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(55, 'Procurement Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(56, 'Product Development Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(57, 'Risk Management Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(58, 'Sap Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(59, 'Stakeholder Management Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(60, 'Supply Chain Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(61, 'Sustainability Skills', NULL, 24, 1, 0, '2020-11-12 05:40:55'),
(62, 'Education', NULL, 0, 1, 0, '2020-11-12 05:40:55'),
(63, 'Bilingual Skills', NULL, 62, 1, 0, '2020-11-12 05:40:55'),
(64, 'English Skills', NULL, 62, 1, 0, '2020-11-12 05:40:55'),
(65, 'Mathmatics Skills', NULL, 62, 1, 0, '2020-11-12 05:40:55'),
(66, 'Engineering', NULL, 0, 1, 0, '2020-11-12 05:40:55'),
(67, 'Automation Skills', NULL, 66, 1, 0, '2020-11-12 05:40:55'),
(68, 'Automotive Skills', NULL, 66, 1, 0, '2020-11-12 05:40:55'),
(69, 'Electrical Skills', NULL, 66, 1, 0, '2020-11-12 05:40:55'),
(70, 'Hardware Skills', NULL, 66, 1, 0, '2020-11-12 05:40:55'),
(71, 'Maintenance Skills', NULL, 66, 1, 0, '2020-11-12 05:40:55'),
(72, 'Manufacturing Skills', NULL, 66, 1, 0, '2020-11-12 05:40:55'),
(73, 'Robotics Skills', NULL, 66, 1, 0, '2020-11-12 05:40:55'),
(74, 'Welding Skills', NULL, 66, 1, 0, '2020-11-12 05:40:55'),
(75, 'Human Resource & Recruitment', NULL, 0, 1, 0, '2020-11-12 05:40:55'),
(76, 'Hris Skills', NULL, 75, 1, 0, '2020-11-12 05:40:55'),
(77, 'Human Resources Skills', NULL, 75, 1, 0, '2020-11-12 05:40:55'),
(78, 'Peoplesoft Skills', NULL, 75, 1, 0, '2020-11-12 05:40:55'),
(79, 'Performance Management Skills', NULL, 75, 1, 0, '2020-11-12 05:40:55'),
(80, 'Recruitment Skills', NULL, 75, 1, 0, '2020-11-12 05:40:55'),
(81, 'Relationship Management Skills', NULL, 75, 1, 0, '2020-11-12 05:40:55'),
(82, 'Training Skills', NULL, 75, 1, 0, '2020-11-12 05:40:55'),
(83, 'IT & Data Management', NULL, 0, 1, 0, '2020-11-12 05:40:55'),
(84, 'Active Directory Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(85, 'Analytics Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(86, 'Angular Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(87, 'Artificial Intelligence Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(88, 'As400 Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(89, 'Aws Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(90, 'Big Data Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(91, 'Bpm Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(92, 'C++ Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(93, 'Ccna Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(94, 'Cloud Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(95, 'Cloud Computing Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(96, 'Cms Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(97, 'Coding Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(98, 'Css Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(99, 'Cyber Security Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(100, 'Data Analysis Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(101, 'Data Analytics Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(102, 'Data Center Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(103, 'Data Collection Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(104, 'Data Management Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(105, 'Data Mining Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(106, 'Data Modelling Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(107, 'Data Warehouse Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(108, 'Database Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(109, 'Database Management Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(110, 'Debugging Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(111, 'Devops Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(112, 'Enterprise Architecture Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(113, 'Etl Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(114, 'Gis Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(115, 'Hadoop Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(116, 'Information Security Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(117, 'Information Technology Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(118, 'Iot Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(119, 'It Security Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(120, 'Java Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(121, 'Javascript Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(122, 'Karma Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(123, 'Linux Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(124, 'Machine Learning Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(125, 'Mainframe Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(126, 'Microsoft Access Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(127, 'Microsoft Office Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(128, 'Network Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(129, 'Network Security Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(130, 'Oop Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(131, 'Oracle Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(132, 'Penetration Testing Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(133, 'Php Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(134, 'React Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(135, 'Scripting Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(136, 'Security Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(137, 'Software Development Skills', NULL, 83, 1, 0, '2020-11-12 05:40:55'),
(138, 'Software Engineering Skills', NULL, 83, 1, 0, '2020-11-12 05:40:56'),
(139, 'Software Testing Skills', NULL, 83, 1, 0, '2020-11-12 05:40:56'),
(140, 'Solution Architecture Skills', NULL, 83, 1, 0, '2020-11-12 05:40:56'),
(141, 'Spark Skills', NULL, 83, 1, 0, '2020-11-12 05:40:56'),
(142, 'Spss Skills', NULL, 83, 1, 0, '2020-11-12 05:40:56'),
(143, 'Sql Server Skills', NULL, 83, 1, 0, '2020-11-12 05:40:56'),
(144, 'Statistical Analysis Skills', NULL, 83, 1, 0, '2020-11-12 05:40:56'),
(145, 'Statistics Skills', NULL, 83, 1, 0, '2020-11-12 05:40:56'),
(146, 'Tableau Skills', NULL, 83, 1, 0, '2020-11-12 05:40:56'),
(147, 'Unix Skills', NULL, 83, 1, 0, '2020-11-12 05:40:56'),
(148, 'Vba Skills', NULL, 83, 1, 0, '2020-11-12 05:40:56'),
(149, 'Web Development Skills', NULL, 83, 1, 0, '2020-11-12 05:40:56'),
(150, 'Wordpress Skills', NULL, 83, 1, 0, '2020-11-12 05:40:56'),
(151, 'Office', NULL, 0, 1, 0, '2020-11-12 05:40:56'),
(152, 'Acrobat Skills', NULL, 151, 1, 0, '2020-11-12 05:40:56'),
(153, 'Internet Skills', NULL, 151, 1, 0, '2020-11-12 05:40:56'),
(154, 'Microsoft Word Skills', NULL, 151, 1, 0, '2020-11-12 05:40:56'),
(155, 'Ms Excel Skills', NULL, 151, 1, 0, '2020-11-12 05:40:56'),
(156, 'Outlook Skills', NULL, 151, 1, 0, '2020-11-12 05:40:56'),
(157, 'Powerpoint Skills', NULL, 151, 1, 0, '2020-11-12 05:40:56'),
(158, 'Scheduling Skills', NULL, 151, 1, 0, '2020-11-12 05:40:56'),
(159, 'Technical Support Skills', NULL, 151, 1, 0, '2020-11-12 05:40:56'),
(160, 'Translation Skills', NULL, 151, 1, 0, '2020-11-12 05:40:56'),
(161, 'Project Management', NULL, 0, 1, 0, '2020-11-12 05:40:56'),
(162, 'Agile Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(163, 'Documentation Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(164, 'Editing Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(165, 'Implementation Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(166, 'Integration Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(167, 'Inventory Management Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(168, 'Lean Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(169, 'Lean Six Sigma Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(170, 'Microsoft Project Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(171, 'Ms Project Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(172, 'Operations Management Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(173, 'Planning Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(174, 'Pmp Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(175, 'Process Improvement Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(176, 'Product Management Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(177, 'Quality Assurance Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(178, 'Quality Management Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(179, 'Report Writing Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(180, 'Reporting Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(181, 'Resource Management Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(182, 'Sas Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(183, 'Testing Skills', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(184, 'Vendor Management Skill', NULL, 161, 1, 0, '2020-11-12 05:40:56'),
(185, 'Sales & Marketing', NULL, 0, 1, 0, '2020-11-12 05:40:56'),
(186, 'Advertising Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(187, 'Cold Calling Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(188, 'Consulting Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(189, 'Content Management Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(190, 'Ecommerce Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(191, 'Email Marketing Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(192, 'Facebook Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(193, 'Lead Generation Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(194, 'Outreach Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(195, 'Public Relations Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(196, 'Purchasing Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(197, 'Sales Management Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(198, 'Storytelling Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(199, 'Technical Writing Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(200, 'Telecommunications Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(201, 'Trading Skills', NULL, 185, 1, 0, '2020-11-12 05:40:56'),
(202, 'Communication & Interpersonal', NULL, 0, 1, 0, '2020-11-12 05:40:56'),
(203, 'Accountability Skills', NULL, 202, 1, 0, '2020-11-12 05:40:56'),
(204, 'Adaptability Skills', NULL, 202, 1, 0, '2020-11-12 05:40:56'),
(205, 'Client Relations Skills', NULL, 202, 1, 0, '2020-11-12 05:40:56'),
(206, 'Community Skills', NULL, 202, 1, 0, '2020-11-12 05:40:56'),
(207, 'Persuasion Skills', NULL, 202, 1, 0, '2020-11-12 05:40:56'),
(208, 'Management', NULL, 0, 1, 0, '2020-11-12 05:40:56'),
(209, 'Asset Management Skills', NULL, 208, 1, 0, '2020-11-12 05:40:56'),
(210, 'Change Management Skills', NULL, 208, 1, 0, '2020-11-12 05:40:56'),
(211, 'Client Management Skills', NULL, 208, 1, 0, '2020-11-12 05:40:56'),
(212, 'Coaching Skills', NULL, 208, 1, 0, '2020-11-12 05:40:56'),
(213, 'Crisis Management Skills', NULL, 208, 1, 0, '2020-11-12 05:40:56'),
(214, 'Facilitation Skills', NULL, 208, 1, 0, '2020-11-12 05:40:56'),
(215, 'Personal', NULL, 0, 1, 0, '2020-11-12 05:40:56'),
(216, 'Flexibility Skills', NULL, 215, 1, 0, '2020-11-12 05:40:56'),
(217, 'Independent Skills', NULL, 215, 1, 0, '2020-11-12 05:40:56'),
(218, 'Innovation Skills', NULL, 215, 1, 0, '2020-11-12 05:40:56'),
(219, 'Motivation Skills', NULL, 215, 1, 0, '2020-11-12 05:40:56'),
(220, 'Multi-tasking Skills', NULL, 215, 1, 0, '2020-11-12 05:40:56'),
(221, 'Proactive Skills', NULL, 215, 1, 0, '2020-11-12 05:40:56'),
(222, 'Reliability Skills', NULL, 215, 1, 0, '2020-11-12 05:40:56'),
(223, 'Responsibility Skills', NULL, 215, 1, 0, '2020-11-12 05:40:56'),
(224, 'Strong Work Ethic Skills', NULL, 215, 1, 0, '2020-11-12 05:40:56'),
(225, 'Work Under Pressure Skills', NULL, 215, 1, 0, '2020-11-12 05:40:56'),
(226, 'Teamwork', NULL, 0, 1, 0, '2020-11-12 05:40:56'),
(227, 'Collaborative Skills', NULL, 226, 1, 0, '2020-11-12 05:40:56'),
(228, 'Coordinating Skills', NULL, 226, 1, 0, '2020-11-12 05:40:56'),
(229, 'Mentoring Skills', NULL, 226, 1, 0, '2020-11-12 05:40:56'),
(230, 'People Management Skills', NULL, 226, 1, 0, '2020-11-12 05:40:56'),
(231, 'Supervision Skills', NULL, 226, 1, 0, '2020-11-12 05:40:56'),
(232, 'Team Leadership Skills', NULL, 226, 1, 0, '2020-11-12 05:40:56'),
(233, 'Team Management Skills', NULL, 226, 1, 0, '2020-11-12 05:40:56'),
(234, 'Thought Process', NULL, 0, 1, 0, '2020-11-12 05:40:56'),
(235, 'Analytical Thinking Skills', NULL, 234, 1, 0, '2020-11-12 05:40:56'),
(236, 'Attention To Detail Skills', NULL, 234, 1, 0, '2020-11-12 05:40:56'),
(237, 'Awareness Skills', NULL, 234, 1, 0, '2020-11-12 05:40:56'),
(238, 'Creative Thinking Skills', NULL, 234, 1, 0, '2020-11-12 05:40:56'),
(239, 'Detail-oriented Skills', NULL, 234, 1, 0, '2020-11-12 05:40:56'),
(240, 'Evaluation Skills', NULL, 234, 1, 0, '2020-11-12 05:40:56'),
(241, 'Fast Learner Skills', NULL, 234, 1, 0, '2020-11-12 05:40:56'),
(242, 'Patience Skills', NULL, 234, 1, 0, '2020-11-12 05:40:56'),
(243, 'Strategic Thinking Skills', NULL, 234, 1, 0, '2020-11-12 05:40:56'),
(244, 'aa', NULL, 0, 0, 1, '2020-12-08 04:08:37');

-- --------------------------------------------------------

--
-- Table structure for table `talent_reviews`
--

DROP TABLE IF EXISTS `talent_reviews`;
CREATE TABLE IF NOT EXISTS `talent_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `reviewer_id` int(11) NOT NULL,
  `poster_id` int(11) NOT NULL,
  `review` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `easy` tinyint(4) NOT NULL DEFAULT '0',
  `clear` tinyint(4) NOT NULL DEFAULT '0',
  `flexibility` tinyint(4) NOT NULL DEFAULT '0',
  `trustworthiness` tinyint(4) NOT NULL DEFAULT '0',
  `viewed` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `talent_reviews`
--

INSERT INTO `talent_reviews` (`id`, `task_id`, `reviewer_id`, `poster_id`, `review`, `easy`, `clear`, `flexibility`, `trustworthiness`, `viewed`, `created_at`, `updated_at`) VALUES
(1, '494048EA-4A1C-40F6-889E-8DE858400541', 1, 2, 'Good payer', 6, 9, 9, 8, 'N', '2020-11-10 11:49:58', NULL),
(2, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 5, 4, 'Was an easy run!', 7, 8, 9, 10, 'N', '2020-11-10 19:04:37', NULL),
(3, '31EDC848-FED9-4DE4-A3A1-F228551E4064', 5, 4, 'Used hands', 6, 7, 6, 8, 'N', '2020-11-10 19:17:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `number` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `sub_category` int(11) NOT NULL,
  `title` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('by-hour','lump-sum') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'by-hour',
  `state_country` text COLLATE utf8_unicode_ci,
  `location` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `budget` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hours` int(11) DEFAULT NULL,
  `skills` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `start_by` datetime DEFAULT NULL,
  `complete_by` datetime DEFAULT NULL,
  `assigned` datetime DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `status` enum('draft','published','in-progress','marked-completed','completed','cancelled','disputed','closed') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `urgent` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `rejection_rate` int(11) NOT NULL DEFAULT '50' COMMENT 'This value is in percentage %',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `tasks_user_id_index` (`user_id`) USING BTREE,
  KEY `tasks_category_index` (`category`) USING BTREE,
  KEY `tasks_sub_category_index` (`sub_category`) USING BTREE,
  KEY `tasks_type_index` (`type`) USING BTREE,
  KEY `tasks_status_index` (`status`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `number`, `user_id`, `category`, `sub_category`, `title`, `slug`, `description`, `type`, `state_country`, `location`, `budget`, `hours`, `skills`, `start_by`, `complete_by`, `assigned`, `assigned_to`, `status`, `urgent`, `rejection_rate`, `created_at`, `updated_at`) VALUES
('E2BCC7C7-6BB8-48AD-A0A1-20018EE91EB6', 'T20101', '2', 5, 31, 'Fix my washing machine', 'fix-my-washing-machine', 'I need an electrician that can repair my washing machine. Come to my house in Sungai Buloh, Selangor.', 'by-hour', 'Kedah, Malaysia', 'Lot 3, Perdana Quay 2, Telaga Harbour Park, Jalan Pantai Kok, 07000 Langkawi, Kedah, Malaysia', '0.00', 5, '', '2020-11-12 00:00:00', '2020-11-13 00:00:00', '2020-11-10 19:55:22', 1, 'in-progress', '1', 50, '2020-11-10 10:37:07', '2020-11-27 04:59:39'),
('494048EA-4A1C-40F6-889E-8DE858400541', 'T20102', '2', 3, 21, 'Babysit 10 children', 'babysit-10-children', 'Babysitter needed urgently. Female, 9-6pm monday-friday. Love playing teaching and cooking for ten adorable boy toddler age two and four. Come to our house in Kajang, Selangor.', 'lump-sum', 'Penang, Malaysia', 'Harmony Residence, Jalan Tanjong Bungah, 11200 Tanjung Bungah, Penang, Malaysia', '150.00', 0, '', '2020-11-16 00:00:00', '2020-11-20 00:00:00', '2020-11-10 19:07:56', 1, 'closed', '0', 10, '2020-11-10 10:38:31', '2020-11-27 05:24:03'),
('537ED05F-1619-4B96-A6FF-1B20157E7A42', 'T20103', '2', 2, 18, 'Deliver my food', 'deliver-my-food', 'I need a dispatch rider that can deliver my food to Sri Hartamas. My house located in Shah Alam, Selangor', 'by-hour', 'Kuala Lumpur, Malaysia', 'Unnamed Road, 43000 Serdang, Selangor, Malaysia', '0.00', 4, '', '2020-11-20 00:00:00', '2020-11-25 00:00:00', NULL, NULL, 'cancelled', '0', 50, '2020-11-13 12:06:12', '2020-12-02 06:20:16'),
('6C2F88C9-EEB4-4849-9A43-FCC7F4D8A605', 'T20104', '2', 4, 27, 'Sing a song', 'sing-a-song', 'Sing a song while play guitra', 'by-hour', 'Selangor, Malaysia', 'Persiaran Cendekiawan, 43009 Kajang, Selangor, Malaysia', '0.00', 5, '', '2020-11-24 00:00:00', '2020-11-26 00:00:00', '2020-11-11 03:23:29', 4, 'in-progress', '0', 40, '2020-11-10 12:14:25', '2020-11-27 04:59:28'),
('60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', '4', 4, 25, 'Become Acgor', 'become-acgor', 'Actor for one day', 'by-hour', 'Selangor, Malaysia', 'remotely', '200.00', 3, '', '2020-11-12 00:00:00', '2020-11-14 00:00:00', '2020-11-11 02:45:57', 5, 'closed', '0', 10, '2020-11-10 18:33:49', '2020-11-10 19:02:33'),
('31EDC848-FED9-4DE4-A3A1-F228551E4064', 'T20106', '4', 1, 12, 'Cleaning my house', 'cleaning-my-house', 'Sweep and moop', 'lump-sum', 'Selangor, Malaysia', '66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia', '200.00', 0, '', '2020-11-20 00:00:00', '2020-11-20 00:00:00', '2020-11-11 03:14:51', 5, 'closed', '0', 50, '2020-11-10 19:07:33', '2020-11-10 19:16:50'),
('5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', '4', 1, 13, 'Feed my fish', 'feed-my-fish', 'with fish food', 'by-hour', 'Selangor, Malaysia', '66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia', '40.00', 5, '', '2020-11-17 00:00:00', '2020-11-19 00:00:00', '2020-11-11 16:05:02', 6, 'closed', '0', 60, '2020-11-11 03:38:29', '2020-11-11 08:15:43'),
('F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', '10', 2, 16, 'Cook a food', 'cook-a-food', 'asdsadadsad', 'by-hour', 'Selangor, Malaysia', '66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia', '100.00', 3, '', '2020-11-13 00:00:00', '2020-11-14 00:00:00', '2020-11-12 14:48:48', 2, 'completed', '0', 10, '2020-11-12 06:45:14', '2020-11-12 06:55:24'),
('70E8134A-10DB-475B-8180-E74CA2481EF2', 'T20109', '2', 3, 22, 'Wash my cup', 'wash-my-cup', 'asdasdsad', 'by-hour', 'Selangor, Malaysia', '66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia', '200.00', 3, '', '2020-11-13 00:00:00', '2021-01-04 00:00:00', '2020-11-12 15:10:15', 10, 'disputed', '0', 60, '2020-11-12 07:08:40', '2020-10-27 05:15:02'),
('66012D96-BA2D-451F-B413-95F2330B54C1', 'T20110', '10', 1, 12, 'Clean my House', 'clean-my-house', 'asdasdsad', 'by-hour', 'Selangor, Malaysia', '66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia', '200.00', 6, '', '2020-11-13 00:00:00', '2020-11-15 00:00:00', '2020-11-13 00:49:18', 2, 'completed', '0', 10, '2020-11-12 13:19:18', '2020-11-13 05:52:45'),
('4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', '11', 3, 20, 'Fold a bird', 'fold-a-bird', 'with lots of paper', 'by-hour', 'Selangor, Malaysia', '66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia', '0.00', 3, '', '2020-11-19 00:00:00', '2020-11-21 00:00:00', '2020-11-18 13:34:55', 10, 'closed', '0', 10, '2020-11-18 05:30:10', '2020-11-27 04:49:37'),
('CDA7DB4E-8703-453F-B33B-D83285F72054', 'T20112', '11', 2, 19, 'Serve water', 'serve-water', 'Carry around drinks', 'by-hour', 'Selangor, Malaysia', '66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia', '0.00', 4, '', '2020-11-20 00:00:00', '2020-11-21 00:00:00', '2020-11-19 16:34:46', 10, 'closed', '0', 10, '2020-11-19 03:02:40', '2020-11-27 04:49:42'),
('F9604331-09FE-4021-9CDB-C98CFA2BD30E', 'T20113', '10', 1, 11, 'Activate Robot Vacuum daily', 'activate-robot-vacuum-daily', 'at 2pm sharp', 'by-hour', '', 'remotely', '10.00', 1, '', '2020-11-27 00:00:00', '2020-12-10 00:00:00', NULL, NULL, 'cancelled', '0', 10, '2020-11-23 07:00:43', '2020-12-21 03:22:57'),
('09107CD4-29AE-4359-98E7-BE4CE4E9290B', 'T20114', '10', 4, 27, 'Drummer for a day', 'drummer-for-a-day', 'Play drum for a day', 'by-hour', 'Selangor, Malaysia', '66, Jalan Fauna 2B/4, 41200, Selangor, Malaysia', '1202.00', 3, '', '2020-12-04 00:00:00', '2020-12-04 00:00:00', NULL, NULL, 'cancelled', '0', 10, '2020-11-23 07:05:54', '2020-12-10 07:07:31'),
('CB438C15-C218-4262-A3C7-0527B82ED4B8', 'T20115', '10', 2, 15, 'Serve Drinks', 'serve-drinks', 'hahaha', 'lump-sum', '', 'remotely', '100.00', 0, '', '2020-11-02 00:00:00', '2020-11-10 00:00:00', NULL, NULL, 'cancelled', '0', 10, '2020-11-01 07:17:34', '2020-12-01 07:20:41'),
('F93AC8F1-5F1B-4459-B9DB-9EFF86B22965', 'B22965', '11', 2, 17, 'Clean tables', 'clean-tables', 'wipe wipe', 'lump-sum', '', 'remotely', '20.00', 0, '', '2020-12-18 00:00:00', '2020-12-19 00:00:00', NULL, NULL, 'published', '0', 10, '2020-12-03 03:09:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_activity`
--

DROP TABLE IF EXISTS `task_activity`;
CREATE TABLE IF NOT EXISTS `task_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `task_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_number` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `event` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` tinytext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `task_id` (`task_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=146 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_activity`
--

INSERT INTO `task_activity` (`id`, `user_id`, `task_id`, `task_number`, `event`, `message`, `created_at`, `updated_at`) VALUES
(1, 2, 'E2BCC7C7-6BB8-48AD-A0A1-20018EE91EB6', 'T20101', 'published', '', '2020-11-10 10:37:07', NULL),
(2, 2, '494048EA-4A1C-40F6-889E-8DE858400541', 'T20102', 'published', '', '2020-11-10 10:38:31', NULL),
(3, 1, '494048EA-4A1C-40F6-889E-8DE858400541', 'T20102', 'applied', '', '2020-11-10 10:41:30', NULL),
(4, 1, '494048EA-4A1C-40F6-889E-8DE858400541', 'T20102', 'favourite', '', '2020-11-10 10:41:44', NULL),
(5, 1, '494048EA-4A1C-40F6-889E-8DE858400541', 'T20102', 'unfavourite', '', '2020-11-10 10:41:50', NULL),
(6, 2, '494048EA-4A1C-40F6-889E-8DE858400541', 'T20102', 'assigned', '', '2020-11-10 11:07:56', NULL),
(7, 2, '494048EA-4A1C-40F6-889E-8DE858400541', 'T20102', 'payment made', '', '2020-11-10 11:08:24', NULL),
(8, 1, '494048EA-4A1C-40F6-889E-8DE858400541', 'T20102', 'completed', '', '2020-11-10 11:28:23', NULL),
(9, 2, '494048EA-4A1C-40F6-889E-8DE858400541', 'T20102', 'rejected', '', '2020-11-10 11:32:11', NULL),
(10, 1, '494048EA-4A1C-40F6-889E-8DE858400541', 'T20102', 'completed', '', '2020-11-10 11:37:15', NULL),
(11, 2, '494048EA-4A1C-40F6-889E-8DE858400541', 'T20102', 'accepted', '', '2020-11-10 11:44:14', NULL),
(12, 1, '494048EA-4A1C-40F6-889E-8DE858400541', 'T20102', 'payment request', '', '2020-11-10 11:46:16', NULL),
(13, 2, '494048EA-4A1C-40F6-889E-8DE858400541', 'T20102', 'closed', '', '2020-11-10 11:47:50', NULL),
(14, 1, 'E2BCC7C7-6BB8-48AD-A0A1-20018EE91EB6', 'T20101', 'applied', '', '2020-11-10 11:54:59', NULL),
(15, 2, 'E2BCC7C7-6BB8-48AD-A0A1-20018EE91EB6', 'T20101', 'assigned', '', '2020-11-10 11:55:22', NULL),
(16, 2, 'E2BCC7C7-6BB8-48AD-A0A1-20018EE91EB6', 'T20101', 'payment made', '', '2020-11-10 11:56:53', NULL),
(17, 2, '537ED05F-1619-4B96-A6FF-1B20157E7A42', 'T20103', 'published', '', '2020-11-10 12:06:12', NULL),
(18, 1, '537ED05F-1619-4B96-A6FF-1B20157E7A42', 'T20103', 'favourite', '', '2020-11-10 12:06:20', NULL),
(19, 1, '537ED05F-1619-4B96-A6FF-1B20157E7A42', 'T20103', 'applied', '', '2020-11-10 12:06:36', NULL),
(20, 2, '537ED05F-1619-4B96-A6FF-1B20157E7A42', 'T20103', 'assigned', '', '2020-11-10 12:06:55', NULL),
(21, 2, '537ED05F-1619-4B96-A6FF-1B20157E7A42', 'T20103', 'payment made', '', '2020-11-10 12:06:57', NULL),
(22, 2, '6C2F88C9-EEB4-4849-9A43-FCC7F4D8A605', 'T20104', 'published', '', '2020-11-10 12:14:25', NULL),
(23, 4, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'published', '', '2020-11-10 18:33:49', NULL),
(24, 5, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'applied', '', '2020-11-10 18:39:24', NULL),
(25, 0, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'unfavourite', 'Unfavourited automatically, because task was assigned', '2020-11-10 18:45:57', NULL),
(26, 4, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'assigned', '', '2020-11-10 18:45:57', NULL),
(27, 4, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'payment made', '', '2020-11-10 18:45:57', NULL),
(28, 5, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'completed', '', '2020-11-10 18:50:18', NULL),
(29, 0, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'unfavourite', 'Unfavourited automatically, because task was completed', '2020-11-10 18:50:18', NULL),
(30, 4, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'rejected', '', '2020-11-10 18:54:14', NULL),
(31, 5, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'completed', '', '2020-11-10 18:55:19', NULL),
(32, 0, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'unfavourite', 'Unfavourited automatically, because task was completed', '2020-11-10 18:55:19', NULL),
(33, 4, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'accepted', '', '2020-11-10 18:56:30', NULL),
(34, 5, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'payment request', '', '2020-11-10 18:57:21', NULL),
(35, 4, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'accepted', '', '2020-11-10 19:01:59', NULL),
(36, 4, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'closed', '', '2020-11-10 19:02:33', NULL),
(37, 0, '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'T20105', 'unfavourite', 'Unfavourited automatically, because task was closed', '2020-11-10 19:02:33', NULL),
(38, 4, '31EDC848-FED9-4DE4-A3A1-F228551E4064', 'T20106', 'published', '', '2020-11-10 19:07:33', NULL),
(39, 5, '31EDC848-FED9-4DE4-A3A1-F228551E4064', 'T20106', 'applied', '', '2020-11-10 19:08:36', NULL),
(40, 0, '31EDC848-FED9-4DE4-A3A1-F228551E4064', 'T20106', 'unfavourite', 'Unfavourited automatically, because task was assigned', '2020-11-10 19:14:51', NULL),
(41, 4, '31EDC848-FED9-4DE4-A3A1-F228551E4064', 'T20106', 'assigned', '', '2020-11-10 19:14:51', NULL),
(42, 4, '31EDC848-FED9-4DE4-A3A1-F228551E4064', 'T20106', 'payment made', '', '2020-11-10 19:14:51', NULL),
(43, 5, '31EDC848-FED9-4DE4-A3A1-F228551E4064', 'T20106', 'completed', '', '2020-11-10 19:15:55', NULL),
(44, 0, '31EDC848-FED9-4DE4-A3A1-F228551E4064', 'T20106', 'unfavourite', 'Unfavourited automatically, because task was completed', '2020-11-10 19:15:55', NULL),
(45, 4, '31EDC848-FED9-4DE4-A3A1-F228551E4064', 'T20106', 'accepted', '', '2020-11-10 19:16:14', NULL),
(46, 5, '31EDC848-FED9-4DE4-A3A1-F228551E4064', 'T20106', 'payment request', '', '2020-11-10 19:16:27', NULL),
(47, 4, '31EDC848-FED9-4DE4-A3A1-F228551E4064', 'T20106', 'closed', '', '2020-11-10 19:16:50', NULL),
(48, 0, '31EDC848-FED9-4DE4-A3A1-F228551E4064', 'T20106', 'unfavourite', 'Unfavourited automatically, because task was closed', '2020-11-10 19:16:50', NULL),
(49, 4, '6C2F88C9-EEB4-4849-9A43-FCC7F4D8A605', 'T20104', 'applied', '', '2020-11-10 19:22:40', NULL),
(50, 0, '6C2F88C9-EEB4-4849-9A43-FCC7F4D8A605', 'T20104', 'unfavourite', 'Unfavourited automatically, because task was assigned', '2020-11-10 19:23:29', NULL),
(51, 2, '6C2F88C9-EEB4-4849-9A43-FCC7F4D8A605', 'T20104', 'assigned', '', '2020-11-10 19:23:29', NULL),
(52, 2, '6C2F88C9-EEB4-4849-9A43-FCC7F4D8A605', 'T20104', 'payment made', '', '2020-11-10 19:23:29', NULL),
(53, 4, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'published', '', '2020-11-11 03:38:29', NULL),
(54, 6, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'favourite', '', '2020-11-11 03:38:52', NULL),
(55, 6, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'applied', '', '2020-11-11 03:38:59', NULL),
(56, 0, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'unfavourite', 'Unfavourited automatically, because task was assigned', '2020-11-11 08:05:02', NULL),
(57, 4, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'assigned', '', '2020-11-11 08:05:02', NULL),
(58, 4, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'payment made', '', '2020-11-11 08:05:02', NULL),
(59, 6, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'marked completed', '', '2020-11-11 08:06:12', NULL),
(60, 0, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'unfavourite', 'Unfavourited automatically, because task was completed', '2020-11-11 08:06:12', NULL),
(61, 4, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'rejected', '', '2020-11-11 08:12:26', NULL),
(62, 6, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'marked completed', '', '2020-11-11 08:13:05', NULL),
(63, 0, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'unfavourite', 'Unfavourited automatically, because task was completed', '2020-11-11 08:13:05', NULL),
(64, 4, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'accepted', '', '2020-11-11 08:13:17', NULL),
(65, 4, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'completed', '', '2020-11-11 08:13:17', NULL),
(66, 6, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'payment request', '', '2020-11-11 08:15:27', NULL),
(67, 4, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'closed', '', '2020-11-11 08:15:43', NULL),
(68, 0, '5F130178-1620-4912-9A7B-F0C54D145372', 'T20107', 'unfavourite', 'Unfavourited automatically, because task was closed', '2020-11-11 08:15:43', NULL),
(69, 10, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'published', '', '2020-11-12 06:45:14', NULL),
(70, 2, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'applied', '', '2020-11-12 06:48:19', NULL),
(71, 0, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'unfavourite', 'Unfavourited automatically, because task was assigned', '2020-11-12 06:48:48', NULL),
(72, 10, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'assigned', '', '2020-11-12 06:48:48', NULL),
(73, 10, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'payment made', '', '2020-11-12 06:48:48', NULL),
(74, 2, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'marked completed', '', '2020-11-12 06:51:19', '2020-11-25 03:49:46'),
(75, 0, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'unfavourite', 'Unfavourited automatically, because task was completed', '2020-11-12 06:51:19', NULL),
(76, 10, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'rejected', '', '2020-11-12 06:51:41', '2020-12-02 06:57:08'),
(77, 2, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'marked completed', '', '2020-11-12 06:54:55', '2020-11-25 03:49:46'),
(78, 0, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'unfavourite', 'Unfavourited automatically, because task was completed', '2020-11-12 06:54:55', NULL),
(79, 10, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'rejected', '', '2020-11-12 06:55:06', '2020-12-02 06:57:08'),
(80, 2, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'marked completed', '', '2020-11-12 06:55:16', '2020-11-25 03:49:46'),
(81, 0, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'unfavourite', 'Unfavourited automatically, because task was completed', '2020-11-12 06:55:16', NULL),
(82, 10, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'accepted', '', '2020-11-12 06:55:24', '2020-12-02 06:57:08'),
(83, 10, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'completed', '', '2020-11-12 06:55:25', NULL),
(84, 2, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'payment request', '', '2020-11-12 06:56:06', NULL),
(85, 2, '70E8134A-10DB-475B-8180-E74CA2481EF2', 'T20109', 'published', '', '2020-11-12 07:08:40', NULL),
(86, 10, '70E8134A-10DB-475B-8180-E74CA2481EF2', 'T20109', 'applied', '', '2020-11-12 07:09:34', NULL),
(87, 0, '70E8134A-10DB-475B-8180-E74CA2481EF2', 'T20109', 'unfavourite', 'Unfavourited automatically, because task was assigned', '2020-11-12 07:10:15', NULL),
(88, 2, '70E8134A-10DB-475B-8180-E74CA2481EF2', 'T20109', 'assigned', '', '2020-11-12 07:10:15', NULL),
(89, 2, '70E8134A-10DB-475B-8180-E74CA2481EF2', 'T20109', 'payment made', '', '2020-11-12 07:10:15', NULL),
(90, 10, '66012D96-BA2D-451F-B413-95F2330B54C1', 'T20110', 'published', '', '2020-11-12 13:19:18', NULL),
(91, 2, 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'T20108', 'payment request', '', '2020-11-12 16:40:32', NULL),
(92, 2, '66012D96-BA2D-451F-B413-95F2330B54C1', 'T20110', 'applied', '', '2020-11-12 16:48:18', NULL),
(93, 0, '66012D96-BA2D-451F-B413-95F2330B54C1', 'T20110', 'unfavourite', 'Unfavourited automatically, because task was assigned', '2020-11-12 16:49:18', NULL),
(94, 10, '66012D96-BA2D-451F-B413-95F2330B54C1', 'T20110', 'assigned', '', '2020-11-12 16:49:18', NULL),
(95, 10, '66012D96-BA2D-451F-B413-95F2330B54C1', 'T20110', 'payment made', '', '2020-11-12 16:49:18', NULL),
(96, 10, '70E8134A-10DB-475B-8180-E74CA2481EF2', 'T20109', 'marked completed', '', '2020-11-12 16:50:25', '2020-11-27 04:23:58'),
(97, 0, '70E8134A-10DB-475B-8180-E74CA2481EF2', 'T20109', 'unfavourite', 'Unfavourited automatically, because task was completed', '2020-11-12 16:50:25', NULL),
(98, 2, '66012D96-BA2D-451F-B413-95F2330B54C1', 'T20110', 'marked completed', '', '2020-11-12 16:50:53', '2020-12-02 06:26:18'),
(99, 0, '66012D96-BA2D-451F-B413-95F2330B54C1', 'T20110', 'unfavourite', 'Unfavourited automatically, because task was completed', '2020-11-12 16:50:53', NULL),
(100, 10, '66012D96-BA2D-451F-B413-95F2330B54C1', 'T20110', 'accepted', '', '2020-11-13 05:52:45', '2020-12-02 06:22:28'),
(101, 10, '66012D96-BA2D-451F-B413-95F2330B54C1', 'T20110', 'completed', '', '2020-11-13 05:52:46', NULL),
(102, 11, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'published', '', '2020-11-18 05:30:10', NULL),
(103, 10, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'applied', '', '2020-11-18 05:30:19', NULL),
(104, 0, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'unfavourite', 'Unfavourited automatically, because task was assigned', '2020-11-18 05:34:55', NULL),
(105, 11, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'assigned', '', '2020-11-18 05:34:55', NULL),
(106, 11, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'payment made', '', '2020-11-18 05:34:55', NULL),
(107, 10, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'marked completed', '', '2020-11-18 05:35:40', '2020-11-25 04:36:26'),
(108, 0, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'unfavourite', 'Unfavourited automatically, because task was completed', '2020-11-18 05:35:40', NULL),
(109, 11, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'rejected', '', '2020-11-18 05:37:47', '2020-12-02 06:56:46'),
(110, 10, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'marked completed', '', '2020-11-18 05:38:05', '2020-11-25 04:36:26'),
(111, 0, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'unfavourite', 'Unfavourited automatically, because task was completed', '2020-11-18 05:38:05', NULL),
(112, 11, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'accepted', '', '2020-11-18 05:38:14', '2020-12-02 06:56:46'),
(113, 11, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'completed', '', '2020-11-18 05:38:15', NULL),
(114, 10, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'payment request', '', '2020-11-18 05:38:29', NULL),
(115, 11, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'closed', '', '2020-11-18 05:47:14', NULL),
(116, 0, '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'T20111', 'unfavourite', 'Unfavourited automatically, because task was closed', '2020-11-18 05:47:14', NULL),
(117, 11, 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'T20112', 'published', '', '2020-11-19 03:02:40', NULL),
(118, 10, 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'T20112', 'applied', '', '2020-11-19 08:34:28', NULL),
(119, 0, 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'T20112', 'unfavourite', 'Unfavourited automatically, because task was assigned', '2020-11-19 08:34:46', NULL),
(120, 11, 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'T20112', 'assigned', '', '2020-11-19 08:34:46', NULL),
(121, 11, 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'T20112', 'payment made', '', '2020-11-19 08:34:46', NULL),
(122, 10, 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'T20112', 'marked completed', '', '2020-11-19 08:36:18', '2020-11-19 08:37:30'),
(123, 0, 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'T20112', 'unfavourite', 'Unfavourited automatically, because task was completed', '2020-11-19 08:36:18', NULL),
(124, 11, 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'T20112', 'accepted', '', '2020-11-19 08:36:34', '2020-12-02 02:15:14'),
(125, 11, 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'T20112', 'completed', '', '2020-11-19 08:36:35', NULL),
(126, 10, 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'T20112', 'payment request', '', '2020-11-19 08:36:53', NULL),
(127, 11, 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'T20112', 'closed', '', '2020-11-19 08:37:29', NULL),
(128, 0, 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'T20112', 'unfavourite', 'Unfavourited automatically, because task was closed', '2020-11-19 08:37:29', NULL),
(129, 10, 'F9604331-09FE-4021-9CDB-C98CFA2BD30E', 'T20113', 'published', '', '2020-11-23 07:00:43', NULL),
(130, 10, '09107CD4-29AE-4359-98E7-BE4CE4E9290B', 'T20114', 'published', '', '2020-11-23 07:05:54', NULL),
(131, 10, '70E8134A-10DB-475B-8180-E74CA2481EF2', 'T20109', 'payment request', '', '2020-11-27 04:03:46', NULL),
(132, 2, '70E8134A-10DB-475B-8180-E74CA2481EF2', 'T20109', 'rejected', '', '2020-11-27 04:05:38', '2020-12-02 02:13:29'),
(133, 10, '70E8134A-10DB-475B-8180-E74CA2481EF2', 'T20109', 'marked completed', '', '2020-11-27 04:06:06', '2020-11-27 04:23:58'),
(134, 0, '', 'T20109', 'unfavourite', 'Unfavourited automatically, because task was completed', '2020-11-27 04:06:06', NULL),
(135, 2, '70E8134A-10DB-475B-8180-E74CA2481EF2', 'T20109', 'accepted', '', '2020-11-27 04:07:49', '2020-12-02 02:13:29'),
(136, 2, '70E8134A-10DB-475B-8180-E74CA2481EF2', 'T20109', 'completed', '', '2020-11-27 04:07:50', NULL),
(137, 2, '70E8134A-10DB-475B-8180-E74CA2481EF2', 'T20109', 'closed', '', '2020-11-27 04:23:55', NULL),
(138, 0, '', 'T20109', 'unfavourite', 'Unfavourited automatically, because task was closed', '2020-11-27 04:23:56', NULL),
(139, 10, 'CB438C15-C218-4262-A3C7-0527B82ED4B8', 'T20115', 'published', '', '2020-12-01 07:17:34', NULL),
(140, 0, 'CB438C15-C218-4262-A3C7-0527B82ED4B8', 'T20115', 'cancelled', 'Cancelled automatically, no applicants and passed end date', '2020-12-01 07:20:41', NULL),
(141, 0, '537ED05F-1619-4B96-A6FF-1B20157E7A42', 'T20103', 'cancelled', 'Cancelled automatically, no applicants and passed end date', '2020-12-02 06:20:16', NULL),
(142, 11, 'F93AC8F1-5F1B-4459-B9DB-9EFF86B22965', 'B22965', 'published', '', '2020-12-03 03:09:00', NULL),
(143, 10, 'F93AC8F1-5F1B-4459-B9DB-9EFF86B22965', 'B22965', 'applied', '', '2020-12-03 03:09:49', NULL),
(144, 0, '09107CD4-29AE-4359-98E7-BE4CE4E9290B', 'T20114', 'cancelled', 'Cancelled automatically, no applicants and passed end date', '2020-12-10 07:07:31', NULL),
(145, 0, 'F9604331-09FE-4021-9CDB-C98CFA2BD30E', 'T20113', 'cancelled', 'Cancelled automatically, no applicants and passed end date', '2020-12-21 03:22:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_attachments`
--

DROP TABLE IF EXISTS `task_attachments`;
CREATE TABLE IF NOT EXISTS `task_attachments` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_attachments_task_id_attachment_unique` (`task_id`,`attachment`),
  KEY `task_attachments_task_id_index` (`task_id`),
  KEY `task_attachments_type_index` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_attachments`
--

INSERT INTO `task_attachments` (`id`, `task_id`, `attachment`, `type`, `created_at`, `updated_at`) VALUES
('390F20DC-DBA2-4C79-989F-775DB8C77553', 'E2BCC7C7-6BB8-48AD-A0A1-20018EE91EB6', '/files/tasks/2/chats.jpeg', 'jpeg', '2020-11-10 10:37:07', NULL),
('DCEA3ACE-6B80-48AF-902E-5CAB8C4424F6', 'E2BCC7C7-6BB8-48AD-A0A1-20018EE91EB6', '/files/tasks/2/Foo-Bar.ppt', 'ppt', '2020-11-10 10:37:07', NULL),
('144D605A-8469-497A-A317-55BFF8C8A32B', '494048EA-4A1C-40F6-889E-8DE858400541', '/files/tasks/2/pexels-photo-220453.jpeg', 'jpeg', '2020-11-10 10:38:31', NULL),
('89C9D7CD-3806-4202-BB32-B56541134C68', '494048EA-4A1C-40F6-889E-8DE858400541', '/files/tasks/2/Hello-World.pptx', 'pptx', '2020-11-10 10:38:31', NULL),
('048D4504-D7AD-4E97-ADCA-1CB6D3A809C3', '537ED05F-1619-4B96-A6FF-1B20157E7A42', '/files/tasks/2/chats.jpeg', 'jpeg', '2020-11-10 12:06:12', NULL),
('45EFDD1E-55F8-4B26-87DD-7D7A4526D6F8', '6C2F88C9-EEB4-4849-9A43-FCC7F4D8A605', '/files/tasks/2/chats.jpeg', 'jpeg', '2020-11-10 12:14:25', NULL),
('8D546F5A-1F1A-4746-899B-2FF4435FBD96', 'CDA7DB4E-8703-453F-B33B-D83285F72054', '/files/tasks/11/Whack-a-virus-game-GamePlay.png', 'png', '2020-11-19 03:02:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_category`
--

DROP TABLE IF EXISTS `task_category`;
CREATE TABLE IF NOT EXISTS `task_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) NOT NULL,
  `sortby` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task_category`
--

INSERT INTO `task_category` (`id`, `images`, `title`, `description`, `parent`, `sortby`, `status`) VALUES
(1, '', 'Accounting/Finance', '', 0, 0, 1),
(2, '', 'Audit & Taxation', '', 1, 0, 1),
(3, '', 'General/Cost Accounting', '', 1, 0, 1),
(4, '', 'Corporate Finance/Investment', '', 1, 0, 1),
(5, '', 'Banking/Financial', '', 1, 0, 1),
(6, '', 'Admin/Human Resources', '', 0, 0, 1),
(7, '', 'Clerical/General Admin', '', 6, 0, 1),
(8, '', 'Human Resources', '', 6, 0, 1),
(9, '', 'Secretarial/Executive Personal Assistant', '', 6, 0, 1),
(10, '', 'Top Management', '', 6, 0, 1),
(11, '', 'Sales/Marketing', '', 0, 0, 1),
(12, '', 'Marketing/Business Dev', '', 11, 0, 1),
(13, '', 'Sales - Corporate', '', 11, 0, 1),
(14, '', 'Sales - Eng/Tech/IT', '', 11, 0, 1),
(15, '', 'Sales - Financial Services', '', 11, 0, 1),
(16, '', 'Retail Sales', '', 11, 0, 1),
(17, '', 'Merchandising', '', 11, 0, 1),
(18, '', 'Telesales/Telemarketing', '', 11, 0, 1),
(19, '', 'E-commerce', '', 11, 0, 1),
(20, '', 'Digital Marketing', '', 11, 0, 1),
(21, '', 'Arts/Media/Communications', '', 0, 0, 1),
(22, '', 'Advertising', '', 20, 0, 1),
(23, '', 'Arts/Creative Design', '', 20, 0, 1),
(24, '', 'Entertainment', '', 20, 0, 1),
(25, '', 'Public Relations', '', 20, 0, 1),
(26, '', 'Services', '', 0, 0, 1),
(27, '', 'Personal Care', '', 25, 0, 1),
(28, '', 'Armed Forces', '', 25, 0, 1),
(29, '', 'Social Services', '', 25, 0, 1),
(30, '', 'Customer Service', '', 25, 0, 1),
(31, '', 'Lawyer/Legal Asst', '', 25, 0, 1),
(32, '', 'Logistics/Supply Chain', '', 25, 0, 1),
(33, '', 'Tech & Helpdesk Support', '', 25, 0, 1),
(34, '', 'Hotel/Restaurant', '', 0, 0, 1),
(35, '', 'Food/Beverage/Restaurant', '', 34, 0, 1),
(36, '', 'Hotel/Tourism', '', 34, 0, 1),
(37, '', 'Education/Training', '', 0, 0, 1),
(38, '', 'Education', '', 37, 0, 1),
(39, '', 'Training & Development', '', 37, 0, 1),
(40, '', 'Computer/Information Technology', '', 0, 0, 1),
(41, '', 'IT-Software', '', 40, 0, 1),
(42, '', 'IT-Hardware', '', 40, 0, 1),
(43, '', 'IT-Network/Sys/DB Admin', '', 40, 0, 1),
(44, '', 'Engineering', '', 0, 0, 1),
(45, '', 'Chemical Engineering', '', 44, 0, 1),
(46, '', 'Electronics', '', 44, 0, 1),
(47, '', 'Electrical', '', 44, 0, 1),
(48, '', 'Other Engineering', '', 44, 0, 1),
(49, '', 'Environmental', '', 44, 0, 1),
(50, '', 'Oil/Gas', '', 44, 0, 1),
(51, '', 'Mechanical', '', 44, 0, 1),
(52, '', 'Industrial Engineering', '', 44, 0, 1),
(53, '', 'Manufacturing', '', 0, 0, 1),
(54, '', 'Maintenance', '', 53, 0, 1),
(55, '', 'Purchasing/Material Mgmt', '', 53, 0, 1),
(56, '', 'Manufacturing', '', 53, 0, 1),
(57, '', 'Process Control', '', 53, 0, 1),
(58, '', 'Quality Assurance', '', 53, 0, 1),
(59, '', 'Building/Construction', '', 0, 0, 1),
(60, '', 'Property/Real Estate', '', 59, 0, 1),
(61, '', 'Architect/Interior Design', '', 59, 0, 1),
(62, '', 'Architect/Interior Design', '', 59, 0, 1),
(63, '', 'Civil/Construction', '', 59, 0, 1),
(64, '', 'Quantity Surveying', '', 59, 0, 1),
(65, '', 'Sciences', '', 0, 0, 1),
(66, '', 'Agriculture', '', 65, 0, 1),
(67, '', 'Actuarial/Statistics', '', 65, 0, 1),
(68, '', 'Food Tech/Nutritionist', '', 65, 0, 1),
(69, '', 'Geology/Geophysics', '', 65, 0, 1),
(70, '', 'Aviation', '', 65, 0, 1),
(71, '', 'Biotechnology', '', 65, 0, 1),
(72, '', 'Chemistry', '', 65, 0, 1),
(73, '', 'Science & Technology', '', 65, 0, 1),
(74, '', 'Biomedical', '', 65, 0, 1),
(75, '', 'Healthcare', '', 0, 0, 1),
(76, '', 'Practitioner/Medical Asst', '', 75, 0, 1),
(77, '', 'Pharmacy', '', 75, 0, 1),
(78, '', 'Diagnosis/Others', '', 75, 0, 1),
(79, '', 'Others', '', 0, 0, 1),
(80, '', 'Journalist/Editors', '', 79, 0, 1),
(81, '', 'General Work', '', 79, 0, 1),
(82, '', 'Publishing', '', 79, 0, 1),
(83, '', 'Others', '', 79, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `task_payment`
--

DROP TABLE IF EXISTS `task_payment`;
CREATE TABLE IF NOT EXISTS `task_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `task_id` varchar(64) NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `total` float(10,2) NOT NULL,
  `service_charge` float(10,2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(64) NOT NULL,
  `phone` varchar(16) NOT NULL,
  `date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `task_payment`
--

INSERT INTO `task_payment` (`id`, `user_id`, `task_id`, `assigned_to`, `total`, `service_charge`, `name`, `email`, `phone`, `date`, `status`) VALUES
(1, 11, 'F93AC8F1-5F1B-4459-B9DB-9EFF86B22965', 10, 20.00, 0.00, 'Bytetoc Computing', 'hr@byte2c.com', '+60102391023921', '2020-12-14 13:08:32', 999),
(2, 11, 'F93AC8F1-5F1B-4459-B9DB-9EFF86B22965', 10, 20.00, 0.00, 'Bytetoc Computing', 'hr@byte2c.com', '+60102391023921', '2020-12-14 13:09:45', 0);

-- --------------------------------------------------------

--
-- Table structure for table `task_planning`
--

DROP TABLE IF EXISTS `task_planning`;
CREATE TABLE IF NOT EXISTS `task_planning` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `task_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `task_id` (`task_id`),
  KEY `date` (`date`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_planning`
--

INSERT INTO `task_planning` (`id`, `user_id`, `task_id`, `date`, `start`, `end`, `created_at`, `updated_at`) VALUES
(14, 1, '494048EA-4A1C-40F6-889E-8DE858400541', '2020-11-18', '18:25:00', '21:25:00', '2020-11-10 11:25:19', NULL),
(19, 2, 'F87485C2-9653-4BFA-826E-0C2A235C7144', '2020-11-13', '01:50:00', '03:50:00', '2020-11-12 06:50:39', NULL),
(18, 2, 'F87485C2-9653-4BFA-826E-0C2A235C7144', '2020-11-13', '01:49:00', '02:49:00', '2020-11-12 06:50:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_questions`
--

DROP TABLE IF EXISTS `task_questions`;
CREATE TABLE IF NOT EXISTS `task_questions` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_questions_id_task_id_unique` (`id`,`task_id`),
  KEY `task_questions_task_id_index` (`task_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_questions`
--

INSERT INTO `task_questions` (`id`, `task_id`, `question`, `created_at`, `updated_at`) VALUES
('C1B4347E-0FEF-4F24-8F49-93DFE5950107', 'E2BCC7C7-6BB8-48AD-A0A1-20018EE91EB6', 'Question 1?', '2020-11-10 10:37:07', NULL),
('637F4562-C106-4D8A-963F-F461B047B5CC', 'E2BCC7C7-6BB8-48AD-A0A1-20018EE91EB6', 'Question 2?', '2020-11-10 10:37:07', NULL),
('8CBDAD73-8C01-40C8-B757-ACBF003E6A37', '494048EA-4A1C-40F6-889E-8DE858400541', 'Question 1?', '2020-11-10 10:38:31', NULL),
('7BFE73A0-DBB8-4B56-9E15-1CAC780A5DAF', '494048EA-4A1C-40F6-889E-8DE858400541', 'Question 2?', '2020-11-10 10:38:31', NULL),
('A7C13D53-079A-4E87-AB46-E511D156A4FA', '537ED05F-1619-4B96-A6FF-1B20157E7A42', 'Question 1?', '2020-11-10 12:06:12', NULL),
('E371C680-966E-490A-9B5C-6EC1953193EB', '537ED05F-1619-4B96-A6FF-1B20157E7A42', 'Question 2?', '2020-11-10 12:06:12', NULL),
('D5020F1A-5873-480A-A759-59C074B7C6D9', '6C2F88C9-EEB4-4849-9A43-FCC7F4D8A605', 'Questuin1', '2020-11-10 12:14:25', NULL),
('05B4F1B7-4E2C-4FAD-8D1A-C0929AAD17A0', 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'q1', '2020-11-19 03:02:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_tags`
--

DROP TABLE IF EXISTS `task_tags`;
CREATE TABLE IF NOT EXISTS `task_tags` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_tags_task_id_tag_unique` (`task_id`,`tag`),
  KEY `task_tags_task_id_index` (`task_id`),
  KEY `task_tags_tag_index` (`tag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_tags`
--

INSERT INTO `task_tags` (`id`, `task_id`, `tag`, `created_at`, `updated_at`) VALUES
('FC374EA7-32B3-4087-83BE-1C8F934F8A15', 'E2BCC7C7-6BB8-48AD-A0A1-20018EE91EB6', 'diy', '2020-11-10 10:37:07', NULL),
('58191035-9A6E-4EE3-8B6E-B19673B0BBFA', 'E2BCC7C7-6BB8-48AD-A0A1-20018EE91EB6', 'electrician', '2020-11-10 10:37:07', NULL),
('1184313C-245D-4CEF-9152-0E6F73825A5F', '494048EA-4A1C-40F6-889E-8DE858400541', 'babysit', '2020-11-10 10:38:31', NULL),
('6C402936-FB58-4706-A7C4-742A8F3D111A', '494048EA-4A1C-40F6-889E-8DE858400541', 'child', '2020-11-10 10:38:31', NULL),
('1E75C92D-8E6B-47E8-923A-05D27982A9D1', '494048EA-4A1C-40F6-889E-8DE858400541', 'kids', '2020-11-10 10:38:31', NULL),
('A10046DC-97D6-4BE9-94F8-235CC67AE72C', '537ED05F-1619-4B96-A6FF-1B20157E7A42', 'grabfood', '2020-11-10 12:06:12', NULL),
('EA482AF6-1EDC-4315-A0EC-896BEE589ECB', '537ED05F-1619-4B96-A6FF-1B20157E7A42', 'foodpand', '2020-11-10 12:06:12', NULL),
('E1A7F78E-6208-484D-B4D6-7786BAF22387', '6C2F88C9-EEB4-4849-9A43-FCC7F4D8A605', 'music', '2020-11-10 12:14:25', NULL),
('5F937633-36D8-4EFC-9E70-D7D688B599C1', '5F130178-1620-4912-9A7B-F0C54D145372', 'seeker', '2020-11-11 03:38:29', NULL),
('99235D47-3CC3-4957-94CC-DB21F99379FF', '5F130178-1620-4912-9A7B-F0C54D145372', 'fish food', '2020-11-11 03:38:29', NULL),
('22C2023D-F501-4D2F-B55F-1092CD2794EA', 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'serve', '2020-11-19 03:02:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `temp_token`
--

DROP TABLE IF EXISTS `temp_token`;
CREATE TABLE IF NOT EXISTS `temp_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` text,
  `email` text,
  `token_status` int(11) DEFAULT '0' COMMENT '0- Not Used 99 - Deleted',
  `created_by` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `expiry_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_token`
--

INSERT INTO `temp_token` (`id`, `token`, `email`, `token_status`, `created_by`, `expiry_date`) VALUES
(1, 'fcbb2c9ae69cab21', 'shah@inspirenow.com.my', 99, '2020-11-10 09:28:23', '2020-11-10 10:28:23'),
(2, '5128220a130f74ee', 'syafiq@inspirenow.com.my', 99, '2020-11-10 09:28:59', '2020-11-10 10:28:59'),
(3, '63da0139624efcfc', 'shah@inka.my', 99, '2020-11-12 03:42:03', '2020-11-12 04:42:03'),
(4, 'aa783d870198955b', 'syafiq@inka.my', 99, '2020-11-12 03:42:03', '2020-11-12 04:42:03'),
(5, '061692e69fc20ad1', 'syafiq@inka.my', 99, '2020-11-12 03:48:38', '2020-11-12 04:48:38'),
(6, 'bb6bf16007fddcf8', 'ken@inspirenow.com.my', 99, '2020-11-12 04:10:16', '2020-11-12 05:10:16'),
(7, '1a8860472ba2e8f4', 'kevin@byte2c.com', 99, '2020-11-12 04:14:38', '2020-11-12 05:14:38'),
(8, 'af93b25397324e2f', 'asa@asda.com', 0, '2020-11-23 08:23:40', '2020-11-23 09:23:40'),
(9, '24014ad0094a7dc0', 'hi@rimbaride.com', 0, '2020-11-23 08:24:08', '2020-11-23 09:24:08');

-- --------------------------------------------------------

--
-- Table structure for table `tracking`
--

DROP TABLE IF EXISTS `tracking`;
CREATE TABLE IF NOT EXISTS `tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `checkout_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_admins`
--

DROP TABLE IF EXISTS `user_admins`;
CREATE TABLE IF NOT EXISTS `user_admins` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_username` text COLLATE utf8_bin NOT NULL,
  `admin_password` text COLLATE utf8_bin NOT NULL,
  `admin_name` text COLLATE utf8_bin NOT NULL,
  `admin_contact` text COLLATE utf8_bin NOT NULL,
  `admin_email` text COLLATE utf8_bin NOT NULL,
  `admin_status` int(1) NOT NULL,
  `admin_role` tinyint(1) NOT NULL,
  `admin_registered` datetime NOT NULL,
  `admin_last_activity` datetime NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user_admins`
--

INSERT INTO `user_admins` (`admin_id`, `admin_username`, `admin_password`, `admin_name`, `admin_contact`, `admin_email`, `admin_status`, `admin_role`, `admin_registered`, `admin_last_activity`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Kev', '60123456789', 'kevin@byte2c.com', 1, 1, '2012-10-24 16:00:58', '2018-01-30 16:22:03'),
(2, 'b2cadmin', 'd9f2e096af0c36ce0c509f779220a0b7', 'Byte2c Admin', '0176995737', 'admin@byte2c.com', 1, 1, '2017-10-25 16:14:02', '2017-10-25 16:14:02');

-- --------------------------------------------------------

--
-- Table structure for table `user_rating`
--

DROP TABLE IF EXISTS `user_rating`;
CREATE TABLE IF NOT EXISTS `user_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` char(36) CHARACTER SET utf8 NOT NULL,
  `project_id` char(36) CHARACTER SET utf8 NOT NULL,
  `project_type` enum('task','job') CHARACTER SET utf8 NOT NULL DEFAULT 'task',
  `timeliness` decimal(10,1) NOT NULL DEFAULT '0.0',
  `quality` decimal(10,1) NOT NULL DEFAULT '0.0',
  `communication` decimal(10,1) NOT NULL DEFAULT '0.0',
  `responsiveness` decimal(10,1) NOT NULL DEFAULT '0.0',
  `review` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `author_id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT 'This would contain the user_id whom wrote the review',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_task`
--

DROP TABLE IF EXISTS `user_task`;
CREATE TABLE IF NOT EXISTS `user_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `task_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `type` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'task',
  `viewed` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_task`
--

INSERT INTO `user_task` (`id`, `user_id`, `task_id`, `type`, `viewed`, `created_at`, `updated_at`) VALUES
(1, '1', '494048EA-4A1C-40F6-889E-8DE858400541', 'task', 'Y', '2020-11-10 10:41:30', '2020-11-10 10:42:47'),
(2, '1', 'E2BCC7C7-6BB8-48AD-A0A1-20018EE91EB6', 'task', 'Y', '2020-11-10 11:54:59', '2020-11-10 11:55:05'),
(3, '1', '537ED05F-1619-4B96-A6FF-1B20157E7A42', 'task', 'Y', '2020-11-10 12:06:36', '2020-11-10 12:06:52'),
(4, '5', '60C8F385-EE68-4346-BD47-59DBD36ABA44', 'task', 'Y', '2020-11-10 18:39:24', '2020-11-10 18:39:36'),
(5, '5', '31EDC848-FED9-4DE4-A3A1-F228551E4064', 'task', 'Y', '2020-11-10 19:08:36', '2020-11-10 19:08:43'),
(6, '4', '6C2F88C9-EEB4-4849-9A43-FCC7F4D8A605', 'task', 'Y', '2020-11-10 19:22:40', '2020-11-10 19:23:09'),
(7, '6', '5F130178-1620-4912-9A7B-F0C54D145372', 'task', 'Y', '2020-11-11 03:38:59', '2020-11-11 07:58:59'),
(8, '2', 'F87485C2-9653-4BFA-826E-0C2A235C7144', 'task', 'Y', '2020-11-12 06:48:19', '2020-11-12 06:48:41'),
(9, '10', '70E8134A-10DB-475B-8180-E74CA2481EF2', 'task', 'Y', '2020-11-12 07:09:34', '2020-11-12 07:09:53'),
(10, '2', '66012D96-BA2D-451F-B413-95F2330B54C1', 'task', 'Y', '2020-11-12 16:48:18', '2020-11-12 16:48:35'),
(11, '10', 'EE236862-F5EE-46CA-A70B-851A50C1A7A4', 'job', 'N', '2020-11-15 20:29:29', NULL),
(12, '12', 'EE236862-F5EE-46CA-A70B-851A50C1A7A4', 'job', 'N', '2020-11-15 20:37:33', NULL),
(13, '10', '769D0CD3-2F98-43E4-AAD2-D36C74DA3679', 'job', 'N', '2020-11-16 19:15:02', NULL),
(14, '10', '9B07069A-7C92-424F-9F42-6BEE7EA0A172', 'job', 'Y', '2020-11-17 06:09:22', '2020-11-17 18:31:59'),
(15, '10', '4F2165B6-C1FD-4F20-8287-2F9B84BF97AA', 'task', 'Y', '2020-11-18 05:30:19', '2020-11-18 05:34:26'),
(16, '10', '8A72888F-4FBF-4D14-8E3F-A9E95711E581', 'job', 'Y', '2020-11-18 07:20:48', '2020-11-18 07:22:37'),
(17, '10', 'CDA7DB4E-8703-453F-B33B-D83285F72054', 'task', 'Y', '2020-11-19 08:34:28', '2020-11-19 08:34:40'),
(18, '10', 'F93AC8F1-5F1B-4459-B9DB-9EFF86B22965', 'task', 'Y', '2020-12-03 03:09:49', '2020-12-03 03:10:04');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

DROP TABLE IF EXISTS `visitors`;
CREATE TABLE IF NOT EXISTS `visitors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` text NOT NULL,
  `visit_count` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks` ADD FULLTEXT KEY `title_slug` (`title`,`slug`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
